<?php
include("include/config.php");
include("include/functions/import.php");

include_once('libraries/eConnect.class.php');
include_once('libraries/emailsms.class.php');
require_once('libraries/cache.class.php');

$econnectClass = new econnect();
$cronClass = new cron();
$emailClass = new email();

//$cronClass->cancelPendingPayment('752611350');

/* $config['working_process']= array(
							'provide_instructions_days' => 2,
							'review_period' => 5,
							'revision_period_procent' => 20,
							'max_reject_times' => 1,
							'payment_day_of_month' => 20
							); */


$cronClass->InitialTasks_closeToLate();
//d('end Initial CloseToLate');
$cronClass->LateInitialTasks();
//d('end Initial Late Tasks');
$cronClass->OngoingTasks_closeToLate();
//d('end Ongoing CloseToLate');
$cronClass->OngoingTasks_Late();
//d('end Ongoing Late');
$cronClass->cancelMutuallyRequestedTasks();
//d('end Cancel Mutually Requested Tasks (late to respond)');
$cronClass->autoCompleteTasks();
//d('end Auto Complete Tasks');
$cronClass->refundDelayedIfVerified();
//d('end Refund Delayed');
$cronClass->sendWDemails();
//d('end Sending Withdrawal Emails');
$cronClass->countDealsPerCat();
//d('end Count Deals per Cat');
//d(strftime('%Y-%m-%d %H:%M:%S'));
$cronClass->removeGarbage();
//d('end Garbage Removed');
$cronClass->sendDelayedMails();
//d('end Delayed Mails Sent');
$cronClass->activateDeals();
d('end Deals Activated');

class cron {
	private $INN_access_token='';

	function CancelPendingPayment($transactionCode) {
		global $econnectClass;

		if (!$econnectClass->INN_access_token) $econnectClass->authorizeINN();
		$response = $econnectClass->cancelPendingPayment($transactionCode, $econnectClass->INN_access_token);
	}

/**
* Tasks that are initiated, but no required details were provided during half period:
* $config [ 'working_process' ] [ 'provide_instructions_days' ] / 2
* Will send a notification to the Buyer to provide with the required details
* Task is supposed to run once per day
**/
	function InitialTasks_closeToLate() {
		global $conn, $config, $emailClass, $lang, $cache;
		$query="SELECT o.`OID`, m.`email`, m.`fullname` FROM `orders` o, `members` m WHERE o.`status`='0' AND  o.`time_added` > '".($_SERVER['REQUEST_TIME'] - $config['working_process']['provide_instructions_days'] * 86400) ."' AND o.`time_added` < '".($_SERVER['REQUEST_TIME'] - $config['working_process']['provide_instructions_days'] * 43200)."' AND o.`USERID`=m.`USERID`";
		//d($query);
		$CloseToLate = $cache->get('CloseToLate');
		$res = $conn->execute($query);
		$rows = $res->getrows();
		foreach ($rows as $r) {
			if ($CloseToLate[$r['OID']])
				continue;
			else {
				$emailClass->send($r['email'], $lang['645'], sprintf($lang['646'],fname($r['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
				$CloseToLate[$r['OID']] = 1;
			}
		}
		$cache->set('CloseToLate', $CloseToLate, 30*24*3600);
	}

	function LateInitialTasks() {
		global $conn, $config, $emailClass, $lang, $cache;

		$query="SELECT o.`OID`, o.`PID`, o.`USERID`, o.`price`, m.`email` as buyer_email, m.`pinprotect` as buyer_verified, m.`fullname` FROM `orders` o, `members` m WHERE o.`status`='0' AND o.`time_added` < '".($_SERVER['REQUEST_TIME'] - $config['working_process']['provide_instructions_days'] * 86400)."' AND o.`USERID`=m.`USERID`";
		//d($query);
		$results=$conn->execute($query);
		$res = $conn->execute($query);
		$rows = $res->getrows();
		$LateInitialTasks = $cache->get('LateInitialTasks');
		//d($rows);
		foreach ($rows as $r) {
			if ($LateInitialTasks[$r['OID']])
				continue;
			else {
				$LateInitialTasks[$r['OID']] = 1;
			}
			$q2 = "SELECT m.`email`, m.`fullname` FROM `members` m, `posts` p WHERE p.`PID`='".intval($r['PID'])."' AND p.`USERID`=m.`USERID` LIMIT 1";
			$r2 = $conn->execute($q2);
			$owner = $r2->fields;

			issue_refund($r['USERID'],$r['OID'],$r['price']);
			
			$query="UPDATE `orders` SET `status`='3', `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($r['OID'])."' AND `PID`='".sql_quote($r['PID'])."' LIMIT 1";
			$results=$conn->execute($query);

			update_score($r['USERID'], $r['OID'], 'initial_task_provide_detail_fail');

			// Task Owner Notification
			$emailClass->send($owner['email'], $lang['647'], sprintf($lang['648'],fname($owner['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
			// Buyer Notification
			if ($r['buyer_verified']) $buyer_text = $lang['649'];
			else $buyer_text = $lang['672'];
			$emailClass->send($r['email'], $lang['647'], sprintf($buyer_text,fname($r['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
		}

		$cache->set('LateInitialTasks', $LateInitialTasks, 30*24*3600);
	}

	function OngoingTasks_closeToLate() {
		global $conn, $config, $emailClass, $lang, $cache;
		//d($_SERVER["REQUEST_TIME"]);
		$query="SELECT o.`OID`, m.`email`, m.`fullname` FROM `orders` o, `posts` p, `members` m WHERE (o.`status`='1' OR o.`status`='6') AND o.`late`='0' AND o.`stime` > (".$_SERVER['REQUEST_TIME'] ." - (p.`days` * 86400)) AND o.`stime` < (".$_SERVER['REQUEST_TIME'] ." - (p.`days` * 86400) + ".($config['working_process']['task_late_days_alert'] * 86400).") AND o.`PID`=p.`PID` AND o.`pending_request` is NULL AND p.`days`>'1' AND p.`USERID`=m.`USERID`";
		//d($query);
		$res = $conn->execute($query);
		$rows = $res->getrows();
		$OngoingCloseToLate = $cache->get('OngoingCloseToLate');
		//d($rows);
		foreach ($rows as $r) {
			if ($OngoingCloseToLate[$r['OID']])
				continue;
			else {
				$OngoingCloseToLate[$r['OID']] = 1;
			}
			$conn->execute("UPDATE `orders` SET `late`='1' WHERE `OID`='".$r['OID']."' LIMIT 1");
			$emailClass->send($r['email'], $lang['650'], sprintf($lang['651'],fname($r['fullname']),$config['working_process']['task_late_days_alert'],$config['baseurl'].'/track?id='.$r['OID']));
		}
		$cache->set('OngoingCloseToLate', $OngoingCloseToLate, 30*24*3600);
	}

	function OngoingTasks_Late() {
		global $conn, $config, $emailClass, $lang, $cache;
		//d($_SERVER["REQUEST_TIME"]);
		$query="SELECT o.`OID`, o.`PID`, o.`USERID`, o.`price`, m.`email` as buyer_email, m.`fullname` FROM `orders` o, `posts` p, `members` m WHERE ( (o.`status`='1' AND o.`pending_request` is NULL AND o.`stime` < (".$_SERVER['REQUEST_TIME'] ." - (p.`days` * 86400) - ".$config['working_process']['task_late_forget_timestamp']." - ".($config['working_process']['task_late_grace_period_days'] * 86400).")) OR (o.`status`='6' AND o.`stime` < (".$_SERVER['REQUEST_TIME'] ." - (p.`days` * 86400) - ".$config['working_process']['task_late_forget_timestamp']." - ".($config['working_process']['task_late_grace_period_days'] * 86400)." - CEIL(p.`days`/100*".$config['working_process']['revision_period_procent'].")*86400)) ) AND o.`PID`=p.`PID` AND o.`USERID`=m.`USERID`";
		$res = $conn->execute($query);
		$rows = $res->getrows();
		$TasksLate = $cache->get('TasksLate');
		//d($rows);
		foreach ($rows as $r) {
			if ($TasksLate[$r['OID']])
				continue;
			else {
				$TasksLate[$r['OID']] = 1;
			}
			$q2 = "SELECT m.`USERID`, m.`email`, m.`fullname` FROM `members` m, `posts` p WHERE p.`PID`='".intval($r['PID'])."' AND p.`USERID`=m.`USERID` LIMIT 1";
			$r2 = $conn->execute($q2);
			$owner = $r2->fields;
			//d($owner,1);
			issue_refund($r['USERID'],$r['OID'],$r['price'], $override_verification_test=true);
			
			$query="UPDATE `orders` SET `status`='3', `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($r['OID'])."' AND `PID`='".sql_quote($r['PID'])."' LIMIT 1";
			$results=$conn->execute($query);

			update_score($owner['USERID'], $r['OID'], 'task_late_fail');

			// Task Owner Notification
			$emailClass->send($owner['email'], $lang['647'], sprintf($lang['652'],fname($owner['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
			// Buyer Notification
			$emailClass->send($r['email'], $lang['647'], sprintf($lang['653'],fname($r['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
		}
		$cache->set('TasksLate', $TasksLate, 30*24*3600);
	}

	function cancelMutuallyRequestedTasks() {
		global $conn, $config, $emailClass, $lang;
		//d($_SERVER["REQUEST_TIME"]);
		$query="SELECT o.`OID`, o.`PID`, o.`USERID`, o.`price`, m.`email` as buyer_email, m.`fullname` FROM `orders` o, `posts` p, `members` m WHERE o.`status`='1' AND o.`pending_request` = 'mutual_cancellation_request' AND o.`PR_start_time` < (".$_SERVER['REQUEST_TIME'] ." - (".$config['working_process']['mutual_cancelation_period_days']." * 86400)) AND o.`PID`=p.`PID` AND o.`USERID`=m.`USERID`";
		//d($query);
		$res = $conn->execute($query);
		$rows = $res->getrows();

		foreach ($rows as $r) {
			$q2 = "SELECT m.`USERID`, m.`email`, m.`fullname` FROM `members` m, `posts` p WHERE p.`PID`='".intval($r['PID'])."' AND p.`USERID`=m.`USERID` LIMIT 1";
			$r2 = $conn->execute($q2);
			$owner = $r2->fields;
			//d($owner,1);
			issue_refund($r['USERID'],$r['OID'],$r['price'], $override_verification_test=true);
			
			$query="UPDATE `orders` SET `status`='2', `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($r['OID'])."' AND `PID`='".sql_quote($r['PID'])."' LIMIT 1";
			$results=$conn->execute($query);

			$conn->execute('UPDATE `inbox2` SET `cancel`="2" WHERE `OID`="'.sql_quote($r['OID']).'" AND `action`="mutual_cancellation_request" AND `cancel`="0" LIMIT 1');

			// Task Owner Notification
			$emailClass->send($owner['email'], $lang['647'], sprintf($lang['654'],fname($owner['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
			// Buyer Notification
			$emailClass->send($r['email'], $lang['647'], sprintf($lang['654'],fname($r['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
		}
	}

	function autoCompleteTasks() {
		global $conn, $config, $emailClass, $lang;
		//d($_SERVER["REQUEST_TIME"]);
		$query="SELECT o.`OID`, o.`PID`, o.`USERID`, o.`price`, m.`email` as buyer_email, m.`fullname` FROM `orders` o, `posts` p, `members` m WHERE o.`status`='4' AND o.`pending_request` = 'requires_buyers_review' AND o.`PR_start_time` < (".$_SERVER['REQUEST_TIME'] ." - (".$config['working_process']['review_period']." * 86400)) AND o.`PID`=p.`PID` AND o.`USERID`=m.`USERID`";
		//d($query);
		$res = $conn->execute($query);
		$rows = $res->getrows();
//		d($rows,1);

		foreach ($rows as $r) {
			$q2 = "SELECT m.`USERID`, m.`email`, m.`fullname` FROM `members` m, `posts` p WHERE p.`PID`='".intval($r['PID'])."' AND p.`USERID`=m.`USERID` LIMIT 1";
			$r2 = $conn->execute($q2);
			$owner = $r2->fields;
			
			$query="UPDATE `orders` SET `status`='5', `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($r['OID'])."' AND `PID`='".sql_quote($r['PID'])."' LIMIT 1";
			$results=$conn->execute($query);

			update_score($owner['USERID'], $r['OID'], 'success_task_owner');
			update_score($r['USERID'], $r['OID'], 'success_task_buyer');
			//$conn->execute('UPDATE `inbox2` SET `cancel`="2" WHERE `OID`="'.sql_quote($r['OID']).'" AND `action`="mutual_cancellation_request" AND `cancel`="0" LIMIT 1');

			// Task Owner Notification
			$emailClass->send($owner['email'], $lang['655'], sprintf($lang['656'],fname($owner['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
			// Buyer Notification
			$emailClass->send($r['email'], $lang['655'], sprintf($lang['657'],fname($r['fullname']),$config['baseurl'].'/track?id='.$r['OID']));
		}
	}

	function refundDelayedIfVerified() {
		global $conn, $config, $emailClass, $econnectClass, $lang;
		//d($_SERVER["REQUEST_TIME"]);
		$query = "SELECT * FROM `payments` WHERE `delayed_cancel`='1' AND `cancel`='0'";
		$res = $conn->execute($query);
		$rows = $res->getrows();

		if (sizeof($rows) && !$econnectClass->INN_access_token) {
			$econnectClass->authorizeINN();
		}
		foreach ($rows as $r) {
			if ($r['payment_gateway'] == 'emoney' && $econnectClass->isUserVerified($r['transactioncode'], $econnectClass->INN_access_token)) {
				$r2 = $conn->execute('SELECT `fullname`, `email`, `pin`, `pinprotect` FROM `members` WHERE `USERID`="'.intval($r['USERID']).'" LIMIT 1');

				if ($r2->fields['pin'] != $PIN) {
					$emailClass->send($config['adminemail'], $lang['670'], sprintf($lang['671'], $r['transactioncode'],$PIN,$r2->fields['pin']));
					continue;
				}
				else {
					$r3 = $conn->execute('UPDATE `members` SET `pinprotect`="1" WHERE `USERID`="'.intval($r['USERID']).'" LIMIT 1');
					$r4 = $conn->execute('UPDATE `payments` SET `cancel`="1" WHERE `ID` = "'.intval($r['ID']).'" AND `USERID`="'.intval($r['USERID']).'" LIMIT 1');
					$response = $econnectClass->cancelPendingPayment($r['transactioncode'], $econnectClass->INN_access_token);
					// Buyer Notification
					if (!$r2->fields['pinprotect'])
						$emailClass->send($r['email'], $lang['673'], sprintf($lang['674'],fname($r2->fields['fullname'])));
				}
			}
		}
	}

	function sendWDemails() {
		global $conn, $cache, $config, $emailClass, $lang;

		$last_wd_sent = $cache->get('WD_EMAILS_countdown');
		// Send mails once per 3 hours
		/**
		Debug here
		**/
		if ($last_wd_sent && $_SERVER['REQUEST_TIME'] - $last_wd_sent < $config['delayed_emails']['wd_mail_period'])
			return false;
		// Check if we have something to send and update time counter
		$wd_emails = $cache->get('WD_EMAILS');
		$cache->set('WD_EMAILS_countdown', $_SERVER['REQUEST_TIME'], 172800);

		if (!is_array($wd_emails) || sizeof($wd_emails)==0) {
			return false;
		}

		// Construct Query to get all data in once
		$queryUID = '';
		foreach ($wd_emails as $USERID => $sum) {
			$queryUID .= intval($USERID).',';
		}

		// Get users info
		$usersResult = $conn->execute('SELECT `USERID`, `fullname`, `email` FROM `members` WHERE `USERID` IN ('.rtrim($queryUID,',').')');
		$users = $usersResult->getrows($usersResult);

		foreach ($users as $user) {
			if ($wd_emails[$user['USERID']] && $wd_emails[$user['USERID']] > 0) {
				/*
				d($user['email']);
				d($lang[678]);
				d(sprintf($lang['679'],fname($user['fullname']), $wd_emails[$user['USERID']]));
				*/
				$emailClass->send($user['email'], $lang['678'], sprintf($lang['679'],fname($user['fullname']), $wd_emails[$user['USERID']]));
			}
			unset($wd_emails[$user['USERID']]);
		}
		$cache->set('WD_EMAILS', $wd_emails, 172800);
	}

	function countDealsPerCat() {
		global $conn, $config, $cache;

		$query = 'SELECT `category` as CATID, COUNT(*) as itemsCC FROM `posts` WHERE `active`="1" GROUP BY `category` ORDER BY itemsCC DESC';
		$itemsResult = $conn->execute($query);
		$items = $itemsResult->getrows();

		$catsCC = array();
		foreach ($items as $i) {
			$catsCC[$i['CATID']] = $i['itemsCC'];
		}
		$cache->set('catsCC', $catsCC, 86400);
	}

	function removeGarbage() {
		global $config;
		$files = scandir($config['basedir'].'/temporary/tmpfiles/');
		foreach ($files as $f) {
			if ($f != '.' && $f != '..') {
				$fname = $config['basedir'].'/temporary/tmpfiles/'.$f;
				if ($_SERVER['REQUEST_TIME'] - filectime($fname) > 1800) {
					@unlink($fname);
				}
			}
		}
	}

	function sendDelayedMails() {
		global $config, $conn, $lang;

		$HH = strftime('%H');
		if ($HH >= 23 || $HH <= 9)
			return false;
		$query = 'SELECT i.`MID`, i.`MSGTO`, i.`time`, i.`MSGFROM`, i.`message`, mf.`username` as `sendername`, mt.`username` as `recipientname`, mt.`email`, mt.`mobile` FROM `inbox` i, `members` mf, `members` mt WHERE i.`sent`="0" AND i.`delayed`="1" AND i.`unread`="1" AND i.`time`<'.intval($_SERVER['REQUEST_TIME'] - 43200).' AND mt.`USERID`=i.`MSGTO` AND mf.`USERID`=i.`MSGFROM` AND mt.`verified`="1" AND mf.`verified`="1"';
		$result = $conn->execute($query);
		$msgs = $result->getrows();
		$updString = '';
		foreach ($msgs as $m) {
			$message = $lang['790'];
			$sendername = stripslashes($m['sendername']);
			$fromname = $config['site_name'];
			$from = $config['site_email'];
			$subject = $lang['437'];
			$sendmailbody = $lang['788'].' '.stripslashes($m['recipientname']).',<br /><br />'.$lang['438']." <a href=\"".$config['baseurl']."/conversations/".$sendername."?mid=".$m['MID']."\">".$sendername."</a>:<br />";
			//$sendmailbody .= $lang['438']."<br /><br />".stripslashes(mb_substr($message_body,0,10,'UTF-8')).(mb_strlen($message_body,'UTF-8')>10 ? '...' : '')."<br /><br />";
			$sendmailbody .= "<a href=\"".$config['baseurl']."/conversations/".$sendername."?mid=".$m['MID']."\">".$config['baseurl']."/conversations/".$sendername."</a><br /><br />";
			$sendmailbody .= $lang['704']."<br>".stripslashes($fromname);
			mailme($m['email'],$fromname,$from,$subject,$sendmailbody,$bcc='', true, false);

			$notifications = new notificationsClass();
			$notifications->msgto = $m['MSGTO'];
			$notifications->msgfrom = $m['MSGFROM'];
			$notifications->action = 'new_pm';
			$notifications->link = 'conversations/'.$sendername.'?mid='.$m['MID'];
			$notifications->time = $m['time'];
			$notifications->send_instant_notification();
			if ($updString) $updString .= ',';
			$updString .= $m['MID'];
		}

		if ($updString) {
			$query = 'UPDATE `inbox` SET `sent`="1" WHERE `MID` IN ('.sql_quote($updString).')';
			$conn->execute($query);
		}
	}

	function activateDeals() {
		global $conn;

		$query = 'SELECT `USERID` FROM `posts` WHERE `active`="6"';
		$result = $conn->execute($query);
		$users = $result->getrows();
		foreach ($users as $user) {
			getUserDealsOnline($user['USERID']);
		}
	}
}

function fname($fullname) {
	if (strpos($fullname, ' ')===FALSE) return $fullname;
	else {
		$name = explode(' ', $fullname);
		return $name[0];
	}
}
?>