<?php
include("include/config.php");
include("include/functions/import.php");
//d($_SESSION);
//d('SESSION  END');
//d($_POST);
if ($_SESSION['USERID'] && ($blocked_till = user_blocked_till($_SESSION['USERID']))) {
	$error = sprintf($lang['662'], $blocked_till);
	$templateselect = "empty.tpl";
	abr('pathURI','new');
	abr('pathTitle',$lang['55']);
	abr('heading',$lang['732']);
	abr('subheading',$lang['733']);
	unset($_SESSION['temp']['message_type']); unset($_SESSION['temp']['message_title']);
}

elseif ((!isset($_SESSION['USERID']) || !$_SESSION['USERID']) && isset($_REQUEST['subform']) && $_REQUEST['subform']) {
	unset($_SESSION['store']);
	$_SESSION['store'] = array(
		'title' 			=> $_REQUEST['title'],
		'cat'				=> $_REQUEST['cat'],
		'desc'				=> $_REQUEST['desc'],
		'instr' 			=> $_REQUEST['instr'],
		'tags'				=> $_REQUEST['tags'],
		'duration'			=> $_REQUEST['duration'],
		'multipleme'		=> $_REQUEST['multipleme'],
		'price'				=> $_REQUEST['price'],
//		'photobase64-i1'	=> $_REQUEST['photobase64-i1'],
//		'photobase64-i2'	=> $_REQUEST['photobase64-i2'],
//		'photobase64-i3'	=> $_REQUEST['photobase64-i3'],
		'video'				=> $_REQUEST['video'],
		'videohost'			=> $_REQUEST['videohost'],
		'videofile'			=> $_REQUEST['videofile'],
		'moneyback'			=> $_REQUEST['moneyback'],
		'subform'			=> $_REQUEST['subform']
		);
	fileSessionStore(1);fileSessionStore(2);fileSessionStore(3);
	$_SESSION['temp']['message_title'] = $lang['724'];
	$_SESSION['temp']['message_type'] = 'error';
	header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('new')); exit;
}
elseif ($_SESSION['USERID'] && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	$query = "SELECT `USERID`, `pin`, `pinprotect`, `eemailprotect` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `status`='1' LIMIT 1";
	$userArr=$conn->execute($query);
	if (!$userArr->fields['USERID'] || $userArr->fields['status'] == '0') {
		$no_redir = true;
		include("logout.php");
		$_SESSION['temp']['message_type'] = 'error';
		$_SESSION['temp']['message_title'] = $lang[745];
		header("Location:$config[baseurl]/signin"); exit;
		exit;
	}
	elseif (!($config['createdeal_settings']['allow_creating_without_pin'] || strlen($userArr->fields['pin']) == 11)) {
		$_SESSION['temp']['message_type'] = 'error';
		$_SESSION['temp']['message_title'] = $lang[558];
		header("Location:$config[baseurl]/settings?check=1&r=".base64_encode('new')); exit;
	}
	elseif (!($config['createdeal_settings']['allow_creating_with_unverified_pin'] || $userArr->fields['pinprotect'] )) {
		$_SESSION['temp']['message_type'] = 'error';
		$_SESSION['temp']['message_title'] = $lang[744];
		header("Location:$config[baseurl]/settings?check=1&r=".base64_encode('new')); exit;
	}
	if (isset($_SESSION['store']) && $_SESSION['store']['subform']) {
		foreach ($_SESSION['store'] as $skey => $sval)
			$_REQUEST[$skey] = $sval;
		unset($_SESSION['store']);
		for ($i=1;$i<=3;$i++) {
			if (file_exists($config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID']))
				$_REQUEST['photobase64-i'.$i] = file_get_contents($config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID']);
				@unlink($config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID']);
		}
	}

	$allowposting = "1";
	$vonly = $config['vonly'];
	if($vonly == "1")
	{
		$uverified = $_SESSION['VERIFIED'];
		if($uverified != "1")
		{
			$allowposting = "0";
		}
	}

	if($config['enable_levels'] == "1" && $config['price_mode'] == "3")
	{
		$timecheck = $_SERVER['REQUEST_TIME'] - 86400;
		$query = "SELECT count(*) as total FROM `posts` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `time_added`>='".sql_quote($timecheck)."' LIMIT 1";
		$executequery=$conn->execute($query);
		$lastjobs = $executequery->fields['total']+0;
		
		$query = "SELECT `email`, `level` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
		$executequery=$conn->execute($query);
		$mlevel = intval($executequery->fields['level']);
		$email = $executequery->fields['email'];
		if($mlevel == "3")
		{
			$jlimit = $config['level3job'];
		}
		elseif($mlevel == "2")
		{
			$jlimit = $config['level2job'];
		}
		elseif($mlevel == "1")
		{
			$jlimit = $config['level1job'];
		}
		else
		{
			$jlimit = "";
		}
		if($jlimit != "")
		{
			if($lastjobs >= $jlimit)
			{
				// $allowposting = "2";
				// Temporary allow unlimited postings
			}
		}
	}
	
	if($allowposting == "1")
	{
		if($_REQUEST['subform'] == "1")
		{
			$gtitle = cleanit($_REQUEST['title']);	
			$gcat = intval(cleanit($_REQUEST['cat']));
			$gdesc = cleanit($_REQUEST['desc']);	
			$ginst = cleanit($_REQUEST['instr']);	
			$gtags = cleanit($_REQUEST['tags']);	
			$gdays = intval(cleanit($_REQUEST['duration']));
			$gprice = intval(cleanit($_REQUEST['price']));
			$gvideo = trim(cleanit($_REQUEST['video']));
			$gvideohost = cleanit($_REQUEST['videohost']);
			$gvideofile = trim(cleanit($_REQUEST['videofile']));
			$gmoneyback = intval(cleanit($_REQUEST['moneyback']));
			

			//$gprice = ($_REQUEST['price'] == 5 || $_REQUEST['price'] == 10 || $_REQUEST['price'] == 15 || $_REQUEST['price'] == 25 || $_REQUEST['price'] == 50) ? intval($_REQUEST['price']) : 5;
			//$gyoutube = cleanit($_REQUEST['gyoutube']);
			//$shipme = intval(cleanit($_REQUEST['shipme']));
			$multipleme = intval(cleanit($_REQUEST['multipleme']));
			//$extrasme = intval(cleanit($_REQUEST['extrasme']));
			//$instanturl = cleanit($_REQUEST['instanturl']);
			if($gtitle == "" || strlen($gtitle) < 10)
			{
				$error = "<li>".$lang['92']."</li>";
			}
			elseif(strlen($gtitle) > 200)
			{
				$error = "<li>".$lang['509']."</li>";
			}
			if($shipme == "1")
			{
				include("new_ship.php");
			}
			
			if($multipleme == "1")
			{
				$result_add_multiple = ", `add_multiple`='999'";	
			}
			else
				$result_add_multiple = ", `add_multiple`='0'";	

			if($extrasme == "1")
			{
				$result_process_extras = "1";
			}

			if(TRUE)
			{
				if (insert_get_member_vas_ownprice(0)) {
					$price = intval(cleanit($_REQUEST['price']));
					if ($price < 5 && $price != 0) {
						$error .= '<li>'.$lang[841].': '.$lang[838].'</li>';
					}
					elseif ($price > 995) {
						$error .= '<li>'.$lang[841].': '.$lang[839].'</li>';
					}
					elseif ($price != $_REQUEST['price']) {
						$error .= '<li>'.$lang[841].': '.$lang[840].'</li>';
					}
					
					$config['price_mode'] = 1;
				}
				if($config['price_mode'] == 1)
				{
					if($price == "0")
					{
						$error .= "<li>".$lang['127']."</li>";
					}
					$comper = intval($config['commission_percent']);
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2);
				}
				elseif($config['price_mode'] == 3)
				{
					$PACID = intval(cleanit($_REQUEST['price']));
					$query = "SELECT `ID`, `pprice`,`pcom`, `l1`, `l2`, `l3`  FROM `packs` WHERE `ID`='".sql_quote($PACID)."' LIMIT 1";
					$executequery=$conn->execute($query);

					$price = intval(cleanit($executequery->fields['pprice']));
					$comper = intval(cleanit($executequery->fields['pcom']));
					if(!$executequery->fields['ID'] || !$executequery->fields['l'.$mlevel])
					{
						$error = "<li>".$lang['435']."</li>";
					}
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2);
					
				}
				elseif($config['price_mode'] == "2")
				{
					$price = intval($config['price']);
					$comper = intval($config['commission_percent']);
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2);
				}
				else
				{
					$price = intval($config['price']);
					$mysetc = $config['commission'];
					$ctp = number_format($mysetc, 2);
				}
			}
			if($gcat == "0")
			{
				$error .= "<li>".$lang['93']."</li>";
			}
			if($gdesc == "" || mb_strlen($gdesc,'UTF8') < 40)
			{
				$error .= "<li>".$lang['94']."</li>";
			}
			elseif(mb_strlen($gdesc,'UTF8') > 450)
			{
				$error = "<li>".$lang['510']."</li>";
			}
			if($ginst == "" || mb_strlen($ginst,'UTF8') < 10)
			{
				$error .= "<li>".$lang['95']."</li>";
			}
			if($gtags == "" || mb_strlen($gtags,'UTF8') < 4)
			{
				$error .= "<li>".$lang['96']."</li>";
			}
			if($gdays < 0 || $gdays > 99)
			{
				$error .= "<li>".$lang['97']."</li>";	
			}

			$photo_base64 = false;
			if (isset($_FILES['photo-i1']['tmp_name']) && $_FILES['photo-i1']['tmp_name'])
				$gphoto = $_FILES['photo-i1']['tmp_name'];
			elseif (isset($_REQUEST['photobase64-i1']) && $_REQUEST['photobase64-i1'])
			{
				$img = $_REQUEST['photobase64-i1'];
				if (strpos($img, 'data:image/png;base64,')!==FALSE)
					$ext2 = 'png';
				elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
					$ext2 = 'jpg';
				elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
					$ext2 = 'jpeg';
				else $ext2 = '';

				if ($ext2 && substr_count($img, 'data:')==1) {
					$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
					if ($ext2 == 'jpeg') $ext2 = 'jpg';
					$img = str_replace(' ', '+', $img);
					$data = base64_decode($img);
//					header('Content-type: image/'.$ext2);					echo $data;					exit;
					$_FILES['photo-i1']['name'] = md5('1'.uniqid().'x') . '.' . $ext2;
					$gphoto = $_FILES['photo-i1']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i1']['name'];
					@file_put_contents($gphoto, $data);
					$photo_base64 = true;
				}
			}

			if($gphoto != "")
			{
				$ext = substr(strrchr($_FILES['photo-i1']['name'], '.'), 1);
				$ext2 = strtolower($ext);
				if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
				{
					$theimageinfo = getimagesize($gphoto);
					if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
					{
						$error .= "<li>".$lang['100']."</li>";
					}
				}
				else
				{
					$error .= "<li>".$lang['100']."</li>";
				}
			}
			else
			{
				$error .= "<li>".$lang['101']."</li>";
			}
			/*
			if($gyoutube != "")
			{
				$gyoutube = str_replace("https://", "http://", $gyoutube);
				$pos = strpos($gyoutube, "http://www.youtube.com/watch?v=");
				$posb = strpos($gyoutube, "http://www.youtu.be/");
				$posc = strpos($gyoutube, "http://youtu.be/");
				if ($pos === false)
				{
					if ($posb === false)
					{
						if ($posc === false)
						{
							$error .= "<li>".$lang['133']."</li>";
						}
					}
				}
			}
			*/
			if (!$error && isset($_SESSION['temp']['new']) && $_SESSION['temp']['new'] == md5($gtitle.$gdesc)) {
				$error = "<li>".$lang['625']."</li>";
				$gtitle = $gdesc = $ginst = $gcat = $gtags = $gdays = $gprice = $gvideo = $gvideohost = $gvideofile = $gmoneyback = '';
			}
			elseif (!$error) {
				$_SESSION['temp']['new'] = md5($gtitle.$gdesc);
			}

			if ($mlevel < 2) $gmoneyback = 1;
			elseif ($gmoneyback) $gmoneyback = '1';
			else $gmoneyback = '0';

			if($error == "")
			{			
				$approve_stories = $config['approve_stories'];
				if($approve_stories == "1")
				{
					$active = "0";
				}
				else
				{
					$active = "1";
				}
				$query="INSERT INTO `posts` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `gtitle`='".sql_quote($gtitle)."', `gtags`='".sql_quote($gtags)."', `gdesc`='".sql_quote($gdesc)."', `ginst`='".sql_quote($ginst)."', `days`='".sql_quote($gdays)."', `youtube`='".sql_quote($gyoutube)."', `category`='".sql_quote($gcat)."', `price`='".sql_quote($price)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `date_added`='".date("Y-m-d")."', `pip`='".$_SERVER['REMOTE_ADDR']."', `moneyback`='{$gmoneyback}', `active`='{$active}', `ctp`='".sql_quote($ctp)."' $result_add_multiple $addship $result_add_instant";
				$result=$conn->execute($query);
				$pid = $conn->_insertid();

				if($result_process_extras == "1")
				{
					include("new_extras.php");
				}
				if($gphoto != "")
				{
					$thepp = $pid."-1";
					if($theimageinfo[2] == 1)
					{
						$error .= "<li>".$lang['100']."</li>";
//						$thepp .= ".gif";
					}
					elseif($theimageinfo[2] == 2)
					{
						$thepp .= ".jpg";
					}
					elseif($theimageinfo[2] == 3)
					{
						$thepp .= ".png";
					}
					if($error == "")
					{
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto, $myvideoimgnew);
						else
							move_uploaded_file($gphoto, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET `p1`='$thepp' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}

				$photo_base64 = false;
				if (isset($_FILES['photo-i2']['tmp_name']) && $_FILES['photo-i2']['tmp_name'] && $ghoto != $_FILES['photo-i2']['tmp_name'])
					$gphoto2 = $_FILES['photo-i2']['tmp_name'];
				elseif (isset($_REQUEST['photobase64-i2']) && $_REQUEST['photobase64-i2'] && $_REQUEST['photobase64-i2'] != $_REQUEST['photobase64-i1'])
				{
					$img = $_REQUEST['photobase64-i2'];
					if (strpos($img, 'data:image/png;base64,')!==FALSE)
						$ext2 = 'png';
					elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
						$ext2 = 'jpg';
					elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
						$ext2 = 'jpeg';
					else $ext2 = '';

					if ($ext2 && substr_count($img, 'data:')==1) {
						$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
						if ($ext2 == 'jpeg') $ext2 = 'jpg';
						$img = str_replace(' ', '+', $img);
						$data = base64_decode($img);
	//					header('Content-type: image/'.$ext2);					echo $data;					exit;
						$_FILES['photo-i2']['name'] = md5('2'.uniqid().'x') . '.' . $ext2;
						$gphoto2 = $_FILES['photo-i2']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i2']['name'];
						@file_put_contents($gphoto2, $data);
						$photo_base64 = true;
					}
				}
				if($gphoto2 != "")
				{
					$ext = substr(strrchr($_FILES['photo-i2']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto2);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid."-2";
						if($theimageinfo[2] == 1)
						{
							$error .= "<li>".$lang['100']."</li>";
//							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
	
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto2, $myvideoimgnew);
						else
							move_uploaded_file($gphoto2, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET `p2`='$thepp' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}

				$photo_base64 = false;
				if (isset($_FILES['photo-i3']['tmp_name']) && $_FILES['photo-i3']['tmp_name'] && $gphoto != $_FILES['photo-i3']['tmp_name'] && $gphoto2 != $_FILES['photo-i3']['tmp_name'])
					$gphoto3 = $_FILES['photo-i3']['tmp_name'];
				elseif (isset($_REQUEST['photobase64-i3']) && $_REQUEST['photobase64-i3'] && $_REQUEST['photobase64-i3'] != $_REQUEST['photobase64-i1'] && $_REQUEST['photobase64-i3'] != $_REQUEST['photobase64-i2'])
				{
					$img = $_REQUEST['photobase64-i3'];
					if (strpos($img, 'data:image/png;base64,')!==FALSE)
						$ext2 = 'png';
					elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
						$ext2 = 'jpg';
					elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
						$ext2 = 'jpeg';
					else $ext2 = '';

					if ($ext2 && substr_count($img, 'data:')==1) {
						$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
						if ($ext2 == 'jpeg') $ext2 = 'jpg';
						$img = str_replace(' ', '+', $img);
						$data = base64_decode($img);
	//					header('Content-type: image/'.$ext2);					echo $data;					exit;
						$_FILES['photo-i3']['name'] = md5('3'.uniqid().'x') . '.' . $ext2;
						$gphoto3 = $_FILES['photo-i3']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i3']['name'];
						@file_put_contents($gphoto3, $data);
						$photo_base64 = true;
					}
				}
				if($gphoto3 != "")
				{
					$ext = substr(strrchr($_FILES['photo-i3']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto3);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid."-3";
						if($theimageinfo[2] == 1)
						{
							$error .= "<li>".$lang['100']."</li>";
//							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
	
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto3, $myvideoimgnew);
						else
							move_uploaded_file($gphoto3, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET `p3`='$thepp' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}

				if ($gvideo && $gvideohost && $gvideofile) {
					if (in_array($gvideohost, array('youtube', 'myvideo')) && strlen($gvideofile) < 12) {
						$query = "UPDATE `posts` SET `videohost`='{$gvideohost}', `videofile`='{$gvideofile}' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
						$conn->execute($query);
					}
					else {
						$gvideo = $gvideohost = $gvideohost = $_REQUEST['video'] = $_REQUEST['videohost'] = $_REQUEST['videofile'] = '';
					}
				}
				if($approve_stories == "1")
				{
					if (!$userArr->fields['pin'] || !$userArr->fields['eemailprotect']) {
						abr('noPINEMONEY',true);
						$message = $lang['756'];
					}
					else
						$message = $lang['126'];
				}
				else
				{
					$gurl = $config['baseurl']."/".get_cat_seo($gcat)."/".$pid."/".seo_clean_titles($gtitle);
					$feurl = $config['baseurl']."/feature?id=".$pid;
					header("Location:$feurl");exit;
				}
			}
			else
			{
				abr('gtitle',$gtitle);
				abr('gcat',$gcat);
				abr('gdesc',$gdesc);
				abr('ginst',$ginst);
				abr('gtags',$gtags);
				abr('gdays',$gdays);
				abr('gprice',$gprice);
				abr('gvideo',$gvideo);
				abr('gvideohost',$gvideohost);
				abr('gvideofile',$gvideofile);
				abr('gmoneyback',$gmoneyback);
			}
		}
		else
		{
			$query = "SELECT count(*) as total FROM `posts` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
			$executequery=$conn->execute($query);
			abr('numofposts', $executequery->fields['total']);
			$gtitle = cleanit($_REQUEST['quicktitle']);
			abr('gtitle',$gtitle);
		}
		$templateselect = "new.tpl";
	}
	elseif($allowposting == "2")
	{
		$error = $lang['498'];
		abr('heading', $lang['780']);
		abr('subheading', $lang['498']);
		$templateselect = "empty.tpl";
	}
	else
	{
		$error = sprintf($lang['445'], $config['baseurl'].'/resendconfirmation.php');
		$templateselect = "empty.tpl";
	}
	$pagetitle = $lang['61'];
	abr('pagetitle',$pagetitle);
}
else
{
	abr('numofposts', '0');
	$pagetitle = $lang['61'];
	$templateselect = "new.tpl";
	abr('pagetitle',$pagetitle);
	//header("Location:$config[baseurl]/login");exit;
}

//TEMPLATES BEGIN
abr('email',$email);
abr('newDeal',1);
abr('moderation_queue_period', $config['moderation_queue_period']);
abr('error',$error);
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END

function fileSessionStore($i=1) {
	global $config;
	if (isset($_FILES['photo-i'.$i]['tmp_name']) && $_FILES['photo-i'.$i]['tmp_name'] && !$_FILES['photo-i'.$i]['error']) {
		$theimageinfo = getimagesize($_FILES['photo-i'.$i]['tmp_name']);
		$ext = '';
		if($theimageinfo[2] == 1)
			$ext = 'gif';
		elseif($theimageinfo[2] == 2)
			$ext = 'jpg';
		elseif($theimageinfo[2] == 3)
			$ext = 'png';

		if ($ext && !file_exists(!$config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID'])) {
			//d($config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID']);
			file_put_contents($config['basedir'].'/temporary/tmpfiles/i'.$i.'.'.$_COOKIE['PHPSESSID'], 'data:image/'.$ext.';base64,'.base64_encode(file_get_contents($_FILES['photo-i'.$i]['tmp_name'])));
		}
	}
}
?>