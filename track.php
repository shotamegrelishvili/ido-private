<?php
include("include/config.php");
include("include/functions/import.php");
$OID = intval(cleanit($_REQUEST['id']));
if (isset($_SESSION['ADMINID']) && $_SESSION['ADMINID'] && is_numeric($_SESSION['ADMINID'])) $is_admin = true;
else $is_admin = false;

if (($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID'])) OR $is_admin)
{
	if($OID > 0)
	{
		$pagetitle = $lang['260']." #".$OID;
		abr('pagetitle',$pagetitle);
		
		$query="SELECT A.*, B.`gtitle`, B.`p1`, B.`price`, B.`USERID` AS owner, B.`days`, B.`ginst`, C.`username`, C.`profilepicture`, C.`lastlogin`, D.`seo`, E.`USERID` as buyerUID, E.`username` as buyer, E.`profilepicture` as buyerprofilepicture, E.`lastlogin` AS buyerlastlogin FROM `orders` A, `posts` B, `members` C, `categories` D, `members` E WHERE C.`USERID`=B.`USERID` AND B.`category`=D.`CATID` AND E.`USERID`=A.`USERID` AND A.`OID`='".sql_quote($OID)."' AND B.`PID`=A.`PID` LIMIT 1";
		$results=$conn->execute($query);
		$o = $results->getrows();
		$owner = $o[0]['owner'];
		$buyer = $o[0]['USERID'];
		$me = $_SESSION['USERID'];
		$PID = $o[0]['PID'];
		$rprice = $o[0]['price'];
		
		if($owner == $me)
		{
			$v = "owner";	
			abr('sm2',"1");
			$templateselect = "track2.tpl";
			$UID = $buyer;
		}
		elseif($buyer == $me  || $is_admin)
		{
			$v = "buyer";
			$templateselect = "track.tpl";
			$UID = $owner;
		}
		else
		{
			header("Location:$config[baseurl]/");exit;
		}
		if($_POST['subabort'] == "1")
		{
			$AMID = intval(cleanit($_POST['AMID']));
			if($AMID > 0)
			{
				$query="UPDATE `inbox2` SET `cancel`='1', `ctime`='".$_SERVER['REQUEST_TIME']."', `CID`='".sql_quote($_SESSION['USERID'])."' WHERE `MID`='".sql_quote($AMID)."' AND `cancel`='0' AND `MSGFROM`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
				$results=$conn->execute($query);
				$conn->execute('UPDATE `orders` SET `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`="'.sql_quote($OID).'" LIMIT 1');
			}
		}
		if($_POST['subdecline'] == "1")
		{
			$DMID = intval(cleanit($_POST['DMID']));
			if($DMID > 0)
			{
				$query = 'SELECT * FROM `inbox2` WHERE `MID`="'.sql_quote($DMID).'" AND `MSGTO`="'.sql_quote($_SESSION['USERID']).'" AND `cancel`="0" LIMIT 1';
				$msgresult = $conn->execute($query);
				if ($msgresult->fields['action'] == 'predelivery') {
					$query="UPDATE `inbox2` SET `cancel`='1', `ctime`='".$_SERVER['REQUEST_TIME']."', `CID`='".sql_quote($_SESSION['USERID'])."' WHERE `MID`='".sql_quote($DMID)."' AND `cancel`='0' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$results=$conn->execute($query);
					$query="UPDATE `orders` SET `status`='1', `substatus`=NULL, `no_moneyback`='0', `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
					$results=$conn->execute($query);
					send_instant_notification($owner, 'predelivery_reject', 'track?id='.$OID.'&mid='.$DMID, $_SESSION['USERID'], true);
				}
				elseif ($msgresult->fields['action'] == 'mutual_cancellation_request') {
					$query="UPDATE `inbox2` SET `cancel`='1', `ctime`='".$_SERVER['REQUEST_TIME']."', `CID`='".sql_quote($_SESSION['USERID'])."' WHERE `MID`='".sql_quote($DMID)."' AND `cancel`='0' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$results=$conn->execute($query);
					$conn->execute('UPDATE `orders` SET `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`="'.sql_quote($OID).'" LIMIT 1');
					$o[0]['pending_request'] = $o[0]['PR_start_time'] = '';
					send_instant_notification($owner, 'mutual_reject', 'track?id='.$OID.'&mid='.$DMID, $_SESSION['USERID'], true);
				}
				elseif ($o[0]['status'] == 4 && !$o[0]['substatus'] AND $o[0]['owner'] == $_SESSION['USERID']) {
					$query="UPDATE `orders` SET `status`='1', `substatus`=NULL WHERE `OID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$results=$conn->execute($query);
				}
			}
		}
		if($_POST['subaccept'] == "1")
		{
			$AMID = intval(cleanit($_POST['AMID']));
			if($AMID > 0) {
				$query = 'SELECT * FROM `inbox2` WHERE `MID`="'.sql_quote($AMID).'" AND `MSGTO`="'.sql_quote($_SESSION['USERID']).'" AND `cancel`="0" LIMIT 1';
				$msgresult = $conn->execute($query);
				if ($msgresult->fields['action'] == 'predelivery') {
					$query="UPDATE `inbox2` SET `cancel`='2', `ctime`='".$_SERVER['REQUEST_TIME']."', `CID`='".sql_quote($_SESSION['USERID'])."' WHERE `MID`='".sql_quote($AMID)."' AND `cancel`='0' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$results=$conn->execute($query);
					if ($o[0]['substatus'] == 2)
						$query="UPDATE `orders` SET `no_moneyback`='1' WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
					else
						$query="UPDATE `orders` SET `status`='1', `substatus`=NULL, `no_moneyback`='1', `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
					send_instant_notification($owner, 'predelivery_accept', 'track?id='.$OID.'&mid='.$AMID, $_SESSION['USERID'], true);
					$results=$conn->execute($query);
				}
				elseif ($msgresult->fields['action'] == 'mutual_cancellation_request') {
					$query="UPDATE `inbox2` SET `cancel`='2', `ctime`='".$_SERVER['REQUEST_TIME']."', `CID`='".sql_quote($_SESSION['USERID'])."' WHERE `MID`='".sql_quote($AMID)."' AND `cancel`='0' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$results=$conn->execute($query);
					issue_refund($buyer,$OID,$rprice,true);
					$query="UPDATE `orders` SET `status`='2', `cltime`='".$_SERVER['REQUEST_TIME']."', `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
					$results=$conn->execute($query);
					cancel_revenue($OID);
					$query="SELECT A.*, B.`gtitle`, B.`p1`, B.`price`, B.`USERID` AS owner, B.`days`, B.`ginst`, C.`username`, D.`seo`, E.`username` as buyer FROM `orders` A, `posts` B, `members` C, `categories` D, `members` E WHERE C.`USERID`=B.`USERID` AND B.`category`=D.`CATID` AND E.`USERID`=A.`USERID` AND A.`OID`='".sql_quote($OID)."' AND B.`PID`=A.`PID` LIMIT 1";
					$results=$conn->execute($query);
					$o = $results->getrows();
					send_instant_notification($owner, 'mutual_accept', 'track?id='.$OID.'&mid='.$AMID, $_SESSION['USERID'], true);
				}
			}
		}
		if ($is_admin)
			$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox2` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($buyer)."' AND B.`MSGFROM`='".sql_quote($owner)."') OR (B.`MSGTO`='".sql_quote($owner)."' AND B.`MSGFROM`='".sql_quote($buyer)."') OR (B.`MSGFROM`='1' AND B.`OID`='".sql_quote($OID)."')) AND B.`OID`='".sql_quote($OID)."' ORDER BY B.`MID` ASC";
		else
			$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox2` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."') OR (B.`MSGFROM`='1' AND B.`OID`='".sql_quote($OID)."'))  AND B.`OID`='".sql_quote($OID)."' ORDER BY B.`MID` ASC";
		$results=$conn->execute($query);
		$m = $results->getrows();
		abr('m',$m);
		abr('v',$v);
		if ($o[0]['status'] == 5) {
			$query="SELECT `good`, `bad`, `time_added`, `comment` FROM `ratings` WHERE `OID`='".sql_quote($OID)."' AND `RATER`='".$buyer."' LIMIT 1";
			$results=$conn->execute($query);
			$r = $results->getrows();
			abr('r',$r[0]);
		}
	}
}
else
{
	$eurl = "track?id=".$OID;
	$rurl = base64_encode($eurl);
	header("Location:$config[baseurl]/signin?r=".$rurl);exit;
}

//TEMPLATES BEGIN
abr('o',$o[0]);
abr('message',$message);
abr('inProject',1);
abr('plupload', true);
abr('working_process', $config['working_process']);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>