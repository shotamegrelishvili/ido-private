idw={
	e : null,
	state : null,
	uri : ('https:' == document.location.protocol ? 'https:' : 'http:')+'//'+(document.location.href.indexOf('localhost')>-1 ? 'localhost/ido' : 'widget.ido.ge')+'/widget.php',

	widget_params : {
		ref : null,
		width : 200,
		maxheight : 100,
		layout : 'standard',
		action : 'jobs',
		controls : true,
		border : true,
		buttons : true
	},

	Init : function(div) {
		var widgetDivs = document.querySelectorAll(div);
		if (widgetDivs.item(0) == null) return false;

		for (m in widgetDivs) {
			// ToDo: reset all params
			if (typeof(widgetDivs[m]) == 'object') {
				this.e = widgetDivs[m];
				for (c in this.e.dataset) {
					this.widget_params[c] = this.e.dataset[c];
				}
				this.Load();
			}
		}
	},

	Load : function() {
		this.state = 'loading';
		this.uriConstructor();
		this.e.innerHTML = '<iframe name="a" src="'+this.uri+'" allowtransparency="true" frameborder="0" scrolling="no" tabindex="0" style="width: '+this.widget_params['width']+'px; height: '+this.widget_params['maxheight']+'px"></iframe>';
	},

	uriConstructor : function() {
		this.uri += '?ref='+encodeURIComponent(this.widget_params['ref'])+'&action='+this.widget_params['action']+'&layout='+this.widget_params['layout']+'&width='+this.widget_params['width']+'&maxheight='+this.widget_params['maxheight']+'&controls='+this.widget_params['controls']+'&border='+this.widget_params['border']+'&buttons='+this.widget_params['buttons'];
	},
}

idw.Init(".ido-widget");

function d(t) {
	// Used for debugging
	console.log(t);
}