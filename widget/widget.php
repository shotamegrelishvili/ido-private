<?php
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: '.($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http' ? 'http' : 'https').'://widget.ido.ge');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: text/html; charset=utf-8');

$config['widget'] = true;

include('../include/config.php');
include('../include/functions/import.php');

if (!isset($_REQUEST['action']) || !in_array($_REQUEST['action'], array('jobs','freelancers','all'))) {
	$_REQUEST['action'] = 'jobs';
}

if ($_REQUEST['maxheight'] && (is_numeric($_REQUEST['maxheight']) || substr($_REQUEST['maxheight'],-2)=='px') && intval($_REQUEST['maxheight']) >= 100 && intval($_REQUEST['maxheight']) <= 1080) {
	abr('height', intval($_REQUEST['maxheight']).'px');
	abr('maxheight', '100vh');
}
elseif ($_REQUEST['maxheight'] && (substr($_REQUEST['maxheight'],-1)=='%' || substr($_REQUEST['maxheight'],-2)=='em' || substr($_REQUEST['maxheight'],-2)=='vw' || substr($_REQUEST['maxheight'],-3)=='rem')) {
	$maxheight = intval($_REQUEST['maxheight']);
	$units = str_replace($maxheight, '', $_REQUEST['maxheight']);
	abr('height', $maxheight.$units);
	abr('maxheight', '100vh');
}
else {
	abr('height', '100%');
	abr('maxheight', '100vh');
}
if ($_REQUEST['action'] == 'all' || $_REQUEST['action']!='jobs') $_REQUEST['action'] = 'freelancers';
if ($_REQUEST['action'] == 'jobs') {
	$res = $conn->execute('SELECT "job" as `type`, w.*, c.`name`, c.`seo` FROM `wants` w, `categories` c WHERE w.`active`="1" AND c.`CATID`=w.`category` ORDER BY RAND() LIMIT 20');
	$arr = $res->getrows();
}
elseif ($_REQUEST['action'] == 'freelancers') {
	$res = $conn->execute('SELECT "freelancer" as `type`, p.*, c.`name`, c.`seo` FROM `posts` p, `categories` c WHERE p.`active`="1" AND c.`CATID`=p.`category` ORDER BY RAND() LIMIT 20');
	$arr = $res->getrows();
}
elseif ($_REQUEST['action'] == 'all') {
	//$res = $conn->execute('SELECT "job" as `type`, w.*, c.`name`, c.`seo` FROM `wants` w, `categories` c WHERE w.`active`="1" AND c.`CATID`=w.`category` ORDER BY w.`WID` DESC LIMIT 2');
	//$arr = $res->getrows();
	$res = $conn->execute('SELECT "freelancer" as `type`, p.*, c.`name`, c.`seo` FROM `posts` p, `categories` c WHERE p.`active`="1" AND c.`CATID`=p.`category` ORDER BY RAND() LIMIT 2');
	$arr = $res->getrows();
	//$arr = array_merge($arr, $res->getrows());
}

if ($_REQUEST['width'] && (is_numeric($_REQUEST['width']) || substr($_REQUEST['width'],-2)=='px') && intval($_REQUEST['width']) >= 200 && intval($_REQUEST['width']) <= 1920) {
	abr('width', intval($_REQUEST['width']).'px');
}
elseif ($_REQUEST['width'] && (substr($_REQUEST['width'],-1)=='%' || substr($_REQUEST['width'],-2)=='em' || substr($_REQUEST['width'],-2)=='vw' || substr($_REQUEST['width'],-3)=='rem')) {
	$width = intval($_REQUEST['width']);
	$units = str_replace($width, '', $_REQUEST['width']);
	abr('width', $width.$units);
	abr('max-width', '71.25em');
}
else {
	abr('width', '100%');
	abr('max-width', '71.25em');
}

if ($_REQUEST['border']=='true' || $_REQUEST['border']=='1') abr('border',1);
if ($_REQUEST['buttons']=='true' || $_REQUEST['buttons']=='1') abr('buttons',1);
abr('ref',$_REQUEST['ref']);
abr('controls',$_REQUEST['controls']);
abr('arr',$arr);
$smarty->display('widget.tpl');
?>