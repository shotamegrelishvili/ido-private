<?php
include("include/config.php");
include("include/functions/import.php");
if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	
	if($_POST['subarc'] == "1")
	{
		$auid = intval(cleanit($_REQUEST['auid']));
		if($auid > 0)
		{
			$query="INSERT INTO `archive` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `AID`='".sql_quote($auid)."'";
			$results=$conn->execute($query);	
			$message = $lang['244'];
		}
	}
	
	$templateselect = "inbox.tpl";
	$pagetitle = $lang['28'];
	abr('pagetitle',$pagetitle);
	
	$s = cleanit($_REQUEST['s']);
	if($s == "" || $s == "all")
	{
		$s = "all";
	}
	elseif($s == "unread")
	{
		$s = "unread";
	}
	elseif($s == "archived")
	{
		$s = "archived";
	}
	abr('s',$s);
	
	$a = cleanit($_REQUEST['a']);
	if($a != "1")
	{
		$a = "0";	
	}
	abr('a',$a);
	$o = cleanit($_REQUEST['o']);
	if($o == "" || $o == "time")
	{
		$o = "time";
		if($a == "1")
		{
			$addsql2 = "time asc";
		}
		else
		{
			$addsql2 = "time desc";
		}
	}
	elseif($o == "name")
	{
		$o = "name";
		if($a == "1")
		{
			$addsql2 = "B.username asc";
		}
		else
		{
			$addsql2 = "B.username desc";
		}
	}
	abr('o',$o);
	
	$u = intval(cleanit($_REQUEST['u']));
	if($u > 0)
	{
		$addsql3 = "AND B.USERID='".sql_quote($u)."'";	
	}
	abr('u',$u);
	
	$query="SELECT `AID` FROM `archive` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."'";
	$results=$conn->execute($query);
	$arc = $results->getrows();
	abr('arc',$arc);
	
	$query = 'SELECT max(A.`time`) as time, max(A.`unread`) as unread, A.`MSGFROM` as msgfrom, B.`username`, B.`USERID` FROM `inbox` A, `members` B WHERE ((A.`MSGFROM`="'.sql_quote($_SESSION['USERID']).'" AND A.`MSGTO`=B.`USERID`) OR (A.`MSGTO`="'.sql_quote($_SESSION['USERID']).'" AND A.`MSGFROM`=B.`USERID`)) '.$addsql3.' GROUP BY B.`username` ORDER BY '.$addsql2;
	$results=$conn->execute($query);
	$m = $results->getrows();
	abr('m',$m);
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>