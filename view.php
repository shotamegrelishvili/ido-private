<?php
include('include/config.php');
include('include/functions/import.php');
$thebaseurl = $config['baseurl'];
$pid = stripslashes(strip_tags($_REQUEST['id']));
if($pid)
{
	if (isset($_SESSION['ADMINID']) && $_SESSION['ADMINID']) {
		abr('status_matrix', $config['status_matrix']);
		$activesql = "(A.`active`='0' OR A.`active`='1') AND A.`is_del`='0'";
	}
	else
		$activesql = "A.`active`='1' AND A.`is_del`='0'";
	$query="SELECT A.*, B.`name`, B.`seo`, C.`username`, C.`rating` as member_rating, C.`ratingcount`, C.`toprated`, C.`country`, C.`addtime` FROM `posts` A, `categories` B, `members` C WHERE A.`PID`='".intval($pid)."' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND {$activesql}";
	$executequery = $conn->Execute($query);
	$p = $executequery->getrows();
	$fnd = sizeof($p);
	$title = stripslashes($p[0]['gtitle']);
	$uname = stripslashes($p[0]['username']);
	$PD = stripslashes($p[0]['PID']);
	$gtags = stripslashes($p[0]['gtags']);
	$uid = stripslashes($p[0]['USERID']);
	abr('pagetitle', $title.' '.$p[0]['price'].' '.$lang['63']);
	abr('mdesc', str_replace(array("\n","\r"), " ", $p['0']['gdesc']));
	if ($p[0]['videohost'] && $p[0]['videofile']) {
		if ($p[0]['videohost'] == 'myvideo')
			$p[0]['video'] = 'http://embed.myvideo.ge/flv_player/player.php?video_id='.$p[0]['videofile'].'.mp4';
		elseif ($p[0]['videohost'] == 'youtube')
			$p[0]['video'] = '//www.youtube.com/embed/'.$p[0]['videofile'];
	}
	if(is_numeric($PD) && $PD > 0)
	{
		$tags = str_replace(',', ' ', $p[0]['gtags']);
		$tags = str_replace('  ', ' ', $tags);
		$tags = str_replace('/', '', $tags);
		$tags = explode(' ', $tags);
		abr('tags',$tags);
		for($i=0;$i<count($tags);$i++)
		{
			$addme .= " OR A.`gtags` like '%".sql_quote($tags[$i])."%'";	
		}
		/*
		// Related Deals
		$query="SELECT A.`PID`, A.`gtitle`, A.`p1`, A.`price`, A.`days`, A.`rating`, A.`rcount`, B.`name`, B.`seo` FROM `posts` A, `categories` B WHERE A.`PID`!='".intval($pid)."' AND A.`category`=B.`CATID` AND A.`active`='1' AND (A.`gtags` like '%".sql_quote($gtags)."%' $addme) ORDER BY RAND() LIMIT $config[view_rel_max]";
		$results=$conn->execute($query);
		$r = $results->getrows();
		abr('r',$r);
		*/

		// Other Deals from the same Author
//		d($config[view_more_max]);
		$query="SELECT A.`PID`, A.`gtitle`, A.`p1`, A.`price`, A.`days`, A.`rating`, A.`rcount`, B.`name`, B.`seo`, '".sql_quote($uname)."' as 'username', C.`profilepicture`, C.`rating` FROM `posts` A, `categories` B, `members` C WHERE A.`PID`!='".intval($pid)."' AND A.category=B.CATID and A.active='1' AND A.`USERID`='".sql_quote($uid)."' AND C.`USERID`=A.`USERID` ORDER BY A.`feat_mainpage` DESC, A.`feat_cat` DESC, A.`feat` DESC, RAND() LIMIT ".($config[view_more_max]+10);
		$results=$conn->execute($query);
		$u = $results->getrows();
		abr('u',$u);
		
		$query="SELECT A.`comment`, A.`good`, A.`bad`, B.`USERID`, B.`username` FROM `ratings` A, `members` B WHERE A.`PID`='".intval($PD)."' AND A.`RATER`=B.`USERID` AND B.`status`='1' ORDER BY A.`RID` DESC";
		$results=$conn->execute($query);
		$f = $results->getrows();
		abr('f',$f);
		$grat = 0;
		$brat = 0;
		$fsize = sizeof($f);
		for($i=0;$i<$fsize;$i++)
		{
			$tgood = $f[$i]['good'];
			$tbad = $f[$i]['bad'];
			if($tgood == "1")
			{
				$grat++;	
			}
			elseif($tbad == "1")
			{
				$brat++;	
			}
		}
		abr('grat',$grat);
		abr('brat',$brat);

		$queryb = "SELECT COUNT(*) as `total` FROM `orders` WHERE `PID`='".intval($PD)."' AND (`status`='0' OR `status`='1' OR `status`='6')"; 
		$executequeryb=$conn->execute($queryb);
		$quecount = $executequeryb->fields['total']+0;
		abr('quecount',$quecount);
		
		$queryb="SELECT COUNT(*) as `total` FROM `bookmarks` WHERE `PID`='".intval($PD)."'";
		$executequeryb=$conn->execute($queryb);
		$ftot = $executequeryb->fields['total']+0;
		abr('ftot',$ftot);
	}
	update_viewcount($pid);
	if($fnd > 0)
	{
		if (isset($_SESSION['ADMINID']) && $_SESSION['ADMINID']) {
			if ($p[0]['moderator_id'] && $p[0]['moderator_id']!=$_SESSION['ADMINID'] && $p[0]['moderation_queue'] > $_SERVER['REQUEST_TIME'] - 1800) {
				abr('moderator_block_changes', true);
				$error = $lang['754'];
			}
			else {
				$conn->execute("UPDATE `posts` SET `moderator_id` = '".intval($_SESSION['ADMINID'])."', `moderation_queue` = '".intval($_SERVER['REQUEST_TIME'])."' WHERE `PID`='".intval($PD)."' LIMIT 1");
			}
		}
		$theme = "view.tpl";
	}
	else
	{
		$query="SELECT `name`,`CATID`,`parent`,`mtitle`,`mdesc`,`mtags`,`details` FROM `categories` WHERE `seo`='".sql_quote($_REQUEST['title'])."' LIMIT 1";
		$executequery=$conn->execute($query);

		if ($executequery->fields['CATID']) {
			header('Location:'.$thebaseurl.'/categories/'.$_REQUEST['title']); exit;
		}
		else {
			abr('pathURI', '/');
			abr('pathTitle', ' ');
			abr('heading', $lang[180]);
			abr('subheading', $lang[737]);
			$theme = "empty.tpl";
		}
	}
}


abr('retargetingPID', $p[0]['PID']);
//abr('retargetingTitle', seo_clean_titles(geo2lat($p[0]['gtitle']));

//TEMPLATES BEGIN
abr('ogurl', $thebaseurl.'/'.$p[0]['seo'].'/'.$p[0]['PID'].'/'.seo_clean_titles($p[0]['gtitle']));
abr('ogtitle',$config['site_name'].': '.$p[0]['gtitle']);
abr('ogdescription',$p[0]['gdesc']);
abr('ogimage',$config['purl'].'/t/'.$p[0]['p1']);

abr('viewpage',1);
abr('itemsPerPage',$config[view_more_max]);
abr('p',$p[0]);
$smarty->display('header.tpl');
$smarty->display($theme);
if (isset($addtheme) && $addtheme)
	$smarty->display($addtheme);
$smarty->display('footer.tpl');
//TEMPLATES END
?>