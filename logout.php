<?php
require_once('include/config.php');
destroy_slrememberme($_SESSION[USERNAME]);
unset($_SESSION);
session_destroy();
if (!isset($no_redir) || !$no_redir)
	header("location: $config[baseurl]/");
?>
