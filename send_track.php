<?php
include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	
	if($_POST['submg'] == "1")
	{
		$msgto = intval(cleanit($_REQUEST['msgto']));
		$FID = intval(cleanit($_REQUEST['attachment_id']));
		$message_body = cleanit(trim($_REQUEST['message_body']));
		$oid = intval(cleanit($_REQUEST['oid']));
		$result = cleanit($_REQUEST['message_format']);
		abr('oid',$oid);
		$who = cleanit($_REQUEST['who']);
		abr('who',$who);
		$err = false;
		
		$query = "SELECT o.`USERID`, o.`PID`, o.`status`, o.`no_moneyback`, o.`price`, o.`pending_request`, p.`moneyback` FROM `orders` o, `posts` p WHERE o.`OID`='".sql_quote($oid)."' AND p.`PID`=o.`PID` LIMIT 1";
		$executequery=$conn->execute($query);
		$USERID = $executequery->fields['USERID'];
		abr('USERID',$USERID);
		$PID = $executequery->fields['PID'];
		$status = $executequery->fields['status'];
		$rprice = $executequery->fields['price'];
		$no_moneyback = $executequery->fields['no_moneyback'];
		$post_moneyback = $executequery->fields['moneyback'];
		$pending_request = $executequery->fields['pending_request'];

		if (($isPMvalid=isPMvalid($message_body))===false) {
			$err = $lang[786];
			abr('err',$err);
		}
		elseif($status != "2" && $status != "3" && $status != "7")
		{
			if($status == "0" && $_SESSION['USERID'] == $USERID)
			{
				$asql = ", `start`='1'";	
			}
			$query = "SELECT `days` FROM `posts` WHERE `PID`='".sql_quote($PID)."' LIMIT 1";
			$executequery=$conn->execute($query);
			$days = $executequery->fields['days'];
			abr('days',$days);
			
			if($result == "mutual_cancellation_request")
			{
				$query = "SELECT COUNT(*) as cc FROM `inbox2` WHERE `OID`='".sql_quote($oid)."' AND `MSGFROM`='".$_SESSION['USERID']."' AND `cancel`='0' AND `action`='mutual_cancellation_request' LIMIT 1";
				$executequery=$conn->execute($query);
				$cc = $executequery->fields['cc'];

				if ($cc) {
					$err = $lang['622'];
					abr('err',$err);
				}
				else {
					if ($pending_request == $result) {
						$last_mutual_request = $conn->execute('SELECT `MID` FROM `inbox2` WHERE `OID`="'.sql_quote($oid).'" AND `MSGTO`="'.intval($_SESSION['USERID']).'" AND `action`="mutual_cancellation_request" AND `cancel`="0" ORDER BY `MID` DESC LIMIT 1');
						if ($last_mutual_request->fields['MID']) {
							$conn->execute('UPDATE `inbox2` SET `cancel`="2", `ctime`="'.$_SERVER['REQUEST_TIME'].'", `CID`="'.sql_quote($_SESSION['USERID']).'" WHERE `MID`="'.sql_quote($last_mutual_request->fields['MID']).'" AND `OID`="'.sql_quote($oid).'" AND `MSGTO`="'.intval($_SESSION['USERID']).'" AND `action`="mutual_cancellation_request" AND `cancel`="0" LIMIT 1 ');
							issue_refund($USERID,$oid,$rprice,true);
							$query="UPDATE `orders` SET `status`='2', `cltime`='".$_SERVER['REQUEST_TIME']."', `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`='".sql_quote($oid)."' LIMIT 1";
							$results=$conn->execute($query);
							cancel_revenue($oid);
						}
					}
					else {
						$asql2 = ", `action`='".sql_quote($result)."', `ctime`='".$_SERVER['REQUEST_TIME']."'";
						$addsql1 = '`pending_request`="'.sql_quote($result).'", `PR_start_time`="'.$_SERVER['REQUEST_TIME'].'"';
					}
				}
			}
			elseif($result == "seller_cancellation" || $result == "buyer_cancellation" || $result == "seller_cancellation_inform_admin" || $result == "buyer_cancellation_inform_admin")
			{
				$asql2 = ", `action`='".sql_quote($result)."', `ctime`='".$_SERVER['REQUEST_TIME']."'";

				if ($result == 'seller_cancellation') {
					$override_verification_test = true;
				}
				else
					$override_verification_test = false;
				if (!$no_moneyback || ($result != 'buyer_cancellation' && $result != 'buyer_cancellation_inform_admin')) {
					issue_refund($USERID,$oid,$rprice,$override_verification_test);
					cancel_revenue($oid);
				}
				
				$query="UPDATE `orders` SET `status`='3', `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `PID`='".sql_quote($PID)."' LIMIT 1";
				$results=$conn->execute($query);
				send_update_email($msgto, $oid);
				
				$query = "SELECT `USERID` FROM `posts` WHERE `PID`='".sql_quote($PID)."' LIMIT 1";
				$executequery=$conn->execute($query);
				$SID = $executequery->fields['USERID'];
			
				if($result == 'seller_cancellation' || $result == 'seller_cancellation_inform_admin') {
					update_score($SID, $oid, 'cancel_task_by_own');
				}
				elseif ($result == 'buyer_cancellation' || $result == 'buyer_cancellation_inform_admin') {
					update_score($_SESSION['USERID'], $oid, 'cancel_task_by_own');
				}
				if ($result == 'buyer_cancellation_inform_admin' || $result == 'seller_cancellation_inform_admin') {
					update_score($_SESSION['USERID'], $oid, 'cancel_task_by_own_inform_admin');
					send_email_to_admin($oid, $message_body);
				}
			}
			elseif($result == "predelivery")
			{
				$asql2 = ", `action`='predelivery'";
				$query="UPDATE `orders` SET `status`='4', `substatus`='1', `no_moneyback`='0', `pending_request`='requires_buyers_confirm', `PR_start_time`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `PID`='".sql_quote($PID)."' LIMIT 1";
				$results=$conn->execute($query);
				send_update_email($msgto, $oid);
			}
			elseif($result == "delivery")
			{
				$asql2 = ", `action`='delivery'";
				if (!$post_moneyback) {
					$query="UPDATE `orders` SET `status`='4', `substatus`='2', `no_moneyback`='1', `pending_request`='requires_buyers_review', `PR_start_time`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `PID`='".sql_quote($PID)."' LIMIT 1";
				}
				else {
					$query="UPDATE `orders` SET `status`='4', `substatus`='2', `pending_request`='requires_buyers_review', `PR_start_time`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `PID`='".sql_quote($PID)."' LIMIT 1";
				}

				$results=$conn->execute($query);
				send_update_email($msgto, $oid);
			}
			elseif($result == "complete")
			{
				if($_POST['subrat'] == "1" && $oid)
				{
					$query="SELECT A.*, B.`gtitle`, B.`p1`, B.`price`, B.`USERID` AS owner, B.`days`, B.`ginst`, C.`username`, D.`seo`, E.`username` as buyer FROM `orders` A, `posts` B, `members` C, `categories` D, `members` E WHERE C.`USERID`=B.`USERID` AND B.`category`=D.`CATID` AND E.`USERID`=A.`USERID` AND A.`OID`='".sql_quote($oid)."' AND B.`PID`=A.`PID` LIMIT 1";
					$results=$conn->execute($query);
					$o = $results->getrows();
					if (isset($o[0]['USERID']) && $o[0]['USERID'] == $_SESSION['USERID']) {
						$ratingvalue = cleanit($_POST['ratingvalue']);
						if($ratingvalue == "1")
						{
							$rad = ", `good`='1'";
							update_rating($o[0]['owner'], $oid, 'increase', $PID, $_SESSION['USERID'], $message_body);
						}
						elseif($ratingvalue == "0")
						{
							$rad = ", `bad`='1'";
							update_rating($o[0]['owner'], $oid, 'decrease', $PID, $_SESSION['USERID'], $message_body);
						}
						else
							die('access denied');
						$asql2 = ", `action`='".sql_quote($result)."'";
						$ratingcomment = cleanit($_POST['message_body']);
						$message = $lang['312'];
						$query="UPDATE `orders` SET `status`='5', `pending_request`=NULL, `PR_start_time`=NULL, `cltime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
						$results=$conn->execute($query);

						update_score($o[0]['owner'], $oid, 'success_task_owner');
						update_score($o[0]['USERID'], $oid, 'success_task_buyer');

						abr('o',$o[0]);

						send_update_email($owner, $OID);
						update_gig_rating($PID);
					}
					else die('access denied');
				}
			}
			elseif($result == "rejection")
			{
				$asql2 = ", `action`='".sql_quote($result)."'";
				$query="UPDATE `orders` SET `status`='6', `substatus`=NULL, `pending_request`=NULL, `PR_start_time`=NULL WHERE `OID`='".sql_quote($oid)."' AND `PID`='".sql_quote($PID)."' LIMIT 1";
				$results=$conn->execute($query);
				$query="UPDATE `inbox2` SET `reject`='1' WHERE `OID`='".sql_quote($oid)."' AND `action`='delivery' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
				$results=$conn->execute($query);
				$query="UPDATE `inbox2` SET `cancel`='1' WHERE `OID`='".sql_quote($oid)."' AND `action`='predelivery' AND `MSGTO`='".sql_quote($_SESSION['USERID'])."' ORDER BY `MID` DESC LIMIT 1";
				$results=$conn->execute($query);
				send_update_email($msgto, $oid);
			}
			
			if($msgto > 0 && $message_body != "" && !$err)
			{
				if (isset($addsql1) && $addsql1) {
					$conn->execute('UPDATE `orders` SET '.$addsql1.' WHERE `OID`="'.intval($oid).'" LIMIT 1');
				}
				$query="INSERT INTO `inbox2` SET `MSGFROM`='".sql_quote($_SESSION['USERID'])."', `MSGTO`='".sql_quote($msgto)."',`message`='".sql_quote($message_body)."', `FID`='".sql_quote($FID)."', `OID`='".sql_quote($oid)."', `time`='".$_SERVER['REQUEST_TIME']."', `ip`='".sql_quote($_SERVER['REMOTE_ADDR'])."' $asql $asql2";
				$insertresult=$conn->execute($query);
				$mid = $conn->_insertid();
				if($mid > 0)
				{
					$UID = $msgto;
					$query="SELECT DISTINCT A.`username` AS mto, A.`mobile` AS `tomobile`, C.`username` AS mfrom, B.* FROM `members` A, `inbox2` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."')) AND B.`OID`='".sql_quote($oid)."' ORDER BY B.`MID` ASC";
					$results=$conn->execute($query);
					$m = $results->getrows();
					abr('m',$m);
					
					if($status == "0")
					{
						$query = "UPDATE `orders` SET `status`='1', `stime`='".$_SERVER['REQUEST_TIME']."' WHERE `OID`='".sql_quote($oid)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1"; 
						//d($query);
						$conn->execute($query);
						if ($isPMvalid === 'suspected') {
							$sendSMS = false;
							send_sms(52, 'suspected', 'administrator/orders_manage.php?fromid='.$oid);
						}
						else $sendSMS = true;
						send_instant_notification($msgto, 'seller_has_new_order', 'track?id='.$oid, $_SESSION['USERID'], true, $sendSMS);
					}
					else {
						if ($result == 'predelivery')
							send_instant_notification($msgto, 'predelivery', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true);
						elseif ($result == 'delivery')
							send_instant_notification($msgto, 'delivery', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true);
						elseif ($result == 'complete')
							send_instant_notification($msgto, 'complete', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true);
						elseif ($result == 'rejection')
							send_instant_notification($msgto, 'rejection', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true);
						elseif ($result == 'seller_cancellation' || $result == 'buyer_cancellation' || $result == 'seller_cancellation_inform_admin' || $result == 'buyer_cancellation_inform_admin')
							send_instant_notification($msgto, 'task_cancel', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true);
						else{
							if ($isPMvalid === 'suspected') {
								$sendSMS = false;
								send_sms(52, 'suspected', 'administrator/orders_manage.php?fromid='.$oid);
							}
							else $sendSMS = true;
							send_instant_notification($msgto, 'task_pm', 'track?id='.$oid.'&mid='.$mid, $_SESSION['USERID'], true, $sendSMS);
						}
					}
					send_update_email($msgto, $oid);
				}
			}
			else {
				$UID = $msgto;
				$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox2` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."')) AND B.`OID`='".sql_quote($oid)."' ORDER BY B.`MID` ASC";
				$results=$conn->execute($query);
				$m = $results->getrows();
				abr('m',$m);
			}
		}
		else
		{
			$UID = $msgto;
			$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox2` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."')) AND B.`OID`='".sql_quote($oid)."' ORDER BY B.`MID` ASC";
			$results=$conn->execute($query);
			$m = $results->getrows();
			abr('m',$m);
		}
	}
}

//TEMPLATES BEGIN
abr('message',$message);
$smarty->display('send_track.tpl');
//TEMPLATES END
?>