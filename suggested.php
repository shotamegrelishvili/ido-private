<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

/*
if (!$_SESSION['USERID']) {
	$_SESSION['temp']['message_title'] = $lang['724'];
	$_SESSION['temp']['message_type'] = 'error';
	header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('suggested'));
	exit;
}
*/
//d($_SESSION,1);
$templateselect = "empty.tpl";

if (isset($_SESSION['store'])) {
	foreach ($_SESSION['store'] as $skey => $sval)
		$_REQUEST[$skey] = $sval;
	unset($_SESSION['store']);
}

$UID = intval(cleanit($_SESSION['USERID']));
$WID = intval(cleanit($_REQUEST['sug']));
$del = intval(cleanit($_REQUEST['del']));
if($UID > 0)
{
	if($del == "1")
	{
		if($WID > 0)
		{
			$query="DELETE FROM `wants` WHERE `WID`='".sql_quote($WID)."' AND `USERID`='".sql_quote($UID)."' AND `active`='1' LIMIT 1";
			$result=$conn->execute($query);	
			$message = $lang['497'];
		}
	}
}

if(@$_REQUEST['sugsub'] == '1')
{
	if(intval($_SESSION['USERID']) && $_SESSION['VERIFIED'])
	{
		$sugcontent = cleanit($_REQUEST['sugcontent']);
		if (!isset($_SESSION['temp']['suggest']) || md5($sugcontent) != $_SESSION['temp']['suggest']) {
			$sugcat = intval(cleanit($_REQUEST['sugcat']));
			if(mb_strlen($sugcontent,'UTF8') > 10 && $sugcat != "")
			{
				$approve_suggests = $config['approve_suggests'];
				if($approve_suggests == "1")
				{
					$active = "0";
				}
				else
				{
					$active = "1";
				}
				$query="INSERT INTO `wants` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `want`='".sql_quote($sugcontent)."', `category`='".sql_quote($sugcat)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `date_added`='".date("Y-m-d")."', `active`='$active'";
				$_SESSION['temp']['suggest'] = md5($sugcontent);
				$result=$conn->execute($query);
				$message = $lang['121'];
			}
			else {
				$error = $lang['739'];
			}
		}
		else {
			$error = $lang['740'];
		//	$_SESSION['temp']['suggest'] = md5($sugcontent);
		//	$message = $lang['121'];
		}
	}
	else {
		if ($_SESSION['USERID'] && !$_SESSION['VERIFIED'])
			$error = sprintf($lang['445'], $config['baseurl'].'/resendconfirmation.php');
		else {
			unset($_SESSION['store']);
			$_SESSION['store'] = array(	'sugcontent' => $_REQUEST['sugcontent'],
										'sugcat' => $_REQUEST['sugcat'],
										'sugsub' => $_REQUEST['sugsub']);
			$_SESSION['temp']['message_type'] = 'error';
			$_SESSION['temp']['message_title'] = $lang[736];
			header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('suggested')); exit;
		}
	}
}

$s = cleanit($_REQUEST['s']);
abr('s',$s);

$page = intval($_REQUEST['page']);

if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

if (isset($_SESSION['USERID']) && $_SESSION['USERID'] && !$s) {
	$dbw = ', CASE WHEN A.`USERID`="'.intval($_SESSION['USERID']).'" THEN 1 ELSE 0 END as `userwants`';
	$dby = '`userwants` DESC, ';
}
else $dbw = $dby = '';

if($s == "dz")
{
	$dby .= "A.WID asc";	
}
else
{
	$dby .= "A.WID desc";	
}

$addsql = '';
if (isset($_GET['WID']) && is_numeric($_GET['WID'])) {
	$addsql .= " AND `WID`='".intval($_GET['WID'])."'";
}

$query1 = "SELECT count(*) as total FROM `wants` WHERE `active`='1' $addsql ORDER BY `WID` DESC LIMIT $config[maximum_results]";
$query2 = "SELECT A.*, M.`username`, M.`country`, M.`toprated`, C.`CATID`".$dbw." FROM `wants` A, `members` M, `categories` C WHERE A.`active`='1'".$addsql." AND A.`USERID`=M.`USERID` AND C.`CATID`=A.`category` ORDER BY $dby LIMIT $pagingstart, $config[items_per_page]";
$executequery1 = $conn->Execute($query1);
$result = $executequery1->fields['total'];
if ($result > 0)
{
	if($executequery1->fields['total']<=$config[maximum_results])
	{
		$total = $executequery1->fields['total'];
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if($s != "")
	{
		$adds = "&s=$s";
	}
	if ($currentpage > 0)
	{
		if($currentpage > 1) 
		{
			$pagelinks.="<li class='prev'><a href='$thebaseurl/suggested?page=$theprevpage$adds'>$theprevpage</a></li>&nbsp;";
		}
		else
		{
			$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
		}
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<li><a href='$thebaseurl/suggested?page=$lowercount$adds'>$lowercount</a></li>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<li><a href='$thebaseurl/suggested?page=$uppercounter$adds'>$uppercounter</a></li>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<li class='next'><a href='$thebaseurl/suggested?page=$thenextpage$adds'>$thenextpage</a></li>";
		}
		else
		{
			$pagelinks.="<li><span class='next'>next page</span></li>";
		}
	}
}
$templateselect = "suggested.tpl";
//TEMPLATES BEGIN
abr('pagetitle',stripslashes($lang['496']));
abr('error',$error);
abr('message',$message);
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total);
abr('posts',$posts);
abr('suggest_page',true);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>