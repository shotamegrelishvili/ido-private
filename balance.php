<?php
include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	$templateselect = "balance.tpl";
	$pagetitle = $lang['205'];
	abr('pagetitle',$pagetitle);
	
	$query="SELECT `OID`, `time`, `t`, `price`, `cancel`, `delayed_cancel` FROM `payments` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' ORDER BY `ID` DESC";
	$buyer_results=$conn->execute($query);
	$buyer_activities = $buyer_results->getrows();
	abr('o',$buyer_activities);
	
	$paid_sum = 0;
	foreach ($buyer_activities as $buyer_activity){
		if (!$buyer_activity['cancel'] && !$buyer_activity['delayed_cancel'])
			$paid_sum += $buyer_activity['price'];
	}
	abr('paid_sum',sprintf("%01.2f", $paid_sum));

	
	$query="SELECT A.`OID`, A.`time`, A.`price`, A.`cancel`, A.`delayed_cancel`, A.`transfer_complete`, B.`status`, B.`cltime`, B.`IID`, C.`ctp` FROM `payments` A, `orders` B, `posts` C WHERE A.`OID`=B.`OID` AND B.`PID`=C.`PID` AND C.`USERID`='".sql_quote($_SESSION['USERID'])."' ORDER BY A.`ID` DESC";
	$seller_results=$conn->execute($query);
	$p = $seller_results->getrows();
	abr('p',$p);
	
	$funds = 0;
	$upcoming = 0;
	$awaiting_clearance = 0;
	$psize=sizeof($p);
	for($i=0; $i<$psize;$i++)
	{
		$status = $p[$i]['status'];
		$paid_price = $p[$i]['price'];
		$IID = intval($p[$i]['IID']);
		if($IID > 0)
		{
			$ctp = get_ctp($IID); // Get Seller share of revenue
		}
		else
		{
			$ctp = $p[$i]['ctp'];
		}
		$paid_price = get_yprice2($p[$i]['price'], $ctp);
		$payment_datetime = $p[$i]['cltime'];
		//d($status);
		if($status == "5")
		{
			$days_awaiting_clearance = 0;
 			if (!$p[$i]['transfer_complete']) {
				$days_awaiting_clearance = get_days_before_transfer($payment_datetime);
					if($days_awaiting_clearance > 0)
				{
					$awaiting_clearance = $awaiting_clearance + $paid_price;
				}
			}
			else {
				$funds += $paid_price;
			}
		}
		elseif($status == "2" || $status == "3" || $status == "7")
		{
		}
		else
		{
			$upcoming = $upcoming + $paid_price;	
			$upcoming = sprintf("%01.2f", $upcoming);
		}
	}
	abr('funds',sprintf("%01.2f",$funds));
	abr('upcoming',sprintf("%01.2f",$upcoming));
	$awaiting_clearance = sprintf("%01.2f", $awaiting_clearance);
	abr('awaiting_clearance',$awaiting_clearance);	

/*
	$query = "SELECT `funds` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1"; 
	$executequery=$conn->execute($query);
	$funds = $executequery->fields['funds'];
	abr('funds',$funds);

$wd_in_process = 0;
if (isset($_POST['wdfunds'])) {
	$wdfunds = floatval(cleanit($_POST['wdfunds']));
//	$wdfunds2 = intval(cleanit($_POST['wdfunds2']));
	if($wdfunds && $wdfunds <= $afunds && $wdfunds >= $config['min_wd'])
	{
		$query="INSERT INTO `withdraw_requests` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `time_added`='".$_SERVER['REQUEST_TIME']."', `wd_sum`='".sql_quote($wdfunds)."', `ap`='0'";
		$conn->execute($query);
		$query="UPDATE `members` SET `afunds` = `afunds`-".sql_quote($wdfunds)." WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
		$conn->execute($query);
		$message = $lang['395'];
	}
	elseif ($wdfunds < $config['min_wd']) {
		$wdfunds = 0;
		$error = $lang['579'].$config['min_wd'].' '.$lang['197'];
	}
	else {
		$wdfunds = 0;
		$error = $lang['578'];
	}
}
else {
	$query = "SELECT `wd_sum` FROM `withdraw_requests` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `request_processed`='0' LIMIT 1";
	$wd_funds_query=$conn->execute($query);
	$wd_in_process = $wd_funds_query->fields['wd_sum'];
}

	if ($wdfunds) {
		$afunds = $afunds - $wdfunds;		
	}
	abr('afunds',$afunds);
	$withdrawn = $executequery->fields['withdrawn'];
	abr('withdrawn',$withdrawn);

	$buyer_query="SELECT SUM(A.`price`) as paid_sum FROM `payments` A WHERE A.`USERID`='".sql_quote($_SESSION['USERID'])."' AND A.`cancel`='0' AND A.`delayed_cancel`='0'";
	$buyer_results=$conn->execute($buyer_query);
*/
	
	
	
	
	$overall = $upcoming + $awaiting_clearance + $afunds;
	$overall = sprintf("%01.2f", $overall);
	abr('overall',$overall);
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
abr('min_wd',$config['min_wd']);
abr('error',$error);
abr('message',$message);
abr('sm3',"1");
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>