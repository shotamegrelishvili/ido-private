<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

$SID = $_SESSION['USERID'];
if ($SID != "" && $SID >= 0 && is_numeric($SID))
{	
	abr('pagetitle',$lang['30']);
	
	$page = intval($_REQUEST['page']);
	
	if($page=="")
	{
		$page = "1";
	}
	$currentpage = $page;
	
	if ($page >=2)
	{
		$pagingstart = ($page-1)*$config['items_per_page'];
	}
	else
	{
		$pagingstart = "0";
	}
	
	$query1 = "SELECT COUNT(*) AS `total` FROM `posts` A, `bookmarks` B WHERE A.`active`='1' AND A.`is_del`='0' AND B.`USERID`='".sql_quote($SID)."' AND A.`PID`=B.`PID` ORDER BY B.`BID` DESC LIMIT $config[maximum_results]";
	$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`country`, C.`toprated`, C.`profilepicture`, C.`rating` FROM `posts` A, `categories` B, `members` C, `bookmarks` D WHERE A.`active`='1' AND A.`is_del`='0' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND D.`USERID`='".sql_quote($SID)."' AND A.`PID`=D.`PID` ORDER BY D.`BID` DESC LIMIT $pagingstart, ".($config[items_per_page]+20);
	$executequery1 = $conn->Execute($query1);
	$result = $executequery1->fields['total'];
	if ($result > 0)
	{
		if($executequery1->fields['total']<=$config[maximum_results])
		{
			$total = $executequery1->fields['total'];
		}
		else
		{
			$total = $config[maximum_results];
		}
		$toppage = ceil($total/$config[items_per_page]);
		if($toppage==0)
		{
			$xpage=$toppage+1;
		}
		else
		{
			$xpage = $toppage;
		}
		$executequery2 = $conn->Execute($query2);
		$posts = $executequery2->getrows();
		$beginning=$pagingstart+1;
		$ending=$pagingstart+$executequery2->recordcount();
		$pagelinks="";
		$k=1;
		$theprevpage=$currentpage-1;
		$thenextpage=$currentpage+1;
		if ($currentpage > 0)
		{
			if($currentpage > 1) 
			{
				$pagelinks.="<li class='prev'><a href='$thebaseurl/bookmarks?page=$theprevpage'>$theprevpage</a></li>&nbsp;";
			}
			else
			{
				$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
			}
			$counter=0;
			$lowercount = $currentpage-5;
			if ($lowercount <= 0) $lowercount = 1;
			while ($lowercount < $currentpage)
			{
				$pagelinks.="<li><a href='$thebaseurl/bookmarks?page=$lowercount'>$lowercount</a></li>&nbsp;";
				$lowercount++;
				$counter++;
			}
			$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
			$uppercounter = $currentpage+1;
			while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
			{
				$pagelinks.="<li><a href='$thebaseurl/bookmarks?page=$uppercounter'>$uppercounter</a></li>&nbsp;";
				$uppercounter++;
			}
			if($currentpage < $toppage) 
			{
				$pagelinks.="<li class='next'><a href='$thebaseurl/bookmarks?page=$thenextpage'>$thenextpage</a></li>";
			}
			else
			{
				$pagelinks.="<li><span class='next'>next page</span></li>";
			}
		}
	}
	$templateselect = "bookmarks.tpl";
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
abr('message',$message);
abr('beginning',$beginning);
abr('ending',$ending);
abr('itemsPerPage',$config[items_per_page]);
//abr('pagelinks',$pagelinks);
if ($total > $config[items_per_page])
	abr('load_more',true);
abr('total',$total);
abr('posts',$posts);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>