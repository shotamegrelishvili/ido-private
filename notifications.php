<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

if (!isset($_SESSION['USERID']) || !$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) 
	{
		header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('notifications')); exit;
	}

$notifications = $cache->get('u.N.'.$_SESSION['USERID']);
$cache->set('u.Ncc.'.$_SESSION['USERID'], 0, 2592000);
//d($notifications);
abr('notifications', $notifications);
abr('addWorkAreaWrapper',1);
//TEMPLATES BEGIN

abr('pagetitle',$lang['795']);
abr('pathTitle',$lang['795']);
abr('pathURI','notifications');
abr('heading',$lang['795']);
abr('subheading',$lang['796']);

//TEMPLATES BEGIN
//abr('error',$error);

$smarty->display('header.tpl');
$smarty->display('empty.tpl');
$smarty->display('notifications.tpl');
$smarty->display('footer.tpl');
//TEMPLATES END
?>