<?php
//include("include/config.php");
//include("include/functions/import.php");
if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
/*	$query = "SELECT COUNT(*) as total FROM `orders` WHERE (`status`='1') AND `pid` IN (SELECT `pid` FROM `posts` WHERE `USERID` = '".sql_quote($_SESSION['USERID'])."')";
	$executequery=$conn->execute($query);
	$activeorders = $executequery->fields['total'];
	abr('activeorders',$activeorders);
*/
	$sp = cleanit($_REQUEST['sp']);
	if ($_REQUEST['tab'] == 'pending' && $tag && mb_strlen($tag, 'UTF-8') >= 3) {
		//if (mb_strlen($query, 'UTF-8') > 4) $query = mb_substr($query, 0, mb_strlen($query,'UTF-8') - 1);
		$addme = " AND (B.`gtitle` LIKE '%{$tag}%' OR B.`gdesc` LIKE '%{$tag}%' OR B.`gtags` LIKE '%{$tag}%' OR B.`ginst` LIKE '%{$tag}%' OR
						A.`OID` IN (SELECT `OID` FROM `inbox2` WHERE (`MSGTO`='".intval($_SESSION['USERID'])."' OR `MSGFROM`='".intval($_SESSION['USERID'])."') AND `message` LIKE '%{$tag}%' AND `OID`>0 )
		)";
		$sp = '';
		abr('search', $tag);
	}
	elseif($sp == "cancelled")
	{
		$addme = " AND (A.`status`='2' OR A.`status`='3' OR A.`status`='7')";
	}
	elseif($sp == "completed")
	{
		$addme = " AND A.`status`='5'";
	}
	elseif($sp == "delivered")
	{
		$addme = " AND A.`status`='4'";
	}
	else
	{
		$addme = " AND (A.`status`='0' OR A.`status`='1' OR A.`status`='6')";
		$sp = "active";
	}
	abr('sp',$sp);
	$b = cleanit($_REQUEST['b']);
	if($b == "date")
	{
		$addme2 = "A.`time_added`";
	}
	elseif($b == "status")
	{
		$addme2 = "A.`status`";
	}
	else
	{
		$addme2 = "A.`OID`";
		$b = "id";
	}
	abr('b',$b);
	$a = cleanit($_REQUEST['a']);
	if($a == "asc")
	{
		$addme3 = "asc";	
	}
	else
	{
		$addme3 = "desc";
		$a = "desc";
	}
	abr('a',$a);
	
	$templateselect = "manage_orders.tpl";
	$pagetitle = $lang['154'];
	abr('pagetitle',$pagetitle);
	
	$query="SELECT A.`OID`, A.`time_added`, A.`status`, A.`stime`, A.`cltime`, B.`gtitle`, B.`days`, C.`username` FROM `orders` A, `posts` B, `members` C WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND C.`USERID`=A.`USERID` AND B.`PID`=A.`PID` $addme order by $addme2 $addme3";
	//d($query);
	$results=$conn->execute($query);
	$p = $results->getrows();
	abr('pendingResultSize',sizeof($p));
	abr('p',$p);

	$addc1 = " AND (A.`status`='2' OR A.`status`='3' OR A.`status`='7')";
	$addc2 = " AND A.`status`='5'";
	$addc3 = " AND A.`status`='4'";
	$addc4 = " AND (A.`status`='0' OR A.`status`='1' OR A.`status`='6')";
	$addc5 = " AND (A.`status`='1' OR (A.`status`='4' AND A.`substatus`='1') OR A.`status`='6')";
	$query="SELECT count(*) as total FROM `orders` A, `posts` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND B.`PID`=A.`PID` {$addc1} ORDER BY {$addme2} {$addme3}";
	$results=$conn->execute($query);
	$counta = $results->fields['total']; // Cancelled Orders
	abr('counta',$counta);
	$query="SELECT count(*) as total FROM `orders` A, `posts` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND B.`PID`=A.`PID` {$addc2} ORDER BY {$addme2} {$addme3}";
	$results=$conn->execute($query);
	$countb = $results->fields['total']; // Completed Orders
	abr('countb',$countb);
	$query="SELECT count(*) as total FROM `orders` A, `posts` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND B.`PID`=A.`PID` {$addc3} ORDER BY {$addme2} {$addme3}";
	$results=$conn->execute($query);
	$countc = $results->fields['total']; // Delivered Orders
	abr('countc',$countc);
	$query="SELECT count(*) as total FROM `orders` A, `posts` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND B.`PID`=A.`PID` {$addc4} ORDER BY {$addme2} {$addme3}";
	$results=$conn->execute($query);
	$countd = $results->fields['total']; // Ongoing Orders
	abr('countd',$countd);
	$query="SELECT count(*) as total FROM `orders` A, `posts` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND B.`PID`=A.`PID` {$addc5}";
	$results=$conn->execute($query);
	$countn = $results->fields['total']; // To notify Orders
	abr('countn',$countn);	
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
/*
abr('message',$message);
abr('sm2',"1");
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
*/
//TEMPLATES END
?>