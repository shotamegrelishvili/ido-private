<?php
include('include/config.php');
include('include/functions/import.php');

sleep(2);

if (!isset($_POST['message_body']) || strlen(trim($_POST['message_body'])) < 2) {
	header('Location:'.$config['baseurl'] . '/inbox'); exit;
}
elseif ($_SESSION['USERID'] != '' && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	
	if($_POST['submg'] == "1")
	{
		$msgto = intval(cleanit($_REQUEST['msgto']));
		$FID = intval(cleanit($_REQUEST['attachment_id']));
		$message_body = cleanit(trim($_REQUEST['message_body']));
		$aboutid = intval(cleanit($_REQUEST['aboutid']));
		if (isset($_SESSION['pm_hash']) && $_SESSION['pm_hash'] == md5($message_body)) {
			abr('error', $lang['789']);
			$message_body = '';
			abr('clean_message_body', true);
		}
		
		if($msgto > 0 && mb_strlen($message_body, 'UTF-8')>2)
		{
			$message_filter_body = filter_messages($message_body);
			$invalidPMcc = $cache->get('invPMcc.'.md5($_SERVER['REMOTE_ADDR']));
			$suspected=isPMvalid($message_body);

			if($message_filter_body == 1 || $suspected===false || ($suspected === 'suspected' && $_POST['suspect_pass']!=1))
			{
				if (!$invalidPMcc && $suspected === 'suspected') {
					abr('suspected', true);
					abr('error',$lang['519']);
				}
				else {
					if (!$invalidPMcc) $invalidPMcc = 1; else $invalidPMcc++;
					$cache->set('invPMcc.'.md5($_SERVER['REMOTE_ADDR']), $invalidPMcc, 7*86400);
					abr('error',$lang['466']);
				}
			}
			else
			{
				$_SESSION['pm_hash'] = md5($message_body);
				$query="INSERT INTO `inbox` SET `MSGFROM`='".sql_quote($_SESSION['USERID'])."', `MSGTO`='".sql_quote($msgto)."', `message`='".sql_quote($message_body)."', `FID`='".sql_quote($FID)."', `PID`='".sql_quote($aboutid)."', `time`='".$_SERVER['REQUEST_TIME']."', `delayed`='".($invalidPMcc ? 1 : 0)."', `sent`='".($invalidPMcc ? 0 : 1)."'";
				$result=$conn->execute($query);
				$mid = $conn->_insertid();
				if($mid > 0)
				{
					$query="DELETE FROM `archive` WHERE `AID`='".sql_quote($msgto)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$result=$conn->execute($query);
					
					$query = "SELECT `username`,`email` FROM `members` WHERE `USERID`='".sql_quote($msgto)."'"; 
					$executequery=$conn->execute($query);
					$sendto = $executequery->fields['email'];
					$recipientname = $executequery->fields['username'];
					if($sendto)
					{
						$sendername = stripslashes($_SESSION['USERNAME']);
						if (!$invalidPMcc) {
							$message = $lang['790'];
							$fromname = $config['site_name'];
							$from = $config['site_email'];
							$subject = $lang['437'];
							$sendmailbody = $lang['788'].' '.stripslashes($recipientname).',<br /><br />'.$lang['438']." <a href=\"".$config['baseurl']."/conversations/".$sendername."?mid=".$mid."\">".$sendername."</a>:<br />";
							//$sendmailbody .= $lang['438']."<br /><br />".stripslashes(mb_substr($message_body,0,10,'UTF-8')).(mb_strlen($message_body,'UTF-8')>10 ? '...' : '')."<br /><br />";
							$sendmailbody .= "<a href=\"".$config['baseurl']."/conversations/".$sendername."?mid=".$mid."\">".$config['baseurl']."/conversations/".$sendername."</a><br /><br />";
							$sendmailbody .= $lang['704']."<br>".stripslashes($fromname);
							mailme($sendto,$fromname,$from,$subject,$sendmailbody,$bcc="");
						}
						if ($isPMvalid === 'suspected' || $invalidPMcc) {
							$sendSMS = false;
							send_sms(52, 'suspected', 'administrator/conversations_manage.php?msgfrom='.$sendername);
						}
						else $sendSMS = true;
						send_instant_notification($msgto, 'new_pm', 'conversations/'.$sendername.'?mid='.$mid, $_SESSION['USERID'], true, $sendSMS);
					}
				}
			}
		}
		
			$UID = $msgto;
			$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."')) ORDER BY B.`MID` ASC";
			$results=$conn->execute($query);
			$m = $results->getrows();
			foreach ($m as $mi=>$msg) {
				$msg['message'] = hyperlinkize(urldecode($msg['message']));
				$m[$mi] = $msg;
			}
			abr('m',$m);
	}
}

//TEMPLATES BEGIN
abr('message',$message);
$smarty->display('sendmessage.tpl');
//TEMPLATES END
?>