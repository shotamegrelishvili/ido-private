<?php
exit;
// Not used yet

include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	$UID = intval(cleanit($_SESSION['USERID']));
	$WID = intval(cleanit($_REQUEST['sug']));
	$del = intval(cleanit($_REQUEST['del']));
		
	if($del == "1")
	{
		if($WID > 0)
		{
			$query="DELETE FROM wants WHERE WID='".sql_quote($WID)."' AND USERID='".sql_quote($UID)."' AND active='1'";
			$result=$conn->execute($query);	
			$message = $lang['497'];
		}
	}
	
	
	$s = cleanit($_REQUEST['s']);
	abr('s',$s);
	
	$page = intval($_REQUEST['page']);
	
	if($page=="")
	{
		$page = "1";
	}
	$currentpage = $page;
	
	if ($page >=2)
	{
		$pagingstart = ($page-1)*$config['items_per_page'];
	}
	else
	{
		$pagingstart = "0";
	}
	
	if($s == "dz")
	{
		$dby = "A.WID asc";	
	}
	else
	{
		$dby = "A.WID desc";	
	}
	
	$query1 = "SELECT count(*) as total from wants where active='1' AND USERID='".sql_quote($UID)."' $addsql order by WID desc limit $config[maximum_results]";
	$query2 = "SELECT A.*, C.username from wants A, members C where A.active='1' AND A.USERID='".sql_quote($UID)."' AND A.USERID=C.USERID order by $dby limit $pagingstart, $config[items_per_page]";
	$executequery1 = $conn->Execute($query1);
	$result = $executequery1->fields['total'];
	if ($result > 0)
	{
		if($executequery1->fields['total']<=$config[maximum_results])
		{
			$total = $executequery1->fields['total'];
		}
		else
		{
			$total = $config[maximum_results];
		}
		$toppage = ceil($total/$config[items_per_page]);
		if($toppage==0)
		{
			$xpage=$toppage+1;
		}
		else
		{
			$xpage = $toppage;
		}
		$executequery2 = $conn->Execute($query2);
		$posts = $executequery2->getrows();
		$beginning=$pagingstart+1;
		$ending=$pagingstart+$executequery2->recordcount();
		$pagelinks="";
		$k=1;
		$theprevpage=$currentpage-1;
		$thenextpage=$currentpage+1;
		if($s != "")
		{
			$adds = "&s=$s";
		}
		if ($currentpage > 0)
		{
			if($currentpage > 1) 
			{
				$pagelinks.="<li class='prev'><a href='$thebaseurl/mysuggestions?page=$theprevpage$adds'>$theprevpage</a></li>&nbsp;";
			}
			else
			{
				$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
			}
			$counter=0;
			$lowercount = $currentpage-5;
			if ($lowercount <= 0) $lowercount = 1;
			while ($lowercount < $currentpage)
			{
				$pagelinks.="<li><a href='$thebaseurl/mysuggestions?page=$lowercount$adds'>$lowercount</a></li>&nbsp;";
				$lowercount++;
				$counter++;
			}
			$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
			$uppercounter = $currentpage+1;
			while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
			{
				$pagelinks.="<li><a href='$thebaseurl/mysuggestions?page=$uppercounter$adds'>$uppercounter</a></li>&nbsp;";
				$uppercounter++;
			}
			if($currentpage < $toppage) 
			{
				$pagelinks.="<li class='next'><a href='$thebaseurl/mysuggestions?page=$thenextpage$adds'>$thenextpage</a></li>";
			}
			else
			{
				$pagelinks.="<li><span class='next'>next page</span></li>";
			}
		}
	}
}
else
{
	header("Location:$config[baseurl]/");exit;
}
$templateselect = "mysuggestions.tpl";
//TEMPLATES BEGIN
abr('pagetitle',stripslashes($lang['511']));
abr('message',$message);
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total);
abr('posts',$posts);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>