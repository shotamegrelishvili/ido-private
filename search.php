<?php
include_once('include/config.php');
include_once('include/functions/import.php');
$thebaseurl = $config['baseurl'];
$tag = cleanit(trim($_REQUEST['query']));
$filter = cleanit(trim($_REQUEST['filter']));
//d($tag_shorter);
abr('tag',$tag);
abr('pagetitle',$tag." ".$lang['124']);

$tag = sql_quote($tag);
$s = cleanit($_REQUEST['s']);
abr('s',$s);

$search_in = cleanit($_REQUEST['search_in']);
$c = intval(cleanit($_REQUEST['c']));
abr('c',$c);

abr('searching', true);

if($search_in != "all")
{
	$search_in = "category";
	if($c > 0)
	{
		$query="SELECT `name`,`parent` FROM `categories` WHERE `CATID`='".intval($c)."' LIMIT 1";
		$executequery=$conn->execute($query);
		$cname = $executequery->fields['name'];
		abr('cname',$cname);
		$parent = intval(cleanit($executequery->fields['parent']));
		if($parent == "0")
		{
			$query="SELECT `CATID` FROM `categories` WHERE `parent`='".sql_quote($c)."'";
			$results=$conn->execute($query);
			$searchsc = $results->getrows();
			if(($cs = sizeof($searchsc)) > 0)
			{
				for($i=0; $i<$cs;$i++)
				{
					$ssc .= " OR A.`category`='".sql_quote($searchsc[$i][0])."'";
					$ssd .= " OR `category`='".sql_quote($searchsc[$i][0])."'";
				}
				$result_addcats = "AND (A.`category`='".sql_quote($c)."' $ssc)";
				$result_addcatr = "AND (`category`='".sql_quote($c)."' $ssd)";
			}
			else
			{
				$result_addcats = " AND A.`category`='".sql_quote($c)."'";
				$result_addcatr = " AND `category`='".sql_quote($c)."'";
			}
		}
		else
		{
			$result_addcats = " AND A.`category`='".sql_quote($c)."'";
			$result_addcatr = " AND `category`='".sql_quote($c)."'";
		}
	}
}
$addssc = "&c=$c&search_in=$search_in&filter=$filter";
abr('search_in',$search_in);

$page = intval($_REQUEST['page']);

if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

if($s == "r")
{
	$dby = "A.rating desc, A.`rcount` desc";	
}
elseif($s == "rz")
{
	$dby = "A.rating asc";	
}
elseif($s == "p")
{
	$dby = "A.viewcount desc";	
}
elseif($s == "pz")
{
	$dby = "A.viewcount asc";	
}
elseif($s == "c")
{
	$dby = "A.price asc";	
}
elseif($s == "cz")
{
	$dby = "A.price desc";	
}
elseif($s == "dz")
{
	$dby = "A.PID asc";	
}
else
{
	$dby = "A.PID desc";	
}

if($s == "ez")
{
	$dby = "A.PID asc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}
elseif($s == "e")
{
	$dby = "A.PID desc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}

$p = intval(cleanit($_REQUEST['p']));
if($p > 0)
{
	$result_addprice = " AND A.price='".sql_quote($p)."'";
	$result_addpriced = " AND price='".sql_quote($p)."'";
	abr('p',$p);
	$addp = "&p=$p";
}	

if ($filter == 'tags') {
	$dbx = ', CASE WHEN `gtags` LIKE "%'.$tag.'%" THEN 1 ELSE 0 END as `tagMatch`';
	$dbz = '`tagMatch` DESC, ';
}
else
	$dbx = $dbz = '';

if ($tag == geo2lat($tag) && strpos($tag, ' ')===FALSE) {
	$addmembersearch = ' OR C.`username` = "'.sql_quote(cleanit(trim($_REQUEST['query']))).'"';
}
else $addmembersearch = '';

require_once($config['basedir'].'/models/search.Class.php');
$keywords = $searchClass->keywordsNormalize($searchClass->prepareKeywords($tag));
$matchAgainst = implode('* ',$keywords);

$query1 = "SELECT count(P.`PID`) as total FROM `posts` P, `members` C WHERE P.`USERID` = C.`USERID` AND P.`active`='1' AND P.`is_del`='0' AND (MATCH(P.`gtitle`, P.`gdesc`, P.`gtags`) AGAINST ('".sql_quote($matchAgainst)."*' IN BOOLEAN MODE)".$addmembersearch.") $addsql $result_addpriced $result_addcatr ORDER BY P.`PID` DESC LIMIT $config[maximum_results]";
$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`country`, C.`toprated`, C.`profilepicture`, C.`rating`".$dbx." FROM `posts` A, `categories` B, `members` C WHERE A.`active`='1' AND `is_del`='0' AND (MATCH(A.`gtitle`, A.`gdesc`, A.`gtags`) AGAINST ('".sql_quote($matchAgainst)."*' IN BOOLEAN MODE)".$addmembersearch.") $addsqlb AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $result_addprice $result_addcats ORDER BY ".$dbz."A.`feat_mainpage` DESC, A.`feat_cat` DESC, A.`feat` DESC, $dby LIMIT $pagingstart, ".($config[items_per_page]+20);
$executequery1 = $conn->Execute($query1);
$result = $executequery1->fields['total'];

if ($result > 0)
{
	if($executequery1->fields['total']<=$config[maximum_results])
	{
		$total = $executequery1->fields['total'];
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if($s != "")
	{
		$adds = "&s=$s".$addp;
	}
	if ($currentpage > 0)
	{
		if($currentpage > 1) 
		{
			$pagelinks.="<li class='prev'><a href='$thebaseurl/search?query=$tag&page=$theprevpage$adds".$addssc."'>$theprevpage</a></li>&nbsp;";
		}
		else
		{
			$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
		}
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<li><a href='$thebaseurl/search?query=$tag&page=$lowercount$adds".$addssc."'>$lowercount</a></li>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<li><a href='$thebaseurl/search?query=$tag&page=$uppercounter$adds".$addssc."'>$uppercounter</a></li>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<li class='next'><a href='$thebaseurl/search?query=$tag&page=$thenextpage$adds".$addssc."'>$thenextpage</a></li>";
		}
		else
		{
			$pagelinks.="<li><span class='next'>next page</span></li>";
		}
	}
}
$templateselect = "search.tpl";

//TEMPLATES BEGIN
abr('message',$message);
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('itemsPerPage',$config[items_per_page]);
abr('total',$total);
abr('posts',$posts);
if (!defined('NODISPLAY')) {
	$smarty->display('header.tpl');
	$smarty->display($templateselect);
	$smarty->display('footer.tpl');
}
//TEMPLATES END
?>