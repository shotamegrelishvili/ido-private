var ind = [];
ind['deactivate'] = true;
var orderRow=$('.modcontrol-order-container').clone();

$('.deal-view-modcontrol').click(function(){
	if (ind['deactivate'] && !$(this).hasClass('disabled') && $(this).data('needs-explanations')) {
		$('.deal-view-modcontrol').not('#deal-view-modcontrol-submit').css('opacity','0.2');
		$(this).css('opacity','1');

		show_explanation_window($(this).data('control'));
		return false;
	}
	if (!$(this).hasClass('disabled') && ($(this).data('noconfirm') || confirm(lang['sure']))) {
		$.post(baseurl+'/include/functions/ajax/dealProcessor.php', {'PID': PID, 'ACTION': $(this).data('control'), 'MSG': $('#message').val()},function(data){updatePanel(data)}, 'json');
	}
});

function updatePanel(data){
	if (data.status == 'OK') {
		$('#send-message-form').hide();
		if (data.fireafter) {
			window[data.fireafter](data.rows);
		}
		else {
			$('#panel-notify').text('OK').fadeIn();
			$('.deal-view-modcontrol').removeClass('success').removeClass('orange').removeClass('alert').removeClass('primary-alt').addClass('disabled').addClass('secondary').css('opacity','0.2');
		}
	}
	else {
		ind['deactivate'] = true;
		alert('Error: '+data.MSG);
	}
}

function show_explanation_window(control) {
	$('#message').val(lang[control]);
	$('#deal-view-modcontrol-submit').data('control',control);
	$('#send-message-form').slideDown();
	ind['deactivate'] = false;
}

function showOrders(rows) {
	var i=0;
	var htmlCode = '';
	$.each(rows, function() {
		row = orderRow;
		row.find('.modcontrol-oid').html('<a href="'+baseurl+'/track?id='+this.OID+'">'+this.OID+'</a>');
		row.find('.modcontrol-username').html('<a href="'+baseurl+'/user/'+this.username+'">'+this.username+'</a>');
		row.find('.modcontrol-status').html('<a href="javascript:" class="button small round secondary mb0">'+status_matrix[this.status]+'</a>');
		row.find('.modcontrol-time-added').html(this.time_added);
		row.find('.modcontrol-pr').html(this.pending_request);
		htmlCode += row.html();
	})
	$('#modcontrol-orders').append(htmlCode).slideDown();
	$('.modcontrol-orders').removeClass('primary-alt').addClass('disabled').addClass('secondary').css('opacity','0.2');
}