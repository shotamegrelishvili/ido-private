$('.message-remove').click(function() {
	if (confirm('Are you sure?')) {
		$.getJSON(baseurl+'/include/functions/ajax/removeContactMessage.php', {'MID': $(this).data('mid')},function(data){processMessageRemove(data)});
	}
})

$('.message-reply').click(function() {
	var mid = $(this).data('mid'), text;
	$('.replyforms').hide();
	$('.contact-row').css('opacity',0.3);
	$('#row-'+mid).css('opacity',1);
	$('#replyform-'+mid).show();
	text = $('#text-'+mid).data('title');
	var reply = replytemplate.replace('%username%', $('#user-'+mid).text()).replace('%message%', text);
	$('#reply-text-'+mid).val(reply);
})

$('.close-replyform').click(function() {
	$('#replyform-'+$(this).data('mid')).hide();
	$('.contact-row').css('opacity',1);
})

$('.has-tip').mouseover(function() {
	if (!$(this).data('title')) {
		$(this).data('title', $(this).attr('title'));
	}
})

function processMessageRemove(data) {
	if (data.status == 'OK') {
		$('#row-'+data.ID).fadeOut('fast');
	}
}