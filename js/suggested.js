$('.btn-delete').click(function() {
	if (confirm(lang['sure'])) {
		//$('#deals_form').attr('action',base_url+'/manage_deals?delete=1');
		$('#delete_suggested-'+$(this).data('wid')).submit();
	} else {
		return false;
	}
});

$('.ta6-anim').on('keyup', function() {
	var th = $(this).height();
	var ts = $(this).get(0).scrollHeight;
	if (ts - th > 40)
		$(this).height(ts);
});

$('#wants-helper').click(function(){
	$('#wants-type-selector-suggest').slideUp();
});

$('.slick-slider').slick({
	dots: true,
	infinite: true,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 4000,
});

$('.fr-text-field').focus(function(){
	$('.slick-slider').slick('slickPause');
})

$('.fr-remove-filter').hover(function(){
	$(this).children('i.icon-toggle').toggleClass('icon-check').toggleClass('icon-check-empty');
},function(){
	$(this).children('i.icon-toggle').toggleClass('icon-check').toggleClass('icon-check-empty');
});

// AutoComplete for experience
var acST, acBT, acET, acRow=0, acCalled=false;
$('.auto-complete').keydown(function(e){
	var k=e.keyCode;
	if (k == 13) {
		if ($('.ac-container .ac-row-hover').length) {
			$('.ac-row.ac-row-hover').trigger('click');
			$('.auto-complete').trigger('blur');
		}
		else {
			return;
		}
		return false;
	}
	else if ((k == 38 || k == 40) && $('.ac-container .ac-row').length) {
		if (k == 40) {
			acRow++;
			var loop = 1;
		}
		else {
			acRow--;
			var loop = $('.ac-row').length-1;
		}
		if (!$('.ac-container .ac-row:nth-child('+acRow+')').length) {
			acRow=loop;
		}
		$('.ac-container .ac-row').removeClass('ac-row-hover');
		$('.ac-container .ac-row:nth-child('+acRow+')').addClass('ac-row-hover');
		return false;
	}
})

$('.auto-complete').keyup(function(e){
	if (e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 40) return false;
	var t = $(this);
	if (t.val().length < t.data('minlength')) return;
	else {
		clearTimeout(acST);
		acST = setTimeout(function(){
			window[t.data('target')](t.val());
		},400);
	}
});
$('.ac-container').on('click','.ac-row',function(){
	acRow=0;
	var e = $(this);
	//d(e.data('mid'))	d(e.data('aid'))
	$('.auto-complete').val(e.data('k')+(e.data('a') ? e.data('a') : ''));
	$('.fr-mid').val(e.data('mid'));
	$('.fr-aid').val(e.data('aid'));
	document.forms[$('.auto-complete').data('form')].submit();
})
$('.auto-complete').blur(function(){
	acBT = setTimeout(function(){
		$('.ac-container').removeClass('ac-container-show');
		$('.experience-col').removeClass('exp-active');
		acRow=0;
	},200)
});
$('.auto-complete').focus(function(){
	clearTimeout(acBT);
	if (!acCalled) {
		var t = $(this);
		window[t.data('target')](t.val());
	}
	if ($(this).val())
		$('.ac-container').addClass('ac-container-show');
});
function experience(value) {
	$.post(base_url+'/include/functions/ajax/exp.php', { action: 'getClosestValues', data: value }, function(data){
		if (data.status == 'success') {
			var r = data.MSG;

			if (!Object.keys(r).length) {
				showExpError(lang.noversions,4000);
				return;
			}
			var row = $('.ac-tmpl').html(), k, a, kID, aID;
			$('.ac-container').html('').addClass('ac-container-show');
			for (var i in r) {
				a = '', aID = '';
				if (typeof r[i].KID != 'undefined') {
					k = r[i].keyword;
					kID = r[i].KID;
					$('.ac-container').append(row.replace(/%k%/g,k).replace(/%a%/g,'').replace('%kID%',kID).replace('%aID%',''));
				}
				if (typeof r[i].aliases != 'undefined') {
					for (var al in r[i].aliases) {
						a = ': '+r[i].aliases[al].alias;
						aID = r[i].aliases[al].ALIASID;
						$('.ac-container').append(row.replace(/%k%/g,k).replace(/%a%/g,a).replace('%kID%',kID).replace('%aID%',aID));
					}
				}
				
			}
		}
		else if (data.MSG) {
			if (data.status == 'error')
				showExpError(data.MSG, 4000);
			else if (data.status == 'halt')
				showExpError(data.MSG, 60000);
		}
	})
}

function showExpError(text,hideTimerout) {
	if ($('.exp-error').is(':visible')) return;
	clearTimeout(acET);
	$('#exp-label').hide();
	$('.exp-error').html(text).addClass('exp-error-shown').hide().fadeIn('slow',function() {
		acET = setTimeout(function(){
			$('.exp-error').removeClass('exp-error-shown').fadeOut(function(){
				$('#exp-label').show();
			});
		},hideTimerout);
	});
}

$('.fr-checkbox-toggle').click(function(){
	if ($(this).hasClass('fr-checkbox-on-toggle')) {
		$('.fr-checkbox-on').addClass('hide');
		$('.fr-checkbox-off').removeClass('hide');
		$('#fr_selected_freelancers').val(',');
	}
	else {
		$('#fr_selected_freelancers').val(',');
		$.each($('.fr-checkbox-on'),function(index){
			$('#fr_selected_freelancers').val($('#fr_selected_freelancers').val()+$(this).data('userid')+',');
		})
		$('.fr-checkbox-off').addClass('hide');
		$('.fr-checkbox-on').removeClass('hide');	
	}
	$('.fr-checkbox-toggle').toggleClass('hide');
});

$('.fr-checkbox-r').click(function(){
	if ($(this).hasClass('fr-checkbox-on')) {
		$('#fr_selected_freelancers').val($('#fr_selected_freelancers').val().replace(','+$(this).data('userid')+',',','));
		$(this).addClass('hide');
		$('#fr-c-off-'+$(this).data('userid')).removeClass('hide');
	}
	else {
		$('#fr_selected_freelancers').val($('#fr_selected_freelancers').val()+$(this).data('userid')+',');
		$(this).addClass('hide');
		$('#fr-c-on-'+$(this).data('userid')).removeClass('hide');
	}
})

$('.fr-new-task-btn').click(function(){
	//$('.fr-list-nf-wrapper').show();
	$('.fr-list-nf-wrapper').slideDown(400);
	$('.fr-hide-on-new-task').hide();
	//$('.fr-new-task').slideDown();
	//$.scrollTo($('.fr-new-task').offset().top,100)});
	
	$.each($('.fr-checkbox-on'),function(index){
		if (!$(this).hasClass('hide')) {
			userid = $(this).data('userid');
			$('.fr-list-notified-freelancers').append('<li class="fr-u-wrapper-'+userid+'"></li>');
			$('.fr-av-'+userid).clone().appendTo('.fr-u-wrapper-'+userid);
			$('.fr-username-'+userid).clone().appendTo('.fr-u-wrapper-'+userid);
		}
	})
})

$('.fr-nt-next').click(function(){
	$('.fr-new-task-slide-'+$(this).data('slide')).removeClass('fr-nt-slide-visible').addClass('fr-nt-slide-hidden');
	$('.fr-new-task-slide-'+($(this).data('slide')+1)).removeClass('fr-nt-slide-hidden').addClass('fr-nt-slide-visible');
})