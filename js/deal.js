function updateTitleCharsCount() {
		var used = $('#title').val().length;
		$('.titleused').html(used);
};
function updateDescCharsCount() {
		var used = $('#desc').val().length;
		$('.descused').html(used);
};
function testFormErrors() {
	var formHasError=false;
	$('input,textarea,select').each(function() {
		//console.log($(this));
		if ($(this).hasClass('field-error-notice')) {
			//console.log('1')
			formHasError=true;
			return false;
		}
		else if ($(this).attr('required') && !$(this).val()) {
			//console.log('2')
			formHasError=true;
			$(this).addClass('field-err');
			return false;
		}
	});
	if (!formHasError && !$('#photo-i1').val() && !$('#photobase64-i1').val() && !$('#photo-indb-i1').val()) {
		//console.log('3')
		formHasError=true;
		$('.photos-block').addClass('field-err');
		$('.send-button').attr('disabled','disabled').removeClass('success').addClass('secondary');
		$('.form-error-notice').fadeIn('slow');
	}
	else if (!formHasError) {
		$('.form-error-notice').hide();
		$('.photos-block').removeClass('field-err');
		$('.send-button').removeAttr('disabled').addClass('success');
	}
	else {

	}
}

$(document).ready( function() {
	if ($('#title').length != 0) {
		updateTitleCharsCount();
		$('#title').keyup(function(){
				updateTitleCharsCount();
		});
	};
	if ($('#desc').length != 0) {
			updateDescCharsCount();
			$('#desc').keyup(function(){
					updateDescCharsCount();
			});
	};

	$('input,textarea').on('blur', function() {
		var s = $(this).data('minsize')
		if (s) {
			if ($(this).val().length < s) {
				showFieldError($(this),lang['minsize']+s);
			}
		else {
				testFormErrors();
			}
		}
	});
	function showFieldError(e, msg) {
		if (e.hasClass('field-err')) return;
		e.addClass('field-err mb0');
		e.after('<span class="red subheading mt2 field-error-notice">'+msg+'</span>');
		$('.send-button').attr('disabled','disabled').removeClass('success').addClass('secondary');
		$('.form-error-notice').fadeIn('slow');
	}
	$('input#price').on('blur', function() {
		var p = $(this).val();
		if (p < 5) {
			showFieldError($(this),lang['minprice']);
		}
		else if (p > 995) {
			showFieldError($(this),lang['maxprice']);
		}
		else if (p.indexOf('.')>-1 || p.indexOf(',')>-1) {
			showFieldError($(this),lang['onlyinteger']);
		}
	});
	$('input,textarea,select').on('keydown', function(){
		$(this).removeClass('field-err');
		$(this).nextAll('.field-error-notice').remove();
	})

	$('input#video').on('keyup', function(){
		checkVideoUrl($(this).val());
	}).on('mouseup', function(){
		checkVideoUrl($(this).val());
	})
});

function checkVideoUrl(url){
	//console.log(url.indexOf('myvideo'))
	$('#videohost,#videofile').val('');
	if (url && url.indexOf('myvideo.ge')>=0 && url.indexOf('?video_id=')>0) {
		var vA = url.split('?video_id=');
		var v = parseInt(vA[1]);
		if (v > 0) {
			$('#video').removeClass('field-err').nextAll('.field-error-notice').remove();
			$('#videohost').val('myvideo');
			$('#videofile').val(v);
		}
	}
	else if (url && url.indexOf('youtube.com')>=0 && url.indexOf('watch?v=')>0) {
		var vA = url.split('watch?v=');
		if (vA[1].length == 11) {
			$('#video').removeClass('field-err').nextAll('.field-error-notice').remove();
			$('#videohost').val('youtube');
			$('#videofile').val(vA[1]);
		}
	}
	else if (url && !$('#video').hasClass('field-err')) {
		// Error
		$('#video').addClass('field-err mb0').after('<span class="red subheading mt2 field-error-notice">'+lang['videohost']+'</span>');
	}
	else if (!url) {
		$('#video').removeClass('field-err').nextAll('.field-error-notice').remove();
	}
}

$('.wants-work').click(function(){
	$('#wants-type-selector,#wants-type-selector-small').slideUp(function(){
		if (!$('#new-deal').is(':visible'))
			$('#place-deal-header,#new-deal').slideDown(function(){
				$('.tour-wrapper').show();
				//if (startNewDealTour) NewDealTour();
			});
	});
})

$('#price').change(function(){
	// for free deal
	if ($(this).val() == '6' && !$(this).data('anyprice')) {
		$('#price-desc').css('display','block')
	}
	else {
	 $('#price-desc').css('display','none') 
	}
})

if (!supports_video()) {
	$('.novideo-bg').show();
}

function supports_video() {
	return !!document.createElement('video').canPlayType;
}

if (startNewDealTour && $('#place-deal-header').is(':visible')) {
	NewDealTour();
}

function NewDealTour(){
	$('#newdeal-info-tour li').hide();
	$('#newdeal-info-tour li:first-child').show();
	$('.tour-progress-bar div').removeClass('bg-green').addClass('bg-gray');
	$('.tour-progress-bar div:first-child').removeClass('bg-gray').addClass('bg-green');
	$('#newdeal-h2,#call-newdealtour-wrapper').hide();
	$('.tour-wrapper').slideDown();
	$('#new-deal').css('opacity',0.2);
	$('.info-tour-next').show();
	$('.form-reveal').removeClass('orange').addClass('secondary');
/*
	$('#newdeal-info-popup').foundation('joyride', 'start', {postRideCallback:function(){
		if (!$('#call-newdealtour-wrapper').is(':visible')) {
			$('#call-newdealtour-wrapper').fadeIn('slow');
		}
	}});
*/
}

$('#call-newdealtour').click(function(){
	$('.joyride-tip-guide').remove();
	NewDealTour();
})

if (dealSubmittedTour) {
	$('#deal-submitted-popup').foundation('joyride', 'start',{
		next_button: false,
		prev_button: false,
		tip_animation_fade_speed: 150,
		scroll_speed: 300
	});
}

$('.newdeal-help').on('click',function(){
	$('.joyride-tip-guide').remove();
	var g=$(this).data('goal');
	$('#'+g+'-info-popup').foundation('joyride', 'start',{
		next_button: false,
		prev_button: false,
		tip_animation_fade_speed: 150,
		scroll_speed: 300
	});
	activeTip=g;
})

$('.info-tour-next').click(function() {
	var tour=$(this).data('tour');
	var l=$('#'+$(this).data('tour')+'-info-tour').children().length;
	var active=1;
	$.each($('#'+tour+'-info-tour').children(), function(index) {
		if ($('#'+tour+'-info-tour li:nth-child('+(index+1)+')').is(':visible')) {
			if ($('#'+tour+'-info-tour li:nth-child('+(index+2)+')').length) {
				active=index+2;
				$('#'+tour+'-info-tour li:nth-child('+(index+1)+')').slideUp(function(){
					$('#'+tour+'-info-tour li:nth-child('+(index+2)+')').slideDown();
					$('.tour-progress-bar div:nth-child('+(index+2)+')').removeClass('bg-gray').addClass('bg-green');
				});
			}
		}
		if (active==l) {
			$('.info-tour-next').hide();
			$('.form-reveal').removeClass('secondary').addClass('orange');
		}
	})
	//console.log($('#'+$(this).data('tour')+'-info-tour').children().length)
})

$('.form-reveal,#new-deal').click(function() {
	$('#newdeal-h2,#call-newdealtour-wrapper').show();
	$('.tour-wrapper').slideUp();
	$('#new-deal').css('opacity',1);
})

// More params clicked
$('.nd-more-params').click(function() {
	$('.hide-nd-more-params').slideUp();
	$('.unhide-nd-more-params').slideDown();
})