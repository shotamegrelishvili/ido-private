var has_unsaved_changes = false, uploadErrorTimer, uploadErrorTimeout=4;

// Show tips
$('.settings-help').on('click',function(){
	$('.joyride-tip-guide').remove();
	var g=$(this).data('goal');
	$('#'+g+'-info-popup').foundation('joyride', 'start',{
		next_button: false,
		prev_button: false,
		tip_animation_fade_speed: 150,
		scroll_speed: 300
	});
	activeTip=g;
})

// Align all portfolio images
/*
function imageAlign() {
	var imgContainerHeight = $('.plupload_file .plupload_file_thumb').height();
	if (imgContainerHeight) {
		$('.plupload_file').each(function(index){
			var i = $(this).find('img');
			if (!i.prop('aligned') && (h=i.height())) {
				if (h > imgContainerHeight) {
					i.css('margin-top',-1*(h - imgContainerHeight)/2).prop('aligned',1);
				}
				else {
					i.css('margin-top',(imgContainerHeight - h)/2).prop('aligned',1);
				}
			}
		})
	}
}
*/

if (typeof fireFunc != 'undefined' && typeof window[fireFunc] == 'function')
	window[fireFunc]();

$("#uploaded-portfolio").sortable({ opacity: 0.6, cursor: 'move', update: function(e) {
		var order = $(this).sortable("serialize");
		var orderArr = $(this).sortable("toArray", {attribute: "data-id"});
		
		var json_params = JSON.stringify(orderArr, null, 2);
		$.post(base_url+'/include/functions/ajax/portfolIO.php', { action: 'sort', data: json_params }, function(data){
			if (data.MSG) {
				$('.wa-message').text(data.MSG).fadeIn(function(){
					setTimeout(function(){
						$('.wa-message').fadeOut().text('');
					}, 3000)
				});
			}
		});
	}
}).disableSelection();

// Remove from portfolio and update PLUPLOAD max_file_count
$('.plupload_content').on('click','.wa-file-remove',function(){
	var FID = $(this).data('fid'), ID = $(this).data('id');
	json_params = JSON.stringify([FID], null, 2);
	$.post(base_url+'/include/functions/ajax/portfolIO.php', { action: 'remove', data: json_params }, function(data){
		if (data.status == 'success') {
			$('#'+ID).fadeOut('slow',function(){
				$(this).remove();
				if (!$('.plupload_file').length) {
					$('#pfile-req').val('');
					fieldProcessor.Init();
					circleProgress.updateProgress(fieldProcessor.filledFields/fieldProcessor.totalFields*100);
				}
			});
			var uploader = $('.plupload-block').plupload('getUploader');
			uploader.setOption('max_file_count',uploader.getOption('max_file_count')+1);
			uploader.disableBrowse(false);
		}
		if (data.MSG) {
			waPopup.show(data.MSG, data.status);
		}
	})
})

// Don't let user to leave w/o saving if there are changes in the form
$('#edit_user input').on('change', function(){
	checkLeave = true;
	has_unsaved_changes = true;
})

// Show the tab, if there is an error in some field
$('.field-err').each(function(index){
	if (!$(this).is(':visible')) {
		$(this).parents('.wa-target').show();
	}
})


$('.submit-wrapper').on('click','.next-button',function(e){
	e.preventDefault();
	var hasError = false;
	$('.form-tab-active input, .form-tab-active select, .form-tab-active textarea').each(function(index) {
		if (($(this).attr('required') == 'required' && $(this).val() == '') || (typeof $(this)[0].validationMessage != 'undefined' && $(this)[0].validationMessage)) {
			$(this).addClass('field-err')
			hasError=true;
		}
		else {
			$(this).removeClass('field-err')
		}
	});
	if (hasError || $('.field-err').length>0) {
		$('.field-err').get(0).focus(function(){
			return false;
		});
	}
	else {
		firePost = fieldProcessor.countFilledFields();

		if (firePost) {
			var postStatus;
			$.post(base_url+'/include/functions/ajax/settingsIO.php', { action: 'save', data: JSON.stringify(fieldProcessor.params, null, 2) }, function(data){
				postStatus = data.status;
				if (data.MSG) {
					if (data.status == 'success') {
						waNext();
					}
					else {
						for (var f in data.MSG) {
							$('#'+f).data('initial','');
						}
						fieldProcessor.decreaseFFcount();
					}
					waPopup.show(data.MSG, data.status);
				}
			});
		}
		else {
			waNext();
		}

		setTimeout(function(){
			p = circleProgress.updateProgress(fieldProcessor.filledFields/fieldProcessor.totalFields*100);
			if (currentProfileProgress < 100 && p == 100 && postStatus=='success') {
				softSubmitNeeded();
			}
			$('.sp-progress').text(p);
		},300);
	}
});

function softSubmitNeeded() {
	checkLeave=0;
	loader.Show($('.next-button'));
	setTimeout(function(){
		window.location.href = base_url+'/settings?tab=' + (currentTab ? currentTab : 'value-added');
	}, 1500);
}


fieldProcessor = {
	totalFields : $('.req-cc').length,
	filledFields : 0,
	params : {},
	changeFlag : false,

	Init : function() {
		this.countFilledFields();
	},

	updateChangeFlag : function(hasChange) {
		this.changeFlag = hasChange;
	},

	increaseFFcount : function() {
		this.filledFields++;
		return this.filledFields;
	},

	decreaseFFcount : function() {
		this.filledFields--;
		return this.filledFields;
	},

	countFilledFields : function() {
		var S=this;
		S.filledFields = 0;
		$('.req-cc').each(function(){
			if ($(this).val() && 
				(!$(this).data('emptyif') || $(this).data('emptyif')!=$(this).val())) {
				S.filledFields++;
				var attr = $(this).attr('data-initial');
				//d(typeof(attr)+' : '+$(this).val());
				if (typeof attr !== typeof undefined && attr !== false && $(this).val()!=$(this).data('initial')) {
					S.params[$(this).attr('name')] = $(this).val();
					$(this).data('initial', S.params[$(this).attr('name')]);
					S.updateChangeFlag(true);
				}
			}
			else if ($(this).data('initial') && (!$(this).data('emptyif') || $(this).data('initial')!=$(this).data('emptyif'))) {
				S.filledFields++;
			}
		});
		return S.changeFlag;
	}
}


// Control Tabs switcher
function waNext() {
	$('.wa-switcher.wa-sb-active')
		.parent()
		.next()
		.children('.wa-switcher')
		.trigger('click');
	if (!$('.wa-switcher.wa-sb-active').parent().next().children('.wa-switcher').length) {
		buttonChange('submit');
	}
	else {
		buttonChange('next');
	}
}

function buttonChange(change) {
	if (change == 'next') {
		$('.send-button').addClass('next-button').removeClass('send-button').val(lang.next);
	}
	else {
		$('.next-button').addClass('send-button').removeClass('next-button').val(lang.submit);
	}
}

$('.wa-switcher').click(function() {
	$('.joyride-tip-guide').remove();
	var target = $(this).data('target');
	var afterFireFunc = $(this).data('afterfire');
	if ($(this).hasClass('wa-sb-active')) {
		tT = $('.'+target).offset().top-80;
		$.scrollTo(tT,150);
		return false;
	}
	else {
		$('.wa-switcher').removeClass('wa-sb-active');
		$('.wa-target').removeClass('form-tab-active').hide();
		$('.'+target)
			.addClass('form-tab-active')
			.slideDown(function(){
			$(this).css('overflow','visible');
			if (typeof window[afterFireFunc] == 'function')
				window[afterFireFunc]();
			tT = $('.'+target).offset().top-80;
			$.scrollTo(tT,150);
		});
		$(this).addClass('wa-sb-active');
		if (!$(this).parent().next().children('.wa-switcher').length) {
			buttonChange('submit');
		}
		else {
			buttonChange('next');
		}
	}
})

$('#emoney:enabled').focus(function() {
	$(this).removeClass('field-err');
	if (eMoneyWidgetEnable) {
		$('#eMoneyAuthorizeWidgetDiv').show();
		$('#eMoneyAuthorizeWidgetFrame').show();
		client.callOut('focusElement','first');
	}
	else {
		$.scrollTo(0);
		$(document).foundation('joyride', 'start');	
	}
});

$('.joyride-close-tip').live('click',function() {
	if (!$('#emoney').val()) {
		$('#emoney').addClass('field-err');
	}
	$('.econnect-act-btn').removeClass('secondary').addClass('alert').css('opacity','1');
})

$('.econnect-btn').live('click',function() {
	checkLeave = 0;
	if ($('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'')
		$(this).css({'background-color':'#e9e9e9','border-color':'#d0d0d0'}).removeClass('econnect-btn');
		$('#econnect_signup').val('1');
		$('#econnect_email').val($('.joyride-tip-guide .eConnect-email').val());
		$('#econnect_password').val($('.joyride-tip-guide .eConnect-password').val());
		$('#edit_user').submit();
})

$('#pin').on('keyup blur change', function(){
	if ($(this).val().length == 11) $(this).removeClass('field-err');
	else $(this).addClass('field-err');
});

document.onkeypress=function(e){
	if (e.keyCode == 13) {
		if ($('.joyride-tip-guide').is(':visible') && $('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'') {
			$('.econnect-btn').click();
		}
	}
};

$(document).keyup(function(e) {
	if (e.keyCode == 27 && $('.joyride-tip-guide').is(':visible')) { 
		$('.joyride-close-tip').click();
	}
});

// Don't let the user leave the window on some criterias
window.onbeforeunload = function (e) {
	if (!checkLeave) return;
	var messageKey;
	$('input').each(function(index) {
		if ($(this).data('restrict-leave') && ($(this).val()=='' && $(this).data('value')=='') || ($(this).val()!='' && $(this).data('value')=='')) {
			messageKey = $(this).data('restrict-leave');
		}
	})

	if (has_unsaved_changes)
		messageKey = 'has_unsaved_changes';

	if (!messageKey) return;
	var message = confirmLeave[messageKey],
	e = e || window.event;
	if (e) {
		e.returnValue = message;
	}
	return message;
}

// Start the Joyride intro
if (startImportantInfoTour) {
	//$('#important-info-popup').foundation('joyride', 'start');
}

// PLUPLOAD: Start plupload Upload on .start-upload button click
$('.submit-wrapper').on('click','.start-upload',function(e){
	e.preventDefault();
	$('.plupload_buttons').hide();
	$('.plupload_started').show();
	$('.plupload_file_thumb').addClass('plupload_upload_started');
	$("#portfolio").plupload('start');
	return false;
})

// PLUPLOAD: on file add Routine
// 	Don't let the user leave without saving
function plupload_onfile_add(filesNumber) {
	$('.next-button').addClass('start-upload').data('next',1).removeClass('next-button');
	has_unsaved_changes = true;
	checkLeave = true;
	clearTimeout(uploadErrorTimer);
}

// PLUPLOAD: on complete upload Routine
// 	Submit the form on upload complete and make portfolio tab default
function plupload_oncomplete_upload() {
	if (!$('#plupload_console').is(':visible')) {
		$('#action-tab').val('portfolio');
		if ($('.start-upload').data('next')) {
			if (!uploadErrorTimer) {
				$('.start-upload').addClass('next-button').removeClass('start-upload');
				waNext();
				waPopup.show(lang.success, 'success');
			}
			$('.plupload_buttons').show();
			if (!$('#pfile-req').val()=='1') {
				$('#pfile-req').val('1');
				fieldProcessor.Init();
				p = circleProgress.updateProgress(fieldProcessor.filledFields/fieldProcessor.totalFields*100);
				$('.sp-progress').text(p);
				if (currentProfileProgress < 100 && p == 100) {
					softSubmitNeeded();
				}
			}
		}
		else {
			$('#edit_user').submit();
		}
	}
}

// PLUPLOAD: on upload progress Routine
// 	Changes the opacity of thumbs on upload
function plupload_onupload_progress(id, percent) {
	var opacity = percent/100;
	$('#'+id+' .plupload_file_thumb').css('opacity', opacity);
}

// PLUPLOAD: on File uploaded
function plupload_onfile_uploaded(file, r) {
	$('#'+file.id+' .thumb-file-action-icon.ui-icon-circle-check').addClass('wa-file-remove').data('id',file.id).data('fid',r.fid).removeClass('plupload_action_icon');
}

// PLUPLOAD: on upload error Routine
//	Shows the error and returns to Upload Button in 4 seconds
function plupload_onupload_error(file, error) {
	var fid = file.id,
		quote = file.name,
		errorMessage = error.message;

	$('.plupload_buttons,.plupload_upload_status,.progress-indicator-icon-message').hide();
	$('#plupload_console').fadeIn();
	$('.start-upload').show();
	clearTimeout(uploadErrorTimer);
	document.getElementById('plupload_console').innerHTML += "\n&quot;" + quote + "&quot;: " + errorMessage + "<br>";

	uploadErrorTimer = setTimeout(function(){
		$('#plupload_console').fadeOut(function(){
			$('.plupload_buttons').show();
			document.getElementById('plupload_console').innerHTML = '';
			if ($('.start-upload').data('next')) {
				$('.start-upload').data('next',0).addClass('next-button').removeClass('start-upload');
			}
		});
	},uploadErrorTimeout*1000)
	uploadErrorTimeout += 1;
}

// AutoComplete for experience
var acST, acBT, acET, acRow=0;
$('.auto-complete').keydown(function(e){
	var k=e.keyCode;
	if (k == 13) {
		if ($('.ac-container .ac-row-hover').length) {
			$('.ac-row.ac-row-hover').trigger('click');
			$('.auto-complete').trigger('blur');
		}
		return false;
	}
	else if ((k == 38 || k == 40) && $('.ac-container .ac-row').length) {
		if (k == 40) {
			acRow++;
			var loop = 1;
		}
		else {
			acRow--;
			var loop = $('.ac-row').length-1;
		}
		if (!$('.ac-container .ac-row:nth-child('+acRow+')').length) {
			acRow=loop;
		}
		$('.ac-container .ac-row').removeClass('ac-row-hover');
		$('.ac-container .ac-row:nth-child('+acRow+')').addClass('ac-row-hover');
		return false;
	}
})

$('.auto-complete').keyup(function(e){
	if (e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 40) return false;
	var t = $(this);
	if (t.val().length < t.data('minlength')) return;
	else {
		clearTimeout(acST);
		acST = setTimeout(function(){
			window[t.data('target')](t.val());
		},400);
	}
});
$('.ac-container').on('click','.ac-row',function(){
	acRow=0;
	var e = $(this);
	var hasErr = false;
	var expAlreadyChosen = $('.experience-col').each(function(index){
		if ($(this).data('exp') && $(this).data('exp') == e.data('mid')+'-'+e.data('aid')) {
			$('.auto-complete').val('');
			showExpError(lang.expAlreadyChosen,4000);
			hasErr = true;
			return false;
		}
	});
	if (hasErr) return;
	var containerNumber = $('.exp-h').data('target');
	$('#exp-'+$('.exp-h').data('target')+' .exp-sub').html(e.html()).parent().data('exp',e.data('mid')+'-'+e.data('aid')).removeClass('exp-empty').addClass('exp-chosen');
	var expValue = 'KID='+e.data('mid');
	if (e.data('aid')) {
		expValue = expValue + '|' + 'AID='+e.data('aid');
	}
	$('#exp-i-'+containerNumber).val(expValue);

	// Needed for Instant Save
	var expArr = $('input[name="exp[]"')
		.map(function() {
			return $(this).val();
		})
		.get()
		.join();
	$('#exp-arr').val(expArr);
	// Needed for Instant Save

	if ($('#exp-'+(containerNumber+1)).length) {
		$('#exp-'+(containerNumber+1)).addClass('exp-queue');
		$('.exp-h').data('target', (containerNumber+1)).text($('#exp-'+(containerNumber+1)).data('title')).fadeOut('slow',function(){$(this).fadeIn('slow')});
	}
	$('.auto-complete').val('');
	$('#action-tab').val('experience');
})
$('.auto-complete').blur(function(){
	acBT = setTimeout(function(){
		$('.ac-container').removeClass('ac-container-show');
		$('.experience-col').removeClass('exp-active');
		acRow=0;
	},200)
});
$('.auto-complete').focus(function(){
	$('#exp-'+$('.exp-h').data('target')).addClass('exp-active');
	clearTimeout(acBT);
	if ($(this).val())
		$('.ac-container').addClass('ac-container-show');
});
$('.exp-empty').click(function(){
	var i = $(this).data('col');
	while (i > 1) {
		if ($('#exp-'+(i-1)).data('exp')=='-') {
			i--;
		}
		else
			break;
	}
	$('.experience-col').removeClass('exp-active');
	$('#exp-'+i).addClass('exp-active');
	$('.exp-h').text($('#exp-'+i).data('title')).data('target',i);
	$('.auto-complete').focus();
})
function experience(value) {
	$.post(base_url+'/include/functions/ajax/exp.php', { action: 'getClosestValues', data: value }, function(data){
		if (data.status == 'success') {
			var r = data.MSG;

			if (!Object.keys(r).length) {
				showExpError(lang.noversions,4000);
				return;
			}
			var row = $('.ac-tmpl').html(), k, a, kID, aID;
			$('.ac-container').html('').addClass('ac-container-show');
			for (var i in r) {
				a = '', aID = '';
				if (typeof r[i].KID != 'undefined') {
					k = r[i].keyword;
					kID = r[i].KID;
					$('.ac-container').append(row.replace('%k%',k).replace('%a%','').replace('%kID%',kID).replace('%aID%',''));
				}
				if (typeof r[i].aliases != 'undefined') {
					for (var al in r[i].aliases) {
						a = ' / '+r[i].aliases[al].alias;
						aID = r[i].aliases[al].ALIASID;
						$('.ac-container').append(row.replace('%k%',k).replace('%a%',a).replace('%kID%',kID).replace('%aID%',aID));
					}
				}
				
			}
		}
		else if (data.MSG) {
			if (data.status == 'error')
				showExpError(data.MSG, 4000);
			else if (data.status == 'halt')
				showExpError(data.MSG, 60000);
		}
	})
}

function showExpError(text,hideTimerout) {
	if ($('.exp-error').is(':visible')) return;
	clearTimeout(acET);
	$('#exp-label').hide();
	$('.exp-error').html(text).addClass('exp-error-shown').hide().fadeIn('slow',function() {
		acET = setTimeout(function(){
			$('.exp-error').removeClass('exp-error-shown').fadeOut(function(){
				$('#exp-label').show();
			});
		},hideTimerout);
	});
}

// Work-Area Popup
var waPopupTimer;
var waPopup = {
	successTimeout : 3000,
	errorTimeout : 4500,
	message : '',

	show: function(msg, status) {
		var t = this;
		if (status == 'error') {
			for (var key in msg) {
				$('#'+key).addClass('field-err');
				 t.message += msg[key];
			}
			var timer = t.errorTimeout;
		}
		else {
			t.message = msg;
			var timer = t.successTimeout;
		}

		if (status=='error') {
			$('.wa-target.form-tab-active .wa-message').addClass('red');
		}
		clearTimeout(waPopupTimer);
		$('.wa-target.form-tab-active .wa-message')
			.html(this.message)
			.fadeIn(function(){
				waPopupTimer = setTimeout(function(){
					$('.wa-target.form-tab-active .wa-message')
					.fadeOut()
					.html('')
					.removeClass('red');
				}, timer);
				t.message = '';
			});

	}
}

// Circle Progress Indicator
var circleProgress = {
	circleDiv : '#circle-progress',
	currentProgress : 10,
	circleMarkup :	'<div class="cp-text"></div>' +
					'<div class="cp-total-circle"></div>' +
					'<div class="cp-progress-wrapper">' +
						'<div class="cp-progress cp-lh"></div>' +
						'<div class="cp-progress cp-rh"></div>' +
					'</div>',

	Init: function() {
		this.circleElement = $(this.circleDiv);

		this.circleElement
			.append(this.circleMarkup);

		this.InitWrapper();
		this.updateText(this.currentProgress);
		this.updateCircle(this.currentProgress);
		//this.updateCircle(11.2566);
	},
	InitWrapper: function() {
		var wrapperClass = '';
		if (this.currentProgress > 50) {
			wrapperClass = 'cp51-100';
		}
		this.circleElement
			.find('.cp-progress-wrapper')
			.removeClass('cp51-100')
			.addClass(wrapperClass);
	},
	updateText: function(percent) {
		percent = parseInt(percent);
		this.circleElement
			.find('.cp-text')
			.text(percent+'%');
	},
	updateCircle: function(percent) {
		if (percent < 50.0001) {
			var rp = parseInt(percent/5);
			if (!rp) rp = 1;
			var deg = 18 * rp;
			this.circleElement
				.find('.cp-lh')
				.css('transform','rotate(0deg)');
			this.circleElement
				.find('.cp-rh')
				.css('transform','rotate('+deg+'deg)');
		}
		if (percent > 50) {
			var rp = parseInt((percent-50)/5);
			if (!rp) rp = 1;
			var deg = 18 * rp;
			this.circleElement
				.find('.cp-rh')
				.css('transform', 'rotate(180deg)');
			this.circleElement
				.find('.cp-lh')
				.css('transform','rotate('+(180+deg)+'deg)');
		}
	},
	updateProgress: function(progressTo) {
		if (this.currentProgress == progressTo) return progressTo;
		this.currentProgress = progressTo;
		this.InitWrapper();
		this.updateText(progressTo);
		this.updateCircle(progressTo);
		return progressTo;
	},
};

circleProgress.Init();
setTimeout(function(){
	circleProgress.updateProgress(currentProfileProgress);
},300);

/* Request Send Button to unlock special features */
if (typeof FB == 'undefined') {
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/ka_GE/sdk.js#xfbml=1&appId=592711610773734&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	// FB Liked our page
	window.fbAsyncInit = function() {
		FB.Event.subscribe('message.send', function(response){
			if (response) {
				$.post(base_url+'/include/functions/ajax/FB.php', {action: 'invite'}, function(d){
					//var a = 'http://ido.ge/?r='+Math.random(); $('#fb-send-btn').attr('data-href',a);
					if (d=='ok') {
						already_invited++;
						$('#invited-friends').text(already_invited);
						$('#invite-sep,#invite-txt').hide();
						$('#invite-thanks').slideDown('slow', function(){
							$('.togglebutton input').attr('disabled', false);
						});
					}
					else {
						already_invited++;
						$('#invited-friends').text(already_invited);
						$('#invite-more')
							.addClass('txt-highlight')
							.text($('#invite-more').data('txt')+' '+(free_min_invites-already_invited))
							.css('opacity',0)
							.fadeTo(400,1);
					}
					//FB.XFBML.parse();
				});
			}
		});
	}
}
