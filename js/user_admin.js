$('.admin-tool-btn').click(function() {
	toolControl($(this));
})

function toolControl(e) {
	$('.user-tools').hide();
	$('.user-panel').show();
	var mod_space = e.data('mod-space');
	var mod_tool = e.data('mod-tool');
	$('#mod-space').val(mod_space);
	$('#mod-tool').val(mod_tool);
	if ($.isFunction(window[mod_tool+'InFunc'])) {
		window[mod_tool+'InFunc']();
	}
	$('.'+mod_space+'-tools .tool').hide();
	$('.'+mod_space+'-tools .'+mod_tool).show();
	$('.'+mod_space+'-panel').slideUp();
	$('.'+mod_space+'-tools').slideDown();
}

$('.admin-tools-submit').click(function() {
	var mod_space = $('#mod-space').val();
	var mod_tool = $('#mod-tool').val();
	if (mod_tool=='rating') {
		var scoredelta = $('#'+mod_space+'-rating').val();
		if (scoredelta > 0) scoreadd = '+'; else scoreadd = '';

		if (scoredelta && confirm(mod_space+' Rating change: '+scoreadd+scoredelta+'\nAre you sure?')) {
			$.getJSON(baseurl+'/include/functions/ajax/changeRank.php', {'UID': $('#'+mod_space+'UID').val(), 'scoredelta': scoredelta, 'OID': 0, 'who': mod_space},function(data){updatePanel(data)});
		}
	}
	else if (mod_tool=='write') {
		if ($('#'+mod_space+'-write').val() && $('#'+mod_space+'-write').val().hashCode() != '-1392945000' && confirm(mod_space+': Write a Message\nAre you sure?')) {
			$.post(baseurl+'/include/functions/ajax/writeMessage.php', {'UID': $('#'+mod_space+'UID').val(), 'message': $('#'+mod_space+'-write').val(), 'OID': 0, 'who': mod_space},function(data){writeOutFunc();updatePanel(data)}, 'json');
		}
	}
	else if (mod_tool=='block') {
		if ($('#'+mod_space+'-block').val()!='' && confirm(mod_space+': Block user for '+$('#'+mod_space+'-block').val()+' days\nAre you sure?')) {
			$.post(baseurl+'/include/functions/ajax/blockUser.php', {'UID': $('#'+mod_space+'UID').val(), 'OID': 0, 'DAYS': $('#'+mod_space+'-block').val(), 'who': mod_space},function(data){updatePanel(data)}, 'json');
		}
	}
//	console.log(mod_space);
//	console.log(mod_tool);
})

$('.tools-close').click(function() {
	var mod_space = $('#mod-space').val();
	var mod_tool = $('#mod-tool').val();
	if ($.isFunction(window[mod_tool+'OutFunc'])) {
		window[mod_tool+'OutFunc']();
	}
	$('.'+mod_space+'-tools').slideUp();
	$('.'+mod_space+'-panel').slideDown();
})

String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

function updatePanel(data) {
	if (data.status == 'OK') {
		$('#'+data.who+'-notification-panel').text('OK').removeClass('alert').addClass('secondry').fadeIn('slow');
		setTimeout(function(){
			$('#'+data.who+'-notification-panel').fadeOut('slow');
			$('.'+data.who+'-panel').slideDown();	
		},2500);
		$('.'+data.who+'-tools').slideUp();
	}
	else {
		if (typeof (errorArr[data.MSG]) != 'undefined') {
			errorMsg = errorArr[data.MSG];
		}
		else 
			errorMsg = data.MSG;

		$('#'+data.who+'-notification-panel').text(errorMsg).removeClass('secondary').addClass('alert').fadeIn('slow');
		setTimeout(function(){
			$('#'+data.who+'-notification-panel').fadeOut('slow');
			$('.'+data.who+'-panel').slideDown();	
		},4000);
		$('.'+data.who+'-tools').slideUp();
	}
}

function writeInFunc() {
	var mod_space = $('#mod-space').val();
	$('.ap-side,.ap-divider').hide();
	$('.ap-'+mod_space+'-block').removeClass('large-4').addClass('large-12').show();
}

function writeOutFunc() {
	$('.ap-side').removeClass('large-12').addClass('large-4').show();
	$('.ap-divider').show();
}