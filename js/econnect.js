if (eMoneyWidgetEnable) {
	client.load("eMoneyAuthorizeWidgetFrame");

	client.on('login',function(accessToken){
		onLogin(accessToken);
	});

//	client.callOut('check');

	client.on('check',function(accessToken){
		if (typeof eConnect_required != 'undefined' && eConnect_required){
			onLogin(accessToken);
			$('.progress-indicator-icon-message').show();
			$('.econnect-act-btn').hide();
		}
	});

	client.on('close',function(a){
			$('#eMoneyAuthorizeWidgetFrame').hide();
	});
	client.on('register',function(a){
//		console.log('REGISTER');
		window.location = "https://www.emoney.ge/index.php/main/signup";
	});

	$('.econnect-act-btn').click(function(e){
		if ($(this).hasClass('econnect-localhost')) return;
		e.preventDefault();
		$(this).removeClass('alert').addClass('secondary').css('opacity','0.3');
		$('.qp-ui-mask-modal').addClass('qp-ui-mask-visible');
		$('#eMoneyAuthorizeWidgetFrame').show();
		client.callOut('focusElement','first');
	});
	onLogin = function (access_token) {
		$.post(base_url + '/include/functions/ajax/emoneyAuth.php',
				{ accessToken: access_token, requestUrl: window.location.search.substring(1), process: eMoneyProcess },
				function (data) {
					if (data.status == 'ERROR') { $('.qp-ui-mask-modal').removeClass('qp-ui-mask-visible'); alert(data.MSG); }
					else if (data.status == 'OK') {
						if (data.redirectURI)
							document.location.href = data.redirectURI;
						else
							document.location.reload(true);
					}
				},
				'json'
		);
	}
}