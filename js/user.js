$('#user-desc').linkify();
$('.deal-desc').linkify();
$(".user-desc a").each(function() {
	$(this).attr("target", "_blank");
});

/* Portfolio Slider */
function startSlider() {
	$('#portfolio-slider').slick({
		dots: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});
}
startSlider();

// Needed to normally show previews for Foundation Clearing
$('.clearing-thumbs').on('click','a',function(){
	$('#portfolio-slider').slick('unslick');
});

// Restore portfolio slider after returning from Clearing Preview
$(document.body).on("closed.fndtn.clearing", function(event) {
	setTimeout(function(){
		startSlider();
	},
	100);
});

/* Portfolio Slider end */

/* Scroll to profile sections */
$('.wa-switcher').click(function() {
	if ($(this).hasClass('wa-sb-active') || !$('#'+$(this).data('target')).length) {
		return false;
	}
	else {
		var targetOffsetTop = $('#'+$(this).data('target')).offset().top;
		$('.wa-switcher').removeClass('wa-sb-active');
		$(this).addClass('wa-sb-active');
		$.scrollTo(targetOffsetTop-57,150);
	}
})
var waT,
	p = $('.wa-abs-side-bar').parent();
	pT = p.offset().top + 30;
	pH = p.height();
	waH = $('.wa-abs-side-bar').height();

$(window).scroll(function(){
	if (!$('.wa-abs-side-bar').is(':visible')) return;
	$('.wa-abs-side-bar').css('opacity',0);
	clearTimeout(waT);
	waT = setTimeout(function(){
		var wT = $(this).scrollTop();

		if (wT < pT) {
			$('.wa-abs-side-bar').css('top',80);
		}
		/*
		else if (wT + waH + 80 > pT + pH) {
			mT = pH - waH;
		}
		*/
		else {
			$('.wa-abs-side-bar').css('top',$(window).scrollTop()-$('.work-area-pad').offset().top+80)
		}

		$('.wa-abs-side-bar').animate({
			opacity: 1
		},300);
		findAndHighlightTargetInView('.wa-switcher');
	},100)
});

function findAndHighlightTargetInView(el) {
	var tT, waT, minDist=99999, closestEl;
	$(el).each(function(index){
		if ($('#'+$(this).data('target')).length) {
			tT = $('#'+$(this).data('target')).offset().top;
			waT = $('.wa-abs-side-bar').offset().top;
			delta = Math.abs(tT-waT);
			if (delta < minDist) {
				closestEl = $(this);
				minDist = delta;
			}
		}
	})
	if (!closestEl.hasClass('wa-sb-active')) {
		$('.wa-switcher').removeClass('wa-sb-active');
		closestEl.addClass('wa-sb-active');
	}
}
/* Scroll to profile sections end */