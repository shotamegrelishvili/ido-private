var enx = {};
window.enx = enx;
enx.messages = new Object();
enx.messages.show_progress = function() {
	$('.progress-indicator-icon-message').show();
	$('.message-form-control .send-button').hide();
};

enx.messages.hide_progress = function() {
	$('.progress-indicator-icon-message').hide();
	$('.message-form-control .send-button').show();
};

enx.messages.remove_attachment = function() {
	if (!$('#attachment_id').val()) return;
		$('#toggle-attach').toggle().removeClass('secondary');
		$('#attachment-container').slideToggle(function(){
			$('#pickfiles').toggle();
		});
		$('#filelist').html('');
		$('#toggle-attach .open,#toggle-attach .close').toggle();
		$('.progress-tag,.progress-percent,.terms').hide();
		$('#attachment_id').val('');
};

$("#toggle-attach").click(function() { 
			$("#attachment-container").slideToggle();
			$(".terms").toggle();
			$(".open").toggle();
			$(".close").toggle();
			$("#toggle-attach").toggleClass('toggle-close');
			$("#toggle-attach").toggleClass('secondary');
			$('#attachment_id').val('');

			try {
			$.scrollTo($('#new_message').position().top + $('#new_message').height() -  GetHeight() + 5);
			} catch(err) {
			}
			return false;
});
$('.upload-cancel').live("click", function() {
			$('#toggle-attach').toggle().toggleClass('secondary');
			$('#attachment-container').slideToggle(function(){
			$('#pickfiles').toggle();
			});
			$('#filelist').html('');
			$('#toggle-attach .open,#toggle-attach .close').toggle();
			$('.progress-tag,.progress-percent,.terms').hide();
			$('#attachment_id').val('');
			return false;
});

$('.send-button').click(function() {
			$('.progress-indicator-icon-message').show();
			$('.message-form-control .send-button').hide();
})

$("#new_message.ajaxy").submit(function() {
			$('.msg-error').hide();
			$('#message_validation_error').html("");
			var necessaryfields_empty=true;
			if ($('.has-form-subelem').hasClass('label')) {
				var f=$('#message_format').val();
				if ($('.'+f+'-subelem').length) {
					$('.'+$('#message_format').val()+'-subelem').each(function() {
						if ($(this).prop('checked')) {
							necessaryfields_empty = false;
							return false;
						}
					});
				}
				else
					necessaryfields_empty = false;
				if (!$('#message_format').val() || necessaryfields_empty) {
					enx.messages.hide_progress();
					$('.msg-error').show();
					$('#message_validation_error').html(lang593);
					$.scrollTo($('#message_validation_error').offset().top-80,200);
					return false;
				}
			}
			if ($('#message_body').val().length < 5){
				enx.messages.hide_progress();
				$('.msg-error').show();
			//$('#message_validation_error').removeClass("green-text");
				if ($('#message_format').val() != '' ) {
						$('#message_validation_error').html(lang358);
				} 
				else {
					$('#message_validation_error').html(lang358);
				}
				$.scrollTo($('#message_validation_error').offset().top-80,200);
				return false;
			}
			if ($('#ostatus').val() == '3' && !confirm(langCloseConfirm)) { 
				enx.messages.hide_progress();
				return false;
			}
			if ($('#ostatus').val() == '6' && !confirm(langRejectConfirm)) { 
				enx.messages.hide_progress();
				return false;
			}

			if ($('#attachment-container').is(':visible') && !$('#attachment_id').val() && !confirm(lang627)) {
				enx.messages.hide_progress();
				return false;
			}
			if ($('#ostatus').val()==4 && !$('#attachment_id').val() && !confirm(lang627)) {
				enx.messages.hide_progress();
				return false;
			}
			
			
			$.post($(this).attr('action'), $(this).serialize(),function(data){
						$('#msgarray').html(data);
						document.getElementById('message_body').value = '';
						enx.messages.remove_attachment();
						enx.messages.hide_progress();
						if ($('#ostatus').val() == "0") {
									$('.contact-form-title').html(lang606);
									$('.ostatus-label').removeClass('alert');
									$('.ostatus-icon').removeClass('red').addClass('gray');
									$('#ostatus').val('1');
						}
						else if ($('#ostatus').val() == "3") {
							$('.ostatus-label').addClass('secondary').text(lang203);
							$('#communication-form').hide();
						}
						else if ($('#ostatus').val() == "4" && $('#message_format').val()) {
							$('.ostatus-label').addClass('success').text(lang371);
							$('#communication-form').hide();
						}
						else if ($('#ostatus').val() == "5" && $('#message_format').val()) {
							$('.ostatus-label').addClass('success').text(lang202);
							$('#communication-form').hide();
						}
						else if ($('#ostatus').val() == "6" && $('#message_format').val()) {
							// Buyer Rejected
							$('.ostatus-label').removeClass('success').text(lang586);
							$('#communication-form').hide();
						}
						else if ($('#message_format').val() == 'mutual_cancellation_request') {
							$('#communication-form').hide();
						}
			});
			return false;
});

$('.message-type').click(function() {
	if ($(this).hasClass('label')) return;
	else {
		if ($(this).hasClass('hide-attach')) {
			if ($('#attachment_id').val()) {
				if (confirm(lang766)) {
					enx.messages.remove_attachment();
					$('.attachment-wrapper').hide();
				}
				else return;
			}
			else {
				$('.attachment-wrapper').hide();
			}
		}
		else
			$('.attachment-wrapper').show();
		$('.message-type').each(function() {
			$(this).addClass($(this).data('hc'));
		})
		if ($(this).data('ostatus')) {
			$('#ostatus').val($(this).data('ostatus'));
		}
		$('#new_message input[type=radio]').prop('checked',false);
		$('span.radio').removeClass('checked');
		$('.message-type').not(this).removeClass('label panel-button');
		$(this).addClass('label panel-button').removeClass($(this).data('hc'));
		$('.message-panel').css('background','#'+$(this).data('panel-bg'));
		$('.action-desc').hide();
		$('.'+$(this).data('show')).show();
		$('.block-showhide').show().not('.'+$(this).data('show')).hide();
		$('#message_format').val($(this).data('message-format'));
	}
});

$('.radio-button').on('change',function() {
	if ($(this).data('ostatus')) $('#ostatus').val($(this).data('ostatus'));
	else $('#ostatus').val($(this).data('initial-ostatus'));
	$('.action-desc-'+$(this).data('action-target')).html($(this).data('text')).show();
	$('#message_format').val($(this).data('message-format'));
});

$('.rating-label').click(function() {
	$('span.radio').removeClass('checked');
	$('#rating_value_'+$(this).data('label-value')).prop('checked',true).nextAll('.radio').addClass('checked');
});

if (mid) {
	$.scrollTo($('#mid'+mid).offset().top - 107,400);
}

// PLUPLOAD

var uploader = new plupload.Uploader({
	runtimes : 'gears,html5,flash,silverlight,browserplus',
	browse_button : 'pickfiles',
	multi_selection : false,
	max_file_size : '10mb',
	url : base_url+'/upload.php',
	flash_swf_url : base_url+'/js/plupload212/plupload.flash.swf',
	silverlight_xap_url : base_url+'/js/plupload212/plupload.silverlight.xap',
	filters : [
		{title : "Image files", extensions : "jpg,jpeg,gif,png,tif,bmp"},
		{title : "Zip files", extensions : "zip"},
		{title : "Rar files", extensions : "rar"},
		{title : "Video files", extensions : "avi,mpeg,mpg,mov,rm,3gp,flv,mp4"},
		{title : "Audio files", extensions : "mp3,wav,wma,ogg"},
		{title : "Documents", extensions : "doc,docx,xls,xlsx,pdf,ppt,rtf"}
	]
});
//jpeg, jpg, gif, png, tif, bmp, avi, mpeg, mpg, mov, rm, 3gp, flv, mp4, txt, zip, rar, mp3, wav, wma áƒ“áƒ ogg

uploader.bind('Init', function(up, params) {
	// $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
	$('.nofiles').hide();
	$('.progress-tag,.progress-percent,#toggle-attach').toggle();
	for (var i in files) {
		$('#filelist').html('<span id="' + files[i].id + '"><i class="icon-paperclip icon-large"></i> <strong>' + files[i].name + '</strong> (' + plupload.formatSize(files[i].size) + ') <a href="javascript:" class="upload-cancel selected button tiny radius alert subheading"><span class="close">'+$('.upload .close').html()+'</span></span>');
	}
	$('.toggle-close').hide();
	$('#pickfiles').toggle();
	uploader.start();
});

uploader.bind('UploadProgress', function(up, file) {
	//$('#'+file.id+'>b').html('<span>' + file.percent + "%</span>");
	$('.progress-percent').attr('data-value',file.percent).css('width',file.percent+'%');
	$('.progress-tag').val(file.percent);
});

uploader.bind('FileUploaded', function(up, file, info) {
	if (info.status==200) {
		$('.progress-tag,.progress-percent,.upload-header').fadeOut('slow');
		$('.terms').slideUp();
		var r = $.parseJSON(info.response);
		$('#attachment_id').val(r.fid);
	}
});