$('.selection').on('change', function() {
	var operation = $(this).val();
	if (!operation) return false;
	else if (operation == 'all') {
		selectCheckboxes();
		deal_controls();
	}
	else if (operation == 'none') {
		unselectCheckboxes();
		$('.deal-controls').prop('disabled',true).find('a').addClass('disabled');
	}
	else if (operation == 'active') {
		selectActive();
		$('.deal-controls').find('a').addClass('disabled');
		deal_controls();
	}
	else if (operation == 'inactive') {
		selectInactive();
		$('.deal-controls').find('a').addClass('disabled');
		deal_controls();
	}
});
$('.selection-link').click(function() {
	if ($(this).hasClass('select-all')) {
		selectCheckboxes();
		deal_controls();
	}
	else if ($(this).hasClass('select-none')) {
		unselectCheckboxes();
		$('.deal-controls').prop('disabled',true).find('a').addClass('disabled');
	}
	else if ($(this).hasClass('select-active')) {
		selectActive();
		$('.deal-controls').find('a').addClass('disabled');
		deal_controls();
	}
	else if ($(this).hasClass('select-inactive')) {
		selectInactive();
		$('.deal-controls').find('a').addClass('disabled');
		deal_controls();
	}

})
$('.deal-controls').on('change', function() {
	var operation = $(this).val();
	if (!operation) return false;
	else $('.'+operation).trigger('click');
});

$('input.checkbox').on('click', function(e) {
   	deal_controls();
})

function deal_controls() {
	var ind=false;
	$('.deal-controls').prop('disabled',true).find('a').addClass('disabled');
	$('.checkbox').each(function(){
		if ($(this).prop('checked')) {
			if ($(this).hasClass('active')) $('.deal-controls').find('a:not(.btn-activate)').removeClass('disabled');
			else if ($(this).hasClass('inactive')) $('.deal-controls').find('a:not(.btn-suspend)').removeClass('disabled');
			else if ($(this).hasClass('pending')) { $('.deal-controls').find('a:not(.btn-activate,.btn-suspend)').removeClass('disabled');}
			ind=true;
		}
	});
	if (ind) $('.deal-controls').prop('disabled',false);
}

$('.btn-suspend').click(function() {
    if ($('.checkbox:checked').size() > 0) {
        $('#deals_form').attr('action',base_url+'/manage_deals?tab=deals&suspend=1');
        $('#deals_form').submit();
    } else {
        return false;
    }
});
$('.btn-activate').click(function() {
    if ($('.checkbox:checked').size() > 0) {
        $('#deals_form').attr('action',base_url+'/manage_deals?tab=deals&activate=1');
        $('#deals_form').submit();
    } else {
        return false;
    }
});
$('.btn-delete').click(function() {
    if ($('.checkbox:checked').size() > 0 && confirm(lang['sure'])) {
        $('#deals_form').attr('action',base_url+'/manage_deals?tab=deals&delete=1');
        $('#deals_form').submit();
    } else {
        return false;
    }
});

$('.tabControlDealsOrders').click(function() {
	if ($(this).attr('disabled')) return false;
	else if (!$(this).hasClass('primary-alt')) {
		if ($(this).children('.notify-wrapper').text() > 1)
			$('.odp-search-wrapper').fadeIn();
		else
			$('.odp-search-wrapper').hide();
		$('#se-message').hide();
		$('.tabControlDealsOrders').removeClass('secondary').removeClass('primary-alt').not(this).addClass('secondary');
		$(this).addClass('primary-alt');
		$('.odp-toggle-section').hide();
		var action = $(this).data('action');
		$('#odp-search').attr('placeholder',$('#odp-search').data('placeholder-prefix')+' '+$(this).data('searchin'));
		$('#odp-search-tab').val(action);
		$('.'+$(this).data('action')+'-section').slideToggle();
		$('#odp-heading').fadeOut(function(){$(this).fadeIn().text(lang[action+'_selected']);
		});
		if (action == $('.hide-on-change').data('show')) $('.hide-on-change').show();
		else $('.hide-on-change').hide();
	}

});

var touchTimer;
$('.touch-tab').on({'touchstart':function(e){
	clearTimeout(touchTimer);
	var Xhref = $(this).attr('href');
	touchTimer = setTimeout(function(){
		location.href=Xhref
	}, 300);
}})

$('.touch-tab').on({'touchmove':function(e){
	clearTimeout(touchTimer);
}})

$('.touch-tab').on({'touchend':function(){
	clearTimeout(touchTimer);
}})
