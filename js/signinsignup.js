$('.econnect-act-btn').removeClass('vh');

$('.econnect-localhost').click(function() {
	$(this).removeClass('alert').addClass('secondary').css('opacity','0.3');
	$(document).foundation('joyride', 'start');
});

$('.joyride-close-tip').live('click',function() {
	$('.econnect-act-btn').removeClass('secondary').addClass('alert').css('opacity','1');
})

$('.econnect-btn').live('click',function() {
	if ($('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'')
		$(this).css({'background-color':'#e9e9e9','border-color':'#d0d0d0'}).removeClass('econnect-btn').parent().submit();
})

document.onkeypress=function(e){
	if (e.keyCode == 13) {
		if ($('.joyride-tip-guide').is(':visible') && $('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'') {
			$('.econnect-btn').click();
		}
	}
};

$(document).keyup(function(e) {
	if (e.keyCode == 27 && $('.joyride-tip-guide').is(':visible')) { 
		$('.joyride-close-tip').click();
	}
});