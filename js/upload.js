var max_files=3;
var uploadContainer = $("#photo-upload-container").html();
//uploadContainer = uploadContainer.replace(/i1/g,'iX');
//var currentBtn;
var isFileDragged=false, dragOver, dragOverInd, dragOverTimer;

function Thumb() {
var fileDiv = $(".browsefile");
var containerDiv = $(".upload-container");
var fileInput = $(".photo-file");

$("#upload-wrapper").on("change",".photo-file",function(e){
  var files = this.files;
  currentBtn = parseInt($(this).data('id').replace('i',''));
  showThumbnail(files, currentBtn);
})

$("#upload-wrapper").on("click",".browsefile",function(e){
  currentBtn = parseInt($(this).data('id').replace('i',''));
  //console.log(currentBtn);
  $("#photo-i"+currentBtn).show().focus().click().hide();
  e.preventDefault();
})

$("#upload-wrapper").on("dragenter",".upload-container",function(e){
  e.stopPropagation();
  e.preventDefault();
});

//containerDiv.on("dragleave",function(e){});

$("#upload-wrapper").on("dragover",".upload-container",function(e){
  e.stopPropagation();
  e.preventDefault();
  
  if (!dragOverInd) {
    dragOverInd = true;
    dragOver = $(this);
    var w = $(this).width(), h = $(this).height();
    $(this).css({'width':w,'height':h}).children().css('opacity',0);
    $(this).find('.dragndrop').css({'opacity':1,'width':w,'height':h,'padding-top':(h/2)-10}).fadeIn('slow');
  }
  
  clearTimeout(dragOverTimer);

  dragOverTimer = setTimeout(function() {
    dragOverInd = false;
    dragOver.find('.dragndrop').hide();
    dragOver.children().css('opacity',1);
  },100)
  return;
});

$("#upload-wrapper").on("drop",".upload-container",function(e){
  e.stopPropagation();
  e.preventDefault();
  currentBtn = parseInt($(this).find('a').data('id').replace('i',''));

  var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
  var files = e.target.files || (dt && dt.files);
  if (files) {
    isFileDragged = true;
    showThumbnail(files, currentBtn);
  }
  else {
    alert('Drag and Drop not supported.');
    return;
  }
});
}

function showThumbnail(files, k){
  var currentBtn = k;
  for(var i=0;i<files.length;i++){
    //console.log(i);
    var file = files[i];
    var imageType = /image.*/;
    var draggedFiles64 = [];

    if(!file.type.match(imageType)){
      alert(lang['notimage']);
      continue;
    }

    var image = document.createElement("img");
    var thumbnail = document.getElementById("photo-thumb-i"+currentBtn);
    image.file = file;
    thumbnail.innerHTML = '';
    thumbnail.appendChild(image)

    var reader = new FileReader()
    //console.log(k);
    reader.onload = (function(aImg){
      return function(e){
        aImg.src = e.target.result;
        if (isFileDragged) {
          //console.log('a: '+k);
          //draggedFiles64[i] = e.target.result;
          $('#photo-i'+k).val('');
          $('#photobase64-i'+k).val(e.target.result);
          k++;
        }
        // console.log(e.target.result);
      };
    }(image))
    var ret = reader.readAsDataURL(file);
    var canvas = document.createElement("canvas");
    ctx = canvas.getContext("2d");

    image.onload= function(){
      ctx.drawImage(image,50,27)
      testFormErrors();
    }
    var image_cc = $('.upload-container').length;
    $('#upload-i'+currentBtn).html(lang['change']).removeClass('success').addClass('secondary');
    $('#delete-i'+currentBtn).addClass('inline-block');
    if (typeof(editform)!="undefined") {
      $('#photo-remove-i'+currentBtn+',#photo-indb-i'+currentBtn).val('0');
      //$('#photo-indb-i'+currentBtn).val('0');
    }
    if (image_cc < max_files) {
      image_cc++;
      //console.log('e');
      //if (isFileDragged) { currentBtn++; }
      if ($('#photo-thumb-i'+(currentBtn+1)).length==0) {
        currentBtn++;
        var uploadContainerIns = uploadContainer.replace(/iX/g, "i"+currentBtn);
        $('.upload-container').last().after('<span class="upload-container rel mb10">'+uploadContainerIns+'</span>');
        $('#photo-thumb-i'+currentBtn+'>img').remove();
        $('#photo-thumb-i'+currentBtn+'>i').show();
      }
      //console.log('b: '+currentBtn);
    }
    else currentBtn++;  
  }
  testFormErrors();
}

$(document).ready( function() {
  Thumb();

  $('.deletefile').live('click', function() {
    
  /*
  $('#upload-'+currentBtn).remove();
  $('#delete-'+currentBtn).remove();
  $('#photo-thumb-'+currentBtn).remove();
  $('#photo-'+currentBtn).remove();
  */
  //image_cc--;
      var find = "iX";
      currentBtn = $(this).data('id');
      var regex = new RegExp(find, "g");
      var uploadContainerIns = uploadContainer.replace(regex, currentBtn);
      $(this).parent().html(uploadContainerIns);
      if (typeof(editform)!="undefined") {
        $('#photo-remove-'+currentBtn).val('1');
      }
      //showThumbnail();
  });
});