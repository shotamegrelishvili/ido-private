$("#search_in").on('change', function() {
  var val = $(this).val();
  if (val != "all") {
    //$(this).find('option:selected').text());
    var s = $(this).find('option:selected');
    if (!s.hasClass('changeMade'))
      s.text(lang['search_in']+s.text()).addClass('changeMade');
    $("#catfield").val(s.val());
  }
  else {
    $("#catfield").val('');
  }
});