$('.econnect-local').click(function() {
	$(document).foundation('joyride', 'start',{
		next_button: false,
		prev_button: false,
		tip_animation_fade_speed: 150,
		scroll_speed: 300
	});
});

$('.joyride-close-tip').live('click',function() {
	$('.econnect-act-btn').removeClass('secondary').addClass('alert').css('opacity','1');
})

$('.econnect-btn').live('click',function() {
	if ($('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'')
		$(this).css({'background-color':'#e9e9e9','border-color':'#d0d0d0'}).removeClass('econnect-btn').parent().submit();
})

$('#eMoney-pay').click(function() {
	if ($(this).hasClass('disabled')) {
		return false;
	}
	$(this).addClass('disabled');
	if (payconfirm && confirm(payconfirm)) {
		$('#paymentgateway').val('2');
		$('#bal_form').submit();
	}
	else if (!payconfirm) {
		$('#bal_form').submit();
	}
})

if (eConnect_autostart==1 && $('#eConnect-local').is(':visible')) $(document).foundation('joyride', 'start');	

document.onkeypress=function(e){
  if (e.keyCode == 13) {
  	if ($('.joyride-tip-guide').is(':visible') && $('.joyride-tip-guide .eConnect-email').val()>'' && $('.joyride-tip-guide .eConnect-password').val()>'') {
  		$('.econnect-btn').click();
  	}
  }
};

$(document).keyup(function(e) {
  if (e.keyCode == 27 && $('.joyride-tip-guide').is(':visible')) { 
  	$('.joyride-close-tip').click();
  }
});

/* eMoney AutoLogin */
if (eMoneyAutoSignin) {
	client.load("eMoney-widget-frame");
	client.on('autologinSuccess',function(){
		console.log('good')
		//window.location = "https://www.emoney.ge/index.php/myaccount/addfunds";
	});

/*
	setTimeout(function(){
		client.callOut('autologin');
	},10000)
*/
	$('#emoneyAutoLogin').click(function(e){
		e.preventDefault();
		client.callOut('autologin');
	})

}
/* end eMoney AutoLogin */