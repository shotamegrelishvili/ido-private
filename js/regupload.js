var isFileDragged=false, dragOver, dragOverInd, dragOverTimer, target='', files_hash=[], file_id=1;
if (typeof maxUploadSize == 'undefined')
	maxUploadSize = 1;
if (typeof maxUploads == 'undefined')
	maxUploads = 1;
if (typeof action == 'undefined')
	action = '';
// File Drag
/*
$(".upload-wrapperX").on("drop",".upload-container",function(e){
	e.stopPropagation();
	e.preventDefault();
	target = $(this).data('target');

	var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
	var files = e.target.files || (dt && dt.files);
	if (files) {
		isFileDragged = true;
		showThumbnail(files);
	}
	else {
		alert('Drag and Drop not supported.');
		return;
	}
});
*/

// General Upload
$(".upload-wrapper").on("change","input[type=file]",function(e){
	var files = this.files;
	showThumbnail(files);
	if ($(this).hasClass('req-cc'))
		$('input[name=uploadFile]').attr('data-initial',files[0].name).addClass('req-cc');
})

// Regular Upload for Single File Uploads
$(".upload-wrapper").on("click",".regupload",function(e){
	target=$(this).data('target');
	$('.'+target+'-file-input').show().focus().click().hide();
	e.preventDefault();
})

function showThumbnail(files) {
	var isFileDragged = false;
	if (!target) return false;
	var thumbnail = document.getElementById(target);

	var singleImageWrapperHTML = '<input type="hidden" name="uploadFile">';
	var multipleImagesWrapperHTML = '<a href="javascript:" class="uw-remove"><i class="icon-remove icon-large"></i></a><input type="hidden" name="uploadFiles[]">';
	var aT = $(thumbnail).data('accept').replace(/,/g,'|');

	if (!aT)
		allowedTypes = /image\/png|image\/jpeg/;
	else
		allowedTypes = new RegExp(aT, 'g');;

	for(var i=0;i<files.length;i++){
		if (file_id>maxUploads) {
			alert(lang['maxUploadsRiched']);
			break;
		}
		var file = files[i];
		var draggedFiles64 = [];
		if (typeof files_hash[file.name+file.size]!='undefined') {
			continue;
		}
		files_hash[file.name+file.size] = 1;

		if(!file.type.match(allowedTypes)){
			alert(lang['fileTypeNotAllowed']+file.name);
			continue;
		}
		if (file.size > maxUploadSize*1048576){
			alert(lang['fileSizeTooLarge']+file.name);
			continue;
		}

		if (!file.type.match(/image.*/)) {
			ext = file.type.substr(file.type.indexOf('/')+1,4);
			var image = document.createElement("span");
			$(image).text(ext);
		}
		else{
			var image = document.createElement("img");
			image.file = file;
			$(image).prop('is-image',1);
		}

		if (!$(thumbnail).data('multiple')) {
			thumbnail.innerHTML = '';
			thumbnail.appendChild(image);
			$(thumbnail).append(singleImageWrapperHTML);
		}
		else {
			var imageWrapper = document.createElement("div");
			$(imageWrapper).prop('id','uw-'+file_id).prop('class','uw-wrapper').append(multipleImagesWrapperHTML);
			imageWrapper.appendChild(image);
			thumbnail.appendChild(imageWrapper);
		}

		var reader = new FileReader()
		reader.onload = (function(aImg){
			return function(e){
				$(aImg).parent().children('input').val(e.target.result);
				$('.'+target+'-file-input').val('');
				if ($(aImg).prop('is-image')){
					aImg.src = e.target.result;
				}
			};
		}(image))

		file_id++;

		var ret = reader.readAsDataURL(file);
		var canvas = document.createElement("canvas");
		ctx = canvas.getContext("2d");

		image.onload= function(){
			if ($(image).prop('is-image')){
				ctx.drawImage(image,50,27)
			}
		}
	}

	if (typeof afterUploadFunction != 'undefined' && typeof window[afterUploadFunction] != 'undefined') {
		window[afterUploadFunction]();
	}
}

$('.g-gallery-multi').on('click', '.uw-remove', function(){
	$(this).parent().remove();
})


// PLUPLOAD START
// Needed for PLUPLOAD also
$(".upload-wrapper").on("dragover",".plupload_wrapper",function(e){
	e.stopPropagation();
	e.preventDefault();

	if (!dragOverInd) {
		dragOverInd = true;
		$('.dragndrop').addClass('dragndrop-hover').fadeIn();
	}

	clearTimeout(dragOverTimer);
	dragOverTimer = setTimeout(function() {
		$('.dragndrop').fadeOut('fast',function(){dragOverInd = false;$(this).removeClass('dragndrop-hover')});
	},200);
	return;
});

$(".plupload-block").plupload({
	// General settings
	runtimes: 'html5,flash,silverlight,html4',
	url: base_url+uploadURI,

	// Maximum file size
	max_file_size: maxUploadSize+'mb',

	max_file_count: maxUploads,

	chunk_size: '1mb',

	// Resize images on clientside if we can
	resize : {
		width: 1920,
		height: 1080,
		quality: 85,
		crop: false // crop to exact dimensions
	},

	filters : {
		prevent_duplicates: true,
		mime_types: [
			{title : "Allowed File Types", extensions : allowed_extensions}
		]
	},

	// Rename files by clicking on their titles
	rename: true,
	
	// Sort files
	sortable: true,

	// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
	dragdrop: true,

	// Views to activate
	views: {
		list: false,
		thumbs: true, // Show thumbs
		active: 'thumbs'
	},

	thumb_width: 200,
	thumb_height: 100,

	flash_swf_url : base_url+'/js/plupload212/plupload.flash.swf',
	silverlight_xap_url : base_url+'/js/plupload212/plupload.silverlight.xap',

	init: {
		FilesAdded: function(up, files) {
			if (typeof plupload_onfile_add != 'undefined') {
				plupload_onfile_add(up.total.queued);
			}
		},

		FileUploaded: function(up, file, r) {
			var response = $.parseJSON(r.response);
			if (typeof response.error != 'undefined' && typeof plupload_onupload_error != 'undefined') {
				plupload_onupload_error(file, response.error);
				this.removeFile(file);
			}
			else {
				plupload_onfile_uploaded(file, response);
			}
		},

		UploadComplete: function(up, files) {
			if (typeof plupload_oncomplete_upload != 'undefined')
				plupload_oncomplete_upload();
		},

		UploadProgress: function(up, file) {
			if (typeof plupload_onupload_progress != 'undefined')
				plupload_onupload_progress(file.id, file.percent);
		},

		Error: function(up, err) {
			if (typeof plupload_onupload_error != 'undefined')
				plupload_onupload_error(err.file, err);
		}
/*
		PostInit: function() {
			document.getElementById('no-html5-support').innerHTML = '';
			$('.browsefile').click(function() {
				target = $(this).data('target');
				console.log(target)
			});
		}
*/
	}
});