// Shopping/Sales Tabs
$('.tabControlSales').click(function() { 
  if (!$(this).hasClass('label')) {
    $('.tabControlSales').toggleClass('label');
    $('.balance-section,.activity-section,#undergoing-deals').slideToggle();
    $('.wd-button').toggle();
  }
});

function toggle_wdsection(e) {
  $(e).toggleClass('wf-form-active');
  $('.balance-section,.wd-section').slideToggle();
  if ($(e).hasClass('wf-form-active')) {
    $(e).toggleClass('success').toggleClass('secondary').html(lang['close_form']);
  }
  else {
   $(e).toggleClass('success').toggleClass('secondary').html(lang['make_wd']); 
  }
}