/*
 * Foundation Responsive Library
 * http://foundation.zurb.com
 * Copyright 2014, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/

(function ($, window, document, undefined) {
	'use strict';

	var header_helpers = function (class_array) {
		var i = class_array.length;
		var head = $('head');

		while (i--) {
			if (head.has('.' + class_array[i]).length === 0) {
				head.append('<meta class="' + class_array[i] + '" />');
			}
		}
	};

	header_helpers([
		'foundation-mq-small',
		'foundation-mq-small-only',
		'foundation-mq-medium',
		'foundation-mq-medium-only',
		'foundation-mq-large',
		'foundation-mq-large-only',
		'foundation-mq-xlarge',
		'foundation-mq-xlarge-only',
		'foundation-mq-xxlarge',
		'foundation-data-attribute-namespace']);

	// Enable FastClick if present

	$(function () {
		if (typeof FastClick !== 'undefined') {
			// Don't attach to body if undefined
			if (typeof document.body !== 'undefined') {
				FastClick.attach(document.body);
			}
		}
	});

	// private Fast Selector wrapper,
	// returns jQuery object. Only use where
	// getElementById is not available.
	var S = function (selector, context) {
		if (typeof selector === 'string') {
			if (context) {
				var cont;
				if (context.jquery) {
					cont = context[0];
					if (!cont) {
						return context;
					}
				} else {
					cont = context;
				}
				return $(cont.querySelectorAll(selector));
			}

			return $(document.querySelectorAll(selector));
		}

		return $(selector, context);
	};

	// Namespace functions.

	var attr_name = function (init) {
		var arr = [];
		if (!init) {
			arr.push('data');
		}
		if (this.namespace.length > 0) {
			arr.push(this.namespace);
		}
		arr.push(this.name);

		return arr.join('-');
	};

	var add_namespace = function (str) {
		var parts = str.split('-'),
				i = parts.length,
				arr = [];

		while (i--) {
			if (i !== 0) {
				arr.push(parts[i]);
			} else {
				if (this.namespace.length > 0) {
					arr.push(this.namespace, parts[i]);
				} else {
					arr.push(parts[i]);
				}
			}
		}

		return arr.reverse().join('-');
	};

	// Event binding and data-options updating.

	var bindings = function (method, options) {
		var self = this,
				bind = function(){
					var $this = S(this),
							should_bind_events = !$this.data(self.attr_name(true) + '-init');
					$this.data(self.attr_name(true) + '-init', $.extend({}, self.settings, (options || method), self.data_options($this)));

					if (should_bind_events) {
						self.events(this);
					}
				};

		if (S(this.scope).is('[' + this.attr_name() +']')) {
			bind.call(this.scope);
		} else {
			S('[' + this.attr_name() +']', this.scope).each(bind);
		}
		// # Patch to fix #5043 to move this *after* the if/else clause in order for Backbone and similar frameworks to have improved control over event binding and data-options updating.
		if (typeof method === 'string') {
			return this[method].call(this, options);
		}

	};

	var single_image_loaded = function (image, callback) {
		function loaded () {
			callback(image[0]);
		}

		function bindLoad () {
			this.one('load', loaded);

			if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
				var src = this.attr( 'src' ),
						param = src.match( /\?/ ) ? '&' : '?';

				param += 'random=' + (new Date()).getTime();
				this.attr('src', src + param);
			}
		}

		if (!image.attr('src')) {
			loaded();
			return;
		}

		if (image[0].complete || image[0].readyState === 4) {
			loaded();
		} else {
			bindLoad.call(image);
		}
	};

	/*
		https://github.com/paulirish/matchMedia.js
	*/

	window.matchMedia = window.matchMedia || (function ( doc ) {

		'use strict';

		var bool,
				docElem = doc.documentElement,
				refNode = docElem.firstElementChild || docElem.firstChild,
				// fakeBody required for <FF4 when executed in <head>
				fakeBody = doc.createElement( 'body' ),
				div = doc.createElement( 'div' );

		div.id = 'mq-test-1';
		div.style.cssText = 'position:absolute;top:-100em';
		fakeBody.style.background = 'none';
		fakeBody.appendChild(div);

		return function (q) {

			div.innerHTML = '&shy;<style media="' + q + '"> #mq-test-1 { width: 42px; }</style>';

			docElem.insertBefore( fakeBody, refNode );
			bool = div.offsetWidth === 42;
			docElem.removeChild( fakeBody );

			return {
				matches : bool,
				media : q
			};

		};

	}( document ));

	/*
	 * jquery.requestAnimationFrame
	 * https://github.com/gnarf37/jquery-requestAnimationFrame
	 * Requires jQuery 1.8+
	 *
	 * Copyright (c) 2012 Corey Frang
	 * Licensed under the MIT license.
	 */

	(function(jQuery) {


	// requestAnimationFrame polyfill adapted from Erik Möller
	// fixes from Paul Irish and Tino Zijdel
	// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
	// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

	var animating,
			lastTime = 0,
			vendors = ['webkit', 'moz'],
			requestAnimationFrame = window.requestAnimationFrame,
			cancelAnimationFrame = window.cancelAnimationFrame,
			jqueryFxAvailable = 'undefined' !== typeof jQuery.fx;

	for (; lastTime < vendors.length && !requestAnimationFrame; lastTime++) {
		requestAnimationFrame = window[ vendors[lastTime] + 'RequestAnimationFrame' ];
		cancelAnimationFrame = cancelAnimationFrame ||
			window[ vendors[lastTime] + 'CancelAnimationFrame' ] ||
			window[ vendors[lastTime] + 'CancelRequestAnimationFrame' ];
	}

	function raf() {
		if (animating) {
			requestAnimationFrame(raf);

			if (jqueryFxAvailable) {
				jQuery.fx.tick();
			}
		}
	}

	if (requestAnimationFrame) {
		// use rAF
		window.requestAnimationFrame = requestAnimationFrame;
		window.cancelAnimationFrame = cancelAnimationFrame;

		if (jqueryFxAvailable) {
			jQuery.fx.timer = function (timer) {
				if (timer() && jQuery.timers.push(timer) && !animating) {
					animating = true;
					raf();
				}
			};

			jQuery.fx.stop = function () {
				animating = false;
			};
		}
	} else {
		// polyfill
		window.requestAnimationFrame = function (callback) {
			var currTime = new Date().getTime(),
				timeToCall = Math.max(0, 16 - (currTime - lastTime)),
				id = window.setTimeout(function () {
					callback(currTime + timeToCall);
				}, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

		window.cancelAnimationFrame = function (id) {
			clearTimeout(id);
		};

	}

	}( $ ));

	function removeQuotes (string) {
		if (typeof string === 'string' || string instanceof String) {
			string = string.replace(/^['\\/"]+|(;\s?})+|['\\/"]+$/g, '');
		}

		return string;
	}

	window.Foundation = {
		name : 'Foundation',

		version : '5.5.1',

		media_queries : {
			'small'       : S('.foundation-mq-small').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'small-only'  : S('.foundation-mq-small-only').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'medium'      : S('.foundation-mq-medium').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'medium-only' : S('.foundation-mq-medium-only').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'large'       : S('.foundation-mq-large').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'large-only'  : S('.foundation-mq-large-only').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'xlarge'      : S('.foundation-mq-xlarge').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'xlarge-only' : S('.foundation-mq-xlarge-only').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''),
			'xxlarge'     : S('.foundation-mq-xxlarge').css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, '')
		},

		stylesheet : $('<style></style>').appendTo('head')[0].sheet,

		global : {
			namespace : undefined
		},

		init : function (scope, libraries, method, options, response) {
			var args = [scope, method, options, response],
					responses = [];

			// check RTL
			this.rtl = /rtl/i.test(S('html').attr('dir'));

			// set foundation global scope
			this.scope = scope || this.scope;

			this.set_namespace();

			if (libraries && typeof libraries === 'string' && !/reflow/i.test(libraries)) {
				if (this.libs.hasOwnProperty(libraries)) {
					responses.push(this.init_lib(libraries, args));
				}
			} else {
				for (var lib in this.libs) {
					responses.push(this.init_lib(lib, libraries));
				}
			}

			S(window).load(function () {
				S(window)
					.trigger('resize.fndtn.clearing')
					.trigger('resize.fndtn.dropdown')
					.trigger('resize.fndtn.equalizer')
					.trigger('resize.fndtn.interchange')
					.trigger('resize.fndtn.joyride')
					.trigger('resize.fndtn.magellan')
					.trigger('resize.fndtn.topbar')
					.trigger('resize.fndtn.slider');
			});

			return scope;
		},

		init_lib : function (lib, args) {
			if (this.libs.hasOwnProperty(lib)) {
				this.patch(this.libs[lib]);

				if (args && args.hasOwnProperty(lib)) {
						if (typeof this.libs[lib].settings !== 'undefined') {
							$.extend(true, this.libs[lib].settings, args[lib]);
						} else if (typeof this.libs[lib].defaults !== 'undefined') {
							$.extend(true, this.libs[lib].defaults, args[lib]);
						}
					return this.libs[lib].init.apply(this.libs[lib], [this.scope, args[lib]]);
				}

				args = args instanceof Array ? args : new Array(args);
				return this.libs[lib].init.apply(this.libs[lib], args);
			}

			return function () {};
		},

		patch : function (lib) {
			lib.scope = this.scope;
			lib.namespace = this.global.namespace;
			lib.rtl = this.rtl;
			lib['data_options'] = this.utils.data_options;
			lib['attr_name'] = attr_name;
			lib['add_namespace'] = add_namespace;
			lib['bindings'] = bindings;
			lib['S'] = this.utils.S;
		},

		inherit : function (scope, methods) {
			var methods_arr = methods.split(' '),
					i = methods_arr.length;

			while (i--) {
				if (this.utils.hasOwnProperty(methods_arr[i])) {
					scope[methods_arr[i]] = this.utils[methods_arr[i]];
				}
			}
		},

		set_namespace : function () {

			// Description:
			//    Don't bother reading the namespace out of the meta tag
			//    if the namespace has been set globally in javascript
			//
			// Example:
			//    Foundation.global.namespace = 'my-namespace';
			// or make it an empty string:
			//    Foundation.global.namespace = '';
			//
			//

			// If the namespace has not been set (is undefined), try to read it out of the meta element.
			// Otherwise use the globally defined namespace, even if it's empty ('')
			var namespace = ( this.global.namespace === undefined ) ? $('.foundation-data-attribute-namespace').css('font-family') : this.global.namespace;

			// Finally, if the namsepace is either undefined or false, set it to an empty string.
			// Otherwise use the namespace value.
			this.global.namespace = ( namespace === undefined || /false/i.test(namespace) ) ? '' : namespace;
		},

		libs : {},

		// methods that can be inherited in libraries
		utils : {

			// Description:
			//    Fast Selector wrapper returns jQuery object. Only use where getElementById
			//    is not available.
			//
			// Arguments:
			//    Selector (String): CSS selector describing the element(s) to be
			//    returned as a jQuery object.
			//
			//    Scope (String): CSS selector describing the area to be searched. Default
			//    is document.
			//
			// Returns:
			//    Element (jQuery Object): jQuery object containing elements matching the
			//    selector within the scope.
			S : S,

			// Description:
			//    Executes a function a max of once every n milliseconds
			//
			// Arguments:
			//    Func (Function): Function to be throttled.
			//
			//    Delay (Integer): Function execution threshold in milliseconds.
			//
			// Returns:
			//    Lazy_function (Function): Function with throttling applied.
			throttle : function (func, delay) {
				var timer = null;

				return function () {
					var context = this, args = arguments;

					if (timer == null) {
						timer = setTimeout(function () {
							func.apply(context, args);
							timer = null;
						}, delay);
					}
				};
			},

			// Description:
			//    Executes a function when it stops being invoked for n seconds
			//    Modified version of _.debounce() http://underscorejs.org
			//
			// Arguments:
			//    Func (Function): Function to be debounced.
			//
			//    Delay (Integer): Function execution threshold in milliseconds.
			//
			//    Immediate (Bool): Whether the function should be called at the beginning
			//    of the delay instead of the end. Default is false.
			//
			// Returns:
			//    Lazy_function (Function): Function with debouncing applied.
			debounce : function (func, delay, immediate) {
				var timeout, result;
				return function () {
					var context = this, args = arguments;
					var later = function () {
						timeout = null;
						if (!immediate) {
							result = func.apply(context, args);
						}
					};
					var callNow = immediate && !timeout;
					clearTimeout(timeout);
					timeout = setTimeout(later, delay);
					if (callNow) {
						result = func.apply(context, args);
					}
					return result;
				};
			},

			// Description:
			//    Parses data-options attribute
			//
			// Arguments:
			//    El (jQuery Object): Element to be parsed.
			//
			// Returns:
			//    Options (Javascript Object): Contents of the element's data-options
			//    attribute.
			data_options : function (el, data_attr_name) {
				data_attr_name = data_attr_name || 'options';
				var opts = {}, ii, p, opts_arr,
						data_options = function (el) {
							var namespace = Foundation.global.namespace;

							if (namespace.length > 0) {
								return el.data(namespace + '-' + data_attr_name);
							}

							return el.data(data_attr_name);
						};

				var cached_options = data_options(el);

				if (typeof cached_options === 'object') {
					return cached_options;
				}

				opts_arr = (cached_options || ':').split(';');
				ii = opts_arr.length;

				function isNumber (o) {
					return !isNaN (o - 0) && o !== null && o !== '' && o !== false && o !== true;
				}

				function trim (str) {
					if (typeof str === 'string') {
						return $.trim(str);
					}
					return str;
				}

				while (ii--) {
					p = opts_arr[ii].split(':');
					p = [p[0], p.slice(1).join(':')];

					if (/true/i.test(p[1])) {
						p[1] = true;
					}
					if (/false/i.test(p[1])) {
						p[1] = false;
					}
					if (isNumber(p[1])) {
						if (p[1].indexOf('.') === -1) {
							p[1] = parseInt(p[1], 10);
						} else {
							p[1] = parseFloat(p[1]);
						}
					}

					if (p.length === 2 && p[0].length > 0) {
						opts[trim(p[0])] = trim(p[1]);
					}
				}

				return opts;
			},

			// Description:
			//    Adds JS-recognizable media queries
			//
			// Arguments:
			//    Media (String): Key string for the media query to be stored as in
			//    Foundation.media_queries
			//
			//    Class (String): Class name for the generated <meta> tag
			register_media : function (media, media_class) {
				if (Foundation.media_queries[media] === undefined) {
					$('head').append('<meta class="' + media_class + '"/>');
					Foundation.media_queries[media] = removeQuotes($('.' + media_class).css('font-family'));
				}
			},

			// Description:
			//    Add custom CSS within a JS-defined media query
			//
			// Arguments:
			//    Rule (String): CSS rule to be appended to the document.
			//
			//    Media (String): Optional media query string for the CSS rule to be
			//    nested under.
			add_custom_rule : function (rule, media) {
				if (media === undefined && Foundation.stylesheet) {
					Foundation.stylesheet.insertRule(rule, Foundation.stylesheet.cssRules.length);
				} else {
					var query = Foundation.media_queries[media];

					if (query !== undefined) {
						Foundation.stylesheet.insertRule('@media ' +
							Foundation.media_queries[media] + '{ ' + rule + ' }');
					}
				}
			},

			// Description:
			//    Performs a callback function when an image is fully loaded
			//
			// Arguments:
			//    Image (jQuery Object): Image(s) to check if loaded.
			//
			//    Callback (Function): Function to execute when image is fully loaded.
			image_loaded : function (images, callback) {
				var self = this,
						unloaded = images.length;

				if (unloaded === 0) {
					callback(images);
				}

				images.each(function () {
					single_image_loaded(self.S(this), function () {
						unloaded -= 1;
						if (unloaded === 0) {
							callback(images);
						}
					});
				});
			},

			// Description:
			//    Returns a random, alphanumeric string
			//
			// Arguments:
			//    Length (Integer): Length of string to be generated. Defaults to random
			//    integer.
			//
			// Returns:
			//    Rand (String): Pseudo-random, alphanumeric string.
			random_str : function () {
				if (!this.fidx) {
					this.fidx = 0;
				}
				this.prefix = this.prefix || [(this.name || 'F'), (+new Date).toString(36)].join('-');

				return this.prefix + (this.fidx++).toString(36);
			},

			// Description:
			//    Helper for window.matchMedia
			//
			// Arguments:
			//    mq (String): Media query
			//
			// Returns:
			//    (Boolean): Whether the media query passes or not
			match : function (mq) {
				return window.matchMedia(mq).matches;
			},

			// Description:
			//    Helpers for checking Foundation default media queries with JS
			//
			// Returns:
			//    (Boolean): Whether the media query passes or not

			is_small_up : function () {
				return this.match(Foundation.media_queries.small);
			},

			is_medium_up : function () {
				return this.match(Foundation.media_queries.medium);
			},

			is_large_up : function () {
				return this.match(Foundation.media_queries.large);
			},

			is_xlarge_up : function () {
				return this.match(Foundation.media_queries.xlarge);
			},

			is_xxlarge_up : function () {
				return this.match(Foundation.media_queries.xxlarge);
			},

			is_small_only : function () {
				return !this.is_medium_up() && !this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
			},

			is_medium_only : function () {
				return this.is_medium_up() && !this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
			},

			is_large_only : function () {
				return this.is_medium_up() && this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
			},

			is_xlarge_only : function () {
				return this.is_medium_up() && this.is_large_up() && this.is_xlarge_up() && !this.is_xxlarge_up();
			},

			is_xxlarge_only : function () {
				return this.is_medium_up() && this.is_large_up() && this.is_xlarge_up() && this.is_xxlarge_up();
			}
		}
	};

	$.fn.foundation = function () {
		var args = Array.prototype.slice.call(arguments, 0);

		return this.each(function () {
			Foundation.init.apply(Foundation, [this].concat(args));
			return this;
		});
	};

}(jQuery, window, window.document));


// Clearing.js

;(function ($, window, document, undefined) {
	'use strict';

	Foundation.libs.clearing = {
		name : 'clearing',

		version : '5.5.1',

		settings : {
			templates : {
				viewing : '<a href="#" class="clearing-close">&times;</a>' +
					'<div class="visible-img" style="display: none"><div class="clearing-touch-label"></div><img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" />' +
					'<p class="clearing-caption"></p><a href="#" class="clearing-main-prev"><span></span></a>' +
					'<a href="#" class="clearing-main-next"><span></span></a></div>'
			},

			// comma delimited list of selectors that, on click, will close clearing,
			// add 'div.clearing-blackout, div.visible-img' to close on background click
			close_selectors : '.clearing-close, div.clearing-blackout',

			// Default to the entire li element.
			open_selectors : '',

			// Image will be skipped in carousel.
			skip_selector : '',

			touch_label : '',

			// event initializers and locks
			init : false,
			locked : false
		},

		init : function (scope, method, options) {
			var self = this;
			Foundation.inherit(this, 'throttle image_loaded');

			this.bindings(method, options);

			if (self.S(this.scope).is('[' + this.attr_name() + ']')) {
				this.assemble(self.S('li', this.scope));
			} else {
				self.S('[' + this.attr_name() + ']', this.scope).each(function () {
					self.assemble(self.S('li', this));
				});
			}
		},

		events : function (scope) {
			var self = this,
					S = self.S,
					$scroll_container = $('.scroll-container');

			if ($scroll_container.length > 0) {
				this.scope = $scroll_container;
			}

			S(this.scope)
				.off('.clearing')
				.on('click.fndtn.clearing', 'ul[' + this.attr_name() + '] li[data-isimg] ' + this.settings.open_selectors,
					function (e, current, target) {
						var current = current || S(this),
								target = target || current,
								next = $(current).next('li.isimg'),
								settings = current.closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init'),
								image = S(e.target);
						e.preventDefault();

						if (!settings) {
							self.init();
							settings = current.closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init');
						}

						// if clearing is open and the current image is
						// clicked, go to the next image in sequence
						if (target.hasClass('visible') &&
							current[0] === target[0] &&
							next.length > 0 && self.is_open(current)) {
							target = next;
							image = S('img', target);
						}

						// set current and target to the clicked li if not otherwise defined.
						self.open(image, current, target);
						self.update_paddles(target);
					})

				.on('click.fndtn.clearing', '.clearing-main-next',
					function (e) { self.nav(e, 'next') })
				.on('click.fndtn.clearing', '.clearing-main-prev',
					function (e) { self.nav(e, 'prev') })
				.on('click.fndtn.clearing', this.settings.close_selectors,
					function (e) { Foundation.libs.clearing.close(e, this) });

			$(document).on('keydown.fndtn.clearing',
					function (e) { self.keydown(e) });

			S(window).off('.clearing').on('resize.fndtn.clearing',
				function () { self.resize() });

			this.swipe_events(scope);
		},

		swipe_events : function (scope) {
			var self = this,
			S = self.S;

			S(this.scope)
				.on('touchstart.fndtn.clearing', '.visible-img', function (e) {
					if (!e.touches) { e = e.originalEvent; }
					var data = {
								start_page_x : e.touches[0].pageX,
								start_page_y : e.touches[0].pageY,
								start_time : (new Date()).getTime(),
								delta_x : 0,
								is_scrolling : undefined
							};

					S(this).data('swipe-transition', data);
					e.stopPropagation();
				})
				.on('touchmove.fndtn.clearing', '.visible-img', function (e) {
					if (!e.touches) {
						e = e.originalEvent;
					}
					// Ignore pinch/zoom events
					if (e.touches.length > 1 || e.scale && e.scale !== 1) {
						return;
					}

					var data = S(this).data('swipe-transition');

					if (typeof data === 'undefined') {
						data = {};
					}

					data.delta_x = e.touches[0].pageX - data.start_page_x;

					if (Foundation.rtl) {
						data.delta_x = -data.delta_x;
					}

					if (typeof data.is_scrolling === 'undefined') {
						data.is_scrolling = !!( data.is_scrolling || Math.abs(data.delta_x) < Math.abs(e.touches[0].pageY - data.start_page_y) );
					}

					if (!data.is_scrolling && !data.active) {
						e.preventDefault();
						var direction = (data.delta_x < 0) ? 'next' : 'prev';
						data.active = true;
						self.nav(e, direction);
					}
				})
				.on('touchend.fndtn.clearing', '.visible-img', function (e) {
					S(this).data('swipe-transition', {});
					e.stopPropagation();
				});
		},

		assemble : function ($li) {
			var $el = $li.parent();

			if ($el.parent().hasClass('carousel')) {
				return;
			}

			$el.after('<div id="foundationClearingHolder"></div>');

			var grid = $el.detach(),
					grid_outerHTML = '';

			if (grid[0] == null) {
				return;
			} else {
				grid_outerHTML = grid[0].outerHTML;
			}

			var holder = this.S('#foundationClearingHolder'),
					settings = $el.data(this.attr_name(true) + '-init'),
					data = {
						grid : '<div class="carousel">' + grid_outerHTML + '</div>',
						viewing : settings.templates.viewing
					},
					wrapper = '<div class="clearing-assembled"><div>' + data.viewing +
						data.grid + '</div></div>',
					touch_label = this.settings.touch_label;

			if (Modernizr.touch) {
				wrapper = $(wrapper).find('.clearing-touch-label').html(touch_label).end();
			}

			holder.after(wrapper).remove();
		},

		open : function ($image, current, target) {
			var self = this,
					body = $(document.body),
					root = target.closest('.clearing-assembled'),
					container = self.S('div', root).first(),
					visible_image = self.S('.visible-img', container),
					image = self.S('img', visible_image).not($image),
					label = self.S('.clearing-touch-label', container),
					error = false;

			// Event to disable scrolling on touch devices when Clearing is activated
			$('body').on('touchmove', function (e) {
				e.preventDefault();
			});

			image.error(function () {
				error = true;
			});

			function startLoad() {
				setTimeout(function () {
					this.image_loaded(image, function () {
						if (image.outerWidth() === 1 && !error) {
							startLoad.call(this);
						} else {
							cb.call(this, image);
						}
					}.bind(this));
				}.bind(this), 100);
			}

			function cb (image) {
				var $image = $(image);
				$image.css('visibility', 'visible');
				// toggle the gallery
				body.css('overflow', 'hidden');
				root.addClass('clearing-blackout');
				container.addClass('clearing-container');
				visible_image.show();
				this.fix_height(target)
					.caption(self.S('.clearing-caption', visible_image), self.S('img', target))
					.center_and_label(image, label)
					.shift(current, target, function () {
						target.closest('li').siblings().removeClass('visible');
						target.closest('li').addClass('visible');
					});
				visible_image.trigger('opened.fndtn.clearing')
			}

			if (!this.locked()) {
				visible_image.trigger('open.fndtn.clearing');
				// set the image to the selected thumbnail
				image
					.attr('src', this.load($image))
					.css('visibility', 'hidden');

				startLoad.call(this);
			}
		},

		close : function (e, el) {
			e.preventDefault();

			var root = (function (target) {
						if (/blackout/.test(target.selector)) {
							return target;
						} else {
							return target.closest('.clearing-blackout');
						}
					}($(el))),
					body = $(document.body), container, visible_image;

			if (el === e.target && root) {
				body.css('overflow', '');
				container = $('div', root).first();
				visible_image = $('.visible-img', container);
				visible_image.trigger('close.fndtn.clearing');
				this.settings.prev_index = 0;
				$('ul[' + this.attr_name() + ']', root)
					.attr('style', '').closest('.clearing-blackout')
					.removeClass('clearing-blackout');
				container.removeClass('clearing-container');
				visible_image.hide();
				visible_image.trigger('closed.fndtn.clearing');
			}

			// Event to re-enable scrolling on touch devices
			$('body').off('touchmove');

			return false;
		},

		is_open : function (current) {
			return current.parent().prop('style').length > 0;
		},

		keydown : function (e) {
			var clearing = $('.clearing-blackout ul[' + this.attr_name() + ']'),
					NEXT_KEY = this.rtl ? 37 : 39,
					PREV_KEY = this.rtl ? 39 : 37,
					ESC_KEY = 27;

			if (e.which === NEXT_KEY) {
				this.go(clearing, 'next');
			}
			if (e.which === PREV_KEY) {
				this.go(clearing, 'prev');
			}
			if (e.which === ESC_KEY) {
				this.S('a.clearing-close').trigger('click').trigger('click.fndtn.clearing');
			}
		},

		nav : function (e, direction) {
			var clearing = $('ul[' + this.attr_name() + ']', '.clearing-blackout');

			e.preventDefault();
			this.go(clearing, direction);
		},

		resize : function () {
			var image = $('img', '.clearing-blackout .visible-img'),
					label = $('.clearing-touch-label', '.clearing-blackout');

			if (image.length) {
				this.center_and_label(image, label);
				image.trigger('resized.fndtn.clearing')
			}
		},

		// visual adjustments
		fix_height : function (target) {
			var lis = target.parent().children(),
					self = this;

			lis.each(function () {
				var li = self.S(this),
						image = li.find('img');

				if (li.height() > image.outerHeight()) {
					li.addClass('fix-height');
				}
			})
			.closest('ul')
			.width(lis.length * 100 + '%');

			return this;
		},

		update_paddles : function (target) {
			target = $(target).closest('li.isimg');
			var visible_image = target
				.closest('.carousel')
				.siblings('.visible-img');

			if (target.next().length > 0) {
				this.S('.clearing-main-next', visible_image).removeClass('disabled');
			} else {
				this.S('.clearing-main-next', visible_image).addClass('disabled');
			}

			if (target.prev().length > 0) {
				this.S('.clearing-main-prev', visible_image).removeClass('disabled');
			} else {
				this.S('.clearing-main-prev', visible_image).addClass('disabled');
			}
		},

		center_and_label : function (target, label) {
			if (!this.rtl && label.length > 0) {
				label.css({
					marginLeft : -(label.outerWidth() / 2),
					marginTop : -(target.outerHeight() / 2)-label.outerHeight()-10
				});
			} else {
				label.css({
					marginRight : -(label.outerWidth() / 2),
					marginTop : -(target.outerHeight() / 2)-label.outerHeight()-10,
					left: 'auto',
					right: '50%'
				});
			}
			return this;
		},

		// image loading and preloading

		load : function ($image) {
			var href;

			if ($image[0].nodeName === 'A') {
				href = $image.attr('href');
			} else {
				href = $image.closest('a').attr('href');
			}

			this.preload($image);

			if (href) {
				return href;
			}
			return $image.attr('src');
		},

		preload : function ($image) {
			this
				.img($image.closest('li').next())
				.img($image.closest('li').prev());
		},

		img : function (img) {
			if (img.length) {
				var new_img = new Image(),
						new_a = this.S('a', img);

				if (new_a.length) {
					new_img.src = new_a.attr('href');
				} else {
					new_img.src = this.S('img', img).attr('src');
				}
			}
			return this;
		},

		// image caption

		caption : function (container, $image) {
			var caption = $image.attr('data-caption');

			if (caption) {
				container
					.html(caption)
					.show();
			} else {
				container
					.text('')
					.hide();
			}
			return this;
		},

		// directional methods

		go : function ($ul, direction) {
			var current = this.S('.visible', $ul),
					target = current[direction]();

			target = this.findtarget(target, direction);
			// Check for skip selector.
			if (this.settings.skip_selector && target.find(this.settings.skip_selector).length != 0) {
				target = target[direction]();
			}

			if (target.length) {
				this.S('img', target)
					.trigger('click', [current, target]).trigger('click.fndtn.clearing', [current, target])
					.trigger('change.fndtn.clearing');
			}
		},

		findtarget : function(target, direction) {
			if (target.length && !$(target).hasClass('isimg')) {
				target = target[direction]();
				target = this.findtarget(target, direction);
			}
			return target;
		},

		shift : function (current, target, callback) {
			var clearing = target.parent(),
					old_index = this.settings.prev_index || target.index(),
					direction = this.direction(clearing, current, target),
					dir = this.rtl ? 'right' : 'left',
					left = parseInt(clearing.css('left'), 10),
					width = target.outerWidth(),
					skip_shift;

			var dir_obj = {};

			// we use jQuery animate instead of CSS transitions because we
			// need a callback to unlock the next animation
			// needs support for RTL **
			if (target.index() !== old_index && !/skip/.test(direction)) {
				if (/left/.test(direction)) {
					this.lock();
					dir_obj[dir] = left + width;
					clearing.animate(dir_obj, 300, this.unlock());
				} else if (/right/.test(direction)) {
					this.lock();
					dir_obj[dir] = left - width;
					clearing.animate(dir_obj, 300, this.unlock());
				}
			} else if (/skip/.test(direction)) {
				// the target image is not adjacent to the current image, so
				// do we scroll right or not
				skip_shift = target.index() - this.settings.up_count;
				this.lock();

				if (skip_shift > 0) {
					dir_obj[dir] = -(skip_shift * width);
					clearing.animate(dir_obj, 300, this.unlock());
				} else {
					dir_obj[dir] = 0;
					clearing.animate(dir_obj, 300, this.unlock());
				}
			}

			callback();
		},

		direction : function ($el, current, target) {
			var lis = this.S('li', $el),
					li_width = lis.outerWidth() + (lis.outerWidth() / 4),
					up_count = Math.floor(this.S('.clearing-container').outerWidth() / li_width) - 1,
					target_index = lis.index(target),
					response;
			this.settings.up_count = up_count;

			if (this.adjacent(this.settings.prev_index, target_index)) {
				if ((target_index > up_count) && target_index > this.settings.prev_index) {
					response = 'right';
				} else if ((target_index > up_count - 1) && target_index <= this.settings.prev_index) {
					response = 'left';
				} else {
					response = false;
				}
			} else {
				response = 'skip';
			}

			this.settings.prev_index = target_index;

			return response;
		},

		adjacent : function (current_index, target_index) {
			for (var i = target_index + 1; i >= target_index - 1; i--) {
				if (i === current_index) {
					return true;
				}
			}
			return false;
		},

		// lock management

		lock : function () {
			this.settings.locked = true;
		},

		unlock : function () {
			this.settings.locked = false;
		},

		locked : function () {
			return this.settings.locked;
		},

		off : function () {
			this.S(this.scope).off('.fndtn.clearing');
			this.S(window).off('.fndtn.clearing');
		},

		reflow : function () {
			this.init();
		}
	};

}(jQuery, window, window.document));


// Tooltip.js

;(function ($, window, document, undefined) {
	'use strict';

	Foundation.libs.tooltip = {
		name : 'tooltip',

		version : '5.5.1',

		settings : {
			additional_inheritable_classes : [],
			tooltip_class : '.tooltip',
			append_to : 'body',
			touch_close_text : 'Tap To Close',
			disable_for_touch : false,
			hover_delay : 200,
			show_on : 'all',
			tip_template : function (selector, content) {
				return '<span data-selector="' + selector + '" id="' + selector + '" class="'
					+ Foundation.libs.tooltip.settings.tooltip_class.substring(1)
					+ '" role="tooltip">' + content + '<span class="nub"></span></span>';
			}
		},

		cache : {},

		init : function (scope, method, options) {
			Foundation.inherit(this, 'random_str');
			this.bindings(method, options);
		},

		should_show : function (target, tip) {
			var settings = $.extend({}, this.settings, this.data_options(target));

			if (settings.show_on === 'all') {
				return true;
			} else if (this.small() && settings.show_on === 'small') {
				return true;
			} else if (this.medium() && settings.show_on === 'medium') {
				return true;
			} else if (this.large() && settings.show_on === 'large') {
				return true;
			}
			return false;
		},

		medium : function () {
			return matchMedia(Foundation.media_queries['medium']).matches;
		},

		large : function () {
			return matchMedia(Foundation.media_queries['large']).matches;
		},

		events : function (instance) {
			var self = this,
					S = self.S;

			self.create(this.S(instance));

			$(this.scope)
				.off('.tooltip')
				.on('mouseenter.fndtn.tooltip mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip',
					'[' + this.attr_name() + ']', function (e) {
					var $this = S(this),
							settings = $.extend({}, self.settings, self.data_options($this)),
							is_touch = false;

					if (Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type) && S(e.target).is('a')) {
						return false;
					}

					if (/mouse/i.test(e.type) && self.ie_touch(e)) {
						return false;
					}

					if ($this.hasClass('open')) {
						if (Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
							e.preventDefault();
						}
						self.hide($this);
					} else {
						if (settings.disable_for_touch && Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
							return;
						} else if (!settings.disable_for_touch && Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
							e.preventDefault();
							S(settings.tooltip_class + '.open').hide();
							is_touch = true;
						}

						if (/enter|over/i.test(e.type)) {
							this.timer = setTimeout(function () {
								var tip = self.showTip($this);
							}.bind(this), self.settings.hover_delay);
						} else if (e.type === 'mouseout' || e.type === 'mouseleave') {
							clearTimeout(this.timer);
							self.hide($this);
						} else {
							self.showTip($this);
						}
					}
				})
				.on('mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip', '[' + this.attr_name() + '].open', function (e) {
					if (/mouse/i.test(e.type) && self.ie_touch(e)) {
						return false;
					}

					if ($(this).data('tooltip-open-event-type') == 'touch' && e.type == 'mouseleave') {
						return;
					} else if ($(this).data('tooltip-open-event-type') == 'mouse' && /MSPointerDown|touchstart/i.test(e.type)) {
						self.convert_to_touch($(this));
					} else {
						self.hide($(this));
					}
				})
				.on('DOMNodeRemoved DOMAttrModified', '[' + this.attr_name() + ']:not(a)', function (e) {
					self.hide(S(this));
				});
		},

		ie_touch : function (e) {
			// How do I distinguish between IE11 and Windows Phone 8?????
			return false;
		},

		showTip : function ($target) {
			var $tip = this.getTip($target);
			if (this.should_show($target, $tip)) {
				return this.show($target);
			}
			return;
		},

		getTip : function ($target) {
			var selector = this.selector($target),
					settings = $.extend({}, this.settings, this.data_options($target)),
					tip = null;

			if (selector) {
				tip = this.S('span[data-selector="' + selector + '"]' + settings.tooltip_class);
			}

			return (typeof tip === 'object') ? tip : false;
		},

		selector : function ($target) {
			var id = $target.attr('id'),
					dataSelector = $target.attr(this.attr_name()) || $target.attr('data-selector');

			if ((id && id.length < 1 || !id) && typeof dataSelector != 'string') {
				dataSelector = this.random_str(6);
				$target
					.attr('data-selector', dataSelector)
					.attr('aria-describedby', dataSelector);
			}

			return (id && id.length > 0) ? id : dataSelector;
		},

		create : function ($target) {
			var self = this,
					settings = $.extend({}, this.settings, this.data_options($target)),
					tip_template = this.settings.tip_template;

			if (typeof settings.tip_template === 'string' && window.hasOwnProperty(settings.tip_template)) {
				tip_template = window[settings.tip_template];
			}

			var $tip = $(tip_template(this.selector($target), $('<div></div>').html($target.attr('title')).html())),
					classes = this.inheritable_classes($target);

			$tip.addClass(classes).appendTo(settings.append_to);

			if (Modernizr.touch) {
				$tip.append('<span class="tap-to-close">' + settings.touch_close_text + '</span>');
				$tip.on('touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip', function (e) {
					self.hide($target);
				});
			}

			$target.removeAttr('title').attr('title', '');
		},

		reposition : function (target, tip, classes) {
			var width, nub, nubHeight, nubWidth, column, objPos;

			tip.css('visibility', 'hidden').show();

			width = target.data('width');
			nub = tip.children('.nub');
			nubHeight = nub.outerHeight();
			nubWidth = nub.outerHeight();

			if (this.small()) {
				tip.css({'width' : '100%'});
			} else {
				tip.css({'width' : (width) ? width : 'auto'});
			}

			objPos = function (obj, top, right, bottom, left, width) {
				return obj.css({
					'top' : (top) ? top : 'auto',
					'bottom' : (bottom) ? bottom : 'auto',
					'left' : (left) ? left : 'auto',
					'right' : (right) ? right : 'auto'
				}).end();
			};

			objPos(tip, (target.offset().top + target.outerHeight() + 10), 'auto', 'auto', target.offset().left);

			if (this.small()) {
				objPos(tip, (target.offset().top + target.outerHeight() + 10), 'auto', 'auto', 12.5, $(this.scope).width());
				tip.addClass('tip-override');
				objPos(nub, -nubHeight, 'auto', 'auto', target.offset().left);
			} else {
				var left = target.offset().left;
				if (Foundation.rtl) {
					nub.addClass('rtl');
					left = target.offset().left + target.outerWidth() - tip.outerWidth();
				}
				objPos(tip, (target.offset().top + target.outerHeight() + 10), 'auto', 'auto', left);
				tip.removeClass('tip-override');
				if (classes && classes.indexOf('tip-top') > -1) {
					if (Foundation.rtl) {
						nub.addClass('rtl');
					}
					objPos(tip, (target.offset().top - tip.outerHeight()), 'auto', 'auto', left)
						.removeClass('tip-override');
				} else if (classes && classes.indexOf('tip-left') > -1) {
					objPos(tip, (target.offset().top + (target.outerHeight() / 2) - (tip.outerHeight() / 2)), 'auto', 'auto', (target.offset().left - tip.outerWidth() - nubHeight))
						.removeClass('tip-override');
					nub.removeClass('rtl');
				} else if (classes && classes.indexOf('tip-right') > -1) {
					objPos(tip, (target.offset().top + (target.outerHeight() / 2) - (tip.outerHeight() / 2)), 'auto', 'auto', (target.offset().left + target.outerWidth() + nubHeight))
						.removeClass('tip-override');
					nub.removeClass('rtl');
				}
			}

			tip.css('visibility', 'visible').hide();
		},

		small : function () {
			return matchMedia(Foundation.media_queries.small).matches &&
				!matchMedia(Foundation.media_queries.medium).matches;
		},

		inheritable_classes : function ($target) {
			var settings = $.extend({}, this.settings, this.data_options($target)),
					inheritables = ['tip-top', 'tip-left', 'tip-bottom', 'tip-right', 'radius', 'round'].concat(settings.additional_inheritable_classes),
					classes = $target.attr('class'),
					filtered = classes ? $.map(classes.split(' '), function (el, i) {
						if ($.inArray(el, inheritables) !== -1) {
							return el;
						}
					}).join(' ') : '';

			return $.trim(filtered);
		},

		convert_to_touch : function ($target) {
			var self = this,
					$tip = self.getTip($target),
					settings = $.extend({}, self.settings, self.data_options($target));

			if ($tip.find('.tap-to-close').length === 0) {
				$tip.append('<span class="tap-to-close">' + settings.touch_close_text + '</span>');
				$tip.on('click.fndtn.tooltip.tapclose touchstart.fndtn.tooltip.tapclose MSPointerDown.fndtn.tooltip.tapclose', function (e) {
					self.hide($target);
				});
			}

			$target.data('tooltip-open-event-type', 'touch');
		},

		show : function ($target) {
			var $tip = this.getTip($target);

			if ($target.data('tooltip-open-event-type') == 'touch') {
				this.convert_to_touch($target);
			}

			this.reposition($target, $tip, $target.attr('class'));
			$target.addClass('open');
			$tip.fadeIn(150);
		},

		hide : function ($target) {
			var $tip = this.getTip($target);

			$tip.fadeOut(150, function () {
				$tip.find('.tap-to-close').remove();
				$tip.off('click.fndtn.tooltip.tapclose MSPointerDown.fndtn.tapclose');
				$target.removeClass('open');
			});
		},

		off : function () {
			var self = this;
			this.S(this.scope).off('.fndtn.tooltip');
			this.S(this.settings.tooltip_class).each(function (i) {
				$('[' + self.attr_name() + ']').eq(i).attr('title', $(this).text());
			}).remove();
		},

		reflow : function () {}
	};
}(jQuery, window, window.document));


// Joyride.js

;(function ($, window, document, undefined) {
	'use strict';

	var Modernizr = Modernizr || false;

	Foundation.libs.joyride = {
		name : 'joyride',

		version : '5.5.1',

		defaults : {
			expose                   : false,     // turn on or off the expose feature
			modal                    : true,      // Whether to cover page with modal during the tour
			keyboard                 : true,      // enable left, right and esc keystrokes
			tip_location             : 'bottom',  // 'top' or 'bottom' in relation to parent
			nub_position             : 'auto',    // override on a per tooltip bases
			scroll_speed             : 1500,      // Page scrolling speed in milliseconds, 0 = no scroll animation
			scroll_animation         : 'linear',  // supports 'swing' and 'linear', extend with jQuery UI.
			timer                    : 0,         // 0 = no timer , all other numbers = timer in milliseconds
			start_timer_on_click     : true,      // true or false - true requires clicking the first button start the timer
			start_offset             : 0,         // the index of the tooltip you want to start on (index of the li)
			next_button              : true,      // true or false to control whether a next button is used
			prev_button              : true,      // true or false to control whether a prev button is used
			tip_animation            : 'fade',    // 'pop' or 'fade' in each tip
			pause_after              : [],        // array of indexes where to pause the tour after
			exposed                  : [],        // array of expose elements
			tip_animation_fade_speed : 300,       // when tipAnimation = 'fade' this is speed in milliseconds for the transition
			cookie_monster           : false,     // true or false to control whether cookies are used
			cookie_name              : 'joyride', // Name the cookie you'll use
			cookie_domain            : false,     // Will this cookie be attached to a domain, ie. '.notableapp.com'
			cookie_expires           : 365,       // set when you would like the cookie to expire.
			tip_container            : 'body',    // Where will the tip be attached
			abort_on_close           : true,      // When true, the close event will not fire any callback
			tip_location_patterns    : {
				top : ['bottom'],
				bottom : [], // bottom should not need to be repositioned
				left : ['right', 'top', 'bottom'],
				right : ['left', 'top', 'bottom']
			},
			post_ride_callback     : function () {},    // A method to call once the tour closes (canceled or complete)
			post_step_callback     : function () {},    // A method to call after each step
			pre_step_callback      : function () {},    // A method to call before each step
			pre_ride_callback      : function () {},    // A method to call before the tour starts (passed index, tip, and cloned exposed element)
			post_expose_callback   : function () {},    // A method to call after an element has been exposed
			template : { // HTML segments for tip layout
				link          : '<a href="#close" class="joyride-close-tip">&times;</a>',
				timer         : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
				tip           : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
				wrapper       : '<div class="joyride-content-wrapper"></div>',
				button        : '<a href="#" class="small button joyride-next-tip"></a>',
				prev_button   : '<a href="#" class="small button joyride-prev-tip"></a>',
				modal         : '<div class="joyride-modal-bg"></div>',
				expose        : '<div class="joyride-expose-wrapper"></div>',
				expose_cover  : '<div class="joyride-expose-cover"></div>'
			},
			expose_add_class : '' // One or more space-separated class names to be added to exposed element
		},

		init : function (scope, method, options) {
			Foundation.inherit(this, 'throttle random_str');

			this.settings = this.settings || $.extend({}, this.defaults, (options || method));

			this.bindings(method, options)
		},

		go_next : function () {
			if (this.settings.$li.next().length < 1) {
				this.end();
			} else if (this.settings.timer > 0) {
				clearTimeout(this.settings.automate);
				this.hide();
				this.show();
				this.startTimer();
			} else {
				this.hide();
				this.show();
			}
		},

		go_prev : function () {
			if (this.settings.$li.prev().length < 1) {
				// Do nothing if there are no prev element
			} else if (this.settings.timer > 0) {
				clearTimeout(this.settings.automate);
				this.hide();
				this.show(null, true);
				this.startTimer();
			} else {
				this.hide();
				this.show(null, true);
			}
		},

		events : function () {
			var self = this;

			$(this.scope)
				.off('.joyride')
				.on('click.fndtn.joyride', '.joyride-next-tip, .joyride-modal-bg', function (e) {
					e.preventDefault();
					this.go_next()
				}.bind(this))
				.on('click.fndtn.joyride', '.joyride-prev-tip', function (e) {
					e.preventDefault();
					this.go_prev();
				}.bind(this))

				.on('click.fndtn.joyride', '.joyride-close-tip', function (e) {
					e.preventDefault();
					this.end(this.settings.abort_on_close);
				}.bind(this))

				.on('keyup.fndtn.joyride', function (e) {
					// Don't do anything if keystrokes are disabled
					// or if the joyride is not being shown
					if (!this.settings.keyboard || !this.settings.riding) {
						return;
					}

					switch (e.which) {
						case 39: // right arrow
							e.preventDefault();
							this.go_next();
							break;
						case 37: // left arrow
							e.preventDefault();
							this.go_prev();
							break;
						case 27: // escape
							e.preventDefault();
							this.end(this.settings.abort_on_close);
					}
				}.bind(this));

			$(window)
				.off('.joyride')
				.on('resize.fndtn.joyride', self.throttle(function () {
					if ($('[' + self.attr_name() + ']').length > 0 && self.settings.$next_tip && self.settings.riding) {
						if (self.settings.exposed.length > 0) {
							var $els = $(self.settings.exposed);

							$els.each(function () {
								var $this = $(this);
								self.un_expose($this);
								self.expose($this);
							});
						}

						if (self.is_phone()) {
							self.pos_phone();
						} else {
							self.pos_default(false);
						}
					}
				}, 100));
		},

		start : function () {
			var self = this,
					$this = $('[' + this.attr_name() + ']', this.scope),
					integer_settings = ['timer', 'scrollSpeed', 'startOffset', 'tipAnimationFadeSpeed', 'cookieExpires'],
					int_settings_count = integer_settings.length;

			if (!$this.length > 0) {
				return;
			}

			if (!this.settings.init) {
				this.events();
			}

			this.settings = $this.data(this.attr_name(true) + '-init');

			// non configureable settings
			this.settings.$content_el = $this;
			this.settings.$body = $(this.settings.tip_container);
			this.settings.body_offset = $(this.settings.tip_container).position();
			this.settings.$tip_content = this.settings.$content_el.find('> li');
			this.settings.paused = false;
			this.settings.attempts = 0;
			this.settings.riding = true;

			// can we create cookies?
			if (typeof $.cookie !== 'function') {
				this.settings.cookie_monster = false;
			}

			// generate the tips and insert into dom.
			if (!this.settings.cookie_monster || this.settings.cookie_monster && !$.cookie(this.settings.cookie_name)) {
				this.settings.$tip_content.each(function (index) {
					var $this = $(this);
					this.settings = $.extend({}, self.defaults, self.data_options($this));

					// Make sure that settings parsed from data_options are integers where necessary
					var i = int_settings_count;
					while (i--) {
						self.settings[integer_settings[i]] = parseInt(self.settings[integer_settings[i]], 10);
					}
					self.create({$li : $this, index : index});
				});

				// show first tip
				if (!this.settings.start_timer_on_click && this.settings.timer > 0) {
					this.show('init');
					this.startTimer();
				} else {
					this.show('init');
				}

			}
		},

		resume : function () {
			this.set_li();
			this.show();
		},

		tip_template : function (opts) {
			var $blank, content;

			opts.tip_class = opts.tip_class || '';

			$blank = $(this.settings.template.tip).addClass(opts.tip_class);
			content = $.trim($(opts.li).html()) +
				this.prev_button_text(opts.prev_button_text, opts.index) +
				this.button_text(opts.button_text) +
				this.settings.template.link +
				this.timer_instance(opts.index);

			$blank.append($(this.settings.template.wrapper));
			$blank.first().attr(this.add_namespace('data-index'), opts.index);
			$('.joyride-content-wrapper', $blank).append(content);

			return $blank[0];
		},

		timer_instance : function (index) {
			var txt;

			if ((index === 0 && this.settings.start_timer_on_click && this.settings.timer > 0) || this.settings.timer === 0) {
				txt = '';
			} else {
				txt = $(this.settings.template.timer)[0].outerHTML;
			}
			return txt;
		},

		button_text : function (txt) {
			if (this.settings.tip_settings.next_button) {
				txt = $.trim(txt) || 'Next';
				txt = $(this.settings.template.button).append(txt)[0].outerHTML;
			} else {
				txt = '';
			}
			return txt;
		},

		prev_button_text : function (txt, idx) {
			if (this.settings.tip_settings.prev_button) {
				txt = $.trim(txt) || 'Previous';

				// Add the disabled class to the button if it's the first element
				if (idx == 0) {
					txt = $(this.settings.template.prev_button).append(txt).addClass('disabled')[0].outerHTML;
				} else {
					txt = $(this.settings.template.prev_button).append(txt)[0].outerHTML;
				}
			} else {
				txt = '';
			}
			return txt;
		},

		create : function (opts) {
			this.settings.tip_settings = $.extend({}, this.settings, this.data_options(opts.$li));
			var buttonText = opts.$li.attr(this.add_namespace('data-button')) || opts.$li.attr(this.add_namespace('data-text')),
					prevButtonText = opts.$li.attr(this.add_namespace('data-button-prev')) || opts.$li.attr(this.add_namespace('data-prev-text')),
				tipClass = opts.$li.attr('class'),
				$tip_content = $(this.tip_template({
					tip_class : tipClass,
					index : opts.index,
					button_text : buttonText,
					prev_button_text : prevButtonText,
					li : opts.$li
				}));

			$(this.settings.tip_container).append($tip_content);
		},

		show : function (init, is_prev) {
			var $timer = null;

			// are we paused?
			if (this.settings.$li === undefined || ($.inArray(this.settings.$li.index(), this.settings.pause_after) === -1)) {

				// don't go to the next li if the tour was paused
				if (this.settings.paused) {
					this.settings.paused = false;
				} else {
					this.set_li(init, is_prev);
				}

				this.settings.attempts = 0;

				if (this.settings.$li.length && this.settings.$target.length > 0) {
					if (init) { //run when we first start
						this.settings.pre_ride_callback(this.settings.$li.index(), this.settings.$next_tip);
						if (this.settings.modal) {
							this.show_modal();
						}
					}

					this.settings.pre_step_callback(this.settings.$li.index(), this.settings.$next_tip);

					if (this.settings.modal && this.settings.expose) {
						this.expose();
					}

					this.settings.tip_settings = $.extend({}, this.settings, this.data_options(this.settings.$li));

					this.settings.timer = parseInt(this.settings.timer, 10);

					this.settings.tip_settings.tip_location_pattern = this.settings.tip_location_patterns[this.settings.tip_settings.tip_location];

					// scroll and hide bg if not modal
					if (!/body/i.test(this.settings.$target.selector)) {
						var joyridemodalbg = $('.joyride-modal-bg');
						if (/pop/i.test(this.settings.tipAnimation)) {
								joyridemodalbg.hide();
						} else {
								joyridemodalbg.fadeOut(this.settings.tipAnimationFadeSpeed);
						}
						this.scroll_to();
					}

					if (this.is_phone()) {
						this.pos_phone(true);
					} else {
						this.pos_default(true);
					}

					$timer = this.settings.$next_tip.find('.joyride-timer-indicator');

					if (/pop/i.test(this.settings.tip_animation)) {

						$timer.width(0);

						if (this.settings.timer > 0) {

							this.settings.$next_tip.show();

							setTimeout(function () {
								$timer.animate({
									width : $timer.parent().width()
								}, this.settings.timer, 'linear');
							}.bind(this), this.settings.tip_animation_fade_speed);

						} else {
							this.settings.$next_tip.show();

						}

					} else if (/fade/i.test(this.settings.tip_animation)) {

						$timer.width(0);

						if (this.settings.timer > 0) {

							this.settings.$next_tip
								.fadeIn(this.settings.tip_animation_fade_speed)
								.show();

							setTimeout(function () {
								$timer.animate({
									width : $timer.parent().width()
								}, this.settings.timer, 'linear');
							}.bind(this), this.settings.tip_animation_fade_speed);

						} else {
							this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed);
						}
					}

					this.settings.$current_tip = this.settings.$next_tip;

				// skip non-existant targets
				} else if (this.settings.$li && this.settings.$target.length < 1) {

					this.show(init, is_prev);

				} else {

					this.end();

				}
			} else {

				this.settings.paused = true;

			}

		},

		is_phone : function () {
			return matchMedia(Foundation.media_queries.small).matches &&
				!matchMedia(Foundation.media_queries.medium).matches;
		},

		hide : function () {
			if (this.settings.modal && this.settings.expose) {
				this.un_expose();
			}

			if (!this.settings.modal) {
				$('.joyride-modal-bg').hide();
			}

			// Prevent scroll bouncing...wait to remove from layout
			this.settings.$current_tip.css('visibility', 'hidden');
			setTimeout($.proxy(function () {
				this.hide();
				this.css('visibility', 'visible');
			}, this.settings.$current_tip), 0);
			this.settings.post_step_callback(this.settings.$li.index(),
				this.settings.$current_tip);
		},

		set_li : function (init, is_prev) {
			if (init) {
				this.settings.$li = this.settings.$tip_content.eq(this.settings.start_offset);
				this.set_next_tip();
				this.settings.$current_tip = this.settings.$next_tip;
			} else {
				if (is_prev) {
					this.settings.$li = this.settings.$li.prev();
				} else {
					this.settings.$li = this.settings.$li.next();
				}
				this.set_next_tip();
			}

			this.set_target();
		},

		set_next_tip : function () {
			this.settings.$next_tip = $('.joyride-tip-guide').eq(this.settings.$li.index());
			this.settings.$next_tip.data('closed', '');
		},

		set_target : function () {
			var cl = this.settings.$li.attr(this.add_namespace('data-class')),
					id = this.settings.$li.attr(this.add_namespace('data-id')),
					$sel = function () {
						if (id) {
							return $(document.getElementById(id));
						} else if (cl) {
							return $('.' + cl).first();
						} else {
							return $('body');
						}
					};

			this.settings.$target = $sel();
		},

		scroll_to : function () {
			var window_half, tipOffset;

			window_half = $(window).height() / 2;
			tipOffset = Math.ceil(this.settings.$target.offset().top - window_half + this.settings.$next_tip.outerHeight());

			if (tipOffset != 0) {
				$('html, body').stop().animate({
					scrollTop : tipOffset
				}, this.settings.scroll_speed, 'swing');
			}
		},

		paused : function () {
			return ($.inArray((this.settings.$li.index() + 1), this.settings.pause_after) === -1);
		},

		restart : function () {
			this.hide();
			this.settings.$li = undefined;
			this.show('init');
		},

		pos_default : function (init) {
			var $nub = this.settings.$next_tip.find('.joyride-nub'),
					nub_width = Math.ceil($nub.outerWidth() / 2),
					nub_height = Math.ceil($nub.outerHeight() / 2),
					toggle = init || false;

			// tip must not be "display: none" to calculate position
			if (toggle) {
				this.settings.$next_tip.css('visibility', 'hidden');
				this.settings.$next_tip.show();
			}

			if (!/body/i.test(this.settings.$target.selector)) {
					var topAdjustment = this.settings.tip_settings.tipAdjustmentY ? parseInt(this.settings.tip_settings.tipAdjustmentY) : 0,
							leftAdjustment = this.settings.tip_settings.tipAdjustmentX ? parseInt(this.settings.tip_settings.tipAdjustmentX) : 0;

					if (this.bottom()) {
						if (this.rtl) {
							this.settings.$next_tip.css({
								top : (this.settings.$target.offset().top + nub_height + this.settings.$target.outerHeight() + topAdjustment),
								left : this.settings.$target.offset().left + this.settings.$target.outerWidth() - this.settings.$next_tip.outerWidth() + leftAdjustment});
						} else {
							this.settings.$next_tip.css({
								top : (this.settings.$target.offset().top + nub_height + this.settings.$target.outerHeight() + topAdjustment),
								left : this.settings.$target.offset().left + leftAdjustment});
						}

						this.nub_position($nub, this.settings.tip_settings.nub_position, 'top');

					} else if (this.top()) {
						if (this.rtl) {
							this.settings.$next_tip.css({
								top : (this.settings.$target.offset().top - this.settings.$next_tip.outerHeight() - nub_height + topAdjustment),
								left : this.settings.$target.offset().left + this.settings.$target.outerWidth() - this.settings.$next_tip.outerWidth()});
						} else {
							this.settings.$next_tip.css({
								top : (this.settings.$target.offset().top - this.settings.$next_tip.outerHeight() - nub_height + topAdjustment),
								left : this.settings.$target.offset().left + leftAdjustment});
						}

						this.nub_position($nub, this.settings.tip_settings.nub_position, 'bottom');

					} else if (this.right()) {

						this.settings.$next_tip.css({
							top : this.settings.$target.offset().top + topAdjustment,
							left : (this.settings.$target.outerWidth() + this.settings.$target.offset().left + nub_width + leftAdjustment)});

						this.nub_position($nub, this.settings.tip_settings.nub_position, 'left');

					} else if (this.left()) {

						this.settings.$next_tip.css({
							top : this.settings.$target.offset().top + topAdjustment,
							left : (this.settings.$target.offset().left - this.settings.$next_tip.outerWidth() - nub_width + leftAdjustment)});

						this.nub_position($nub, this.settings.tip_settings.nub_position, 'right');

					}

					if (!this.visible(this.corners(this.settings.$next_tip)) && this.settings.attempts < this.settings.tip_settings.tip_location_pattern.length) {

						$nub.removeClass('bottom')
							.removeClass('top')
							.removeClass('right')
							.removeClass('left');

						this.settings.tip_settings.tip_location = this.settings.tip_settings.tip_location_pattern[this.settings.attempts];

						this.settings.attempts++;

						this.pos_default();

					}

			} else if (this.settings.$li.length) {

				this.pos_modal($nub);

			}

			if (toggle) {
				this.settings.$next_tip.hide();
				this.settings.$next_tip.css('visibility', 'visible');
			}

		},

		pos_phone : function (init) {
			var tip_height = this.settings.$next_tip.outerHeight(),
					tip_offset = this.settings.$next_tip.offset(),
					target_height = this.settings.$target.outerHeight(),
					$nub = $('.joyride-nub', this.settings.$next_tip),
					nub_height = Math.ceil($nub.outerHeight() / 2),
					toggle = init || false;

			$nub.removeClass('bottom')
				.removeClass('top')
				.removeClass('right')
				.removeClass('left');

			if (toggle) {
				this.settings.$next_tip.css('visibility', 'hidden');
				this.settings.$next_tip.show();
			}

			if (!/body/i.test(this.settings.$target.selector)) {

				if (this.top()) {

						this.settings.$next_tip.offset({top : this.settings.$target.offset().top - tip_height - nub_height});
						$nub.addClass('bottom');

				} else {

					this.settings.$next_tip.offset({top : this.settings.$target.offset().top + target_height + nub_height});
					$nub.addClass('top');

				}

			} else if (this.settings.$li.length) {
				this.pos_modal($nub);
			}

			if (toggle) {
				this.settings.$next_tip.hide();
				this.settings.$next_tip.css('visibility', 'visible');
			}
		},

		pos_modal : function ($nub) {
			this.center();
			$nub.hide();

			this.show_modal();
		},

		show_modal : function () {
			if (!this.settings.$next_tip.data('closed')) {
				var joyridemodalbg =  $('.joyride-modal-bg');
				if (joyridemodalbg.length < 1) {
					var joyridemodalbg = $(this.settings.template.modal);
					joyridemodalbg.appendTo('body');
				}

				if (/pop/i.test(this.settings.tip_animation)) {
						joyridemodalbg.show();
				} else {
						joyridemodalbg.fadeIn(this.settings.tip_animation_fade_speed);
				}
			}
		},

		expose : function () {
			var expose,
					exposeCover,
					el,
					origCSS,
					origClasses,
					randId = 'expose-' + this.random_str(6);

			if (arguments.length > 0 && arguments[0] instanceof $) {
				el = arguments[0];
			} else if (this.settings.$target && !/body/i.test(this.settings.$target.selector)) {
				el = this.settings.$target;
			} else {
				return false;
			}

			if (el.length < 1) {
				if (window.console) {
					console.error('element not valid', el);
				}
				return false;
			}

			expose = $(this.settings.template.expose);
			this.settings.$body.append(expose);
			expose.css({
				top : el.offset().top,
				left : el.offset().left,
				width : el.outerWidth(true),
				height : el.outerHeight(true)
			});

			exposeCover = $(this.settings.template.expose_cover);

			origCSS = {
				zIndex : el.css('z-index'),
				position : el.css('position')
			};

			origClasses = el.attr('class') == null ? '' : el.attr('class');

			el.css('z-index', parseInt(expose.css('z-index')) + 1);

			if (origCSS.position == 'static') {
				el.css('position', 'relative');
			}

			el.data('expose-css', origCSS);
			el.data('orig-class', origClasses);
			el.attr('class', origClasses + ' ' + this.settings.expose_add_class);

			exposeCover.css({
				top : el.offset().top,
				left : el.offset().left,
				width : el.outerWidth(true),
				height : el.outerHeight(true)
			});

			if (this.settings.modal) {
				this.show_modal();
			}

			this.settings.$body.append(exposeCover);
			expose.addClass(randId);
			exposeCover.addClass(randId);
			el.data('expose', randId);
			this.settings.post_expose_callback(this.settings.$li.index(), this.settings.$next_tip, el);
			this.add_exposed(el);
		},

		un_expose : function () {
			var exposeId,
					el,
					expose,
					origCSS,
					origClasses,
					clearAll = false;

			if (arguments.length > 0 && arguments[0] instanceof $) {
				el = arguments[0];
			} else if (this.settings.$target && !/body/i.test(this.settings.$target.selector)) {
				el = this.settings.$target;
			} else {
				return false;
			}

			if (el.length < 1) {
				if (window.console) {
					console.error('element not valid', el);
				}
				return false;
			}

			exposeId = el.data('expose');
			expose = $('.' + exposeId);

			if (arguments.length > 1) {
				clearAll = arguments[1];
			}

			if (clearAll === true) {
				$('.joyride-expose-wrapper,.joyride-expose-cover').remove();
			} else {
				expose.remove();
			}

			origCSS = el.data('expose-css');

			if (origCSS.zIndex == 'auto') {
				el.css('z-index', '');
			} else {
				el.css('z-index', origCSS.zIndex);
			}

			if (origCSS.position != el.css('position')) {
				if (origCSS.position == 'static') {// this is default, no need to set it.
					el.css('position', '');
				} else {
					el.css('position', origCSS.position);
				}
			}

			origClasses = el.data('orig-class');
			el.attr('class', origClasses);
			el.removeData('orig-classes');

			el.removeData('expose');
			el.removeData('expose-z-index');
			this.remove_exposed(el);
		},

		add_exposed : function (el) {
			this.settings.exposed = this.settings.exposed || [];
			if (el instanceof $ || typeof el === 'object') {
				this.settings.exposed.push(el[0]);
			} else if (typeof el == 'string') {
				this.settings.exposed.push(el);
			}
		},

		remove_exposed : function (el) {
			var search, i;
			if (el instanceof $) {
				search = el[0]
			} else if (typeof el == 'string') {
				search = el;
			}

			this.settings.exposed = this.settings.exposed || [];
			i = this.settings.exposed.length;

			while (i--) {
				if (this.settings.exposed[i] == search) {
					this.settings.exposed.splice(i, 1);
					return;
				}
			}
		},

		center : function () {
			var $w = $(window);

			this.settings.$next_tip.css({
				top : ((($w.height() - this.settings.$next_tip.outerHeight()) / 2) + $w.scrollTop()),
				left : ((($w.width() - this.settings.$next_tip.outerWidth()) / 2) + $w.scrollLeft())
			});

			return true;
		},

		bottom : function () {
			return /bottom/i.test(this.settings.tip_settings.tip_location);
		},

		top : function () {
			return /top/i.test(this.settings.tip_settings.tip_location);
		},

		right : function () {
			return /right/i.test(this.settings.tip_settings.tip_location);
		},

		left : function () {
			return /left/i.test(this.settings.tip_settings.tip_location);
		},

		corners : function (el) {
			var w = $(window),
					window_half = w.height() / 2,
					//using this to calculate since scroll may not have finished yet.
					tipOffset = Math.ceil(this.settings.$target.offset().top - window_half + this.settings.$next_tip.outerHeight()),
					right = w.width() + w.scrollLeft(),
					offsetBottom =  w.height() + tipOffset,
					bottom = w.height() + w.scrollTop(),
					top = w.scrollTop();

			if (tipOffset < top) {
				if (tipOffset < 0) {
					top = 0;
				} else {
					top = tipOffset;
				}
			}

			if (offsetBottom > bottom) {
				bottom = offsetBottom;
			}

			return [
				el.offset().top < top,
				right < el.offset().left + el.outerWidth(),
				bottom < el.offset().top + el.outerHeight(),
				w.scrollLeft() > el.offset().left
			];
		},

		visible : function (hidden_corners) {
			var i = hidden_corners.length;

			while (i--) {
				if (hidden_corners[i]) {
					return false;
				}
			}

			return true;
		},

		nub_position : function (nub, pos, def) {
			if (pos === 'auto') {
				nub.addClass(def);
			} else {
				nub.addClass(pos);
			}
		},

		startTimer : function () {
			if (this.settings.$li.length) {
				this.settings.automate = setTimeout(function () {
					this.hide();
					this.show();
					this.startTimer();
				}.bind(this), this.settings.timer);
			} else {
				clearTimeout(this.settings.automate);
			}
		},

		end : function (abort) {
			if (this.settings.cookie_monster) {
				$.cookie(this.settings.cookie_name, 'ridden', {expires : this.settings.cookie_expires, domain : this.settings.cookie_domain});
			}

			if (this.settings.timer > 0) {
				clearTimeout(this.settings.automate);
			}

			if (this.settings.modal && this.settings.expose) {
				this.un_expose();
			}

			// Unplug keystrokes listener
			$(this.scope).off('keyup.joyride')

			this.settings.$next_tip.data('closed', true);
			this.settings.riding = false;

			$('.joyride-modal-bg').hide();
			this.settings.$current_tip.hide();

			if (typeof abort === 'undefined' || abort === false) {
				this.settings.post_step_callback(this.settings.$li.index(), this.settings.$current_tip);
				this.settings.post_ride_callback(this.settings.$li.index(), this.settings.$current_tip);
			}

			$('.joyride-tip-guide').remove();
		},

		off : function () {
			$(this.scope).off('.joyride');
			$(window).off('.joyride');
			$('.joyride-close-tip, .joyride-next-tip, .joyride-modal-bg').off('.joyride');
			$('.joyride-tip-guide, .joyride-modal-bg').remove();
			clearTimeout(this.settings.automate);
			this.settings = {};
		},

		reflow : function () {}
	};
}(jQuery, window, window.document));


// Slick.js (Orbit.js depreciated)

/*
 Version: 1.4.1
	Author: Ken Wheeler
 Website: http://kenwheeler.github.io
 */

!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):"undefined"!=typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){"use strict";var b=window.Slick||{};b=function(){function c(c,d){var f,g,h,e=this;if(e.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:a(c),appendDots:a(c),arrows:!0,asNavFor:null,prevArrow:'<button type="button" data-role="none" class="slick-prev">Previous</button>',nextArrow:'<button type="button" data-role="none" class="slick-next">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(a,b){return'<button type="button" data-role="none">'+(b+1)+"</button>"},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rtl:!1,slide:"",slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,variableWidth:!1,vertical:!1,waitForAnimate:!0},e.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,$list:null,touchObject:{},transformsEnabled:!1},a.extend(e,e.initials),e.activeBreakpoint=null,e.animType=null,e.animProp=null,e.breakpoints=[],e.breakpointSettings=[],e.cssTransitions=!1,e.hidden="hidden",e.paused=!1,e.positionProp=null,e.respondTo=null,e.shouldClick=!0,e.$slider=a(c),e.$slidesCache=null,e.transformType=null,e.transitionType=null,e.visibilityChange="visibilitychange",e.windowWidth=0,e.windowTimer=null,f=a(c).data("slick")||{},e.options=a.extend({},e.defaults,f,d),e.currentSlide=e.options.initialSlide,e.originalSettings=e.options,g=e.options.responsive||null,g&&g.length>-1){e.respondTo=e.options.respondTo||"window";for(h in g)g.hasOwnProperty(h)&&(e.breakpoints.push(g[h].breakpoint),e.breakpointSettings[g[h].breakpoint]=g[h].settings);e.breakpoints.sort(function(a,b){return e.options.mobileFirst===!0?a-b:b-a})}"undefined"!=typeof document.mozHidden?(e.hidden="mozHidden",e.visibilityChange="mozvisibilitychange"):"undefined"!=typeof document.msHidden?(e.hidden="msHidden",e.visibilityChange="msvisibilitychange"):"undefined"!=typeof document.webkitHidden&&(e.hidden="webkitHidden",e.visibilityChange="webkitvisibilitychange"),e.autoPlay=a.proxy(e.autoPlay,e),e.autoPlayClear=a.proxy(e.autoPlayClear,e),e.changeSlide=a.proxy(e.changeSlide,e),e.clickHandler=a.proxy(e.clickHandler,e),e.selectHandler=a.proxy(e.selectHandler,e),e.setPosition=a.proxy(e.setPosition,e),e.swipeHandler=a.proxy(e.swipeHandler,e),e.dragHandler=a.proxy(e.dragHandler,e),e.keyHandler=a.proxy(e.keyHandler,e),e.autoPlayIterator=a.proxy(e.autoPlayIterator,e),e.instanceUid=b++,e.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,e.init(),e.checkResponsive(!0)}var b=0;return c}(),b.prototype.addSlide=b.prototype.slickAdd=function(b,c,d){var e=this;if("boolean"==typeof c)d=c,c=null;else if(0>c||c>=e.slideCount)return!1;e.unload(),"number"==typeof c?0===c&&0===e.$slides.length?a(b).appendTo(e.$slideTrack):d?a(b).insertBefore(e.$slides.eq(c)):a(b).insertAfter(e.$slides.eq(c)):d===!0?a(b).prependTo(e.$slideTrack):a(b).appendTo(e.$slideTrack),e.$slides=e.$slideTrack.children(this.options.slide),e.$slideTrack.children(this.options.slide).detach(),e.$slideTrack.append(e.$slides),e.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),e.$slidesCache=e.$slides,e.reinit()},b.prototype.animateHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.animate({height:b},a.options.speed)}},b.prototype.animateSlide=function(b,c){var d={},e=this;e.animateHeight(),e.options.rtl===!0&&e.options.vertical===!1&&(b=-b),e.transformsEnabled===!1?e.options.vertical===!1?e.$slideTrack.animate({left:b},e.options.speed,e.options.easing,c):e.$slideTrack.animate({top:b},e.options.speed,e.options.easing,c):e.cssTransitions===!1?(e.options.rtl===!0&&(e.currentLeft=-e.currentLeft),a({animStart:e.currentLeft}).animate({animStart:b},{duration:e.options.speed,easing:e.options.easing,step:function(a){a=Math.ceil(a),e.options.vertical===!1?(d[e.animType]="translate("+a+"px, 0px)",e.$slideTrack.css(d)):(d[e.animType]="translate(0px,"+a+"px)",e.$slideTrack.css(d))},complete:function(){c&&c.call()}})):(e.applyTransition(),b=Math.ceil(b),d[e.animType]=e.options.vertical===!1?"translate3d("+b+"px, 0px, 0px)":"translate3d(0px,"+b+"px, 0px)",e.$slideTrack.css(d),c&&setTimeout(function(){e.disableTransition(),c.call()},e.options.speed))},b.prototype.asNavFor=function(b){var c=this,d=null!==c.options.asNavFor?a(c.options.asNavFor).slick("getSlick"):null;null!==d&&d.slideHandler(b,!0)},b.prototype.applyTransition=function(a){var b=this,c={};c[b.transitionType]=b.options.fade===!1?b.transformType+" "+b.options.speed+"ms "+b.options.cssEase:"opacity "+b.options.speed+"ms "+b.options.cssEase,b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.autoPlay=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer),a.slideCount>a.options.slidesToShow&&a.paused!==!0&&(a.autoPlayTimer=setInterval(a.autoPlayIterator,a.options.autoplaySpeed))},b.prototype.autoPlayClear=function(){var a=this;a.autoPlayTimer&&clearInterval(a.autoPlayTimer)},b.prototype.autoPlayIterator=function(){var a=this;a.options.infinite===!1?1===a.direction?(a.currentSlide+1===a.slideCount-1&&(a.direction=0),a.slideHandler(a.currentSlide+a.options.slidesToScroll)):(0===a.currentSlide-1&&(a.direction=1),a.slideHandler(a.currentSlide-a.options.slidesToScroll)):a.slideHandler(a.currentSlide+a.options.slidesToScroll)},b.prototype.buildArrows=function(){var b=this;b.options.arrows===!0&&b.slideCount>b.options.slidesToShow&&(b.$prevArrow=a(b.options.prevArrow),b.$nextArrow=a(b.options.nextArrow),b.htmlExpr.test(b.options.prevArrow)&&b.$prevArrow.appendTo(b.options.appendArrows),b.htmlExpr.test(b.options.nextArrow)&&b.$nextArrow.appendTo(b.options.appendArrows),b.options.infinite!==!0&&b.$prevArrow.addClass("slick-disabled"))},b.prototype.buildDots=function(){var c,d,b=this;if(b.options.dots===!0&&b.slideCount>b.options.slidesToShow){for(d='<ul class="'+b.options.dotsClass+'">',c=0;c<=b.getDotCount();c+=1)d+="<li>"+b.options.customPaging.call(this,b,c)+"</li>";d+="</ul>",b.$dots=a(d).appendTo(b.options.appendDots),b.$dots.find("li").first().addClass("slick-active")}},b.prototype.buildOut=function(){var b=this;b.$slides=b.$slider.children(b.options.slide+":not(.slick-cloned)").addClass("slick-slide"),b.slideCount=b.$slides.length,b.$slides.each(function(b,c){a(c).attr("data-slick-index",b)}),b.$slidesCache=b.$slides,b.$slider.addClass("slick-slider"),b.$slideTrack=0===b.slideCount?a('<div class="slick-track"/>').appendTo(b.$slider):b.$slides.wrapAll('<div class="slick-track"/>').parent(),b.$list=b.$slideTrack.wrap('<div class="slick-list"/>').parent(),b.$slideTrack.css("opacity",0),(b.options.centerMode===!0||b.options.swipeToSlide===!0)&&(b.options.slidesToScroll=1),a("img[data-lazy]",b.$slider).not("[src]").addClass("slick-loading"),b.setupInfinite(),b.buildArrows(),b.buildDots(),b.updateDots(),b.options.accessibility===!0&&b.$list.prop("tabIndex",0),b.setSlideClasses("number"==typeof this.currentSlide?this.currentSlide:0),b.options.draggable===!0&&b.$list.addClass("draggable")},b.prototype.checkResponsive=function(b){var d,e,f,c=this,g=c.$slider.width(),h=window.innerWidth||a(window).width();if("window"===c.respondTo?f=h:"slider"===c.respondTo?f=g:"min"===c.respondTo&&(f=Math.min(h,g)),c.originalSettings.responsive&&c.originalSettings.responsive.length>-1&&null!==c.originalSettings.responsive){e=null;for(d in c.breakpoints)c.breakpoints.hasOwnProperty(d)&&(c.originalSettings.mobileFirst===!1?f<c.breakpoints[d]&&(e=c.breakpoints[d]):f>c.breakpoints[d]&&(e=c.breakpoints[d]));null!==e?null!==c.activeBreakpoint?e!==c.activeBreakpoint&&(c.activeBreakpoint=e,"unslick"===c.breakpointSettings[e]?c.unslick():(c.options=a.extend({},c.originalSettings,c.breakpointSettings[e]),b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())):(c.activeBreakpoint=e,"unslick"===c.breakpointSettings[e]?c.unslick():(c.options=a.extend({},c.originalSettings,c.breakpointSettings[e]),b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())):null!==c.activeBreakpoint&&(c.activeBreakpoint=null,c.options=c.originalSettings,b===!0&&(c.currentSlide=c.options.initialSlide),c.refresh())}},b.prototype.changeSlide=function(b,c){var f,g,h,d=this,e=a(b.target);switch(e.is("a")&&b.preventDefault(),h=0!==d.slideCount%d.options.slidesToScroll,f=h?0:(d.slideCount-d.currentSlide)%d.options.slidesToScroll,b.data.message){case"previous":g=0===f?d.options.slidesToScroll:d.options.slidesToShow-f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide-g,!1,c);break;case"next":g=0===f?d.options.slidesToScroll:f,d.slideCount>d.options.slidesToShow&&d.slideHandler(d.currentSlide+g,!1,c);break;case"index":var i=0===b.data.index?0:b.data.index||a(b.target).parent().index()*d.options.slidesToScroll;d.slideHandler(d.checkNavigable(i),!1,c);break;default:return}},b.prototype.checkNavigable=function(a){var c,d,b=this;if(c=b.getNavigableIndexes(),d=0,a>c[c.length-1])a=c[c.length-1];else for(var e in c){if(a<c[e]){a=d;break}d=c[e]}return a},b.prototype.clickHandler=function(a){var b=this;b.shouldClick===!1&&(a.stopImmediatePropagation(),a.stopPropagation(),a.preventDefault())},b.prototype.destroy=function(){var b=this;b.autoPlayClear(),b.touchObject={},a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&"object"!=typeof b.options.prevArrow&&b.$prevArrow.remove(),b.$nextArrow&&"object"!=typeof b.options.nextArrow&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-center slick-visible").removeAttr("data-slick-index").css({position:"",left:"",top:"",zIndex:"",opacity:"",width:""}),b.$slider.removeClass("slick-slider"),b.$slider.removeClass("slick-initialized"),b.$list.off(".slick"),a(window).off(".slick-"+b.instanceUid),a(document).off(".slick-"+b.instanceUid),b.$slider.html(b.$slides)},b.prototype.disableTransition=function(a){var b=this,c={};c[b.transitionType]="",b.options.fade===!1?b.$slideTrack.css(c):b.$slides.eq(a).css(c)},b.prototype.fadeSlide=function(a,b){var c=this;c.cssTransitions===!1?(c.$slides.eq(a).css({zIndex:1e3}),c.$slides.eq(a).animate({opacity:1},c.options.speed,c.options.easing,b)):(c.applyTransition(a),c.$slides.eq(a).css({opacity:1,zIndex:1e3}),b&&setTimeout(function(){c.disableTransition(a),b.call()},c.options.speed))},b.prototype.filterSlides=b.prototype.slickFilter=function(a){var b=this;null!==a&&(b.unload(),b.$slideTrack.children(this.options.slide).detach(),b.$slidesCache.filter(a).appendTo(b.$slideTrack),b.reinit())},b.prototype.getCurrent=b.prototype.slickCurrentSlide=function(){var a=this;return a.currentSlide},b.prototype.getDotCount=function(){var a=this,b=0,c=0,d=0;if(a.options.infinite===!0)d=Math.ceil(a.slideCount/a.options.slidesToScroll);else if(a.options.centerMode===!0)d=a.slideCount;else for(;b<a.slideCount;)++d,b=c+a.options.slidesToShow,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d-1},b.prototype.getLeft=function(a){var c,d,f,b=this,e=0;return b.slideOffset=0,d=b.$slides.first().outerHeight(),b.options.infinite===!0?(b.slideCount>b.options.slidesToShow&&(b.slideOffset=-1*b.slideWidth*b.options.slidesToShow,e=-1*d*b.options.slidesToShow),0!==b.slideCount%b.options.slidesToScroll&&a+b.options.slidesToScroll>b.slideCount&&b.slideCount>b.options.slidesToShow&&(a>b.slideCount?(b.slideOffset=-1*(b.options.slidesToShow-(a-b.slideCount))*b.slideWidth,e=-1*(b.options.slidesToShow-(a-b.slideCount))*d):(b.slideOffset=-1*b.slideCount%b.options.slidesToScroll*b.slideWidth,e=-1*b.slideCount%b.options.slidesToScroll*d))):a+b.options.slidesToShow>b.slideCount&&(b.slideOffset=(a+b.options.slidesToShow-b.slideCount)*b.slideWidth,e=(a+b.options.slidesToShow-b.slideCount)*d),b.slideCount<=b.options.slidesToShow&&(b.slideOffset=0,e=0),b.options.centerMode===!0&&b.options.infinite===!0?b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)-b.slideWidth:b.options.centerMode===!0&&(b.slideOffset=0,b.slideOffset+=b.slideWidth*Math.floor(b.options.slidesToShow/2)),c=b.options.vertical===!1?-1*a*b.slideWidth+b.slideOffset:-1*a*d+e,b.options.variableWidth===!0&&(f=b.slideCount<=b.options.slidesToShow||b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow),c=f[0]?-1*f[0].offsetLeft:0,b.options.centerMode===!0&&(f=b.options.infinite===!1?b.$slideTrack.children(".slick-slide").eq(a):b.$slideTrack.children(".slick-slide").eq(a+b.options.slidesToShow+1),c=f[0]?-1*f[0].offsetLeft:0,c+=(b.$list.width()-f.outerWidth())/2)),c},b.prototype.getOption=b.prototype.slickGetOption=function(a){var b=this;return b.options[a]},b.prototype.getNavigableIndexes=function(){var e,a=this,b=0,c=0,d=[];for(a.options.infinite===!1?(e=a.slideCount-a.options.slidesToShow+1,a.options.centerMode===!0&&(e=a.slideCount)):(b=-1*a.slideCount,c=-1*a.slideCount,e=2*a.slideCount);e>b;)d.push(b),b=c+a.options.slidesToScroll,c+=a.options.slidesToScroll<=a.options.slidesToShow?a.options.slidesToScroll:a.options.slidesToShow;return d},b.prototype.getSlick=function(){return this},b.prototype.getSlideCount=function(){var c,d,e,b=this;return e=b.options.centerMode===!0?b.slideWidth*Math.floor(b.options.slidesToShow/2):0,b.options.swipeToSlide===!0?(b.$slideTrack.find(".slick-slide").each(function(c,f){return f.offsetLeft-e+a(f).outerWidth()/2>-1*b.swipeLeft?(d=f,!1):void 0}),c=Math.abs(a(d).attr("data-slick-index")-b.currentSlide)||1):b.options.slidesToScroll},b.prototype.goTo=b.prototype.slickGoTo=function(a,b){var c=this;c.changeSlide({data:{message:"index",index:parseInt(a)}},b)},b.prototype.init=function(){var b=this;a(b.$slider).hasClass("slick-initialized")||(a(b.$slider).addClass("slick-initialized"),b.buildOut(),b.setProps(),b.startLoad(),b.loadSlider(),b.initializeEvents(),b.updateArrows(),b.updateDots()),b.$slider.trigger("init",[b])},b.prototype.initArrowEvents=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.on("click.slick",{message:"previous"},a.changeSlide),a.$nextArrow.on("click.slick",{message:"next"},a.changeSlide))},b.prototype.initDotEvents=function(){var b=this;b.options.dots===!0&&b.slideCount>b.options.slidesToShow&&a("li",b.$dots).on("click.slick",{message:"index"},b.changeSlide),b.options.dots===!0&&b.options.pauseOnDotsHover===!0&&b.options.autoplay===!0&&a("li",b.$dots).on("mouseenter.slick",function(){b.paused=!0,b.autoPlayClear()}).on("mouseleave.slick",function(){b.paused=!1,b.autoPlay()})},b.prototype.initializeEvents=function(){var b=this;b.initArrowEvents(),b.initDotEvents(),b.$list.on("touchstart.slick mousedown.slick",{action:"start"},b.swipeHandler),b.$list.on("touchmove.slick mousemove.slick",{action:"move"},b.swipeHandler),b.$list.on("touchend.slick mouseup.slick",{action:"end"},b.swipeHandler),b.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},b.swipeHandler),b.$list.on("click.slick",b.clickHandler),b.options.autoplay===!0&&(a(document).on(b.visibilityChange,function(){b.visibility()}),b.options.pauseOnHover===!0&&(b.$list.on("mouseenter.slick",function(){b.paused=!0,b.autoPlayClear()}),b.$list.on("mouseleave.slick",function(){b.paused=!1,b.autoPlay()}))),b.options.accessibility===!0&&b.$list.on("keydown.slick",b.keyHandler),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),a(window).on("orientationchange.slick.slick-"+b.instanceUid,function(){b.checkResponsive(),b.setPosition()}),a(window).on("resize.slick.slick-"+b.instanceUid,function(){a(window).width()!==b.windowWidth&&(clearTimeout(b.windowDelay),b.windowDelay=window.setTimeout(function(){b.windowWidth=a(window).width(),b.checkResponsive(),b.setPosition()},50))}),a("*[draggable!=true]",b.$slideTrack).on("dragstart",function(a){a.preventDefault()}),a(window).on("load.slick.slick-"+b.instanceUid,b.setPosition),a(document).on("ready.slick.slick-"+b.instanceUid,b.setPosition)},b.prototype.initUI=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.show(),a.$nextArrow.show()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.show(),a.options.autoplay===!0&&a.autoPlay()},b.prototype.keyHandler=function(a){var b=this;37===a.keyCode&&b.options.accessibility===!0?b.changeSlide({data:{message:"previous"}}):39===a.keyCode&&b.options.accessibility===!0&&b.changeSlide({data:{message:"next"}})},b.prototype.lazyLoad=function(){function g(b){a("img[data-lazy]",b).each(function(){var b=a(this),c=a(this).attr("data-lazy");b.load(function(){b.animate({opacity:1},200)}).css({opacity:0}).attr("src",c).removeAttr("data-lazy").removeClass("slick-loading")})}var c,d,e,f,b=this;b.options.centerMode===!0?b.options.infinite===!0?(e=b.currentSlide+(b.options.slidesToShow/2+1),f=e+b.options.slidesToShow+2):(e=Math.max(0,b.currentSlide-(b.options.slidesToShow/2+1)),f=2+(b.options.slidesToShow/2+1)+b.currentSlide):(e=b.options.infinite?b.options.slidesToShow+b.currentSlide:b.currentSlide,f=e+b.options.slidesToShow,b.options.fade===!0&&(e>0&&e--,f<=b.slideCount&&f++)),c=b.$slider.find(".slick-slide").slice(e,f),g(c),b.slideCount<=b.options.slidesToShow?(d=b.$slider.find(".slick-slide"),g(d)):b.currentSlide>=b.slideCount-b.options.slidesToShow?(d=b.$slider.find(".slick-cloned").slice(0,b.options.slidesToShow),g(d)):0===b.currentSlide&&(d=b.$slider.find(".slick-cloned").slice(-1*b.options.slidesToShow),g(d))},b.prototype.loadSlider=function(){var a=this;a.setPosition(),a.$slideTrack.css({opacity:1}),a.$slider.removeClass("slick-loading"),a.initUI(),"progressive"===a.options.lazyLoad&&a.progressiveLazyLoad()},b.prototype.next=b.prototype.slickNext=function(){var a=this;a.changeSlide({data:{message:"next"}})},b.prototype.pause=b.prototype.slickPause=function(){var a=this;a.autoPlayClear(),a.paused=!0},b.prototype.play=b.prototype.slickPlay=function(){var a=this;a.paused=!1,a.autoPlay()},b.prototype.postSlide=function(a){var b=this;b.$slider.trigger("afterChange",[b,a]),b.animating=!1,b.setPosition(),b.swipeLeft=null,b.options.autoplay===!0&&b.paused===!1&&b.autoPlay()},b.prototype.prev=b.prototype.slickPrev=function(){var a=this;a.changeSlide({data:{message:"previous"}})},b.prototype.progressiveLazyLoad=function(){var c,d,b=this;c=a("img[data-lazy]",b.$slider).length,c>0&&(d=a("img[data-lazy]",b.$slider).first(),d.attr("src",d.attr("data-lazy")).removeClass("slick-loading").load(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad()}).error(function(){d.removeAttr("data-lazy"),b.progressiveLazyLoad()}))},b.prototype.refresh=function(){var b=this,c=b.currentSlide;b.destroy(),a.extend(b,b.initials),b.init(),b.changeSlide({data:{message:"index",index:c}},!0)},b.prototype.reinit=function(){var b=this;b.$slides=b.$slideTrack.children(b.options.slide).addClass("slick-slide"),b.slideCount=b.$slides.length,b.currentSlide>=b.slideCount&&0!==b.currentSlide&&(b.currentSlide=b.currentSlide-b.options.slidesToScroll),b.slideCount<=b.options.slidesToShow&&(b.currentSlide=0),b.setProps(),b.setupInfinite(),b.buildArrows(),b.updateArrows(),b.initArrowEvents(),b.buildDots(),b.updateDots(),b.initDotEvents(),b.options.focusOnSelect===!0&&a(b.$slideTrack).children().on("click.slick",b.selectHandler),b.setSlideClasses(0),b.setPosition(),b.$slider.trigger("reInit",[b])},b.prototype.removeSlide=b.prototype.slickRemove=function(a,b,c){var d=this;return"boolean"==typeof a?(b=a,a=b===!0?0:d.slideCount-1):a=b===!0?--a:a,d.slideCount<1||0>a||a>d.slideCount-1?!1:(d.unload(),c===!0?d.$slideTrack.children().remove():d.$slideTrack.children(this.options.slide).eq(a).remove(),d.$slides=d.$slideTrack.children(this.options.slide),d.$slideTrack.children(this.options.slide).detach(),d.$slideTrack.append(d.$slides),d.$slidesCache=d.$slides,d.reinit(),void 0)},b.prototype.setCSS=function(a){var d,e,b=this,c={};b.options.rtl===!0&&(a=-a),d="left"==b.positionProp?Math.ceil(a)+"px":"0px",e="top"==b.positionProp?Math.ceil(a)+"px":"0px",c[b.positionProp]=a,b.transformsEnabled===!1?b.$slideTrack.css(c):(c={},b.cssTransitions===!1?(c[b.animType]="translate("+d+", "+e+")",b.$slideTrack.css(c)):(c[b.animType]="translate3d("+d+", "+e+", 0px)",b.$slideTrack.css(c)))},b.prototype.setDimensions=function(){var a=this;if(a.options.vertical===!1?a.options.centerMode===!0&&a.$list.css({padding:"0px "+a.options.centerPadding}):(a.$list.height(a.$slides.first().outerHeight(!0)*a.options.slidesToShow),a.options.centerMode===!0&&a.$list.css({padding:a.options.centerPadding+" 0px"})),a.listWidth=a.$list.width(),a.listHeight=a.$list.height(),a.options.vertical===!1&&a.options.variableWidth===!1)a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.width(Math.ceil(a.slideWidth*a.$slideTrack.children(".slick-slide").length));else if(a.options.variableWidth===!0){var b=0;a.slideWidth=Math.ceil(a.listWidth/a.options.slidesToShow),a.$slideTrack.children(".slick-slide").each(function(){b+=a.listWidth}),a.$slideTrack.width(Math.ceil(b)+1)}else a.slideWidth=Math.ceil(a.listWidth),a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0)*a.$slideTrack.children(".slick-slide").length));var c=a.$slides.first().outerWidth(!0)-a.$slides.first().width();a.options.variableWidth===!1&&a.$slideTrack.children(".slick-slide").width(a.slideWidth-c)},b.prototype.setFade=function(){var c,b=this;b.$slides.each(function(d,e){c=-1*b.slideWidth*d,b.options.rtl===!0?a(e).css({position:"relative",right:c,top:0,zIndex:800,opacity:0}):a(e).css({position:"relative",left:c,top:0,zIndex:800,opacity:0})}),b.$slides.eq(b.currentSlide).css({zIndex:900,opacity:1})},b.prototype.setHeight=function(){var a=this;if(1===a.options.slidesToShow&&a.options.adaptiveHeight===!0&&a.options.vertical===!1){var b=a.$slides.eq(a.currentSlide).outerHeight(!0);a.$list.css("height",b)}},b.prototype.setOption=b.prototype.slickSetOption=function(a,b,c){var d=this;d.options[a]=b,c===!0&&(d.unload(),d.reinit())},b.prototype.setPosition=function(){var a=this;a.setDimensions(),a.setHeight(),a.options.fade===!1?a.setCSS(a.getLeft(a.currentSlide)):a.setFade(),a.$slider.trigger("setPosition",[a])},b.prototype.setProps=function(){var a=this,b=document.body.style;a.positionProp=a.options.vertical===!0?"top":"left","top"===a.positionProp?a.$slider.addClass("slick-vertical"):a.$slider.removeClass("slick-vertical"),(void 0!==b.WebkitTransition||void 0!==b.MozTransition||void 0!==b.msTransition)&&a.options.useCSS===!0&&(a.cssTransitions=!0),void 0!==b.OTransform&&(a.animType="OTransform",a.transformType="-o-transform",a.transitionType="OTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.MozTransform&&(a.animType="MozTransform",a.transformType="-moz-transform",a.transitionType="MozTransition",void 0===b.perspectiveProperty&&void 0===b.MozPerspective&&(a.animType=!1)),void 0!==b.webkitTransform&&(a.animType="webkitTransform",a.transformType="-webkit-transform",a.transitionType="webkitTransition",void 0===b.perspectiveProperty&&void 0===b.webkitPerspective&&(a.animType=!1)),void 0!==b.msTransform&&(a.animType="msTransform",a.transformType="-ms-transform",a.transitionType="msTransition",void 0===b.msTransform&&(a.animType=!1)),void 0!==b.transform&&a.animType!==!1&&(a.animType="transform",a.transformType="transform",a.transitionType="transition"),a.transformsEnabled=null!==a.animType&&a.animType!==!1},b.prototype.setSlideClasses=function(a){var c,d,e,f,b=this;b.$slider.find(".slick-slide").removeClass("slick-active").removeClass("slick-center"),d=b.$slider.find(".slick-slide"),b.options.centerMode===!0?(c=Math.floor(b.options.slidesToShow/2),b.options.infinite===!0&&(a>=c&&a<=b.slideCount-1-c?b.$slides.slice(a-c,a+c+1).addClass("slick-active"):(e=b.options.slidesToShow+a,d.slice(e-c+1,e+c+2).addClass("slick-active")),0===a?d.eq(d.length-1-b.options.slidesToShow).addClass("slick-center"):a===b.slideCount-1&&d.eq(b.options.slidesToShow).addClass("slick-center")),b.$slides.eq(a).addClass("slick-center")):a>=0&&a<=b.slideCount-b.options.slidesToShow?b.$slides.slice(a,a+b.options.slidesToShow).addClass("slick-active"):d.length<=b.options.slidesToShow?d.addClass("slick-active"):(f=b.slideCount%b.options.slidesToShow,e=b.options.infinite===!0?b.options.slidesToShow+a:a,b.options.slidesToShow==b.options.slidesToScroll&&b.slideCount-a<b.options.slidesToShow?d.slice(e-(b.options.slidesToShow-f),e+f).addClass("slick-active"):d.slice(e,e+b.options.slidesToShow).addClass("slick-active")),"ondemand"===b.options.lazyLoad&&b.lazyLoad()},b.prototype.setupInfinite=function(){var c,d,e,b=this;if(b.options.fade===!0&&(b.options.centerMode=!1),b.options.infinite===!0&&b.options.fade===!1&&(d=null,b.slideCount>b.options.slidesToShow)){for(e=b.options.centerMode===!0?b.options.slidesToShow+1:b.options.slidesToShow,c=b.slideCount;c>b.slideCount-e;c-=1)d=c-1,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d-b.slideCount).prependTo(b.$slideTrack).addClass("slick-cloned");for(c=0;e>c;c+=1)d=c,a(b.$slides[d]).clone(!0).attr("id","").attr("data-slick-index",d+b.slideCount).appendTo(b.$slideTrack).addClass("slick-cloned");b.$slideTrack.find(".slick-cloned").find("[id]").each(function(){a(this).attr("id","")})}},b.prototype.selectHandler=function(b){var c=this,d=parseInt(a(b.target).parents(".slick-slide").attr("data-slick-index"));return d||(d=0),c.slideCount<=c.options.slidesToShow?(c.$slider.find(".slick-slide").removeClass("slick-active"),c.$slides.eq(d).addClass("slick-active"),c.options.centerMode===!0&&(c.$slider.find(".slick-slide").removeClass("slick-center"),c.$slides.eq(d).addClass("slick-center")),c.asNavFor(d),void 0):(c.slideHandler(d),void 0)},b.prototype.slideHandler=function(a,b,c){var d,e,f,g,h=null,i=this;return b=b||!1,i.animating===!0&&i.options.waitForAnimate===!0||i.options.fade===!0&&i.currentSlide===a||i.slideCount<=i.options.slidesToShow?void 0:(b===!1&&i.asNavFor(a),d=a,h=i.getLeft(d),g=i.getLeft(i.currentSlide),i.currentLeft=null===i.swipeLeft?g:i.swipeLeft,i.options.infinite===!1&&i.options.centerMode===!1&&(0>a||a>i.getDotCount()*i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):i.options.infinite===!1&&i.options.centerMode===!0&&(0>a||a>i.slideCount-i.options.slidesToScroll)?(i.options.fade===!1&&(d=i.currentSlide,c!==!0?i.animateSlide(g,function(){i.postSlide(d)}):i.postSlide(d)),void 0):(i.options.autoplay===!0&&clearInterval(i.autoPlayTimer),e=0>d?0!==i.slideCount%i.options.slidesToScroll?i.slideCount-i.slideCount%i.options.slidesToScroll:i.slideCount+d:d>=i.slideCount?0!==i.slideCount%i.options.slidesToScroll?0:d-i.slideCount:d,i.animating=!0,i.$slider.trigger("beforeChange",[i,i.currentSlide,e]),f=i.currentSlide,i.currentSlide=e,i.setSlideClasses(i.currentSlide),i.updateDots(),i.updateArrows(),i.options.fade===!0?(c!==!0?i.fadeSlide(e,function(){i.postSlide(e)}):i.postSlide(e),i.animateHeight(),void 0):(c!==!0?i.animateSlide(h,function(){i.postSlide(e)}):i.postSlide(e),void 0)))},b.prototype.startLoad=function(){var a=this;a.options.arrows===!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.hide(),a.$nextArrow.hide()),a.options.dots===!0&&a.slideCount>a.options.slidesToShow&&a.$dots.hide(),a.$slider.addClass("slick-loading")},b.prototype.swipeDirection=function(){var a,b,c,d,e=this;return a=e.touchObject.startX-e.touchObject.curX,b=e.touchObject.startY-e.touchObject.curY,c=Math.atan2(b,a),d=Math.round(180*c/Math.PI),0>d&&(d=360-Math.abs(d)),45>=d&&d>=0?e.options.rtl===!1?"left":"right":360>=d&&d>=315?e.options.rtl===!1?"left":"right":d>=135&&225>=d?e.options.rtl===!1?"right":"left":"vertical"},b.prototype.swipeEnd=function(){var c,b=this;if(b.dragging=!1,b.shouldClick=b.touchObject.swipeLength>10?!1:!0,void 0===b.touchObject.curX)return!1;if(b.touchObject.edgeHit===!0&&b.$slider.trigger("edge",[b,b.swipeDirection()]),b.touchObject.swipeLength>=b.touchObject.minSwipe)switch(b.swipeDirection()){case"left":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide+b.getSlideCount()):b.currentSlide+b.getSlideCount(),b.slideHandler(c),b.currentDirection=0,b.touchObject={},b.$slider.trigger("swipe",[b,"left"]);break;case"right":c=b.options.swipeToSlide?b.checkNavigable(b.currentSlide-b.getSlideCount()):b.currentSlide-b.getSlideCount(),b.slideHandler(c),b.currentDirection=1,b.touchObject={},b.$slider.trigger("swipe",[b,"right"])}else b.touchObject.startX!==b.touchObject.curX&&(b.slideHandler(b.currentSlide),b.touchObject={})},b.prototype.swipeHandler=function(a){var b=this;if(!(b.options.swipe===!1||"ontouchend"in document&&b.options.swipe===!1||b.options.draggable===!1&&-1!==a.type.indexOf("mouse")))switch(b.touchObject.fingerCount=a.originalEvent&&void 0!==a.originalEvent.touches?a.originalEvent.touches.length:1,b.touchObject.minSwipe=b.listWidth/b.options.touchThreshold,a.data.action){case"start":b.swipeStart(a);break;case"move":b.swipeMove(a);break;case"end":b.swipeEnd(a)}},b.prototype.swipeMove=function(a){var d,e,f,g,h,b=this;return h=void 0!==a.originalEvent?a.originalEvent.touches:null,!b.dragging||h&&1!==h.length?!1:(d=b.getLeft(b.currentSlide),b.touchObject.curX=void 0!==h?h[0].pageX:a.clientX,b.touchObject.curY=void 0!==h?h[0].pageY:a.clientY,b.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(b.touchObject.curX-b.touchObject.startX,2))),e=b.swipeDirection(),"vertical"!==e?(void 0!==a.originalEvent&&b.touchObject.swipeLength>4&&a.preventDefault(),g=(b.options.rtl===!1?1:-1)*(b.touchObject.curX>b.touchObject.startX?1:-1),f=b.touchObject.swipeLength,b.touchObject.edgeHit=!1,b.options.infinite===!1&&(0===b.currentSlide&&"right"===e||b.currentSlide>=b.getDotCount()&&"left"===e)&&(f=b.touchObject.swipeLength*b.options.edgeFriction,b.touchObject.edgeHit=!0),b.swipeLeft=b.options.vertical===!1?d+f*g:d+f*(b.$list.height()/b.listWidth)*g,b.options.fade===!0||b.options.touchMove===!1?!1:b.animating===!0?(b.swipeLeft=null,!1):(b.setCSS(b.swipeLeft),void 0)):void 0)},b.prototype.swipeStart=function(a){var c,b=this;return 1!==b.touchObject.fingerCount||b.slideCount<=b.options.slidesToShow?(b.touchObject={},!1):(void 0!==a.originalEvent&&void 0!==a.originalEvent.touches&&(c=a.originalEvent.touches[0]),b.touchObject.startX=b.touchObject.curX=void 0!==c?c.pageX:a.clientX,b.touchObject.startY=b.touchObject.curY=void 0!==c?c.pageY:a.clientY,b.dragging=!0,void 0)},b.prototype.unfilterSlides=b.prototype.slickUnfilter=function(){var a=this;null!==a.$slidesCache&&(a.unload(),a.$slideTrack.children(this.options.slide).detach(),a.$slidesCache.appendTo(a.$slideTrack),a.reinit())},b.prototype.unload=function(){var b=this;a(".slick-cloned",b.$slider).remove(),b.$dots&&b.$dots.remove(),b.$prevArrow&&"object"!=typeof b.options.prevArrow&&b.$prevArrow.remove(),b.$nextArrow&&"object"!=typeof b.options.nextArrow&&b.$nextArrow.remove(),b.$slides.removeClass("slick-slide slick-active slick-visible").css("width","")},b.prototype.unslick=function(){var a=this;a.destroy()},b.prototype.updateArrows=function(){var b,a=this;b=Math.floor(a.options.slidesToShow/2),a.options.arrows===!0&&a.options.infinite!==!0&&a.slideCount>a.options.slidesToShow&&(a.$prevArrow.removeClass("slick-disabled"),a.$nextArrow.removeClass("slick-disabled"),0===a.currentSlide?(a.$prevArrow.addClass("slick-disabled"),a.$nextArrow.removeClass("slick-disabled")):a.currentSlide>=a.slideCount-a.options.slidesToShow&&a.options.centerMode===!1?(a.$nextArrow.addClass("slick-disabled"),a.$prevArrow.removeClass("slick-disabled")):a.currentSlide>=a.slideCount-1&&a.options.centerMode===!0&&(a.$nextArrow.addClass("slick-disabled"),a.$prevArrow.removeClass("slick-disabled")))
},b.prototype.updateDots=function(){var a=this;null!==a.$dots&&(a.$dots.find("li").removeClass("slick-active"),a.$dots.find("li").eq(Math.floor(a.currentSlide/a.options.slidesToScroll)).addClass("slick-active"))},b.prototype.visibility=function(){var a=this;document[a.hidden]?(a.paused=!0,a.autoPlayClear()):(a.paused=!1,a.autoPlay())},a.fn.slick=function(){var g,a=this,c=arguments[0],d=Array.prototype.slice.call(arguments,1),e=a.length,f=0;for(f;e>f;f++)if("object"==typeof c||"undefined"==typeof c?a[f].slick=new b(a[f],c):g=a[f].slick[c].apply(a[f].slick,d),"undefined"!=typeof g)return g;return a},a(function(){a("[data-slick]").slick()})});