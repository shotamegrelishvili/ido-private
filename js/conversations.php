<?
header('Content-type: application/javascript');
include("../include/config.php");
?>
		do_dont_show = true;
		var enx = {};
		window.enx = enx;
		enx.messages = new Object();
		enx.messages.show_progress = function() {
			$('.progress-indicator-icon-message').show();
			$('.message-form-control .send-button').hide();
		};

		enx.messages.hide_progress = function() {
			$('.progress-indicator-icon-message').hide();
			$('.message-form-control .send-button').show();
		};

		enx.messages.remove_attachment = function() {
				if (!$('#attachment_id').val()) return;
				$('#toggle-attach').toggle().toggleClass('secondary');
				$('#attachment-container').slideToggle(function(){
					$('#pickfiles').toggle();
				});
				$('#filelist').html('');
				$('#toggle-attach .open,#toggle-attach .close').toggle();
				$('.progress-tag,.progress-percent,.terms').hide();
				$('#attachment_id').val('');
		};

		function GetHeight() {
		var y = 0;
		if (self.innerHeight)
		{
		y = self.innerHeight;
		}
		else if (document.documentElement && document.documentElement.clientHeight)
		{
		y = document.documentElement.clientHeight;
		}
		else if (document.body)
		{
		y = document.body.clientHeight;
		}
		return y;
		}
		
		try {
		$.scrollTo($('#new_message').position().top + $('#new_message').height() - GetHeight() + 5)
		} catch(err) {
		}
		
		$("#toggle-attach").click(function() { 
			$("#attachment-container").slideToggle();
			$(".terms").toggle();
			$(".open").toggle();
			$(".close").toggle();
			$("#toggle-attach").toggleClass('toggle-close');
			$("#toggle-attach").toggleClass('secondary');
			$('#attachment_id').val('');

			try {

			$.scrollTo($('#new_message').position().top + $('#new_message').height() -  GetHeight() + 5);
			} catch(err) {
			}
			return false;
			});

			$('.message-form-control .send-small').click(function() {
				enx.messages.show_progress();
				return true;
				});

			$('.remove_attachment').click(function() {
				 enx.messages.remove_attachment();
				 enx.messages.show_tos();
				});

		$("#new_message").submit(function() {
		$('.msg-error, #toggle-attach').hide();
		document.getElementById('message_validation_error').innerHTML = '';
		var msgusr = $("#message_body").val();
		enx.messages.show_progress();
		if(msgusr.length >= 1)
		{
			$.post($(this).attr('action'), $(this).serialize(),function(data){
				$('#msgresults').show().html(data);
				enx.messages.hide_progress();
				$('#toggle-attach').show();
			});
		}
		else
		{
			document.getElementById('message_validation_error').innerHTML = "<?php echo $lang['257']; ?>";
			$('.msg-error').show();
			enx.messages.hide_progress();
			$('#toggle-attach').show();
		} 
			return false;
			});

		$('.spam-message').click(function(e) {
			e.preventDefault();
			$.post($(this).data('href'), null, null, "script");
			return false;
			});
			
		$('.compose').click(function() {
				$('#message_body').focus();
				$.scrollTo('#new_message');
				return false;
				});

		$('.remove_attachment').click(function() {
				$("#attachment_id").val("");
				$("#attached_file_name").html("");
				$('.files-added').hide();
				});
		$('.message-form-control .send-small').click(function() {
				enx.messages.show_progress();
				return true;
		});
		
		$('.page_refresh').live("click", function() {
				window.location.reload();
				});
		$('.new_message_toggler').click(function() {
				$("#new_message").toggle();
				return false;
				});
	 $('.upload-cancel').live("click", function() {
				$('#toggle-attach').toggle().toggleClass('secondary');
				$('#attachment-container').slideToggle(function(){
					$('#pickfiles').toggle();
				});
				$('#filelist').html('');
				$('#toggle-attach .open,#toggle-attach .close').toggle();
				$('.progress-tag,.progress-percent,.terms').hide();
				$('#attachment_id').val('');
				return false;
	 });

$.easing.elasout = function(x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
				};


// Run periodical update
$(document).ready( function() {
var poll_handler = $.PeriodicalUpdater(
	base_url+'/check_new', 
	{
		method: 'get',
		data: {last_id: last_id, uid: USERID},
		minTimeout: 1000, 
		maxTimeout: 256000,
		multiplier: 2,
		type: 'text', 
		maxCalls: 0,   
		autoStop: 0 
	}, 
	function(data) {
		if ( eval(data) > 0) {
		poll_handler.stop();
		//$('body').append('<div class="refresh-session hide">'+lang242+' <a class="page_refresh">'+lang243+'</a></div>'); $('.refresh-session').slideDown('slow');
		location.href=base_url+'/conversations/'+conversationWith+'?scrollTo='+last_id;
		} 
	}
	);

	if (scrollTo) {
		$.scrollTo( 0 );
		sT = $('#mid'+scrollTo).offset().top - 107;
		$.scrollTo(sT, 10);
	}
}
);

$('#message_body').keydown(function (e) {

	if (e.ctrlKey && e.keyCode == 13) {
		$('.send-button').click();
	}
});

// PLUPLOAD

var uploader = new plupload.Uploader({
	runtimes : 'gears,html5,flash,silverlight,browserplus',
	browse_button : 'pickfiles',
	multi_selection : false,
	max_file_size : '10mb',
	url : base_url+'/upload.php',
	flash_swf_url : base_url+'/js/plupload212/plupload.flash.swf',
	silverlight_xap_url : base_url+'/js/plupload212/plupload.silverlight.xap',
	filters : [
		{title : "Image files", extensions : "jpg,jpeg,gif,png,tif,bmp"},
		{title : "Zip files", extensions : "zip"},
		{title : "Rar files", extensions : "rar"},
		{title : "Video files", extensions : "avi,mpeg,mpg,mov,rm,3gp,flv,mp4"},
		{title : "Audio files", extensions : "mp3,wav,wma,ogg"},
		{title : "Documents", extensions : "doc,docx,xls,xlsx,pdf,ppt,rtf"}
	]
});
//jpeg, jpg, gif, png, tif, bmp, avi, mpeg, mpg, mov, rm, 3gp, flv, mp4, txt, zip, rar, mp3, wav, wma áƒ“áƒ ogg

uploader.bind('Init', function(up, params) {
	// $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
	$('.nofiles').hide();
	$('.progress-tag,.progress-percent,#toggle-attach').toggle();
	for (var i in files) {
		$('#filelist').html('<span id="' + files[i].id + '"><i class="icon-paperclip icon-large"></i> <strong>' + files[i].name + '</strong> (' + plupload.formatSize(files[i].size) + ') <a href="javascript:" class="upload-cancel selected button tiny radius alert subheading"><span class="close">'+$('.upload .close').html()+'</span></span>');
	}
	$('.toggle-close').hide();
	$('#pickfiles').toggle();
	uploader.start();
});

uploader.bind('UploadProgress', function(up, file) {
	//$('#'+file.id+'>b').html('<span>' + file.percent + "%</span>");
	$('.progress-percent').attr('data-value',file.percent).css('width',file.percent+'%');
	$('.progress-tag').val(file.percent);
});

uploader.bind('FileUploaded', function(up, file, info) {
	if (info.status==200) {
		$('.progress-tag,.progress-percent,.upload-header').fadeOut('slow');
		$('.terms').slideUp();
		var r = $.parseJSON(info.response);
		$('#attachment_id').val(r.fid);
	}
});