$('.faq li>a').click(function(e) {
	if ($(this).attr('href') == '#') {
		e.preventDefault();
		var time = 300;
		$(this).next('div').slideToggle(time);
		$(this).parent().siblings().find('div').slideUp(time);
	}
});

var QueryString = function () {
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = pair[1];
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]], pair[1] ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(pair[1]);
    }
  } 
    return query_string;
} ();

if (QueryString.t) {
	var q = $('a[data-t="'+QueryString.t+'"]');
	q.trigger('click');
  setTimeout(function(){
    $.scrollTo(q);
  },100);
}