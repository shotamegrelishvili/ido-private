function d(t) {
	//if (typeof debug != 'undefined' && debug)
		console.log(t);
}

// Top Menu
var startX,startY,inAnim=false,inMenu=false,sScroll=0,scrollFlag=true,scY,fireF;
	
// Show side menu
$('.menu-icon').click(function(){
	scY = $(window).scrollTop();
	$('.side-bar').addClass('side-bar-visible');
	$('.qp-ui-mask-modal').addClass('qp-ui-mask-visible');
	$('html,body').addClass('disable-scroll');
	$('.zopim').css('z-index',3);

	inMenu=true;
	$('body').on('touchstart',function(e){
		if (!inMenu) return;
		startX = e.originalEvent.touches[0].pageX;
		startY = e.originalEvent.touches[0].pageY; 
	});
	$('body').on('touchmove',function(e){
		if (!inMenu) return;
		var dX = e.originalEvent.touches[0].pageX - startX;
		var dY = e.originalEvent.touches[0].pageY - startY;
		if (!inAnim && dY < 20 && dX < -20) {
			// hide Menu
			//inAnim = true;
			$('.qp-ui-mask-modal').trigger('click');
			/*
			$('.side-bar').animate({
				opacity: 0,
				left: "-241px"
			},200, function(){
				$(this).css({'left':0,'opacity':1});
				$('.qp-ui-mask-modal').trigger('click');
				inAnim=false;
			});
			*/
		}

	});
});

// Notifications Ajax Loader
$('.tb-notifications-activate').click(function(){
	$('.tb-notifications-pm').load(base_url+'/include/functions/ajax/notifications.php');
})

// Show More menu
$('.pm-dropdown').click(function(){
	$('.pm-dropdown').removeClass('tb-dropdown-active');
	$(this).addClass('tb-dropdown-active');
	$('html').on('click',function(e){
		if ($(e.target).parents().hasClass('pm-dropdown')) {
			return;
		}
		else {
			$('.pm-dropdown').removeClass('tb-dropdown-active');
			$(this).off('click');
		}
	});
	$( '.paper-menu' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {

		var e0 = e.originalEvent,
			delta = e0.wheelDelta || -e0.detail;
		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
		e.preventDefault();
		e.stopPropagation();
	});
});

$('.paper-menu').on('click','.close-pm',function(e){
	$('.pm-dropdown').removeClass('tb-dropdown-active');
	e.stopPropagation();
})

// Show Search on Top menu
$('.top-bar-search-activate').click(function(){
	if ($('#query').val()) {
		$('.top-bar-search').submit();
		return;
	}
	scrollFlag = false;
	scY = $(window).scrollTop();
	$('html,body').addClass('disable-scroll');

	$(this).keyup(function(e) {
	  if (e.keyCode == 27) { 
		$('.qp-ui-mask-modal').click();
	  }
	});
	if (!$('.top-bar').hasClass('top-bar-fixed'))
	{
		$('.top-bar').addClass('top-bar-fixed');
		$('.title-area').hide();
		$('.fixed-title-area').show();
	}
	$('.top-bar').css('z-index',5).addClass('tb-sa');
	//$('#top-bar-activate-search .icon-search').css('left','0');
	$('.qp-ui-mask-modal').addClass('qp-ui-mask-visible');
	$('form.top-bar-search').addClass('top-bar-search-active');
	$('form.top-bar-search input').focus();
});

// Top Menu / Side menu Touch Controls
$('#sb-menu-swing-layer').on('touchstart',function(e){
	startX = e.originalEvent.touches[0].pageX;
	startY = e.originalEvent.touches[0].pageY;
})
$('#sb-menu-swing-layer').on('touchmove',function(e){
	var dX = e.originalEvent.touches[0].pageX - startX;
	var dY = e.originalEvent.touches[0].pageY - startY;

	if (dX > 20 && dY < 10) {
		// Main menu call
		$('.menu-icon').trigger('click');
	}
})

// Remove Top/Side Menu
$('.qp-ui-mask-modal').click(function(){
	if (!fh || $('nav.top-bar').hasClass('top-bar-regular')) {
		$('.top-bar').removeClass('top-bar-fixed');
	}
	$('#query').val('');
	$('html,body').removeClass('disable-scroll');
	inMenu=false;
	//$('body').off('touchstart').off('touchmove').removeClass('disable-scroll');
	$('.menu-icon').show();
	$('.modal-window').fadeOut();
	$('.top-bar').removeClass('tb-sa').css('z-index',3);
	$('.zopim').css('z-index',10002);
	$('.side-bar').removeClass('side-bar-visible');
	$('#top-bar-search-activate .icon-search').css('right','10');
	$('form.top-bar-search').removeClass('top-bar-search-active');
	$('.qp-ui-mask-modal').removeClass('qp-ui-mask-visible');
	$.scrollTo(scY, 0);
	scrollFlag = true;
	if (fireF) {
		window[fireF]();
	}
})

// Show Top Menu on Scroll
$(window).scroll(function(e){
	if (!scrollFlag) return;
	if ($(this).scrollTop() <= sScroll) {
		if ($(this).scrollTop() == 0) {
			$('.top-bar').removeClass('top-bar-fixed');
			if (!fh) {
				$('.title-area').show();
				$('.fixed-title-area').hide();
			}
		}
		else {
			showFixedMenu();
		}
		sScroll = $(this).scrollTop();
	}
	else {
		if (window.screen.availWidth >= '1024') {
			if (!$('.top-bar').hasClass('top-bar-fixed')){
				showFixedMenu();
			}
			else if ($(this).scrollTop() == 0 && $('.top-bar').hasClass('top-bar-fixed')) {
				$('.top-bar').removeClass('top-bar-fixed');
			}
			return;
		}
		sScroll = $(this).scrollTop();
		$('.top-bar').removeClass('top-bar-fixed');
	}
});

function showFixedMenu(){
	$('.top-bar').addClass('top-bar-fixed');
	$('.title-area').hide();
	$('.fixed-title-area').show();
	$('.menu-icon').show();
}

// Side Menu Controls
$('.has-dropdown').click(function() {
	$(this).toggleClass('has-dropdown-active');
	$('.'+$(this).data('target')).slideToggle('swing');
})
// End Side Menu Controls

// Main Action menu

// End Main Action menu

// Get deals-wrapper from LocalStorage on NavigationBack
if (localStorage['dc-nav'] && typeof(page)!='undefined' && page == localStorage['dc-nav-page']  && $('.deals-wrapper').is(':visible') && localStorage['deals-wrapper'] && localStorage['dc-nav-timer'] > Math.round((new Date()).getTime() / 1000) - 120) {
	$('.deals-wrapper').html(localStorage['deals-wrapper']);
	var dT = localStorage['dc-nav'] - 120;
	setTimeout(function(){
		window.scrollTo(0,dT);
	},10)
	
	localStorage['deals-wrapper'] = '';
	localStorage['dc-nav'] = '';
	localStorage['dc-nav-page'] = '';
	localStorage['dc-nav-timer'] = '';
}
// End NavBack


$(document).ready( function() {
	if (typeof(def_js)!="undefined") {
		var def_js_length = def_js.length;
		for (var i = 0; i < def_js_length; i++) {
		  var e = document.createElement('script');
		  e.src = def_js[i];
		  document.body.appendChild(e);
		}
	}

	GeoKBD.map('', '.geo', 'geo_flag'); 
	var triggers = $(".modalInput").overlay({
	  // some mask tweaks suitable for modal dialogs
	  mask: {
		color: '#eaeaea',
		loadSpeed: 200,
		opacity: 0.95
	  },

	  closeOnClick: true
	});

// Catch navigation event for Load More Ajax
/*
	if (typeof history.pushState === "function") {
		//window.onpopstate = function () {
			console.log(history.state)
			localStorage['dc-nav-active'] = '1';
			//history.go(-1);
		};
		//history.pushState('j'+Math.random(), null, null);
	}
	else {
		var ignoreHashChange = true;
		window.onhashchange = function () {
			if (!ignoreHashChange) {
				ignoreHashChange = true;
				window.location.hash = Math.random();
				localStorage['dc-nav-active'] = true;
				history.go(-1);
				// Detect and redirect change here
				// Works in older FF and IE9
			}
			else {
				ignoreHashChange = false;   
			}
		};
	}
*/
// end Catch navigation event for Load More Ajax

// Facebook SDK
	if (!FB_isFan && !FB_isFan_delayed && !FB_Fan_hide) {
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=592711610773734&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
		if (main_page) t1 = 30000;
		else t1 = 10000;
		setTimeout(function() {
			if (!$('.joyride-tip-guide').is(':visible')) {
				scY = $(window).scrollTop();
				$('html,body').addClass('disable-scroll');
				$('.qp-ui-mask-modal').addClass('qp-ui-mask-visible');
				$('#fb-like-wrapper').fadeIn('slow');
				fireF = 'isFan_cookie';
			}
		}, t1);

		// FB Liked our page
		window.fbAsyncInit = function() {
			FB.Event.subscribe('edge.create', function(response){
				if (response)
					$.post(base_url+'/include/functions/ajax/FB.php', {action : 'fan'}, function(d){
						if (d=='ok') {
							fireF = '';
							$('#fb-like-wrapper').addClass('fb-say-something');
						}
					});
			});
		}
	}
	// end FB Liked our page
});

// Modal Windows Part
$('.modal-close').click(function() {
	if ($(this).data('id'))
		$('#'+$(this).data('id')).hide();
	if ($(this).data('qp-active'))
		$('.qp-ui-mask-modal').trigger('click');
	if ($(this).data('cookie')) {
		createCookie($(this).data('cookie'),$(this).data('cookie-value'),1);
	}
})
// end Modal Windows Part

// Temporary cookie for FB_isFan
function isFan_cookie() {
	if (FB_isFan) return;
	var date = new Date();
	createCookie('FB_isFan_delayed',(parseInt(date.getTime()/1000)+180),1);
}

// Deal Card Controls
$('.deals-wrapper').on('click','.deal-card', function(){
	if (typeof page != 'undefined') {
		localStorage['deals-wrapper'] = $('.deals-wrapper').html();
		localStorage['dc-nav'] = parseInt($(this).offset().top);
		localStorage['dc-nav-page'] = page;
		localStorage['dc-nav-timer'] = Math.round((new Date()).getTime() / 1000);
	}
	if ($(this).data('target'))
		window.location = $(this).data('target');
})
// end Deal Card Controls

// Rest
var notify_cookie=0;

jQuery.ajaxSetup({ 
	'beforeSend': function(xhr) {
		xhr.setRequestHeader("Accept", "text/javascript")
	}
})
$(document).ready( function(){
	$('.geo').not('.hide-geo-span').before('<span class="geo-box-span" title="Georgian/English keyboard switcher">GE</span>');
	$('a.contact-support').click(function() { $('.contact-form').toggle(); return false; });
	$('textarea#suggestion_content').click(function() { $('.suggest-category-select').show(); });
		$('.browser-notice .close span').click(function() { $('.browser-notice').fadeOut(); });
		$('a.question').click(function() {
	  $(this).parent().next().toggle();
	 return false;
	});
	$('#conversations_quick_navigation').change(function(){
	  if ($(this).val() != "Quick navigation") {
		window.location = base_url + '/inbox?' + $(this).val();
	  }
	  });
	toggleSmallSearchBoxHint();
function toggleSmallSearchBoxHint(){
	if( $('.swap-value-small-search').val() == '' ){
		$('.swap-value-small-search').css("background","#fff url(" + base_url +"/images/bg-small-search-box-idle.gif) repeat-x");
	}
	else{
		$('.swap-value-small-search').css('background','#fff url(' + base_url +'/images/bg-input02.gif) repeat-x');     
	}   
} 
$('.swap-value-small-search').focus(function(){
	$('.swap-value-small-search').css('background','#fff')
});
$('.swap-value-small-search').blur(function(){
	toggleSmallSearchBoxHint();
});
$('.column3 .more-options').click(function() {
	$(this).next().toggle('fast');
	return true;
});
$('.edit-gig-form .button').click(function() {
		$('.edit-gig-form .button').hide();
	$('.progress-indicator-icon-gigform').show();
	return true;
});
$('.inbox-compose .button').click(function() {
		$('.inbox-compose .button').hide();
	$('.progress-indicator-icon-message').show();
	return true;
});
$('.thank-form .button').click(function() {
		$('.thank-form .button').hide();
	$('.progress-indicator-icon-review').show();
	return true;
});
$('.settings-form .btn-update').click(function() {
		$('.settings-form .btn-update').hide();
	$('.settings-form .progress-indicator-icon-gigform').show();
	return true;
});
$('.reset-form .btn-change').click(function() {
		$('.reset-form .btn-change').hide();
	$('.reset-form .progress-indicator-icon-gigform').show();
	return true;
});
$('.notify-form .row .button').click(function() {
		$('.notify-form .row .button').hide();
	$('.notify-form .row .progress-indicator-icon-notifycomp').show();
	return true;
});
$('.suggest-form .button').click(function() {
	if ($('#suggest_form #suggestion_content').val() == '') {
	return false;} else {
	$('.suggest-form .button').hide();
	$('.suggest-form .suggest-progress-indicator-icon-message').show();
	return true;}
});
$('.row01 .progress-indicator-icon-message').click(function() {
		$('.row01 .button').hide();
	$('.row01 .progress-indicator-icon-message').show();
	return true;
});
function expandSideMenu(){
	$('#side_menu_expanded').toggle("fast", toggleMoreToLess);
}
$('#side_menu_expander').click(function() {
	expandSideMenu();
	return false;
});
function toggleMoreToLess() {
	if ($('#side_menu_expander').text() == "More..."){
		$('#side_menu_expander').text("Less...");
	}else{
		$('#side_menu_expander').text("More...");           
	}
}
$('#login-button').click(function() {
});
$('#join-button').click(function() {
	$('.register-popup').toggle();
	$('.login-popup').hide();
	$("input:text:visible:first").focus();
	return false;
});
$('a.show_join_popup').click(function() {
	$('.register-popup').show();
	return false;
});
$('a.join-close-button').click(function() {
	$('.register-popup').hide();
	return false;
});
$('#suggest_form').submit(function() {
	if ($.inArray($('#suggest_form #content').val(),swapValues) != '-1') {
		return false;
	} else {
		$.post(this.action, $(this).serialize(), null, "script");
		return false;
	}
});
$('#submit_new_gig').submit(function() {
	if ($.inArray($('#submit_new_gig #quicktitle').val(),swapValues) != '-1') {
		return false;
	} else {
		return true;
	}
});
$('#search_form').submit(function() {
	if ($.inArray($('#search_form #query').val(),swapValues) != '-1') {
		return false;
	} else {
		return true;
	}
});
$('input.checkbox').on('click', function(e) {
	$(this).next('.i-checkbox').toggleClass('icon-check').toggleClass('icon-check-empty');
})
$('a.select-all').on('click',function() {
	selectCheckboxes();
	return false;
});

$('a.select-none').click(function() {
	unselectCheckboxes();
	return false;
});
$('a.select-active').click(function() {
	selectActive();
	deal_controls();
	return false;
});
$('a.select-suspended').click(function() {
	unselectCheckboxes();
	$('.checkbox.suspended').each(function(){
		$(this).attr('checked', true);
	});
	return false;
});
$('a.select-pending').click(function() {
	selectPending();
	deal_controls();
	return false;
});
$('a.select-read').click(function() {
	unselectCheckboxes();
	$('.checkbox.read').each(function(){
		$(this).attr('checked', true);
	});
	return false;
});
$('a.select-unread').click(function() {
	unselectCheckboxes();
	$('.checkbox.unread').each(function(){
		$(this).attr('checked', true);
	});
	return false;
});

$('.btn-mark-as-read').click(function() {
	if ($('.checkbox:checked').size() > 0) {
		$('#messages_form').attr('action',$('#messages_form').attr('action')+'/read_selected');
		$('#messages_form').submit();
	} else {
		return false;
	}
});
$('a.forgotpassword').click(function() {
	$('div.loginwrapper').hide();
	$('div.forgotpasswordform').show();
	return false;
});
$('a.backtologin').click(function() {
	$('div.forgotpasswordform').hide();
	$('div.loginwrapper').show();
	return false;
});

$('input[type=text],textarea').focus(function(){
	if ($(this).hasClass('has-do-dont')) {
		if (typeof do_dont_show == 'undefined' || do_dont_show) {
			$('.do-dont').slideDown(200,function(){
				$(this).addClass('do-dont-visible');
				var sT = $('.do-dont-title').offset().top-57;
				$.scrollTo(sT,200);
			});
		}
	}
});
$('input[maxlength],textarea[maxlength]').keyup(function(){
	var max = parseInt($(this).attr('maxlength'));
	if($(this).val().length > max){
		$(this).val($(this).val().substr(0, max));
		if ($('.maxlength-error-notice').length==0) {
		  $(this).css('margin-bottom','0').after('<span class="maxlength-error-notice subheading">'+lang['maxlength']+max+'</span>');
		}
	}
	else
		$('.maxlength-error-notice').remove();
});
$('a.addbookmark').click(function() {
	$.ajax({
		url: this.href,
		dataType: "script"
	});
	$('#addbookmark').parent().hide();
	$('#removebookmark').parent().show();
	return false;
});
$('a.removebookmark').click(function() {
	$.ajax({
		url: this.href,
		dataType: "script"
	});
	$('#removebookmark').parent().hide();
	$('#addbookmark').parent().show();
	return false;
});
$('a.changepassword').click(function() {
	$('.reset-form').toggle('fast');
	return false;
});
$('a.shareit').click(function() {
	if ($(this).parent().parent().next('.pop-share').css('display') == "none") {
		$(this).parent().parent().next('.pop-share').show();
	} else {
		$(this).parent().parent().next('.pop-share').hide();
	}
	return false;
});
$('a.pshare').click(function() {
	if ($(this).next('.p-share').css('display') == "none") {
		$(this).next('.p-share').show();
	} else {
		$(this).next('.p-share').hide();
	}
	return false;
});
$("#user_username").keyup(function() {
	checkUsername();
});
$('#join_form').submit(function() {
document.getElementById("status").style.display="none";
$('#joinresults').html('');
$('#join_form .button').hide();
$('#join_form .progress-indicator-icon-join').show();
$.post($(this).attr('action'), $(this).serialize(), function(data) {
  $('#joinresults').html(data);
  $('#join_form .progress-indicator-icon-join').hide();
  $('#join_form .button').show();
});
return false;
});
$('#session_form').submit(function() {
	if($('#session_form #l_username').val() == '' && $('#session_form #l_password').val() == ''){
		return false;
	}
$('#logresults').html('');
$('#session_form .button').hide();
$('#session_form .progress-indicator-icon-login').show();
$.post($(this).attr('action'), $(this).serialize(),function(data){
	$('#logresults').html(data);
	$('#session_form .progress-indicator-icon-login').hide();
	$('#session_form .button').show();
});
return false;
});
$('#forgot_password_form').submit(function() {
$('#fpresults').html('');
$('#forgot_password_form .button').hide();
$('#forgot_password_form .progress-indicator-icon-login').show();
$.post($(this).attr('action'), $(this).serialize(),function(data){
	$('#fpresults').html(data);
	$('#forgot_password_form .progress-indicator-icon-login').hide();
	$('#forgot_password_form .button').show();
});
return false;
});

	if (ISUSER) {
		/*
		var timestamp;
		function callNode() {
			$.ajax({
				url: base_url+'/include/functions/ajax/cabinet.php',
				dataType: "jsonp",
				data: {"timestamp":timestamp},
				jsonpCallback: "_testcb",
				cache: false,
				timeout: 35000,
				success: function(response, code, xhr) {
					if ('ok' == response) {
						console.log(response);
						callNode();
						return false;
					}

					console.log(response);

					timestamp = response.time;
					// make new call
					callNode();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log('error ' + textStatus + " " + errorThrown);
				}
			});
		}
		$(function () {
			setTimeout(callNode, 1); //call function with setTimeout in order to avoid ugly constant browser loading 
		});
		*/
		$.PeriodicalUpdater(base_url+'/include/functions/ajax/cabinet.php',{minTimeout:1000,maxTimeout:60000,multiplier:5,verbose:0},function(rdata){
			data = $.parseJSON(rdata);
			if (data.status == 'OK') {
				notify = readCookie('notify');
				if (notify === null || notify != data.nCC) {
					createCookie('notify',data.nCC,1);
					notify_cookie = notify;
					if (data.nCC > 0) {
						var audioElement = document.createElement('audio');
						audioElement.setAttribute('src', base_url+'/res/notify.ogg');
						audioElement.setAttribute('autoplay', 'autoplay');
						$('.tb-notifications-activate').addClass('tb-n-active');
					}
					else {
						$('.tb-notifications-activate').removeClass('tb-n-active');
					}
					if (data.nCC > 9)
						$('#tb-n').text('9+');
					else
						$('#tb-n').text(data.nCC);
				}
			}
		});
		
		// Cookie evaluation for multiple windows
		setInterval(function(){
			notify = readCookie('notify');
			if (notify === null) notify = 0;
			if (notify_cookie != notify) {
				if (notify > 9)
					$('#tb-n').text('9+');
				else
					$('#tb-n').text(notify);
				if (notify > 0) $('.tb-notifications-activate').addClass('tb-n-active');
				else $('.tb-notifications-activate').removeClass('tb-n-active');
				notify_cookie = notify;
			}
		},1000);
	}
});
pic1 = new Image(16, 16);
pic1.src = base_url + "/images/loader.gif";
function checkUsername() {
	var usr = $("#user_username").val();
	if(usr.length >= 4)
	{
		$("#status").html('<div class="status-checking"><img align="absmiddle" src="' + base_url + '/images/loader.gif" /> Checking availability...</div>');
		$.ajax({
			type: "POST",
			url: base_url + "/checkuser.php",
			data: "username="+ usr,
			success: function(msg){
				$("#status").ajaxComplete(function(event, request, settings){
					if(msg == 'OK')
					{
						$("#user_username").removeClass('object_error');
						$("#user_username").addClass("object_ok");
						$(this).html(' <div class="status-ok"><img align="absmiddle" src="' + base_url + '/images/bg-valid-name.gif" /></div> ');
					}
					else
					{
						$("#user_username").removeClass('object_ok');
						$("#user_username").addClass("object_error");
						$(this).html(msg);
					}
				});
			}
		});
	}
	else
	{
		$("#status").html('<div class="status-error">Username should have at least 4 characters</div>');
		$("#user_username").removeClass('object_ok');
		$("#user_username").addClass("object_error");
	}
};
function deleteSelectedMessages() {
	if ($('.checkbox:checked').size() > 0) {
		$('#messages_form').attr('action',$('#messages_form').attr('action')+'/delete_selected');
		$('#messages_form').submit();
	} else {
		return false;
	}
	};
function spamSelectedMessages() {
	if ($('.checkbox:checked').size() > 0) {
		$('#messages_form').attr('action',$('#messages_form').attr('action')+'/spam_selected');
		$('#messages_form').submit();
	}else {
		return false;
	}
};
function unselectCheckboxes() {
	$('.checkbox').each(function(){
		$(this).attr('checked', false);
	});
	$('.i-checkbox').removeClass('icon-check').addClass('icon-check-empty');
};
function selectActive() {
	unselectCheckboxes();
	$('.checkbox.active').each(function(){
		$(this).attr('checked', true);
		$(this).next('.i-checkbox').removeClass('icon-check-empty').addClass('icon-check');
	});
}
function selectPending() {
	unselectCheckboxes();
	$('.checkbox.pending').each(function(){
		$(this).attr('checked', true);
		$(this).next('.i-checkbox').removeClass('icon-check-empty').addClass('icon-check');
	});
}
function selectInactive() {
	unselectCheckboxes();
	$('.checkbox.inactive').each(function(){
		$(this).attr('checked', true);
		$(this).next('.i-checkbox').removeClass('icon-check-empty').addClass('icon-check');
	});
}
function reset_html(id) {
	$('#'+id).html($('#'+id).html());
}
function selectCheckboxes() {
	$('.checkbox').each(function(){
		$(this).attr('checked', true);
	});
	$('.i-checkbox').removeClass('icon-check-empty').addClass('icon-check');
}
(function($){
	var url1 = /(^|&lt;|\s)(www\..+?\..+?)(\s|&gt;|$)/g,
	url2 = /(^|&lt;|\s)(((https?|ftp):\/\/|mailto:).+?)(\s|&gt;|$)/g,
	linkifyThis = function () {
		var childNodes = this.childNodes,
		i = childNodes.length;
		while(i--)
		{
			var n = childNodes[i];
			if (n.nodeType == 3) {
				var html = $.trim(n.nodeValue);
				if (html)
				{
					html = html.replace(/&/g, '&amp;')
					.replace(/</g, '&lt;')
					.replace(/>/g, '&gt;')
					.replace(url1, '$1<a href="http://$2" target="_blank">$2</a>$3')
					.replace(url2, '$1<a href="$2" target="_blank">$2</a>$5');
					$(n).after(html).remove();
				}
			}
			else if (n.nodeType == 1  &&  !/^(a|button|textarea)$/i.test(n.tagName)) {
				linkifyThis.call(n);
			}
		}
	};
	$.fn.linkify = function () {
		return this.each(linkifyThis);
	};
})(jQuery);

// Own functions
$('.cats-btn').click(function(){
	//$('.cats-btn').hide();
	$('.cats-2c').slideToggle('fast');
	$(this).children('.cat-btn-toggle').toggle();
});

$('#load-more').click(function() {
  $.get(base_url+'/ajax-loadmore.php', { 'order' : $(this).data('order'), 'start' : $(this).data('start'), 'type' : $(this).data('type'), 'catid' : $(this).data('catid'), 'source' : $(this).data('source') }, function(data) {
	$('.deals-wrapper').append(data);
	$('.fadein').fadeIn('slow',function(){
		$.scrollTo($(this),400);
		$(this).removeClass('fadein');
	});
	
	$('.send-button').show();
	$('.progress-indicator-icon-message').hide();
  });   
})

function createCookie(name, value, days) {
	var expires;

	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
	} else {
		expires = "";
	}
	document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = escape(name) + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

