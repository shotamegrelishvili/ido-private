$('.submit-wrapper').on('click','.send-button',function(e) {
	checkLeave = 0;
	var hasError = false;
	if (typeof $(this).parents('form:first')[0] != 'undefined') {
		var formID = $(this).parents('form:first')[0].id;
		$('form#'+formID+' input, form#'+formID+' select, form#'+formID+' textarea').each(function(index) {
			if (($(this).attr('required') == 'required' && $(this).val() == '') || (typeof $(this)[0].validationMessage != 'undefined' && $(this)[0].validationMessage)) {
				$(this).addClass('field-err')
				hasError=true;
			}
			else {
				$(this).removeClass('field-err')
			}
		})
	}
	
	if (hasError || $('.field-err').length>0) {
		e.preventDefault();
		$('.field-err').get(0).focus(function(){
			return false;
		});
	}
	else {
		loader.Show($(this));
	}
})

var loader = {
	Show : function(elemHide) {
		elemHide.hide();
		$('.progress-indicator-icon-message').show();
	}
}

$('form').on('input','.field-err',function(){
	$(this).removeClass('field-err');
})