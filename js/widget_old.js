idw={
	d : document,
	e : null,
	state : null,
	rows : null,
	links : null,
	wrapperWidth : null,
	wrapperHeight : null,
	Slider : {
		enabled : false,
		multipleCards : false, // not used yet
		rows : 10,
		firstSlideTimeout : 4500,
		nextSlideTimeout : 3200,
		activeCard : null,
		Timer : null,
	},

	//uri : ('https:' == document.location.protocol ? 'https:' : 'http:')+'//ido.ge/include/functions/ajax/widget.php',
	uri : ('https:' == document.location.protocol ? 'https:' : 'http:')+'//'+(document.location.href.indexOf('localhost')>-1 ? 'localhost/ido' : 'ido.ge')+'/include/functions/ajax/widget.php',

	widget_params : {
		ref : null,
		width : 200,
		maxheight : 100,
		layout : 'standard',
		action : 'jobs',
		controls : true,
	},


	Init : function(div) {
		//this.e 
		var widgetDivs = document.querySelectorAll(div);
		if (widgetDivs.item(0) == null) return false;

		for (m in widgetDivs) {
			// ToDo: reset all params
			if (typeof(widgetDivs[m]) == 'object') {
				this.e = widgetDivs[m];
				for (c in this.e.dataset) {
					this.widget_params[c] = this.e.dataset[c];
				}
				this.Load();
			}
		}
	},

	Load : function() {
		var xmlhttp;

		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var self=this;
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				this.state = 'loaded';
				if(xmlhttp.status == 200){
					self.ParseData(xmlhttp.responseText);
				}
				else if(xmlhttp.status == 400) {
					err('There was an error 400')
				}
				else {
					err('something else other than 200 was returned')
				}
			}
		}

		this.state = 'loading';
		this.uriConstructor();

		xmlhttp.open('GET', this.uri, true);
		xmlhttp.send();
	},

	uriConstructor : function() {
		this.uri += '?ref='+encodeURIComponent(this.widget_params['ref'])+'&action='+this.widget_params['action']+'&layout='+this.widget_params['layout']+'&width='+this.widget_params['width']+'&maxheight='+this.widget_params['maxheight']+'&controls='+this.widget_params['controls'];
	},

	ParseData : function(data) {
		this.state = 'parsing';
		if (!data) {
			this.state = 'halt';
			return false;
		}
		//var result = JSON.parse(data);
		this.e.innerHTML = '<iframe name="a" src="'+this.uri+'" allowtransparency="true" frameborder="0" scrolling="no" tabindex="0" style="width: '+this.widget_params['width']+'px; height: '+this.widget_params['maxheight']+'px"></iframe>';
		return;
		this.state = 'parsed';
		// Run After Parse functions
		this.Align();
		this.RunSlider();
	},

	Align : function() {
		this.links = this.d.querySelectorAll("a.idw-a");
		for (m in this.links) {
			if (this.links[m].clientHeight > 32) {
				this.links[m].className += " idw-a-scroll";
			}
		}
		var wrapper = this.d.querySelector(".idw-wrapper");
		this.wrapperHeight = wrapper.clientHeight;
		this.wrapperWidth = wrapper.clientWidth;
		if (this.wrapperWidth < 320) this.d.querySelector(".idw-logo").className += " idw-logo-right";
	},

	RunSlider : function() {
		this.InitSlider();
		if (this.Slider.enabled) {
			var self = this;
			this.Slider.Timer = setTimeout(function(){
				self.LoopSlider();
			},self.Slider.firstSlideTimeout);
		

			this.e.onmouseover = function() {
				clearTimeout(self.Slider.Timer);
			}

			this.e.onmouseleave = function() {
				self.Slider.Timer = setTimeout(function(){
					self.LoopSlider();
				},self.Slider.firstSlideTimeout);
			}
		}
	},

	InitSlider : function() {
		this.rows = this.d.querySelectorAll(".idw-a-r");

		this.Slider.rows = parseInt((this.wrapperHeight - 100) / 30) + 1;
		if (this.Slider.rows < 10) {
			this.Slider.enabled = true;
			this.d.querySelector('.idw-wrapper').className += " idw-wrapper-sw";
		}

		this.ShowFirstSlide();
	},

	ShowFirstSlide : function() {
		i=1;
		for (m in this.rows) {
			this.rows[m].className += " idw-a-rs";
			
			this.Slider.activeCard = this.rows[m].dataset['type'];
			if (i >= this.Slider.rows) break;
			else i++;
		}
	},

	LoopSlider : function() {
		// Find Shown Row(s) and hide them
		var i=0,self=this,lastSlide=false;

		for (m in this.rows) {
			if (hasClass(this.rows[m],'idw-a-rs')) {
				removeClass(this.rows[m], 'idw-a-rs');
				if (typeof(this.rows[i+this.Slider.rows])=='object') {
					this.ShowNewSlide(i+this.Slider.rows);
					break;
				}
				else {
					lastSlide = true;
				}
			}
			else i++;
		}

		if (lastSlide) this.ShowFirstSlide();

		this.Slider.Timer = setTimeout(function(){
		//	self.LoopSlider();
		},self.Slider.nextSlideTimeout);
	},

	ShowNewSlide : function(i) {
		// Show new rows
		if (typeof(this.rows[i])=='undefined') i = 0;

		/*
		for (var m=i; m<=i+this.Slider.rows-1; m++) {
			if (!hasClass(this.rows[m],'idw-t-'+this.Slider.activeCard)) {
				this.d.querySelector('.idw-'+this.Slider.activeCard).className += " idw-hide";
				this.Slider.activeCard = this.rows[m].dataset['type'];
				removeClass(this.d.querySelector('.idw-'+this.Slider.activeCard), 'idw-hide');
			}
			//this.rows[m].className += " idw-block";
			setTimeout(function(){
				self.rows[m-1].className += " idw-a-rs";
			},0)
		}
		*/
		this.rows[i].className += " idw-a-rs";
	}
}

idw.Init(".ido-widget");

function d(t) {
	console.log(t);
}

function err(t) {
	d(t);
}

function hasClass(element, cls) {
	return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function removeClass(element, cls) {
	element.className = element.className.replace(' '+cls,'').replace(cls,'');
}