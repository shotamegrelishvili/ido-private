<?php
include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	
	$PID = intval(cleanit($_REQUEST['id']));
	$multi = intval(cleanit($_REQUEST['multi']));
	$query = "SELECT `PID` FROM `posts` WHERE `PID`='".sql_quote($PID)."' AND `active`='1' AND `is_del`='0' LIMIT 1";
	$executequery=$conn->execute($query);
	if (!$executequery->fields['PID']) {
		$error = $lang['144'];
		$templateselect = "empty.tpl";
		abr('pathURI','/');
		abr('pathTitle',' ');
		abr('heading',$lang['144']);
		abr('subheading',' ');
		$PID = 0;
	}
	else {
		$query = "SELECT `USERID`, `pin`, `pinprotect` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `status`='1' LIMIT 1";
		$executequery=$conn->execute($query);
		if (!$executequery->fields['USERID'] || $executequery->fields['status'] == '0') {
			$no_redir = true;
			include("logout.php");
			$_SESSION['temp']['message_type'] = 'error';
			$_SESSION['temp']['message_title'] = $lang[745];
			header("Location:$config[baseurl]/signin"); exit;
		}
		elseif (!($config['purchase_settings']['allow_purchasing_without_pin'] || strlen($executequery->fields['pin']) == 11)) {
			$_SESSION['temp']['message_type'] = 'error';
			$_SESSION['temp']['message_title'] = $lang[558];
			header("Location:$config[baseurl]/settings?check=1&r=".base64_encode('ordering.php?id='.$PID)); exit;
		}
		elseif (!($config['purchase_settings']['allow_purchasing_with_unverified_pin'] || $executequery->fields['pinprotect'] )) {
			$_SESSION['temp']['message_type'] = 'error';
			$_SESSION['temp']['message_title'] = $lang[744];
			header("Location:$config[baseurl]/settings?check=1&r=".base64_encode('ordering.php?id='.$PID)); exit;
		}
	}
	if($PID > 0)
	{
		$query="INSERT INTO `order_items` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($PID)."'";
		$result=$conn->execute($query);
		$IID = $conn->_insertid($conn);
		if($IID > 0)
		{			
			$query = "SELECT `USERID`, `price`, `ctp` FROM `posts` WHERE `PID`='".sql_quote($PID)."' LIMIT 1";
			$executequery=$conn->execute($query);
			$price = $executequery->fields['price'];
			$ctp = $executequery->fields['ctp'];
			$OWNER = $executequery->fields['USERID'];
			$total = $price;

			$vas_query = "SELECT `VID` FROM `members_vas` WHERE `USERID`='".intval($OWNER)."' AND `service`='zerocommission' AND `status`='1' AND UNIX_TIMESTAMP(`validtill_datetime`) > '".$_SERVER['REQUEST_TIME']."' LIMIT 1";
			$vas = $conn->execute($vas_query);
			if (isset($vas->fields['VID'])) {
				$totacom = 0;
			}
			else
				$totacom = $ctp;
			
			if($multi > 1)
			{
				$total = $price * $multi;
				//$totacom = $ctp * $multi;
				$addmulti = ", multi='".sql_quote($multi)."'";
			}
			
			$query="UPDATE `order_items` SET `totalprice`='".sql_quote($total)."', `ctp`='".sql_quote($totacom)."' $addmulti WHERE `IID`='".sql_quote($IID)."' LIMIT 1";
			$result=$conn->execute($query);
			
			header("Location:$config[baseurl]/order?item=".$IID);exit;
		}
	}
	else {
		abr('error',$error);
		$smarty->display('header.tpl');
		$smarty->display($templateselect);
		$smarty->display('footer.tpl');
	}

}
else
{
	header("Location:$config[baseurl]/");exit;
}
?>