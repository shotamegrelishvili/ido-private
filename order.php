<?php
include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] && ($blocked_till = user_blocked_till($_SESSION['USERID']))) {
	$error = sprintf($lang['662'], $blocked_till);
	$templateselect = "empty.tpl";
	abr('pathURI','order');
	abr('pathTitle',$lang['55']);
	abr('heading',$lang['732']);
	abr('subheading',$lang['733']);
}

elseif ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	if (isset($_POST['econnect_signup']) && $_POST['email'] && $_POST['password']) {
		include_once('libraries/eConnect.class.php');
		$econnectClass = new econnect();

		
		if (!($res=$econnectClass->_Authorize($_REQUEST['email'], $_REQUEST['password']))) {
			$error .= "<li>".$lang['628']."</li>";
		}
		else {
			$econnected_user = $econnectClass->getUserInfo($res->access_token);

			if ($econnectClass->_CheckProtectedFields($_SESSION['USERID'], $econnected_user)) {
				$error .= "<li>".$lang['636']."</li>";
			}
			else {
				$econnectClass->_UpdateSession($res);

				if ($econnectClass->_CheckDuplicateUser($econnected_user) && !$error) {
					$error .= "<li>".$lang['561']."</li>";
				}
				elseif (!$error) {
					$econnectClass->_UpdateUser($econnected_user);
					$_SESSION['temp']['message_title'] = $lang['700'];
					$_SESSION['temp']['message_type'] = 'success';
					//d('processing');
					//$message = $lang['172'];
				}
			}
			//d($_SESSION);
		}
	}

	$templateselect = "order.tpl";
	$pagetitle = $lang['258'];
	abr('pagetitle',$pagetitle);

	
	$iid = intval(cleanit($_REQUEST['item']));
	if($iid > 0)
	{
		$query="SELECT A.`IID`, A.`PID`, A.`totalprice`, A.`multi`, A.`ctp`, B.`gtitle`, B.`days`, B.`USERID` as OWNERID, B.`moneyback` FROM `order_items` A, `posts` B WHERE A.`IID`='".sql_quote($iid)."' AND A.`USERID`='".sql_quote($_SESSION['USERID'])."' AND A.`PID`=B.`PID`";
		$results=$conn->execute($query);
		$p = $results->getrows();
		if (!isset($p[0])) {
			header("Location:{$config[baseurl]}");exit;
		}
		
		abr('p',$p[0]);
		$eid = base64_encode($p[0]['IID']);
		abr('eid',$eid);
		$id = $p[0]['PID'];
		$multi = $p[0]['multi'];

		$query="SELECT count(*) as pending_orders FROM `orders` WHERE `PID`='".intval($id)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' AND (`status`='0' OR `status`='1' OR `status`='4') LIMIT 1";
		$results=$conn->execute($query);
		$o = $results->fields['pending_orders'];
		if ($o > 0) abr('error',$lang['626']);
		
		$query = "SELECT `username`, `funds`, `afunds`, `eemail`,`eemailprotect`, `e_account_type`, `pinprotect` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
		$executequery=$conn->execute($query);
		if ($executequery->fields['e_account_type']=='business') {
			$error = $lang[843];
			abr('eConnect_disabled', true);
		}
		abr('eemail', $executequery->fields['eemail']);
		$funds = $executequery->fields['funds'];
		abr('funds',$funds);
		$afunds = $executequery->fields['afunds'];
		abr('afunds',$afunds);
		$buyer_verified = $executequery->fields['pinprotect'];
		$buyer = $executequery->fields['username'];

// Debug
		if (isset($_SESSION['d']) && $_SESSION['d']) {
			//$_SESSION['access_token'] = 1;
			//$_SESSION['signin_gateway'] = 'econnect';
		}
		if (!$executequery->fields['eemail'] || !$executequery->fields['eemailprotect']) {
			abr('eConnect_required', true);
			abr('eConnect_account_required', true);
		}
		elseif (!isset($_SESSION['signin_gateway']) || $_SESSION['signin_gateway'] != 'econnect' || !$_SESSION['access_token']) {
			abr('eConnect_required', true);
		}
		else {
			include_once('libraries/eConnect.class.php');
			$econnectClass = new econnect();

			$econnected_user = $econnectClass->getUserInfo($_SESSION['access_token']);
			//d($econnected_user);

			// Debug
			if (isset($_SESSION['d']) && $_SESSION['d']) {
				//$econnected_user['email'] = 's.megrelishvili@gmail.com';
			}

			if (!isset($econnected_user['email']) || !$econnected_user['email']) {
				abr('eConnect_required', true);
			}
			else {
				abr('eConnect_canpay',true);
			}
		}
		
		if(FALSE && $_POST['paymentgateway'] == "1") // TURNED OFF
		{ // Balance Payment
			$price = $p[0]['totalprice'];
			if($afunds >= $price)
			{
				$query1 = "UPDATE `members` SET `afunds`=`afunds`-$price WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
				$executequery1=$conn->execute($query1);
				
				if($multi > 1)
				{
					$query = "SELECT `price` FROM `posts` WHERE `PID`='".sql_quote($id)."'"; 
					$executequery=$conn->execute($query);
					$eachprice = $executequery->fields['price'];
		
					for ($i=1; $i<=$multi; $i++)
					{
						$query = "INSERT INTO `orders` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($id)."', `IID`='".sql_quote($iid)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `status`='0', `price`='".sql_quote($eachprice)."'"; 
						$executequery=$conn->execute($query);
						$order_id = $conn->_insertid();

						insert_order_agreement($order_id);

						if($order_id > 0 && $eachprice > 0)
						{
							$query = "INSERT IGNORE INTO `payments` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `OID`='".sql_quote($order_id)."', `IID`='".sql_quote($iid)."', `time`='".$_SERVER['REQUEST_TIME']."', `price`='".sql_quote($eachprice)."', `t`='1', `balance`='1'";
							$executequery=$conn->execute($query);
							
							$query = "UPDATE `posts` SET `rev`=`rev`+$eachprice WHERE `PID`='".sql_quote($id)."' LIMIT 1";
							$executequery=$conn->execute($query);							
						}
					}
					header("Location:{$config[baseurl]}/thank_you?g=".$eid);exit;
				}
				else
				{
					$query = "INSERT INTO `orders` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($id)."', `IID`='".sql_quote($iid)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `status`='0', `price`='".sql_quote($price)."'";
					$executequery=$conn->execute($query);
					$order_id = $conn->_insertid();
					insert_order_agreement($order_id);
					if($order_id > 0)
					{
						if ($price > 0) {
							$query = "INSERT IGNORE INTO `payments` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `OID`='".sql_quote($order_id)."', `IID`='".sql_quote($iid)."', `time`='".$_SERVER['REQUEST_TIME']."', `price`='".sql_quote($price)."', `t`='1', `balance`='1'"; 
							$executequery=$conn->execute($query);
							
							$query = "UPDATE `posts` SET `rev`=`rev`+$price WHERE `PID`='".sql_quote($id)."' LIMIT 1";
							$executequery=$conn->execute($query);	
						}
											
						header("Location:$config[baseurl]/thank_you?g=".$eid);exit;
					}
				}
			}
		}
		elseif ($_POST['paymentgateway']==2) {
			// eMoney Payment
			$price = $p[0]['totalprice'];
			
			// Debug
			//$buyer_verified = 0;

			$real_needed_period_days = ceil(
									$config['working_process']['provide_instructions_days'] // Provide instructions
									+ $p[0]['days'] // Work duration as set in the task description
									+ ceil($config['working_process']['task_late_forget_timestamp'] * ($config['working_process']['max_reject_times']+1) / 86400) // Forget period for delivering the work
									+ ($config['working_process']['review_period'] * ($config['working_process']['max_reject_times'] + 1)) // Review period
									+ (($p[0]['days'] / 100) * $config['revision_period_procent'] * $config['working_process']['max_reject_times']) // Re-delivering the work after the review
									+ $config['working_process']['task_late_grace_period_days'] // Grace period for late delivery
									+ 1); // 1 day more to keep things good
			//d('real: '.$real_needed_period_days);
			//d('verified: '.$buyer_verified);
			if (!$buyer_verified) $add_block_period_days = $config['working_process']['unverifiedmember_add_block_period_days'];
			else $add_block_period_days = 0;
			$escrow_timestamp = strtotime('+'.($config['working_process']['payment_day_of_month']-1).' days', strtotime('first day of +1 month',strtotime('+ '.($real_needed_period_days+$add_block_period_days).' days')));
			$escrow_days = ceil(abs(($escrow_timestamp - $_SERVER['REQUEST_TIME'])/60/60/24));

			// Temporary, while eMoney fixes more than 99 days escrow issue
			//d($escrow_days);
			//d('Ready to pass to eMoney');

			if ($escrow_days > 99) $escrow_days = 99;
			
			if ($price > 0 && in_array($_SERVER['REMOTE_ADDR'], $config['debug_ips'])) { 
				//$process_price = '0.01';
				$process_price = $price;
				$receiver = 'liamegrelishvili@yandex.ru';
			}
			else {
				$process_price = $price;
			// Check if all fields for money receiver are OK
				$ownerquery=$conn->execute("SELECT `USERID`, `email`, `eemail`, `pin`, `pinprotect`, `eemailprotect`,`status` FROM `members` WHERE `USERID`='".sql_quote($p[0]['OWNERID'])."' LIMIT 1");
				$required_fields = '';

				if (!$ownerquery->fields['eemail'] || !$ownerquery->fields['eemailprotect']) {
					$required_fields .= $lang['162'].PHP_EOL;
				}
				if (!$ownerquery->fields['pin'] || !$ownerquery->fields['pinprotect']) {
					$required_fields .= $lang['551'].PHP_EOL;
				}
			// End check
				$receiver = $ownerquery->fields['eemail'];
			}

			if (!$p[0]['ctp'] || $p[0]['ctp']=='0.00') {
				$withoutCommission = true;
			}
			else {
				$withoutCommission = false;
			}

			if ($price)
				$res = $econnectClass->paymentWithHold($receiver, $process_price, 'GEL', $config['site_name'].': "'.$p[0]['gtitle'].'"', $escrow_days, $withoutCommission);
			//d('Res:');
			//d($res);
			//d('TransCode: '.$econnectClass->transactionCode);

			//d('Error: '.$error);
//$res = 0;
//$econnectClass->transactionCode = 1;
			if ($res == '500') {
				include_once($config['basedir'].'/libraries/emailsms.class.php');
				$emailClass = new email();
				$emailClass->send('s.megrelishvili@gmail.com', 'eMoney Error!', 'URI: '.$econnectClass->callUri.PHP_EOL.'USERID: '.$_SESSION['USERID'].PHP_EOL.$econnectClass->_response);
				$error .= "<li>".$lang[842]."</li>";
			}
			elseif ($price > 0 && $res != '0' && $econnectClass->transactionCode) {
				if (!$econnectClass->INN_access_token) $econnectClass->authorizeINN();
				$response = $econnectClass->cancelPendingPayment($econnectClass->transactionCode, $econnectClass->INN_access_token);
			}
			if ($price > 0 && $res == '5') {
				abr('eMoneyAutoSignin', true);
				abr('insufficient_funds', true);
				$error .= "<li>".$lang['639']."</li>";
			}

			//$_SESSION['access_token'] = 1;
			if ($price > 0 && !$econnectClass->transactionCode && $res != '500') {
				//d($_SESSION['access_token']);
				//d('No transactionCode. Res='.$res,1);
				abr('eConnect_required', true);
				abr('eConnect_autostart', true);
			}
			elseif (!$error) {
				require_once('libraries/cache.class.php');
				// Debugging Purposes store escrow days for transaction
				$escrow_cache = $cache->get('escrow');
				$escrow_cache[$econnectClass->transactionCode] = $escrow_days;
				$cache->set('escrow', $escrow_cache, 7*24*3600);
				// Payment OK, recording to DB
				$query = "INSERT INTO `orders` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($id)."', `IID`='".sql_quote($iid)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `status`='0', `price`='".sql_quote($price)."'";
				$executequery=$conn->execute($query);
				$order_id = $conn->_insertid();
				insert_order_agreement($order_id);

				if($order_id > 0)
				{
					if ($price > 0) {
						$query = "INSERT IGNORE INTO `payments` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `OID`='".sql_quote($order_id)."', `IID`='".sql_quote($iid)."', `time`='".$_SERVER['REQUEST_TIME']."', `price`='".sql_quote($price)."', `t`='1', `transactioncode`='".intval($econnectClass->transactionCode)."', `payment_gateway`='emoney'";
						$executequery=$conn->execute($query);
						
						$query = "UPDATE `posts` SET `rev`=`rev`+$price WHERE `PID`='".sql_quote($id)."' LIMIT 1";
						$executequery=$conn->execute($query);
					}
					include_once('libraries/emailsms.class.php');
					$emailClass = new email();

					if ($required_fields) {
						// Send money receiver note to add/confirm required fields
						$emailClass->send($ownerquery->fields['email'], $lang['640'], sprintf($lang['641'], $required_fields, $config['baseurl'].'/settings'));
					}

					$emailClass->send($ownerquery->fields['email'], sprintf($lang['642'], $p[0]['gtitle']), sprintf($lang['643'], $p[0]['gtitle'], $config['baseurl'].'/track?id='.$order_id));
					$emailClass->send($_SESSION['EMAIL'], sprintf($lang['642'], $p[0]['gtitle']), sprintf($lang['644'], $p[0]['gtitle'], $config['baseurl'].'/track?id='.$order_id));

					// Notify admins on the Purchase
					if ($price>0)
						$emailClass->send('s.megrelishvili@gmail.com', 'GEL'.number_format($price,2).' Shekveta', '"'.$p[0]['gtitle']."\"\nMkidveli: {$buyer}\nShekvetis bmuli:\n".$config['baseurl'].'/track?id='.$order_id);

					if ($config['enable_notify']) {
						$USERSESS = $cache->get('UserSess.'.intval($p[0]['OWNERID']));
						$USERSESS['last_update'] = $_SERVER['REQUEST_TIME'];
						$USERSESS['new_orders'] = ($USERSESS['new_orders'] ? $USERSESS['new_orders'] + 1 : 1);
						$cache->set('UserSess.'.intval($p[0]['OWNERID']), $USERSESS, 86400 * 30);
					}
					header("Location:$config[baseurl]/thank_you?g=".$eid);exit;
				}
			}
		}
		elseif($_POST['mybal_x'] == "1")
		{
			$price = $p[0]['totalprice'];
			if($afunds >= $price)
			{
				$query1 = "UPDATE `members` SET `afunds`=`afunds`-$price , `used`=`used`+$price WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1"; 
				$executequery1=$conn->execute($query1);
				
				if($multi > 1)
				{
					$query = "SELECT `price` FROM `posts` WHERE `PID`='".sql_quote($id)."' LIMIT 1"; 
					$executequery=$conn->execute($query);
					$eachprice = $executequery->fields['price'];
		
					for ($i=1; $i<=$multi; $i++)
					{
						$query = "INSERT INTO `orders` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($id)."', `IID`='".sql_quote($iid)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `status`='0', `price`='".sql_quote($eachprice)."'";
						$executequery=$conn->execute($query);
						$order_id = $conn->_insertid();
						if($order_id > 0)
						{
							$query = "INSERT INTO `payments` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `OID`='".sql_quote($order_id)."', `IID`='".sql_quote($iid)."', `time`='".$_SERVER['REQUEST_TIME']."', `price`='".sql_quote($eachprice)."', `t`='1', `available`='1'";
							$executequery=$conn->execute($query);
							
							$query = "UPDATE `posts` SET `rev`=`rev`+$eachprice WHERE `PID`='".sql_quote($id)."' LIMIT 1";
							$executequery=$conn->execute($query);							
						}
					}
					header("Location:$config[baseurl]/thank_you?g=".$eid);exit;
				}
				else
				{
					$query = "INSERT INTO `orders` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `PID`='".sql_quote($id)."', `IID`='".sql_quote($iid)."', `time_added`='".$_SERVER['REQUEST_TIME']."', `status`='0', `price`='".sql_quote($price)."'";
					$executequery=$conn->execute($query);
					$order_id = $conn->_insertid();
					if($order_id > 0)
					{
						if ($price > 0) {
							$query = "INSERT INTO `payments` SET `USERID`='".sql_quote($_SESSION['USERID'])."', `OID`='".sql_quote($order_id)."', `IID`='".sql_quote($iid)."', `time`='".$_SERVER['REQUEST_TIME']."', `price`='".sql_quote($price)."', `t`='1', `available`='1'";
							$executequery=$conn->execute($query);
							
							$query = "UPDATE `posts` SET `rev`=`rev`+$price WHERE `PID`='".sql_quote($id)."' LIMIT 1";
							$executequery=$conn->execute($query);
						}
					
						header("Location:$config[baseurl]/thank_you?g=".$eid);exit;
					}
				}
			}
		}
	}
	else
	{
		header("Location:$config[baseurl]/");exit;
	}
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
//if ($_SESSION['d'])	abr('d',true);
abr('error',$error);
abr('message',$message);
abr('FB_Fan_hide',1);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END

function insert_order_agreement($order_id) {
	global $conn, $smarty;

	$_GET['order'] = $order_id;
	$_GET['print'] = 1;
	$insert_into_db = 1;
	abr('insert_into_db', 1);
	include('agreement.php');
	$query = "INSERT INTO `order_agreements` SET `OID` = '".intval($order_id)."', `sign_date`='".sql_quote(strftime('%Y-%m-%d %H:%M:%S'))."', `agreement_text` = '".sql_quote(base64_encode(gzcompress($agreement_txt,8)),false)."'";
	$executequery=$conn->execute($query);
	$order_id = $conn->_insertid();
}
?>