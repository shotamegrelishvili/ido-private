<?php
include("include/config.php");
include("include/functions/import.php");
//d($_REQUEST);
//d($_FILES);
$SID = $_SESSION['USERID'];
$EID = intval(cleanit($_REQUEST['id']));
if($EID > 0)
{
	if ($SID != "" && $SID >= 0 && is_numeric($SID))
	{
		$query="SELECT * FROM `posts` WHERE `USERID`='".sql_quote($SID)."' AND `PID`='".sql_quote($EID)."' AND `is_del`='0' LIMIT 1";
		$results=$conn->execute($query);
		$g = $results->getrows();

		if (!isset($g[0])) {
			$_SESSION['temp']['message_title'] = $lang['144'];
			$_SESSION['temp']['message_type'] = 'error';
			header("Location:$config[baseurl]/manage_deals?tab=deals");exit;
		}
		else {
			$queryb = "SELECT COUNT(*) as `total` FROM `orders` WHERE `PID`='".intval($EID)."' AND (`status`='0' OR `status`='1' OR `status`='6')"; 
			$executequeryb=$conn->execute($queryb);
			if ($executequeryb->fields['total']) {
				$_SESSION['temp']['message_title'] = $lang['701'];
				$_SESSION['temp']['message_type'] = 'error';
				header("Location:$config[baseurl]/manage_deals?tab=deals");exit;
			}
			else {
				$queryc = "SELECT COUNT(*) as `total` FROM `orders` WHERE `PID`='".intval($EID)."'";
				$executequeryc=$conn->execute($queryc);
				if ($executequeryc->fields['total'] > 0) {
					$deny_change_fields = array('title'=>'gtitle', 'price'=>'price', 'cat'=>'category');
					abr('deny_change_fields', $deny_change_fields);
				}
				else
					abr('deny_change_fields', array());
			}
		}

		if (insert_get_member_vas_ownprice(0)) {
			if (isset($deny_change_fields['price']))
				$price = intval($g[0]['price']);
			else {
				$price = intval(cleanit($_REQUEST['price']));
				if ($price < 5 && $price != 0) {
					$error .= '<li>'.$lang[841].': '.$lang[838].'</li>';
				}
				elseif ($price > 995) {
					$error .= '<li>'.$lang[841].': '.$lang[839].'</li>';
				}
				elseif ($price != $_REQUEST['price']) {
					$error .= '<li>'.$lang[841].': '.$lang[840].'</li>';
				}
			}
			$config['price_mode'] = 1;
		}
		else {
			// Get a price ID of the deal && Check if the user can change the price
			if ($_REQUEST['price'] && !isset($deny_change_fields['price'])) {
				$query = "SELECT `ID` FROM `packs` WHERE `ID`='".intval(cleanit($_REQUEST['price']))."' LIMIT 1";
			}
			else {
				$query = "SELECT `ID` FROM `packs` WHERE `pprice`='".intval(cleanit($g[0]['price']))."' LIMIT 1";
			}
				$executequeryp=$conn->execute($query);
				$g[0]['PRICEID'] = $executequeryp->fields['ID'];
			// Price ID end
		}
		if ($g[0]['videohost'] && $g[0]['videofile']) {
			if ($g[0]['videohost'] == 'myvideo')
				$g[0]['video'] = 'http://myvideo.ge/?video_id='.trim($g[0]['videofile']);
			elseif ($g[0]['videohost'] == 'youtube')
				$g[0]['video'] = 'https://www.youtube.com/watch?v='.trim($g[0]['videofile']);
		}
		abr('g',$g[0]);
		//d($g[0]);
		if($_POST['subform'] == "1")
		{
			$gtitle = cleanit($_REQUEST['title']);
			//$gprice = cleanit($_REQUEST['price']);
			$gcat = intval(cleanit($_REQUEST['cat']));
			$gdesc = cleanit($_REQUEST['desc']);	
			$ginst = cleanit($_REQUEST['instr']);	
			$gtags = cleanit($_REQUEST['tags']);	
			$gdays = intval(cleanit($_REQUEST['duration']));
			$gdays = intval(cleanit($_REQUEST['duration']));
			$gvideo = trim(cleanit($_REQUEST['video']));
			$gvideohost = cleanit($_REQUEST['videohost']);
			$gvideofile = trim(cleanit($_REQUEST['videofile']));
			$gmoneyback = intval(cleanit($_REQUEST['moneyback']));

			$multipleme = intval(cleanit($_REQUEST['multipleme']));
			
			if($gtitle == "")
			{
				$error = "<li>".$lang['92']."</li>";
			}
			
			if($gcat == "0")
			{
				$error .= "<li>".$lang['93']."</li>";
			}
			if($gdesc == "")
			{
				$error .= "<li>".$lang['94']."</li>";
			}
			if($ginst == "")
			{
				$error .= "<li>".$lang['95']."</li>";
			}
			if($gtags == "")
			{
				$error .= "<li>".$lang['96']."</li>";
			}
			if($gdays < 0 || $gdays > 99)
			{
				$error .= "<li>".$lang['97']."</li>";
			}

			if (sizeof($deny_change_fields)) {
				//d($_REQUEST);
				foreach ($deny_change_fields as $df=>$dfv) {
					if (isset($_REQUEST[$df]) && cleanit($_REQUEST[$df]) && cleanit($_REQUEST[$df]) != $g[0][$dfv]) {
						$error = $lang['737'];
						break;
					}
				}
			}
			$addpricesql = '';

			if($error == '') {
				if ($config['price_mode'] == '1') {
					$comper = intval($config['commission_percent']);
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2);
					$addpricesql = "`price`='".sql_quote($price)."', `ctp`='".sql_quote($ctp)."',";
				}
				elseif ($config['price_mode'] == "3") {
					$query = "SELECT `level` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
					$executequery=$conn->execute($query);
					$mlevel = intval($executequery->fields['level']);

					$query = "SELECT `ID`, `pprice`,`pcom`, `l1`, `l2`, `l3`  FROM `packs` WHERE `ID`='".sql_quote($g[0]['PRICEID'])."' LIMIT 1";
					$executequery=$conn->execute($query);

					$price = intval(cleanit($executequery->fields['pprice']));
					$comper = intval(cleanit($executequery->fields['pcom']));
					if(!$executequery->fields['ID'] || !$executequery->fields['l'.$mlevel])
					{
						$error = "<li>".$lang['435']."</li>";
					}
					$count1 = $comper / 100;
					$count2 = $count1 * $price;
					$ctp = number_format($count2, 2);
					$addpricesql = "`price`='".sql_quote($price)."', `ctp`='".sql_quote($ctp)."',";
				}
			}
/*
			if($gyoutube != "")
			{
				$gyoutube = str_replace("https://", "http://", $gyoutube);
				$pos = strpos($gyoutube, "http://www.youtube.com/watch?v=");
				$posb = strpos($gyoutube, "http://www.youtu.be/");
				$posc = strpos($gyoutube, "http://youtu.be/");
				if ($pos === false)
				{
					if ($posb === false)
					{
						if ($posc === false)
						{
							$error .= "<li>".$lang['133']."</li>";
						}
					}
				}
			}
*/			
			if ($mlevel < 2) $gmoneyback = 1;
			elseif ($gmoneyback) $gmoneyback = '1';
			else $gmoneyback = '0';

			if($error == "")
			{			
				$approve_stories = $config['approve_stories'];
				if($approve_stories == "1")
				{
					$active = "0";
				}
				else
				{
					$active = "1";
				}
				
				if($multipleme == "1")
				{
					$result_add_multiple = ", `add_multiple`='999'";	
				}
				else
					$result_add_multiple = ", `add_multiple`='0'";
				
				$query="UPDATE `posts` SET `gtitle`='".sql_quote($gtitle)."',`gtags`='".sql_quote($gtags)."', `gdesc`='".sql_quote($gdesc)."', `ginst`='".sql_quote($ginst)."', `category`='".sql_quote($gcat)."', {$addpricesql} `days`='".sql_quote($gdays)."', `youtube`='".sql_quote($gyoutube)."', `category`='".sql_quote($gcat)."', `pip`='".$_SERVER['REMOTE_ADDR']."', `moneyback`='{$gmoneyback}', `active`='$active' {$result_add_multiple} WHERE `USERID`='".sql_quote($SID)."' AND `PID`='".sql_quote($EID)."' LIMIT 1";
				$result=$conn->execute($query);
				$pid = $EID;

				$photo_base64 = $mainpic_indb = false;

				if (isset($_FILES['photo-i1']['tmp_name']) && $_FILES['photo-i1']['tmp_name'])
					$gphoto = $_FILES['photo-i1']['tmp_name'];
				elseif (isset($_POST['photobase64-i1']) && $_POST['photobase64-i1'])
				{
					$img = $_POST['photobase64-i1'];
					if (strpos($img, 'data:image/png;base64,')!==FALSE)
						$ext2 = 'png';
					elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
						$ext2 = 'jpg';
					elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
						$ext2 = 'jpeg';
					else $ext2 = '';

					if ($ext2 && substr_count($img, 'data:')==1) {
						$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
						if ($ext2 == 'jpeg') $ext2 = 'jpg';
						$img = str_replace(' ', '+', $img);
						$data = base64_decode($img);
	//					header('Content-type: image/'.$ext2);					echo $data;					exit;
						$_FILES['photo-i1']['name'] = md5('1'.uniqid().'x') . '.' . $ext2;
						$gphoto = $_FILES['photo-i1']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i1']['name'];
						@file_put_contents($gphoto, $data);
						$photo_base64 = true;
					}
				}
				elseif (isset($_POST['photo-indb-i1']) && $_POST['photo-indb-i1'] && file_exists($config['pdir'].'/t/' . $g[0]['p1'])) {
					$mainpic_indb = true;
				}

				if($gphoto != "")
				{
					$ext = substr(strrchr($_FILES['photo-i1']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$error .= "<li>".$lang['100']."</li>";
						}
					}
					else
					{
						$error .= "<li>".$lang['100']."</li>";
					}
				}
				elseif (!$mainpic_indb)
				{
					$error .= "<li>".$lang['101']."</li>";
				}
				if($gphoto != "")
				{
					$thepp = $pid.'-'.substr(md5($pid.$_SERVER['REQUEST_TIME'].'-1'),-4);
					if($theimageinfo[2] == 1)
					{
						$error .= "<li>".$lang['100']."</li>";
//						$thepp .= ".gif";
					}
					elseif($theimageinfo[2] == 2)
					{
						$thepp .= ".jpg";
					}
					elseif($theimageinfo[2] == 3)
					{
						$thepp .= ".png";
					}
					if($error == "")
					{
						if ($g[0]['p1'] && file_exists($config['pdir'].'/'.$g[0]['p1'])) {
							@unlink($config['pdir'].'/'.$g[0]['p1']);
							@unlink($config['pdir'].'/t/'.$g[0]['p1']);
							@unlink($config['pdir'].'/t2/'.$g[0]['p1']);
						}
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto, $myvideoimgnew);
						else
							move_uploaded_file($gphoto, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET p1='$thepp' WHERE PID='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}

				$photo_base64 = false;
				if (isset($_FILES['photo-i2']['tmp_name']) && $_FILES['photo-i2']['tmp_name'] && $ghoto != $_FILES['photo-i2']['tmp_name'])
					$gphoto2 = $_FILES['photo-i2']['tmp_name'];
				elseif (isset($_POST['photobase64-i2']) && $_POST['photobase64-i2'] && $_POST['photobase64-i2'] != $_POST['photobase64-i1'])
				{
					$img = $_POST['photobase64-i2'];
					if (strpos($img, 'data:image/png;base64,')!==FALSE)
						$ext2 = 'png';
					elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
						$ext2 = 'jpg';
					elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
						$ext2 = 'jpeg';
					else $ext2 = '';

					if ($ext2 && substr_count($img, 'data:')==1) {
						$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
						if ($ext2 == 'jpeg') $ext2 = 'jpg';
						$img = str_replace(' ', '+', $img);
						$data = base64_decode($img);
	//					header('Content-type: image/'.$ext2);					echo $data;					exit;
						$_FILES['photo-i2']['name'] = md5('2'.uniqid().'x') . '.' . $ext2;
						$gphoto2 = $_FILES['photo-i2']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i2']['name'];
						@file_put_contents($gphoto2, $data);
						$photo_base64 = true;
					}
				}
				if($gphoto2 != "")
				{
					$ext = substr(strrchr($_FILES['photo-i2']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto2);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid.'-'.substr(md5($pid.$_SERVER['REQUEST_TIME'].'-2'),-4);
						if($theimageinfo[2] == 1)
						{
							$error .= "<li>".$lang['100']."</li>";
//							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
						if ($g[0]['p2'] && file_exists($config['pdir'].'/'.$g[0]['p2'])) {
							@unlink($config['pdir'].'/'.$g[0]['p2']);
							@unlink($config['pdir'].'/t/'.$g[0]['p2']);
							@unlink($config['pdir'].'/t2/'.$g[0]['p2']);
						}
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto2, $myvideoimgnew);
						else
							move_uploaded_file($gphoto2, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET `p2`='$thepp' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}
				elseif ($_POST['photo-remove-i2'] && $g[0]['p2']) {
					$query = "UPDATE posts SET p2='' WHERE PID='".sql_quote($pid)."'";
					$conn->execute($query);
					$extUnlink = substr($g[0]['p2'],-3);
					@unlink($config['pdir'].'/'.$pid.'-2.'.$extUnlink);
					@unlink($config['pdir'].'/t/'.$pid.'-2.'.$extUnlink);
					@unlink($config['pdir'].'/t2/'.$pid.'-2.'.$extUnlink);
				}

				$photo_base64 = false;
				if (isset($_FILES['photo-i3']['tmp_name']) && $_FILES['photo-i3']['tmp_name'] && $gphoto != $_FILES['photo-i3']['tmp_name'] && $gphoto2 != $_FILES['photo-i3']['tmp_name'])
					$gphoto3 = $_FILES['photo-i3']['tmp_name'];
				elseif (isset($_POST['photobase64-i3']) && $_POST['photobase64-i3'] && $_POST['photobase64-i3'] != $_POST['photobase64-i1'] && $_POST['photobase64-i3'] != $_POST['photobase64-i2'])
				{
					$img = $_POST['photobase64-i3'];
					if (strpos($img, 'data:image/png;base64,')!==FALSE)
						$ext2 = 'png';
					elseif (strpos($img, 'data:image/jpg;base64,')!==FALSE)
						$ext2 = 'jpg';
					elseif (strpos($img, 'data:image/jpeg;base64,')!==FALSE)
						$ext2 = 'jpeg';
					else $ext2 = '';

					if ($ext2 && substr_count($img, 'data:')==1) {
						$img = str_replace('data:image/'.$ext2.';base64,', '', $img);
						if ($ext2 == 'jpeg') $ext2 = 'jpg';
						$img = str_replace(' ', '+', $img);
						$data = base64_decode($img);
	//					header('Content-type: image/'.$ext2);					echo $data;					exit;
						$_FILES['photo-i3']['name'] = md5('3'.uniqid().'x') . '.' . $ext2;
						$gphoto3 = $_FILES['photo-i3']['tmp_name'] = $config['pdir'].'/temp/' . $_FILES['photo-i3']['name'];
						@file_put_contents($gphoto3, $data);
						$photo_base64 = true;
					}
				}
				if($gphoto3 != "")
				{
					$ext = substr(strrchr($_FILES['photo-i3']['name'], '.'), 1);
					$ext2 = strtolower($ext);
					if($ext2 == "jpeg" || $ext2 == "jpg" || $ext2 == "png")
					{
						$theimageinfo = getimagesize($gphoto3);
						if($theimageinfo[2] != 1 && $theimageinfo[2] != 2 && $theimageinfo[2] != 3)
						{
							$gstop = "1";
						}
						else
						{
							$gstop = "0";
						}
					}
					if($gstop == "0")
					{
						$thepp = $pid.'-'.substr(md5($pid.$_SERVER['REQUEST_TIME'].'-3'),-4);
						if($theimageinfo[2] == 1)
						{
							$error .= "<li>".$lang['100']."</li>";
//							$thepp .= ".gif";
						}
						elseif($theimageinfo[2] == 2)
						{
							$thepp .= ".jpg";
						}
						elseif($theimageinfo[2] == 3)
						{
							$thepp .= ".png";
						}
	
						$myvideoimgnew=$config['pdir']."/".$thepp;
						if ($g[0]['p3'] && file_exists($config['pdir'].'/'.$g[0]['p3'])) {
							@unlink($config['pdir'].'/'.$g[0]['p3']);
							@unlink($config['pdir'].'/t/'.$g[0]['p3']);
							@unlink($config['pdir'].'/t2/'.$g[0]['p3']);
						}
						if(file_exists($myvideoimgnew))
						{
							unlink($myvideoimgnew);
						}
						if (isset($photo_base64))
							rename($gphoto3, $myvideoimgnew);
						else
							move_uploaded_file($gphoto3, $myvideoimgnew);
						do_resize_image($myvideoimgnew, "540", "280", false, $config['pdir']."/t/".$thepp, true);
						do_resize_image($myvideoimgnew, "255", "144", false, $config['pdir']."/t2/".$thepp, true);
						if(file_exists($config['pdir']."/".$thepp))
						{
							$query = "UPDATE `posts` SET `p3`='$thepp' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
							$conn->execute($query);
						}
					}
				}
				elseif ($_POST['photo-remove-i3'] && $g[0]['p3']) {
					$query = "UPDATE `posts` SET `p3`='' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
					$conn->execute($query);
					$extUnlink = substr($g[0]['p3'],-3);
					@unlink($config['pdir'].'/'.$pid.'-3.'.$extUnlink);
					@unlink($config['pdir'].'/t/'.$pid.'-3.'.$extUnlink);
					@unlink($config['pdir'].'/t2/'.$pid.'-3.'.$extUnlink);
				}
				if ($gvideo && $gvideohost && $gvideofile) {
					if (in_array($gvideohost, array('youtube', 'myvideo')) && strlen($gvideofile) < 12) {
						$query = "UPDATE `posts` SET `videohost`='{$gvideohost}', `videofile`='{$gvideofile}' WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
						$conn->execute($query);
					}
					else {
						$gvideo = $gvideohost = $gvideohost = $_REQUEST['video'] = $_REQUEST['videohost'] = $_REQUEST['videofile'] = '';
					}
				}
				elseif (!$gvideo && $g[0]['video']) {
					$query = "UPDATE `posts` SET `videohost`=NULL, `videofile`=NULL WHERE `PID`='".sql_quote($pid)."' LIMIT 1";
					$conn->execute($query);
				}
				if($approve_stories == "1")
				{
					usleep(100);
					refresh($config[baseurl].'/manage_deals',$lang['145'],'success');
					//$message = $lang['145'];
				}
				else
				{
					usleep(100);
					refresh($config[baseurl].'/manage_deals',$lang['146'],'success');
					//$message = $lang['146'];
				}
			}
		}
		
		$pagetitle = $lang['141'];
		abr('pagetitle',$pagetitle);
	}
	else
	{
		header("Location:$config[baseurl]/");exit;
	}
}
else
{
	$message = $lang['144'];
}

//TEMPLATES BEGIN
abr('sm1',"1");
abr('error',$error);
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display('edit.tpl');
$smarty->display('footer.tpl');
//TEMPLATES END
?>