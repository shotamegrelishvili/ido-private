<?php
include("include/config.php");
include("include/functions/import.php");

$path = $config['basedir'].'/files/';

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


if (!isset($_SESSION['USERID']) || !$_SESSION['USERID'] || !is_numeric($_SESSION['USERID']))
	die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "User not signed in."}, "id" : "id"}');
// Settings
$targetDir = $config['upload_tmp_dir'];
//$targetDir = ini_get("upload_tmp_dir");
//$targetDir = 'uploads';

$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
//usleep(1000);

// Get parameters
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

// Clean the fileName for security reasons
$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

// Make sure the fileName is unique but only if chunking is disabled
if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
	$ext = strrpos($fileName, '.');
	$fileName_a = substr($fileName, 0, $ext);
	$fileName_b = substr($fileName, $ext);

	$count = 1;
	while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
		$count++;

	$fileName = $fileName_a . '_' . $count . $fileName_b;
}

$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

// Create target dir
if (!file_exists($targetDir))
	@mkdir($targetDir, 0700, TRUE);

// Remove old temp files	
if ($cleanupTargetDir) {
	if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
		while (($file = readdir($dir)) !== false) {
			$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

			// Remove temp file if it is older than the max age and is not the current file
			if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < $_SERVER['REQUEST_TIME'] - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
				@unlink($tmpfilePath);
			}
		}
		closedir($dir);
	} else {
		die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
	}
}	

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

if (isset($_SERVER["CONTENT_TYPE"]))
	$contentType = $_SERVER["CONTENT_TYPE"];

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false) {
	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		// Open temp file
		$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
		if ($out) {
			// Read binary input stream and append it to temp file
			$in = @fopen($_FILES['file']['tmp_name'], "rb");

			if ($in) {
				while ($buff = fread($in, 4096))
					fwrite($out, $buff);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			@fclose($in);
			@fclose($out);
			@unlink($_FILES['file']['tmp_name']);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
	} else
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
} else {
	// Open temp file
	$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
	if ($out) {
		// Read binary input stream and append it to temp file
		$in = @fopen("php://input", "rb");

		if ($in) {
			while ($buff = fread($in, 4096))
				fwrite($out, $buff);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

		@fclose($in);
		@fclose($out);
	} else
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}
// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
	// Stript the temp .part suffix off 
	//echo "{$filePath}.part --- ".$filePath.PHP_EOL;
//	rename("{$filePath}.part", $filePath);
	$_FILES['file']['name'] = $fileName;
}
else 
	exit;

if (isset($_FILES['file']['name'])) 
{ 
	//if ($_FILES["fileInput"]["error"] > 0) 
		$clean_file = cleanit($_FILES['file']['name'], 'ISO-8859-1');
		$clean_file = str_replace("'", '', $clean_file);
		$clean_file = str_replace('"', '', $clean_file);

		$ext = substr(strrchr($_FILES['file']['name'], '.'), 1);
		$ext2 = strtolower($ext);

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$f_type=finfo_file($finfo, "{$filePath}.part");

		$allowed = true;
		if (isset($_GET['target']) && isset($config[$_GET['target']]['allowed_types']) && !in_array($f_type, $config[$_GET['target']]['allowed_types'])) {
			$allowed = false;
		}

		if($allowed && !in_array($f_type, $config['not_allowed_mime_types']) && ($ext2 == 'jpeg' || $ext2 == 'jpg' || $ext2 == 'gif' || $ext2 == 'png' || $ext2 == 'tif' || $ext2 == 'bmp' || $ext2 == 'avi' || $ext2 == 'mpeg' || $ext2 == 'mpg' || $ext2 == 'mov' || $ext2 == 'rm' || $ext2 == '3gp' || $ext2 == 'flv' || $ext2 == 'mp4' || $ext2 == 'zip' || $ext2 == 'rar' || $ext2 == 'mp3' || $ext2 == 'wav' || $ext2 == 'wma' || $ext2 == 'ogg' || $ext2 == 'doc' || $ext2 == 'docx' || $ext2 == 'rtf' || $ext2 == 'ppt' || $ext2 == 'xls' || $ext2 == 'xlsx' || $ext2 == 'pdf' || $ext2 == 'txt'))
		{
			$file_hash = md5(file_get_contents("{$filePath}.part"));

			$query='SELECT `FID` FROM `files` WHERE `hash`="'.sql_quote($file_hash).'" LIMIT 1';
			$result=$conn->execute($query);

			if ($result->fields['FID']) {
				if (isset($_GET['target']) && $_GET['target'] == 'portfolio') {
					return_add_portfolio_item( add_portfolio_item($result->fields['FID']) );
				}
				@unlink("{$filePath}.part");
				die(json_encode(array('fid'=>$result->fields['FID'])));
			}

			$s = strftime('%y%m').'-'.substr(md5($_SESSION['USERID'].$config['unique_salt']),0,27);
			$saveme = $path.$s;
			$file_loc = $saveme.'/'.$_FILES['file']['name'];

			if (!file_exists($saveme))
				mkdir($saveme, 0700, TRUE);

			$query="INSERT INTO `files` SET `hash` = '".sql_quote($file_hash)."', `s`='".sql_quote($s)."', `fname`='".sql_quote($clean_file)."', `time`='".$_SERVER['REQUEST_TIME']."', `ip`='".$_SERVER['REMOTE_ADDR']."'";
			$result=$conn->execute($query);
			$fid = $conn->_insertid();

			rename("{$filePath}.part", $file_loc); 

			if (isset($_GET['target']) && $_GET['target'] == 'portfolio') {
				return_add_portfolio_item( add_portfolio_item($fid) );
			}
			die(json_encode(array('fid'=>$fid)));
			//die('{"jsonrpc" : "2.0", "result" : success, "id" : '.$fid.'}');
		}
		else
		{
			@unlink("{$filePath}.part");
			if (isset($_GET['target'])) 
				die('{"jsonrpc" : "2.0", "error" : {"code": 106, "message": "'.$lang[829].'"} }');
			else
				exit("document.getElementById('message_validation_error').innerHTML = '".$lang['241']."'; $('.msg-error').show(); enx.messages.hide_progress();");
		}
}

function return_add_portfolio_item($result) {
	global $lang;
	if ($result === false) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "'.$lang[827].'"}, "id" : "id"}');
	}
	elseif ($result === 0) {
		die('{"jsonrpc" : "2.0", "error" : {"code": 107, "message": "'.$lang[826].'"}, "id" : "id"}');
	}
}
?>