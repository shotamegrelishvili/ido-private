<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
// header('Location:'.$thebaseurl.'/signin'); exit;

if ($_SESSION['USERID'] != '' && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	$_SESSION['is_user'] = true;
/*
	if (isset($_SESSION['temp']['golink']) && $_SESSION['temp']['golink']) {
		header("Location:$config[baseurl]/".$_SESSION['temp']['golink']);
		unset($_SESSION['temp']['golink']);
		exit;
	}
*/
	header("Location:$config[baseurl]/");exit;
}

$r = cleanit(stripslashes($_REQUEST['r']));
abr('r',$r);

if($_REQUEST['jlog'] == "1")
{	
	$user_username = cleanit($_REQUEST['l_username']);
	abr('user_username',$user_username);
	if($user_username == "")
	{
		$error = "<li>".$lang['13']."</li>";	
	}
	
	$user_password = cleanit($_REQUEST['l_password']);
	if($user_password == "")
	{
		$error .= "<li>".$lang['17']."</li>";	
	}
	
	$l_remember_me = cleanit($_REQUEST['l_remember_me']);
	
	if($error == "")
	{
		$encryptedpassword = md5($user_password);
		$query="SELECT `status`,`USERID`,`email`,`username`,`verified`,`tour_seen` FROM `members` WHERE `username`='".sql_quote($user_username)."' AND `password`='".sql_quote($encryptedpassword)."' LIMIT 1";
		$result=$conn->execute($query);
		
		if($result->recordcount()<1)
		{
			$error = "<li>".$lang['42']."</li>";
		}
		elseif($result->fields['status']=="0")
		{
			$error = "<li>".$lang['43']."</li>";
		}

		if($error=="")
		{
			$_SESSION['is_user'] = true;
			$query="UPDATE `members` SET `lastlogin`='".$_SERVER['REQUEST_TIME']."', `lip`='".$_SERVER['REMOTE_ADDR']."' WHERE `username`='".sql_quote($user_username)."' LIMIT 1";
			$conn->execute($query);
	    	$_SESSION['USERID']=$result->fields['USERID'];
			$_SESSION['EMAIL']=$result->fields['email'];
			$_SESSION['USERNAME']=$result->fields['username'];
			$_SESSION['VERIFIED']=$result->fields['verified'];
			$_SESSION['TOUR_SEEN']=$result->fields['tour_seen'];
			if($l_remember_me == "1")
			{
				create_slrememberme();
			}
			$redirect = base64_decode($r);
			if($redirect == "")
			{
				header("Location:$thebaseurl/");exit;
			}
			else
			{
				$rto = $thebaseurl."/".$redirect;
				header("Location:$rto");exit;
			}
		}	
	}
}

$templateselect = "login.tpl";
$pagetitle = $lang['40'];
abr('pagetitle',$pagetitle);

//TEMPLATES BEGIN
abr('error',$error);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>