<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$AID = intval($_REQUEST[AID]);

if($_POST['submitform'] == "1")
{
	$details = cleanit($_POST['details']);
	$code = $_POST['c'];
	$active = intval($_POST['active']);
	
	if($AID > 0)
	{
		if($details == "")
		{
			$error = "Error: Please enter a description.";
		}
		elseif($code == "")
		{
			$error = "Error: Please enter your advertisement code.";
		}
		else
		{
			$sql = "UPDATE advertisements set description='".sql_quote($details)."', code='".sql_quote($code)."', active='".sql_quote($active)."' WHERE AID='".sql_quote($AID)."'";
			$conn->execute($sql);
			$message = "Advertisement Successfully Edited.";
			abr('message',$message);
		}
	}
}

if($AID > 0)
{
	$query = $conn->execute("select * from advertisements where AID='".sql_quote($AID)."' limit 1");
	$ad = $query->getrows();
	abr('ad', $ad[0]);
}

$mainmenu = "11";
$submenu = "1";
abr('error',$error);
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/ads_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>