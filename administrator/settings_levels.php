<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("enable_levels", "level1job", "level2job", "level3job", "level2num", "level2rate", "level3num", "level3rate");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$message = "Job Levels Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "4";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_levels.tpl");
$smarty->display("administrator/global_footer.tpl");
?>