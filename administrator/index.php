<?php
include("../include/config.php");

if (@$_SESSION['ADMINID'] != "" && $_SESSION['ADMINUSERNAME'] != "" && $_SESSION['ADMINPASSWORD'] != "")
{
	if ($_SESSION['MODERATOR_MODE'])
		$redirect = $config['baseurl'];
	else
		$redirect = $config['adminurl']."/home.php";
    header("location: $redirect");
}
else
{

if(@$_POST['login']!="")
{
	$adminusername = $_POST['username'];
	$adminpassword = $_POST['password'];
	if ($adminusername == "")
	{
     	$error = "Error: Username not entered.";
	}
	elseif ($adminpassword == "")
	{
     	$error = "Error: Password not entered.";
	}
	else
	{
		$encodedadminpassword = sha1('((-Mm7,1'.md5(md5($adminpassword)));
        $query="SELECT * FROM `administrators` WHERE `username`='".sql_quote($adminusername)."' AND `password`='".sql_quote($encodedadminpassword)."'";
        $executequery=$conn->execute($query);
		$getid = $executequery->fields['ADMINID'];
        $getusername = $executequery->fields['username'];
		$getpassword = $executequery->fields['password'];
		$moderator_mode = $executequery->fields['moderator_mode'];

		if (is_numeric($getid) && $getusername != "" && $getpassword != "" && $getusername == $adminusername && $getpassword == $encodedadminpassword)
		{
        	$_SESSION['ADMINID'] = $getid;
        	$_SESSION['ADMINUSERNAME'] = $getusername;
			$_SESSION['ADMINPASSWORD'] = $encodedadminpassword;
			$_SESSION['MODERATOR_MODE'] = $moderator_mode;

			if ($moderator_mode)
				$redirect = $config['baseurl'];
			else
				$redirect = $config['adminurl']."/home.php";
        	header("location: $redirect");
		}
		else
		{
			$error = "Invalid username/password entered.";
		}
    }
}

abr('message',@$message);
abr('error',@$error);
$smarty->display('administrator/index.tpl');

}

?>