<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$USERID = intval($_REQUEST['USERID']);

if($_POST['submitform'] == "1")
{
	if($USERID > 0)
	{
		$arr = array("username", "email", "pemail", "fullname", "verified", "status", "funds", "level");
		foreach ($arr as $value)
		{
			$sql = "update members set $value='".sql_quote($_POST[$value])."' where USERID='".sql_quote($USERID)."'";
			$conn->execute($sql);
		}
		
		$sql = "update members set description='".sql_quote($_POST[vdescription])."' where USERID='".sql_quote($USERID)."'";
		$conn->execute($sql);
		
		$newpass2 = $_POST['newpass2'];
		if($newpass2 != "")
		{
			$newpass = md5($newpass2);
			$sql = "update members set password='".sql_quote($newpass)."', pwd='".sql_quote($newpass2)."' where USERID='".sql_quote($USERID)."'";
			$conn->execute($sql);
		}
		
		$message = "Member Successfully Edited.";
		abr('message',$message);
	}
}

if($USERID > 0)
{
	$query = $conn->execute("select * from members where USERID='".sql_quote($USERID)."' limit 1");
	$member = $query->getrows();
	abr('member', $member[0]);
}

$mainmenu = "7";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/members_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>