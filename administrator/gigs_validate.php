<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

// DELETE 
if($_REQUEST['delete']=="1")
{
	$DPID = intval($_REQUEST['PID']);
	if($DPID > 0)
	{
		$sql="UPDATE posts SET active='3' WHERE PID='".sql_quote($DPID)."'";
		$conn->Execute($sql);
		$message = "Gig Successfully Marked As Deleted.";
		abr('message',$message);
	}
}
// DELETE 

//ACTIVE
if($_POST['asub']=="1")
{
	$APID = $_POST['APID'];
	$aval = $_POST['aval'];
	if($aval == "0")
	{
		$aval2 = "1";
		$message = "Gig Successfully Approved.";
	}
	else
	{
		$aval2 = "0";
		$message = "Gig Successfully Deactivated.";
	}
	$sql="UPDATE posts SET active='".intval($aval2)."' WHERE PID='".sql_quote($APID)."'";
	$conn->Execute($sql);
	abr('message',$message);
}
//ACTIVE

if($_REQUEST['sortby']=="gtitle")
{
	$sortby = "gtitle";
	$sort =" order by A.gtitle";
	$add1 = "&sortby=gtitle";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="pip")
{
	$sortby = "pip";
	$sort =" order by A.pip";
	$add1 = "&sortby=pip";
}
elseif($_REQUEST['sortby']=="time_added")
{
	$sortby = "time_added";
	$sort =" order by A.time_added";
	$add1 = "&sortby=time_added";
}
elseif($_REQUEST['sortby']=="active")
{
	$sortby = "active";
	$sort =" order by A.active";
	$add1 = "&sortby=active";
}
else
{
	$sortby = "PID";
	$sort =" order by A.PID";
	$add1 = "&sortby=PID";
}

if($_REQUEST['sorthow']=="desc")
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}
else
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$gtitle = htmlentities(strip_tags($_REQUEST['gtitle']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&gtitle=$gtitle&username=$username&active=$active";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $gtitle!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.PID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.PID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.PID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($gtitle != "")
	{
		$addtosql .= "AND A.gtitle like'%".sql_quote($gtitle)."%'";
		abr('gtitle',$gtitle);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.PID,B.username from posts A, members B WHERE A.PID>0 AND A.USERID=B.USERID AND A.active='0' $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*,B.username from posts A, members B WHERE A.PID>0 AND A.USERID=B.USERID AND A.active='0' $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalposts = $executequeryselected->rowcount();	
if ($totalposts > 0)
{
	if($totalposts<=$config[maximum_results])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/gigs_validate.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "There are no gigs awaiting validation.";
}

$mainmenu = "4";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/gigs_validate.tpl");
$smarty->display("administrator/global_footer.tpl");
?>