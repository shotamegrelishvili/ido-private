<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$CATID = intval($_REQUEST[CATID]);
if($CATID > 0)
{
	if($_POST['submitform'] == "1")
	{
		$name = htmlentities(strip_tags($_REQUEST['name']), ENT_COMPAT, "UTF-8");
		$seo = htmlentities(strip_tags($_REQUEST['seo']), ENT_COMPAT, "UTF-8");
		$seo = str_replace("\/", "-", $seo);
		$seo = str_replace("/", "-", $seo);
		$seo = str_replace("&amp;", "-", $seo);
		$seo = str_replace("&", "-", $seo);
		$seo = str_replace(" ", "-", $seo);
		$details = cleanit($_POST['details']);
		$mtitle = cleanit($_POST['mtitle']);
		$mdesc = cleanit($_POST['mdesc']);
		$mtags = cleanit($_POST['mtags']);
		$sql = "update categories set name='".sql_quote($name)."', seo='".sql_quote($seo)."', details='".sql_quote($details)."', mtitle='".sql_quote($mtitle)."', mdesc='".sql_quote($mdesc)."', mtags='".sql_quote($mtags)."' where CATID='".sql_quote($CATID)."'";
		$conn->execute($sql);
		$message = "Category Successfully Edited.";
		abr('message',$message);
	}

	$query = $conn->execute("select * from categories where CATID='".sql_quote($CATID)."' limit 1");
	$category = $query->getrows();
	abr('category', $category[0]);
}

$mainmenu = "3";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/cat_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>