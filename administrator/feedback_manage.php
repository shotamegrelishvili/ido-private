<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

// DELETE BEGIN
if($_REQUEST[delete]=="1")
{
	$DRID = intval($_REQUEST['RID']);
	if($DRID > 0)
	{
		$sql="DELETE FROM ratings WHERE RID='".sql_quote($DRID)."'";
		$conn->Execute($sql);
		$message = "Feedback Successfully Deleted.";
		abr('message',$message);
	}
}
// DELETE END

if($_REQUEST['sortby']=="details")
{
	$sortby = "details";
	$sort =" order by A.comment";
	$add1 = "&sortby=details";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="gtitle")
{
	$sortby = "gtitle";
	$sort =" order by C.gtitle";
	$add1 = "&sortby=gtitle";
}
else
{
	$sortby = "RID";
	$sort =" order by A.RID";
	$add1 = "&sortby=RID";
}

if($_REQUEST['sorthow']=="desc")
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}
else
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$gtitle = htmlentities(strip_tags($_REQUEST['gtitle']), ENT_COMPAT, "UTF-8");
$details = htmlentities(strip_tags($_REQUEST['details']), ENT_COMPAT, "UTF-8");
$active = htmlentities(strip_tags($_REQUEST['active']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&details=$details&active=$active";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $details!="" || $gtitle!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.RID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.RID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.RID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($details != "")
	{
		$addtosql .= "AND A.comment like'%".sql_quote($details)."%'";
		abr('details',$details);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	if($gtitle != "")
	{
		$addtosql .= "AND C.gtitle like'%".sql_quote($gtitle)."%'";
		abr('gtitle',$gtitle);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.RID, B.username, C.gtitle  from ratings A, members B, posts C WHERE A.RATER=B.USERID AND A.PID=C.PID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*, B.username, C.gtitle from ratings A, members B, posts C WHERE A.RATER=B.USERID AND A.PID=C.PID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();	
if ($totalvideos > 0)
{
	if($totalvideos<=$config[maximum_results])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/feedback_manage.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "Sorry, there is no feedback yet.";
}

$mainmenu = "15";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/feedback_manage.tpl");
$smarty->display("administrator/global_footer.tpl");
?>