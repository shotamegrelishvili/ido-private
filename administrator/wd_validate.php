<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

//ACTIVE
if($_POST['asub']=="1")
{
	$AWID = intval($_POST['AWID']);
	$auid = intval($_POST['auid']);
	$apay = cleanit($_POST['apay']);
	if($AWID > 0 && $auid > 0 && $apay > 0)
	{
		$sql="UPDATE members SET afunds=afunds-$apay WHERE USERID='".sql_quote($auid)."'";
		$conn->Execute($sql);
		$sql="UPDATE members SET withdrawn=withdrawn+$apay WHERE USERID='".sql_quote($auid)."'";
		$conn->Execute($sql);
		$sql="DELETE FROM withdraw_requests WHERE WID='".sql_quote($AWID)."'";
		$conn->Execute($sql);
		$message = "Withdrawal successfully processed.";
		abr('message',$message);
	}
}
//ACTIVE

if($_REQUEST['sortby']=="pemail")
{
	$sortby = "pemail";
	$sort =" order by B.pemail";
	$add1 = "&sortby=pemail";
}
elseif($_REQUEST['sortby']=="aemail")
{
	$sortby = "aemail";
	$sort =" order by B.aemail";
	$add1 = "&sortby=aemail";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
else
{
	$sortby = "WID";
	$sort =" order by A.WID";
	$add1 = "&sortby=WID";
}

if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$pemail = htmlentities(strip_tags($_REQUEST['pemail']), ENT_COMPAT, "UTF-8");
$aemail = htmlentities(strip_tags($_REQUEST['aemail']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&pemail=$pemail&aemail=$aemail&username=$username";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $pemail!="" || $aemail!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.WID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.WID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.WID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($pemail != "")
	{
		$addtosql .= "AND B.pemail like'%".sql_quote($pemail)."%'";
		abr('pemail',$pemail);
	}
	if($aemail != "")
	{
		$addtosql .= "AND B.aemail like'%".sql_quote($aemail)."%'";
		abr('aemail',$aemail);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.WID,B.username from withdraw_requests A, members B WHERE A.USERID=B.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*,B.username, B.afunds, B.pemail, B.aemail from withdraw_requests A, members B WHERE A.USERID=B.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();	
if ($totalvideos > 0)
{
	if($totalvideos<=$config[maximum_results])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/wd_validate.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "There are no withdrawals to validate.";
}

$mainmenu = "8";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/wd_validate.tpl");
$smarty->display("administrator/global_footer.tpl");
?>