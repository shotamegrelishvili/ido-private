<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

//DELETE REPORT
if($_REQUEST['delete']=="1")
{
	$DRID = intval($_REQUEST['DRID']);	
	if($DRID > 0)
	{
		$sql="DELETE FROM inbox_reports WHERE RID='".sql_quote($DRID)."'";
		$conn->Execute($sql);
	}
	$message = "Report Successfully Deleted.";
	abr('message',$message);
}
//DELETE REPORT END

//DELETE MESSAGE
if($_REQUEST['delete']=="2")
{
	$DMID = intval($_REQUEST['DMID']);	
	if($DMID > 0)
	{
		$sql="DELETE FROM inbox WHERE MID='".sql_quote($DMID)."'";
		$conn->Execute($sql);
	}
	$DRID = intval($_REQUEST['DRID']);	
	if($DRID > 0)
	{
		$sql="DELETE FROM inbox_reports WHERE RID='".sql_quote($DRID)."'";
		$conn->Execute($sql);
	}
	$message = "Message Successfully Deleted.";
	abr('message',$message);
}
//DELETE MESSAGE END

if($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by C.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="msg")
{
	$sortby = "msg";
	$sort =" order by B.message";
	$add1 = "&sortby=msg";
}
else
{
	$sortby = "RID";
	$sort =" order by A.RID";
	$add1 = "&sortby=RID";
}

if($_REQUEST['sorthow']=="desc")
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}
else
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$msg = htmlentities(strip_tags($_REQUEST['msg']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&username=$username&msg=$msg";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $username!="" || $msg!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.MID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.MID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.MID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($username != "")
	{
		$addtosql .= "AND C.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	if($msg != "")
	{
		$addtosql .= "AND B.message like'%".sql_quote($msg)."%'";
		abr('msg',$msg);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.RID, B.message, C.username from inbox_reports A, inbox B, members C WHERE A.MID=B.MID AND B.MSGFROM=C.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.RID, A.MID, A.time, B.message, C.username from inbox_reports A, inbox B, members C WHERE A.MID=B.MID AND B.MSGFROM=C.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();	
if ($totalvideos > 0)
{
	if($totalvideos<=$config[maximum_results])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/reports_messages.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "There are no reported messages yet.";
}

$mainmenu = "10";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/reports_messages.tpl");
$smarty->display("administrator/global_footer.tpl");
?>