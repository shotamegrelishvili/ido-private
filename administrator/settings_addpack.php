<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

if($_POST['submitform'] == "1")
{
	$pprice = cleanit($_POST['pprice']);
	$pcom = cleanit($_POST['pcom']);
	$l1 = intval(cleanit($_POST['l1']));
	$l2 = intval(cleanit($_POST['l2']));
	$l3 = intval(cleanit($_POST['l3']));
	
	if($pprice == "")
	{
		$error = "Error: Please enter a price.";
	}
	elseif($pcom == "")
	{
		$error = "Error: Please enter a percentage commission.";
	}
	else
	{

		$sql = "insert into packs set pprice='".sql_quote($pprice)."', pcom='".sql_quote($pcom)."', l1='".sql_quote($l1)."', l2='".sql_quote($l2)."', l3='".sql_quote($l3)."'";
		$conn->execute($sql);
		$message = "Price Pack Successfully Added.";
		abr('message',$message);

	}
}

$mainmenu = "2";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/settings_addpack.tpl");
$smarty->display("administrator/global_footer.tpl");
?>