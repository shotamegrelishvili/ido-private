<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

// DELETE BEGIN
if($_REQUEST[delete]=="1")
{
	$DFID = intval($_REQUEST['FID']);
	if($DFID > 0)
	{
		$query = "select fname,s from files where FID='".sql_quote($DFID)."' limit 1"; 
		$executequery=$conn->execute($query);
		$s = $executequery->fields['s'];
		$fname = $executequery->fields['fname'];
		$path = $config['basedir'].'/files/';
		$cf = md5($DFID).$s;
		$saveme = $path.$cf;
		$file_loc = $saveme."/".$fname; 
		if (file_exists($file_loc))
		{
			@unlink($file_loc);
		}
		$sql="DELETE FROM files WHERE FID='".sql_quote($DFID)."'";
		$conn->Execute($sql);
		$sql="UPDATE inbox2 SET FID='0' WHERE FID='".sql_quote($DFID)."'";
		$conn->Execute($sql);
		$message = "File Successfully Deleted.";
		abr('message',$message);
	}
}
// DELETE END

if($_REQUEST['sortby']=="fname")
{
	$sortby = "fname";
	$sort =" order by B.fname";
	$add1 = "&sortby=fname";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "active";
	$sort =" order by C.username";
	$add1 = "&sortby=username";
}
else
{
	$sortby = "FID";
	$sort =" order by B.FID";
	$add1 = "&sortby=FID";
}

if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$fname = htmlentities(strip_tags($_REQUEST['fname']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&fname=$fname&username=$username";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $fname!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND B.FID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND B.FID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND B.FID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($fname != "")
	{
		$addtosql .= "AND B.fname like'%".sql_quote($fname)."%'";
		abr('fname',$fname);
	}
	if($username != "")
	{
		$addtosql .= "AND C.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.MSGFROM, B.*, C.username from inbox2 A, files B, members C WHERE A.FID>'0' AND A.FID=B.FID AND A.MSGFROM=C.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.MSGFROM, B.*, C.username from inbox2 A, files B, members C WHERE A.FID>'0' AND A.FID=B.FID AND A.MSGFROM=C.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();	
if ($totalvideos > 0)
{
	if($totalvideos<=$config[maximum_results])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/files_orders.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/files_orders.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/files_orders.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/files_orders.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/files_orders.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/files_orders.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "There are no files uploaded in orders yet.";
}

$mainmenu = "16";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/files_orders.tpl");
$smarty->display("administrator/global_footer.tpl");
?>