<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

function insert_get_trans_id($a)
{
    global $conn;
	$query = "select txn_id, memo from paypal_table where id='".sql_quote($a[id])."'"; 
	$executequery=$conn->execute($query);
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

if($_REQUEST['sortby']=="OID")
{
	$sortby = "OID";
	$sort =" order by A.OID";
	$add1 = "&sortby=OID";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="time")
{
	$sortby = "time";
	$sort =" order by A.time";
	$add1 = "&sortby=time";
}
elseif($_REQUEST['sortby']=="PAYPAL")
{
	$sortby = "PAYPAL";
	$sort =" order by A.PAYPAL";
	$add1 = "&sortby=PAYPAL";
}
else
{
	$sortby = "ID";
	$sort =" order by A.ID";
	$add1 = "&sortby=ID";
}

if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$OID = htmlentities(strip_tags($_REQUEST['OID']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&OID=$OID&username=$username";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $OID!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.ID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.ID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.ID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($OID != "")
	{
		$addtosql .= "AND A.OID='".sql_quote($OID)."'";
		abr('OID',$OID);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.ID,B.username from payments A, members B WHERE A.ID>0 AND A.USERID=B.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*,B.username from payments A, members B WHERE A.ID>0 AND A.USERID=B.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalposts = $executequeryselected->rowcount();	
if ($totalposts > 0)
{
	if($totalposts<=$config[maximum_results])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/payments_manage.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "Sorry, no payments were found.";
}

$mainmenu = "6";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/payments_manage.tpl");
$smarty->display("administrator/global_footer.tpl");
?>