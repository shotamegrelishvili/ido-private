<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

function insert_get_all_gigs()
{
    global $config,$conn;
	$query = "select PID,gtitle from posts order by PID desc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_get_all_mems()
{
    global $config,$conn;
	$query = "select USERID,username from members WHERE username!='' order by username asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

if($_POST['submitform'] == "1")
{
	$PID = intval($_POST['PID']);
	$RATER = intval($_POST['RATER']);
	$rating = intval($_POST['rating']);
	$comment = cleanit($_POST['comment']);
	
	if($PID == "0")
	{
		$error = "Error: Please choose a gig.";
	}
	elseif($RATER == "0")
	{
		$error = "Error: Please choose a user.";
	}
	else
	{
		$query = "select USERID from posts where PID='".sql_quote($PID)."'"; 
		$executequery=$conn->execute($query);
		$USERID = intval($executequery->fields['USERID']);
		if($USERID == "0")
		{
			$error = "Error: Could not retrieve the owner of the gig.";
		}
		elseif($USERID == $RATER)
		{
			$error = "Error: The user adding the feedback cannot be the seller.";
		}
		else
		{
			if($rating == "1")
			{
				$addme = ", good='1', bad='0'"	;
			}
			else
			{
				$addme = ", good='0', bad='1'";	
			}
			$sql = "insert ratings set USERID='".sql_quote($USERID)."', PID='".sql_quote($PID)."', RATER='".sql_quote($RATER)."', comment='".sql_quote($comment)."', time_added='".time()."' $addme";
			$conn->execute($sql);
			$message = "Feedback Successfully Added.";
			abr('message',$message);
		}
	}
}

$mainmenu = "15";
$submenu = "1";
abr('error',$error);
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/feedback_add.tpl");
$smarty->display("administrator/global_footer.tpl");
?>