<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

// DELETE BEGIN
if($_REQUEST[delete]=="1")
{
	$DRID = intval($_REQUEST['RID']);
	if($DRID > 0)
	{
		$sql="DELETE FROM referrals WHERE RID='".sql_quote($DRID)."'";
		$conn->Execute($sql);
		$message = "Referral Successfully Deleted.";
		abr('message',$message);
	}
}
// DELETE END

if($_REQUEST['sortby']=="refd")
{
	$sortby = "refd";
	$sort =" order by C.username";
	$add1 = "&sortby=refd";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="money")
{
	$sortby = "money";
	$sort =" order by A.money";
	$add1 = "&sortby=money";
}
elseif($_REQUEST['sortby']=="time_added")
{
	$sortby = "time_added";
	$sort =" order by A.time_added";
	$add1 = "&sortby=time_added";
}
elseif($_REQUEST['sortby']=="ip")
{
	$sortby = "ip";
	$sort =" order by A.ip";
	$add1 = "&sortby=ip";
}
elseif($_REQUEST['sortby']=="status")
{
	$sortby = "status";
	$sort =" order by A.status";
	$add1 = "&sortby=status";
}
else
{
	$sortby = "RID";
	$sort =" order by A.RID";
	$add1 = "&sortby=RID";
}

if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$refd = htmlentities(strip_tags($_REQUEST['refd']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$ip = htmlentities(strip_tags($_REQUEST['ip']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&refd=$refd&username=$username";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $refd!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.RID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.RID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.RID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($refd != "")
	{
		$addtosql .= "AND C.username like'%".sql_quote($refd)."%'";
		abr('refd',$refd);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	if($ip != "")
	{
		$addtosql .= "AND A.ip like'%".sql_quote($ip)."%'";
		abr('ip',$ip);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.RID, B.username, C.username as refd from referrals A, members B, members C WHERE A.USERID=B.USERID AND A.REFERRED=C.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.*, B.username, C.username as refd from referrals A, members B, members C WHERE A.USERID=B.USERID AND A.REFERRED=C.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();	
if ($totalvideos > 0)
{
	if($totalvideos<=$config['maximum_results'])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config['maximum_results'];
	}
	$toppage = ceil($total/$config['items_per_page']);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/referrals_manage.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "There are no referrals yet.";
}

$mainmenu = "17";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/referrals_manage.tpl");
$smarty->display("administrator/global_footer.tpl");
?>