<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();
$adminurl = $config['adminurl'];

//DELETE MESSAGE
if($_REQUEST['delete']=="1")
{
	$DMID = intval($_REQUEST['DMID']);
	if($DMID > 0)
	{
		$sql="DELETE FROM inbox WHERE MID='".sql_quote($DMID)."'";
		$conn->Execute($sql);
	}
	$message = "Message Successfully Deleted.";
	abr('message',$message);
}
//DELETE MESSAGE END

if($_REQUEST['sortby']=="msgfrom")
{
	$sortby = "msgfrom";
	$sort =" order by B.username";
	$add1 = "&sortby=msgfrom";
}
elseif($_REQUEST['sortby']=="msgto")
{
	$sortby = "msgto";
	$sort =" order by C.username";
	$add1 = "&sortby=msgto";
}
elseif($_REQUEST['sortby']=="msg")
{
	$sortby = "msg";
	$sort =" order by A.message";
	$add1 = "&sortby=msg";
}
else
{
	$sortby = "MID";
	$sort =" order by A.MID";
	$add1 = "&sortby=MID";
}
if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}
//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$msgfrom = htmlentities(strip_tags($_REQUEST['msgfrom']), ENT_COMPAT, "UTF-8");
$msgto = htmlentities(strip_tags($_REQUEST['msgto']), ENT_COMPAT, "UTF-8");
$msg = htmlentities(strip_tags($_REQUEST['msg']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&msgfrom=$msgfrom&msgto=$msgto&msg=$msg";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $msgfrom!="" || $msgto!="" || $msg!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.MID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.MID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.MID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($msgfrom != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($msgfrom)."%'";
		abr('msgfrom',$msgfrom);
	}
	if($msgto != "")
	{
		$addtosql .= "AND C.username like'%".sql_quote($msgto)."%'";
		abr('msgfrom',$msgfrom);
	}
	if($msg != "")
	{
		$addtosql .= "AND A.message like'%".sql_quote($msg)."%'";
		abr('msg',$msg);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select A.MID,A.message,B.username as msgfrom,C.username as msgto from inbox A,members B,members C where A.msgfrom=B.USERID and A.msgto=C.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select A.MID,A.message,B.username as msgfrom,C.username as msgto from inbox A,members B,members C where A.msgfrom=B.USERID and A.msgto=C.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalvideos = $executequeryselected->rowcount();
if ($totalvideos > 0)
{
	if($totalvideos<=$config[maximum_results])
	{
		$total = $totalvideos;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{
		if($currentpage > 1)
		{
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		}
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage)
		{
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/conversations_manage.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		}
	}
}
else
{
	$error = "There are no conversations yet.";
}

$mainmenu = "14";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
$smarty->display("administrator/conversations_manage.tpl");
$smarty->display("administrator/global_footer.tpl");
?>