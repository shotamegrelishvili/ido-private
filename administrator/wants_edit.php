<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

function insert_get_all_cats()
{
    global $config,$conn;
	$query = "select CATID,name from categories order by name asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_get_all_mems()
{
    global $config,$conn;
	$query = "select USERID,username from members order by username asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

$WID = intval($_REQUEST[WID]);
if($WID > 0)
{
	if($_POST['submitform'] == "1")
	{
		$want = htmlentities(strip_tags($_REQUEST['want']), ENT_COMPAT, "UTF-8");
		$category = intval($_REQUEST['category']);
		$USERID = intval($_REQUEST['USERID']);
		$sql = "update wants set USERID='".sql_quote($USERID)."', want='".sql_quote($want)."', category='".sql_quote($category)."' where WID='".sql_quote($WID)."'";
		$conn->execute($sql);
		$message = "Want Successfully Edited.";
		abr('message',$message);
	}

	$query = $conn->execute("select * from wants where WID='".sql_quote($WID)."' limit 1");
	$want = $query->getrows();
	abr('want', $want[0]);
}

$mainmenu = "9";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/wants_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>