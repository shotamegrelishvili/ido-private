<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$myip = $_SERVER['REMOTE_ADDR'];
	if($myip == $_POST['add'])
	{
		$error = "Error: You cannot ban yourself.";
		abr('error',$error);
	}
	else
	{
		$sql = "insert bans_ips set ip='".sql_quote($_POST['add'])."'";
		$conn->execute($sql);
		$message = "IP Successfully Banned.";
		abr('message',$message);
	}
}

$mainmenu = "13";
$submenu = "2";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/bans_ip_add.tpl");
$smarty->display("administrator/global_footer.tpl");
?>