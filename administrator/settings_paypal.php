<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("enable_paypal", "paypal_email", "notify_email", "currency", "paypal_confirm");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$message = "PayPal Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "6";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_paypal.tpl");
$smarty->display("administrator/global_footer.tpl");
?>