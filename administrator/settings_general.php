<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("site_name", "site_slogan", "site_email", "items_per_page", "approve_stories", "max_suggest", "approve_suggests", "view_rel_max", "view_more_max", "enable_fc", "FACEBOOK_APP_ID", "FACEBOOK_SECRET", "twitter", "short_urls", "vonly", "toprated_rating", "toprated_count", "verify_pm", "def_country", "proxy_block");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$message = "General Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_general.tpl");
$smarty->display("administrator/global_footer.tpl");
?>