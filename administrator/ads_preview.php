<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$AID = intval($_REQUEST[AID]);
if($AID > 0)
{
	$query = $conn->execute("select code from advertisements where AID='".sql_quote($AID)."' limit 1");
	$ad = $query->getrows();
	abr('ad', $ad[0]);
}

$mainmenu = "11";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/ads_preview.tpl");
$smarty->display("administrator/global_footer.tpl");
?>