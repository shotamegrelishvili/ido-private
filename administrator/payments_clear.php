<?php
include("../include/config.php");
include_once("../include/functions/import.php");
require_once($config['basedir'].'/libraries/cache.class.php');
verify_login_admin();
$adminurl = $config['adminurl'];

// Notify if Scheduled sending of mails was close
if ($_POST['asub']!="1") {
	$last_wd_sent = $cache->get('WD_EMAILS_countdown');
	if ($_SERVER['REQUEST_TIME'] + 600 > ($last_wd_sent + $config['delayed_emails']['wd_mail_period'])) {
		$error = 'Mail sending was scheduled to run within 10 mins!';
	}
}

// Reset the countdown to send WD mails
$cache->set('WD_EMAILS_countdown', $_SERVER['REQUEST_TIME'], 172800);

// Cancel Transaction
if ($_POST['csub']==1) {
	$AID = intval($_POST['AID']);
	$transactioncode = intval($_POST['transactioncode']);
	if ($AID && $transactioncode) {
		require_once($config['basedir'].'/libraries/eConnect.class.php');
		$econnectClass = new econnect();
		if (!$econnectClass->INN_access_token) $econnectClass->authorizeINN();
		$response = $econnectClass->cancelPendingPayment($transactioncode, $econnectClass->INN_access_token);
		if ($econnectClass->INN_access_token) {
			$sql="UPDATE `payments` SET `cancel`='1' WHERE `ID`='".sql_quote($AID)."' AND `transactioncode`='".sql_quote($transactioncode)."' LIMIT 1";
			$conn->Execute($sql);
			$message = "Payment cancelled.";
			abr('message',$message);
		}
		else {
			$error = 'INN Authorization Error';
		}
	}
}

//CLEAR
if($_POST['asub']==1)
{
	$AID = intval($_POST['AID']);
	$transactioncode = intval($_POST['transactioncode']);
	$aseller = intval($_POST['aseller']);
	$apay = number_format($_POST['apay'], 2, '.', '');

	/**
	 Debugging
	**/
	 //$_SESSION['d'] = 'LANA12062010';
	 /*$AID = 6;
	 $transactioncode = '773332173';
	 include_once($config['basedir'].'/libraries/eConnect.class.php');
	 $econnectClass = new econnect();
	 $commitResult = $econnectClass->commitPayment($transactioncode);
	 d('Commit Result:');
	 d($commitResult,1);
	 */
	 

	if ($AID && $transactioncode) {
		$query = 'SELECT * FROM `payments` WHERE `ID`="'.$AID.'" AND `transactioncode`="'.$transactioncode.'" AND `wd`="0" AND `transfer_complete`="0" AND `cancel`="0" AND `delayed_cancel`="0" LIMIT 1';
		$paymentres = $conn->Execute($query);
	}
	if ($paymentres->fields['ID']) {
		if ($paymentres->fields['payment_gateway'] == 'emoney') {
			// Commit Payment with eMoney
			include_once($config['basedir'].'/libraries/eConnect.class.php');
			$econnectClass = new econnect();
			$commitResult = $econnectClass->commitPayment($transactioncode);
		}
		else {
			// Commit Payment with internal balance
			$sql="UPDATE `members` SET `afunds`=`afunds`+{$apay} WHERE `USERID`='".sql_quote($aseller)."' LIMIT 1";
			$conn->Execute($sql);
			if ($conn->_affectedrows()) {
				$commitResult = true;
			}
		}

		if ($commitResult) {
			// Update payments table
			$sql="UPDATE `payments` SET `wd`='1', `transfer_complete`='1' WHERE `ID`='".sql_quote($AID)."' LIMIT 1";
			$conn->Execute($sql);

			// Insert delayed emails as notification on Payment Transfer
			if ($conn->_affectedrows()) {
				$wd_emails = $cache->get('WD_EMAILS');
				if ($wd_emails[$aseller]) {
					$wd_emails[$aseller] += $apay;
				}
				else {
					$wd_emails[$aseller] = $apay;
				}
				$wd_emails = $cache->set('WD_EMAILS',$wd_emails,172800);
				$message = "Payment successfully cleared.";
				abr('message',$message);
			}
		}
		else {
			$error = "Error! Payment wasn't sent :( Contact Admin and let him know the Transaction Code: ".$transactioncode;
		}
	}
	else {
		$error = "Payment Data not found!";
	}
}
//CLEAR

function insert_get_trans_id($a)
{
    global $conn;
	$query = "select txn_id, memo from paypal_table where id='".sql_quote($a[id])."'"; 
	$executequery=$conn->execute($query);
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

if($_REQUEST['sortby']=="OID")
{
	$sortby = "OID";
	$sort =" order by A.OID";
	$add1 = "&sortby=OID";
}
elseif($_REQUEST['sortby']=="username")
{
	$sortby = "username";
	$sort =" order by B.username";
	$add1 = "&sortby=username";
}
elseif($_REQUEST['sortby']=="time")
{
	$sortby = "time";
	$sort =" order by A.time";
	$add1 = "&sortby=time";
}
elseif($_REQUEST['sortby']=="PAYPAL")
{
	$sortby = "PAYPAL";
	$sort =" order by A.PAYPAL";
	$add1 = "&sortby=PAYPAL";
}
else
{
	$sortby = "ID";
	$sort =" order by A.ID";
	$add1 = "&sortby=ID";
}

if($_REQUEST['sorthow']=="asc")
{
	$sorthow ="asc";
	$add1 .= "&sorthow=asc";
}
else
{
	$sorthow ="desc";
	$add1 .= "&sorthow=desc";
}

//Search
$fromid = intval($_REQUEST['fromid']);
$toid = intval($_REQUEST['toid']);
$OID = htmlentities(strip_tags($_REQUEST['OID']), ENT_COMPAT, "UTF-8");
$username = htmlentities(strip_tags($_REQUEST['username']), ENT_COMPAT, "UTF-8");
$add1 .= "&fromid=$fromid&toid=$toid&OID=$OID&username=$username";
if($_POST['submitform'] == "1" || ($_REQUEST['fromid']!="" || $toid>0 || $OID!="" || $username!=""))
{
	if($fromid > 0)
	{
		$addtosql = "AND A.ID>='".sql_quote($fromid)."'";
		abr('fromid',$fromid);
	}
	else
	{
		$addtosql = "AND A.ID>'".sql_quote($fromid)."'";
	}
	if($toid > 0)
	{
		$addtosql .= "AND A.ID<='".sql_quote($toid)."'";
		abr('toid',$toid);
	}
	if($OID != "")
	{
		$addtosql .= "AND A.OID='".sql_quote($OID)."'";
		abr('OID',$OID);
	}
	if($username != "")
	{
		$addtosql .= "AND B.username like'%".sql_quote($username)."%'";
		abr('username',$username);
	}
	abr('search',"1");
}
//Search End

$page = intval($_REQUEST['page']);
if($page=="")
{
	$page = "1";
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

$queryselected = "select D.ctp, A.ID,B.username, C.cltime, D.USERID as seller from payments A, members B, orders C, posts D WHERE D.PID=C.PID AND A.OID=C.OID AND C.status='5' AND A.cancel='0' AND A.`delayed_cancel`='0' AND A.wd='0' AND A.ID>0 AND D.USERID=B.USERID $addtosql $sort $sorthow limit $config[maximum_results]";
$query2 = "select D.ctp, A.*,B.username, C.cltime, C.IID, D.USERID as seller from payments A, members B, orders C, posts D WHERE D.PID=C.PID AND A.OID=C.OID AND C.status='5' AND A.cancel='0' AND A.`delayed_cancel`='0' AND A.wd='0' AND A.ID>0 AND D.USERID=B.USERID $addtosql $sort $sorthow limit $pagingstart, $config[items_per_page]";
$executequeryselected = $conn->Execute($queryselected);
$totalposts = $executequeryselected->rowcount();	
if ($totalposts > 0)
{
	if($totalposts<=$config[maximum_results])
	{
		$total = $totalposts;
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);	
	$results = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if ($currentpage > 0)
	{	
		if($currentpage > 1) 
		{
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=1$add1' title='first page'>First</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=$theprevpage$add1'>Previous</a>&nbsp;";
		};
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=$lowercount$add1'>$lowercount</a>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.=$currentpage."&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=$uppercounter$add1'>$uppercounter</a>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=$thenextpage$add1'>Next</a>&nbsp;";
			$pagelinks.="<a href='$adminurl/payments_clear.php?page=$toppage$add1' title='last page'>Last</a>&nbsp;";
		};
	}
}
else
{
	$error = "No payments are ready to be cleared yet";
}

$mainmenu = "6";
$submenu = "2";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
abr('sorthow',$sorthow);
abr('sortby',$sortby);
abr('currentpage',$currentpage);
$smarty->display("administrator/global_header.tpl");
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('total',$total+0);
abr('results',$results);
abr('error',$error);
abr('currencySymbol',$config['currencySymbol']);
$smarty->display("administrator/payments_clear.tpl");
$smarty->display("administrator/global_footer.tpl");
?>