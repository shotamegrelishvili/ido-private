<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("enable_ref", "ref_price");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$message = "Referral Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_referrals.tpl");
$smarty->display("administrator/global_footer.tpl");
?>