<?php


include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

function insert_get_all_cats()
{
    global $config,$conn;
	$query = "select CATID,name from categories order by name asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_get_all_mems()
{
    global $config,$conn;
	$query = "select USERID,username from members order by username asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

$PID = intval($_REQUEST['PID']);

if($_POST['submitform'] == "1")
{
	if($PID > 0)
	{
		$arr = array("USERID", "gtitle", "gtags", "gdesc", "ginst", "days", "youtube", "category", "active", "feat", "feat_cat", "feat_mainpage", "rating", "viewcount", "price");
		foreach ($arr as $value)
		{
			$sql = "update `posts` set {$value}='".sql_quote($_POST[$value])."' where `PID`='".sql_quote($PID)."' LIMIT 1";
			$conn->execute($sql);
		}
		$message = "Gig Successfully Edited.";
		abr('message',$message);
	}
}

if($PID > 0)
{
	$query = $conn->execute("select * from posts where PID='".sql_quote($PID)."' limit 1");
	$gig = $query->getrows();
	abr('gig', $gig[0]);
}

$mainmenu = "4";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/gigs_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>