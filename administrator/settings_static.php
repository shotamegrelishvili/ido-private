<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform1'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title1'])."', value='".sql_quote($_POST['value1'])."' where ID='1'";
	$conn->execute($sql);
	$message = "Terms Of Use Successfully Saved.";
	abr('message',$message);
}

if($_POST['submitform2'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title2'])."', value='".sql_quote($_POST['value2'])."' where ID='2'";
	$conn->execute($sql);
	$message = "Privacy Policy Successfully Saved.";
	abr('message',$message);
}

if($_POST['submitform3'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title3'])."', value='".sql_quote($_POST['value3'])."' where ID='3'";
	$conn->execute($sql);
	$message = "About Us Successfully Saved.";
	abr('message',$message);
}

if($_POST['submitform4'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title4'])."', value='".sql_quote($_POST['value4'])."' where ID='4'";
	$conn->execute($sql);
	$message = "Advertising Successfully Saved.";
	abr('message',$message);
}

if($_POST['submitform5'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title5'])."', value='".sql_quote($_POST['value5'])."' where ID='5'";
	$conn->execute($sql);
	$message = "Contact Us Successfully Saved.";
	abr('message',$message);
}

if($_POST['submitform6'] == "1")
{
	$sql = "update static set title='".sql_quote($_POST['title6'])."', value='".sql_quote($_POST['value6'])."' where ID='6'";
	$conn->execute($sql);
	$message = "Job Levels Successfully Saved.";
	abr('message',$message);
}

$query = $conn->execute("select * from static");
$static = $query->getrows();
abr('static1', $static[0]);
abr('static2', $static[1]);
abr('static3', $static[2]);
abr('static4', $static[3]);
abr('static5', $static[4]);
abr('static6', $static[5]);

$mainmenu = "2";
$submenu = "7";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_static.tpl");
$smarty->display("administrator/global_footer.tpl");
?>