<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("enable_alertpay", "alertpay_email", "ap_code", "alertpay_currency");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$message = "Payza Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "7";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_alertpay.tpl");
$smarty->display("administrator/global_footer.tpl");
?>