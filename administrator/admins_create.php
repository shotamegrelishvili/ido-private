<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$username = $_POST[username];
	$password = $_POST[password];
	$email = $_POST[email];
	
	if($username == "")
	{
		$error = "Error: Please enter a username.";
	}
	elseif($password == "")
	{
		$error = "Error: Please enter a password.";
	}
	elseif($email == "")
	{
		$error = "Error: Please enter a e-mail address.";
	}
	else
	{
		$sql="select count(*) as total from administrators WHERE username='".sql_quote($username)."'";
		$executequery = $conn->Execute($sql);
		$tadmins = $executequery->fields[total];
		
		if($tadmins == "0")
		{ 
			$sql="select count(*) as total from administrators WHERE email='".sql_quote($email)."'";
			$executequery = $conn->Execute($sql);
			$tadmins = $executequery->fields[total];
			
			if($tadmins == "0")
			{
				$sql = "insert administrators set username='".sql_quote($username)."', password='".sha1('((-Mm7,1'.md5(md5($password)))."', email='".sql_quote($email)."'";
				$conn->execute($sql);
				$message = "Administrator Successfully Added.";
				abr('message',$message);
			}
			else
			{
				$error = "Error: The e-mail address $email is already taken.";
			}
		}
		else
		{
			$error = "Error: The username $username is already taken.";
		}
	}
}

$mainmenu = "12";
$submenu = "1";
abr('error',$error);
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/admins_create.tpl");
$smarty->display("administrator/global_footer.tpl");
?>