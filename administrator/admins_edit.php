<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

$ADMINID = intval($_REQUEST[ADMINID]);

if($_POST['submitform'] == "1")
{
	$username = $_POST[username];
	$password = $_POST[password];
	$email = $_POST[email];
	
	if($ADMINID > 0)
	{
		if($username == "")
		{
			$error = "Error: Please enter a username.";
		}
		elseif($email == "")
		{
			$error = "Error: Please enter a e-mail address.";
		}
		else
		{
			$sql="select count(*) as total from administrators WHERE username='".sql_quote($username)."' AND ADMINID!='".sql_quote($ADMINID)."'";
			$executequery = $conn->Execute($sql);
			$tadmins = $executequery->fields[total];
						
			if($tadmins == "0")
			{ 
				$sql="select count(*) as total from administrators WHERE email='".sql_quote($email)."' AND ADMINID!='".sql_quote($ADMINID)."'";
				$executequery = $conn->Execute($sql);
				$tadmins = $executequery->fields[total];
				
				if($tadmins == "0")
				{
					$addtosql = "";
					if ($password != "")
					{
						$newpassword = escape($password);
						$newpassword = sha1('((-Mm7,1'.md5(md5($newpassword)));
						$addtosql = ", password = '".sql_quote($newpassword)."'"; 
					}
	
					$sql = "UPDATE administrators set username='".sql_quote($username)."', email='".sql_quote($email)."' $addtosql WHERE ADMINID='".sql_quote($ADMINID)."'";
					$conn->execute($sql);
					$message = "Administrator Successfully Edited.";
					abr('message',$message);
					
					if($_SESSION['ADMINID'] == $ADMINID)
					{
						$_SESSION['ADMINUSERNAME'] = $username;
						
						if ($password != "")
						{
							$_SESSION['ADMINPASSWORD'] = $newpassword;
						}
					}
					
				}
				else
				{
					$error = "Error: The e-mail address $email is already taken.";
				}
			}
			else
			{
				$error = "Error: The username $username is already taken.";
			}
		}
	}
}

if($ADMINID > 0)
{
	$query = $conn->execute("select * from administrators where ADMINID='".sql_quote($ADMINID)."' limit 1");
	$admin = $query->getrows();
	abr('admin', $admin[0]);
}

$mainmenu = "12";
$submenu = "1";
abr('error',$error);
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/admins_edit.tpl");
$smarty->display("administrator/global_footer.tpl");
?>