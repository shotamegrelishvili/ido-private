<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$name = htmlentities(strip_tags($_REQUEST['name']), ENT_COMPAT, "UTF-8");
	$seo = htmlentities(strip_tags($_REQUEST['seo']), ENT_COMPAT, "UTF-8");
	$seo = str_replace("\/", "", $seo);
	$seo = str_replace("/", "-", $seo);
	$seo = str_replace("&amp;", "", $seo);
	$seo = str_replace("&", "", $seo);
	$seo = str_replace(" ", "", $seo);
	$parent = intval(cleanit($_POST['parent']));
	$details = cleanit($_POST['details']);
	$mtitle = cleanit($_POST['mtitle']);
	$mdesc = cleanit($_POST['mdesc']);
	$mtags = cleanit($_POST['mtags']);
	$sql = "insert categories set name='".sql_quote($name)."', seo='".sql_quote($seo)."', parent='".sql_quote($parent)."', details='".sql_quote($details)."', mtitle='".sql_quote($mtitle)."', mdesc='".sql_quote($mdesc)."', mtags='".sql_quote($mtags)."'";
	$conn->execute($sql);
	$message = "Category Successfully Added.";
	abr('message',$message);
}

$mainmenu = "3";
$submenu = "1";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/cat_add.tpl");
$smarty->display("administrator/global_footer.tpl");
?>