<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$details = cleanit($_POST['details']);
	$code = $_POST['c'];
	$active = intval($_POST[active]);
	
	if($details == "")
	{
		$error = "Error: Please enter a description.";
	}
	elseif($code == "")
	{
		$error = "Error: Please enter your advertisement code.";
	}
	else
	{
		$sql = "insert advertisements set description='".sql_quote($details)."', code='".sql_quote($code)."', active='".sql_quote($active)."'";
		$conn->execute($sql);
		$message = "Advertisement Successfully Added.";
		abr('message',$message);
	}
}

$mainmenu = "11";
$submenu = "1";
abr('error',$error);
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/ads_create.tpl");
$smarty->display("administrator/global_footer.tpl");
?>