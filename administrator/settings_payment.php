<?php
include("../include/config.php");
include_once("../include/functions/import.php");
verify_login_admin();

if($_POST['submitform'] == "1")
{
	$arr = array("price_mode", "price", "commission", "days_before_withdraw", "commission_percent", "fprice", "fdays");
	foreach ($arr as $value)
	{
		$sql = "update config set value='".sql_quote($_POST[$value])."' where setting='$value'";
		$conn->execute($sql);
		abr($value,strip_mq_gpc($_POST[$value]));
	}
	$pmode = intval(cleanit($_POST['price_mode']));
	if($pmode != "3")
	{
		$sql = "update config set value='0' where setting='enable_levels'";
		$conn->execute($sql);
	}
	$message = "Payment Settings Successfully Saved.";
	abr('message',$message);
}

$mainmenu = "2";
$submenu = "4";
abr('mainmenu',$mainmenu);
abr('submenu',$submenu);
$smarty->display("administrator/global_header.tpl");
$smarty->display("administrator/settings_payment.tpl");
$smarty->display("administrator/global_footer.tpl");
?>