<?php
include("include/config.php");
include("include/functions/import.php");

$thebaseurl = $config['baseurl'];

$s = $_GET['order'];
if (!isset($_GET['type']) || !$_GET['type'] || $_GET['type'] == 'recent') { $t = 'recent'; }
elseif ($_GET['type'] == 'bookmarks' && isset($_SESSION['USERID']) && $_SESSION['USERID']) $t = 'bookmarks';
elseif ($_GET['type'] == 'category' && isset($_GET['catid'])) { $t = 'category'; $catid = intval($_GET['catid']); }
elseif ($_GET['type'] == 'suggested') { $t = 'wants'; $catid = intval($_GET['catid']); }
else die('Permission denied.');

if($s == "r")
{
	$dby = "A.rating desc";	
}
elseif($s == "rz")
{
	$dby = "A.rating asc";	
}
elseif($s == "p")
{
	$dby = "A.viewcount desc";	
}
elseif($s == "pz")
{
	$dby = "A.viewcount asc";	
}
elseif($s == "c")
{
	$dby = "A.price asc";	
}
elseif($s == "cz")
{
	$dby = "A.price desc";	
}
elseif($s == "dz")
{
	$dby = "A.PID asc";	
}
else
{
	$dby = "A.PID desc";	
}

if($s == "ez")
{
	$dby = "A.PID asc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}
elseif($s == "e")
{
	$dby = "A.PID desc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}
else $addsqlb = '';

$addprice = '';

if (isset($_GET['start']))
$pagingstart = intval($_GET['start']);
else
$pagingstart = $config[items_per_page];

if ($t == 'recent')
	$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`location`, C.`toprated` FROM `posts` A, `categories` B, `members` C WHERE A.`active`='1' AND A.`is_del`='0' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $addsqlb $addprice ORDER BY A.`feat_mainpage` DESC, $dby LIMIT $pagingstart, ".($config[items_per_page]+3);
elseif ($t == 'bookmarks')
	$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`location` FROM `posts` A, `categories` B, `members` C, `bookmarks` D WHERE A.`active`='1' AND A.`is_del`='0' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND D.`USERID`='".sql_quote($_SESSION['USERID'])."' AND A.`PID`=D.`PID` ORDER BY D.`BID` DESC LIMIT $pagingstart, ".($config[items_per_page]+3);
elseif ($t == 'category')
	$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`location`, C.`toprated` FROM `posts` A, `categories` B, `members` C WHERE A.`active`='1' AND A.`is_del`='0' AND A.`category`='".sql_quote($catid)."' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $addsqlb $addprice ORDER BY A.`feat_mainpage` DESC, A.`feat_cat` DESC, $dby LIMIT $pagingstart, ".($config[items_per_page]+3);
elseif ($t == 'wants')
	$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`location`, C.`toprated` FROM `wants` A, `categories` B, `members` C WHERE A.`active`='1' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $addsqlb $addprice ORDER BY A.`time_added` DESC LIMIT $pagingstart, ".($config[items_per_page]+3);
//d($query2);
$executequery2 = $conn->Execute($query2);
$foundrows = $executequery2->recordcount();
if ($foundrows)
	$posts = $executequery2->getrows();
else $posts = array();
if (sizeof($posts) < $config[items_per_page]+1) $no_more_items = 1; else { array_pop($posts); $foundrows--; $no_more_items = 0; }

if ($t != 'wants')
	$templateselect = "bit.tpl";
else {
	$templateselect = "suggest_container.tpl";
}

//TEMPLATES BEGIN
abr('error',$error);
abr('posts', $posts);
abr('itemsPerPage',$config[items_per_page]);
abr('no_more_items', $no_more_items);
abr('foundrows',$foundrows);
abr('loadmore','fadein');
$smarty->display($templateselect);
$smarty->display('script_update_ajax_loadmore.tpl');
//TEMPLATES END
?>