<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];

$uname = cleanit($_REQUEST['uname']);
if($uname != "")
{
	abr('uname',$uname);
	$query = "SELECT `USERID`, `addtime`, `description`, `toprated`, `score`, `rating`, `ratingcount`, `level`, `location`, `lastlogin` FROM `members` WHERE `username`='".sql_quote($uname)."' AND status='1' LIMIT 1"; 
	$executequery=$conn->execute($query);
	$USERID = $executequery->fields['USERID'];
	$addtime = $executequery->fields['addtime'];
	abr('addtime',$addtime);
	$desc = $executequery->fields['description'];
	abr('desc',$desc);
	$toprated = $executequery->fields['toprated'];
	abr('toprated',$toprated);
	$score = $executequery->fields['score'];
	abr('score',$score);
	$rating = $executequery->fields['rating'];
	abr('rating',$rating);
	$ratingcount = $executequery->fields['ratingcount'];
	abr('ratingcount',$ratingcount);
	$level = $executequery->fields['level'];
	abr('level',$level);
	$location = $executequery->fields['location'];
	abr('location',$location);
	$lastlogin = $executequery->fields['lastlogin'];
	abr('lastlogin',$lastlogin);
	if($USERID > 0)
	{
		abr('USERID',$USERID);
		$query = "SELECT A.*, B.`name`, B.`seo`, C.`username`, C.`profilepicture`, C.`rating`, C.`profilepicture`, C.`rating` FROM `posts` A, `categories` B, `members` C WHERE A.`active`='1' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND A.`USERID`='".sql_quote($USERID)."' ORDER BY A.`feat_mainpage`DESC, A.`feat_cat` DESC, A.`feat` DESC, A.`PID` DESC LIMIT 100000";
		$results=$conn->execute($query);
		$posts = $results->getrows();

		//$q = 'SELECT r.`comment`, r.`time_added`, SUBSTRING_INDEX(SUBSTRING_INDEX(`fullname`, " ", 1), " ", -1) as firstname, m.`profilepicture`, m.`location` FROM `ratings` r, `members` m WHERE r.`PID` IN (SELECT `PID` FROM `posts` WHERE `USERID`="'.intval($USERID).'") AND m.`USERID`=r.`USERID` ORDER BY RAND()';
		$q = 'SELECT r.`comment`, r.`time_added`, SUBSTRING_INDEX(SUBSTRING_INDEX(`fullname`, " ", 1), " ", -1) as firstname, m.`profilepicture`, m.`location` FROM `ratings` r, `members` m WHERE r.`USERID`="'.intval($USERID).'" AND m.`USERID`=r.`RATER` ORDER BY RAND()';
		$ratingsRes = $conn->execute($q);
		$ratings = $ratingsRes->getrows();

		$portfolioquery = "SELECT p.`FID` FROM `portfolio` p WHERE p.`USERID`='".intval($USERID)."' ORDER BY `order` ASC";
		$portfolioresults=$conn->execute($portfolioquery);
		$portfolio = $portfolioresults->getrows();

		$expResult = $conn->execute('SELECT * FROM `members_exp` WHERE `USERID`="'.intval($USERID).'" LIMIT 10');
		$expArr = $expResult->getrows();
		if (is_array($expArr)) {
			foreach ($expArr as $e=>$exp) {
				if ($exp) {
					$expClass = new expClass();
					$mainKeywords = $expClass->getMainKeys();
					$aliases = $expClass->getAliasKeys();
					if ($exp['KID'] && isset($mainKeywords[$exp['KID']])) {
						$expDisplay[$e] = array(
							'KID' => $exp['KID'],
							'title' => $mainKeywords[$exp['KID']]['keyword']
							);
					}

					if ($exp['ALIASID'] && isset($aliases[$exp['ALIASID']])) {
						$expDisplay[$e]['AID'] = $exp['ALIASID'];
						$expDisplay[$e]['title'] .= ' / '.$aliases[$exp['ALIASID']]['alias'];
					}
				}
			}
			abr('exp', $expDisplay);
		}

		abr('portfolioSettings',$config['portfolio']);
		abr('portfolio', $portfolio);
		abr('ratings', $ratings);
		abr('posts',$posts);
		abr('pagetitle',$uname);
		$t = 'user.tpl';
	}
	else
	{
		$t = 'error.tpl';
	}
}
else {
	header('Location:'.$config['baseurl'].'/'); exit;
}
abr('ogurl',$config['baseurl'].'/user/'.$uname);
abr('ogtitle',$config['site_name'].': '.$posts[0]['gtitle']);
abr('ogdescription',$posts[0]['gdesc']);
abr('ogimage',$config['purl'].'/t/'.$posts[0]['p1']);

//TEMPLATES BEGIN
abr('message',$message);
abr('itemsPerPage',$config[items_per_page]);
$smarty->display('header.tpl');
$smarty->display($t);
$smarty->display('footer.tpl');
//TEMPLATES END
?>