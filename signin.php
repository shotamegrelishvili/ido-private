<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
//d($_SESSION);//d($_REQUEST,1);

if (isset($_REQUEST['r']) && $_REQUEST['r']) {
	$redirect = base64_decode(cleanit(stripslashes($_REQUEST['r'])));
	$_SESSION['temp']['redirect'] = cleanit(stripslashes($_REQUEST['r']));
}

if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	if($redirect == '')
	{
		header("Location:$thebaseurl/");exit;
	}
	else
	{
		$rto = $thebaseurl.'/'.$redirect;
		header("Location:$rto");exit;
	}
}

abr('r',$r);
if ($config['enable_ref'] == "1")
{
	$ref = intval(cleanit(stripslashes($_REQUEST['ref'])));
	abr('ref',$ref);
}
if ($_REQUEST['econnect_signup'] && $_REQUEST['email'] && $_REQUEST['password']) {
	include('libraries/eConnect.class.php');
	$econnectClass = new econnect();
	if (!($res=$econnectClass->_Authorize($_REQUEST['email'], $_REQUEST['password']))) {
		$error .= $lang['628'];
	}
	else {
		$econnectClass->_UpdateSession($res);
		$econnected_user = $econnectClass->getUserInfo($_SESSION['access_token']);

		if ($econnected_user['email']) {
			$query="SELECT `USERID`, `username`, `pin`, `mobile`, `profilepicture`, `status`, `FB_isFan` FROM `members` WHERE `eemail`='".sql_quote($econnected_user['email'])."' OR `email`='".sql_quote($econnected_user['email'])."' LIMIT 1";
			$result=$conn->execute($query);

			if (!$result->_numOfRows) {
				$username = getUniqueUsername($econnected_user, geo2lat(ucfirst(strtolower($econnected_user['first_name'])).ucfirst(mb_substr(strtolower($econnected_user['last_name']),0,3,'UTF-8'))));

				$econnected_user['USERID'] = registerNewUser($econnected_user, $username);
				signinUser($econnected_user, $username);
				$_SESSION['temp']['message_title'] = $lang['629'];
				$_SESSION['temp']['message_type'] = 'success';
				header("Location:$config[baseurl]/settings"); exit;
			}
			else {
				if ($result->fields['status'] == '0') {
					$no_redir = true;
					include("logout.php");
					$_SESSION['temp']['message_type'] = 'error';
					$_SESSION['temp']['message_title'] = $lang[745];
					header("Location:$config[baseurl]/signin"); exit;
				}
				$r=$econnectClass->_UpdateUser($econnected_user, $result->fields['USERID']);
				if ($r) {
					$econnected_user['USERID'] = $result->fields['USERID'];
					$econnected_user['FB_isFan'] = $result->fields['FB_isFan'];
				}
				signinUser($econnected_user, $result->fields['username']);
				if($redirect == '')
				{
					header("Location:$thebaseurl/");exit;
				}
				else
				{
					$rto = $thebaseurl.'/'.$redirect;
					header("Location:$rto");exit;
				}
			}
		}
	}
	//d($econnectClass->Authenticate($_GET['email'], $_GET['password']));
}
if($_REQUEST['jsub'] == "1" && FALSE)
{
	$user_email = cleanit($_REQUEST['user_email']);
	if($user_email == "")
	{
		$error .= "<li>".$lang['12']."</li>";
	}
	elseif(!verify_valid_email($user_email))
	{
		$error .= "<li>".$lang['15']."</li>";
	}
	elseif (!verify_email_unique($user_email))
	{
		$error .= "<li>".$lang['16']."</li>";
	}
	
	$user_username = cleanit($_REQUEST['user_username']);
	if($user_username == "")
	{
		$error .= "<li>".$lang['13']."</li>";	
	}
	elseif(strlen($user_username) < 4)
	{
		$error .= "<li>".$lang['25']."</li>";	
	}
	elseif(strlen($user_username) > 15)
	{
		$error .= "<li>".$lang['508']."</li>";	
	}
	elseif(!preg_match("/^[a-zA-Z0-9]*$/i",$user_username))
	{
		$error .= "<li>".$lang['24']."</li>";
	}
	elseif(!verify_email_username($user_username))
	{
		$error .= "<li>".$lang['14']."</li>";
	}
	
	$user_password = cleanit($_REQUEST['user_password']);
	$user_password2 = str_replace(" ", "", $user_password);
	if($user_password == "" || $user_password2 == "")
	{
		$error .= "<li>".$lang['17']."</li>";	
	}
	
	$user_captcha_solution = cleanit($_REQUEST['user_captcha_solution']);
	if($user_captcha_solution == "")
	{
		$error .= "<li>".$lang['18']."</li>";	
	}
	elseif($user_captcha_solution != $_SESSION['imagecode'])
	{
		$error .= "<li>".$lang['19']."</li>";	
	}
	
	$user_terms_of_use = cleanit($_REQUEST['user_terms_of_use']);
	if($user_terms_of_use != "1")
	{
		$error .= "<li>".$lang['20']."</li>";	
	}
	
	if($error == "")
	{
		$md5pass = md5($user_password);
		$def_country = $config['def_country'];
		if($def_country == "")
		{
			$def_country = "GE";
		}
		$query="INSERT INTO `members` SET `email`='".sql_quote($user_email)."',`username`='".sql_quote($user_username)."', `password`='".sql_quote($md5pass)."', `pwd`='".sql_quote($user_password)."', `addtime`='".$_SERVER['REQUEST_TIME']."', `lastlogin`='".$_SERVER['REQUEST_TIME']."', `ip`='".$_SERVER['REMOTE_ADDR']."', `lip`='".$_SERVER['REMOTE_ADDR']."', `country`='".sql_quote($def_country)."'";
		$result=$conn->execute($query);
		$userid = $conn->_insertid();
		
		if($userid != "" && is_numeric($userid) && $userid > 0)
		{
			$query="SELECT `USERID`,`email`,`username`,`verified` FROM `members` WHERE `USERID`='".sql_quote($userid)."' LIMIT 1";
			$result=$conn->execute($query);
			
			$SUSERID = $result->fields['USERID'];
			$SEMAIL = $result->fields['email'];
			$SUSERNAME = $result->fields['username'];
			$SVERIFIED = $result->fields['verified'];
			$_SESSION['USERID']=$SUSERID;
			$_SESSION['EMAIL']=$SEMAIL;
			$_SESSION['USERNAME']=$SUSERNAME;
			$_SESSION['VERIFIED']=$SVERIFIED;
			
			// Generate Verify Code Begin
			$verifycode = generateCode(5).time();
			$query = "INSERT INTO `members_verifycode` SET `USERID`='".sql_quote($SUSERID)."', `code`='$verifycode'";
            $conn->execute($query);
			if(mysql_affected_rows()>=1)
			{
				$proceedtoemail = true;
			}
			else
			{
				$proceedtoemail = false;
			}
			// Generate Verify Code End
			
			// Send Welcome E-Mail Begin
			if ($proceedtoemail)
			{
                $sendto = $SEMAIL;
                $sendername = $config['site_name'];
                $from = $config['site_email'];
                $subject = $lang['21']." ".$sendername;
                $sendmailbody = stripslashes($_SESSION['USERNAME']).",<br><br>";
				$sendmailbody .= $lang['22']."<br>";
				$sendmailbody .= "<a href=".$config['baseurl']."/confirmemail?c=$verifycode>".$config['baseurl']."/confirmemail?c=$verifycode</a><br><br>";
				$sendmailbody .= $lang['23'].",<br>".stripslashes($sendername);
                mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
			}
			// Send Welcome E-Mail End
			
			if ($config['enable_ref'] == "1")
			{
				$ref_price = cleanit($config['ref_price']);
				if($ref > 0)
				{
					$query = "INSERT INTO referrals SET USERID='".sql_quote($ref)."', REFERRED='".sql_quote($SUSERID)."', money='".sql_quote($ref_price)."', time_added='".time()."', ip='".$_SERVER['REMOTE_ADDR']."'";
            		$conn->execute($query);	
				}
			}
			
			if($redirect == "")
			{
				header("Location:$thebaseurl/");exit;
			}
			else
			{
				$rto = $thebaseurl."/".$redirect;
				header("Location:$rto");exit;
			}
		}	
	}
	else
	{
		abr('user_email',$user_email);
		abr('user_username',$user_username);
		abr('user_password',$user_password);
		abr('user_password2',$user_password2);
		abr('user_terms_of_use',$user_terms_of_use);
	}
}

$templateselect = "signup.tpl";
if (isset($_GET['signup']))
	$pagetitle = $lang['1'];
else
	$pagetitle = $lang['2'];
abr('pagetitle',$pagetitle);
abr('site_name',$config['site_name']);
abr('FB_Fan_hide',1);
//TEMPLATES BEGIN
abr('error',$error);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>