<?php
/*
if (isset($_GET['access']) && $_GET['access'] == '5lari-Team') {
	setcookie('access', '5lari-Team', $_SERVER['REQUEST_TIME'] + 86400, '.');
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/'.str_replace('/index.php', '', $_SERVER['SCRIPT_NAME'])); exit;
}
elseif ($_SERVER['HTTP_HOST'] != 'localhost' && (!isset($_COOKIE['access']) || $_COOKIE['access'] != '5lari-Team')) die('access denied');
*/
include('include/config.php');
include('include/functions/import.php');
/*
require_once('libraries/cache.class.php');
$usersTemp = $cache->get('usersTemp');
d($usersTemp);
d($config['def_country']);
$usersQuery = $cache->get('usersQuery');
d($usersQuery);
*/

$thebaseurl = $config['baseurl'];

if (isset($_SESSION['temp']['redirect']) && $_SESSION['temp']['redirect'] && isset($_SESSION['USERID']) && $_SESSION['USERID']) {
	$r = $_SESSION['temp']['redirect'];
	unset($_SESSION['temp']['redirect']);
	header('Location:'.$thebaseurl.'/'.base64_decode($r)); exit;
}

$s = cleanit(@$_REQUEST['s']);
abr('s',$s);

$page = intval(@$_REQUEST['page']);

if($page=='')
{
	$page = '1';
}
$currentpage = $page;

if ($page >=2)
{
	$pagingstart = ($page-1)*$config['items_per_page'];
}
else
{
	$pagingstart = "0";
}

if($s == "r")
{
	$dby = "A.`rating` desc, A.`rcount` desc";	
}
elseif($s == "rz")
{
	$dby = "A.rating asc";	
}
elseif($s == "p")
{
	$dby = "A.viewcount desc";	
}
elseif($s == "pz")
{
	$dby = "A.viewcount asc";	
}
elseif($s == "c")
{
	$dby = "A.price asc";	
}
elseif($s == "cz")
{
	$dby = "A.price desc";	
}
elseif($s == "dz")
{
	$dby = "A.PID asc";	
}
else
{
	$dby = "A.PID desc";	
}

if($s == "ez")
{
	$dby = "A.PID asc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}
elseif($s == "e")
{
	$dby = "A.PID desc";	
	$addsql = "AND days='1'";
	$addsqlb = "AND A.days='1'";
}
else $addsql = $addsqlb = '';

/*$p = intval(cleanit(@$_REQUEST['p']));
if($p > 0)
{
	$result_addprice = " AND A.price='".sql_quote($p)."'";
	$result_addpriced = " AND price='".sql_quote($p)."'";
	abr('p',$p);
	$addp = "&p=$p";
}
else 
*/

$result_addprice = $result_addpriced = '';

$query1 = "SELECT count(*) as `total` from `posts` where `active`='1' AND `is_del`='0' {$addsql} {$result_addpriced} ORDER BY `PID` DESC LIMIT {$config['maximum_results']}";
$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`country`, C.`toprated`, C.`profilepicture`, C.`rating` FROM `posts` A, `categories` B, `members` C where A.`active`='1' AND A.`is_del`='0' AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $addsqlb $result_addprice ORDER BY A.`feat_mainpage` DESC, $dby LIMIT {$pagingstart}, ".($config[items_per_page]+20);
//d($query2);
$executequery1 = $conn->Execute($query1);
$result = $executequery1->fields['total'];

if ($result > 0)
{
	if($executequery1->fields['total']<=$config[maximum_results])
	{
		$total = $executequery1->fields['total'];
	}
	else
	{
		$total = $config[maximum_results];
	}
	$toppage = ceil($total/$config[items_per_page]);
	if($toppage==0)
	{
		$xpage=$toppage+1;
	}
	else
	{
		$xpage = $toppage;
	}
	$executequery2 = $conn->Execute($query2);
	$posts = $executequery2->getrows();
	$beginning=$pagingstart+1;
	$ending=$pagingstart+$executequery2->recordcount();
	$pagelinks="";
	$k=1;
	$theprevpage=$currentpage-1;
	$thenextpage=$currentpage+1;
	if($s != "")
	{
		$adds = "&s=$s".$addp;
	}
	if ($currentpage > 0)
	{
		if($currentpage > 1) 
		{
			$pagelinks.="<li class='prev'><a href='$thebaseurl/?page=$theprevpage$adds'>$theprevpage</a></li>&nbsp;";
		}
		else
		{
			$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
		}
		$counter=0;
		$lowercount = $currentpage-5;
		if ($lowercount <= 0) $lowercount = 1;
		while ($lowercount < $currentpage)
		{
			$pagelinks.="<li><a href='$thebaseurl/?page=$lowercount$adds'>$lowercount</a></li>&nbsp;";
			$lowercount++;
			$counter++;
		}
		$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
		$uppercounter = $currentpage+1;
		while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
		{
			$pagelinks.="<li><a href='$thebaseurl/?page=$uppercounter$adds'>$uppercounter</a></li>&nbsp;";
			$uppercounter++;
		}
		if($currentpage < $toppage) 
		{
			$pagelinks.="<li class='next'><a href='$thebaseurl/?page=$thenextpage$adds'>$thenextpage</a></li>";
		}
		else
		{
			$pagelinks.="<li><span class='next'>next page</span></li>";
		}
	}
}

$templateselect = "index.tpl";


//TEMPLATES BEGIN
abr('ogimage',$config['baseurl'].'/images/iDo-logo-medium_2.png');
abr('ogurl', $config['baseurl']);
abr('ogtitle',$config['site_name'].': '.$config['site_slogan']);
abr('ogdescription',$config['metadescription']);

if (isset($_GET['logout'])) {
	abr('logout',true);
	if ($_SESSION['signin_gateway'] == 'econnect') {
		abr('eConnect_disconnect',1);
	}
	destroy_slrememberme($_SESSION[USERNAME]);
	unset($_SESSION);
	session_destroy();
}

abr('autostart_tour', autostart_tour());
abr('pagetitle',stripslashes($config['site_slogan']));
abr('message',@$message);
abr('beginning',@$beginning);
abr('ending',@$ending);
abr('itemsPerPage',$config[items_per_page]);
//abr('pagelinks',@$pagelinks);
abr('total',@$total);
abr('posts',@$posts);
abr('main_page',true);
abr('enable_fc',$config['enable_fc']);
abr('environment', $config['environment']);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>