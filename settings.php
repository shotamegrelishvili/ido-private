<?php
include("include/config.php");
include("include/functions/import.php");

define('IN_APP',1);

$SID = $_SESSION['USERID'];
if ($SID != "" && $SID >= 0 && is_numeric($SID))
{	
	$SID = intval($_SESSION['USERID']);
	$messageadd = '';
	if (isset($_POST['econnect_signup']) && $_POST['econnect_email'] && $_POST['econnect_password']) {
		include('libraries/eConnect.class.php');
		$econnectClass = new econnect();

		if ($econnectClass->_CheckProtectedFields($SID, array())) {
			$error .= "<li>".$lang['636']."</li>";
		}
		else {
			if (!($res=$econnectClass->_Authorize($_REQUEST['econnect_email'], $_REQUEST['econnect_password']))) {
				$error .= "<li>".$lang['628']."</li>";
			}
			else {
				$econnectClass->_UpdateSession($res);
				$econnected_user = $econnectClass->getUserInfo($_SESSION['access_token']);

				if ($econnectClass->_CheckDuplicateUser($econnected_user) && !$error) {
					$error .= "<li>".$lang['561']."</li>";
				}
				elseif (!$error) {
					$econnectClass->_UpdateUser($econnected_user);
					$message = $lang['172'].$messageadd;
				}
			}
		}
	}


// Needed for Submit and general view
require ('models/settings.Class.php');
$settings = new settingsClass();
$settings->countFilledFields();
$services = $settings->getUserVAS($_SESSION['USERID']);

require ('libraries/crypt.class.php');
$crypt = new cryptClass();

if ($_POST['subform']==1) {
	$_GET['tab'] = $_POST['tab'];
	$fname = cleanit($_POST['fname']);	
	$user_email = cleanit($_POST['email']);
	$query="SELECT `mobile`, `pin`, `pinprotect`,`eemail`,`eemailprotect`, `profilepicture` FROM `members` WHERE `USERID`='".sql_quote($SID)."' AND `status`='1' LIMIT 1";
	$results=$conn->execute($query);
	$profilepicture = $results->fields['profilepicture'];
	$pin = $results->fields['pin'];
	$pinprotect = $results->fields['pinprotect'];
	if (!$pin && $_POST['pin'])
		$pin = cleanit($_POST['pin']);
	elseif ($pin) $_POST['pin'] = $pin;
	$emoney = $results->fields['eemail'];
	$eemailprotect = $results->fields['eemailprotect'];
	if (!$emoney || !$eemailprotect) {
		$emoney = cleanit($_POST['emoney']);
	}
	elseif ($emoney && $eemailprotect) $_POST['emoney'] = $emoney;
	//$paypal = cleanit($_POST['paypal']);	
	//$alertpay = cleanit($_POST['alertpay']);
	$details = cleanit($_POST['details']);
	$location = cleanit($_POST['location']);
	$mobile = cleanit($_POST['mobile']);
	if (!$mobile) $mobile = substr($results->fields['mobile'],-9);

	if($user_email == "")
	{
		$error .= "<li>".$lang['12']."</li>";
		$csserror['email'] = true;
	}
	elseif(!verify_valid_email($user_email))
	{
		$error .= "<li>".$lang['15']."</li>";
		$user_email = $_POST['email'] = '';
		$csserror['email'] = true;
	}
	else
	{
		$query = "SELECT COUNT(*) as total FROM `members` WHERE `email`='".sql_quote($user_email)."' AND `USERID`!='".sql_quote($SID)."' LIMIT 1"; 
		$executequery = $conn->execute($query);
		$te = $executequery->fields[total]+0;
		if($te > 0)
		{
			$error .= "<li>".$lang['16']."</li>";
			$user_email = $_POST['email'] = '';
			$csserror['email'] = true;
		}
	}
	if($fname == '' || trim($fname) == '' || mb_strlen($fname) < 5)
	{
		$error .= "<li>".$lang['559']."</li>";
		$fname = $_POST['fname'] = '';
		$csserror['fname'] = true;
	}
	if($mobile == '' || !is_numeric($mobile) || strlen($mobile) != 9 || substr($mobile, 0, 1) != '5' || substr($mobile, 0, 3) == '500' || substr($mobile, 0, 3) == '511' || substr($mobile, 0, 3) == '512') {
		$error .= "<li>".$lang['763'];
		$mobile = $_POST['mobile'] = '';
		$csserror['mobile'] = true;
	}
	else {
		$query = "SELECT count(*) as total FROM `members` WHERE `mobile` like '%".sql_quote($mobile)."' AND `USERID`!='".sql_quote($SID)."' LIMIT 1";
		$executequery = $conn->execute($query);
		$mu = $executequery->fields[total]+0;
		if($mu > 0)
		{
			$error .= "<li>".$lang['765']."</li>";
			$mobile = $_POST['mobile'] = '';
			$csserror['mobile'] = true;
		}
	}
	if($pin == '' || trim($pin) == '' || strpos($pin,'00000000000')!==FALSE ||  strpos($pin,'11111111111')!==FALSE || strpos($pin,'123456789')!==FALSE)
	{
		$error .= "<li>".$lang['558']."</li>";
		$pin = $_POST['pin'] = '';
		$csserror['pin'] = true;
	}
	else {
		$query = "SELECT count(*) as total FROM `members` WHERE `pin`='".sql_quote($pin)."' AND `USERID`!='".sql_quote($SID)."' limit 1"; 
		$executequery = $conn->execute($query);
		$te = $executequery->fields[total]+0;
		if($te > 0)
		{
			$error .= "<li>".$lang['560']."</li>";
			$user_email = $_POST['pin'] = '';
			$csserror['pin'] = true;
		}
	}


	if($config['enable_paypal'] == "1")
	{
		if($emoney == "" || !verify_valid_email($emoney))
		{
			$error .= "<li>".$lang['171']."</li>";
			$emoney = $_POST['emoney'] = '';
			$csserror['emoney'] = true;
		}
		else
		{
			$query = "SELECT count(*) as total FROM `members` WHERE `eemail`='".sql_quote($emoney)."' AND `USERID`!='".sql_quote($SID)."' limit 1"; 
			$executequery = $conn->execute($query);
			$te = $executequery->fields[total]+0;
			if($te > 0)
			{
				$error .= "<li>".$lang['561']."</li>";
				$emoney = $_POST['emoney'] = '';
				$csserror['emoney'] = true;
			}
			else
				$addme = ", eemail='".sql_quote($emoney)."'";
		}
	}
	if($config['enable_alertpay'] == "1" && FALSE)
	{
		if($alertpay != "")
		{
			if(!verify_valid_email($alertpay))
			{
				$error .= "<li>".$lang['454']."</li>";
			}
			else
			{
				$addme .= ", aemail='".sql_quote($alertpay)."'";	
			}
		}
		else
		{
			$addme .= ", aemail=''";
		}
	}
	if (isPMvalid($details)!==true || includes_link($details)) {
		$error .= "<li>".$lang['466']."</li>";
		$csserror['details'] = true;
	}

// Update Avatar
	if (isset($_POST['uploadFile'])) {
		$uploadClass = new uploadClass();
		$uploadClass->allowedTypes = array('image/jpg', 'image/jpeg', 'image/png');
		$uploadClass->filepath = $config['membersprofilepicdir'].'/o/';
		$uploadClass->filename = $SID.'-'.substr(md5($_SERVER['REQUEST_TIME']),-4);
		$uploadedPath = $uploadClass->processUpload($_POST['uploadFile']);

		if ($uploadedPath) {
			unlink($config['membersprofilepicdir'].'/o/'.$profilepicture);
			unlink($config['membersprofilepicdir'].'/thumbs/'.$profilepicture);
			unlink($config['membersprofilepicdir'].'/thumbs/A_'.$profilepicture);
			unlink($config['membersprofilepicdir'].'/'.$profilepicture);
			do_resize_image($config['membersprofilepicdir']."/o/".$uploadClass->filename.$uploadClass->fileext, "100", "100", false, $config['membersprofilepicdir']."/".$uploadClass->filename.$uploadClass->fileext, true);
			do_resize_image($config['membersprofilepicdir']."/o/".$uploadClass->filename.$uploadClass->fileext, "54", "54", false, $config['membersprofilepicdir']."/thumbs/".$uploadClass->filename.$uploadClass->fileext, true);
			do_resize_image($config['membersprofilepicdir']."/o/".$uploadClass->filename.$uploadClass->fileext, "32", "32", false, $config['membersprofilepicdir']."/thumbs/A_".$uploadClass->filename.$uploadClass->fileext, true);
			
			$query = "UPDATE `members` SET `profilepicture`='".sql_quote($uploadClass->filename.$uploadClass->fileext)."', `profilepicture_protect`='1' WHERE `USERID`='".sql_quote($SID)."' LIMIT 1";
			$conn->execute($query);
		}
	}
// Update Avatar end

// Store Experience
	if ($_POST['exp'] != array('KID=|AID=', 'KID=|AID=', 'KID=|AID=')) {
		$expClass = new expClass();
		$mainKeywords = $expClass->getMainKeys();
		$aliases = $expClass->getAliasKeys();
		$conn->execute('DELETE FROM `members_exp` WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 10');
		foreach ($_POST['exp'] as $e=>$exp) {
			$KID = $AID = null;
			if ($exp) {
				if (preg_match('/KID=(\d+)\|?/', $exp, $kArr)) {
					$KID = $kArr[1];
				}
				if (preg_match('/\|AID=(\d+)/', $exp, $aArr)) {
					$AID = $aArr[1];
				}
				$insertSQL = '';
				if (!$KID)
					continue;
				if ($KID && is_numeric($KID) && isset($mainKeywords[$KID])) {
					$expDisplay[$e+1] = array(
						'KID' => $KID,
						'title' => $mainKeywords[$KID]['keyword']
						);
					// Inserting MainKeyword
					$insertSQL = '`CATID`="'.intval($mainKeywords[$KID]['CATID']).'", `KID`="'.intval($KID).'"';
				}

				if ($AID && is_numeric($AID) && isset($aliases[$AID])) {
					$expDisplay[$e+1]['AID'] = $AID;
					$expDisplay[$e+1]['title'] .= ' / '.$aliases[$AID]['alias'];
					// Inserting Alias
					$insertSQL .= ', `ALIASID`="'.intval($AID).'"';
				}

				if ($insertSQL) {
					$conn->execute('INSERT IGNORE INTO `members_exp` SET `USERID`="'.intval($_SESSION['USERID']).'", '.$insertSQL);
				}
			}
		}
		abr('exp',$expDisplay);
	}
// Store Experience end

// Store Value-added
	if ($settings->ccFilledFields >= $settings->ccFields && $settings->resultFields['members_friends']['invites'] >= $config['value_added']['free_min_invites']) {
		$settings->updateUserVAS('setownprice', $_SESSION['USERID'], isset($_POST['setownprice']) ? 1 : 0);
		$settings->updateUserVAS('zerocommission', $_SESSION['USERID'], isset($_POST['zerocommission']) ? 1 : 0);
		$services = $settings->userVAS;
	}
// Store Value-added end

	if($error == '') {
		if($user_email != $_SESSION['EMAIL'])
		{
			$addmail = ",`email`='".sql_quote($user_email)."', `verified`='0'";
			$_SESSION['VERIFIED']= 0;
			$_SESSION['EMAIL']= $user_email;
			
			$verifycode = generateCode(5).time();
			$query = "DELETE FROM `members_verifycode` WHERE `USERID`='".sql_quote($SID)."'";
			$conn->execute($query);
			$query = "INSERT INTO `members_verifycode` SET `USERID`='".sql_quote($SID)."', `code`='".sql_quote($verifycode)."'";
			$conn->execute($query);

			$sendto = $_SESSION['EMAIL'];
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $lang['406'];

			$sendmailbody = stripslashes($_SESSION['USERNAME']).",<br /><br />";
			$sendmailbody .= $lang['482']."<br />";
			$sendmailbody .= "<a href=".$config['baseurl']."/confirmemail?c=$verifycode>".$config['baseurl']."/confirmemail?c=$verifycode</a><br /><br />";
			$sendmailbody .= $lang['704']."<br />".stripslashes($sendername)." - ".$lang['557'];
			$confirmationSentIPs = $cache->get('confirmationSentIPs');

			if (!isset($confirmationSentIPs[$_SERVER['REMOTE_ADDR']]) || $confirmationSentIPs[$_SERVER['REMOTE_ADDR']] < $_SERVER['REQUEST_TIME'] - 86400) {
				mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
				$confirmationSentIPs[$_SERVER['REMOTE_ADDR']] = $_SERVER['REQUEST_TIME'];
				$cache->set('confirmationSentIPs', $confirmationSentIPs, 86400);
				$messageadd = ' '.$lang['481'];
			}
		}
//			if (!$pin || !$pinprotect) $sqladd = ",`pin`='".sql_quote($pin)."'"; else $sqladd = '';
		$query="UPDATE `members` SET `fullname`='".sql_quote($fname)."' $addmail,`pin`='".sql_quote($pin)."', `location`='".sql_quote($location)."', `mobile`='".sql_quote($mobile)."' $addme , `description`='".sql_quote($details)."' WHERE `USERID`='".sql_quote($SID)."' AND `status`='1' LIMIT 1";
		$result=$conn->execute($query);

		$message = $lang['172'].$messageadd;
		if (isset($_POST['r']) && $_POST['r'] && $pin && $emoney) {
			$_SESSION['temp']['message_title'] = $message;
			$_SESSION['temp']['message_type'] = 'success';
			$redirect = base64_decode(cleanit(stripslashes($_POST['r'])));
			header("Location:$config[baseurl]/".$redirect); exit;
		}
	}

	$settings->countFilledFields();
} // END FORM SUBMIT

if($_POST['subpass'] == 1) {
	$pass = cleanit($_REQUEST['pass']);	
	$pass2 = cleanit($_REQUEST['pass2']);		
	if($pass == "")
	{
		$error .= "<li>".$lang['173']."</li>";
	}
	if($pass2 == "")
	{
		$error .= "<li>".$lang['174']."</li>";
	}
	if($pass != "" && $pass2 != "")
	{
		if($pass == $pass2)
		{
			$mp = md5($pass);
			$query = "UPDATE `members` SET `password`='".sql_quote($mp)."', `pwd`='".sql_quote($pass)."' WHERE `USERID`='".sql_quote($SID)."' AND `status`='1' AND LIMIT 1";
			$conn->execute($query);
			$message = $lang['176'];
		}
		else
		{
			$error .= "<li>".$lang['175']."</li>";
		}
	}
}

abr('pagetitle',$lang['31']);
$SID = intval($SID);
$query="SELECT m.* FROM `members` m WHERE m.`USERID`='".$SID."' AND m.`status`='1' LIMIT 1";
$results=$conn->execute($query);
$p = $results->getrows();
abr('p',$p[0]);
//$portfolioquery = "SELECT f.`fname`, f.`s`, f.`hash` FROM `portfolio` p, `files` f WHERE p.`USERID`='".$SID."' AND p.`FID`=f.`FID`";
$portfolioquery = "SELECT p.`FID` FROM `portfolio` p WHERE p.`USERID`='".$SID."' ORDER BY `order` ASC";
$portfolioresults=$conn->execute($portfolioquery);
$portfolio = $portfolioresults->getrows();
abr('portfolio', $portfolio);

// Show User experience
if (!$expDisplay) {
	$expResult = $conn->execute('SELECT * FROM `members_exp` WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 10');
	$expArr = $expResult->getrows();
	if (is_array($expArr)) {
		foreach ($expArr as $e=>$exp) {
			if ($exp) {
				$expClass = new expClass();
				$mainKeywords = $expClass->getMainKeys();
				$aliases = $expClass->getAliasKeys();
				if ($exp['KID'] && isset($mainKeywords[$exp['KID']])) {
					$expDisplay[$e+1] = array(
						'KID' => $exp['KID'],
						'title' => $mainKeywords[$exp['KID']]['keyword']
						);
				}

				if ($exp['ALIASID'] && isset($aliases[$exp['ALIASID']])) {
					$expDisplay[$e+1]['AID'] = $exp['ALIASID'];
					$expDisplay[$e+1]['title'] .= ' / '.$aliases[$exp['ALIASID']]['alias'];
				}
			}
		}
		abr('exp', $expDisplay);
	}
}
// Show User experience end
}
else
{
	header("Location:$config[baseurl]/");exit;
}

abr('resultFields',$settings->resultFields);
abr('currentProfileProgress', round(($settings->ccFilledFields/$settings->ccFields)*100));
abr('free_min_invites', $config['value_added']['free_min_invites']);
// Needed for security reasons in Ajax Uploader
$_SESSION['temp']['ajax_allow'] = $_SERVER['REQUEST_TIME'] + 3600;

//TEMPLATES BEGIN
abr('portfolio_allowed_types', implode(',',$config['portfolio']['allowed_types']));
$allowed_extensions = '';
foreach ($config['portfolio']['allowed_types'] as $t) {
	list($t,$ext) = explode('/',$t);
	$allowed_extensions .= ','.$ext;
}

if ($settings->requiredTab && !isset($_GET['tab'])) {
	$_GET['tab'] = $settings->requiredTab;
	abr('userSelectedTab', '');
}
else {
	abr('userSelectedTab', $_GET['tab']);
}

abr('refLink', $config['website_url'].'/?f='.base64_encode(($crypt->Encrypt($_SESSION['USERID']))));
abr('services', $services);
abr('fields',$settings->field2Tab);
abr('addCss', 'settings');
abr('plupload',true);
abr('allowed_extensions', ltrim($allowed_extensions,','));
abr('maxUploadSize', $config['portfolio']['maxUploadSize']);
abr('maxUploads', $config['portfolio']['maxUploads']);
abr('error',$error);
abr('csserror',$csserror);
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display('settings.tpl');
$smarty->display('footer.tpl');
//TEMPLATES END
?>