<?php
include("include/config.php");
include("include/functions/import.php");

$SID = intval(cleanit($_SESSION['USERID']));
if ($SID > "0")
{
	$query = "SELECT `code` FROM `members_verifycode` WHERE `USERID`='".sql_quote($SID)."' LIMIT 1";
	$executequery = $conn->execute($query);
	$verifycode = $executequery->fields['code'];	
	$sendto = $_SESSION['EMAIL'];
	$sendername = $config['site_name'];
	$from = $config['site_email'];
	$subject = $lang['406'];
	$sendmailbody = stripslashes($_SESSION['USERNAME']).",<br /><br />";
	$sendmailbody .= $lang['482']."<br />";
	$sendmailbody .= "<a href=".$config['baseurl']."/confirmemail?c=$verifycode>".$config['baseurl']."/confirmemail?c=$verifycode</a><br /><br />";
	$sendmailbody .= $lang['704']."<br />".stripslashes($sendername)." - ".$lang['557'];
	require_once('libraries/cache.class.php');
	$confirmationSentIPs = $cache->get('confirmationSentIPs');

	if (!isset($confirmationSentIPs[$_SERVER['REMOTE_ADDR']]) || $confirmationSentIPs[$_SERVER['REMOTE_ADDR']] < $_SERVER['REQUEST_TIME'] - 86400) {
		mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		$confirmationSentIPs[$_SERVER['REMOTE_ADDR']] = $_SERVER['REQUEST_TIME'];
		$cache->set('confirmationSentIPs', $confirmationSentIPs, 86400);
		$message = $lang['481'];
	}
	else
		$error = $lang['706'];
}
else
{
	$eurl = "resendconfirmation.php";
	$rurl = base64_encode($eurl);
	header("Location:$config[baseurl]/signin?r=".$rurl);exit;
}

abr('pagetitle',$lang['480']);
abr('message',$message);
abr('error',$error);

//TEMPLATES BEGIN
$smarty->display('header.tpl');
$smarty->display('resendconfirmation.tpl');
$smarty->display('footer.tpl');
//TEMPLATES END
?>