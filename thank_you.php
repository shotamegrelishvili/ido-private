<?php
include("include/config.php");
include("include/functions/import.php");

if(isset($_REQUEST['g']) && isset($_POST['item_number']))
{
	header("Location:$config[baseurl]/thank_you?g=".cleanit($_REQUEST['g']));exit;	
}
if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	
	$pagetitle = $lang['23'];
	abr('pagetitle',$pagetitle);
	
	$IID = intval(base64_decode(cleanit($_REQUEST['g'])));
	$query = "SELECT `PID`, `multi` FROM `order_items` WHERE `IID`='".sql_quote($IID)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
	$executequery=$conn->execute($query);
	$PID = $executequery->fields['PID'];
	$multi = $executequery->fields['multi'];
	
	if($PID > 0)
	{
		if($multi > 1)
		{
			$query = "SELECT `OID` FROM `orders` WHERE `PID`='".sql_quote($PID)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' ORDER BY `OID` DESC LIMIT $multi";
			$results = $conn->execute($query);
			$returnthis = $results->getrows();
			abr('OID',$returnthis);
			$templateselect = "thank_you_multi.tpl";
		}
		else
		{
			$query = "SELECT `OID` FROM `orders` WHERE `PID`='".sql_quote($PID)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' ORDER BY `OID` DESC LIMIT 1";
			$executequery=$conn->execute($query);
			$OID = $executequery->fields['OID'];
			abr('OID',$OID);
			$templateselect = "thank_you.tpl";
		}
	}
	else
	{
		header("Location:$config[baseurl]/orders");exit;
	}
}
else
{
	header("Location:$config[baseurl]/");exit;
}

//TEMPLATES BEGIN
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>