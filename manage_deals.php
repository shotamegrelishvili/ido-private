<?php
include("include/config.php");
include("include/functions/import.php");
/**
active:
0 = Not active
1 = Active
2 = Suspended
3 = Deleted by User
4 = Suspended by Admin
5 = Soft Reject (Returned by moderator to the user to improve/fix)
6 = Approved, awaiting PIN and eConnect
*/
if (!$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) {
	$_SESSION['temp']['message_title'] = $lang['724'];
	$_SESSION['temp']['message_type'] = 'error';
	header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('manage_deals?t='.$_GET['t'])); exit;
}
$sp = cleanit($_REQUEST['sp']);
$tag = sql_quote(cleanit(trim($_POST['search'])));
$searchActive = false;
if (!$sp && !$tag) $sp = 'active';

/*
d('totalOrders: '. $totalOrders);
d('dealsTotal: '. $dealsTotal);
d('pendingOrders: '. $pendingOrders);
*/
//if ((isset($_GET['t']) && $_GET['t']=='orders') || (!isset($_GET['t']) && $totalOrders && !$dealsTotal && !$pendingOrders)) {
//	include('orders.php');
//	exit;
//}

if (TRUE)
{
	if($_POST['subme'] == "1")
	{
		$suspend = intval(cleanit($_REQUEST['suspend']));
		$activate = intval(cleanit($_REQUEST['activate']));
		$delete = intval(cleanit($_REQUEST['delete']));
		if($suspend == "1")
		{
			$gig = $_POST['deal'];
			$gcnt = sizeof($gig);
			for ($i = 0; $i < $gcnt; $i++)
			{
				$tgig = intval($gig[$i]);
				if ($tgig > 0)
				{
					$query="UPDATE `posts` SET `active`='2' WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `active`='1' AND `is_del`='0' AND `PID`='".sql_quote($tgig)."' LIMIT 1";
					$result=$conn->execute($query);
					if ($conn->_affectedrows())
						$message = $lang['177'];
					else 
						abr('error',$lang['572']);
				}
			}
		}
		elseif($activate == "1")
		{
			$gig = $_POST['deal'];
			$gcnt = sizeof($gig);
			for ($i = 0; $i < $gcnt; $i++)
			{
				$tgig = intval($gig[$i]);
				if ($tgig > 0)
				{
					$approve_stories = $config['approve_stories'];
					if($approve_stories == "1")
					{
						$active = "0";
					}
					else
					{
						$active = "1";
					}
					$query="UPDATE `posts` SET `active`='".sql_quote($active)."' WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `PID`='".sql_quote($tgig)."' AND `active`!='0' AND `active`!='1' AND `active`!='2' AND `active`!='5' AND `is_del`!='1' LIMIT 1";
					$result=$conn->execute($query);
					$query="UPDATE `posts` SET `active`='1' WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `PID`='".sql_quote($tgig)."' AND `active`='2' AND `is_del`='0' LIMIT 1";
					$result=$conn->execute($query);
					if ($conn->_affectedrows())
						$message = $lang['178'];
					else 
						abr('error',$lang['573']);
				}
			}
		}
		elseif($delete == "1")
		{
			$gig = $_POST['deal'];
			$gcnt = sizeof($gig);
			for ($i = 0; $i < $gcnt; $i++)
			{
				$tgig = intval($gig[$i]);
				if ($tgig > 0)
				{
					$query="UPDATE `posts` SET `active`='3' WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' AND `PID`='".sql_quote($tgig)."' AND `is_del`='0' LIMIT 1";
					$result=$conn->execute($query);
					$message = $lang['179'];
				}
			}
		}
	}


	$USERID = intval($_SESSION['USERID']);
	$query="SELECT count(A.`OID`) as totalOrders FROM `orders` A WHERE A.`USERID`='".$USERID."'";
	$results=$conn->execute($query);
	$totalOrders = $results->fields['totalOrders'];

	$query="SELECT count(B.`PID`) as dealsTotal FROM `posts` B WHERE B.`USERID`='".$USERID."' AND (B.`active`='0' OR B.`active`='1' OR B.`active`='5' OR B.`active`='6') AND `is_del`='0'";
	$results=$conn->execute($query);
	$dealsTotal = $results->fields['dealsTotal'];

	$query="SELECT count(C.`OID`) as pendingOrders FROM `orders` C, `posts` D WHERE D.`USERID`='".$USERID."' AND C.`PID`=D.`PID` AND (C.`status`='0' OR C.`status`='1' OR C.`status`='4' OR C.`status`='6')";
	$results=$conn->execute($query);
	$pendingOrders = $results->fields['pendingOrders'];

	abr('totalOrders', $totalOrders);
	abr('dealsTotal', $dealsTotal);
	abr('pendingOrders', $pendingOrders);
	
	if($dealsTotal > 0)
	{
		include('manage_orders.php');
		
		$pagetitle = $lang['153'];
		abr('pagetitle',$pagetitle);
		
		$page = intval($_REQUEST['page']);
	
		if($page=="")
		{
			$page = "1";
		}
		$currentpage = $page;
		
		if ($page >=2)
		{
			$pagingstart = ($page-1)*$config['items_per_page'];
		}
		else
		{
			$pagingstart = "0";
		}
		
		$query1 = "SELECT COUNT(*) as total FROM `posts` A, `members` B WHERE B.`USERID`='".sql_quote($_SESSION['USERID'])."' AND A.`USERID`=B.`USERID` AND A.`active`!='3' AND A.`is_del`='0' ORDER BY A.`PID` DESC LIMIT $config[maximum_results]";
		if ($_REQUEST['tab'] == 'deals' && $tag && mb_strlen($tag, 'UTF-8') >= 3) {
			$query2 = "SELECT A.*, B.`seo`, C.`username` FROM `posts` A, `categories` B, `members` C WHERE A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND C.`USERID`='".sql_quote($_SESSION['USERID'])."' AND C.`USERID`=A.`USERID` AND A.`active`!='3' AND A.`is_del`='0' AND (A.`gtitle` LIKE '%{$tag}%' OR A.`gdesc` LIKE '%{$tag}%' OR A.`gtags` LIKE '%{$tag}%' OR A.`ginst` LIKE '%{$tag}%') ORDER BY A.`PID` DESC LIMIT $pagingstart, $config[items_per_page]";
			$s = '';
			abr('search', $tag);
		}
		else
			$query2 = "SELECT A.*, B.`seo`, C.`username` FROM `posts` A, `categories` B, `members` C WHERE A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` AND C.`USERID`='".sql_quote($_SESSION['USERID'])."' AND C.`USERID`=A.`USERID` AND A.`active`!='3' AND A.`is_del`='0' ORDER BY A.`PID` DESC LIMIT $pagingstart, $config[items_per_page]";
		$executequery1 = $conn->Execute($query1);
		$result = $executequery1->fields['total'];
		if ($result > 0)
		{
			if($executequery1->fields['total']<=$config[maximum_results])
			{
				$total = $executequery1->fields['total'];
			}
			else
			{
				$total = $config[maximum_results];
			}
			$toppage = ceil($total/$config[items_per_page]);
			if($toppage==0)
			{
				$xpage=$toppage+1;
			}
			else
			{
				$xpage = $toppage;
			}
			$executequery2 = $conn->Execute($query2);
			$posts = $executequery2->getrows();
			$beginning=$pagingstart+1;
			$ending=$pagingstart+$executequery2->recordcount();
			$pagelinks="";
			$k=1;
			$theprevpage=$currentpage-1;
			$thenextpage=$currentpage+1;
			if ($currentpage > 0)
			{
				if($currentpage > 1) 
				{
					$pagelinks.='<li><a href="'.$config[baseurl].'/manage_deals?tab=deals&amp;page='.$theprevpage.'">&laquo;</a></li>';
				}
				else
				{
					$pagelinks.='<li class="arrow unavailable">&laquo;</li>';
				}
				$counter=0;
				$lowercount = $currentpage-5;
				if ($lowercount <= 0) $lowercount = 1;
				while ($lowercount < $currentpage)
				{
					$pagelinks.='<li><a href="'.$config[baseurl].'/manage_deals?tab=deals&amp;page='.$lowercount.'">'.$lowercount.'</a></li>';
					$lowercount++;
					$counter++;
				}
				$pagelinks.='<li class="current"><a href="">'.$currentpage.'</a></li>';
				$uppercounter = $currentpage+1;
				while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
				{
					$pagelinks.='<li><a href="'.$config[baseurl].'/manage_deals?tab=deals&amp;page='.$uppercounter.'"">'.$uppercounter.'</a></li>';
					$uppercounter++;
				}
				if($currentpage < $toppage) 
				{
					$pagelinks.='<li class="arrow"><a href="'.$config[baseurl].'/manage_deals?tab=deals&amp;page='.$thenextpage.'">&raquo;</a></li>';
				}
				else
				{
					$pagelinks.='<li class="arrow unavailable">&raquo;</li>';
				}
			}
		}
		abr('dealsResultSize',sizeof($posts));
		abr('beginning',$beginning);
		abr('ending',$ending);
		abr('pagelinks',$pagelinks);
		abr('total',$total);
		abr('posts',$posts);
	}
	if ($totalOrders) {
		require_once('orders.php');
	}

}
else
{
	header("Location:$config[baseurl]/");exit;
}

if ($pendingOrders > 0) {
	require_once($config['basedir'].'/libraries/cache.class.php');
	$USERSESS = $cache->get('UserSess.'.$USERID);
	$USERSESS['last_update'] = $_SERVER['REQUEST_TIME'];
	abr('notify_new_orders', $USERSESS['new_orders']);
	$USERSESS['new_orders'] = $_SESSION['US']['new_orders'] = 0;
	$cache->set('UserSess.'.intval($_SESSION['USERID']), $USERSESS, 259200);
	setcookie('notify',NULL,0,'/','',false,true);
	abr('resetNotifier',true);
}

$templateselect = "manage_deals2.tpl";
//TEMPLATES BEGIN
abr('sp',$sp);
abr('message',$message);
abr('sm1',"1");
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>