<?php
if (!isset($insert_into_db)) {
include("include/config.php");
include("include/functions/import.php");
}

if((!isset($_GET['item']) && !isset($_GET['order'])) || (!isset($_SESSION['USERID']) && !$_SESSION['ADMINID'])) {
	header("Location:$config[baseurl]");exit;
}

if (isset($_GET['item'])) {
	$IID = intval($_GET['item']);
	$query = "SELECT `PID` FROM `order_items` WHERE `IID`='".sql_quote($IID)."' AND `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
	$executequery=$conn->execute($query);
	$PID = $executequery->fields['PID'];
	if(!$PID) {
		header("Location:$config[baseurl]");exit;
	}
	$query = "SELECT o.*, p.`USERID` as OWNERID, p.`moneyback` FROM `orders` o, `posts` p WHERE `IID`='".sql_quote($IID)."' AND o.`PID`=p.`PID` LIMIT 1";
}
elseif (isset($_GET['order'])) {
	$OID = intval($_GET['order']);
	$query = "SELECT o.*, p.`USERID` as OWNERID, p.`moneyback` FROM `orders` o, `posts` p WHERE `OID`='".sql_quote($OID)."' AND o.`PID`=p.`PID` LIMIT 1";
}
$executequery=$conn->execute($query);
$order = $executequery->getrows();
if (isset($order[0])) {
	if ($order[0]['USERID'] != $_SESSION['USERID'] && $order[0]['OWNERID'] != $_SESSION['USERID'] && (!isset($_SESSION['ADMINID']) || !$_SESSION['ADMINID'])) {
		sleep(2);
		header("Location:$config[baseurl]");exit;
	}
	$PID = $order[0]['PID'];
	if (isset($_GET['order'])) {
		$q2 = "SELECT `agreement_text` FROM `order_agreements` WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
		$executequery=$conn->execute($q2);
		if ($executequery->fields['agreement_text']) {
			$agreement_txt = gzuncompress(base64_decode($executequery->fields['agreement_text']));
			abr('agreement_txt',$agreement_txt);
		}
	}
	abr('order',$order[0]);
}
elseif (isset($_GET['order'])) {
	sleep(2);
	header("Location:$config[baseurl]");exit;
}

$query = "SELECT p.*, m.`fullname`, m.`pin` FROM `posts` p, `members` m WHERE p.`PID`='".sql_quote($PID)."' AND m.`USERID` = p.`USERID` LIMIT 1";
$executequery=$conn->execute($query);
//$OWNER = $executequery->fields['USERID'];
$side_post = $executequery->getrows();
$query = "SELECT m.`fullname`, m.`pin` FROM `members` m WHERE m.`USERID` = '".sql_quote($_SESSION['USERID'])."' LIMIT 1";
$executequery=$conn->execute($query);
$side_order = $executequery->getrows();
abr('side_post', $side_post[0]);
abr('side_order', $side_order[0]);
//TEMPLATES BEGIN
//abr('message',$message);

$real_needed_period_days = ceil(
						$side_post[0]['days']
						+ ($config['working_process']['review_period'] * ($config['working_process']['max_reject_times'] + 1))
						+ ($side_post[0]['days'] / 100 * $config['revision_period_procent'])
						+ 1);

$escrow_timestamp = strtotime('+'.($config['working_process']['payment_day_of_month']-1).' days', strtotime('first day of +1 month',strtotime('+ '.$real_needed_period_days.' days')));
$escrow_days = ceil(abs(($escrow_timestamp - $_SERVER['REQUEST_TIME'])/60/60/24));

abr('params',$config['working_process']);
abr('revision_period', ceil($side_post[0]['days']/100*$config['working_process']['revision_period_procent']));
abr('escrow_days', $escrow_days);

//$_GET['print'] = $insert_into_db = 1;
if (isset($_GET['print'])) {
	if (isset($insert_into_db)) {
		$agreement_txt = $smarty->fetch('agreement.tpl');
		//$query = "INSERT INTO `order_agreements` SET `OID` = '9', `sign_date`='".sql_quote(strftime('%Y-%m-%d %H:%M:%S'))."', `agreement_text` = '".sql_quote(base64_encode(gzcompress($agreement_txt,8)),false)."'";
		//$executequery=$conn->execute($query);
		//d($query);
	//$order_id = $conn->_insertid();
	//d('ok',1);
	}
	else
		$smarty->display('agreement.tpl');
}
else {
	$smarty->display('header.tpl');
	$smarty->display('agreement.tpl');
	$smarty->display('footer.tpl');
}
//TEMPLATES END
?>