<?
require_once('config.local.php');

// Begin Configuration

if ($config['environment'] == 'production' && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'http' && !isset($config['widget'])) {
	if (!isset($_COOKIE['ssl'])) {
		$bad_agents = array('Win16','Windows 95','Win95','Windows_95','Windows 98','Win98','Windows NT 5.0','Windows 2000','Windows NT 5.1','Windows NT 5.2','Windows XP');
		$is_bad_agent = false;

		foreach ($bad_agents as $bad) {
			if (strpos($_SERVER['HTTP_USER_AGENT'], $bad)!==FALSE) {
				setcookie('ssl','1',25920000,'/','',false,true);
				$config['baseurl']     =  'http://ido.ge';
				$is_bad_agent = true;
				break;
			}
		}
	}
	else $is_bad_agent = true;

	if (!$is_bad_agent) {
		//exit ($_SERVER['HTTP_USER_AGENT']);
		header('HTTP/1.1 301 Moved Permanently'); 
		header('Location:'.$config['baseurl'].$_SERVER['REQUEST_URI']); exit;
	}
}

$default_language = 'georgian'; //Valid choices are english, spanish,  french, portuguese or hebrew
// End Configuration

session_name ( 'SESSID' );
session_start();
date_default_timezone_set('Asia/Tbilisi');

if (isset($_SESSION['USERID']) && $_SESSION['USERID']) {
	require_once($config['basedir'].'/libraries/cache.class.php');
	user_online_session_manager();
}

$config['debug_ips'] = array('127.0.0.1', 
	'::1',
	'178.134.194.127', // Home
	//'31.146.161.242', // Office
	);

$config['domain']			= 'ido.ge';
$config['marketingDomain']	= 'iDo.ge';
$config['currencySymbol']	= 'GEL';
$config['adminurl']			= $config['baseurl'].'/administrator';
$config['adminemail']		= 's.megrelishvili@gmail.com';
$config['adminNickname']	= 'iDo.ge';
$config['adminUserID']		= '52';
$config['cssurl']			= $config['baseurl'].'/css';
$config['imagedir']			= $config['basedir'].'/images';
$config['imageurl']			= $config['baseurl'].'/images';
$config['pdir']				= $config['basedir'].'/pics';
$config['purl']				= $config['baseurl'].'/pics';
$config['membersprofilepicdir']	= $config['imagedir'].'/membersprofilepic';
$config['membersprofilepicurl']	= $config['imageurl'].'/membersprofilepic';
$config['proxy_block'] = false;
$config['enable_fc'] = false;
$config['unique_salt'] = 'J_HuH59JR$^Bz';


$config['user_keepalive'] = 900; // Seconds of user session keepalive
$config['sms_period'] = 7200; // Seconds to send next SMS
$config['sms_from'] = '2051714';

$config['portfolio'] = array(
	'allowed_types' => array('image/png','image/jpg','image/jpeg','application/pdf'),
	'minDimensionsForFeaturing' => array('width'=>600,'height'=>264),
	'maxUploadSize' => 10, // Max Upload Size in MB
	'maxUploads' => 12); // Max Upload Size in MB

$config['not_allowed_mime_types'] = array('text/x-php','application/x-dosexec','application/octet-stream','text/x-shellscript','text/x-msdos-batch');

$config['maximum_results'] = 20;
$config['items_per_page'] = 10;
$config['min_wd'] = 15;
$config['moderation_queue_period'] = 2; // days

$config['status_matrix'] = array('0' => 'registered', '1' => 'ongoing', '2' => 'buyer_cancelled', '3' => 'seller_cancelled', '4' => 'delivered', '5' => 'completed', '6' => 'requires_revision', '7' => 'admin_cancelled');
$config['status_lang_matrix'] = array('registered' => 586, 'ongoing' => 586, 'requires_revision' => 586, 'buyer_cancelled' => 203, 'seller_cancelled' => 203, 'admin_cancelled' => 203, 'delivered' => 201, 'completed' => 202);

$config['purchase_settings'] = array('allow_purchasing_without_pin' => false, 'allow_purchasing_with_unverified_pin' => true);
$config['createdeal_settings'] = array('allow_creating_without_pin' => true, 'allow_creating_with_unverified_pin' => true);

$config['working_process']= array(
							'provide_instructions_days' => 2, // Must provide details on the work to do in X days, otherwise the task is canceled and buyer refunded and seller warned/de-rated
							'task_late_days_alert' => 1, // X days before the task deadline, warn the seller with a message
							'task_late_forget_timestamp' => 43200, // X seconds to forget the seller, if the deadline passed
							'task_late_grace_period_days' => 10, // Grace X days to delivery completed work
							'mutual_cancelation_period_days' => 5, // X days for automatical mutual cancellation of the task after the request of one side
							'review_period' => 5,
							'revision_period_procent' => 20,
							'max_reject_times' => 1,
							'payment_day_of_month' => 20,
							'unverifiedmember_add_block_period_days' => 90
							);

$config['score_matrix'] =	array(
							'registration_initial_score' => 300,
							'initial_task_provide_detail_fail' => -50,
							'initial_task_provide_detail_fail_CC' => true,
							'task_late_fail' => -50,
							'task_late_fail_CC' => true,
							'cancel_task_by_own' => -50,
							'cancel_task_by_own_CC' => true,
							'cancel_task_by_own_inform_admin' => -300,
							'success_task_owner' => 100,
							'success_task_buyer' => 100,
							'cancel_inform_admin' => -150,
							'cancel_inform_admin_CC' => true
							);

$config['delayed_emails'] = array('wd_mail_period' => 10800); // Send every 3 hours

$config['value_added'] = array('free_min_invites' => 5, 'free_valid_period' => 5356800); // 2 months valid free services

$config['enable_notify'] = true;

$config['contact_matrix'] = array(1 => 712, 2 => 713, 3 => 714, 10 => 715);

$config['site_slogan'] = 'იმუშავე შენი პროფესიით სახლიდან გაუსვლელად!';

$config['aws_credentials'] = array('key' => 'AKIAIEQVPO6WVJR56Q3Q', 'secret' => '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');

require_once($config['basedir'].'/smarty/libs/Smarty.class.php');
require_once($config['basedir'].'/libraries/mysmarty.class.php');
require_once($config['basedir'].'/libraries/SConfig.php');
require_once($config['basedir'].'/libraries/SError.php');
require_once($config['basedir'].'/libraries/adodb/adodb.inc.php');
require_once($config['basedir'].'/libraries/phpmailer/class.phpmailer.php');
require_once($config['basedir'].'/libraries/SEmail.php');


$smarty = new STemplate();

function strip_mq_gpc($arg)
{
  if (get_magic_quotes_gpc())
  {
	$arg = str_replace('"',"'",$arg);
	$arg = stripslashes($arg);
	return $arg;
  } 
  else
  {
	$arg = str_replace('"',"'",$arg);
	return $arg;
  }
}

$conn = ADONewConnection($DBTYPE);
$conn->PConnect($DBHOST, $DBUSER, $DBPASSWORD, $DBNAME);
//@mysqli_query("SET NAMES 'UTF8'");
mysqli_set_charset($conn->_connectionID, "utf8");
$sql = "SELECT * from config";
$rsc = $conn->Execute($sql);

if($rsc){while(!$rsc->EOF)
{
$field = $rsc->fields['setting'];
$config[$field] = $rsc->fields['value'];
abr($field, strip_mq_gpc($config[$field]));
@$rsc->MoveNext();
}}

if (!isset($_REQUEST['language'])) $_REQUEST['language'] = 'georgian';
else $_REQUEST['language'] = 'georgian';
/*
if ($_REQUEST['language'] != '')
{
	if ($_REQUEST['language'] == "georgian")
	{
		$_SESSION['language'] = "georgian";
	}
	if ($_REQUEST['language'] == "english")
	{
		$_SESSION['language'] = "english";
	}
	elseif ($_REQUEST['language'] == "spanish")
	{
		$_SESSION['language'] = "spanish";
	}
	elseif ($_REQUEST['language'] == "french")
	{
		$_SESSION['language'] = "french";
	}
	elseif ($_REQUEST['language'] == "portuguese")
	{
		$_SESSION['language'] = "portuguese";
	}
	elseif ($_REQUEST['language'] == "hebrew")
	{
		$_SESSION['language'] = "hebrew";
	}
}
*/
if ($_SESSION['language'] == "")
{
	$_SESSION['language'] = $default_language;
}

if ($_SESSION['language'] == "georgian")
{
include('lang/georgian.php');
}
elseif ($_SESSION['language'] == "english")
{
include("lang/english.php");
}
elseif ($_SESSION['language'] == "spanish")
{
include("lang/spanish.php");
}
elseif ($_SESSION['language'] == "french")
{
include("lang/french.php");
}
elseif ($_SESSION['language'] == "portuguese")
{
include("lang/portuguese.php");
}
elseif ($_SESSION['language'] == "hebrew")
{
include("lang/hebrew.php");
}
else
{
	include("lang/".$default_language.".php");
}

$langsize = sizeof($lang);
for ($i=0; $i<$langsize; $i++)
{
	$smarty->assign('lang'.$i, $lang[$i]);
}

abr('environment',	$config['environment']);
abr('baseurl',		$config['baseurl']);
abr('basedir',		$config['basedir']);
abr('adminurl',		$config['adminurl']);
abr('cssurl',		$config['cssurl']);
abr('imagedir',		$config['imagedir']);
abr('imageurl',		$config['imageurl']);
abr('pdir',			$config['pdir']);
abr('purl',			$config['purl']);
abr('membersprofilepicdir',        $config['membersprofilepicdir']);
abr('membersprofilepicurl',        $config['membersprofilepicurl']);
$smarty->setCompileDir($config['basedir'].'/temporary');
$smarty->setTplDir($config['basedir'].'/themes');

if(isset($sban) && $sban != '1')
{
	$bquery = "SELECT count(*) as total FROM `bans_ips` WHERE ip='".sql_quote($_SERVER['REMOTE_ADDR'])."'";
	$bresult = $conn->execute($bquery);
	$bcount = $bresult->fields['total'];
	if($bcount > "0")
	{
		$brdr = $config['baseurl']."/banned.php";
		header("Location:$brdr");
		exit;
	}
}

if($config['proxy_block'] == '1')
{
	if($_SERVER['HTTP_X_FORWARDED_FOR'] || $_SERVER['HTTP_X_FORWARDED'] || $_SERVER['HTTP_FORWARDED_FOR'] || $_SERVER['HTTP_VIA'] || in_array($_SERVER['REMOTE_PORT'], array(8080,80,6588,8000,3128,553,554)) || @fsockopen($_SERVER['REMOTE_ADDR'], 80, $errno, $errstr, 1))
	{
		exit('Proxy detected.');
	}
}

if (isset($_REQUEST['f'])) {
	require_once ('libraries/crypt.class.php');
	$crypt = new cryptClass();
	$f = $crypt->Decrypt(base64_decode(($_REQUEST['f'])));
	if (is_numeric($f) && !isset($_COOKIE['f'])) {
		setcookie('f', $_REQUEST['f'], $_SERVER['REQUEST_TIME']+2592000*3, '/');
	}
}

function create_slrememberme() {
		$key = md5(uniqid(rand(), true));
		global $conn;
		$sql="update members set remember_me_time='".date('Y-m-d H:i:s')."', remember_me_key='".$key."' WHERE username='".sql_quote($_SESSION[USERNAME])."'";
		$conn->execute($sql);
		setcookie('slrememberme', gzcompress(serialize(array($_SESSION[USERNAME], $key)), 9), time()+60*60*24*30);
}

function destroy_slrememberme($username) {
		if (strlen($username) > 0) {
				global $conn;
				$sql="UPDATE `members` SET `remember_me_time`=NULL, `remember_me_key`=NULL WHERE `username`='".sql_quote($username)."' LIMIT 1";
				$conn->execute($sql);
		}
		setcookie ('slrememberme', '', $_SERVER['REQUEST_TIME'] - 3600);
}

if (!isset($_SESSION["USERNAME"]) && isset($_COOKIE['slrememberme'])) 
{
		$sql="update members set remember_me_time=NULL and remember_me_key=NULL WHERE remember_me_time<'".date('Y-m-d H:i:s', mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))."'";
		$conn->execute($sql);
		list($username, $key) = @unserialize(gzuncompress(stripslashes($_COOKIE['slrememberme'])));
		if (strlen($username) > 0 && strlen($key) > 0)
		{
			$sql="SELECT status,USERID,email,username,verified from members WHERE username='".sql_quote($username)."' and remember_me_key='".sql_quote($key)."'";
			$rs=$conn->execute($sql);
			if($rs->recordcount()<1)
			{
				$error = "Error: Could not locate your account.";
			}
			elseif($rs->fields['status'] == "0")
			{
				$error = "Error: Your account has been disabled by the administrator.";
			}
			if($error=="")
			{				
				$_SESSION['USERID']=$rs->fields['USERID'];
				$_SESSION['EMAIL']=$rs->fields['email'];
				$_SESSION['USERNAME']=$rs->fields['username'];
				$_SESSION['VERIFIED']=$rs->fields['verified'];
				create_slrememberme();
			}
			else
			{
				destroy_slrememberme($username);
			}
		}
}

function generateCode($length) 
{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	$clen = strlen($chars) - 1;
	while (strlen($code) < $length) {
		$code .= $chars[mt_rand(0,$clen)];
	}
	return $code;
}
function getCurrentPageUrl()
{
	 static $pageURL = '';
	 if(empty($pageURL)){
		  $pageURL = 'http';
		  if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')$pageURL .= 's';
		  $pageURL .= '://';
		  if($_SERVER['SERVER_PORT'] != '80')$pageURL .= $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		  else $pageURL .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	 }
	 return $pageURL;
}
function download_photo($url, $saveto)
{
	global $config;
	if (!curlSaveToFile($url, $saveto))
	{
		if (!secondarysave($url, $saveto))
		{
			return false;
		}
		return true;
	}
	return true;
}
function download_photo_new($url, $saveto, $sourceurl)
{
	global $config;
	include("functions/curl.php");
	$curl = new Curl_HTTP_Client();
	$useragent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13";
	$curl->set_user_agent($useragent);
	$curl->set_referrer($url);
	$cookies_file = $config['basedir']."/temporary/cookies.txt";
	$curl->store_cookies($cookies_file);
	$html = $curl->fetch_url($url);
	if(file_exists($saveto))
	{
		unlink($saveto);
	}
	$fh = fopen($saveto, 'x');
	fwrite($fh, $html);
	fclose($fh);
	if(file_exists($saveto))
	{
		return true;
	}
	else
	{
		return false;
	}
}
function secondarysave( $url, $local )
{
	$ch = curl_init($url);
	$fp = fopen($local, 'wb');
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);

	if( filesize($local) > 10 ) {
		return true;
	}
	
	return false;
}

function curlSaveToFile( $url, $local )
{
	$ch = curl_init();
	$fh = fopen($local, 'w');
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_FILE, $fh);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_NOPROGRESS, true);
	curl_setopt($ch, CURLOPT_USERAGENT, '"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.11) Gecko/20071204 Ubuntu/7.10 (gutsy) Firefox/2.0.0.11');
	curl_exec($ch);
	if( curl_errno($ch) ) {
		return false;
	}
	
	curl_close($ch);
	fclose($fh);
	
	if( filesize($local) > 10 ) {
		return true;
	}
	
	return false;
}

function do_resize_image_2($file, $width = 0, $height = 0, $proportional = false, $output = 'file')
{
	if($height <= 0 && $width <= 0)
	{
	  return false;
	}
	
	$info = getimagesize($file);
	$image = '';

	$final_width = 0;
	$final_height = 0;
	list($width_old, $height_old) = $info;

	if($proportional) 
	{
	  if ($width == 0) $factor = $height/$height_old;
	  elseif ($height == 0) $factor = $width/$width_old;
	  else $factor = min ( $width / $width_old, $height / $height_old);   
	  
	  $final_width = round ($width_old * $factor);
	  $final_height = round ($height_old * $factor);
		  
	  if($final_width > $width_old && $final_height > $height_old)
	  {
		  $final_width = $width_old;
		  $final_height = $height_old;

	  }
	}
	else 
	{
	  $final_width = ( $width <= 0 ) ? $width_old : $width;
	  $final_height = ( $height <= 0 ) ? $height_old : $height;
	}
	
	switch($info[2]) 
	{
	  case IMAGETYPE_GIF:
		$image = imagecreatefromgif($file);
	  break;
	  case IMAGETYPE_JPEG:
		$image = imagecreatefromjpeg($file);
	  break;
	  case IMAGETYPE_PNG:
		$image = imagecreatefrompng($file);
	  break;
	  default:
		return false;
	}

	$image_resized = imagecreatetruecolor( $final_width, $final_height );

	if(($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG))
	{
	  $trnprt_indx = imagecolortransparent($image);
	
	  if($trnprt_indx >= 0)
	  {
		$trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
		$trnprt_indx    = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
		imagefill($image_resized, 0, 0, $trnprt_indx);
		imagecolortransparent($image_resized, $trnprt_indx);	
	  } 
	  elseif($info[2] == IMAGETYPE_PNG) 
	  {
		imagealphablending($image_resized, false);
		$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
		imagefill($image_resized, 0, 0, $color);
		imagesavealpha($image_resized, true);
	  }
	}
	imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);

	switch( strtolower($output))
	{
	  case 'browser':
		$mime = image_type_to_mime_type($info[2]);
		header("Content-type: $mime");
		$output = NULL;
	  break;
	  case 'file':
		$output = $file;
	  break;
	  case 'return':
		return $image_resized;
	  break;
	  default:
	  break;
	}
	
	if(file_exists($output))
	{
		@unlink($output);
	}

	switch($info[2])
	{
	  case IMAGETYPE_GIF:
		imagegif($image_resized, $output);
	  break;
	  case IMAGETYPE_JPEG:
		imagejpeg($image_resized, $output, 100);
	  break;
	  case IMAGETYPE_PNG:
		imagepng($image_resized, $output);
	  break;
	  default:
		return false;
	}
	return true;
}
if($config['enable_fc'] == 1)
{
	if($_SESSION['USERID'] == '')
	{
		$A = $config['FACEBOOK_APP_ID'];
		$B = $config['FACEBOOK_SECRET'];
		define('FACEBOOK_APP_ID', $A);
		define('FACEBOOK_SECRET', $B);
		abr('FACEBOOK_APP_ID',$A);
		abr('FACEBOOK_SECRET',$B);
		
		function get_facebook_cookie($app_id, $application_secret) {
		  $args = array();
		  parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
		  ksort($args);
		  $payload = '';
		  foreach ($args as $key => $value) {
			if ($key != 'sig') {
			  $payload .= $key . '=' . $value;
			}
		  }
		  if (md5($payload . $application_secret) != $args['sig']) {
			return null;
		  }
		  return $args;
		}
		
		$code = $_REQUEST['code'];
		if($code != "")
		{
			$my_url = $config['baseurl']."/";
			$token_url = "https://graph.facebook.com/oauth/access_token?"
			. "client_id=" . $A . "&redirect_uri=" . urlencode($my_url)
			. "&client_secret=" . $B . "&code=" . $code;
			$response = @file_get_contents($token_url);
			$params = null;
			parse_str($response, $params);
			$graph_url = "https://graph.facebook.com/me?access_token=" 
			. $params['access_token'];
			//d($graph_url,1);
			$user = json_decode(file_get_contents($graph_url));
			$is_Fan = file_get_contents('https://api.facebook.com/method/pages.isFan?format=json&access_token=' . $params['access_token'] . '&page_id=1431292867086735' );
			$_SESSION['FB_isFan'] = ($is_Fan=='true' ? 1 : 0);
			$friends = json_decode(file_get_contents('https://graph.facebook.com/me/friends?access_token='.$params['access_token']));
			$fbID = sql_quote($user->id);

			$fname = htmlentities(strip_tags($user->first_name), ENT_COMPAT, "UTF-8");
			$lname = htmlentities(strip_tags($user->last_name), ENT_COMPAT, "UTF-8");
			$femail = htmlentities(strip_tags($user->email), ENT_COMPAT, "UTF-8");
			$fusername = htmlentities(str_replace(' ','',strip_tags($user->username)), ENT_COMPAT, "UTF-8");
			list($flocation, $fcountry) = explode(', ',$user->location->name);
			//pics
			$fbpid = htmlentities(strip_tags($user->id), ENT_COMPAT, "UTF-8");
			$fbpicurl = "http://graph.facebook.com/".$fbpid."/picture";
			$fbpicurl2 = "http://graph.facebook.com/".$fbpid."/picture?type=large";

			//pics
			$query="SELECT `USERID`,`email`,`username`,`verified`,`status`,`country`,`location` FROM `members` WHERE `email`='".sql_quote($femail)."' OR `fbID`='".$fbID."' LIMIT 1";
			$result=$conn->execute($query);
			$FUID = intval($result->fields['USERID']);
			if($FUID > 0)
			{
				// UPDATE User Session on FB LOGIN
				$FSTATUS = intval($result->fields['status']);
				//$query="SELECT USERID,email,username,verified FROM members WHERE USERID='".sql_quote($FUID)."' and status='1'";
				//$result=$conn->execute($query);
				if ($FSTATUS == 1) {
					$sqladd = '';
					if (!$result->fields['country'] && $fcountry)
						if ($fcountry == 'Georgia')
							$sqladd .= ", country='GE'";
						else $sqladd .= ", country='".sql_quote($fcountry)."'";
					if (!$result->fields['location'] && $flocation)
						if (strtolower($flocation) == 'tbilisi')
							$sqladd .= ", location='".sql_quote($lang[630])."'";
						elseif (strtolower($flocation) == 'batumi')
							$sqladd .= ", locaiton='".sql_quote($lang[631])."'";
						elseif (strtolower($flocation) == 'kutaisi')
							$sqladd .= ", locaiton='".sql_quote($lang[632])."'";
						elseif (strtolower($flocation) == 'rustavi')
							$sqladd .= ", locaiton='".sql_quote($lang[633])."'";

					$query="UPDATE `members` SET `lastlogin`='".$_SERVER['REQUEST_TIME']."', `lip`='".$_SERVER['REMOTE_ADDR']."'".$sqladd.", `fbID`='".$fbID."', `FB_isFan`='".intval($_SESSION['FB_isFan'])."' WHERE `USERID`='".sql_quote($FUID)."' LIMIT 1";
					$conn->execute($query);
					$query="INSERT INTO `members_details` SET `USERID`='".$result->fields['USERID']."', `fb_profile`='".mysqli_real_escape_string($conn->_connectionID, serialize($user))."' ON DUPLICATE KEY UPDATE `fb_profile`='".mysqli_real_escape_string($conn->_connectionID, serialize($user))."'";
					$conn->execute($query);

//					Store FB Friends Array
					$friends_addquery = '';
					foreach ($friends->data as $f) {
						$friends_addquery .= $f->id.'['.$f->name.'],';
					}
					$conn->execute("INSERT INTO `members_friends` SET `USERID`='".$result->fields['USERID']."', `friendsFBIDs`='".sql_quote(rtrim($friends_addquery,','))."' ON DUPLICATE KEY UPDATE `friendsFBIDs`='".sql_quote(rtrim($friends_addquery,','))."'");
//					Store FB Friends Array End

					$_SESSION['USERID']=$result->fields['USERID'];
					$_SESSION['EMAIL']=$result->fields['email'];
					$_SESSION['USERNAME']=$result->fields['username'];
					$_SESSION['VERIFIED']=$result->fields['verified'];
					$_SESSION['FB']="1";

					if (isset($_SESSION['temp']['redirect']) && $_SESSION['temp']['redirect'] && isset($_SESSION['USERID']) && $_SESSION['USERID']) {
						$r = $_SESSION['temp']['redirect'];
						unset($_SESSION['temp']['redirect']);
						header('Location:'.$config['baseurl'].'/'.base64_decode($r)); exit;
					}			
					else {
						header("Location:$config[baseurl]/");exit;
					}
				}
			}
			else
			{
				// CREATE New User on FB Login
				$md5pass = md5(generateCode(5).$_SERVER['REQUEST_TIME']);
				
				if($fname != "" && $femail != "")
				{
					$def_country = $config['def_country'];
					if($def_country == "")
					{
						$def_country = "US";	
					}
					$sqladd = '';
					if ($fcountry)
						if ($fcountry == 'Georgia')
							$sqladd .= ", country='GE'";
						else $sqladd .= ", country='".sql_quote($fcountry)."'";
					else $sqladd .= ", country='".sql_quote($def_country)."'";

					if ($flocation)
						if (strtolower($flocation) == 'tbilisi')
							$sqladd .= ", `location`='".sql_quote($lang[630])."'";
						elseif (strtolower($flocation) == 'batumi')
							$sqladd .= ", `location`='".sql_quote($lang[631])."'";
						elseif (strtolower($flocation) == 'kutaisi')
							$sqladd .= ", `location`='".sql_quote($lang[632])."'";
						elseif (strtolower($flocation) == 'rustavi')
							$sqladd .= ", `location`='".sql_quote($lang[633])."'";
					if ($fname > '' && $lname > '') {
						$fusername = getUniqueUsername(array('first_name'=>$fname, 'last_name'=>$lname), ucfirst(geo2lat(strtolower($fname))).ucfirst(geo2lat(substr(strtolower($lname),0,3))));
					}
					else {
						if (stripos($fusername, 'shvili')!==FALSE)
							$fusername = substr($fusername,0,strlen($fusername)-8);
						elseif (stripos($fusername, 'dze')!==FALSE)
							$fusername = substr($fusername,0,strlen($fusername)-5);
						$fusername = getUniqueUsername(array('first_name'=>$fusername, 'last_name'=>''), $fusername);
					}

					if (isset($_COOKIE['f'])) {
						require_once ('libraries/crypt.class.php');
						$crypt = new cryptClass();
						$f = $crypt->Decrypt(base64_decode(($_COOKIE['f'])));
						if (is_numeric($f)) {
							$sqladd .= ", `ref_USERID`='".intval($f)."'";
						}
					}
					$query="INSERT INTO `members` SET `fbID`='".$fbID."', `email`='".sql_quote($femail)."',`fullname`='".transgeo($fname.' '.$lname)."', `username`='".sql_quote($fusername)."', `password`='".sql_quote($md5pass)."', `addtime`='".$_SERVER['REQUEST_TIME']."', `lastlogin`='".$_SERVER['REQUEST_TIME']."', `ip`='".$_SERVER['REMOTE_ADDR']."', `lip`='".$_SERVER['REMOTE_ADDR']."', `score`='".sql_quote($config['score_matrix']['registration_initial_score'])."', `verified`='1', `profilepicture_protect`='0', `FB_isFan`='".intval($_SESSION['FB_isFan'])."'".$sqladd;
					/*
					require_once('libraries/cache.class.php');
					$usersQuery = $cache->get('usersQuery');
					if (!$usersQuery) $usersQuery = array();
					$usersQuery[] = $query;
					$cache->set('usersQuery', $usersQuery, 86400);
					*/
					$result=$conn->execute($query);
					$userid = $conn->_insertid();

					if($userid != "" && is_numeric($userid) && $userid > 0)
					{
						// Store FB Profile
						$query="INSERT INTO `members_details` SET `USERID`='".intval($userid)."', `fb_profile`='".mysqli_real_escape_string($conn->_connectionID, serialize($user))."' ON DUPLICATE KEY UPDATE `fb_profile`='".mysqli_real_escape_string($conn->_connectionID, serialize($user))."'";
						$conn->execute($query);
						// Store FB Profile End

						// Store FB Friends Array
						$friends_addquery = '';
						foreach ($friends->data as $f) {
							$friends_addquery .= $f->id.'['.$f->name.'],';
						}

						$conn->execute("INSERT INTO `members_friends` SET `USERID`='".intval($userid)."', `friendsFBIDs`='".sql_quote(rtrim($friends_addquery,','))."' ON DUPLICATE KEY UPDATE `friendsFBIDs`='".sql_quote(rtrim($friends_addquery,','))."'");
						// Store FB Friends Array End

						$_SESSION['USERID']=$userid;
						$_SESSION['USERNAME'] = $fusername;
						$_SESSION['EMAIL']=$femail;
						$_SESSION['VERIFIED']=1;
						$_SESSION['FB']="1";		
						//add pics
						if(intval($fbpid) > 0)
						{
							$fp1 = $fbpicurl;
							$fp2 = $fbpicurl2;
							$tfpp = $userid.'-'.substr(md5($_SERVER['REQUEST_TIME']),-4).'.jpg';
							$fimage=$config['membersprofilepicdir']."/o/".$tfpp;
							if(!download_photo($fp2, $fimage))
							{
								if(file_exists($fimage))
								{
									unlink($fimage);
								}
							}
							else
							{
								$fi2=$config['membersprofilepicdir']."/".$tfpp;
								do_resize_image_2($fimage, "100", "100", false, $fi2, true);
								$fi3=$config['membersprofilepicdir']."/thumbs/".$tfpp;
								do_resize_image_2($fimage, "54", "54", false, $fi3, true);
								$fi4=$config['membersprofilepicdir']."/thumbs/A_".$tfpp;
								do_resize_image_2($fimage, "32", "32", false, $fi4, true);
								if(file_exists($config['membersprofilepicdir']."/o/".$tfpp))
								{
									$query = "UPDATE `members` SET `profilepicture`='$tfpp' WHERE `USERID`='".sql_quote($userid)."' LIMIT 1";
									$conn->execute($query);
								}
							}
						}
						if (isset($_SESSION['temp']['redirect']) && $_SESSION['temp']['redirect'] && isset($_SESSION['USERID']) && $_SESSION['USERID']) {
							$r = $_SESSION['temp']['redirect'];
							unset($_SESSION['temp']['redirect']);
							header('Location:'.$config['baseurl'].'/'.base64_decode($r)); exit;
						}			
						else {
							$_SESSION['temp']['message_title'] = $lang['629'];
							$_SESSION['temp']['message_type'] = 'success';
							header("Location:$config[baseurl]/settings"); exit;
						}
					}
				}
			}
		}
	}
	if($_SESSION['USERNAME'] == "" && $_SESSION['FB'] == "1")
	{	
		$url = getCurrentPageUrl();
		$myurl = $config['baseurl']."/settings";
//		$cssurl = $config['baseurl']."/css/style.php";
		$myurl2 = $config['baseurl']."/logout.php";
		if(($url != $myurl) && ($url != $cssurl) && ($url != $myurl2))
		{
			$_SESSION['temp']['message_title'] = $lang['629'];
			$_SESSION['temp']['message_type'] = 'success';
			header("Location:$config[baseurl]/settings"); exit;
		}
	}
}

/**
 * escape chars!
 *
 * @param string $value
 * @return string
 */
function strip_html_tags($text) {
	$text = preg_replace ( array (
		
		// Remove invisible content
		'@<head[^>]*?>.*?</head>@siu', 
		'@<style[^>]*?>.*?</style>@siu', 
		'@<script[^>]*?.*?</script>@siu', 
		'@<object[^>]*?.*?</object>@siu', 
		'@<embed[^>]*?.*?</embed>@siu', 
		'@<applet[^>]*?.*?</applet>@siu', 
		'@<noframes[^>]*?.*?</noframes>@siu', 
		'@<noscript[^>]*?.*?</noscript>@siu', 
		'@<noembed[^>]*?.*?</noembed>@siu', 
		// Add line breaks before and after blocks
		'@</?((address)|(blockquote)|(center)|(del)|(marquee)|(map))@iu', 
		'@</?((div)|(ins)|(isindex)|(pre))@iu', 
		'@</?((dir)|(dl)|(dt)|(dd)|(menu))@iu', 
		'@</?((form)|(button)|(fieldset)|(legend)|(input))@iu', 
		'@</?((label)|(select)|(optgroup)|(option)|(textarea))@iu', 
		'@</?((frameset)|(frame)|(iframe))@iu' 
	), array (
		
		' ', 
		' ', 
		' ', 
		' ', 
		' ', 
		' ', 
		' ', 
		' ', 
		' ', 
		"\n\$0", 
		"\n\$0", 
		"\n\$0", 
		"\n\$0", 
		"\n\$0", 
		"\n\$0", 
		"\n\$0", 
		"\n\$0" 
	), $text );
	//    return strip_tags( $text );
	return $text;
}

function sql_quote($value, $toStrip = true) {
	global $conn;
	$value = str_replace('<x>', '', $value);
	
	if ($toStrip) {
		$value = strip_html_tags ( $value );
	}
	if (get_magic_quotes_gpc ()) {
		$value = stripslashes ( $value );
	}
	//check if this function exists
	$value = addslashes ( $value );
	
	return mysqli_real_escape_string($conn->_connectionID, $value);
}

// Debug function
function d($arr, $x=false) {
	global $config;
	if (!in_array($_SERVER['REMOTE_ADDR'], $config['debug_ips']) && (!isset($_SESSION['d']) || $_SESSION['d']!='DevTeam-online')) {
		if (!isset($_SESSION['d']) || $_SESSION['d']!='DevTeam-online!') {
			if (!isset($_GET['debug']) || $_GET['debug']!='dev-Team')
				return;
			else {
				$_SESSION['d'] = 'DevTeam-online';
				echo '<pre>'; print_r($arr); echo '</pre>',PHP_EOL;
				if ($x) exit;
			}
		}
	}
	else {
		echo '<pre>'; 
		print_r($arr); 
		echo PHP_EOL;
		echo '</pre>';

		if ($x) exit;
	}

}
function transgeo($text) {
	$g = array('sh','ch','tch','ts','kh','ph','gh','dz','zh');
	$l = array('შ','ჩ','ჭ','ც','ხ','ფ','ღ','ძ','ჟ');
	$text = str_ireplace($g, $l, $text);
	$g = array('W','R','T','S','Z','C');
	$l = array('ჭ','ღ','თ','შ','ძ','ჩ');
	$text = str_replace($g, $l, $text);
	$g = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
	$l = array('ა','ბ','ც','დ','ე','ფ','გ','ჰ','ი','ჯ','კ','ლ','მ','ნ','ო','პ','ქ','რ','ს','ტ','უ','ვ','წ','ხ','ყ','ზ');
	return str_ireplace($g, $l, $text);
}

function geo2lat($text) {
	$g = array('a','b','ts','d','e','ph','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','ts','k','z','sh','ch','tch','kh','ph','gh','dz','zh','t');
	$l = array('ა','ბ','ც','დ','ე','ფ','გ','ჰ','ი','ჯ','კ','ლ','მ','ნ','ო','პ','ქ','რ','ს','ტ','უ','ვ','წ','ყ','ზ','შ','ჩ','ჭ','ხ','ფ','ღ','ძ','ჟ','თ');
	return str_ireplace($l, $g, $text);
}


function getUniqueUsername($user_array, $username) {
	global $conn;
	$username = str_replace(' ', '', $username);
	$user_array['first_name'] = str_replace(' ', '', $user_array['first_name']);
	$user_array['last_name'] = str_replace(' ', '', $user_array['last_name']);
	$q2 = "SELECT `USERID` FROM `members` WHERE `username` = '".sql_quote($username)."' LIMIT 1";
	$r2 = $conn->execute($q2);
	if ($r2->_numOfRows) {
		$username = ucfirst(strtolower(geo2lat($user_array['first_name']))).ucfirst(substr(strtolower(geo2lat($user_array['last_name'])),0,3)).rand(100,999);
		return getUniqueUsername($user_array, $username);
	}
	else return $username;
}

function user_online_session_manager() {
	global $cache;
	$cache->set('u.la.'.$_SESSION['USERID'], $_SERVER['REQUEST_TIME'], 900);
}
?>