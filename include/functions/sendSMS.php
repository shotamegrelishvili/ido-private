<?php
// Library to send SMS

if (!isset($_GET['token']) || !$_GET['token']) die('access denied 1');
require_once('../../include/config.php');
if (md5($_GET['to'].$config['tokenHash'].md5($_GET['body'])) != $_GET['token']) die('access denied 2');
require_once('../../libraries/cache.class.php');
$smsCache = $cache->get('smsCache.'.$_GET['to']);
if (strlen($_GET['to']) == 9) $to = '995'.intval($_GET['to']);
else $to = intval($_GET['to']);
$res = file_get_contents('https://secure.myphone.ge/sms/ido.ge.php?accountid='.$config['myphoneID'].'&username='.$config['emoney_app_key'].'&password='.$config['myphonePassword'].'&o_number='.$to.'&o_text='.urlencode($_GET['body']));
$smsCache[] = array('t'=>strftime('%Y-%m-%d %H:%M'),'b'=>$_GET['body'],'r'=>$res);
$cache->set('smsCache.'.$_GET['to'],$smsCache,86400*7);
?>