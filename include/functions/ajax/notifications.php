<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);

if (!isset($_SESSION['USERID']) || !$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) die('access denied');

$notifications = $cache->get('u.N.'.$_SESSION['USERID']);
$cache->set('u.Ncc.'.$_SESSION['USERID'], 0, 2592000);
//d($notifications);
abr('notifications', $notifications);
//TEMPLATES BEGIN
$smarty->display('ajax_notifications.tpl');
//TEMPLATES END
?>