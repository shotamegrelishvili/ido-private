<?php
include('../../config.php');

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!$config['enable_notify'] || !$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) die('');
else {
	$USERID = intval($_SESSION['USERID']);
}

$nCC = $cache->get('u.Ncc.'.$_SESSION['USERID']);
if ($nCC === false || $nCC === null) $nCC = 0;

echo json_encode(array('status'=>'OK', 'nCC'=>$nCC));
?>