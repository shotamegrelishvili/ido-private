<?php
include('../../config.php');
include('../import.php');

header ('Access-Control-Allow-Origin: '.$config['baseurl']);
//print_r($_REQUEST);
if (stripos($_SERVER['HTTP_REFERER'], $config['baseurl']) !== 0) {
	die('access denied');
	//die('access denied 1: '.$_SERVER['HTTP_ORIGIN'].' != '.$config['baseurl']);
}
elseif (!isset($_POST['accessToken']) || !$_POST['accessToken'] || !preg_match('/[0-9A-Z]{30,32}/', $_POST['accessToken'])) {
	die('access denied');
	//die('access denied 2: '.$_POST['accessToken'].' strlen: '.strlen($_POST['accessToken']));
}
else {
	include($config['basedir'].'/libraries/eConnect.class.php');
	$econnectClass = new econnect();
	
	$econnected_user = $econnectClass->getUserInfo($_POST['accessToken']);
	//print_r($econnected_user);
	if ($econnected_user['email']) {
			$query="SELECT `USERID`, `username`, `eemail`, `eemailprotect`, `pin`, `pinprotect`, `mobile`, `profilepicture`, `status`, `FB_isFan` FROM `members` WHERE `eemail`='".sql_quote($econnected_user['email'])."' OR `email`='".sql_quote($econnected_user['email'])."' LIMIT 1";
			$result=$conn->execute($query);

			if (!$result->_numOfRows) {
				// Update User Records to include eMoney Data
				if (isset($_SESSION['USERID']) && $_SESSION['USERID']) {
					if (!$pin && $econnected_user['pin'])
						$addSql = ',`pin`="'.sql_quote($econnected_user['pin']).'", `pinprotect`="1"';
					else $addSql = '';
					$conn->execute('UPDATE `members` SET `eemail`="'.sql_quote($econnected_user['email']).'", `eemailprotect`="1"'.$addSql.' WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1');
				}
				// End Update User Records

				$username = getUniqueUsername($econnected_user, geo2lat(ucfirst(strtolower($econnected_user['first_name'])).ucfirst(mb_substr(strtolower($econnected_user['last_name']),0,3,'UTF-8'))));
				$econnected_user['USERID'] = registerNewUser($econnected_user, $username);
				signinUser($econnected_user, $username);
				$_SESSION['temp']['message_title'] = $lang['629'];
				$_SESSION['temp']['message_type'] = 'success';
				$_SESSION['signin_gateway'] = 'econnect';
				$_SESSION['session_expiry'] = $_SERVER['REQUEST_TIME'] + 3000;
			}
			else {
				if ($result->fields['status'] == '0') {
					$no_redir = true;
					include($config['basedir'].'/logout.php');
					$_SESSION['temp']['message_type'] = 'error';
					$_SESSION['temp']['message_title'] = $lang[745];
					exit(json_encode(array('status'=>'ERROR','MSG'=>$lang[745])));
				}
				
				// Update User Records to include eMoney Data
				if (isset($_SESSION['USERID']) && $_SESSION['USERID'] && $_SESSION['USERID'] != $result->fields['USERID'])
					exit(json_encode(array('status'=>'ERROR','MSG'=>$lang['561'])));
				// End Update User Records

				$r=$econnectClass->_UpdateUser($econnected_user, $result->fields['USERID']);
				if ($r) {
					$econnected_user['USERID'] = $result->fields['USERID'];
					$econnected_user['FB_isFan'] = $result->fields['FB_isFan'];
				}
				signinUser($econnected_user, $result->fields['username']);
				$_SESSION['signin_gateway'] = 'econnect';
				$_SESSION['access_token'] = $_POST['accessToken'];
				$_SESSION['session_expiry'] = $_SERVER['REQUEST_TIME'] + 3000;
			}

			$redirectURI = '';
			if ($_POST['process'] == 'signin') {
				if ($_POST['requestUrl'] && strpos($_POST['requestUrl'], 'r=')!==false)
					$r = explode('r=', $_POST['requestUrl']);
				if (@$r[1]) {
					$redirectURI = $config['baseurl'].'/'.base64_decode(cleanit(stripslashes($r[1])));
				}
				else {
					$redirectURI = $config['baseurl'];
				}
			}
			
		}

	exit(json_encode(array('status'=>'OK','redirectURI'=>$redirectURI)));
}
?>