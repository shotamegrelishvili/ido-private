<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!$_SESSION['ADMINID']) die('access denied');
elseif (!isset($_POST['UID']) || !is_numeric($_POST['UID']) || !isset($_POST['DAYS']) || !is_numeric($_POST['DAYS']) || !isset($_POST['OID']) || !is_numeric($_POST['OID'])) die('access denied');

if ($_POST['DAYS'] > 0) $days = $_POST['DAYS']+1; else $days = 0;
$block_till_date = strftime('%Y-%m-%d',strtotime('now +'.$days.' days'));
$query = "UPDATE `members` SET `blocked_till_date`='".sql_quote($block_till_date)."' WHERE `USERID` = '".intval($_POST['UID'])."' LIMIT 1";
//d($query);
$executequery=$conn->execute($query);

$query = "INSERT INTO `block_history` SET `USERID` = '".intval($_POST['UID'])."', `blocked_datetime`=NOW(), `days`='".intval($_POST['DAYS'])."', `ADMINID`='".intval($_SESSION['ADMINID'])."', `OID`='".intval($_POST['OID'])."'";
//d($query);
$executequery=$conn->execute($query);

$query = "SELECT `username`, `email` FROM `members` WHERE `USERID`='".intval($_POST['UID'])."' LIMIT 1";
$user=$conn->execute($query);

$sendmailbody =  sprintf($lang['663'], $user->fields['username'], $block_till_date);
$sendmailbody .= '<br /><br />'.$lang['704'].'<br />'.stripslashes($config['site_name']).sprintf($lang['664'], $config['site_email']);
mailme($user->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody);

echo json_encode(array('status'=>'OK', 'who'=>$_POST['who']));
?>