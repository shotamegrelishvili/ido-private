<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: GET');

if (!$_SESSION['ADMINID']) die('access denied');
elseif (!isset($_GET['UID']) || !is_numeric($_GET['UID']) || !isset($_GET['scoredelta']) || !is_numeric($_GET['scoredelta']) || !isset($_GET['OID']) || !is_numeric($_GET['OID'])) die('access denied');

if ($_GET['scoredelta'] > 0) $scoresign = '+'; 
else {
	$scoresign = '';
}

$query = "UPDATE `members` SET `score`=`score`".$scoresign.intval($_GET['scoredelta'])." WHERE `USERID` = '".intval($_GET['UID'])."' LIMIT 1";
$executequery=$conn->execute($query);

$query = "INSERT INTO `scores` SET `USERID` = '".intval($_GET['UID'])."', `action`='admin_decision', `score_delta`='".intval($_GET['scoredelta'])."', `OID`='".intval($_GET['OID'])."'";
$executequery=$conn->execute($query);


echo json_encode(array('status'=>'OK', 'who'=>$_GET['who']));
?>