<?php
include('../../config.php');
//include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) {
	$_SESSION['FB_isFan'] = 1;
	setcookie('FB_isFan', $_SERVER['REQUEST_TIME'] + 2592000, $_SERVER['REQUEST_TIME'] + 2592000, '/');
	exit('ok');
}
else {
	$USERID = intval($_SESSION['USERID']);
	$conn->execute('UPDATE `members` SET `FB_isFan`="1" WHERE `USERID`="'.$USERID.'" LIMIT 1');
	$_SESSION['FB_isFan'] = 1;
	exit('ok');
}
?>