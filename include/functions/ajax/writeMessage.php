<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!$_SESSION['ADMINID']) die('access denied');
elseif (!isset($_POST['UID']) || !is_numeric($_POST['UID']) || !isset($_POST['message']) || !isset($_POST['OID']) || !is_numeric($_POST['OID']) || !isset($_POST['who'])) die('access denied');

if (!isset($_SESSION['temp']['adminmessage_md5']) || $_SESSION['temp']['adminmessage_md5'] != md5($_POST['UID'].$_POST['message'])) {
	$_SESSION['temp']['adminmessage_md5'] = md5($_POST['UID'].$_POST['message']);
}
else {
	exit (json_encode(array('status'=>'ERROR', 'MSG'=>'message_already_sent', 'who'=>$_POST['who'])));
}

if ($_POST['OID']) {
	$query = "INSERT INTO `inbox2` SET `MSGTO`='".intval($_POST['UID'])."', `MSGFROM`='1', `message`='".sql_quote($_POST['message'])."', `OID`='".intval($_POST['OID'])."', `time`='".$_SERVER['REQUEST_TIME']."', `action`='admin_message', `ADMINID`='".intval($_SESSION['ADMINID'])."', `ip`='".sql_quote($_SERVER['REMOTE_ADDR'])."'";
	$executequery=$conn->execute($query);
}

$query = "SELECT `username`, `email` FROM `members` WHERE `USERID`='".intval($_POST['UID'])."' LIMIT 1";
$user=$conn->execute($query);

if ($_POST['OID']) {
	$sendmailbody =  sprintf($lang['660'], $user->fields['username'], $config['baseurl'].'/track?id='.$_POST['OID'], $config['baseurl'].'/track?id='.$_POST['OID']);
	$sendmailbody .= '<br /><br />'.$lang['704'].'<br />'.stripslashes($config['site_name']).$lang['661'];
}
else
	$sendmailbody =  $_POST['message'];

mailme($user->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody);

echo json_encode(array('status'=>'OK', 'who'=>$_POST['who']));
?>