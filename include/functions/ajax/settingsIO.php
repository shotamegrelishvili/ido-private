<?php
include("../../config.php");
include("../import.php");

$config['allow_update'] = array(
	'fname' => 'fullname',
	'pin' => 'pin',
	'location' => 'location',
	'email' => 'email',
	'mobile' => 'mobile',
	'uploadFile' => 'profilepicture',
	'exp_arr' => 'exp',
	'details' => 'description',
	);

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (stripos($_SERVER['HTTP_REFERER'], $config['baseurl']) !== 0 
	|| !isset($_SESSION['USERID']) || !$_SESSION['USERID']
	|| !isset($_SESSION['temp']['ajax_allow']) || !isset($_POST['action']) || !$_POST['action'] || !isset($_POST['data']) || !$_POST['data']) {
	exit(outputJSON('access denied'));
}
elseif ($_SESSION['temp']['ajax_allow'] < $_SERVER['REQUEST_TIME']) {
	exit(outputJSON($lang['832'], 'error'));
}

$row = json_decode($_POST['data']);

if ($_POST['action'] == 'save') {


require ($config['basedir'].'/models/settings.Class.php');
$settings = new settingsClass();

foreach ($row as $field=>$r) {
	if (isset($config['allow_update'][$field])) {
		if ($settings->checkValidity($config['allow_update'][$field], $_SESSION['USERID'], $r)) {
			//d($settings->record);
			if ($settings->record['field'] && $settings->record['value']) {
				//d('UPDATE `members` SET `'.sql_quote($settings->record['field']).'`="'.sql_quote($settings->record['value']).'" WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1');
				$conn->execute('UPDATE `members` SET `'.sql_quote($settings->record['field']).'`="'.sql_quote($settings->record['value']).'" WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1');
			}
		}
		else {
			//d($settings->error);
			outputJSON($settings->error, 'error');
		}
	}
}

}
elseif ($_POST['action'] == 'remove') {
	//$conn->execute( 'DELETE FROM `portfolio` WHERE `USERID`="'.intval($_SESSION['USERID']).'" AND `FID`="'.intval($data[0]).'" LIMIT 1' );
}

outputJSON($lang[828], 'success');

function outputJSON($msg, $status = 'error'){
	header('Content-Type: application/json');
	die(json_encode(array(
		'MSG' => $msg,
		'status' => $status
	)));
}
?>