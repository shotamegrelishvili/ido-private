<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: GET');

if (!$_SESSION['ADMINID']) die('access denied');
elseif (!isset($_GET['MID']) || !is_numeric($_GET['MID'])) die('access denied');

$query = "UPDATE `contact` SET `is_del`='1', `answer_datetime`=NOW() WHERE `id` = '".intval($_GET['MID'])."' LIMIT 1";
$executequery=$conn->execute($query);

echo json_encode(array('status'=>'OK','ID'=>intval($_GET['MID'])));
?>