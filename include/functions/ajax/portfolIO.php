<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (stripos($_SERVER['HTTP_REFERER'], $config['baseurl']) !== 0 
	|| !isset($_SESSION['USERID']) || !$_SESSION['USERID']
	|| !isset($_SESSION['temp']['ajax_allow']) || !isset($_POST['action']) || !$_POST['action'] || !isset($_POST['data']) || !$_POST['data']) {
	exit(outputJSON('access denied'));
}
elseif ($_SESSION['temp']['ajax_allow'] < $_SERVER['REQUEST_TIME']) {
	exit(outputJSON($lang['832'], 'error'));
}

$data = json_decode($_POST['data']);

if (!is_array($data))
	exit(outputJSON('access denied'));

if ($_POST['action'] == 'sort') {
	$s = sizeof($data);

	$q = '';
	for($i=0;$i<$s;$i++) {
		$conn->execute( 'UPDATE `portfolio` SET `order`="'.($i+1).'" WHERE `USERID`="'.intval($_SESSION['USERID']).'" AND `FID`="'.intval($data[$i]).'" LIMIT 1' );
	}
}
elseif ($_POST['action'] == 'remove' && is_numeric($data[0])) {
	$conn->execute( 'DELETE FROM `portfolio` WHERE `USERID`="'.intval($_SESSION['USERID']).'" AND `FID`="'.intval($data[0]).'" LIMIT 1' );
}

outputJSON($lang[828], 'success');

function outputJSON($msg, $status = 'error'){
	header('Content-Type: application/json');
	die(json_encode(array(
		'MSG' => $msg,
		'status' => $status
	)));
}
?>