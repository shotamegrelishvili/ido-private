<?php
if (!defined('IN_SEARCH')) {
	include_once('../../config.php');
	include_once('../import.php');

	header('Access-Control-Allow-Origin: '.$config['baseurl']);
	header('Access-Control-Allow-Methods: POST');

	if (stripos($_SERVER['HTTP_REFERER'], $config['baseurl']) !== 0 
	//	|| !isset($_SESSION['USERID']) || !$_SESSION['USERID']
		|| !isset($_SESSION['temp']['ajax_allow']) || !isset($_POST['action']) || !$_POST['action'] || !isset($_POST['data']) || !$_POST['data']) {
		exit(outputJSON('access denied'));
	}
	elseif ($_SESSION['temp']['ajax_allow'] < $_SERVER['REQUEST_TIME']) {
		exit(outputJSON($lang['832'], 'error'));
	}

	$data = trim($_POST['data']);
}

/* General Configuration Params */
$configExp = array(
	'keywordMinLength' => 2,
	'seekAliasesMinResult' => 14,
	'minTryoutsBeforeContact' => 3,
	'enableCachedAnswer' => 1,
	);

if (!$data)
	exit(outputJSON('access denied'));
elseif ($configExp['enableCachedAnswer']) {
	$cachedAnswer = $cache->get('exp.res.'.md5($data));
	if ($cachedAnswer) {
		outputJSON($cachedAnswer,'success');
	}
	else {
		searchRun();
	}
}

function searchRun() {
	global $config, $configExp, $conn, $cache, $lang, $data;
	/* Get keywords and aliases */
	if ($_POST['action'] == 'getClosestValues') {
		if (!($mainKeywords = $cache->get('exp.mainKeywords')) || isset($_GET['no-cache'])) {
			$r = $conn->execute( 'SELECT * FROM `exp_keys`' );
			$keys = $r->getrows();
			foreach ($keys as $k) {
				$mainKeywords[$k['KID']] = array(
					'KID' 		=> $k['KID'],
					'keyword'	=> $k['keyword'],
					'CATID'		=> $k['CATID'],
					'aliasof'	=> $k['aliasof']
					);
			}
			//d($mainKeywords);
			$cache->set('exp.mainKeywords', $mainKeywords, 2592000);
		}
		if (!($aliases = $cache->get('exp.aliases')) || isset($_GET['no-cache'])) {
			$r = $conn->execute( 'SELECT *, (SELECT GROUP_CONCAT(`KID` SEPARATOR "|") FROM `exp_alias2keys` WHERE `ALIASID` = `exp_aliases`.`ALIASID`) AS `alias2keys` FROM `exp_aliases`' );
			$keys = $r->getrows();
			foreach ($keys as $k) {
				$aliases[$k['ALIASID']] = array(
					'ALIASID'		=> $k['ALIASID'],
					'alias'			=> $k['alias'],
					'show'			=> $k['show'],
					'alias2keys'	=> $k['alias2keys']
					);
			}
			//d($aliasKeywords);
			$cache->set('exp.aliases', $aliases, 2592000);
		}
	}

	$searchKeys = keywordsNormalize( prepareKeywords($data) );

	$answer = $relevance = array();
	$foundAnswers = 0;
	foreach ($searchKeys as $searchTerm) {
		$searchTermLen = mb_strlen($searchTerm);
	//	d('SearchTerm: '.$searchTerm);
		if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
		// Exact Seek results in MainKeywords
		foreach ($mainKeywords as $m=>$mainKey) {
			if (($f = mb_stripos($mainKey['keyword'], $searchTerm))!==false) {
				// Looks if there is a space before start of the word
				if ($f > 2 && mb_substr($mainKey['keyword'],$f-1,1,'UTF-8') != ' ' && mb_substr($mainKey['keyword'],$f-2,1,'UTF-8') != ' ') {
					continue;
				}
				$mainKeywords[$m]['used'] = 1;
				
				if (!isset($relevance[$mainKey['keyword']])) 
					$relevance[$mainKey['keyword']] = 10;
				else
					$relevance[$mainKey['keyword']] += 10;
				$answer[$mainKey['keyword']] = $mainKey;
				//d('eXact Key: '.$mainKey['keyword'].' = 10');
				//$foundAnswers++;
				if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
			}
		}
		// Exact Seek results in Aliases
		if ($foundAnswers < $configExp['seekAliasesMinResult']) {
			foreach ($aliases as $a=>$alias) {
				if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
				elseif ($alias['used']) continue;
				//d($alias['alias']. ' = '.$searchTerm.' strlen='.$searchTermLen.' : '.$al.' **** '.mb_strpos($alias['alias'], $searchTerm));
				if (($f = mb_stripos($alias['alias'], $searchTerm))!==false || mb_stripos($searchTerm, $alias['alias'])!==false) {
					//d($alias['alias'].' = '.$searchTerm);
					if ($f > 2 && mb_substr($alias['alias'],$f-1,1,'UTF-8') != ' ' && mb_substr($alias['alias'],$f-2,1,'UTF-8') != ' ') {
						continue;
					}
					
					$aliasParents = explode('|',$alias['alias2keys']);
					foreach ($aliasParents as $aliasParent) {
						//d($alias['alias'].' = '.$searchTerm);
						//d($answer[$mainKeywords[$aliasParent]['keyword']]);
						if (!isset($relevance[$mainKeywords[$aliasParent]['keyword']]))
							$relevance[$mainKeywords[$aliasParent]['keyword']] = 6;
						else
							$relevance[$mainKeywords[$aliasParent]['keyword']] += 6;
						// Show the parent of alias first as a more general result
						if (!isset($answer[$mainKeywords[$aliasParent]['keyword']])) {
							//if ($alias['show'])	$mainKeywords[$aliasParent]['hasAliases'] = 1;
							$answer[$mainKeywords[$aliasParent]['keyword']] = $mainKeywords[$aliasParent];
							//d($relevance);
							//d('eXact: '.$alias['alias'].' M: '.$mainKeywords[$aliasParent]['keyword'].' = 6');
							$foundAnswers++;
							if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
						}
						$aliases[$a]['used'] = 1;
						// If alias has permission to be shown, show it in results
						if ($alias['show']) {
							$answer[$mainKeywords[$aliasParent]['keyword']]['aliases'][] = $alias;
							$foundAnswers++;
						}
					}
				}
			}
		}

		// If there is still room to show search results, go to not-exact equation of Main keywords
		if ($foundAnswers < $configExp['seekAliasesMinResult'] && $searchTermLen > 2) {
			foreach ($mainKeywords as $mainKey) {
				if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
				elseif (isset($mainKey['used'])) continue;
				$allMainKeys = explode(' ',$mainKey['keyword']);
				foreach ($allMainKeys as $key) {
					if (mb_strlen($key) < $searchTermLen) continue;
					else similar_text($searchTerm,mb_substr($key,0,$searchTermLen),$perc);
					// If the word is 85% similar, or the levenshtein distance is less than 3, then we have a match
					if ($perc > 85 || ($perc > 78 && levenshtein($searchTerm, mb_substr($key,0,$searchTermLen), 1 , 1, 1) < 3)) {
						//d('A1: '.$key.' P: '.$perc.' L: '.levenshtein($searchTerm, mb_substr($key,0,$searchTermLen), 1 , 1, 1));
						$reladd = 10 - floor((100-$perc)/2);
						if ($reladd < 0) $reladd = 1;
						//d('RADD: '.$reladd);
						if (!isset($relevance[$mainKey['keyword']]))
							$relevance[$mainKey['keyword']] = $reladd;
						else
							$relevance[$mainKey['keyword']] += $reladd;
						//d($relevance,1);
						$answer[$mainKey['keyword']] = $mainKey;
						$foundAnswers++;
						break;
					}
				}
			}
		}
		//d($answer);

		// If there is still room to show search results, go to not-exact equation of Aliases
		if ($foundAnswers < $configExp['seekAliasesMinResult'] && $searchTermLen > 2) {
			foreach ($aliases as $alias) {
				if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
				elseif (isset($alias['used'])) continue;
				$allAliasKeys = explode(' ',$alias['alias']);
				foreach ($allAliasKeys as $key) {
					if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
					elseif (mb_strlen($key) < $searchTermLen) continue;
					else similar_text($searchTerm,mb_substr($key,0,$searchTermLen),$perc);
					if ($perc > 85 || ($perc > 78 && levenshtein($searchTerm, mb_substr($key,0,$searchTermLen), 1 , 1, 1) < 3)) {
						// d('A2: '.$key.' P: '.$perc.' M: '.$mainKeywords[$aliasParent]['keyword'].' L: '.levenshtein($searchTerm, mb_substr($key,0,$searchTermLen), 1 , 1, 1));
						$aliasParents = explode('|',$alias['alias2keys']);
						foreach ($aliasParents as $aliasParent) {
							// Show the parent of alias first as a more general result
							$reladd = 5 - floor((100-$perc)/2);
							if ($reladd < 0) $reladd = 1;
							//d('RADD for '.$searchTerm.' = '.$reladd);
							if (!isset($relevance[$mainKeywords[$aliasParent]['keyword']]))
								$relevance[$mainKeywords[$aliasParent]['keyword']] = $reladd;
							else
								$relevance[$mainKeywords[$aliasParent]['keyword']] += $reladd;
							//d($relevance,1);
							if (!isset($answer[$mainKeywords[$aliasParent]['keyword']])) {
								$answer[$mainKeywords[$aliasParent]['keyword']] = $mainKeywords[$aliasParent];
								$foundAnswers++;
								if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
							}
							// If alias has permission to be shown, show it in results
							// Check if main keyword already presented, skip adding alias to results
							if ($alias['show'] && !isset($answer[$mainKeywords[$aliasParent]['keyword']])) {
								$answer[$mainKeywords[$aliasParent]['keyword']]['aliases'][] = $alias;
								$foundAnswers++;
								if ($foundAnswers >= $configExp['seekAliasesMinResult']) break;
							}
						}
					}
				}
			}
		}
		
	}

	//$answer['results'] = $foundAnswers;
	//d($answer);

	arsort($relevance);
	// d($relevance);
	$maxRelevance = 0;
	foreach ($relevance as $k=>$r) {
		if (!$maxRelevance) $maxRelevance = $r;
		elseif ($maxRelevance >= 10 && $r < $maxRelevance/2.5) break;
		$sortedArray[$k] = $answer[$k];
	}
	//d($sortedArray);
	//d($relevance);
	if (sizeof($sortedArray)>0) {
	//	d($answer);
		$cache->set('exp.res.'.md5($data), $sortedArray, 7200);
		outputJSON($sortedArray, 'success');
	}
	elseif (mb_strlen($data)>5) {
		// Send 1 email on the keyword not found 
		// + Store keyword in the cache for later review
		if (!isset($_SESSION['temp']['exp_tryout']))
			$_SESSION['temp']['exp_tryout']=1;
		else $_SESSION['temp']['exp_tryout']++;
		if ($_SESSION['temp']['exp_tryout'] > $configExp['minTryoutsBeforeContact']) {
			$exp2Review = $cache->get('exp2Review');
			if (!$exp2Review || $exp2Review['last_update'] < $_SERVER['REQUEST_TIME'] - 86400) {
				include_once($config['basedir'].'/libraries/emailsms.class.php');
				$emailClass = new email();
				$emailClass->send('s.megrelishvili@gmail.com', 'Skill/Knowledge Add Request', 'Xom ar davamatot:'.PHP_EOL.$data.PHP_EOL.'USER: <a href="'.$config['baseurl'].'/user/'.$_SESSION['USERNAME'].'">'.$_SESSION['USERNAME'].' [id='.$_SESSION['USERID'].']</a>', true, false);
			}
			if (isset($exp2Review['keywords']))
				$keywords = $exp2Review['keywords'];
			else
				$keywords = array();
			$keywords[$data] = array(
						'USERID'	=> $_SESSION['USERID'],
						'USERNAME'	=> $_SESSION['USERNAME']
						);
			$exp2Review = array(
				'last_update'	=> $_SERVER['REQUEST_TIME'],
				'keywords'		=> $keywords
				);
			$cache->set('exp2Review',$exp2Review,864000);
			outputJSON($lang[833], 'halt');
		}
	}
}

// Changes multiple form to single form for every keyword
function keywordsNormalize($keywords = array()) {
	global $configExp;
	foreach ($keywords as $k=>$keyword) {
		if ($keyword AND ($l=mb_strlen($keyword, 'UTF-8')) > 4 AND ($f=mb_strpos($keyword, 'ების')) > 0 AND $f == $l-4) {
			if ($l-4 >= $configExp['keywordMinLength'])
				$keywords[$k] = mb_substr($keyword,0,$l-4,'UTF-8');
			else
				unset($keywords[$k]);
		}
		elseif ($keyword AND ($l=mb_strlen($keyword, 'UTF-8')) > 3 AND ($f=mb_strpos($keyword, 'ები')) > 0 AND $f == $l-3) {
			if ($l-3 >= $configExp['keywordMinLength'])
				$keywords[$k] = mb_substr($keyword,0,$l-3,'UTF-8');
			else
				unset($keywords[$k]);
		}
		elseif ($l > 3 AND ((($ll=mb_substr($keyword,$l-1,1,'UTF-8'))=='ს' OR $ll=='ი'))) {
			$keywords[$k] = mb_substr($keyword, 0, $l-1, 'UTF-8');
		}
		elseif (!$keyword || $l < $configExp['keywordMinLength']) {
			unset($keywords[$k]);
		}
	}
	return $keywords;
}

// Cleans the input
function prepareKeywords($data = '') {
	return (explode(' ', preg_replace('/[ ,.;_:]+/', ' ', $data)));
}

function outputJSON($msg, $status = 'error'){
	if (defined('IN_SEARCH')) {
		$firstElement = array_shift($msg);
		$_REQUEST['catid'] = $firstElement['CATID'];
	}
	else {
		header('Content-Type: application/json');
		die(json_encode(array(
			'MSG' => $msg,
			'status' => $status
		)));
	}
}
?>