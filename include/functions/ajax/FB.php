<?php
include('../../config.php');
//include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!isset($_POST['action']) || !$_POST['action'])
	exit('access denied');

if ($_POST['action'] == 'fan') {
	if (!$_SESSION['USERID'] || !is_numeric($_SESSION['USERID'])) {
		$_SESSION['FB_isFan'] = 1;
		setcookie('FB_isFan', $_SERVER['REQUEST_TIME'] + 2592000, $_SERVER['REQUEST_TIME'] + 2592000, '/');
		exit('ok');
	}
	else {
		$USERID = intval($_SESSION['USERID']);
		$conn->execute('UPDATE `members` SET `FB_isFan`="1" WHERE `USERID`="'.$USERID.'" LIMIT 1');
		$_SESSION['FB_isFan'] = 1;
		exit('ok');
	}
}
elseif ($_POST['action'] == 'invite') {
	if (!$_SESSION['USERID'])
		die('access deniend');
	else {
		$r = $conn->execute('SELECT `invites` FROM `members_friends` WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1');
		if ($r->fields['invites'] == '') {
			$conn->execute('INSERT INTO `members_friends` SET `USERID`="'.intval($_SESSION['USERID']).'", `invites`="1"');
			$invites = 1;
		}
		else {
			$conn->execute('UPDATE `members_friends` SET `invites`=`invites`+1 WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1');
			$invites = $r->fields['invites'] + 1;
		}
		if ($invites >= $config['value_added']['free_min_invites']) {
			exit('ok');
		}
	}
}
?>