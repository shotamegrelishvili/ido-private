<?php
include("../../config.php");
include("../import.php");

header('Access-Control-Allow-Origin: '.$config['baseurl']);
header('Access-Control-Allow-Methods: POST');

if (!$_SESSION['ADMINID']) die('access denied');
elseif (!isset($_POST['PID']) || !is_numeric($_POST['PID']) || !isset($_POST['ACTION'])) die('access denied');

$PID = intval($_POST['PID']);

$query = "SELECT P.`PID`, M.`username`, M.`email`, M.`eemail`, M.`eemailprotect`, M.`pin`, P.`active`, P.`last_status`, P.`gtitle`, C.`seo`, P.`moderation_queue`, P.`moderator_id` FROM `members` M, `posts` P, `categories` C WHERE P.`PID`='".$PID."' AND P.`USERID`=M.`USERID` AND C.`CATID`=P.`category` LIMIT 1";
$post_details=$conn->execute($query);

if ($post_details->fields['moderator_id'] && $post_details->fields['moderator_id']!=$_SESSION['ADMINID'] && $post_details->fields['moderation_queue'] > $_SERVER['REQUEST_TIME'] - 1800) {
	exit(json_encode(array('status'=>'ERROR','MSG'=>'Sxva moderatori mushaobs gancxadebaze')));
}

if (!$post_details->fields['PID']) {
	exit(json_encode(array('status'=>'ERROR','MSG'=>'pid_not_found')));
}

if ($_POST['ACTION'] == 'approve') {
	if ($post_details->fields['active'] == '1') {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'deal_already_active')));	
	}

	if (!$post_details->fields['pin'] || !$post_details->fields['eemail'] || !$post_details->fields['eemailprotect'])
		$activeState = 6;
	else
		$activeState = 1;
	$query = "UPDATE `posts` SET `active`='".intval($activeState)."', `last_status`='".$post_details->fields['active']."' WHERE `PID` = '".intval($_POST['PID'])."' AND `active`='0'  LIMIT 1";
	$executequery=$conn->execute($query);

	$query = 'INSERT INTO `moderator_logs` SET `PID`="'.intval($_POST['PID']).'", `ADMINID`="'.intval($_SESSION['ADMINID']).'", `action`= "approve", `action_datetime`= NOW(), `comment`="last_status: '.$post_details->fields['last_status'].'"';
	$executequery=$conn->execute($query);

	
	if ($activeState == 1) {
		$url = $config['baseurl'].'/'.$post_details->fields['seo'].'/'.$PID.'/'.seo_clean_titles($post_details->fields['gtitle']);
		$sendmailbody =  sprintf($lang['665'], $post_details->fields['username'], $url, $url);
	}
	else {
		$r = base64_encode($post_details->fields['seo'].'/'.$PID.'/'.seo_clean_titles($post_details->fields['gtitle']));
		$url = $config['baseurl'].'/settings?check=1&amp;r='.$r;
		$sendmailbody =  sprintf($lang['675'], $post_details->fields['username'], $url, $config['baseurl'].'/settings');
	}
	$sendmailbody .= '<br /><br />'.$lang['704'].'<br />'.stripslashes($config['site_name']).sprintf($lang['664'], $config['site_email']);
	//d($sendmailbody,1);
	mailme($post_details->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody);

	echo json_encode(array('status'=>'OK', 'who'=>'approve')); exit;
}
elseif ($_POST['ACTION'] == 'deactivate') {
	if ($post_details->fields['active'] == '5') {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'deal_already_rejected')));
	}
	elseif ($post_details->fields['active'] == '0' && $post_details->fields['last_status'] == '1') {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'deal_already_inactive')));	
	}
	elseif (!isset($_POST['MSG']) || !$_POST['MSG'] || strpos($_POST['MSG'], '____')!==FALSE) {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'write_message_to_user')));
	}

	if ($post_details->fields['active'] == '1') {
		$active = '0';
		$last_status = '1';
		$action = 'deactivate';
	}
	elseif ($post_details->fields['active'] == '0') {
		$active = '5';
		$last_status = '0';
		$action = 'soft_reject';
	}
	else {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'unknown_status')));
	}

	$query = "UPDATE `posts` SET `active`='".intval($active)."', `last_status`='".intval($last_status)."' WHERE `PID` = '".intval($_POST['PID'])."'  LIMIT 1";
	$executequery=$conn->execute($query);

	$query = 'INSERT INTO `moderator_logs` SET `PID`="'.intval($_POST['PID']).'", `ADMINID`="'.intval($_SESSION['ADMINID']).'", `action`= "'.sql_quote($action).'", `action_datetime`= NOW(), `comment`="last_status: '.$last_status.'; MSG: '.sql_quote($_POST['MSG']).'"';
	$executequery=$conn->execute($query);

	$sendmailbody = $_POST['MSG'].sprintf($lang['664'], $config['site_email']);
	mailme($post_details->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody);

	echo json_encode(array('status'=>'OK', 'who'=>'deactivate')); exit;
}
elseif ($_POST['ACTION'] == 'remove') {
	//if ($post_details->fields['active'] == '1') {
	//	exit(json_encode(array('status'=>'ERROR','MSG'=>'remove_not_available_for_active_deal')));	
	//}
	if (true) {
		$queryb = "SELECT COUNT(*) as `total` FROM `orders` WHERE `PID`='".$PID."' AND `status` IN ('0', '1', '4', '6')";
		$executequeryb=$conn->execute($queryb);
		$quecount = $executequeryb->fields['total']+0;
		if ($quecount)
			exit(json_encode(array('status'=>'ERROR','MSG'=>'remove_not_available_for_deal_with_active_orders')));
	}
	if (!isset($_POST['MSG']) || !$_POST['MSG'] || strpos($_POST['MSG'], '____')!==FALSE) {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'write_message_to_user')));
	}

	if ($post_details->fields['active'] == '1') {
		$last_status = '1';
		$action = 'remove';
	}
	elseif ($post_details->fields['active'] == '0') {
		$last_status = '0';
		$action = 'hard_reject';
	}
	else {
		exit(json_encode(array('status'=>'ERROR','MSG'=>'unknown_status')));
	}

	$query = "UPDATE `posts` SET `is_del`='1', `last_status`='".intval($last_status)."' WHERE `PID` = '".intval($_POST['PID'])."' LIMIT 1";
	$executequery=$conn->execute($query);

	$query = 'INSERT INTO `moderator_logs` SET `PID`="'.intval($_POST['PID']).'", `ADMINID`="'.intval($_SESSION['ADMINID']).'", `action`= "'.sql_quote($action).'", `action_datetime`= NOW(), `comment`="last_status: '.$last_status.'; MSG: '.sql_quote($_POST['MSG']).'"';
	$executequery=$conn->execute($query);

	$sendmailbody = $_POST['MSG'].sprintf($lang['664'], $config['site_email']);
	mailme($post_details->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody,1);

	echo json_encode(array('status'=>'OK', 'who'=>'remove')); exit;
}
elseif ($_POST['ACTION'] == 'orders') {
	$query = "SELECT O.`OID`, O.`status`, LEFT(FROM_UNIXTIME(O.`time_added`),16) as `time_added`, O.`stime`, O.`pending_request`, O.`PR_start_time`, M.`username`, CASE WHEN O.`status` IN ('0','1','4','6') THEN 1 ELSE 0 END as `status_order` FROM `orders` O, `members` M WHERE O.`PID`='".$PID."' AND M.`USERID`=O.`USERID` ORDER BY `status_order` DESC, O.`OID` DESC";
	$executequery=$conn->execute($query);
	$rows = $executequery->getrows();
	if (!sizeof($rows)){
		exit(json_encode(array('status'=>'ERROR','MSG'=>'active_orders_not_found')));
	}
	$result = array();
	foreach ($rows as $r1=>$r2) {
		foreach ($r2 as $k=>$v) {
			if (is_numeric($k)) continue;
			else $result[$r1][$k] = $v;
		}
	}
	echo json_encode(array('status'=>'OK', 'who'=>'orders', 'rows'=>$result, 'fireafter'=>'showOrders')); exit;
}
?>