<?php
require_once($config['basedir'].'/libraries/cache.class.php');
function insert_file_version($var)
{
	if (isset($var['value']) && file_exists($var['value'])) echo '?v=',filemtime($var['value']);
	else return false;
}

function filter_messages($var)
{
	global $config;
	//d($config);
	$text = $var;
	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
	$ret = ' ' . $text;

	preg_match_all('#(http[s]?://)?(www\.|ftp\.)?(\S+\.\S+)#i', $ret, $matches);
	foreach ($matches[3] as $i=>$match) {
		if (($matches[1][$i] || $matches[2][$i]) && stripos($match, $config['domain'])!==0) {
			return '1';
		}
	}

//	$ret = preg_replace('#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is', "\\1replacement", $ret);
//	$ret = preg_replace('#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is', "\\1replacement", $ret);
	

	$ret = preg_replace('#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i', "\\1replacement", $ret);
	$ret = substr($ret, 1);

	if (preg_match('/replacement/i', $ret)) 
	{
		return '1';
	} 
	else 
	{
		return '0';
	}
}

function hyperlinkize($text='', $nofollow=false) {
	global $config;
	preg_match_all('#(http[s]?://)?(www\.)?('.$config['domain'].')([^-][\S]{1,})#ui', $text, $matches);
	foreach ($matches[0] as $i=>$match) {
		$text = str_replace($match, '<a href="http://'.$config['domain'].$matches[4][$i].'"'.($nofollow ? 'rel="nofollow"' : '').'>http://'.$config['marketingDomain'].$matches[4][$i].'</a>', $text);
	}
	return $text;
}

function autostart_tour()
{
	global $config,$conn;
	if (isset($_COOKIE['_ts']) && $_COOKIE['_ts'] > 2){
		if(isset($_SESSION['TOUR_SEEN']) && !$_SESSION['TOUR_SEEN']) {
			$query = "UPDATE `members` SET `tour_seen` = '1' WHERE `USERID`='".intval($_SESSION['USERID'])."' LIMIT 1"; 
			$executequery=$conn->execute($query);
			$_SESSION['TOUR_SEEN'] = 1;
		}
		return 'false';
	}
	elseif (isset($_SESSION['TOUR_SEEN']) && $_SESSION['TOUR_SEEN']) {
		return 'false';
	}
	if (!isset($_COOKIE['_t']) || $_COOKIE['_t'] < $_SERVER['REQUEST_TIME']) {
		$t_s = isset($_COOKIE['_ts']) ? $_COOKIE['_ts']+1 : 1;
		setcookie('_t', $_SERVER['REQUEST_TIME'] + 172800, $_SERVER['REQUEST_TIME'] + 172800, '/');
		setcookie('_ts', $t_s, $_SERVER['REQUEST_TIME'] + 90 * 24 * 3600, '/');
		return 'true';
	}
	elseif (isset($_SESSION['TOUR_SEEN']) && !$_SESSION['TOUR_SEEN']) {
		$_SESSION['TOUR_SEEN'] = 1;
		return 'true';
	}
	else {
		if (isset($_SESSION['USERID']) && $_SESSION['USERID'] && !isset($_SESSION['TOUR_SEEN'])) {
			$executequery=$conn->execute("SELECT `tour_seen` FROM `members` WHERE `USERID`='".intval($_SESSION['USERID'])."' LIMIT 1");
			$_SESSION['TOUR_SEEN'] = 1;
			if ($executequery->fields['tour_seen']) {
				return 'false';
			}
			else
				return 'true';
		}
		return 'false';
	}
}

function insert_get_notifications_cc() {
	global $config, $cache;

	if (!$_SESSION['USERID']) return 0;
	else
		return ($cache->get('u.Ncc.'.$_SESSION['USERID'])+0);
}

function insert_get_ismember_online($var) {
	global $cache, $config;
	$userLastActivity = $cache->get('u.la.'.$var['USERID']);
	if (!$userLastActivity || $_SERVER['REQUEST_TIME'] - $userLastActivity > $config['user_keepalive'])
		return false;
	else
		return true;
}

function insert_get_member_vas_ownprice($a)
{
	global $conn;
	if($_SESSION['USERID'])
	{
		$query = "SELECT `VID` FROM `members_vas` WHERE `USERID`='".intval($_SESSION['USERID'])."' AND `service`='setownprice' AND `status`='1' LIMIT 1"; 
		$executequery=$conn->execute($query);
		return intval($executequery->fields['VID']);
	}
	else return '0';
}

function insert_get_member_level($a)
{
	global $config,$conn;
	$me = intval($_SESSION['USERID']);
	if($me > "0")
	{
		$query = "SELECT `level` FROM `members` WHERE `USERID`='".sql_quote($me)."' LIMIT 1"; 
		$executequery=$conn->execute($query);
		return intval($executequery->fields['level']);
	}
	else return '0';
}

function insert_get_post_salesnumber($a)
{
	global $config,$conn;

	if($a['PID'])
	{
		$query = "SELECT count(*) as total FROM `orders` WHERE `PID`='".sql_quote($a['PID'])."' AND `status` = '5' LIMIT 1";
		$executequery=$conn->execute($query);
		return intval($executequery->fields['total']);
	}
	else return;
}

function insert_get_packs($a)
{
	global $config,$conn;
	if($config['enable_levels'] == "1" && $config['price_mode'] == "3")
	{
		$me = intval($_SESSION['USERID']);
		if($me > "0")
		{
			$query = "SELECT `level` FROM `members` WHERE `USERID`='".sql_quote($me)."' LIMIT 1";
			$executequery=$conn->execute($query);
			$mlevel = intval($executequery->fields['level']);
			if($mlevel == "3")
			{
				$addl = "WHERE `l3`='1'";
			}
			elseif($mlevel == "2")
			{
				$addl = "WHERE `l2`='1'";
			}
			elseif($mlevel == "1")
			{
				$addl = "WHERE `l1`='1'";
			}
		}
	}
	$query = "SELECT `ID`, `pprice` FROM `packs` $addl ORDER BY `pprice` ASC"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_get_seo_profile($a)
{
		$uname = $a['username'];
		//echo 'user/'.$uname;
		return 'user/'.$uname;
}

function get_seo_profile($uname)
{
		return "user/".$uname;
}

function insert_get_seo_convo($a)
{
		$uname = $a['username'];
		echo "conversations/".$uname;
}

function insert_add_plus($a)
{
	global $lang, $config;
	$p = $a['price'];
	$tt = $lang['62']."+".$a['topic']."+".$lang['63'].$p;
	$tt = str_replace(' ', '+', $tt);
	echo $tt;
}

function insert_get_percent($a)
{
	global $conn;
	$query = "select good, bad from ratings where USERID='".sql_quote($a[userid])."'"; 
	$results=$conn->execute($query);
	$f = $results->getrows();
	$grat = 0;
	$brat = 0;
	$fc = sizeof($f);
	for($i=0;$i<$fc;$i++)
	{
		$tgood = $f[$i]['good'];
		$tbad = $f[$i]['bad'];
		if($tgood == "1")
		{
			$grat++;	
		}
		elseif($tbad == "1")
		{
			$brat++;	
		}
	}
	$g = $grat;
	$b = $brat;
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		return round($r, 1);
	}
	else
	{
		return 0;
	}
}

function insert_get_percent2($a)
{
	global $conn;
	$query = "select good, bad from ratings where USERID='".sql_quote($_SESSION[USERID])."'"; 
	$results=$conn->execute($query);
	$f = $results->getrows();
	$grat = 0;
	$brat = 0;
	$fc = sizeof($f);
	for($i=0;$i<$fc;$i++)
	{
		$tgood = $f[$i]['good'];
		$tbad = $f[$i]['bad'];
		if($tgood == "1")
		{
			$grat++;	
		}
		elseif($tbad == "1")
		{
			$brat++;	
		}
	}
	$g = $grat;
	$b = $brat;
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		return round($r, 1);
	}
	else
	{
		return 0;
	}
}

function insert_get_rating($a)
{
	$g = $a['g'];
	$b = $a['b'];
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		return round($r, 1);
	}
	else
	{
		return 0;
	}
}

function insert_get_rating2($a)
{
	global $conn;
	$query = "select A.good, A.bad from ratings A, members B where A.PID='".sql_quote($a[pid])."' AND A.RATER=B.USERID and B.status='1'"; 
	$results=$conn->execute($query);
	$f = $results->getrows();
	$grat = 0;
	$brat = 0;
	$fc = sizeof($f);
	for($i=0;$i<$fc;$i++)
	{
		$tgood = $f[$i]['good'];
		$tbad = $f[$i]['bad'];
		if($tgood == "1")
		{
			$grat++;	
		}
		elseif($tbad == "1")
		{
			$brat++;	
		}
	}
	$g = $grat;
	$b = $brat;
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		return round($r, 1);
	}
	else
	{
		return 0;
	}
}
function insert_user_balance($var)
{
		global $conn;
		$query="SELECT funds FROM members WHERE USERID='".intval($_SESSION[USERID])."'";
		$executequery=$conn->execute($query);
		$funds = $executequery->fields[funds];
		return "$funds";
}
function insert_get_gtitle($a)
{
	global $conn;
	$query = "select A.gtitle from posts A, orders B where B.OID='".sql_quote($a[oid])."' AND B.PID=A.PID"; 
	$executequery=$conn->execute($query);
	$gtitle = $executequery->fields['gtitle'];
	return $gtitle;
}

function insert_get_total_members($var)
{
	global $conn;
	$query = "SELECT COUNT(*) as mcc FROM members WHERE status='1'"; 
	$executequery=$conn->execute($query);
	$mcc = $executequery->fields['mcc'];
	return $mcc;
}

function insert_get_total_complete_orders($var)
{
	global $conn;
	$query = "SELECT COUNT(*) as mcc FROM `orders` WHERE status='5'"; 
	$executequery=$conn->execute($query);
	$mcc = $executequery->fields['mcc'];
	return $mcc;
}

function insert_get_total_posts($var)
{
	global $conn;
	$query = "SELECT COUNT(*) as cc FROM `posts` WHERE `active`='1' AND `is_del`='0'";
	$executequery=$conn->execute($query);
	$cc = $executequery->fields['cc'];
	return $cc;
}

function insert_last_unread($a)
{
	global $conn;
	$mto = intval($a['uid']);
	$mfr = intval($_SESSION['USERID']);
	$query = "select MSGTO, unread from inbox where ((MSGTO='".sql_quote($mto)."' AND MSGFROM='".sql_quote($mfr)."') OR (MSGTO='".sql_quote($mfr)."' AND MSGFROM='".sql_quote($mto)."')) order by MID desc limit 1"; 
	$executequery=$conn->execute($query);
	$unread = $executequery->fields['unread'];
	$MSGTO = $executequery->fields['MSGTO'];
	if($MSGTO == $mfr)
	{
		return $unread;
	}
	else
	{
		return 0;	
	}
}

function insert_last_email($a)
{
	global $conn;
	$mto = intval($a['uid']);
	$mfr = intval($_SESSION['USERID']);
	$query = "select message from inbox where ((MSGTO='".sql_quote($mto)."' AND MSGFROM='".sql_quote($mfr)."') OR (MSGTO='".sql_quote($mfr)."' AND MSGFROM='".sql_quote($mto)."')) order by MID desc limit 1"; 
	$executequery=$conn->execute($query);
	$message = $executequery->fields['message'];
	return $message;

}

function insert_last_delivery($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$query = "SELECT `MID` FROM `inbox2` WHERE `OID`='".sql_quote($oid)."' AND `action`='delivery' ORDER BY `MID` DESC LIMIT 1"; 
	$executequery=$conn->execute($query);
	$MID = $executequery->fields['MID'];
	return $MID;

}

function insert_last_predelivery($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$query = "SELECT `MID` FROM `inbox2` WHERE `OID`='".sql_quote($oid)."' AND `action`='predelivery' ORDER BY `MID` DESC LIMIT 1"; 
	$executequery=$conn->execute($query);
	$MID = $executequery->fields['MID'];
	return $MID;
}

function insert_get_status($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$query = "select status from orders where OID='".sql_quote($oid)."'"; 
	$executequery=$conn->execute($query);
	$status = $executequery->fields['status'];
	return $status;

}

function insert_fback($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$query = "SELECT COUNT(*) as total FROM `ratings` WHERE `OID`='".sql_quote($oid)."' AND `RATER`='".sql_quote($_SESSION[USERID])."'";
	$executequery=$conn->execute($query);
	$total = $executequery->fields['total']+0;
	return $total;

}

function insert_wdreq($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$query = "select count(*) as total from withdraw_requests where USERID='".sql_quote($_SESSION[USERID])."'";
	$executequery=$conn->execute($query);
	$total = $executequery->fields['total']+0;
	return $total;

}

function insert_fback2($a)
{
	global $conn;
	$oid = intval($a['oid']);
	$sid = intval($a['sid']);
	$query = "select count(*) as total from ratings where OID='".sql_quote($oid)."' AND RATER='".sql_quote($sid)."'"; 
	$executequery=$conn->execute($query);
	$total = $executequery->fields['total']+0;
	return $total;

}

function insert_gig_details($a)
{
	global $conn;
	$query = "SELECT A.*, B.seo from posts A, categories B where A.active='1' AND A.category=B.CATID AND A.PID='".sql_quote($a[pid])."' limit 1";
	$results = $conn->execute($query);
	$w = $results->getrows();
	return $w;
}

function insert_file_details($a)
{
	global $conn;
	$query = "SELECT `FID`, `fname`, `s` FROM `files` WHERE `FID`='".sql_quote($a[fid])."' LIMIT 1";
	$results = $conn->execute($query);
	$w = $results->getrows();
	return $w;
}

function user_blocked_till($UID=0) {
	global $conn;
	if (!$UID) return false;

	$query = "SELECT `blocked_till_date` FROM `members` WHERE `USERID`='".intval($UID)."' LIMIT 1";
	$result = $conn->execute($query);
	if (!$result->fields['blocked_till_date'] || $result->fields['blocked_till_date'] > strftime('%Y-%m-%d'))
		return $result->fields['blocked_till_date'];
	else
		return false;
}

function insert_gfs($a)
{
	global $conn, $config;
	$query = "SELECT `FID`,`fname`,`s`,`hash` FROM `files` WHERE `FID`='".sql_quote($a[fid])."' LIMIT 1"; 
	$r=$conn->execute($query);
	if ($r->fields['hash']) {
		$file_path = $r->fields['s'].'/'.$r->fields['fname'];
		$file_loc = $config['basedir'].'/files/'.$file_path;
	}
	else {
		$file_path = md5($a[fid]).$r->fields['s'].'/'.$r->fields['fname'];
		$file_loc = $config['basedir'].'/files/'.$file_path;
	}
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$f_type=finfo_file($finfo, $file_loc);
	//d($f_type);
	if (in_array($f_type, $config['not_allowed_mime_types']))
		return false;
	else {
		if (strpos($f_type, 'image') !== false) {
			list($width, $height) = getimagesize($file_loc);
			return array('FID'=>$r->fields['FID'],'file_path'=>$file_path,'file_name'=>$r->fields['fname'],'file_type'=>$f_type,'file_ext'=>substr($r->fields['fname'],-3), 'file_size'=>formatBytes(filesize($file_loc), 1), 'file_width'=>$width, 'file_height'=>$height);
		}
		else
			return array('FID'=>$r->fields['FID'],'file_path'=>$file_path,'file_name'=>$r->fields['fname'],'file_type'=>$f_type,'file_ext'=>substr($r->fields['fname'],-3), 'file_size'=>formatBytes(filesize($file_loc), 1));
	}
}

function formatBytes($bytes, $precision = 2) 
{
	$units = array('B', 'KB', 'MB', 'GB', 'TB');
	$bytes = max($bytes, 0);
	$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
	$pow = min($pow, count($units) - 1);
	$bytes /= pow(1024, $pow);
	return round($bytes, $precision) . ' ' . $units[$pow];
}

function insert_mark_read($a)
{
	global $conn;
	$query = "UPDATE inbox SET unread='0' WHERE MID='".sql_quote($a[mid])."'";
	$conn->execute($query);
}

function escape($data)
{
	if (ini_get('magic_quotes_gpc'))
	{
		$data = stripslashes($data);
	}
	return sql_quote($data);
}

function insert_get_wants($cat)
{
	global $conn, $config;
	if (isset($cat['value']) && $cat['value'])
		$addsql = " AND A.`category`='".intval($cat['value'])."'";
	else $addsql = '';
	$query="SELECT B.`username`, A.`want` FROM `wants` A, `members` B WHERE A.`active`='1'{$addsql} AND A.`USERID`=B.`USERID` ORDER BY rand() LIMIT $config[max_suggest]";
	$results = $conn->execute($query);
	$w = $results->getrows();
	return $w;
}

function insert_get_categories($a)
{
	global $config,$conn,$cache;
	$cc = $cache->get('catsCC');
	
	$query = "select * from `categories` where parent='0' order by `ord` ASC"; 
	$results = $conn->execute($query);
	$cats = $results->getrows();
	if (is_array($cc) && sizeof($cc)>1) {
		foreach ($cats as $i=>$c) {
			$cats[$i]['CC'] = $cc[$c['CATID']];
		}
	}
	return $cats;
}

function insert_get_categoriesbyid($a)
{
	global $config,$conn;
	$query = "select * from `categories` where parent='0' order by `ord` ASC"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	$cats = array();
	foreach ($returnthis as $cat) {
		$cats[$cat['CATID']] = $cat;
	}
	return $cats;
}

function insert_get_subcategories($a)
{
	global $config,$conn;
	$query = "select * from categories where parent='".sql_quote($a['parent'])."' order by name asc"; 
	$results = $conn->execute($query);
	$returnthis = $results->getrows();
	return $returnthis;
}

function insert_like_cnt($var)
{
	global $conn;
	$query = "select count(*) as total from bookmarks where USERID='".sql_quote($_SESSION[USERID])."' AND PID='".sql_quote($var[pid])."'"; 
	$executequery=$conn->execute($query);
	$cnt = $executequery->fields['total'];
	if($cnt > 0)
	{
		return "1";
	}
	else
	{
		return "0";
	}
}

function insert_active_orders($a)
{
	global $conn;
	$query = "select count(*) as total from orders where PID='".sql_quote($a[PID])."' AND (status='0' OR status='1' OR status='6')"; 
	$executequery=$conn->execute($query);
	$cnt = $executequery->fields['total'];
	return $cnt;

}

function insert_done_orders($a)
{
	global $conn;
	$query = "select count(*) as total from orders where PID='".sql_quote($a[PID])."' AND status='5'"; 
	$executequery=$conn->execute($query);
	$cnt = $executequery->fields['total'];
	return $cnt;

}

function insert_gig_cnt($var)
{
	global $conn;
	$query = "select count(*) as total from posts where USERID='".sql_quote($_SESSION[USERID])."'"; 
	$executequery=$conn->execute($query);
	$cnt = $executequery->fields['total'];
	if($cnt > 0)
	{
		return "1";
	}
	else
	{
		return "0";
	}
}

function insert_msg_cnt($var)
{
	global $conn;
	$query = "SELECT count(*) AS `total` FROM `inbox` WHERE `MSGTO`='".sql_quote($_SESSION[USERID])."' AND `unread`='1' LIMIT 1";
	$executequery=$conn->execute($query);
	$cnt = $executequery->fields['total'];
	return ($cnt+0);
}

function insert_get_advertisement($var)
{
		global $conn;
		$query="SELECT `code` FROM `advertisements` WHERE `AID`='".sql_quote($var[AID])."' AND `active`='1' LIMIT 1";
		$executequery=$conn->execute($query);
		$getad = $executequery->fields[code];
		echo strip_mq_gpc($getad);
}

function verify_login_admin()
{
		global $config,$conn;
		if($_SESSION['ADMINID'] != "" && is_numeric($_SESSION['ADMINID']) && $_SESSION['ADMINUSERNAME'] != "" && $_SESSION['ADMINPASSWORD'] != "")
		{
			$query="SELECT * FROM `administrators` WHERE `username`='".sql_quote($_SESSION['ADMINUSERNAME'])."' AND `password`='".sql_quote($_SESSION['ADMINPASSWORD'])."' AND `ADMINID`='".sql_quote($_SESSION['ADMINID'])."' LIMIT 1";
			$executequery=$conn->execute($query);

			if($executequery->fields['ADMINID'])
			{
				if ($executequery->fields['moderator_mode']) {
					header("location: ".$config['baseurl']);
					exit;
				}
			}
			else
			{
				header("location:$config[adminurl]/index.php");
				exit;
			}
			
		}
		else
		{
			header("location:$config[adminurl]/index.php");
			exit;
		}
}

function verify_email_username($usernametocheck)
{
	global $config,$conn;
	$query = "select count(*) as total from members where username='".sql_quote($usernametocheck)."' limit 1"; 
	$executequery = $conn->execute($query);
	$totalu = $executequery->fields[total];
	if ($totalu >= 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function verify_valid_email($emailtocheck)
{	
	if(!preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $emailtocheck))
	{
		return false;
	}
	else
	{
		return true;
	}

}

function verify_email_unique($emailtocheck)
{
	global $config,$conn;
	$query = "select count(*) as total from members where email='".sql_quote($emailtocheck)."' limit 1"; 
	$executequery = $conn->execute($query);
	$totalemails = $executequery->fields[total];
	if ($totalemails >= 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="",$html=true,$showdebug=false)
{
	global $config;
	if ($_SERVER['HTTP_HOST'] == 'localhost')
		if ($showdebug) { d(array($sendto, $sendername, $from, $subject, $sendmailbody)); return; }
		else return;
	
	require_once($config['basedir'].'/libraries/awsses.class.php');

	$ses = new SimpleEmailService($config['aws_credentials']['key'], $config['aws_credentials']['secret']);
	$m = new SimpleEmailServiceMessage();
	//global $SERVER_NAME;
	$subject = nl2br($subject);
	$message = nl2br($sendmailbody);
	$m->addTo($sendto);
	$m->setFrom('"'.$sendername.'" <'.$from.'>');
	$m->setSubject($subject);

	if ($html)
		$m->setMessageFromString(strip_tags(str_replace(array('<br>','<p>','<br/>','<br />'), "\n", $message)), $message);
	else
		$m->setMessageFromString(strip_tags(str_replace(array('<br>','<p>','<br/>','<br />'), "\n", $message)));
	
	return $ses->sendEmail($m);
/*
	if($bcc!="")
	{
		$headers = "Bcc: ".$bcc."\n";
	}
	$headers = "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=utf-8 \n";
	$headers .= "X-Priority: 3\n";
	$headers .= "X-MSMail-Priority: Normal\n";
	$headers .= "X-Mailer: PHP/"."MIME-Version: 1.0\n";
	$headers .= "From: " . $from . "\n";
	$headers .= "Content-Type: text/html\n";
	//mail("$sendto","$subject","$sendmailbody","$headers");
*/
}

function get_cat_seo($cid)
{
		global $conn;
		$query="SELECT seo FROM categories WHERE CATID='".sql_quote($cid)."' limit 1";
		$executequery=$conn->execute($query);
		$seo = $executequery->fields['seo'];
		return $seo;
}

function get_cat($cid)
{
		global $conn;
		$query="SELECT name FROM categories WHERE CATID='".sql_quote($cid)."' limit 1";
		$executequery=$conn->execute($query);
		$name = $executequery->fields[name];
		return $name;
}

function insert_get_cat($var)
{
		global $conn;
		$query="SELECT name FROM categories WHERE CATID='".sql_quote($var[CATID])."' limit 1";
		$executequery=$conn->execute($query);
		$name = $executequery->fields[name];
		echo $name;
}

function insert_get_stripped_phrase($a)
{
	$stripper = $a[details]; 
	$stripper = str_replace("\\n", "<br>", $stripper);
	$stripper = str_replace("\\r", "", $stripper);
	$stripper = str_replace("\\", "", $stripper);
	return $stripper;
}

function insert_get_stripped_phrase2($a)
{
	$stripper = $a[details]; 
	$stripper = str_replace("\\n", "\n", $stripper);
	$stripper = str_replace("\\r", "\r", $stripper);
	return $stripper;
}

function listdays($selected)
{
	$days = "";
	for($i=1;$i<=31;$i++)
	{
		if($i == $selected)
		{
			$days .= "<option value=\"$i\" selected>$i</option>";
		}
		else
		{
			$days .= "<option value=\"$i\">$i</option>";
		}
	}
	return $days;
}

function listmonths($selected)
{
	$months = "";
	$allmonths = array("","January","February","March","April","May","June","July","August","September","October","November","December");
	for($i=1;$i<=12;$i++)
	{
		if($i == $selected)
		{
			$months .= "<option value=\"$i\" selected>$allmonths[$i]</option>";
		}
		else
		{
			$months .= "<option value=\"$i\">$allmonths[$i]</option>";
		}
	}
	return $months;
}

function listyears($selected)
{
		$years = "";
		$thisyear = date("Y");
		for($i=$thisyear-100+13;$i<=$thisyear-13;$i++)
		{
				if($i == $selected)
						$years .= "<option value=\"$i\" selected>$i</option>";
				else
						$years .= "<option value=\"$i\">$i</option>";
		}
		return $years;
}

function listcountries($selected)
{
	$country="";
	$countries = array("Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antartica","Antigua and Barbuda","Argentina","Armenia","Aruba","Ascension Island","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Botswana","Bouvet Island","Brazil","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde Islands","Cayman Islands","Chad","Chile","China","Christmas Island","Colombia","Comoros","Cook Islands","Costa Rica","Cote d Ivoire","Croatia/Hrvatska","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Ireland","Isle of Man","Israel","Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte Island", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Island", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion Island", "Romania", "Russian Federation", "Rwanda", "Saint Helena", "Saint Lucia", "San Marino", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovak Republic", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "Spain", "Sri Lanka", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga Islands", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Western Sahara", "Western Samoa", "Yemen", "Yugoslavia", "Zambia","Zimbabwe");
	for($i=0;$i<count($countries);$i++)
	{
		if($selected == $countries[$i])
		{
			$country .="<option value=\"$countries[$i]\" selected>$countries[$i]</option>";
		}
		else
		{
			$country .="<option value=\"$countries[$i]\">$countries[$i]</option>";
		}
	}
	return $country;
}

function insert_get_member_profilepicture($var)
{
		global $conn;
		$query="SELECT `profilepicture` FROM `members` WHERE `USERID`='".sql_quote($var[USERID])."' limit 1";
		//d($query);
		$executequery=$conn->execute($query);
		$results = $executequery->fields[profilepicture];
		if ($results == "")
			return '';
		else
			return $results;
}

function insert_get_past_period($var)
{
	global $lang;
	$y = floor(($_SERVER['REQUEST_TIME'] - $var['t']) / 946080000);
	if ($y) return $y.$lang['540'];
	else {
		$m = floor(($_SERVER['REQUEST_TIME'] - $var['t']) / 2592000);
		if ($m) return $m.$lang['541'];
		else {
			$d = floor(($_SERVER['REQUEST_TIME'] - $var['t']) / 86400);
			if ($var['precise'] && !$d && $_SERVER['REQUEST_TIME'] - strtotime('today') < $_SERVER['REQUEST_TIME'] - $var['t']) {
				return $lang['587'];
			}
			elseif ($d)
				return $d.$lang['542'];
			else {
				if ($var['precise']) {
					$h = floor(($_SERVER['REQUEST_TIME'] - $var['t'])/3600);
					if ($h)
						return $h.$lang['793'];
					else {
						$M = ceil(($_SERVER['REQUEST_TIME'] - $var['t'])/60);
						return $M.$lang['794'];
					}
				}
				else
					return $lang['568'];
			}
		}
	}
}

function insert_com_count($var)
{
	global $conn;
	$query="SELECT count(*) as total FROM posts_comments WHERE PID='".intval($var[PID])."'";
	$executequery=$conn->execute($query);
	$total = $executequery->fields[total];
	return intval($total);
}

function does_post_exist($a)
{
	global $conn, $config;
	$query="SELECT USERID FROM posts WHERE PID='".sql_quote($a)."'";
	$executequery=$conn->execute($query);
	if ($executequery->recordcount()>0)
		return true;
	else
		return false;
}

function update_last_viewed($a)
{
		global $conn;
		$query = "UPDATE posts SET last_viewed='".time()."' WHERE PID='".sql_quote($a)."'";
		$executequery=$conn->execute($query);
}

function session_verification()
{
	if ($_SESSION[USERID] != "")
	{
		if (is_numeric($_SESSION[USERID]))
		{
			return true;
		}
	}
	else
		return false;
}
function insert_get_username_from_userid($var)
{
		global $conn;
		$query="SELECT `username` FROM `members` WHERE `USERID`='".sql_quote($var[USERID])."' LIMIT 1";
		$executequery=$conn->execute($query);
		$getusername = $executequery->fields[username];
		return "$getusername";
}

function update_score($USERID, $oid, $action='') {
	global $conn, $config;
/*
$config['score_matrix'] =	array(
							'registration_initial_score' => 300,
							'initial_task_provide_detail_fail' => -50,
							'initial_task_provide_detail_fail_CC' => true,
							'cancel_task_by_own' => -50,
							'cancel_task_by_own_CC' => true,
							'success_task' => 100,
							'cancel_inform_admin' => -150,
							'cancel_inform_admin_CC' => true
							);
*/
	if (!$action || !$config['score_matrix'][$action] || !$oid || !$USERID) return false;

	if (isset($config['score_matrix'][$action.'_CC'])) {
		$query="SELECT `CC` FROM `members` WHERE USERID='".sql_quote($USERID)."' LIMIT 1";
		$executequery=$conn->execute($query);
		$CC = $executequery->fields[CC];
		if ($config['score_matrix'][$action] < 0) $sign = ''; else $sign = '+';
		if ($config['score_matrix'][$action.'_CC']) {
			$score = '`score` '.$sign.$config['score_matrix'][$action].' * '.($CC+1);
			$score_delta = $config['score_matrix'][$action] * ($CC+1);
		}
		else {
			$score = '`score` '.$sign.$config['score_matrix'][$action];
			$score_delta = $config['score_matrix'][$action];
		}
		$addsql = ', `CC`=`CC`+1';
	}
	else {
		$CC = 0;
		$score = '`score` + '.$config['score_matrix'][$action];
		$score_delta = $config['score_matrix'][$action];
		$addsql = '';
	}
	//d('UPDATE `members` SET `score` = '.$score.$addsql.' WHERE `USERID` = "'.sql_quote($USERID).'" LIMIT 1');
	$conn->execute('UPDATE `members` SET `score` = '.$score.$addsql.' WHERE `USERID` = "'.sql_quote($USERID).'" LIMIT 1');
	//d("INSERT INTO `scores` SET `USERID`='".sql_quote($USERID)."', `score_delta`='".sql_quote($score_delta)."',`action`='".$action."', `OID`='".sql_quote($oid)."'",1);
	$conn->execute("INSERT INTO `scores` SET `USERID`='".sql_quote($USERID)."', `score_delta`='".sql_quote($score_delta)."',`action`='".$action."', `OID`='".sql_quote($oid)."'");
}

function update_rating($USERID, $oid, $action='', $PID='', $RATERID='', $message_body='') {
	global $conn;

	if (!$action || !$oid || !$USERID) return false;
	elseif ($action == 'decrease') {
		$badadd = 1;
		$goodadd = 0;
	}
	elseif ($action == 'increase') {
		$badadd = 0;
		$goodadd = 1;
	}
	$query="SELECT SUM(`bad`) as badcc, SUM(`good`) as goodcc, COUNT(*) as cc FROM `ratings` WHERE USERID='".sql_quote($USERID)."' LIMIT 1";
	$executequery=$conn->execute($query);
	if ($executequery->fields[cc]) {
		$badcc = $executequery->fields[badcc] + $badadd;
		$goodcc = $executequery->fields[goodcc] + $goodadd;
		$cc = $executequery->fields[cc] + 1;
	}
	else {
		$goodcc = $goodadd;
		$badcc = $badadd;
		$cc = 1;
	}

	if ($badcc < $goodcc) {
		$rating = ($goodcc-$badcc)/$cc*100;
	}
	else
		$rating = '0';
//    d('UPDATE `members` SET `rating` = "'.$rating.'", `ratingcount`= "'.$cc.'" WHERE `USERID` = "'.sql_quote($USERID).'" LIMIT 1',1);
	$executequery=$conn->execute('UPDATE `members` SET `rating` = "'.$rating.'", `ratingcount`= "'.$cc.'" WHERE `USERID` = "'.sql_quote($USERID).'" LIMIT 1');
	if ($RATERID) $addq1 = ', `RATER`="'.sql_quote($RATERID).'"';
	else $addq1 = '';
	if ($message_body) $addq2 = ', `comment`="'.sql_quote($message_body).'"';
	else $addq2 = '';
	if ($PID) $addq3 = ', `PID`="'.sql_quote($PID).'"';
	else $addq3 = '';
	$query="INSERT INTO `ratings` SET `USERID`='".sql_quote($USERID)."', `bad`='".$badadd."', `good`='".$goodadd."', `time_added`='".$_SERVER['REQUEST_TIME']."', `OID`='".sql_quote($oid)."'".$addq1.$addq2.$addq3;
	$results=$conn->execute($query);
}

function does_profile_exist($a)
{
	global $conn;
	global $config;
	$query="SELECT username FROM members WHERE USERID='".sql_quote($a)."'";
	$executequery=$conn->execute($query);
	if ($executequery->recordcount()>0)
		return true;
	else
		return false;
}

function update_viewcount_profile($a)
{
		global $conn;
		$query = "UPDATE members SET profileviews = profileviews + 1 WHERE USERID='".sql_quote($a)."'";
		$executequery=$conn->execute($query);
}

function update_viewcount($a)
{
		global $conn;
		$query = "UPDATE posts SET viewcount = viewcount + 1 WHERE PID='".sql_quote($a)."'";
		$executequery=$conn->execute($query);
}

function insert_get_member_comments_count($var)
{
		global $conn;
		$query="SELECT count(*) as total FROM posts_comments WHERE USERID='".sql_quote($var[USERID])."'";
		$executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		echo "$results";
}

function insert_get_posts_count($var)
{
		global $conn;
		$query="SELECT count(*) as total FROM posts WHERE USERID='".sql_quote($var[USERID])."'";
		$executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		echo "$results";
}

function insert_return_posts_count($var)
{
		global $conn;
		$query="SELECT count(*) as total FROM posts WHERE USERID='".sql_quote($var[USERID])."'";
		$executequery=$conn->execute($query);
		$results = $executequery->fields[total];
		return "$results";
}

function insert_get_static($var)
{
		global $conn;
		$query="SELECT $var[sel] FROM `static` WHERE ID='".sql_quote($var[ID])."'";
		$executequery=$conn->execute($query);
		$returnme = $executequery->fields[$var[sel]];
		d($returnme);
		$returnme = strip_mq_gpc($returnme);
		echo "$returnme";
}

function insert_strip_special($a)
{
	$text = $a['text'];
	$text = str_replace(",", " ", $text);
	$text = str_replace(".", " ", $text);
	$text=nl2br($text);
	$text = str_replace("\n", " ", $text);
	$text = str_replace("\r", " ", $text);
	$text = str_replace("<br />", " ", $text);
	$text = str_replace("  ", " ", $text);
	$clean = preg_replace("/^[^a-z0-9]?(.*?)[^a-z0-9]?$/i", "$1", $text);
	return $clean;
}

function insert_clickable_link($var)
{
	$text = $var['text'];
	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);
	$ret = ' ' . $text;
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
	$ret = substr($ret, 1);
	return $ret;
}

function firstDayOfMonth2($uts=null)
{
	$today = is_null($uts) ? getDate() : getDate($uts);
	$first_day = getdate(mktime(0,0,0,$today['mon'],1,$today['year']));
	return $first_day[0];
} 

function firstDayOfYear2($uts=null)
{
	$today = is_null($uts) ? getDate() : getDate($uts);
	$first_day = getdate(mktime(0,0,0,1,1,$today['year']));
	return $first_day[0];
}

function cleanit($text, $encoding='UTF-8')
{
	return htmlentities(strip_tags(stripslashes($text)), ENT_COMPAT, $encoding);
}

function do_resize_image($file, $width = 0, $height = 0, $proportional = false, $output = 'file', $crop=false)
{
	if($height <= 0 && $width <= 0)
	{
	  return false;
	}
	
	$info = getimagesize($file);
	$image = '';

	$final_width = 0;
	$final_height = 0;
	list($width_old, $height_old) = $info;

	if($proportional && !$crop) 
	{
	  if ($width == 0) $factor = $height/$height_old;
	  elseif ($height == 0) $factor = $width/$width_old;
	  else $factor = min ( $width / $width_old, $height / $height_old);   
  
	  $final_width = round ($width_old * $factor);
	  $final_height = round ($height_old * $factor);
	  
	  if($final_width > $width_old && $final_height > $height_old)
	  {
		  $final_width = $width_old;
		  $final_height = $height_old;

	  }
	}
	elseif ($crop) {
		$wk = $width_old / $width;
		$hk = $height_old / $height;

		if ($wk < $hk) {
			$final_width = floor ($width_old / $wk);
			$final_height = floor ($height_old / $wk);
			$to_crop_array = array('x' =>0 , 'y' => floor(($final_height - $height)/2), 'width' => $width, 'height'=> $height);
			$final_height = $final_height;
		}
		elseif ($wk > $hk) {
			$final_width = floor ($width_old / $hk);
			$final_height = floor ($height_old / $hk);
			$to_crop_array = array('x' => floor(($final_width - $width)/2), 'y' => 0, 'width' => $width, 'height'=> $height);
		}
		elseif ($wk == $hk) {
			$final_width = floor ($width_old / $hk);
			$final_height = floor ($height_old / $hk);
		}

	}
	else 
	{
	  $final_width = ( $width <= 0 ) ? $width_old : $width;
	  $final_height = ( $height <= 0 ) ? $height_old : $height;
	}

	
	
	switch($info[2]) 
	{
	  case IMAGETYPE_GIF:
		$image = imagecreatefromgif($file);
	  break;
	  case IMAGETYPE_JPEG:
		$image = imagecreatefromjpeg($file);
	  break;
	  case IMAGETYPE_PNG:
		$image = imagecreatefrompng($file);
	  break;
	  default:
		return false;
	}

	$image_resized = imagecreatetruecolor( $final_width, $final_height );
	
	if(($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG))
	{
	  $trnprt_indx = imagecolortransparent($image);
	
	  if($trnprt_indx >= 0)
	  {
		$trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
		$trnprt_indx    = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
		imagefill($image_resized, 0, 0, $trnprt_indx);
		imagecolortransparent($image_resized, $trnprt_indx);
	  } 
	  elseif($info[2] == IMAGETYPE_PNG) 
	  {
		imagealphablending($image_resized, false);
		$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
		imagefill($image_resized, 0, 0, $color);
		imagesavealpha($image_resized, true);
	  }
	}


	imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
	if ($crop && isset($to_crop_array) && is_array($to_crop_array)) {
		$image_resized = imagecrop($image_resized, $to_crop_array);
	}

	switch( strtolower($output))
	{
	  case 'browser':
		$mime = image_type_to_mime_type($info[2]);
		header("Content-type: $mime");
		$output = NULL;
	  break;
	  case 'file':
		$output = $file;
	  break;
	  case 'return':
		return $image_resized;
	  break;
	  default:
	  break;
	}
	
	if(file_exists($output))
	{
		@unlink($output);
	}

	switch($info[2])
	{
	  case IMAGETYPE_GIF:
		imagegif($image_resized, $output);
	  break;
	  case IMAGETYPE_JPEG:
		imagejpeg($image_resized, $output, 80);
		optimize_image($output, IMAGETYPE_JPEG);
	  break;
	  case IMAGETYPE_PNG:
		imagepng($image_resized, $output);
		optimize_image($output, IMAGETYPE_PNG);
	  break;
	  default:
		return false;
	}
	return true;
}

function optimize_image($file, $type) {
	ob_start();
	if ($type == IMAGETYPE_PNG)
	{
		@passthru('optipng '.$file);
	}
	elseif ($type == IMAGETYPE_JPEG) {
		@passthru('jpegoptim --strip-all '.$file);
	}
	ob_end_clean();
	return;
}

function insert_seo_clean_titles($a)
{
	$title2 = explode(" ", $a['title']);
	$i = 0;
	foreach($title2 as $line)
	{
		if($i < 15)
		{
			$title .= $line."-";
			$i++;
		}
	}
	$title = str_replace(array(":", ".", "^", "*", ",", ";", "~", "[", "]", "<", ">", "\\", "/", "=", "+", "%","\""),"", $title);
	$last = substr($title, -1);
	if($last == "-")
	{
		$title = substr($title, 0, -1);
	}
	$title = str_replace(" ", "-", $title);
	return htmlentities($title);
}

function seo_clean_titles($a)
{
	$title2 = explode(" ", $a);
	$i = 0;
	foreach($title2 as $line)
	{
		if($i < 15)
		{
			$title .= $line."-";
			$i++;
		}
	}
	$title = str_replace(array(":", ".", "^", "*", ",", ";", "~", "[", "]", "<", ">", "\\", "/", "=", "+", "%"),"", $title);
	$last = substr($title, -1);
	if($last == "-")
	{
		$title = substr($title, 0, -1);
	}
	$title = str_replace(" ", "-", htmlentities($title));
	return $title;
}

function delete_gig($PID)
{
	global $config, $conn;
	$PID = intval($PID);
	if($PID > 0)
	{		
		$query = "select p1,p2,p3 from posts WHERE PID='".sql_quote($PID)."' AND USERID='".sql_quote($_SESSION['USERID'])."'"; 
		$results = $conn->execute($query);
		$p1=$results->fields['p1'];
		$p2=$results->fields['p2'];
		$p3=$results->fields['p3'];
		if($p1 != "")
		{
			$dp1 = $config['pdir']."/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
			$dp1 = $config['pdir']."/t/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
			$dp1 = $config['pdir']."/t2/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
		}
		if($p2 != "")
		{
			$dp2 = $config['pdir']."/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
			$dp2 = $config['pdir']."/t/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
			$dp2 = $config['pdir']."/t2/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
		}
		if($p3 != "")
		{
			$dp3 = $config['pdir']."/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
			$dp3 = $config['pdir']."/t/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
			$dp3 = $config['pdir']."/t2/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
		}

		$query = "DELETE FROM posts WHERE PID='".sql_quote($PID)."' AND USERID='".sql_quote($_SESSION['USERID'])."'";
		$conn->Execute($query);		
	}
}

function delete_gig_admin($PID)
{
	global $config, $conn;
	$PID = intval($PID);
	if($PID > 0)
	{		
		$query = "select p1,p2,p3 from posts WHERE PID='".sql_quote($PID)."'"; 
		$results = $conn->execute($query);
		$p1=$results->fields['p1'];
		$p2=$results->fields['p2'];
		$p3=$results->fields['p3'];
		if($p1 != "")
		{
			$dp1 = $config['pdir']."/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
			$dp1 = $config['pdir']."/t/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
			$dp1 = $config['pdir']."/t2/".$p1;
			@chmod($dp1, 0777);
			if (file_exists($dp1))
			{
				@unlink($dp1);
			}
		}
		if($p2 != "")
		{
			$dp2 = $config['pdir']."/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
			$dp2 = $config['pdir']."/t/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
			$dp2 = $config['pdir']."/t2/".$p2;
			@chmod($dp2, 0777);
			if (file_exists($dp2))
			{
				@unlink($dp2);
			}
		}
		if($p3 != "")
		{
			$dp3 = $config['pdir']."/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
			$dp3 = $config['pdir']."/t/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
			$dp3 = $config['pdir']."/t2/".$p3;
			@chmod($dp3, 0777);
			if (file_exists($dp3))
			{
				@unlink($dp3);
			}
		}

		$query = "DELETE FROM posts WHERE PID='".sql_quote($PID)."'";
		$conn->Execute($query);		
	}
}

function issue_refund($buyer,$OID,$price,$override_verification_test=false)
{
	global $conn;

	if($buyer > 0 && $OID > 0 && $price > 0)
	{
		$query = "SELECT `status`, `price` FROM `orders` WHERE `OID`='".sql_quote($OID)."' AND `USERID`='".sql_quote($buyer)."' LIMIT 1";
		$executequery=$conn->execute($query);
		$status = $executequery->fields['status'];

		if($price == $executequery->fields['price']) {
			if($status != 3 && $status != 5 && $status != 7) {
				$query = "SELECT * FROM `payments` WHERE `USERID`='".sql_quote($buyer)."' AND `OID`='".sql_quote($OID)."' AND `cancel`='0' LIMIT 1";
				$executequery=$conn->execute($query);
				$transid = $executequery->fields['ID'];
				$transgateway = $executequery->fields['payment_gateway'];
				$transactioncode = $executequery->fields['transactioncode'];

				if($transid > 0)
				{
					$query = "SELECT `pinprotect` FROM `members` WHERE `USERID`='".sql_quote($buyer)."' LIMIT 1";
					$buyerArr=$conn->execute($query);
					if ($buyerArr->fields['pinprotect'] || $override_verification_test == true) {
						// Buyer verified
						$query = "UPDATE `payments` SET `cancel`='1' WHERE OID='".sql_quote($OID)."' AND `t`='1' AND `cancel`='0' AND `USERID`='".sql_quote($buyer)."' LIMIT 1"; 
						$executequery=$conn->execute($query);
						
						payment_cancel($transgateway, $transaction = array('transactionCode'=>$transactioncode, 'buyerID'=>$buyer, 'paidAmount'=>$price));
					}
					else {
						// Buyer not verified
						$query = "UPDATE `payments` SET `delayed_cancel`='1' WHERE OID='".sql_quote($OID)."' AND `t`='1' AND `cancel`='0' AND `USERID`='".sql_quote($buyer)."' LIMIT 1"; 
						$executequery=$conn->execute($query);
					}
				}
			}
		}
	}
}

function payment_cancel($gateway='balance', $transaction=array()) {
	global $conn, $config;

	if ($gateway == 'balance') {
		$query = "UPDATE `members` SET `afunds`=`afunds`+".sql_quote($transaction['paidAmount'])." WHERE `USERID`='".sql_quote($transaction['buyerID'])."' LIMIT 1";
		$executequery=$conn->execute($query);
	}
	elseif ($gateway == 'emoney' && $transaction['transactionCode']) {
		require_once($config['basedir'].'/libraries/eConnect.class.php');
		$econnectClass = new econnect();

		if (!$econnectClass->INN_access_token) $econnectClass->authorizeINN();
		$response = $econnectClass->cancelPendingPayment($transaction['transactionCode'], $econnectClass->INN_access_token);
	}
}

function insert_get_time_to_days_ago($a)
{
	$td = date('d-m-Y');
	$timestamp = strtotime($td);
	//$currenttime = time();
	//$timediff = $currenttime - $a[time];
	//$oneday = 60 * 60 * 24;
	//$dayspassed = floor($timediff/$oneday);
	//if ($dayspassed == "0")
	if($a[time] > $timestamp)
	{
		return date('M d, Y',$a[time]);
	}
	else
	{
		return date("M j, Y",$a[time]);
	}
}

function insert_get_deadline($a)
{
	$days = intval($a['days']);
	$time = intval($a['time']);
	$ctime = $days * 24 * 60 * 60;
	$utime = $time + $ctime;
	return date("M j, Y",$utime);
}

function count_days($a, $b)
{
	$gd_a = getdate( $a );
	$gd_b = getdate( $b );
	$a_new = mktime( 12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year'] );
	$b_new = mktime( 12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year'] );
	return round( abs( $a_new - $b_new ) / 86400 );
}

function insert_countdown($a)
{
	global $lang;
	$days = intval($a['days']);
	$time = intval($a['time']);
	if ($days)
		$ctime = $days * 24 * 60 * 60;
	else 
		$ctime = 24 * 60 * 60;
	$f_timestamp = $time + $ctime;
	$c_timestamp = $_SERVER['REQUEST_TIME'];
	if($f_timestamp > $c_timestamp)
	{
		$days = floor( ($f_timestamp-$c_timestamp)/(60*60*24) );
		$f_timestamp = $f_timestamp - ($days*60*60*24);
		$hours = floor( ($f_timestamp-$c_timestamp)/(60*60) );
		$f_timestamp = $f_timestamp - ($hours*60*60);
		$minutes = floor( ($f_timestamp-$c_timestamp)/(60) );
		$f_timestamp = $f_timestamp - ($minutes*60);
		$seconds = $f_timestamp-$c_timestamp;
		$go = '';
		if($days > 0)
		{
			$go .= 	$days." ";
			if($days == "1")
			{
				$go .= 	$lang['281']." ";
			}
			else
			{
				$go .= 	$lang['280']." ";
			}
		}
		if($hours > 0)
		{
			$go .= 	$hours." ";
			if($hours == "1")
			{
				$go .= 	$lang['285']." ";
			}
			else
			{
				$go .= 	$lang['284']." ";
			}
		}
		if($minutes > 0)
		{
			$go .= 	$minutes." ";
			if($minutes == "1")
			{
				$go .= 	$lang['283']." ";
			}
			else
			{
				$go .= 	$lang['282']." ";
			}
		}
		return $go;	
	}
}

function insert_late($a)
{
	$days = intval($a['days']);
	$time = intval($a['time']);
	$ctime = $days * 24 * 60 * 60;
	$utime = $time + $ctime;
	$now = time();
	if($now > $utime)
	{
		return "1";	
	}
	else
	{
		return "0";	
	}
}

function insert_get_days_withdraw($a)
{
	global $config;

	// Get order month
	$b = getdate($a['t']);
	$order_month_timestamp = mktime ( '0', '0', '0', $b['mon'], '1', $b['year']);

	$t = strtotime('+'.($config['working_process']['payment_day_of_month']-1).' days', strtotime('first day of +1 month', $order_month_timestamp));
	$n = $_SERVER['REQUEST_TIME'];

	if($t > $n)
	{
		return count_days($t, $n);
	}
	else
	{
		return "0";	
	}
}

function get_days_before_transfer($a)
{
	global $config;

	// Get order month
	$b = getdate($a);
	$order_month_timestamp = mktime ( '0', '0', '0', $b['mon'], '1', $b['year']);

	$t = strtotime('+'.($config['working_process']['payment_day_of_month']-1).' days', strtotime('first day of +1 month', $order_month_timestamp));
	$n = $_SERVER['REQUEST_TIME'];
	if($t > $n)
	{
		return count_days($t, $n);
	}
	else
	{
		return "0";	
	}
}

function insert_get_yprice($a)
{
	global $config;
	$p = number_format($a['p'], 2, '.', '');
	$c = number_format($a['c'], 2, '.', '');
	
	if($c == "0")
	{
		$c = number_format($config['commission'], 2, '.', '');
	}
	if($p > $c)
	{
		$pc = $p - $c;
		$formatted = sprintf("%01.2f", $pc);
		return $formatted;
	}
	else
	{
		return "0.00";	
	}
}

function get_yprice($a)
{
	global $config;	
	$c = number_format($config['commission'], 2, '.', '');
	$p = number_format($a, 2, '.', '');
	
	if($p > $c)
	{
		$pc = $p - $c;
		$formatted = sprintf("%01.2f", $pc);
		return $formatted;
	}
	else
	{
		return "0.00";	
	}
}

function get_yprice2($a, $b)
{
	global $config;
	$c = number_format($b, 2, '.', '');
	$p = number_format($a, 2, '.', '');
	if($p > $c)
	{
		$pc = $p - $c;
		$formatted = sprintf("%01.2f", $pc);
		return $formatted;
	}
	else
	{
		return "0.00";	
	}
}

function insert_get_explode($a)
{
	$tags = explode(' ', $a['gtags']);
	return $tags;
}

function send_update_email($msgto, $oid, $debug=false)
{
	if($msgto > 0 && $oid > 0)
	{
		global $config, $conn, $lang;
		$query = "SELECT `username`, `email` FROM `members` WHERE `USERID`='".sql_quote($msgto)."' LIMIT 1"; 
		$executequery=$conn->execute($query);
		$sendto = $executequery->fields['email'];
		$sendname = $executequery->fields['username'];
		if($sendto != "")
		{
			$sendername = $config['site_name'];
			$from = $config['site_email'];
			$subject = $lang['407'];
			$sendmailbody = stripslashes($sendname).",<br><br>";
			$sendmailbody .= $lang['408']." ".$lang['409']."<br>";
			$sendmailbody .= "<a href=".$config['baseurl']."/track?id=$oid>".$config['baseurl']."/track?id=$oid</a><br><br>";
			$sendmailbody .= $lang['23'].",<br>".stripslashes($sendername);
			if ($debug) {
				d($subject);
				d($sendmailbody);
			}
			mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
		}
	}
}

function send_email_to_admin($oid, $message)
{
	global $config, $conn, $lang;
	$query = "SELECT `username`, `email` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1"; 
	$executequery=$conn->execute($query);
	$sendername = $executequery->fields['username'];
	$senderemail = $executequery->fields['email'];
	$from = $senderemail;
	$subject = $lang['611'];
	$sendmailbody = $lang['612'].'<a href="'.$config['baseurl'].'/user/'.stripslashes($sendername).'">'.stripslashes($sendername)."</a> (".$senderemail."),<br><br>";
	$sendmailbody .= $lang['613'].'<br>'.$message.'<br>';
	$sendmailbody .= "<a href=".$config['baseurl']."/track?id=$oid>".$config['baseurl']."/track?id=$oid</a><br><br>";
	$sendmailbody .= 'IP: '.$_SERVER['REMOTE_ADDR'];
	//d(array($config['adminemail'], $sendername, $from, $subject, $sendmailbody),1);
	mailme($config['adminemail'],$config['site_name'],$config['site_email'],$lang['658'],$sendmailbody);
}

function cancel_revenue($OID)
{
	global $config,$conn;
	if($OID > 0)
	{
		$query = "SELECT `PID` FROM `orders` WHERE `OID`='".sql_quote($OID)."' LIMIT 1";
		$executequery=$conn->execute($query);
		$PID = $executequery->fields['PID'];
		if($PID > 0)
		{
			$query = "SELECT `price` FROM `posts` WHERE `PID`='".sql_quote($PID)."' LIMIT 1";
			$executequery=$conn->execute($query);
			$price = $executequery->fields['price'];
			if($price > 0)
			{
				$query = "UPDATE `posts` SET `rev`=`rev`-{$price} WHERE `PID`='".sql_quote($PID)."' LIMIT 1";
				$executequery=$conn->execute($query);
			}
		}
	}
}

function insert_get_short_url($a)
{
	global $conn, $config;
	$SPID = intval($a['PID']);
	$stitle = stripslashes($a['title']);
	$sshort = stripslashes($a['short']);
	$SSEO = stripslashes($a['seo']);
	$SSEO = str_replace(' ', '+', $SSEO);
	return $config['baseurl'].'/'.$SSEO.'/'.$SPID.'/'.$stitle;
}

function insert_get_redirect($a)
{
	$PID = intval($a['PID']);
	$seo = $a['seo'];
	$gtitle = $a['gtitle'];
	$rme = stripslashes($seo).'/'.$PID.'/'.stripslashes($gtitle);
	return urlencode(base64_encode($rme));
} 

function insert_get_redirect2($a)
{
	$uname = $a['uname'];
	$pid = $a['pid'];
	if($pid > 0)
	{
		$addp = "?id=".$pid;	
	}
	$rme = "conversations/".stripslashes($uname).$addp;
	return base64_encode($rme);
}

function update_gig_rating($PID)
{
	global $conn;
	$query = "select good, bad from ratings where PID='".sql_quote($PID)."'"; 
	$results=$conn->execute($query);
	$f = $results->getrows();
	$t = 0;
	$grat = 0;
	$brat = 0;
	$fs = sizeof($f);
	for($k=0;$k<$fs;$k++)
	{
		$tgood = $f[$k]['good'];
		$tbad = $f[$k]['bad'];
		if($tgood == "1")
		{
			$grat++;	
		}
		elseif($tbad == "1")
		{
			$brat++;	
		}
	}
	$g = $grat;
	$b = $brat;
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		$gr = round($r, 1);
	}
	else
	{
		$gr = 0;
	}
	$uquery = "UPDATE posts SET rating='".$gr."', rcount=rcount+1 WHERE PID='".sql_quote($PID)."'";
	$conn->execute($uquery);
}

function insert_country_code_to_country($a)
{
	$code = $a['code'];
	$country = '';
	if( $code == 'AF' ) $country = 'Afghanistan';
	if( $code == 'AX' ) $country = 'Aland Islands';
	if( $code == 'AL' ) $country = 'Albania';
	if( $code == 'DZ' ) $country = 'Algeria';
	if( $code == 'AS' ) $country = 'American Samoa';
	if( $code == 'AD' ) $country = 'Andorra';
	if( $code == 'AO' ) $country = 'Angola';
	if( $code == 'AI' ) $country = 'Anguilla';
	if( $code == 'AQ' ) $country = 'Antarctica';
	if( $code == 'AG' ) $country = 'Antigua and Barbuda';
	if( $code == 'AR' ) $country = 'Argentina';
	if( $code == 'AM' ) $country = 'Armenia';
	if( $code == 'AW' ) $country = 'Aruba';
	if( $code == 'AU' ) $country = 'Australia';
	if( $code == 'AT' ) $country = 'Austria';
	if( $code == 'AZ' ) $country = 'Azerbaijan';
	if( $code == 'BS' ) $country = 'Bahamas the';
	if( $code == 'BH' ) $country = 'Bahrain';
	if( $code == 'BD' ) $country = 'Bangladesh';
	if( $code == 'BB' ) $country = 'Barbados';
	if( $code == 'BY' ) $country = 'Belarus';
	if( $code == 'BE' ) $country = 'Belgium';
	if( $code == 'BZ' ) $country = 'Belize';
	if( $code == 'BJ' ) $country = 'Benin';
	if( $code == 'BM' ) $country = 'Bermuda';
	if( $code == 'BT' ) $country = 'Bhutan';
	if( $code == 'BO' ) $country = 'Bolivia';
	if( $code == 'BA' ) $country = 'Bosnia and Herzegovina';
	if( $code == 'BW' ) $country = 'Botswana';
	if( $code == 'BV' ) $country = 'Bouvet Island (Bouvetoya)';
	if( $code == 'BR' ) $country = 'Brazil';
	if( $code == 'IO' ) $country = 'British Indian Ocean Territory (Chagos Archipelago)';
	if( $code == 'VG' ) $country = 'British Virgin Islands';
	if( $code == 'BN' ) $country = 'Brunei Darussalam';
	if( $code == 'BG' ) $country = 'Bulgaria';
	if( $code == 'BF' ) $country = 'Burkina Faso';
	if( $code == 'BI' ) $country = 'Burundi';
	if( $code == 'KH' ) $country = 'Cambodia';
	if( $code == 'CM' ) $country = 'Cameroon';
	if( $code == 'CA' ) $country = 'Canada';
	if( $code == 'CV' ) $country = 'Cape Verde';
	if( $code == 'KY' ) $country = 'Cayman Islands';
	if( $code == 'CF' ) $country = 'Central African Republic';
	if( $code == 'TD' ) $country = 'Chad';
	if( $code == 'CL' ) $country = 'Chile';
	if( $code == 'CN' ) $country = 'China';
	if( $code == 'CX' ) $country = 'Christmas Island';
	if( $code == 'CC' ) $country = 'Cocos (Keeling) Islands';
	if( $code == 'CO' ) $country = 'Colombia';
	if( $code == 'KM' ) $country = 'Comoros the';
	if( $code == 'CD' ) $country = 'Congo';
	if( $code == 'CG' ) $country = 'Congo the';
	if( $code == 'CK' ) $country = 'Cook Islands';
	if( $code == 'CR' ) $country = 'Costa Rica';
	if( $code == 'CI' ) $country = 'Cote d\'Ivoire';
	if( $code == 'HR' ) $country = 'Croatia';
	if( $code == 'CU' ) $country = 'Cuba';
	if( $code == 'CY' ) $country = 'Cyprus';
	if( $code == 'CZ' ) $country = 'Czech Republic';
	if( $code == 'DK' ) $country = 'Denmark';
	if( $code == 'DJ' ) $country = 'Djibouti';
	if( $code == 'DM' ) $country = 'Dominica';
	if( $code == 'DO' ) $country = 'Dominican Republic';
	if( $code == 'EC' ) $country = 'Ecuador';
	if( $code == 'EG' ) $country = 'Egypt';
	if( $code == 'SV' ) $country = 'El Salvador';
	if( $code == 'GQ' ) $country = 'Equatorial Guinea';
	if( $code == 'ER' ) $country = 'Eritrea';
	if( $code == 'EE' ) $country = 'Estonia';
	if( $code == 'ET' ) $country = 'Ethiopia';
	if( $code == 'FO' ) $country = 'Faroe Islands';
	if( $code == 'FK' ) $country = 'Falkland Islands (Malvinas)';
	if( $code == 'FJ' ) $country = 'Fiji the Fiji Islands';
	if( $code == 'FI' ) $country = 'Finland';
	if( $code == 'FR' ) $country = 'France, French Republic';
	if( $code == 'GF' ) $country = 'French Guiana';
	if( $code == 'PF' ) $country = 'French Polynesia';
	if( $code == 'TF' ) $country = 'French Southern Territories';
	if( $code == 'GA' ) $country = 'Gabon';
	if( $code == 'GM' ) $country = 'Gambia the';
	if( $code == 'GE' ) $country = 'Georgia';
	if( $code == 'DE' ) $country = 'Germany';
	if( $code == 'GH' ) $country = 'Ghana';
	if( $code == 'GI' ) $country = 'Gibraltar';
	if( $code == 'GR' ) $country = 'Greece';
	if( $code == 'GL' ) $country = 'Greenland';
	if( $code == 'GD' ) $country = 'Grenada';
	if( $code == 'GP' ) $country = 'Guadeloupe';
	if( $code == 'GU' ) $country = 'Guam';
	if( $code == 'GT' ) $country = 'Guatemala';
	if( $code == 'GG' ) $country = 'Guernsey';
	if( $code == 'GN' ) $country = 'Guinea';
	if( $code == 'GW' ) $country = 'Guinea-Bissau';
	if( $code == 'GY' ) $country = 'Guyana';
	if( $code == 'HT' ) $country = 'Haiti';
	if( $code == 'HM' ) $country = 'Heard Island and McDonald Islands';
	if( $code == 'VA' ) $country = 'Holy See (Vatican City State)';
	if( $code == 'HN' ) $country = 'Honduras';
	if( $code == 'HK' ) $country = 'Hong Kong';
	if( $code == 'HU' ) $country = 'Hungary';
	if( $code == 'IS' ) $country = 'Iceland';
	if( $code == 'IN' ) $country = 'India';
	if( $code == 'ID' ) $country = 'Indonesia';
	if( $code == 'IR' ) $country = 'Iran';
	if( $code == 'IQ' ) $country = 'Iraq';
	if( $code == 'IE' ) $country = 'Ireland';
	if( $code == 'IM' ) $country = 'Isle of Man';
	if( $code == 'IL' ) $country = 'Israel';
	if( $code == 'IT' ) $country = 'Italy';
	if( $code == 'JM' ) $country = 'Jamaica';
	if( $code == 'JP' ) $country = 'Japan';
	if( $code == 'JE' ) $country = 'Jersey';
	if( $code == 'JO' ) $country = 'Jordan';
	if( $code == 'KZ' ) $country = 'Kazakhstan';
	if( $code == 'KE' ) $country = 'Kenya';
	if( $code == 'KI' ) $country = 'Kiribati';
	if( $code == 'KP' ) $country = 'Korea';
	if( $code == 'KR' ) $country = 'Korea';
	if( $code == 'KW' ) $country = 'Kuwait';
	if( $code == 'KG' ) $country = 'Kyrgyz Republic';
	if( $code == 'LA' ) $country = 'Lao';
	if( $code == 'LV' ) $country = 'Latvia';
	if( $code == 'LB' ) $country = 'Lebanon';
	if( $code == 'LS' ) $country = 'Lesotho';
	if( $code == 'LR' ) $country = 'Liberia';
	if( $code == 'LY' ) $country = 'Libyan Arab Jamahiriya';
	if( $code == 'LI' ) $country = 'Liechtenstein';
	if( $code == 'LT' ) $country = 'Lithuania';
	if( $code == 'LU' ) $country = 'Luxembourg';
	if( $code == 'MO' ) $country = 'Macao';
	if( $code == 'MK' ) $country = 'Macedonia';
	if( $code == 'MG' ) $country = 'Madagascar';
	if( $code == 'MW' ) $country = 'Malawi';
	if( $code == 'MY' ) $country = 'Malaysia';
	if( $code == 'MV' ) $country = 'Maldives';
	if( $code == 'ML' ) $country = 'Mali';
	if( $code == 'MT' ) $country = 'Malta';
	if( $code == 'MH' ) $country = 'Marshall Islands';
	if( $code == 'MQ' ) $country = 'Martinique';
	if( $code == 'MR' ) $country = 'Mauritania';
	if( $code == 'MU' ) $country = 'Mauritius';
	if( $code == 'YT' ) $country = 'Mayotte';
	if( $code == 'MX' ) $country = 'Mexico';
	if( $code == 'FM' ) $country = 'Micronesia';
	if( $code == 'MD' ) $country = 'Moldova';
	if( $code == 'MC' ) $country = 'Monaco';
	if( $code == 'MN' ) $country = 'Mongolia';
	if( $code == 'ME' ) $country = 'Montenegro';
	if( $code == 'MS' ) $country = 'Montserrat';
	if( $code == 'MA' ) $country = 'Morocco';
	if( $code == 'MZ' ) $country = 'Mozambique';
	if( $code == 'MM' ) $country = 'Myanmar';
	if( $code == 'NA' ) $country = 'Namibia';
	if( $code == 'NR' ) $country = 'Nauru';
	if( $code == 'NP' ) $country = 'Nepal';
	if( $code == 'AN' ) $country = 'Netherlands Antilles';
	if( $code == 'NL' ) $country = 'Netherlands the';
	if( $code == 'NC' ) $country = 'New Caledonia';
	if( $code == 'NZ' ) $country = 'New Zealand';
	if( $code == 'NI' ) $country = 'Nicaragua';
	if( $code == 'NE' ) $country = 'Niger';
	if( $code == 'NG' ) $country = 'Nigeria';
	if( $code == 'NU' ) $country = 'Niue';
	if( $code == 'NF' ) $country = 'Norfolk Island';
	if( $code == 'MP' ) $country = 'Northern Mariana Islands';
	if( $code == 'NO' ) $country = 'Norway';
	if( $code == 'OM' ) $country = 'Oman';
	if( $code == 'PK' ) $country = 'Pakistan';
	if( $code == 'PW' ) $country = 'Palau';
	if( $code == 'PS' ) $country = 'Palestinian Territory';
	if( $code == 'PA' ) $country = 'Panama';
	if( $code == 'PG' ) $country = 'Papua New Guinea';
	if( $code == 'PY' ) $country = 'Paraguay';
	if( $code == 'PE' ) $country = 'Peru';
	if( $code == 'PH' ) $country = 'Philippines';
	if( $code == 'PN' ) $country = 'Pitcairn Islands';
	if( $code == 'PL' ) $country = 'Poland';
	if( $code == 'PT' ) $country = 'Portugal, Portuguese Republic';
	if( $code == 'PR' ) $country = 'Puerto Rico';
	if( $code == 'QA' ) $country = 'Qatar';
	if( $code == 'RE' ) $country = 'Reunion';
	if( $code == 'RO' ) $country = 'Romania';
	if( $code == 'RU' ) $country = 'Russian Federation';
	if( $code == 'RW' ) $country = 'Rwanda';
	if( $code == 'BL' ) $country = 'Saint Barthelemy';
	if( $code == 'SH' ) $country = 'Saint Helena';
	if( $code == 'KN' ) $country = 'Saint Kitts and Nevis';
	if( $code == 'LC' ) $country = 'Saint Lucia';
	if( $code == 'MF' ) $country = 'Saint Martin';
	if( $code == 'PM' ) $country = 'Saint Pierre and Miquelon';
	if( $code == 'VC' ) $country = 'Saint Vincent and the Grenadines';
	if( $code == 'WS' ) $country = 'Samoa';
	if( $code == 'SM' ) $country = 'San Marino';
	if( $code == 'ST' ) $country = 'Sao Tome and Principe';
	if( $code == 'SA' ) $country = 'Saudi Arabia';
	if( $code == 'SN' ) $country = 'Senegal';
	if( $code == 'RS' ) $country = 'Serbia';
	if( $code == 'SC' ) $country = 'Seychelles';
	if( $code == 'SL' ) $country = 'Sierra Leone';
	if( $code == 'SG' ) $country = 'Singapore';
	if( $code == 'SK' ) $country = 'Slovakia (Slovak Republic)';
	if( $code == 'SI' ) $country = 'Slovenia';
	if( $code == 'SB' ) $country = 'Solomon Islands';
	if( $code == 'SO' ) $country = 'Somalia, Somali Republic';
	if( $code == 'ZA' ) $country = 'South Africa';
	if( $code == 'GS' ) $country = 'South Georgia and the South Sandwich Islands';
	if( $code == 'ES' ) $country = 'Spain';
	if( $code == 'LK' ) $country = 'Sri Lanka';
	if( $code == 'SD' ) $country = 'Sudan';
	if( $code == 'SR' ) $country = 'Suriname';
	if( $code == 'SJ' ) $country = 'Svalbard & Jan Mayen Islands';
	if( $code == 'SZ' ) $country = 'Swaziland';
	if( $code == 'SE' ) $country = 'Sweden';
	if( $code == 'CH' ) $country = 'Switzerland, Swiss Confederation';
	if( $code == 'SY' ) $country = 'Syrian Arab Republic';
	if( $code == 'TW' ) $country = 'Taiwan';
	if( $code == 'TJ' ) $country = 'Tajikistan';
	if( $code == 'TZ' ) $country = 'Tanzania';
	if( $code == 'TH' ) $country = 'Thailand';
	if( $code == 'TL' ) $country = 'Timor-Leste';
	if( $code == 'TG' ) $country = 'Togo';
	if( $code == 'TK' ) $country = 'Tokelau';
	if( $code == 'TO' ) $country = 'Tonga';
	if( $code == 'TT' ) $country = 'Trinidad and Tobago';
	if( $code == 'TN' ) $country = 'Tunisia';
	if( $code == 'TR' ) $country = 'Turkey';
	if( $code == 'TM' ) $country = 'Turkmenistan';
	if( $code == 'TC' ) $country = 'Turks and Caicos Islands';
	if( $code == 'TV' ) $country = 'Tuvalu';
	if( $code == 'UG' ) $country = 'Uganda';
	if( $code == 'UA' ) $country = 'Ukraine';
	if( $code == 'AE' ) $country = 'United Arab Emirates';
	if( $code == 'GB' ) $country = 'United Kingdom';
	if( $code == 'US' ) $country = 'United States of America';
	if( $code == 'UM' ) $country = 'United States Minor Outlying Islands';
	if( $code == 'VI' ) $country = 'United States Virgin Islands';
	if( $code == 'UY' ) $country = 'Uruguay, Eastern Republic of';
	if( $code == 'UZ' ) $country = 'Uzbekistan';
	if( $code == 'VU' ) $country = 'Vanuatu';
	if( $code == 'VE' ) $country = 'Venezuela';
	if( $code == 'VN' ) $country = 'Vietnam';
	if( $code == 'WF' ) $country = 'Wallis and Futuna';
	if( $code == 'EH' ) $country = 'Western Sahara';
	if( $code == 'YE' ) $country = 'Yemen';
	if( $code == 'ZM' ) $country = 'Zambia';
	if( $code == 'ZW' ) $country = 'Zimbabwe';
	if( $country == '') $country = $code;
	return $country;
}

function country_code_to_country($code)
{
	$country = '';
	if( $code == 'AF' ) $country = 'Afghanistan';
	if( $code == 'AX' ) $country = 'Aland Islands';
	if( $code == 'AL' ) $country = 'Albania';
	if( $code == 'DZ' ) $country = 'Algeria';
	if( $code == 'AS' ) $country = 'American Samoa';
	if( $code == 'AD' ) $country = 'Andorra';
	if( $code == 'AO' ) $country = 'Angola';
	if( $code == 'AI' ) $country = 'Anguilla';
	if( $code == 'AQ' ) $country = 'Antarctica';
	if( $code == 'AG' ) $country = 'Antigua and Barbuda';
	if( $code == 'AR' ) $country = 'Argentina';
	if( $code == 'AM' ) $country = 'Armenia';
	if( $code == 'AW' ) $country = 'Aruba';
	if( $code == 'AU' ) $country = 'Australia';
	if( $code == 'AT' ) $country = 'Austria';
	if( $code == 'AZ' ) $country = 'Azerbaijan';
	if( $code == 'BS' ) $country = 'Bahamas the';
	if( $code == 'BH' ) $country = 'Bahrain';
	if( $code == 'BD' ) $country = 'Bangladesh';
	if( $code == 'BB' ) $country = 'Barbados';
	if( $code == 'BY' ) $country = 'Belarus';
	if( $code == 'BE' ) $country = 'Belgium';
	if( $code == 'BZ' ) $country = 'Belize';
	if( $code == 'BJ' ) $country = 'Benin';
	if( $code == 'BM' ) $country = 'Bermuda';
	if( $code == 'BT' ) $country = 'Bhutan';
	if( $code == 'BO' ) $country = 'Bolivia';
	if( $code == 'BA' ) $country = 'Bosnia and Herzegovina';
	if( $code == 'BW' ) $country = 'Botswana';
	if( $code == 'BV' ) $country = 'Bouvet Island (Bouvetoya)';
	if( $code == 'BR' ) $country = 'Brazil';
	if( $code == 'IO' ) $country = 'British Indian Ocean Territory (Chagos Archipelago)';
	if( $code == 'VG' ) $country = 'British Virgin Islands';
	if( $code == 'BN' ) $country = 'Brunei Darussalam';
	if( $code == 'BG' ) $country = 'Bulgaria';
	if( $code == 'BF' ) $country = 'Burkina Faso';
	if( $code == 'BI' ) $country = 'Burundi';
	if( $code == 'KH' ) $country = 'Cambodia';
	if( $code == 'CM' ) $country = 'Cameroon';
	if( $code == 'CA' ) $country = 'Canada';
	if( $code == 'CV' ) $country = 'Cape Verde';
	if( $code == 'KY' ) $country = 'Cayman Islands';
	if( $code == 'CF' ) $country = 'Central African Republic';
	if( $code == 'TD' ) $country = 'Chad';
	if( $code == 'CL' ) $country = 'Chile';
	if( $code == 'CN' ) $country = 'China';
	if( $code == 'CX' ) $country = 'Christmas Island';
	if( $code == 'CC' ) $country = 'Cocos (Keeling) Islands';
	if( $code == 'CO' ) $country = 'Colombia';
	if( $code == 'KM' ) $country = 'Comoros the';
	if( $code == 'CD' ) $country = 'Congo';
	if( $code == 'CG' ) $country = 'Congo the';
	if( $code == 'CK' ) $country = 'Cook Islands';
	if( $code == 'CR' ) $country = 'Costa Rica';
	if( $code == 'CI' ) $country = 'Cote d\'Ivoire';
	if( $code == 'HR' ) $country = 'Croatia';
	if( $code == 'CU' ) $country = 'Cuba';
	if( $code == 'CY' ) $country = 'Cyprus';
	if( $code == 'CZ' ) $country = 'Czech Republic';
	if( $code == 'DK' ) $country = 'Denmark';
	if( $code == 'DJ' ) $country = 'Djibouti';
	if( $code == 'DM' ) $country = 'Dominica';
	if( $code == 'DO' ) $country = 'Dominican Republic';
	if( $code == 'EC' ) $country = 'Ecuador';
	if( $code == 'EG' ) $country = 'Egypt';
	if( $code == 'SV' ) $country = 'El Salvador';
	if( $code == 'GQ' ) $country = 'Equatorial Guinea';
	if( $code == 'ER' ) $country = 'Eritrea';
	if( $code == 'EE' ) $country = 'Estonia';
	if( $code == 'ET' ) $country = 'Ethiopia';
	if( $code == 'FO' ) $country = 'Faroe Islands';
	if( $code == 'FK' ) $country = 'Falkland Islands (Malvinas)';
	if( $code == 'FJ' ) $country = 'Fiji the Fiji Islands';
	if( $code == 'FI' ) $country = 'Finland';
	if( $code == 'FR' ) $country = 'France, French Republic';
	if( $code == 'GF' ) $country = 'French Guiana';
	if( $code == 'PF' ) $country = 'French Polynesia';
	if( $code == 'TF' ) $country = 'French Southern Territories';
	if( $code == 'GA' ) $country = 'Gabon';
	if( $code == 'GM' ) $country = 'Gambia the';
	if( $code == 'GE' ) $country = 'Georgia';
	if( $code == 'DE' ) $country = 'Germany';
	if( $code == 'GH' ) $country = 'Ghana';
	if( $code == 'GI' ) $country = 'Gibraltar';
	if( $code == 'GR' ) $country = 'Greece';
	if( $code == 'GL' ) $country = 'Greenland';
	if( $code == 'GD' ) $country = 'Grenada';
	if( $code == 'GP' ) $country = 'Guadeloupe';
	if( $code == 'GU' ) $country = 'Guam';
	if( $code == 'GT' ) $country = 'Guatemala';
	if( $code == 'GG' ) $country = 'Guernsey';
	if( $code == 'GN' ) $country = 'Guinea';
	if( $code == 'GW' ) $country = 'Guinea-Bissau';
	if( $code == 'GY' ) $country = 'Guyana';
	if( $code == 'HT' ) $country = 'Haiti';
	if( $code == 'HM' ) $country = 'Heard Island and McDonald Islands';
	if( $code == 'VA' ) $country = 'Holy See (Vatican City State)';
	if( $code == 'HN' ) $country = 'Honduras';
	if( $code == 'HK' ) $country = 'Hong Kong';
	if( $code == 'HU' ) $country = 'Hungary';
	if( $code == 'IS' ) $country = 'Iceland';
	if( $code == 'IN' ) $country = 'India';
	if( $code == 'ID' ) $country = 'Indonesia';
	if( $code == 'IR' ) $country = 'Iran';
	if( $code == 'IQ' ) $country = 'Iraq';
	if( $code == 'IE' ) $country = 'Ireland';
	if( $code == 'IM' ) $country = 'Isle of Man';
	if( $code == 'IL' ) $country = 'Israel';
	if( $code == 'IT' ) $country = 'Italy';
	if( $code == 'JM' ) $country = 'Jamaica';
	if( $code == 'JP' ) $country = 'Japan';
	if( $code == 'JE' ) $country = 'Jersey';
	if( $code == 'JO' ) $country = 'Jordan';
	if( $code == 'KZ' ) $country = 'Kazakhstan';
	if( $code == 'KE' ) $country = 'Kenya';
	if( $code == 'KI' ) $country = 'Kiribati';
	if( $code == 'KP' ) $country = 'Korea';
	if( $code == 'KR' ) $country = 'Korea';
	if( $code == 'KW' ) $country = 'Kuwait';
	if( $code == 'KG' ) $country = 'Kyrgyz Republic';
	if( $code == 'LA' ) $country = 'Lao';
	if( $code == 'LV' ) $country = 'Latvia';
	if( $code == 'LB' ) $country = 'Lebanon';
	if( $code == 'LS' ) $country = 'Lesotho';
	if( $code == 'LR' ) $country = 'Liberia';
	if( $code == 'LY' ) $country = 'Libyan Arab Jamahiriya';
	if( $code == 'LI' ) $country = 'Liechtenstein';
	if( $code == 'LT' ) $country = 'Lithuania';
	if( $code == 'LU' ) $country = 'Luxembourg';
	if( $code == 'MO' ) $country = 'Macao';
	if( $code == 'MK' ) $country = 'Macedonia';
	if( $code == 'MG' ) $country = 'Madagascar';
	if( $code == 'MW' ) $country = 'Malawi';
	if( $code == 'MY' ) $country = 'Malaysia';
	if( $code == 'MV' ) $country = 'Maldives';
	if( $code == 'ML' ) $country = 'Mali';
	if( $code == 'MT' ) $country = 'Malta';
	if( $code == 'MH' ) $country = 'Marshall Islands';
	if( $code == 'MQ' ) $country = 'Martinique';
	if( $code == 'MR' ) $country = 'Mauritania';
	if( $code == 'MU' ) $country = 'Mauritius';
	if( $code == 'YT' ) $country = 'Mayotte';
	if( $code == 'MX' ) $country = 'Mexico';
	if( $code == 'FM' ) $country = 'Micronesia';
	if( $code == 'MD' ) $country = 'Moldova';
	if( $code == 'MC' ) $country = 'Monaco';
	if( $code == 'MN' ) $country = 'Mongolia';
	if( $code == 'ME' ) $country = 'Montenegro';
	if( $code == 'MS' ) $country = 'Montserrat';
	if( $code == 'MA' ) $country = 'Morocco';
	if( $code == 'MZ' ) $country = 'Mozambique';
	if( $code == 'MM' ) $country = 'Myanmar';
	if( $code == 'NA' ) $country = 'Namibia';
	if( $code == 'NR' ) $country = 'Nauru';
	if( $code == 'NP' ) $country = 'Nepal';
	if( $code == 'AN' ) $country = 'Netherlands Antilles';
	if( $code == 'NL' ) $country = 'Netherlands the';
	if( $code == 'NC' ) $country = 'New Caledonia';
	if( $code == 'NZ' ) $country = 'New Zealand';
	if( $code == 'NI' ) $country = 'Nicaragua';
	if( $code == 'NE' ) $country = 'Niger';
	if( $code == 'NG' ) $country = 'Nigeria';
	if( $code == 'NU' ) $country = 'Niue';
	if( $code == 'NF' ) $country = 'Norfolk Island';
	if( $code == 'MP' ) $country = 'Northern Mariana Islands';
	if( $code == 'NO' ) $country = 'Norway';
	if( $code == 'OM' ) $country = 'Oman';
	if( $code == 'PK' ) $country = 'Pakistan';
	if( $code == 'PW' ) $country = 'Palau';
	if( $code == 'PS' ) $country = 'Palestinian Territory';
	if( $code == 'PA' ) $country = 'Panama';
	if( $code == 'PG' ) $country = 'Papua New Guinea';
	if( $code == 'PY' ) $country = 'Paraguay';
	if( $code == 'PE' ) $country = 'Peru';
	if( $code == 'PH' ) $country = 'Philippines';
	if( $code == 'PN' ) $country = 'Pitcairn Islands';
	if( $code == 'PL' ) $country = 'Poland';
	if( $code == 'PT' ) $country = 'Portugal, Portuguese Republic';
	if( $code == 'PR' ) $country = 'Puerto Rico';
	if( $code == 'QA' ) $country = 'Qatar';
	if( $code == 'RE' ) $country = 'Reunion';
	if( $code == 'RO' ) $country = 'Romania';
	if( $code == 'RU' ) $country = 'Russian Federation';
	if( $code == 'RW' ) $country = 'Rwanda';
	if( $code == 'BL' ) $country = 'Saint Barthelemy';
	if( $code == 'SH' ) $country = 'Saint Helena';
	if( $code == 'KN' ) $country = 'Saint Kitts and Nevis';
	if( $code == 'LC' ) $country = 'Saint Lucia';
	if( $code == 'MF' ) $country = 'Saint Martin';
	if( $code == 'PM' ) $country = 'Saint Pierre and Miquelon';
	if( $code == 'VC' ) $country = 'Saint Vincent and the Grenadines';
	if( $code == 'WS' ) $country = 'Samoa';
	if( $code == 'SM' ) $country = 'San Marino';
	if( $code == 'ST' ) $country = 'Sao Tome and Principe';
	if( $code == 'SA' ) $country = 'Saudi Arabia';
	if( $code == 'SN' ) $country = 'Senegal';
	if( $code == 'RS' ) $country = 'Serbia';
	if( $code == 'SC' ) $country = 'Seychelles';
	if( $code == 'SL' ) $country = 'Sierra Leone';
	if( $code == 'SG' ) $country = 'Singapore';
	if( $code == 'SK' ) $country = 'Slovakia (Slovak Republic)';
	if( $code == 'SI' ) $country = 'Slovenia';
	if( $code == 'SB' ) $country = 'Solomon Islands';
	if( $code == 'SO' ) $country = 'Somalia, Somali Republic';
	if( $code == 'ZA' ) $country = 'South Africa';
	if( $code == 'GS' ) $country = 'South Georgia and the South Sandwich Islands';
	if( $code == 'ES' ) $country = 'Spain';
	if( $code == 'LK' ) $country = 'Sri Lanka';
	if( $code == 'SD' ) $country = 'Sudan';
	if( $code == 'SR' ) $country = 'Suriname';
	if( $code == 'SJ' ) $country = 'Svalbard & Jan Mayen Islands';
	if( $code == 'SZ' ) $country = 'Swaziland';
	if( $code == 'SE' ) $country = 'Sweden';
	if( $code == 'CH' ) $country = 'Switzerland, Swiss Confederation';
	if( $code == 'SY' ) $country = 'Syrian Arab Republic';
	if( $code == 'TW' ) $country = 'Taiwan';
	if( $code == 'TJ' ) $country = 'Tajikistan';
	if( $code == 'TZ' ) $country = 'Tanzania';
	if( $code == 'TH' ) $country = 'Thailand';
	if( $code == 'TL' ) $country = 'Timor-Leste';
	if( $code == 'TG' ) $country = 'Togo';
	if( $code == 'TK' ) $country = 'Tokelau';
	if( $code == 'TO' ) $country = 'Tonga';
	if( $code == 'TT' ) $country = 'Trinidad and Tobago';
	if( $code == 'TN' ) $country = 'Tunisia';
	if( $code == 'TR' ) $country = 'Turkey';
	if( $code == 'TM' ) $country = 'Turkmenistan';
	if( $code == 'TC' ) $country = 'Turks and Caicos Islands';
	if( $code == 'TV' ) $country = 'Tuvalu';
	if( $code == 'UG' ) $country = 'Uganda';
	if( $code == 'UA' ) $country = 'Ukraine';
	if( $code == 'AE' ) $country = 'United Arab Emirates';
	if( $code == 'GB' ) $country = 'United Kingdom';
	if( $code == 'US' ) $country = 'United States of America';
	if( $code == 'UM' ) $country = 'United States Minor Outlying Islands';
	if( $code == 'VI' ) $country = 'United States Virgin Islands';
	if( $code == 'UY' ) $country = 'Uruguay, Eastern Republic of';
	if( $code == 'UZ' ) $country = 'Uzbekistan';
	if( $code == 'VU' ) $country = 'Vanuatu';
	if( $code == 'VE' ) $country = 'Venezuela';
	if( $code == 'VN' ) $country = 'Vietnam';
	if( $code == 'WF' ) $country = 'Wallis and Futuna';
	if( $code == 'EH' ) $country = 'Western Sahara';
	if( $code == 'YE' ) $country = 'Yemen';
	if( $code == 'ZM' ) $country = 'Zambia';
	if( $code == 'ZW' ) $country = 'Zimbabwe';
	if( $country == '') $country = $code;
	return $country;
}

function update_top_rated($userid, $toprated)
{
	global $config, $conn;
	$result_toprated_count = intval($config['toprated_count']);
	$result_toprated_rating = intval($config['toprated_rating']);
	$query = "select good, bad from ratings where USERID='".sql_quote($userid)."'"; 
	$results=$conn->execute($query);
	$f = $results->getrows();
	$grat = 0;
	$brat = 0;
	for($i=0;$i<count($f);$i++)
	{
		$tgood = $f[$i]['good'];
		$tbad = $f[$i]['bad'];
		if($tgood == "1")
		{
			$grat++;	
		}
		elseif($tbad == "1")
		{
			$brat++;	
		}
	}
	$g = $grat;
	$b = $brat;
	$t = $g + $b;
	if($t > 0)
	{
		$r = (($g / $t) * 100);
		$r = round($r, 1);
				
		if($t >= $result_toprated_count && $r >= $result_toprated_rating)
		{
			$querym="UPDATE members SET toprated='1' WHERE USERID='".sql_quote($userid)."' limit 1";
			$conn->execute($querym);
		}
		elseif($toprated == "1")
		{
			$querym="UPDATE members SET toprated='0' WHERE USERID='".sql_quote($userid)."' limit 1";
			$conn->execute($querym);	
		}
	}
	
}

function get_ctp($IID)
{
	global $config,$conn;
	$query = "SELECT `ctp` FROM `order_items` WHERE `IID`='".sql_quote($IID)."' LIMIT 1";
	$executequery=$conn->execute($query);
	$rctp = $executequery->fields['ctp'];
	return $rctp;
}

function insert_get_ctp($a)
{
	global $config,$conn;
	$query = "select ctp from order_items where IID='".sql_quote($a[IID])."'"; 
	$executequery=$conn->execute($query);
	$rctp = $executequery->fields['ctp'];
	return $rctp;
}


function refresh($url = '', $message = '', $type = 'success') {
	global $config;
	if ($url == '') {
		$_SERVER ['REQUEST_URI'] = str_ireplace ( '//', '/', $_SERVER ['REQUEST_URI'] );
		$url = $config['baseurl'] . '' . $_SERVER ['REQUEST_URI'];
	}
	
	if ($message != '') {
		$_SESSION ['temp'] ['message_title'] = $message;
	}
	
	$_SESSION ['temp'] ['message_type'] = $type;

	header ( "Location: $url" );
	die ();
}

function insert_get_error_message($var)
{
	if (isset($_SESSION['temp']['message_title']) && $_SESSION['temp']['message_type'] == 'error')
	{
		$message = $_SESSION['temp']['message_title'];
		unset($_SESSION['temp']['message_title']);
		unset($_SESSION['temp']['message_type']);
		return $message;
	}
	else
		return '';
}
function insert_get_success_message($var)
{
	if (isset($_SESSION['temp']['message_title']) && $_SESSION['temp']['message_type'] == 'success')
	{
		$message = $_SESSION['temp']['message_title'];
		unset($_SESSION['temp']['message_title']);
		unset($_SESSION['temp']['message_type']);
		return $message;
	}
	else
		return '';
}
function insert_get_emoney_account($a)
{
	if (!isset($_SESSION['USERID']) || !$_SESSION['USERID']) return false;
	global $config,$conn;
	$query = "SELECT `eemail` FROM `members` WHERE `USERID`='".sql_quote($_SESSION['USERID'])."' LIMIT 1";
	$executequery=$conn->execute($query);
	$eemail = $executequery->fields['eemail'];
	return $eemail;
}

function upload_and_update_avatar($USERID, $avatar_uri) {
	global $conn, $config;
	$avatar_uri = str_replace('https','http',$avatar_uri);
	$theimageinfo = getimagesize($avatar_uri);
		if($theimageinfo[2] == 2)
			$ext = '.jpg';
		elseif($theimageinfo[2] == 3)
			$ext = '.png';
		else $ext = '';
	if ($ext) {
		$avatar = file_get_contents($avatar_uri);
		file_put_contents($myvideoimgnew=$config['membersprofilepicdir']."/o/".$USERID.$ext, $avatar);
		$myvideoimgnew2=$config['membersprofilepicdir']."/".$USERID.$ext;
		do_resize_image($myvideoimgnew, "100", "100", false, $myvideoimgnew2);
		$myvideoimgnew3=$config['membersprofilepicdir']."/thumbs/".$USERID.$ext;
		do_resize_image($myvideoimgnew, "50", "50", false, $myvideoimgnew3);
		$myvideoimgnew4=$config['membersprofilepicdir']."/thumbs/A_".$USERID.$ext;
		do_resize_image($myvideoimgnew, "24", "24", false, $myvideoimgnew4);
		$query="UPDATE `members` SET `profilepicture`='".sql_quote($USERID.$ext)."' WHERE `USERID`='".intval($USERID)."' LIMIT 1";
		$conn->execute($query);
	}
}

function getUserDealsOnline($USERID) {
	global $conn, $config, $lang;

	$query = "SELECT `USERID`, `email`, `username`, `pin`, `eemail`, `eemailprotect` FROM `members` WHERE `USERID`='".intval($USERID)."' LIMIT 1";
	$result = $conn->execute($query);
	if ($result->fields['pin'] && strlen($result->fields['pin']) == 11 && $result->fields['eemail'] && $result->fields['eemailprotect'] == 1) {
		$conn->execute("UPDATE `posts` SET `active`='1' WHERE `USERID`='".intval($USERID)."' AND `active`='6'");
		if ($conn->_affectedrows()) {
			$url = $config['baseurl'].'/user/'.seo_clean_titles($result->fields['username']);
			$sendmailbody =  sprintf($lang['676'], $result->fields['username'], $url, $url);
			$sendmailbody .= '<br /><br />'.$lang['704'].'<br />'.stripslashes($config['site_name']).sprintf($lang['664'], $config['site_email']);
			// d($sendmailbody,1);
			mailme($result->fields['email'],$config['site_name'],$config['site_email'],$lang['659'],$sendmailbody);
			return true;
		}
	}
	else return false;
}

function isPMvalid($text='') {
	global $lang;
	if (!$text) return true;
	elseif (preg_match('/[_\.0-9a-zA-Z-]+[ ]{0,}[(]?[ ]{0,}@[ ]{0,}[)]?[ ]{0,}[0-9a-zA-Z-]+\.[a-zA-Z]+/', $text)) {
		return false;
	}
	elseif (preg_match('/5[0-9]{6}/', $text)) {
		return false;
	}
	elseif (preg_match_all('/([0-9]{2,3})[ -]{1}([0-9]{2,3})[ -]?([0-9]{0,3})/', $text, $matches)) {
		if ($matches[1][0] && $matches[2][0] && $matches[3][0]) {
			return false;
		}
	}
	elseif (preg_match_all('/([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}([0-9]{1})[ -]{0,1}/', $text, $matches)) {
		return false;
	}
	elseif (strpos($text, $lang[797])!==false || stripos($text, $lang[798])!==false || stripos($text, $lang[799])!==false) {
		return 'suspected';
	}
	else {
		if (sizeof(array_filter(str_split(urldecode($text)),'is_numeric'))>=9) {
			return 'suspected';
		}
	}
	return true;
}

function includes_link($text) {
	if (stripos($text, 'http') || stripos($text, 'www'))
		return true;
}

/*
// Experience and Skills Class
*/
class expClass {
	public function getMainKeys() {
		global $cache;
		if (!($mainKeywords = $cache->get('exp.mainKeywords'))) {
			global $conn;
			$r = $conn->execute( 'SELECT * FROM `exp_keys`' );
			$keys = $r->getrows();
			foreach ($keys as $k) {
				$mainKeywords[$k['KID']] = array(
					'KID' 		=> $k['KID'],
					'keyword'	=> $k['keyword'],
					'CATID'		=> $k['CATID'],
					'aliasof'	=> $k['aliasof']
					);
			}
			$cache->set('exp.mainKeywords', $mainKeywords, 2592000);
		}
		return $mainKeywords;
	}

	public function getAliasKeys() {
		global $cache;

		if (!($aliases = $cache->get('exp.aliases')) || 1) {
			global $conn;
			$r = $conn->execute( 'SELECT *, (SELECT GROUP_CONCAT(`KID` SEPARATOR "|") FROM `exp_alias2keys` WHERE `ALIASID` = `exp_aliases`.`ALIASID`) AS `alias2keys` FROM `exp_aliases`' );
			$keys = $r->getrows();
			foreach ($keys as $k) {
				$aliases[$k['ALIASID']] = array(
					'ALIASID'		=> $k['ALIASID'],
					'alias'			=> $k['alias'],
					'show'			=> $k['show'],
					'alias2keys'	=> $k['alias2keys']
					);
			}
			$cache->set('exp.aliases', $aliases, 2592000);
		}
		return $aliases;
	}
}

/*
// Send SMS and instant notification to the user
// Optional Params:
// $checkOnline (default=true) to prevent SMS if a user is online
// $sendSMS (default=true) allow or not sending out SMS
// $notify (default=true) allow instant notification on web-site
*/

class notificationsClass {
	public $msgto=0;
	public $msgfrom=0;
	public $action='';
	public $link='';
	public $checkOnline=true;
	public $sendSMS=true;
	public $notify=true;
	public $time='';

	function send_instant_notification() {
		global $config, $lang, $cache;

		if (!$this->msgto || !$this->action) return false;
		
		if ($this->checkOnline) {
			$userLastActivity = $cache->get('u.la.'.$this->msgto);
			if (!$userLastActivity) $userLastActivity = 0;
			if ($userLastActivity && $_SERVER['REQUEST_TIME'] - $userLastActivity < $config['user_keepalive']) {
				$isOnline = true;
			}
			else $isOnline = false;
		}

		$lastSMS = $cache->get('u.ltSMS.'.$this->msgto);
		//$lastSMS = 0;
		if ($this->sendSMS && (!$this->checkOnline || !$isOnline) && (!$lastSMS || $lastSMS < $_SERVER['REQUEST_TIME'] || ($this->action != 'new_pm' && $this->action != 'task_pm'))) {
			send_sms($this->msgto, $this->action, $config['website_url'].'/'.$this->link);
		}

		if ($this->notify) {
			$userNotifications = $cache->get('u.N.'.$this->msgto);
			$nCC = $cache->get('u.Ncc.'.$this->msgto);

			if ($nCC === false || $nCC === null) $nCC = 1;
			else $nCC++;

			$insertNotification = array('msgfrom'=>$this->msgfrom,'action'=>$this->action,'link'=>$this->link,'time'=>($this->time ? $this->time : $_SERVER['REQUEST_TIME']));
			if (!is_array($userNotifications))
				$userNotifications[] = $insertNotification;
			else
				array_unshift($userNotifications, $insertNotification);

			$cache->set('u.N.'.$this->msgto, $userNotifications, 2592000);
			$cache->set('u.Ncc.'.$this->msgto, $nCC, 2592000);
		}
		return true;
	}
}
function send_instant_notification($msgto=0, $action='', $link='', $msgfrom=0, $checkOnline=true, $sendSMS=true, $notify=true) {
	$notifications = new notificationsClass();
	$notifications->msgto = $msgto;
	$notifications->msgfrom = $msgfrom;
	$notifications->action = $action;
	$notifications->link = $link;
	$notifications->checkOnline = $checkOnline;
	$notifications->sendSMS = $sendSMS;
	$notifications->notify = $notify;
	$notifications->send_instant_notification();
}

function send_sms($msgto=0, $action='', $link='') {
	global $conn, $config, $lang, $cache;

	$r = $conn->execute('SELECT `username`, `mobile`, `lastlogin` FROM `members` WHERE `USERID`="'.intval($msgto).'" LIMIT 1');
	if ($r->fields['mobile']) {
		$body = $config['site_name'].': ';
		if ($action == 'seller_has_new_order') $body .= $lang[800];
		elseif ($action == 'new_pm') $body .= $r->fields['username'].'- '.$lang[801];
		elseif ($action == 'task_pm' || $action == 'mutual_reject') $body .= $r->fields['username'].'- '.$lang[802];
		elseif ($action == 'predelivery') $body .= $r->fields['username'].'- '.$lang[803];
		elseif ($action == 'delivery') $body .= $r->fields['username'].'- '.$lang[804];
		elseif ($action == 'predelivery_accept') $body .= $r->fields['username'].'- '.$lang[805];
		elseif ($action == 'predelivery_reject') $body .= $r->fields['username'].'- '.$lang[806];
		elseif ($action == 'complete') $body .= $r->fields['username'].'- '.$lang[807];
		elseif ($action == 'mutual_accept' || $action == 'task_cancel') $body .= $r->fields['username'].'- '.$lang[808];
		elseif ($action == 'rejection') $body .= $r->fields['username'].'- '.$lang[809];
		elseif ($action == 'suspected') $body .= $lang[810];

		$body .= $link;
//		d($config['website_url'].'/include/functions/sendSMS.php?to='.$r->fields['mobile'].'&body='.urlencode($body).'&token='.md5($r->fields['mobile'].$config['tokenHash'].md5($body)),1);
		$a = file_get_contents($config['website_url'].'/include/functions/sendSMS.php?to='.$r->fields['mobile'].'&body='.urlencode($body).'&token='.md5($r->fields['mobile'].$config['tokenHash'].md5($body)));
		$cache->set('u.ltSMS.'.$msgto, $_SERVER['REQUEST_TIME']+$config['sms_period'], $config['sms_period']);
	}
}

function registerNewUser($user, $username) {
	global $conn, $config;
	$sqladd = '';
	if (!$user['email']) return false;
	elseif ($user['pin'] && strlen($user['pin']) == 11) $sqladd = ", pinprotect='1'";

	if (@$_COOKIE['FB_isFan'] || @$_SESSION['FB_isFan']) {
		$sqladd .= ",`FB_isFan`='1'";
	}

	if (isset($_COOKIE['f'])) {
		require_once ($config['basedir'].'/libraries/crypt.class.php');
		$crypt = new cryptClass();
		$f = $crypt->Decrypt(base64_decode(($_COOKIE['f'])));
		if (is_numeric($f)) {
			$sqladd .= ", `ref_USERID`='".intval($f)."'";
		}
	}

	if ($user['locked'])
		$sqladd .= ",`e_account_type`='business'";

	$query="INSERT INTO `members` SET `email`='".sql_quote($user['email'])."',`eemail`='".sql_quote($user['email'])."',`username`='".sql_quote($username)."', `fullname`='".transgeo($user['first_name'].' '.$user['last_name'])."', `pin`='".sql_quote($user['pin'])."', `mobile`='".sql_quote(substr($user['phone'],-9))."', `password`='".sql_quote(md5('0(8&7'.$user['account']))."', `addtime`='".$_SERVER['REQUEST_TIME']."', `lastlogin`='".$_SERVER['REQUEST_TIME']."', `ip`='".$_SERVER['REMOTE_ADDR']."', `lip`='".$_SERVER['REMOTE_ADDR']."', `country`='".sql_quote($user['country'])."', `verified`='1', `status`='1', `score`='".sql_quote($config['score_matrix']['registration_initial_score'])."',`eemailprotect`='1'".$sqladd;
	$result=$conn->execute($query);
	$USERID = $conn->_insertid();
	if ($user['avatar']) {
		upload_and_update_avatar($USERID, $user['avatar']);
	}
	return $USERID;
}

function updateUser($rowdata, $user) {
	global $conn, $config;

	$sqladd = '';
	if (!$rowdata['profilepicture'] && $user['avatar']) {
		upload_and_update_avatar($rowdata['USERID'], $user['avatar']);
	}
	if (!$rowdata['pinprotect'] && $user['pin']) {
		$sqladd .= ",pin='".sql_quote($user['pin'])."', pinprotect='1'";
	}
	if (!$rowdata['mobile'] && $user['phone']) {
		$sqladd .= ",mobile='".sql_quote($user['phone'])."'";
	}
	if (@$_COOKIE['FB_isFan'] || @$_SESSION['FB_isFan']) {
		$sqladd .= ",`FB_isFan`='1'";
	}
	if ($user['locked'])
		$sqladd .= ",`e_account_type`='business'";
	
	$query="UPDATE `members` SET `eemail`='".sql_quote($user['email'])."', `lastlogin`='".$_SERVER['REQUEST_TIME']."', `lip`='".$_SERVER['REMOTE_ADDR']."', `country`='".sql_quote($user['country'])."', `verified`='1',`eemailprotect`='1'".$sqladd." WHERE USERID='".intval($rowdata['USERID'])."' LIMIT 1";
	$conn->execute($query);
	return true;
}

function signinUser($user, $username) {
	if (!$user['USERID']) return false;
	$_SESSION['USERID'] = $user['USERID'];
	$_SESSION['EMAIL'] = $user['email'];
	$_SESSION['USERNAME'] = $username;
	$_SESSION['VERIFIED'] = 1;
	$_SESSION['FB_isFan'] = $user['FB_isFan'];
	return true;
}

function add_portfolio_item($fid = 0) {
	if (!$fid || !$_SESSION['USERID']) return false;
	global $conn, $config;

	$query = 'SELECT COUNT(*) as cc FROM `portfolio` WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1';
	$result = $conn->execute($query);

	if ($result->fields['cc'] >= $config['portfolio']['maxUploads']) return false;
	else {
		$query="INSERT IGNORE INTO `portfolio` SET `FID`='".intval($fid)."', `USERID`='".intval($_SESSION['USERID'])."', `order`='".($result->fields['cc']+1)."'";
		$conn->execute($query);
		return($conn->_affectedrows());
	}
}

class uploadClass {
	public $allowedTypes = array('image/jpg', 'image/jpeg', 'image/png');
	public $uniqueID = '';
	public $nameDefinition = 'complex';
	public $filename = '';
	public $fileext = null;
	public $filepath = '';

	protected function isFileAllowed($file) {
		foreach ($this->allowedTypes as $type) {
			if (strpos($file, 'data:'.$type.';base64,')!==false && substr_count($file, 'data:')==1) {
				return $type;
				break;
			}
		}
		return false;
	}

	public function processUpload($file='') {
		global $config;
		if (!$file || !($type=$this->isFileAllowed($file))) return false;
		list($t, $ext) = explode('/',$type);
		$file = str_replace('data:'.$type.';base64,', '', $file);
		$file = str_replace(' ', '+', $file);
		if ($ext == 'jpeg') $ext = 'jpg';

		$data = base64_decode($file);
		if (!$this->filename) {
			$this->filename = md5($this->uniqueID.uniqid().'x');
		}
		if ($this->fileext == null) {
			$this->fileext = '.'.$ext;
		}
		if (!$this->filepath)
			$filepath = $config['pdir'].'/temp/' . $this->filename.$this->fileext;
		else
			$filepath = $this->filepath.$this->filename.$this->fileext;

		@file_put_contents($filepath, $data);
		return $filepath;
	}
}
?>