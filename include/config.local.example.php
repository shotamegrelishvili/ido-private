<?
error_reporting(E_ALL ^ E_NOTICE);
/**
 *
 * error_reporting(0); // For Production! 
 * დააკოპირეთ ეს ფაილი, დაარქვით config.local.php და მიუთითეთ თქვენი პარამეტრები.
 * !!!!		config.local.php დამატებულია gitignore ში
 *
**/
$local_places = array('localhost', '::1', 'გარე IP', 'შიდა IP'); // ჩაწერეთ თქვენი გარე ip, შიდა ip, localhost და ყველა მისამართი, რომელსაც იყენებთ ამ საიტზე შესასვლელად შიდა ქსელიდან

// Begin Configuration
if (in_array($_SERVER['HTTP_HOST'], $local_places)) {
	$config['basedir']     =  '~/Documents/ido';
	$config['upload_tmp_dir']     =  '~/Documents/ido/temp';
	$config['baseurl']     =  'http://'.$_SERVER['HTTP_HOST'];
	$config['environment'] = 'local';
	$config['memcached'] = '';

	$DBTYPE = 'mysqli';
	$DBHOST = '127.0.0.1';
	$DBUSER = 'root';
	$DBPASSWORD = '';
	$DBNAME = 'ido';
}