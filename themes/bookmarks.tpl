<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/bookmarks" class=current>{$lang30}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang30}</h1>
    <h2 class="subheading">{$lang562}</h2>
  </div>
</div>

{if $message ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">{include file="error.tpl"}</div>
</div>
{/if}

<div class="row separator mt10"></div>

<div class="work-area-wrapper">
{include file="bit.tpl"}
</div>

{if $posts}
<div class="row mt20">
  <div class="large-12 tcenter columns">
    <a href="javascript:" class="button large secondary subheading radius" id="load-more" data-order="{$smarty.get.s}" data-start="{$ending}" data-type="category">{$lang532}</a>
  </div>
</div>
{else}
<div class="row mt20">
    <div class="large-12 tcenter columns">
        <span class="subheading">არცერთი განცხადება არ გაქვს მონიშნული როგორც რჩეული</span>
    </div>
</div>
{/if}

<script>
    var page = 'bookmarks';
</script>