<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/signin" rel="nofollow" class=current>{if $smarty.get.signup}{$lang1}{else}{$lang2}{/if}</a>
	</ul>
</div>

{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="row mt20"><div class="large-12 small-12 columns">{include file="error.tpl"}</div></div>{elseif isset($smarty.get.signup)}
<div class="row mt20">
	<div class="large-12 small-12 columns tcenter"><span class="heading">არ გაქვს ანგარიში?</span><br><i class="icon-info-sign green icon-large"></i> <span class="subheading">რეგისტრაცია და განცხადების განთავსება სრულიად <strong class="red">უფასოა</strong>!</span></div>
</div>
{/if}

{if $smarty.server['HTTP_HOST'] != 'localhost'}
<script src="https://www.emoney.ge/js/econnect/v1/lib.js"></script>
<script src="https://www.emoney.ge/js/econnect/v1/cl.js"></script>

<iframe id="eMoneyAuthorizeWidgetFrame" src="https://www.emoney.ge/index.php/widget/login_common?lang=ka&amp;protocol={$smarty.server['HTTP_X_FORWARDED_PROTO']}&amp;domain={$smarty.server['HTTP_HOST']}&amp;external_registration=0" style="position:fixed;top:0;left:0;z-index:998;height:100%;width:100%;border:0px;display:none;"></iframe>
{/if}

{if $enable_fc eq "1"}
<div class="row mt20">
	<div class="large-7 large-centered small-12 columns tcenter mt20 submit-wrapper">
	<form action="#" id="signin">
		<a class="button large alert split-left radius expand econnect-act-btn {if $smarty.server['HTTP_HOST'] == 'localhost'}econnect-localhost{/if} vh mb3"><span class="splitter"><img src="{$baseurl}/images/eauth.png" alt="" width="40" height="36"></span><span class="heading">{if isset($smarty.get.signup)}რეგისტრაცია{else}შესვლა{/if}</span></a><br>
		<i class="icon-key"></i> <span class="subheading">არ გაქვს ანგარიში eMoney-ს სისტემაში? <a href="https://www.emoney.ge/index.php/main/signup" target="_blank"><u>გაიარე რეგისტრაცია 1 წუთში</u></a></span>
		<a href="https://www.facebook.com/dialog/permissions.request?app_id={$FACEBOOK_APP_ID}&amp;display=page&amp;next={$baseurl}/&amp;response_type=code&amp;fbconnect=1&amp;perms=email,user_friends" class="split-left button send-button large radius heading expand mt20"><span class="splitter"><i class="icon-facebook icon-2x"></i></span> {if isset($smarty.get.signup)}რეგისტრაცია{else}შესვლა{/if}</a>
		<div class="progress-indicator-icon-message mt10 hide">
		{include file="loader.tpl"}
		</div>
	</form>
	</div>
</div>
{/if}

{if isset($smarty.get.signup)}
<div class="row mt20">
	<div class="large-7 large-centered small-12 columns tcenter mt10">
		<span class="heading">გსურს გაიგო მეტი?</span><br><br>
		<a href="{$baseurl}/about" class="button round secondary small subheading"><i class="icon-question-sign"></i> როგორ მუშაობს?</a><a href="{$baseurl}/about?advantages" class="button round secondary small ml20 subheading"><i class="icon-rocket"></i> {$site_name}-ს უპირატესობები</a> 
	</div>
</div>
{/if}

<ol id="eConnect" class="joyride-list" data-joyride>
<li data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade;nextButton:false">
<form action="{$baseurl}/signin?r={$smarty.get.r}" class="row eConnect-form" method="post">
	<label for="email" class="heading"><i class="icon-envelope gray"></i> ელფოსტა:</label>
	<input type="email" name="email" class="eConnect-email" placeholder="personal@email.ge" required>
	<label for="password" class="heading"><i class="icon-key gray"></i> პაროლი:</label>
	<input type="password" name="password" class="eConnect-password" required><br>
	<input type="hidden" name="econnect_signup" value="1">
	<a class="button medium secondary radius expand econnect-btn" href="javascript:"><span class="lat light-gray"><strong class=h-large><i>e</i></strong> </span> <span class="h-large">Connect</span></a>
</form>
</li>
</ol>

<div class="row mt50">
	&nbsp;
</div>

<script>
	var eMoneyWidgetEnable={if $smarty.server['HTTP_HOST'] != 'localhost'}1{else}0{/if};
	var eMoneyProcess='signin';
	var startTour = true;
	var eConnect_required = 1;
	var def_js = ['{$baseurl}/js/signinsignup.js?v=6','{$baseurl}/js/submitprocessor.js?v=7','{$baseurl}/js/econnect.js?v=12'];
</script>

{if $environment=='local' OR $smarty.get.debug==1}
			  <div class="row">
				<div id="main">
				  <div class="content mt50">
						<link href="{$baseurl}/css/login.css" media="screen" rel="stylesheet" type="text/css" />
						<div class="login-holder">  
							<div id="login-toggle-box" class="login-container">
								<div id="reg-login">
									<div class="login-area">
										<div class="loginform" >
											<div class="loginwrapper" >
												<div class="badge-header">
													<h2>{$lang40}</h2>
													<h3>{$lang48} <a href="{$baseurl}/signup{if $r ne ""}?r={$r|stripslashes}{/if}">{$lang49}</a></h3>
												</div>
												<form action="{$baseurl}/login" method="post">  
													
													{if $error ne ""}
														<div id="errorExplanation">
															<h2>{$lang11}</h2>
															<ul>
																{$error}
															</ul>
														</div>
													{/if}
																								  
													<div class="form-entry">
														<label for="l_username">{$lang36}</label>
														<input class="text" id="l_username" maxlength="16" name="l_username" size="16" tabindex="1" type="text" value="{$user_username}" />
													</div>
													<div class="form-entry">
														<div class="form-label">
															<label for="l_password">{$lang37}</label>
															<span> <a href="{$baseurl}/forgotpassword" style="text-decoration:none">{$lang39}</a></span>
														</div>
														<input class="text" id="l_password" name="l_password" size="30" tabindex="2" type="password" />
													</div>
													<div class="row">
														<input type="submit" value="{$lang2}" class="button" style="padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px;" />
														<input type="hidden" name="jlog" id="jlog" value="1" />
														<div class="remember">
															<input class="checkbox" id="l_remember_me" name="l_remember_me" type="checkbox" value="1" />
															<label for="l_remember_me">{$lang38}</label>
														</div>
													</div>
													<input type="hidden" name="r" value="{$r|stripslashes}" />
												</form>
											</div>
										</div>                            
									</div>
								</div>
							</div>
						</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>

	  </div>
{/if}
{if $environment=='local' && FALSE}
			  <div class="row">
				<div id="main">
				  <div class="content mt50">
						<link href="{$baseurl}/css/login.css" media="screen" rel="stylesheet" type="text/css" />
						<div class="login-holder">	
							<div id="join-toggle-box" class="join-holder">
								<div class="login-container">
									<div id="reg-register">
										<div class="join-area" style="display:block !important;">
											<div class="loginwrapper">                        
												<div class="badge-header">
													<h2>{$lang1}</h2>
													<h3>{$lang10} <a href="{$baseurl}/login{if $r ne ""}?r={$r|stripslashes}{/if}">{$lang2}</a></h3>
												</div>                        
												<form action="{$baseurl}/signup" method="post">
												
													{if $error ne ""}
														<div id="errorExplanation">
															<h2>{$lang11}</h2>
															<ul>
																{$error}
															</ul>
														</div>
													{/if}
													
													<div class="form-entry">
														<label>{$lang4}</label>
														<input class="text" id="user_email" name="user_email" size="30" type="text" value="{$user_email|stripslashes}" />
													</div>
													<div class="form-entry">
														<label>{$lang5}</label>
														<input class="text username" id="user_username" maxlength="15" name="user_username" size="15" type="text" value="{$user_username|stripslashes}" />
														<div id="status" class="username-validation"></div>
													</div>
													<div class="form-entry">
														<label class="style3">{$lang6}</label>
														<input class="text style1" id="user_password" name="user_password" size="30" type="password" value="{$user_password|stripslashes}" />
													</div>
													<div class="bottom">
														<div class="right">
															<div class="form-entry">                                                                
																<div class="captcha">
																	<label class="style1">{$lang7}</label><br/>
																	<span><img src="{$baseurl}/include/captcha.php" style="border: 0px; margin:0px; padding:0px" id="cimg" /></span> <input class="text style2" id="captcha" name="user_captcha_solution" size="30" type="text" />
																</div>					
															</div>
														</div>
														<div class="left">
															<div class="remember" style="padding-bottom:5px;">
																<input class="checkbox" id="user_terms_of_use" name="user_terms_of_use" type="checkbox" value="1" {if $user_terms_of_use eq "1"}checked="checked"{/if} />
																<label for="user_terms_of_use"><a href="{$baseurl}/terms_of_service" target="_blank" style="text-decoration:none">{$lang8}</a></label>
															</div>
															<input type="submit" value="{$lang46}" class="button" style="padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px;" />
															<input type="hidden" name="jsub" id="jsub" value="1" />
														</div>
													</div>
													<input type="hidden" name="r" value="{$r|stripslashes}" />
													{if $enable_ref eq "1"}<input type="hidden" name="ref" value="{$ref|stripslashes}" />{/if}
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>

	  </div>
{/if}