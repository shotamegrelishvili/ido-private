{insert name=last_delivery value=a assign=lasdel oid=$oid}
{insert name=last_predelivery value=a assign=laspredel oid=$oid}
	{section name=i loop=$m}
	{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].MSGFROM}
	{if $m[i].action eq "mutual_cancellation_request"}
		{if $who eq "buyer"}
					{if $m[i].MSGFROM eq $smarty.session.USERID}
					<div class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
						<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
						<div class="large-10 columns subheading mt10"><span class="gray">{$lang601}</span>
						<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>

									{if $m[i].cancel eq "0"}
									<i class="icon-info-sign gray"></i> {$lang602}
									<form name="abort{$m[i].MID}" method="post">
									<input type="hidden" name="subabort" value="1">
									<input type="hidden" name="AMID" value="{$m[i].MID}">
									</form>
									<div class="panel">{$lang289}<br><a href="#" onclick="document.abort{$m[i].MID}.submit()">{$lang603}</a></div>
									{/if}
						</div>
					</div>
					<div class="separator"></div>
					{else}
					<div class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format})">
						<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
						<div class="large-10 columns subheading mt10">
								<span class="gray">{$lang288}</span>
								<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p> 
									{if $m[i].cancel eq "0"}
									<form name="decline{$m[i].MID}" method="post">
									<input type="hidden" name="subdecline" value="1">
									<input type="hidden" name="DMID" value="{$m[i].MID}">
									</form>
									<form name="accept{$m[i].MID}" method="post">
									<input type="hidden" name="subaccept" value="1">
									<input type="hidden" name="AMID" value="{$m[i].MID}">
									</form>
									<div class="status-control tcenter"><a href="javascript:" onclick="document.decline{$m[i].MID}.submit()" class="button small radius subheading ml10 alert"><i class="icon-remove"></i> {$lang296}</a> <a href="#" onclick="document.accept{$m[i].MID}.submit()" class="button small radius secondary subheading"><i class="icon-rotate-left"></i> {$lang301}</a></div>
									{if $m[i].cancel ne "1" AND $o.status ne "2"}
									<div class="panel mt10"><i class="icon-info-sign gray"></i> {$lang594}</div>
									{/if}
									{/if}
						</div>
					</div>
					{/if}
					{if $m[i].cancel eq "1"}
					<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
						<div class="large-2 columns"><i class="icon-rotate-left icon-3x green"></i></div>
						<div class="large-10 columns subheading">{if $m[i].CID eq $smarty.session.USERID}შენი გადაწყვეტილებით{elseif $m[i].CID eq $o.USERID}დამკვეთის გადაწყვეტილებით{else}შემსრულებლის გადაწყვეტილებით{/if} {if $m[i].CID eq $smarty.session.USERID AND $o.USERID ne $smarty.session.USERID}{$lang297}{elseif $o.USERID ne $smarty.session.USERID}{$lang297}{else}{$lang595}{/if}</div>
					</div>
					<div class="separator"></div>
					{elseif $m[i].cancel eq "2"}
					<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
						<div class="large-2 columns"><i class="icon-remove-sign icon-3x red"></i></div>
						<div class="large-10 columns subheading">
							{$lang303}
						</div>
					</div>
					{/if}
			{elseif $who eq "owner"}
				{if $m[i].MSGFROM eq $smarty.session.USERID}
				<div class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
					<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
					<div class="large-10 columns subheading mt10"><span class="gray">{$lang286}</span>
					<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>

								{if $m[i].cancel eq "0"}
								{$lang291}
								<form name="abort{$m[i].MID}" method="post">
								<input type="hidden" name="subabort" value="1">
								<input type="hidden" name="AMID" value="{$m[i].MID}">
								</form>
								<div class="panel">{$lang289}<br><a href="#" onclick="document.abort{$m[i].MID}.submit()">{$lang290}</a></div>
								{/if}
					</div>
				</div>
				<div class="separator"></div>
					{else}
						{if $m[i].MSGFROM eq $smarty.session.USERID}
						<div class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
							<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
							<div class="large-10 columns subheading mt10"><span class="gray"> {if $o.USERID eq $smarty.session.USERID}{$lang601}{else}{$lang286}{/if}</span>
							<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>

										{if $m[i].cancel eq "0"}
										<i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{$lang602}{else}{$lang291}{/if}
										<form name="abort{$m[i].MID}" method="post">
										<input type="hidden" name="subabort" value="1">
										<input type="hidden" name="AMID" value="{$m[i].MID}">
										</form>
										<div class="panel">{$lang289}<br><a href="#" onclick="document.abort{$m[i].MID}.submit()">{if $o.USERID eq $smarty.session.USERID}{$lang603}{else}{$lang290}{/if}</a></div>
										{/if}
							</div>
						</div>
						<div class="separator"></div>
						{else}
						<div class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
							<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
							<div class="large-10 columns subheading mt10"><span class="gray">{if $o.USERID eq $smarty.session.USERID}{$lang288}{else}{$lang294}{/if}</span>
							<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>
								{if $m[i].cancel eq "0"}
								<form name="decline{$m[i].MID}" method="post">
								<input type="hidden" name="subdecline" value="1">
								<input type="hidden" name="DMID" value="{$m[i].MID}">
								</form>
								<form name="accept{$m[i].MID}" method="post">
								<input type="hidden" name="subaccept" value="1">
								<input type="hidden" name="AMID" value="{$m[i].MID}">
								</form>
								<div class="tcenter">
									<a href="javascript:" onclick="document.accept{$m[i].MID}.submit()" class="button small radius subheading ml10 alert"><i class="icon-remove"></i> {$lang301}</a>
									<a href="javascript:" onclick="document.decline{$m[i].MID}.submit()" class="button small radius secondary subheading"><i class="icon-rotate-left"></i> {$lang296}</a>
								</div>
								{/if}
								<div class="panel mt10"><i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{$lang594}{else}{$lang604}{/if}</div>
							</div>
						</div>
						<div class="separator"></div>
						{/if}
					{/if}
					{if $m[i].cancel eq "1"}
					<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
						<div class="large-2 columns"><i class="icon-rotate-left icon-3x green"></i></div>
						<div class="large-10 columns subheading">{if $m[i].CID eq $smarty.session.USERID}შენი გადაწყვეტილებით{elseif $m[i].CID eq $o.USERID}დამკვეთის გადაწყვეტილებით{else}შემსრულებლის გადაწყვეტილებით{/if} {if $m[i].CID eq $smarty.session.USERID AND $o.USERID ne $smarty.session.USERID}{$lang297}{elseif $o.USERID ne $smarty.session.USERID}{$lang297}{else}{$lang595}{/if}</div>
					</div>
					<div class="separator"></div>
					{elseif $m[i].cancel eq "2"}
					<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
						<div class="large-2 columns"><i class="icon-remove-sign icon-3x red"></i></div>
						<div class="large-10 columns subheading">
							{$lang302}
						</div>
					</div>
					{/if}
			{/if}
	{elseif $m[i].action eq "seller_cancellation" OR $m[i].action eq "buyer_cancellation" OR $m[i].action eq "seller_cancellation_inform_admin" OR $m[i].action eq "buyer_cancellation_inform_admin"}
		<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
			<div class="large-2 columns"><i class="icon-remove-sign red icon-3x"></i></div>
			<div class="large-10 columns mt10 subheading">{$lang305}{if $m[i].action eq "seller_cancellation_inform_admin" OR $m[i].action eq "buyer_cancellation_inform_admin"}<br>{$lang614}{/if}</div>
		</div>
	{elseif $m[i].action eq "rejection"}
		{if $who eq "buyer"}
			<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns"><i class="icon-remove-sign red icon-3x"></i></div>
				<div class="large-10 columns mt5 subheading"><span class="gray">{$lang321}:</span>
					<p>{$m[i].message|stripslashes|nl2br}</p>
				</div>
			</div>
			{else}
			<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns"><i class="icon-remove-sign red icon-3x"></i></div>
				<div class="large-10 columns mt5 subheading"><span class="gray">{$lang323}:</span>
					<p>{$m[i].message|stripslashes|nl2br}</p>
				</div>
			</div>
			{/if}
			<div class="separator"></div>
	{elseif $m[i].action eq "delivery"}
	<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-gift green icon-3x"></i></div>
		<div class="large-10 columns subheading">
			{if $profilepicture}<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}">{else}<i class="icon-user gray"></i>{/if} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
			<div class="subheading mt10 panel success-panel">
				<span class="gray block mb10">{if $o.USERID eq $smarty.session.USERID}{$lang308}{else}{$lang306}{/if}</span>
				{$m[i].message|stripslashes|nl2br}<br>
				{if $m[i].FID GT "0"}
				{insert name=file_details value=a assign=fd fid=$m[i].FID}
				{section name=x loop=$fd}
				{insert name=gfs value=a assign=afs fid=$fd[x].FID}
				<div class="attached-files mt10">
					<ul>
						<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
					</ul>
				</div>
				{/section}
				{/if}
			</div>
			{if $m[i].MID eq $lasdel AND $m[i].reject eq "0"}
			<span class="subheading gray"> 
				{if $o.status eq "5"}{$lang319} {$o.cltime|date_format}{elseif $o.status ne "2" AND $o.status ne "7" AND $o.status ne "3"}{if $o.USERID eq $smarty.session.USERID}{$lang307}<span class="block tright mt5"><i class="icon-time"></i> {$m[i].time|date_format:"%b %d, %Y %R"}</span>{else}{$lang615}{/if}{/if}
			</span> 
			{/if}
		</div>
	</div>
	<div class="separator mt5"></div>
	{elseif $m[i].action eq "predelivery"}
	<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-question-sign red icon-3x"></i></div>
		<div class="large-10 columns subheading">
			{if $profilepicture}<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}">{else}<i class="icon-user gray"></i>{/if} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
			<div class="subheading mt10">
				<span class="gray block mb10">{if $who=="buyer"}{$lang768}{else}{$lang767}{/if}</span>
				<strong class="block">{$lang545}:</strong>
				<p>{$m[i].message|stripslashes|nl2br}</p>
				{if $m[i].FID GT "0"}
				{insert name=file_details value=a assign=fd fid=$m[i].FID}
				{section name=x loop=$fd}
				{insert name=gfs value=a assign=afs fid=$fd[x].FID}
				<div class="attached-files">
					<ul>
						<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
					</ul>
				</div>
				{/section}
				{/if}
			</div>
			{if $who=='buyer' AND $m[i].MID eq $laspredel AND $m[i].reject eq "0" AND !$m[i].cancel}
				<form name="decline{$m[i].MID}" method="post">
					<input type="hidden" name="subdecline" value="1">
					<input type="hidden" name="DMID" value="{$m[i].MID}">
					</form>
					<form name="accept{$m[i].MID}" method="post">
					<input type="hidden" name="subaccept" value="1">
					<input type="hidden" name="AMID" value="{$m[i].MID}">
				</form>
				<div class="tcenter mt10">
					<a href="javascript:" onclick="if (confirm('{$lang774}')) document.accept{$m[i].MID}.submit()" class="button medium radius success ml10 subheading"><i class="icon-check"></i> {$lang772}</a>
					<a href="javascript:" onclick="if (confirm('{$lang773}')) document.decline{$m[i].MID}.submit()" class="button medium radius subheading alert"><i class="icon-remove"></i> {$lang771}</a>
				</div>
			{/if}
			{if $m[i].cancel ne "0"}
				<div class="panel mt10{if $m[i].cancel eq '2'} success-panel{/if}"><i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{if $m[i].cancel eq '1'}{$lang775}{if $m[i].MID eq $laspredel}<br>{$lang779}{/if}{elseif $m[i].cancel eq '2'}{$lang776}{/if}{else}{if $m[i].cancel eq '1'}{$lang777}{elseif $m[i].cancel eq '2'}{$lang778}{/if}{/if}</div>
			{/if}
		</div>
	</div>
	<div class="separator mt5"></div>
		{*{if $who eq "buyer"}
			<div class="message action" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format})"> 
				<div class="metadata"> 
					<div class="buddyicon"> 
						<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" /></a> 
					</div>                        
					<div class="tip green">&nbsp;</div> 
				</div> 
				<div class="delivery-box"> 
					<div class="img-delivery">&nbsp;</div> 
					<div class="complete delivery"> 
						<div class="message-border"> 
							<div class="message-inner"> 
								<div class="message-content reqbox"> 
									<div class="says"> 
										<h4>{$lang308}</h4> 
									</div> 
									<div class="said"> 
										<p>{$m[i].message|stripslashes|nl2br}</p> 
											{if $m[i].FID GT "0"}
											{insert name=file_details value=a assign=fd fid=$m[i].FID}
											{section name=x loop=$fd}
											{insert name=gfs value=a assign=afs fid=$fd[x].FID}
											<div class="files">
												<ul>
													<li><a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
												</ul>
											</div>
											{/section}
											{/if} 
									</div> 
									{if $m[i].MID eq $lasdel AND $m[i].reject eq "0"}
									<div class="delivery-footer"> 
											<h5>{$lang307}</h5>                                               
									</div> 
									{/if} 
								</div> 
							</div> 
						</div> 
					</div> 
				</div> 
			</div> 
			{else}
			<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
			<div class="large-2 columns"><i class="icon-gift green icon-3x"></i></div>
			<div class="large-10 columns mt10 subheading">
				<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" /> <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
				<p class="subheading">
					<span class="gray">{$lang306}</span><br>
					{$m[i].message|stripslashes|nl2br}
				</p>
				{if $m[i].FID GT "0"}
					{insert name=file_details value=a assign=fd fid=$m[i].FID}
					{section name=x loop=$fd}
					{insert name=gfs value=a assign=afs fid=$fd[x].FID}
					<div class="attached-files">
						<ul>
							<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
						</ul>
					</div>
					{/section}
				{/if}
			</div>
			</div>


			<div class="message action" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format})">
				<div class="metadata">
					<div class="buddyicon">
						<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" /></a>
					</div>                        
					<div class="tip green">&nbsp;</div> 
				</div>
				<div class="delivery-box">
					<div class="img-delivery">&nbsp;</div>
					<div class="complete delivery">
						<div class="message-border">
							<div class="message-inner"> 
								<div class="message-content reqbox">
									<div class="says">
										<h4>{$lang306}</h4>
									</div>
									<div class="said">
										<p>{$m[i].message|stripslashes|nl2br}</p>
											{if $m[i].FID GT "0"}
											{insert name=file_details value=a assign=fd fid=$m[i].FID}
											{section name=x loop=$fd}
											{insert name=gfs value=a assign=afs fid=$fd[x].FID}
											<div class="files">
												<ul>
													<li><a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
												</ul>
											</div>
											{/section}
											{/if}
									</div>   
									{if $m[i].MID eq $lasdel AND $m[i].reject eq "0"}                                         
									<div class="delivery-footer">
											<h5>{$lang307}</h5>
									</div>
									{/if}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/if}
			*}
	{else}
			<div id="message_{$m[i].MID}" class="row by-{if $m[i].MSGFROM eq $smarty.session.USERID}me{else}partner{/if}" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format})">
				<div class="large-2 columns mt10">
				{if $m[i].start eq "1"}
					<i class="icon-download-alt icon-3x green"></i>
				{else}
				<span class="block track-avatar">
					<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}"></a></span>
				{/if}
				</div>
				<div class="large-10 columns subheading mt10">
				{if $m[i].start eq "1"}
					<span class="gray">ინსტრუქციის მიხედვით გაგზავნილი ინფორმაცია:</span>
					<textarea name="instr_answer" rows="6" class="ta6 mt5" disabled="">{$m[i].message|stripslashes|nl2br}</textarea>
				{else}
					<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
					<p class="subheading">{$m[i].message|stripslashes|nl2br}</p>
				{/if}
					{if $m[i].FID GT "0"}
					{insert name=file_details value=a assign=fd fid=$m[i].FID}
					{section name=x loop=$fd}
					{insert name=gfs value=a assign=afs fid=$fd[x].FID}
					<div class="attached-files">
						<ul>
							<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
						</ul>
					</div>
					{/section}
					{/if}
				</div>
			</div>
			<div class="separator"></div>
				
			{if $m[i].start eq "1" AND $USERID eq $smarty.session.USERID}
			<div class="row">
				<div class="large-2 columns mt10">&nbsp;</div>
				<div class="large-10 columns subheading mt10"><span class="gray">{$lang275}! {$lang277}</span>
				<p class="mt5 mb0">{$lang276} <b>{insert name=get_deadline value=a assign=deadline days=$days time=$m[i].time}{$deadline}</b></p>
				</div>
			</div>
			<div class="separator mt10"></div>
			{/if}
	{/if}
{/section}
{if $err}
	<div class="row mt10">
		<div class="large-12 columns panel red subheading">{$err}</div>
	</div>
{/if}
								{* if $who eq "buyer"}
										{insert name=fback value=a assign=fbvl oid=$oid}
										{if $lasdel GT "0" AND $fbvl eq "0"}
												{insert name=get_status value=a assign=stat oid=$oid}
												{if $stat ne "6"}
												<form action="{$baseurl}/track?id={$oid}" class="review_form" id="new_rating" method="post">
												<div class="order-finish"> 
													<div class="respond-to-start-l"> 
														<span class="arr-r"></span><b>{$lang310}</b></span> 
													</div> 
													<div class="post-order-rating"> 
														<p><input checked="checked" class="good-review-button" id="rating_value_1" name="ratingvalue" type="radio" value="1" /><img src="{$imageurl}/thumb_up.png" align="absmiddle"/></p> 
														<p><input class="bad-review-button" id="rating_value_0" name="ratingvalue" type="radio" value="0" /><img src="{$imageurl}/thumb_down.png" align="absmiddle"/></p> 
													</div> 
													<br clear="both"/> 
													<div class="share-experience"> 
														<textarea cols="35" id="rating_comment" maxlength="300" name="ratingcomment" rows="5" title="{$lang311}">{$lang311}</textarea> 
														<br clear="all"/> 
													</div> 
														<input type="submit" value="{$lang46}"  />
												</div>
												<input type="hidden" name="subrat" value="1" /> 
												</form>
												{/if}
										{/if}
								{/if *}