<form action="{$baseurl}/suggested" id="suggest_form" method="post">                        
  <div class="{if !$hide_jobs}large-4{else}large-6 large-centered{/if} columns mt10">
    <label class="subheading">{$lang117}</label>
    <textarea class="suggestion-box mt5" cols="20" id="suggestion_content" maxlength="80" name="sugcontent" rows="3" placeholder="ჩაწერე სამუშაოს აღწერა აქ. ეცადე მოკლედ და გასაგებად აღწერო დავალება."></textarea>
    <div class="suggest-category-select hide">
      <div class="subheading">{$lang119}</div>
      <div class="category mt5">
        <select id="suggestion_category_id" name="sugcat" class="subheading">
        {section name=i loop=$cats}
        <option value="{$cats[i].CATID|stripslashes}">{$cats[i].name|stripslashes}</option>
        {/section}
        </select>
      </div>
      <input type="submit" value="{$lang118}" class="button tiny heading right">
    </div>
    <input type="hidden" name="sugsub" value="1">
  </div>
</form>