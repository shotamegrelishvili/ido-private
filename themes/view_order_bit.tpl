{if !$smarty.session.ADMINID || $smarty.get.debug eq 1}
{* GENERAL USER MODE *}
{if $smarty.session.USERID eq $p.USERID}   
<a href="{$baseurl}/edit?id={$p.PID}" class="order-now-g button large round alert expand subheading mb0"><i class="icon-edit"></i> {$lang141}</a>
{elseif $smarty.session.USERID GT "0"}
<a onclick="document.ordermulti.submit();" href="#" class="order-now-g button radius success large expand heading mb0">{$lang140} ({if $p.price ne "0"}{$p.price} <span class="subheading">{$lang63}</span>{else}{$lang743}{/if})</a>
{else}
<a href="{$baseurl}/signin?r={insert name=get_redirect value=a assign=rurl PID=$p.PID seo=$p.seo gtitle=$title}{$rurl|stripslashes}" class="login-link order-now-g button radius success large expand heading mb0">{$lang140} ({if $p.price ne "0"}{$p.price} <span class="subheading">{$lang63}</span>{else}{$lang743}{/if})</a>
{/if}
{else}
{* GOD MODE *}
<a href="#" class="button large {if !$moderator_block_changes}alert{else}secondary{/if} expand"><i class="icon-legal"></i> GOD MODE</a>
{/if}