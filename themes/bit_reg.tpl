{insert name=seo_clean_titles assign=title value=a title=$posts[i].gtitle}
<div id="dc-{$posts[i].PID}" class="bit-reg deal-card" data-target="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID}/{$title}">
  <div class="bit-img-wrapper">
    <div class="bit-author-wrapper">
      {if $posts[i].profilepicture}
      <img src="{$membersprofilepicurl}/thumbs/A_{$posts[i].profilepicture}" alt="">
      {else}
        <i class="icon-user icon-large"></i>
      {/if}{$posts[i].username}
    </div>
    <a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" rel="nofollow"><img alt="{$posts[i].gtitle|stripslashes|escape:'html'}" src="{$purl}/t2/{$posts[i].p1}" class="bit-deal-img"></a>
  </div>
  <div class="bit-txt">{*$posts[i].PID*}
    <a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}">{$posts[i].gtitle|stripslashes|mb_truncate:50:"...":'UTF-8'}</a>
  </div>
  <div class="bit-bottom">
    <span data-tooltip class="has-tip bit-time" data-width="210" title="{$posts[i].days} {$lang528}"><i class="icon-time"></i> {$posts[i].days}</span>
    <span class="bit-rating{if !$posts[i].rcount} bit-rating-not-rated{/if}">
    {for $s=1 to 5}{if $posts[i].rating >= $s*20}<i class="icon-star"></i>{elseif $posts[i].rating > (($s-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}<small>{if $posts[i].rcount} <span data-tooltip class="has-tip" data-width="210" title="{$posts[i].rcount} {$lang529}">{$posts[i].rcount}</span>{/if}</small></span>
    <span class="bit-price">
      {if $posts[i].price eq "0"}<span class="bit-price-geo bit-price-free">{$lang743}</span>{else}<span class="bit-price-geo">{$lang63}</span>{$posts[i].price|stripslashes}{/if}
    </span>
  </div>
</div>