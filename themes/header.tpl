<!DOCTYPE html>
<html lang=ka>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width">
	<title>{if $mtitle ne ""}{$mtitle}{else}{$site_name}: {if $pagetitle ne ""}{$pagetitle}{/if}{/if}</title>
	{if $ogtitle}<meta property="og:title" content="{$ogtitle|stripslashes|escape:'html'}">
	{/if}
	{if $ogdescription}<meta property="og:description" content="{$ogdescription|replace:"\n":" "|stripslashes|escape:'html'}">
	{/if}
	{if $ogimage}<meta property="og:image" content="{$ogimage}">
	{/if}
	{if $ogurl}<meta property="og:url" content="{$ogurl}">
	{/if}

	<meta name="description" content="{if $mdesc ne ""}{$mdesc|stripslashes|escape:'html'}{else}{if $pagetitle ne ""}{$pagetitle|stripslashes|escape:'html'} - {/if}{if $metadescription ne ""}{$metadescription|stripslashes|escape:'html'} - {/if}{$site_name|stripslashes|escape:'html'}{/if}">
	<meta name="keywords" content="{if $mtags ne ""}{$mtags|stripslashes|escape:'html'}{else}{if $pagetitle ne ""}{$pagetitle|stripslashes|escape:'html'},{/if}{if $metakeywords ne ""}{$metakeywords},{/if}{$site_name|stripslashes|escape:'html'}{/if}">
	<script>
	(function(){
		function createStyle(txt) {
				var style = document.createElement('style');
				style.textContent = txt;
				style.rel = 'stylesheet';
				document.head.insertBefore(style, document.head.firstChild);
		}
		{literal}
		var URL = {'framework':'css/foundation.min.css','staticfonts':'css/geofonts.woff.css','iconfonts':'css/fa.css'};
		var versions = {'framework':5,'staticfonts':3,'iconfonts':1};
		var storedVersions = localStorage['versions'];
		var nRequest = new Array();
		{/literal}

		if (typeof(storedVersions) != 'undefined')
			var pulledVersions = JSON.parse(storedVersions);
		else
			var pulledVersions = new Object();

		for (var i in URL){
			if (typeof(pulledVersions[i])!='undefined' && localStorage[i] && pulledVersions[i]==versions[i])
				createStyle(localStorage[i]);
			else {
				(function(i) {
					nRequest[i] = new XMLHttpRequest();
					nRequest[i].open("GET", '{$baseurl}/'+URL[i]+'?v='+versions[i], true);
					nRequest[i].onreadystatechange = function (oEvent) {
						if (nRequest[i].readyState === 4) {
							if (nRequest[i].status >= 200 && nRequest[i].status < 400) {
								localStorage[i] = nRequest[i].responseText;
								pulledVersions[i] = versions[i];
								localStorage['versions'] = JSON.stringify(pulledVersions);
								createStyle(nRequest[i].responseText);
							} else {
								console.log("Error", nRequest[i].statusText);
							}
						}
					};
					nRequest[i].send(null);
				})(i);
			}
		}
	}());
	</script>
	<link href="{$baseurl}/css/main.css{insert name=file_version value="css/main.css"}" media="screen" rel="stylesheet">
	{if $addCss}
	<link rel="stylesheet" media="screen" href="{$baseurl}/css/{$addCss}.css{insert name=file_version value="css/$addCss.css"}">
	{/if}
	<link rel="apple-touch-icon" sizes="57x57" href="{$baseurl}/images/fav/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="{$baseurl}/images/fav/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="{$baseurl}/images/fav/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="{$baseurl}/images/fav/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="{$baseurl}/images/fav/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="{$baseurl}/images/fav/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="{$baseurl}/images/fav/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="{$baseurl}/images/fav/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="{$baseurl}/images/fav/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="{$baseurl}/images/fav/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="{$baseurl}/images/fav/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="{$baseurl}/images/fav/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="{$baseurl}/images/fav/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="{$baseurl}/images/fav/manifest.json">
	<meta name="msapplication-TileColor" content="#3399cc">
	<meta name="msapplication-TileImage" content="{$baseurl}/images/fav/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<script src="{$baseurl}/js/modernizr.js"></script>
</head>
<body>
{*
<div class="subheading s08 p10 tcenter test-mode-active">
	საიტი β-ტესტირების რეჟიმშია. შეცდომების აღმოჩენის შემთხვევაში, გთხოვთ <a href="{$baseurl}/contact" rel="nofollow" class="gray">მოგვწეროთ</a>.
</div>
*}
<div id="loadme"></div>

<nav class="top-bar{if $main_page AND !$smarty.session.USERID AND !$smarty.cookies['hv']} top-bar-abs{else} top-bar-regular{/if}">
{insert name=get_categories assign=cats}
<div id="snow-X"></div>
	<!-- Fixed Menu -->
	<ul class="title-area{if !$main_page OR $smarty.session.USERID OR $smarty.cookies['hv']} hide{/if}">
		<!-- Title Area -->
		<li class="name hide-for-small">
			<a href="{$baseurl}/" rel="nofollow"><img src="{$baseurl}/images/ido_logo_blue2.png" alt="{$site_name}" class="logo" id="offerTour">{*<img src="{$baseurl}/images/ido_logo2_small.png" alt="{$site_name}" data-interchange="{$baseurl}/images/ido_logo2.png, (default)], [{$baseurl}/images/ido_logo_blue2.png, (large)]" class="logo" id="offerTour">*}</a>
		</li>
		<!-- Mobile Menu -->
		<li class="toggle-topbar menu-icon show-for-small"><a href="#"><span></span></a></li>
		<li class="tb-logo show-for-small"><a href="{$baseurl}/" class="tb-logo-text-img" rel="nofollow"><img src="{$baseurl}/images/ido_text_logo.png" alt="{$site_name}"></a></li>

		<li class="show-for-small top-bar-more-activate pm-dropdown">
			<a href="javascript:" class="q">
				<svg width="4" height="16" viewBox="0 0 4 16" xmlns="http://www.w3.org/2000/svg"><path d="m2 4c1.1 0 2-.9 2-2 0-1.1-.9-2-2-2-1.1 0-2 .9-2 2 0 1.1.9 2 2 2m0 2c-1.1 0-2 .9-2 2 0 1.1.9 2 2 2 1.1 0 2-.9 2-2 0-1.1-.9-2-2-2m0 6c-1.1 0-2 .9-2 2 0 1.1.9 2 2 2 1.1 0 2-.9 2-2 0-1.1-.9-2-2-2" class="white"></path></svg>
			</a>
			<ul class="paper-menu tb-more-pm">
				<li><a href="javascript:" class="close-pm"><i class="icon-long-arrow-left gray"></i></a></li>
				{if !$smarty.session.USERID}
				<li><a href="{$baseurl}/signin" rel=nofollow><i class="icon-lock"></i>{$lang2}</a></li>
				<li><a href="{$baseurl}/signin?signup" rel=nofollow><i class="icon-key"></i>{$lang1}</a></li>
				{/if}
				<li class="pm-context">კატეგორიები</li>
				{section name=i loop=$cats}
				{if $cats[i].CC}
					<li><a href="{$baseurl}/categories/{$cats[i].seo|stripslashes}">{$cats[i].name|stripslashes}</a><span class="pm-f">{$cats[i].CC}</span></li>
				{/if}
				{/section}
				<li class="pm-d hide-for-small"></li>
				<li class="hide-for-small"><a href="{$baseurl}/terms_of_service">{$lang253}</a></li>
				<li class="hide-for-small"><a href="{$baseurl}/contact">{$lang417}</a></li>
				<li class="show-for-small">&nbsp;</li>
			</ul>
		</li>

		<li class="show-for-small top-bar-search-activate"><a href="javascript:" class="search-icon"><svg width="16px" height="16px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path d="m12.5 11h-.8l-.3-.3c1-1.1 1.6-2.6 1.6-4.2 0-3.6-2.9-6.5-6.5-6.5-3.6 0-6.5 2.9-6.5 6.5 0 3.6 2.9 6.5 6.5 6.5 1.6 0 3.1-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5m-6 0c-2.5 0-4.5-2-4.5-4.5 0-2.5 2-4.5 4.5-4.5 2.5 0 4.5 2 4.5 4.5 0 2.5-2 4.5-4.5 4.5" class="white"></path></svg></a>
		</li>
		<li class="tb-fm right hide-for-small"><a href="{$baseurl}/signin?signup" rel=nofollow>{$lang1}</a></li>
		<li class="tb-fm right hide-for-small"><a href="{$baseurl}/signin" rel=nofollow>{$lang2}</a></li>
		<li class="tb-fm right hide-for-small" id="new-deal-btn"><a href="{$baseurl}/new">{$lang55}</a></li>
	</ul>
	<!-- end Fixed Menu -->

	<!-- Floating Menu -->
	<ul class="fixed-title-area{if $main_page AND !$smarty.session.USERID AND !$smarty.cookies['hv']} hide{/if}">
		<li class="toggle-topbar menu-icon"><a href="javascript:" class="q"><span></span></a></li>
		<li class="tb-logo tb-sa-hide"><a href="{$baseurl}/" class="tb-logo-text-img" rel="nofollow"><img src="{$baseurl}/images/ido_text_logo.png" alt="{$site_name}"></a></li>
		<!-- More menu -->
		<li class="top-bar-more-activate pm-dropdown tb-sa-hide" data-target="tb-more-pm">
			<a href="javascript:" class="q">
				<svg width="4" height="16" viewBox="0 0 4 16" xmlns="http://www.w3.org/2000/svg"><path d="m2 4c1.1 0 2-.9 2-2 0-1.1-.9-2-2-2-1.1 0-2 .9-2 2 0 1.1.9 2 2 2m0 2c-1.1 0-2 .9-2 2 0 1.1.9 2 2 2 1.1 0 2-.9 2-2 0-1.1-.9-2-2-2m0 6c-1.1 0-2 .9-2 2 0 1.1.9 2 2 2 1.1 0 2-.9 2-2 0-1.1-.9-2-2-2" class="white"/></svg>
			</a>
			<span class="pm-ar"></span>
			<ul class="paper-menu tb-more-pm">
				<li class="show-for-small"><a href="javascript:" class="close-pm"><i class="icon-long-arrow-left gray"></i></a></li>
				<li class="pm-context hide-for-small">დამატებითი ფუნქციები</li>
				<li class="pm-d hide-for-small"></li>
				{if !$smarty.session.USERID}
				{*<li><a href="{$baseurl}/new"><i class="icon-plus-sign-alt"></i>{$lang55}</a></li>*}
				<li><a href="{$baseurl}/signin" rel=nofollow><i class="icon-lock"></i>{$lang2}</a></li>
				<li><a href="{$baseurl}/signin?signup" rel=nofollow><i class="icon-key"></i>{$lang1}</a></li>
				<li class="pm-d hide-for-small"></li>
				{/if}
				<!-- More Menu / Categories are shown only for mobiles -->
				<li class="pm-context show-for-small">კატეგორიები</li>
				{section name=i loop=$cats}
				{if $cats[i].CC}
					<li class="show-for-small"><a href="{$baseurl}/categories/{$cats[i].seo|stripslashes}">{$cats[i].name|stripslashes}</a><span class="pm-f">{$cats[i].CC}</span></li>
				{/if}
				{/section}
				<!-- More Menu / Additional Functions are shown only for large screens -->
				<li class="hide-for-small"><a href="{$baseurl}/faq">{$lang755}</a></li>
				<li class="hide-for-small"><a href="{$baseurl}/terms_of_service">{$lang253}</a></li>
				<li class="hide-for-small"><a href="{$baseurl}/contact">{$lang417}</a></li>
				<li>&nbsp;</li>
			</ul>
		</li>
		{if $smarty.session.USERID}
		{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$smarty.session.USERID}
		<li class="tb-user-avatar pm-dropdown hide-for-small" data-target="tb-more-pm">
			{if $profilepicture}<a href="javascript:" class="tb-avatar-img-wrapper"><img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width="32" height="32" alt="{$smarty.session.USERNAME|stripslashes}"></a>{else}<a href="javascript:" class="tb-avatar-wrapper"><i class="icon-user icon-large"></i></a>{/if}
			<span class="pm-ar"></span>
			<ul class="paper-menu tb-user-more">
				<li class="pm-context pm-user hide-for-small">{if $profilepicture}<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="{$smarty.session.USERNAME|stripslashes}" class="pm-avatar-img">{else}<div class="pm-avatar-wrapper"><i class="icon-user icon-large"></i></div>{/if} <strong>{$smarty.session.USERNAME|stripslashes}</strong></li>
				<li class="pm-d"></li>
				{insert name=msg_cnt assign=msgc}
				<li><a href="{$baseurl}/inbox"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M2.01 21l20.99-9-20.99-9-.01 7 15 2-15 2z"/>
				<path d="M0 0h24v24h-24z" fill="none"/>
				</svg>{$lang28}<span class="right{if !$msgc} inactive{/if}">{if $msgc>9}{$msgc="9+"}{/if}{$msgc}</span></a></li>
				<li><a href="{$baseurl}/manage_deals"><i class="icon-briefcase icon-large"></i>{$lang750}</a></li>
				<li><a href="{$baseurl}/bookmarks"><i class="icon-bookmark-empty icon-large"></i>{$lang30}</a></li>
				<li><a href="{$baseurl}/user/{$smarty.session.USERNAME|stripslashes}"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="24" viewBox="0 0 20 24">
				<path d="M12 5.9c1.16 0 2.1.94 2.1 2.1s-.94 2.1-2.1 2.1-2.1-.94-2.1-2.1.94-2.1 2.1-2.1m0 9c2.97 0 6.1 1.46 6.1 2.1v1.1h-12.2v-1.1c0-.64 3.13-2.1 6.1-2.1m0-10.9c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z"/>
				<path d="M0 0h24v24h-24z" fill="none"/>
				</svg>{$lang31}</a></li>
				<li><a href="{$baseurl}/balance"><span class="icon-gel">¢</span>{$lang155}</a></li>
				<li><a href="{$baseurl}/settings"><i class="icon-cogs"></i>{$lang29}</a></li>
				<li><a href="{$baseurl}/?logout"><i class="icon-signout icon-large"></i>{$lang27}</a></li>
			</ul>
		</li>
		<li class="tb-notifications pm-dropdown tb-sa-hide" data-target="tb-notifications-pm">{insert name=get_notifications_cc assign=nCC}
			<a href="javascript:" class="tb-notifications-activate q{if $nCC>0} tb-n-active{/if}" title="{if !$nCC}{$lang791} {$lang792}{else}{$nCC} {$lang791}{/if}"><span id="tb-n">{if $nCC>9}9+{else}{$nCC}{/if}</span>
			</a>
			<span class="pm-ar"></span>
			<ul class="paper-menu tb-notifications-pm">
				<li class="p10 tcenter"><svg class="loader spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="circle" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></li>
			</ul>
		</li>
		{/if}

		<li class="top-bar-cats-activate pm-dropdown hide-for-small" data-target="tb-cats-pm">
			<a href="javascript:" class="q">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="hide-for-small"><path d="M4 11h5v-6h-5v6zm0 7h5v-6h-5v6zm6 0h5v-6h-5v6zm6 0h5v-6h-5v6zm-6-7h5v-6h-5v6zm6-6v6h5v-6h-5z" class="white"/></svg>
			</a>
			<span class="pm-ar"></span>
			<ul class="paper-menu tb-cats-pm">
				<li class="show-for-small"><a href="javascript:" class="close-pm"><i class="icon-long-arrow-left gray"></i></a></li>
				<li class="pm-context">კატეგორიები</li>
				<li class="pm-d"></li>
				{section name=i loop=$cats}
				{if $cats[i].CC}
					<li><a href="{$baseurl}/categories/{$cats[i].seo|stripslashes}">{$cats[i].name|stripslashes}</a><span class="pm-f">{$cats[i].CC}</span></li>
				{/if}
				{/section}
				<li>&nbsp;</li>
			</ul>
		</li>
		
		<li class="top-bar-search-activate show-for-small">
			<a href="javascript:" class="search-icon q" rel="nofollow"><svg width="16px" height="16px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path d="m12.5 11h-.8l-.3-.3c1-1.1 1.6-2.6 1.6-4.2 0-3.6-2.9-6.5-6.5-6.5-3.6 0-6.5 2.9-6.5 6.5 0 3.6 2.9 6.5 6.5 6.5 1.6 0 3.1-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5m-6 0c-2.5 0-4.5-2-4.5-4.5 0-2.5 2-4.5 4.5-4.5 2.5 0 4.5 2 4.5 4.5 0 2.5-2 4.5-4.5 4.5" class="white"></path></svg></a>
			<form action="{$baseurl}/search" method="get" class="top-bar-search"><input type=checkbox id="geo_flag" checked=checked class=hide><input type="text" name="query" value="{$smarty.get.query}" class="subheading geo hide-geo-span" placeholder="{$lang524}"></form>
		</li>
		{if !$hideHeaderSearch}
		<li class="tb-s hide-for-small">
			<form action="{$baseurl}/search" method="get" class="rel">
			<i class="icon-search"></i>
			<input type="text" name="query" id="query" class="subheading geo hide-geo-span" placeholder="{$lang524}"></form>
		</li>
		{/if}
	</ul>
</nav>
<!-- Side Bar -->
<div class="side-bar">
{if $smarty.session.USERID}
	<div class="sb-user-wrapper">
		<div class="sb-image">
			{if $profilepicture}<img src="{$membersprofilepicurl}/thumbs/{$profilepicture}" alt="{$smarty.session.USERNAME|stripslashes}">{else}<div class="sb-avatar"><i class="icon-user icon-2x"></i></div>{/if}
		</div>
		<a class="sb-username has-dropdown" data-target="sb-username-dd">{$smarty.session.USERNAME|stripslashes}<b class="caret"></b></a>
		<ul class="dropdown sb-username-dd mt3 subheading">
			<li><a href="{$baseurl}/bookmarks">{$lang30}</a></li>
			<li><a href="{$baseurl}/user/{$smarty.session.USERNAME|stripslashes}">{$lang31}</a></li>
			<li><a href="{$baseurl}/balance">{$lang155}</a></li>
			<li><a href="{$baseurl}/settings">{$lang29}</a></li>
			<li><a href="{$baseurl}/?logout">{$lang27}</a></li>
		</ul>
	</div>
{/if}
	<ul class="mb0">
{if !$smarty.session.USERID}
		<li class="sb-logo"><a href="{$baseurl}/" class="sb-logo-img" rel=nofollow><img src="{$baseurl}/images/ido_logo_blue2.png" alt="{$lang557}"></a></li>
{/if}
		{*<li class="subheading sb-nav mt10"><a href="{$baseurl}/new" rel=nofollow><i class="icon-plus-sign-alt icon-large"></i>{$lang55}</a></li>*}
		{if !$smarty.session.USERID}
		<li class="subheading sb-nav"><a href="{$baseurl}/signin" rel=nofollow><i class="icon-key icon-large"></i>{$lang2}</a></li>
		<li class="subheading sb-nav"><a href="{$baseurl}/signin?signup" rel=nofollow><i class="icon-lock icon-large"></i>{$lang1}</a></li>
		{else}
		{* USER LOGGED IN *}
		<li class="subheading sb-nav"><a href="{$baseurl}/notifications">
		<svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 24 24">
		<path d="M11.5 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6.5-6v-5.5c0-3.07-2.13-5.64-5-6.32v-.68c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68c-2.87.68-5 3.25-5 6.32v5.5l-2 2v1h17v-1l-2-2z"/>
		<path d="M0 0h24v24h-24z" fill="none"/>
		</svg>{$lang795}<span class="right{if !$nCC} inactive{/if}">{$nCC}</span></a></li>
		<li class="subheading sb-nav"><a href="{$baseurl}/inbox">
		<svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 24 24">
		<path d="M2.01 21l20.99-9-20.99-9-.01 7 15 2-15 2z"/>
		<path d="M0 0h24v24h-24z" fill="none"/>
		</svg>{$lang28}<span class="right{if !$msgc} inactive{/if}">{if $msgc>9}{$msgc="9+"}{/if}{$msgc}</span></a></li>
		<li class="subheading sb-nav rel has-dropdown" data-target="sb-cabinet-dd">
			<a href="javascript:"><i class="icon-briefcase icon-large"></i><span class="notify-wrapper notify-wrapper-left{if !$smarty.session.US['new_orders'] OR $resetNotifier} hide{/if}">1</span>{$lang750}</a><b class="caret"></b>
			<ul class="dropdown sb-cabinet-dd">
				<li><a href="{$baseurl}/manage_deals?tab=orders">{$lang158}</a>
				<li><a href="{$baseurl}/manage_deals?tab=deals">{$lang156}</a></li>
				<li><a href="{$baseurl}/manage_deals?tab=pending">{$lang751}</a></li>
			</ul>
		</li>
		{* END USER LOGGED IN *}
		{/if}
		<li class="divider"></li>
		<li class="subheading sb-nav"><a href="{$baseurl}/about"><i class="icon-question icon-large"></i>{$lang522}</a></li>
		<li class="subheading sb-nav"><a href="{$baseurl}/faq"><i class="icon-check icon-large"></i>{$lang755}</a></li>
	</ul>
	<div class="mt30">&nbsp;</div>
</div>
<!-- Side Bar -->
<div class="debug-pan-X"></div>