{insert name=fback value=a assign=fbvl oid=$o.OID}
{if $lasdel GT "0" AND $fbvl eq "0"}
{if $o.status ne "6" AND $o.status ne "2" AND $o.status ne "7" AND $o.status ne "3"}
  <div class="row mt10">
    <div class="large-4 columns tright mt10"><input class="good-review-button complete-subelem" id="rating_value_1" name="ratingvalue" type="radio" value="1"></div>
    <div class="large-2 columns">
    <label for="rating_value_1" class="inline rating-label" data-label-value=1><i class="icon-thumbs-up-alt icon-3x white"></i></label></div>
    <div class="large-1 columns tright mt10"><input class="bad-review-button complete-subelem ml10" id="rating_value_0" name="ratingvalue" type="radio" value="0"></div>
    <div class="large-5 columns mt5"><label for="rating_value_0" class="inline rating-label" data-label-value=0><i class="icon-thumbs-down-alt icon-3x white"></i></label></div>
  </div>
  <span class="s08 block mt10">{$lang311}</span>
  <input type="hidden" name="subrat" value="1"> 
{/if}
{/if}

{if $fbvl eq "1"}
<div class="milestone thumb-down"> 
  <div class="status-label"></div> 
  <div class="mutual-status complete-rating"> 
        <h3>{$lang313}</h3> 
    </div> 
</div>
{/if}
