{if $smarty.session.ADMINID AND $smarty.get.usermode!=1}
<div class="row collapse">
<div class="tcenter">
	<a href="{$baseurl}/track?id={$o.OID}&amp;usermode=1" class="button medium secondary">USER MODE</a>
</div>
<form action="#" class="custom">
	<div class="large-4 columns tcenter ap-vendor-block ap-side">შემსრულებელი:<br>
		{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$o.owner}
		<img alt="" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width="16">
		<a href="{$baseurl}/user/{$o.username}" class="s08">( {$o.username} )</a><br>
		<br>
		<div id="vendor-notification-panel" class="hide button small round secondary"></div>
		<div class="vendor-panel">
			<a href="javascript:" data-mod-space="vendor" data-mod-tool="rating" class="admin-tool-btn button small expand radius subheading"><i class="icon-signal"></i> რეიტინგი +/-</a><br>
			<a href="javascript:" data-mod-space="vendor" data-mod-tool="write" class="admin-tool-btn button small expand radius success subheading"><i class="icon-envelope-alt"></i> მივწეროთ</a><br>
			<a href="javascript:" data-mod-space="vendor" data-mod-tool="block" class="admin-tool-btn button small expand radius alert subheading"><i class="icon-lock"></i> დავბლოკოთ</a>
		</div>
		<div class="vendor-tools hide">
			<div class="tools-close tright"><a href="javascript:" class="tools-close"><i class="icon-remove-sign"></i></a></div>
			<div class="rating tool hide">
				<input type="text" name="vendor-rating" id="vendor-rating" pattern="[0-9]" placeholder="მაგ.: -300"><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
			</div>
			<div class="write tool hide geo-box">
				<textarea name="vendor-write" id="vendor-write" cols="30" rows="10" placeholder="შეიყვანე მისაწერი ტექსტი" class="geo ta10">{$lang730|sprintf:$o.username}</textarea><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
			</div>
			<div class="block tool hide">
				<input type="text" class="mb0" name="vendor-block" id="vendor-block" pattern="[0-9]" placeholder="მაგ.: 7"><span class="subheading mt2 s08">ბლოკირების პერიოდი (დღე)</span><input type="button" class="admin-tools-submit prefix button secondary mt5" value="OK">
			</div>
		</div>
	</div>
	<div class="large-2 columns ap-divider">&nbsp;</div>
	<div class="large-4 columns tcenter ap-buyer-block ap-side">დამკვეთი:<br>
		{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$o.buyerUID}
		<img alt="" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width="16">
		<a href="{$baseurl}/user/{$o.buyer}" class="s08">( {$o.buyer} )</a><br>
		<br>
		<div id="buyer-notification-panel" class="hide button small round secondary"></div>
		<div class="buyer-panel">
			<a href="javascript:" data-mod-space="buyer" data-mod-tool="rating" class="admin-tool-btn button small expand radius subheading"><i class="icon-signal"></i> რეიტინგი +/-</a><br>
			<a href="javascript:" data-mod-space="buyer" data-mod-tool="write" class="admin-tool-btn button small expand radius success subheading"><i class="icon-envelope-alt"></i> მივწეროთ</a><br>
			<a href="javascript:" data-mod-space="buyer" data-mod-tool="block" class="admin-tool-btn button small expand radius alert subheading"><i class="icon-lock"></i> დავბლოკოთ</a>
		</div>
		<div class="buyer-tools hide">
			<div class="tools-close tright"><a href="javascript:" class="tools-close"><i class="icon-remove-sign"></i></a></div>
			<div class="rating tool hide">
				<input type="text" name="buyer-rating" id="buyer-rating" pattern="[0-9]" placeholder="მაგ.: -300"><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
			</div>
			<div class="write tool hide geo-box">
				<textarea name="buyer-write" id="buyer-write" cols="30" rows="10" placeholder="შეიყვანე მისაწერი ტექსტი" class="geo ta10">{$lang730|sprintf:$o.buyer}</textarea><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
			</div>
			<div class="block tool hide">
				<input type="text" class="mb0" name="buyer-block" id="buyer-block" pattern="[0-9]" placeholder="მაგ.: 7"><span class="subheading mt2 s08">ბლოკირების პერიოდი (დღე)</span><input type="button" class="admin-tools-submit prefix button secondary mt5" value="OK">
			</div>
		</div>
	</div>
	<input type="hidden" id="mod-space" value="">
	<input type="hidden" id="mod-tool" value="">
	<input type="hidden" id="vendorUID" value="{$o.owner}">
	<input type="hidden" id="buyerUID" value="{$o.buyerUID}">
	</form>
</div>
{else}
<form action="{$baseurl}/send_track" class="ajaxy mt5 custom" id="new_message" method="post">
	<input type="hidden" name="submg" value="1">
	<input type="hidden" name="msgto" value="{if $who eq "buyer"}{$o.owner}{else}{$o.USERID}{/if}">
	<input type="hidden" name="oid" value="{$o.OID}">
			<div class="msg-error mb10 hide">
				<p id="message_validation_error" class="mb0 red"></p> 
			</div>
			<div class="row collapse">
			{if $o.status eq "4" AND $o.substatus!=1 AND $o.USERID eq $smarty.session.USERID}
			<a href="javascript:" class="message-type label panel-button success subheading ml10 green hide-attach has-form-subelem" data-panel-bg=5da423 data-hc=green data-show=deliveryok-deal-message data-message-format=complete data-ostatus=5><i class="icon-thumbs-up"></i> სამუშაოს მიღება</a>{/if}
			{if $o.status GT "0"}<a href="javascript:" class="message-type reg-msg-tab{if ($o.status ne "4" OR ($o.status eq "4" AND $o.substatus eq "1")) OR $o.USERID ne $smarty.session.USERID} label panel-button{else} ml10{/if} subheading" data-panel-bg=2ba6cb data-hc=primary data-message-format="" data-ostatus={$o.status}>{$lang606}</a>{/if}
			{if $o.USERID ne $smarty.session.USERID AND ($o.status eq "1" OR ($o.status eq "4" AND $o.substatus eq "1") OR $o.status eq "6") AND !$o.pending_request}
			<a href="javascript:" class="message-type success subheading ml10 green has-form-subelem" data-panel-bg=5da423 data-hc=green data-show=complete-deal-message><i class="icon-upload-alt"></i> სამუშაოს ჩაბარება</a>{/if}
			{if $o.status GT "0"}<a href="javascript:" class="message-type alert subheading ml10 red hide-attach has-form-subelem" data-panel-bg=c60f13 data-hc=red data-show=problem-deal><i class="icon-warning-sign"></i> პრობლემის გადაწყვეტა</a>{/if}
			<div class="{if $o.status GT "0"}panel message-panel {if $o.status eq "4" AND $o.substatus!=1 AND $o.USERID eq $smarty.session.USERID}panel-mission-deliveryok{else}panel-mission-message{/if}{/if}">
				{if $o.status eq "4" AND $o.substatus!=1 AND $o.USERID eq $smarty.session.USERID}
					<div class="deliveryok-deal-message white mb10 block-showhide">
					{$lang616}
					{include file="rate_completed_work.tpl"}  
					</div>
				{/if}
				<div class="complete-deal-message white mb10 hide block-showhide">
				{* Deliver the work panel for contributor *}
					<div class="row collapse p10">
						<div class="large-6 columns">
							<div class="row collapse">
								<div class="large-2 columns"><input type="radio" name="deliver-type" id="pre-deliver" class="radio-button" data-text="{$lang761|htmlentities}" data-action-target=delivery data-message-format=predelivery data-ostatus=4></div>
								<div class="large-10 columns"><label for="pre-deliver" class="subheading white">წინასწარი ჩაბარება</label></div>
							</div>
						</div>
						<div class="large-6 columns">
							<div class="row collapse">
								<div class="large-2 columns"><input type="radio" name="deliver-type" id="final-deliver" class="radio-button" data-text="{$lang357}" data-action-target=delivery data-message-format=delivery data-ostatus=4></div>
								<div class="large-10 columns"><label for="final-deliver" class="subheading white">სრული ჩაბარება</label></div>
							</div>
						</div>
					</div>
					<div class="action-desc action-desc-delivery white subheading mt10 mb10"></div>
				</div>
				<div class="problem-deal white mb10 hide block-showhide rel">
					<span class="problem-helper-art" data-reveal-id="need-help"><i class="icon-question-sign icon-large"></i></span>
					<div class="row collapse">
						<div class="large-4 columns">
							<div class="row collapse">
								<div class="large-2 columns"><input type="radio" name="cancel-reason" id="mutual-cancel" class="radio-button" data-text="{if $o.USERID eq $smarty.session.USERID}{if $o.status eq "4"}{$lang354}{else}{$lang599}{/if}{else}{$lang355}{/if}" data-message-format={if $o.USERID eq $smarty.session.USERID AND $o.status eq "4"}rejection data-ostatus=6{else}mutual_cancellation_request{/if} data-action-target=cancel></div>
								<div class="large-10 columns"><label for="mutual-cancel" class="subheading white">{if $o.USERID eq $smarty.session.USERID}{if $o.status eq "4"}{$lang344}{else}{$lang596}{/if}{else}{$lang328}{/if}</label></div>
							</div>
						</div>
						<div class="large-4 columns">
							<div class="row collapse">
								<div class="large-2 columns"><input type="radio" name="cancel-reason" id="personal-cancel" class="radio-button" data-text="{if $o.USERID eq $smarty.session.USERID}{if $o.status eq "4"}{$lang355}{else}{$lang598}{/if}{else}{$lang333}{/if}" data-message-format={if $o.USERID eq $smarty.session.USERID}{if $o.status eq "4"}mutual_cancellation_request{else}buyer_cancellation{/if}{else}seller_cancellation{/if} data-ostatus=3 data-action-target=cancel></div>
								<div class="large-10 columns"><label for="personal-cancel" class="subheading white">{if $o.USERID eq $smarty.session.USERID}{if $o.status eq "4"}{$lang596}{else}{$lang597}{/if}{else}{$lang331}{/if}</label></div>
							</div>
						</div>
						<div class="large-4 columns">
							<div class="row collapse">
								<div class="large-2 columns" id="problem-faq-div"><input type="radio" name="cancel-reason" id="problem-faq" class="problem-faq-radio radio-button hide" data-text="{if $o.USERID eq $smarty.session.USERID}{$lang600}{else}{$lang592}{/if}<br><a href=javascript: data-reveal-id=need-help class=white><u>{$lang335}</u></a>" data-message-format={if $o.USERID eq $smarty.session.USERID}buyer{else}seller{/if}_cancellation_inform_admin data-ostatus=3 data-action-target=cancel></div>
								<div class="large-10 columns"><label for="problem-faq" class="subheading white">შეკვეთის გაუქმება და შეტყობინება ადმინისტრაციას</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="action-desc action-desc-cancel white subheading mb10"></div>
				<div class="geo-box"><textarea cols="75" id="message_body" maxlength="1000" name="message_body" rows="3" class="ta6 subheading geo"></textarea></div>
			</div>
			</div>
				<div class="row collapse attachment-wrapper{if $o.status eq "4" AND $o.USERID eq $smarty.session.USERID} hide{/if}">
					<div class="large-12 small-12 columns">
							<a href="#attach" title="Attach a file to your message" class="upload selected button raised small round subheading" id="toggle-attach">
								<span class="open"><i class="icon-paper-clip"></i> {$lang248}</span>
								<span class="close hide"><i class="icon-remove"></i> {$lang548}</span>
							</a>
							<div id="attachment-container" class=hide>
								<div id="filelist" class="mt10 ml10 subheading"><span class="nofiles red block mb10">ფაილი არ არის ატვირთული</span></div>
								<p style="width: 0%" data-value="0" class="progress-percent hide">&nbsp;</p>
								<progress max="100" value="0" class="progress-tag mb10 hide">
									<span class="progress-bar">
											<span style="width: 0%;">პროგრესი: 0%</span>
									</span>
								</progress>
								<a id="pickfiles" href="javascript:" class="button medium primary radius heading"><i class="icon-upload icon-large"></i> მისამაგრებელი ფაილის არჩევა</a> 
								{* <a id="uploadfiles" href="javascript:" class="button small success radius heading"><i class="icon-upload icon-large"></i> ატვირთვა</a> *}
								<h5 class="subheading upload-header">{$lang248} <b>({$lang249})</b></h5>
							</div>
							<input id="attachment_id" name="attachment_id" type="hidden" value="">
							<!--input id="fileInput" name="fileInput" type="file" class=hide-->
							<p class="terms subheading hide">{$lang250}<br>
									<b>{$lang251}</b>: {$lang252} (<a href="{$baseurl}/terms_of_service" target="_blank" title="{$lang253}">{$lang254}</a>)<br>
									<b>{$lang255}</b>: {$lang256}
							</p>
					</div>
				</div>
			<input id="message_format" name="message_format" type="hidden"{if $o.status eq "4" AND $o.USERID eq $smarty.session.USERID} value="complete"{/if}> 
			<input type="hidden" name="who" value="{$who}">
			<input type="hidden" id="ostatus" name="ostatus" value="{if $o.status eq "4" AND $o.substatus!=1 AND $o.USERID eq $smarty.session.USERID}5{else}{$o.status}{/if}" data-initial-ostatus="{$o.status}">
			<div class="form-error-notice subheading red mt10 hide">{$lang566}</div>
			<div class="message-form-control tcenter">
				<input type="submit" value="{$lang46}" class="send-button success button large subheading radius mt10">
				<div class="progress-indicator-icon-message hide">
					{include file="loader.tpl"}
				</div>
			</div>
</form>
{/if}