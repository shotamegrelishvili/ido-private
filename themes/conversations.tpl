<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/inbox" class=current>{$lang226}</a>
	</ul>
</div>

<div class="work-area-wrapper">
<div class="row">
	{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div id="se-message" class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
	<div class="large-12 small-12 columns">
		<h1 class="heading h-large mt20 mb0">{$lang226}</h1>
		<h2 class="subheading">{$lang235}: <strong class="h-large"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$u.username|stripslashes}">{$u.username|stripslashes}</a></strong></h2>
	</div>
</div>

<div class="row">
{if $m|@count GT "0"}
	<form action="{$baseurl}/inbox" method="post" name="arform" id="arform">                               
	<input type="hidden" name="auid" value="{$u.USERID}">
	<input type="hidden" name="subarc" value="1">
	</form>
	<div class="large-8 small-8 columns subheading">
		&bull; {$lang237} {$m[0].time|date_format}<br>
		&bull; <span id="conversation_update_info">
		{$lang238} <strong>{math assign="t1" equation="x-1" x=$m|@count}{$m[$t1].mfrom|stripslashes}</strong>
		</span >
	</div>
	<div class="large-4 small-4 columns tright subheading">
		<a href="#" onclick="document.arform.submit()" class="button small radius alert"><i class="icon-remove-sign"></i> {$lang236}</a>
	</div> 
{/if}
</div>

<div class="row work-area work-area-pad">
<div class="row mt20{if $m|@sizeof EQ "0"} hide{/if}" id="msgresults">
	{if $m|@sizeof GT "0"}
	{section name=i loop=$m}
		{if $m[i].unread eq "1"}
		{if $smarty.session.USERID eq $m[i].MSGTO}
		{insert name=mark_read value=a mid=$m[i].MID}
		{/if}
		{/if}
		<div id="mid{$m[i].MID}" class="{if $smarty.session.USERID eq $m[i].MSGFROM}message by-me{else}message by-partner{/if}">
			<div class="row metadata">
				<div class="large-1 small-2 columns tcenter">
					<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">
					{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].MSGFROM}{if !$profilepicture}<i class="show-for-small icon-user icon-large gray"></i><i class="hide-for-small icon-user icon-large gray"></i><i class="icon-user icon-large gray"></i>{else}<span class="show-for-small avatar ml10 mt2"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width=24 height=24></span><span class="hide-for-small avatar avatar-2x ml10 mt2"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width=24 height=24></span>{/if}</a>
				</div>     
				<div class="large-2 small-2 columns inb-user-from"><a href="{$baseurl}/user/{$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><strong>{$m[i].mfrom|stripslashes}</strong></a></div>
				<div class="large-9 small-8 columns content-text subheading">
					{if $m[i].PID GT "0"}
						{insert name=gig_details value=a assign=gml pid=$m[i].PID}
						{section name=z loop=$gml}
						{insert name=seo_clean_titles assign=ztitle value=a title=$gml[z].gtitle}
						<p class="related-gig">
							<span>{$lang239} <a href="{$baseurl}/{$gml[z].seo|stripslashes}/{$gml[z].PID|stripslashes}/{$ztitle}">{$lang62} {$gml[z].gtitle|stripslashes} {$lang63}{$gml[z].price|stripslashes}</a></span>
						</p>
						{/section}
						{/if}
						
						<p{if $m[i].FID GT "0"} class=mb0{/if}>{$m[i].message|stripslashes|nl2br}</p>
						{if $m[i].FID GT "0"}
							{insert name=file_details value=a assign=fd fid=$m[i].FID}
							{section name=x loop=$fd}
							{insert name=gfs value=a assign=afs fid=$fd[x].FID}
							<div class="attached-files">
								<ul>
									<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
								</ul>
							</div>
							{/section}
						{/if}
						<div class="msg-footer">
								{if $m[i].MSGFROM ne $smarty.session.USERID}
								<a href="javascript:void(0)" data-href="{$baseurl}/spam?id={$m[i].MID}" class="spam-message" id="spam_message={$m[i].MID}">{$lang245}</a> &bull;
								{/if}
								<time datetime="{$m[i].time|date_format:"%H"}" class="gray s08">{$m[i].time|date_format:"%H:%M %b %d, %Y"}</time>
						</div>
				</div>
		</div>
	</div>
		{/section}  
	{/if}
</div>

	{insert name=get_member_profilepicture assign=mppc value=var USERID=$smarty.session.USERID}
<form action="{$baseurl}/sendmessage.php" class="new_message message-form-control" id="new_message" method="post">
	<div class="row collapse mt20">
		<div class="large-12 small-12 columns respond">
			<input type="hidden" name="submg" value="1">
			<input type="hidden" name="msgto" value="{$u.USERID}">
			<input type="hidden" name="aboutid" value="{$aboutid}">
			<div class="row do-dont do-dont-title">
				<div class="large-12 small-12 columns">მიმოწერისას გაითვალისწინე:</div>
			</div>
			<div class="row do-dont">
				<div class="large-5 small-6 columns do-dont-txt">
					გამარჯობა, წერილს მივამაგრე ლოგოს ის ვარიანტი, რომელიც მომწონს სტილისტურად. გთხოვთ აქვე მიპასუხოთ, შეძლებთ თუ არა ლოგოს ასეთ სტილში დამზადებას.
				</div>
				<div class="large-5 large-offset-2 small-6 columns do-dont-txt">
					ჩემი ნომერია: 529 123456. დამირეკეთ ან მომწერეთ მეილზე: mevar@email.com
				</div>
			</div>
			<div class="row do-dont">
				<div class="large-5 small-6 columns do-dont-do">
					<div class="do-dont-title">მისაღებია</div>
					აწარმოე მიმოწერა საიტის საშუალებით და მეორე მხარესაც მოთხოვე საიტითვე გიპასუხოს.
				</div>
				<div class="large-5 large-offset-2 small-6 columns do-dont-dont">
					<div class="do-dont-title">დაუშვებელია</div>
					კომუნიკაციისათვის არ გამოიყენო სხვა საშუალებები (მეილი, ტელეფონი და სხვა).
				</div>
			</div>
			
			<h3 class="heading mt20">{$lang546} {$u.username}{$lang547}: <span class="subheading fwn right hide-for-small">{$lang738}</span></h3>

			<div class="msg-error mb10 subheading red hide">
				<span id="message_validation_error"></span>
			</div>

			<div class="geo-box"><textarea cols="75" id="message_body" maxlength="1200" name="message_body" rows="3" class="ta6 geo has-do-dont">{$smarty.post.message_body}</textarea></div>
		</div>
	</div>

	<div class="row collapse">
		<div class="large-12 small-12 columns">
				<a href="#attach" title="Attach a file to your message" class="upload selected button raised small waves-effect subheading" id="toggle-attach">
					<span class="open"><i class="icon-paper-clip"></i> {$lang248}</span>
					<span class="close hide"><i class="icon-remove"></i> {$lang548}</span>
				</a>
				<div id="attachment-container" class=hide>
					<div id="filelist" class="mt10 ml10 subheading"><span class="nofiles block mb10">ფაილი არ არის ატვირთული</span></div>
					<p style="width: 0%" data-value="0" class="progress-percent s08 hide">&nbsp;</p>
					<progress max="100" value="0" class="progress-tag mb10 hide">
						<div class="progress-bar">
								<span style="width: 0%;">პროგრესი: 0%</span>
						</div>
					</progress>
					<a id="pickfiles" href="javascript:;" class="button medium success radius heading"><i class="icon-upload icon-large"></i> ფაილის მიმაგრება</a> 
					{* <a id="uploadfiles" href="javascript:;" class="button small success radius heading"><i class="icon-upload icon-large"></i> ატვირთვა</a> *}
					<h5 class="subheading upload-header">{$lang248} <b>({$lang249})</b></h5>
				</div>
				<input id="attachment_id" name="attachment_id" type="hidden" value="">
				<!--input id="fileInput" name="fileInput" type="file" class=hide-->
				<p class="terms subheading hide">{$lang250}<br>
						<b>{$lang251}</b>: {$lang252} (<a href="{$baseurl}/terms_of_service" target="_blank" title="{$lang253}">{$lang254}</a>)<br>
						<b>{$lang255}</b>: {$lang256}
				</p>
		</div>
	</div>
	<div class="row collapse">
		<div class="large-12 small-12 tcenter columns">
			<input type="submit" value="{$lang549}" class="button raised large success radius subheading send-button">
			<div class="progress-indicator-icon-message hide">
        {include file="loader.tpl"}
    </div>
		</div>
	</div>
</form>
</div>
</div>

<script>
	var USERID = '{$u.USERID}';
	var last_id = {$lastm};
	var conversationWith = '{$u.username|stripslashes}';
	var lang242 = '{$lang242}';
	var lang243 = '{$lang243}';
	var scrollTo = '{$smarty.get.mid}';

	var def_js = ['{$baseurl}/js/conversations.php?v=5'];
</script>