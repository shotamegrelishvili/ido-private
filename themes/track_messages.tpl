{section name=i loop=$m}
	{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].MSGFROM}
	{if $m[i].start eq "1"}
		<div id="mid{$m[i].MID}" class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
			<div class="large-2 columns mt10"><i class="icon-download-alt icon-3x green"></i></div>
			<div class="large-10 columns subheading mt10"><span class="gray">ინსტრუქციის მიხედვით გაგზავნილი ინფორმაცია:</span>
			<textarea name="instr_answer" rows="6" class="ta6 mt5" disabled>{$m[i].message|stripslashes}</textarea>
			{if $m[i].FID GT "0"}
			{insert name=file_details value=a assign=fd fid=$m[i].FID}
			{section name=x loop=$fd}
			{insert name=gfs value=a assign=afs fid=$fd[x].FID}
			<div class="attached-files">
				<ul>
					<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
				</ul>
			</div>
			{/section}
			{/if}
			<p class="mt5 mb0">{$lang276} <b>{insert name=get_deadline value=a assign=deadline days=$o.days time=$m[i].time}{$deadline}</b></p>
			</div>
		</div>
		<div class="separator mt10"></div>
	{elseif $m[i].action eq "mutual_cancellation_request"}
			{if $m[i].MSGFROM eq $smarty.session.USERID}
			<div id="mid{$m[i].MID}" class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
				<div class="large-10 columns subheading mt10"><span class="gray"> {if $o.USERID eq $smarty.session.USERID}{$lang601}{else}{$lang286}{/if}</span>
				<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>

							{if $m[i].cancel eq "0"}
							<i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{$lang602}{else}{$lang291}{/if}
							<form name="abort{$m[i].MID}" method="post">
							<input type="hidden" name="subabort" value="1">
							<input type="hidden" name="AMID" value="{$m[i].MID}">
							</form>
							<div class="panel">{$lang289}<br><a href="#" onclick="document.abort{$m[i].MID}.submit()">{if $o.USERID eq $smarty.session.USERID}{$lang603}{else}{$lang290}{/if}</a></div>
							{/if}
				</div>
			</div>
			<div class="separator"></div>
			{else}
			<div id="mid{$m[i].MID}" class="row" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns mt10"><i class="icon-question-sign icon-3x red"></i></div>
				<div class="large-10 columns subheading mt10"><span class="gray">{if $o.USERID eq $smarty.session.USERID}{$lang288}{else}{$lang294}{/if}</span>
				<p><strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}</p>
					{if $m[i].cancel eq "0"}
					<form name="decline{$m[i].MID}" method="post">
					<input type="hidden" name="subdecline" value="1">
					<input type="hidden" name="DMID" value="{$m[i].MID}">
					</form>
					<form name="accept{$m[i].MID}" method="post">
					<input type="hidden" name="subaccept" value="1">
					<input type="hidden" name="AMID" value="{$m[i].MID}">
					</form>
					<div class="tcenter">
						<a href="javascript:" onclick="document.accept{$m[i].MID}.submit()" class="button small radius subheading ml10 alert"><i class="icon-remove"></i> {if $o.USERID eq $smarty.session.USERID}{$lang609}{else}{$lang301}{/if}</a>
						<a href="javascript:" onclick="document.decline{$m[i].MID}.submit()" class="button small radius secondary subheading"><i class="icon-rotate-left"></i> {if $o.USERID eq $smarty.session.USERID}{$lang610}{else}{$lang296}{/if}</a>
					</div>
					{/if}
					{if $m[i].cancel ne "1" AND $o.status ne "2"}
					<div class="panel mt10"><i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{$lang594}{else}{$lang604}{/if}</div>
					{/if}
				</div>
			</div>
			<div class="separator"></div>
			{/if}
			{if $m[i].cancel eq "1"}
			<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns"><i class="icon-rotate-left icon-3x green"></i></div>
				<div class="large-10 columns subheading">{if $m[i].CID eq $smarty.session.USERID}შენი გადაწყვეტილებით{elseif $m[i].CID eq $o.USERID}დამკვეთის გადაწყვეტილებით{else}შემსრულებლის გადაწყვეტილებით{/if} {if $m[i].CID eq $smarty.session.USERID AND $o.USERID ne $smarty.session.USERID}{$lang297}{elseif $o.USERID ne $smarty.session.USERID}{$lang297}{else}{$lang595}{/if}</div>
			</div>
			<div class="separator"></div>
			{elseif $m[i].cancel eq "2"}
			<div class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns"><i class="icon-remove-sign icon-3x red"></i></div>
				<div class="large-10 columns subheading">
					{if $o.USERID eq $smarty.session.USERID}{$lang303}{else}{$lang302}{/if}
				</div>
			</div>
			{/if}
	{elseif $m[i].action eq "seller_cancellation" OR $m[i].action eq "buyer_cancellation" OR $m[i].action eq "seller_cancellation_inform_admin" OR $m[i].action eq "buyer_cancellation_inform_admin"}
	<div id="mid{$m[i].MID}" class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-remove-sign icon-3x red"></i></div>
		<div class="large-10 columns mt5 subheading"><span class="gray">{if $m[i].MSGFROM eq $smarty.session.USERID}{$lang305}{elseif $o.USERID eq $m[i].MSGFROM}{$lang608}{else}{$lang304}{/if}{if $m[i].action eq "seller_cancellation_inform_admin" OR $m[i].action eq "buyer_cancellation_inform_admin"}<br>{$lang614}{/if}</span>
		<p>
			<strong>{$lang287}:</strong> {$m[i].message|stripslashes|nl2br}
		</p>
		</div>
	</div>
	{elseif $m[i].action eq "rejection"}
	<div id="mid{$m[i].MID}" class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-remove-sign red icon-3x"></i></div>
		<div class="large-10 columns mt5 subheading"><span class="gray">{if $o.USERID eq $smarty.session.USERID}{$lang321}{else}{$lang323}{/if}:</span>
			<p>{$m[i].message|stripslashes|nl2br}</p>
		</div>
	</div>
	<div class="separator"></div>
	{elseif $m[i].action eq "delivery"}
	<div id="mid{$m[i].MID}" class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-gift green icon-3x"></i></div>
		<div class="large-10 columns subheading">
			{if $profilepicture}<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}">{else}<i class="icon-user gray"></i>{/if} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
			<div class="subheading mt10{if $o.status eq "4"} panel success-panel{/if}">
				<span class="gray block mb10">{if $o.USERID eq $smarty.session.USERID}{$lang308}{else}{$lang306}{/if}</span>
				{$m[i].message|stripslashes|nl2br}<br>
				{if $m[i].FID GT "0"}
				{insert name=file_details value=a assign=fd fid=$m[i].FID}
				{section name=x loop=$fd}
				{insert name=gfs value=a assign=afs fid=$fd[x].FID}
				<div class="attached-files mt10">
					<ul>
						<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
					</ul>
				</div>
				{/section}
				{/if}
			</div>
			{if $m[i].MID eq $lasdel AND $m[i].reject eq "0"}
			<span class="subheading gray">
				{if $o.status ne "2" AND $o.status ne "7" AND $o.status ne "3" AND $o.status ne "5"}{if $o.USERID eq $smarty.session.USERID}{$lang307}<span class="block tright mt5"><i class="icon-time"></i> {$m[i].time|date_format:"%b %d, %Y %R"}</span>{else}{$lang615}{/if}{/if}
			</span> 
			{/if}
		</div>
	</div>
	<div class="separator mt5"></div>
	{elseif $m[i].action eq "predelivery"}
	<div id="mid{$m[i].MID}" class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-question-sign red icon-3x"></i></div>
		<div class="large-10 columns subheading">
			{if $profilepicture}<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}">{else}<i class="icon-user gray"></i>{/if} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
			<div class="subheading mt10">
				<div class="gray block mb10">{if $o.USERID eq $smarty.session.USERID}{if $m[i].MID eq $laspredel}{$lang768}{else}{$lang770}{/if}{elseif $m[i].MID eq $laspredel}{$lang767}{else}{$lang769}{/if}</div>
				<strong class="block">{$lang545}:</strong>
				<p>{$m[i].message|stripslashes|nl2br}</p>
				{if $m[i].FID GT "0"}
				{insert name=file_details value=a assign=fd fid=$m[i].FID}
				{section name=x loop=$fd}
				{insert name=gfs value=a assign=afs fid=$fd[x].FID}
				<div class="attached-files">
					<ul>
						<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank" class="s08">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
					</ul>
				</div>
				{/section}
				{/if}
			</div>
			{if $o.USERID eq $smarty.session.USERID AND $m[i].MID eq $laspredel AND $m[i].reject eq "0" AND !$m[i].cancel}
				<form name="decline{$m[i].MID}" method="post">
					<input type="hidden" name="subdecline" value="1">
					<input type="hidden" name="DMID" value="{$m[i].MID}">
					</form>
					<form name="accept{$m[i].MID}" method="post">
					<input type="hidden" name="subaccept" value="1">
					<input type="hidden" name="AMID" value="{$m[i].MID}">
				</form>
				<div class="tcenter mt10">
					<a href="javascript:" onclick="if (confirm('{$lang774}')) document.accept{$m[i].MID}.submit()" class="button medium radius success ml10 subheading"><i class="icon-check"></i> {$lang772}</a>
					<a href="javascript:" onclick="if (confirm('{$lang773}')) document.decline{$m[i].MID}.submit()" class="button medium radius subheading alert"><i class="icon-remove"></i> {$lang771}</a>
				</div>
			{/if}
			{if $m[i].cancel ne "0"}
				<div class="panel mt10{if $m[i].cancel eq '2'} success-panel{/if}"><i class="icon-info-sign gray"></i> {if $o.USERID eq $smarty.session.USERID}{if $m[i].cancel eq '1'}{$lang775}{if $m[i].MID eq $laspredel}<br>{$lang779}{/if}{elseif $m[i].cancel eq '2'}{$lang776}{/if}{else}{if $m[i].cancel eq '1'}{$lang777}{elseif $m[i].cancel eq '2'}{$lang778}{/if}{/if}</div>
			{/if}
		</div>
	</div>
	<div class="separator mt5"></div>
	{elseif $m[i].action eq "complete"}
	<div id="mid{$m[i].MID}" class="row mt10" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-thumbs-{if $r.bad}down red{elseif $r.good}up green{/if} icon-3x"></i></div>
		<div class="large-10 columns subheading">
			<div class="panel success-panel">
				<span class="gray">{$lang619}{if $r.comment} {$lang620}{/if}</span>
				<p class="mt5">{$r.comment|stripslashes|nl2br}</p>
			</div>
			{if $o.status eq "5"}<span class="block mb20 subheading gray">{$lang319} {$o.cltime|date_format:"%d/%m/%Y"}</span>{/if}
		</div>
	</div>
	{elseif $m[i].action eq "admin_message"}
	<div id="mid{$m[i].MID}" class="row mt10" title="Admin ({$m[i].time|date_format:"%b %d, %Y %R"})">
		<div class="large-2 columns"><i class="icon-legal icon-3x red"></i></div>
		<div class="large-10 columns subheading">
			<div class="panel success-panel">
				<p>{$m[i].message|stripslashes|nl2br}</p>
			</div>
			{if $o.status eq "5"}<span class="subheading gray">{$lang319} {$o.cltime|date_format}</span>{/if}
		</div>
	</div>
	<div class="separator"></div>
	{else}
	{* GENERAL CONVERSATION *}
			<div id="mid{$m[i].MID}" class="row by-{if $m[i].MSGFROM eq $smarty.session.USERID}me{else}partner{/if}" title="{$m[i].mfrom|stripslashes} ({$m[i].time|date_format:"%b %d, %Y %R"})">
				<div class="large-2 columns mt10">
					<span class="track-avatar">
						<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}"></a>
					</span>                        
				</div>
				<div class="large-10 columns mt10 subheading">
							<a href="{$baseurl}/user/{$m[i].mfrom|stripslashes}">{$m[i].mfrom|stripslashes}</a>
								<p>{$m[i].message|stripslashes|nl2br}</p>
							{if $m[i].FID GT "0"}
							{insert name=file_details value=a assign=fd fid=$m[i].FID}
							{section name=x loop=$fd}
							{insert name=gfs value=a assign=afs fid=$fd[x].FID}
							<div class="attached-files">
								<ul>
									<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
								</ul>
							</div>
							{/section}
							{/if}
			</div>
		</div>
		<div class="separator"></div>
	{/if}
{/section}