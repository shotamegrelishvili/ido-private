{insert name=get_error_message assign=session_error}
{insert name=get_success_message assign=session_success}
{if $session_error ne "" AND $error eq ""}{assign var=error value=$session_error}{/if}
{if $session_success ne "" AND $message eq ""}{assign var=message value=$session_success}{/if}
{if $error ne ""}
<div class="msg-error flash-error">
	<h2 class=subheading>{$error}</h2>
</div>
{/if}
{if $message ne ""}
<div class="msg-success flash-notice">
    <h2 class=subheading><i class="icon-check icon-large"></i> {$message}</h2>
</div>
{/if}
