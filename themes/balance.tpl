<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/balance" class=current>{$lang205}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang205}</h1>
    <h2 class="subheading">{$lang206}</h2>
  </div>
  {if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
</div>

<div class="row collapse mt20">
  <div class="large-8 small-7 columns subheading filters">
    <ul>
      <li><a href="javascript:" class="balance label radius tabControlSales">ბალანსი</a></li>
      <li><a href="javascript:" class="activity radius tabControlSales">აქტივობა</a></li>
    </ul>
  </div>
  <div class="large-4 small-5 columns subheading tright">
    {*{insert name=wdreq value=a assign=wreqnt}
    <a href="#" class="button small radius wd-button {if $afunds eq "0" OR $wreqnt GT "0"}secondary{else}success{/if}"{if $afunds eq "0" OR $wreqnt GT "0"} disabled{/if}{if $wreqnt eq "0" AND $afunds ne "0"} onclick="toggle_wdsection(this)"{/if}>{if $wreqnt GT "0"}{$lang396}{else}{$lang212}{/if}</a>*}
  </div>
</div>
<div class="row collapse panel balance-section">
  <div class="row collapse">
    <div class="large-6 small-6 columns">
      <div class="row">
        <div class="large-1 columns round-box hide-for-small"><i class="icon-check-sign"></i></div>
        <div class="large-11 columns ph10">
          <h3 class=heading><span class="label success mr15">{$funds} {$lang197}</span>{$lang213}</h3>
          <span class="subheading s08">{$lang214}</span>
        </div>
      </div>
    </div>
    <div class="large-6 small-6 columns">
      <div class="row">
        <div class="large-1 columns round-box hide-for-small"><i class="icon-circle"></i></div>
        <div class="large-11 columns ph10">
          <h3 class=heading><span class="label success mr15">{$upcoming} {$lang197}</span>{$lang217}</h3>
          <span class="subheading s08">{$lang218}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="row collapse">
    <div class="large-6 small-6 columns mt20">
      <div class="row">
        <div class="large-1 columns round-box hide-for-small"><i class="icon-rotate-right"></i></div>
        <div class="large-11 columns ph10">
          <h3 class=heading><span class="label success mr15">{$awaiting_clearance} {$lang197}</span>{$lang194}</h3>
          <span class="subheading s08">{$lang394}</span>
        </div>
      </div>
    </div>
    <div class="large-6 small-6 columns mt20">
      <div class="row">
        <div class="large-1 columns round-box hide-for-small"><i class="icon-credit-card"></i></div>
        <div class="large-11 columns ph10">
          <h3 class=heading><span class="label success mr15">{$paid_sum} {$lang197}</span>{$lang221}</h3>
          <span class="subheading s08">{$lang222}</span>
        </div>
      </div>
    </div>
  </div>
  </div>
  

{if $afunds ne "0"}
{if $wreqnt eq "0"}
<form name="wdfrm" id="wdfrm" method="post" class="wd-section row panel hide">
  <div class="row tcenter">
    <h3 class="heading">მოგების განაღდების (თანხის გატანის) მოთხოვნა</h3>
    <span class="subheading">მოგების განაღდების მოთხოვნა მუშავდება ხელით, მოთხოვნიდან შემდეგი თვის 15 რიცხვამდე.</span>
  </div>
  <div class="mt20"></div>
  <div class="row">
    <div class="large-4 large-centered columns">
      <input type="text" name="wdfunds" value="{$afunds}" class="mb0">
      <span class="subheading s08">მინ.: 10 {$lang197}, მაქს.: {$afunds} {$lang197}</span>
    </div>
  </div>
  <div class="row">
    <div class="large-4 mt10 large-centered columns">
      {insert name=get_emoney_account value=a assign=eemail}
      <input type="radio" name="wfgateway" checked{if !$eemail} disabled{/if}> <span class="subheading{if !$eemail} gray{/if}">eMoney-ს ანგარიშზე</span>
      {if !$eemail}<a href="{$baseurl}/settings" class="subheading red"><i class="icon-cogs"></i> თანხის გასატანად მიუთითე eMoney-ს ანგარიში</a>{/if}
    </div>
  </div>
  <div class="row">
    <div class="large-4 large-centered columns mt20">
      <input type="submit" value="თანხის გატანის მოთხოვნა" class="button medium {if $eemail}success{else}secondary{/if} subheading expand radius"{if !$eemail} disabled{/if}>
    </div>
  </div>
</form>
{/if}
{/if}

<div class="row activity-section hide">
  <h2 class="heading">{if $o|@count eq "0"}{$lang209}{else}{$lang379}{/if}</h2>
  {if $o|@count eq "0"}                    
  <p class="subheading mt20">
    {$lang210}<br>
    <a href="{$baseurl}/">{$lang211}</a>
  </p>
  {else}
    <div class="row collapse mt20">
      <div class="large-1 columns subheading s08 table-head tcenter">{$lang110}</div>
      <div class="large-1 columns tcenter s08 table-head">#</div>
      <div class="large-6 columns table-head s08">&nbsp;</div>
      <div class="large-3 columns subheading s08 table-head">&nbsp;</div>
      <div class="large-1 columns tcenter s08 table-head"><span class="icon-gel">¢</span></div>
    </div>
    {section name=i loop=$o}
    {insert name=get_gtitle value=a assign=gtitle oid=$o[i].OID}
    <div class="row collapse table-row-hover subheading">
      <div class="large-1 columns tcenter s08">{insert name=get_time_to_days_ago value=a time=$o[i].time}</div>
      <div class="large-1 columns tcenter s08">{if $gtitle eq ""}#{$o[i].OID}{else}<a href="{$baseurl}/track?id={$o[i].OID}">#{$o[i].OID}</a>{/if}</div>
      <div class="large-6 columns subheading s08">{if $gtitle eq ""}{$lang384}{else}{$gtitle|stripslashes}{/if}</div>
      {if !$o[i].cancel AND !$o[i].delayed_cancel}
      <div class="large-3 columns tcenter" title="{$lang385}: {$o[i].price} {$lang197}"><span class="label subheading radius">{$lang387}</span></div>
      {elseif $o[i].cancel}
      <div class="large-3 columns tcenter" title="{$lang386}: {$o[i].price} {$lang197}"><span class="label subheading radius alert">{$lang388}</span></div>
      {else}
      <div class="large-3 columns tcenter" title="{$lang386}: {$o[i].price} {$lang197}"><span class="label subheading radius secondary">{$lang203}</span></div>
      {/if}
      <div class="large-1 columns tcenter s08">{$o[i].price|string_format:"%01.2f"} {$lang523}</div>
    </div>
    {/section}
  {/if}
</div>


{if $p|@count GT "0"}
<div id="undergoing-deals">
<div class="row mt20">
  <div class="large-1 columns subheading s08 table-head tcenter">{$lang110}</div>
  <div class="large-1 columns tcenter s08 table-head">#</div>
  <div class="large-6 columns table-head s08">&nbsp;</div>
  <div class="large-3 columns subheading s08 table-head">&nbsp;</div>
  <div class="large-1 columns tcenter s08 table-head"><span class="icon-gel">¢</span></div>
</div>
  {section name=i loop=$p}
  <div class="row table-row-hover">
  {if $p[i].IID GT "0"}
  {insert name=get_ctp value=a assign=myctp IID=$p[i].IID}
  {insert name=get_yprice value=a assign=yprice p=$p[i].price c=$myctp}
  {else}
  {insert name=get_yprice value=a assign=yprice p=$p[i].price c=$p[i].ctp}
  {/if}
  {insert name=get_gtitle value=a assign=gtitle oid=$p[i].OID}
    <div class="large-1 columns tcenter s08">{insert name=get_time_to_days_ago value=a time=$p[i].time}</div>
    <div class="large-1 columns tcenter s08">{if $gtitle eq ""}#{$p[i].OID}{else}<a href="{$baseurl}/track?id={$p[i].OID}">#{$p[i].OID}</a>{/if}</div>
    <div class="large-6 columns subheading s08">{if $gtitle eq ""}{$lang384}{else}{$gtitle|stripslashes}{/if}</div>
  {if $p[i].status eq "5"}
    {insert name=get_days_withdraw value=a assign=days_awaiting_clearance t=$p[i].cltime}
    {if $days_awaiting_clearance GT "0" AND !$p[i].transfer_complete}
    <div class="large-3 columns tcenter subheading" title="{$days_awaiting_clearance} {$lang391}">{$lang392} &nbsp;<strong>{$days_awaiting_clearance}</strong> {$lang131}</div>
    <div class="large-1 columns tcenter subheading">{$yprice}{$lang197}</div>
    {else}
    <div class="large-3 columns tcenter" title="{$lang393}"><span class="label subheading radius">{$lang393}</span></div>
    <div class="large-1 columns tcenter s08">{$yprice}</div>
  {/if}
  {elseif $p[i].status eq "2" OR $p[i].status eq "3" OR $p[i].status eq "7"}
  <div class="large-3 columns" title="{$lang390}">{$lang203} {$lang197}0</div>
  {elseif $p[i].status eq "4"}
  <div class="large-3 columns" title="{$lang140}">{$lang201}{$lang197}{$yprice}</div>
  {else}
  <div class="large-3 columns tcenter" title="{$lang590}"><span class="label subheading radius">{$lang586}</span></div>
  <div class="large-1 columns tcenter s08">{$yprice}</div>
  {/if}
  </div>
  {/section}
</div>
<div class="mt20"></div>
{/if}


<script>
  var editform = true;
  var lang = [];
  lang['make_wd'] = '{$lang212}';
  lang['close_form'] = '{$lang580}';
  var def_js = ['{$baseurl}/js/balance.js'];
</script>


{*
            <script src="{$baseurl}/js/jquery.qtip-1.0.0-rc3.js" type="text/javascript"></script>
            <script src="{$baseurl}/js/balance.js" type="text/javascript"></script>

			  <div class="main-wrapper">
                <div id="main">
                  <div class="content">
                  {if $message ne ""}
                  {include file="error.tpl"}
                  {/if}
					
                    <div class="page-title">
                    	<h2>{$lang205} ({$lang197}{$funds})<b class="balance">{$lang206}</b></h2>
                    </div>
                    <div class="tabs">
                        <h2 class="richer"><span>{$lang207} <b>{$lang197}{$overall}</b></span></h2>
                        <div class="tabSet">
                          <div class="tabControlShopping {if $smarty.request.tab ne "sales"}selected{/if}"><span>{$lang33} </span></div>
                          <div class="tabControlSales {if $smarty.request.tab eq "sales"}selected{/if}"><span>{$lang208} </span></div>
                        </div>
                        <div id="tabs-shopping" class="tabShopping yellow tabs index" style="{if $smarty.request.tab eq "sales"}display:none;{/if}">
                          <div class="info">
                              {if $o|@count eq "0"}{$lang209}{else}<b>{$o|@count}</b> {$lang379}{/if}
                          </div>    
                          {if $o|@count eq "0"}                    
                          <div class="stats shopping">
                            <div class="notice">
                              <p>{$lang210} <a href="{$baseurl}/">{$lang211}</a></p>
                            </div>
                          </div>
                          {else}
                          	<div class="table-container"> 
                            <table width="100%"> 
                              <thead class="topics icons"> 
                                <tr> 
                                  <td class="date first">{$lang110}</td> 
                                  <td class="order">{$lang140}</td> 
                                  <td class="statuses"></td> 
                                  <td class="statuses"></td> 
                                  <td class="amount last">{$lang389}</td> 
                                </tr> 
                              </thead> 
                              <tbody> 
                              	{section name=i loop=$o}
                                {insert name=get_gtitle value=a assign=gtitle oid=$o[i].OID}
                                <tr class="entry"> 
                                  <td class="first" align="left"><div>{insert name=get_time_to_days_ago value=a time=$o[i].time}</div></td>  
                                  <td class="id">{if $gtitle eq ""}#{$o[i].OID}{else}<a href="{$baseurl}/track?id={$o[i].OID}">#{$o[i].OID}</a>{/if} </td> 
                                  <td class="gig"><div>{if $gtitle eq ""}{$lang384}{else}{$gtitle|stripslashes}{/if}</div></td> 
                                  {if $o[i].t eq "1"}
                                  <td class="status payment" title="{$lang385}: {$lang197}{$o[i].price}"><div>{$lang387}</div></td>
                                  {else}
                                  <td class="status reversal" title="{$lang386}: {$lang197}{$o[i].price}"><div>{$lang388}</div></td> 
                                  {/if}
                                  <td class="gross last" width="5px"><div>{$lang197}{$o[i].price}</div></td> 
                                </tr> 
                                {/section}                     
                              </tbody> 
                            </table> 
                            <div class="sep"></div> 
                          </div> 
                          {/if}
                        </div>  
                        
                        <div id="tabs-sales" class="tabSales green tabs index" style="{if $smarty.request.tab ne "sales"}display:none;{/if}">
                            <div class="info">                            
                                <span class="left">{if $p|@count GT "0"}<b>{$p|@count}</b> {$lang379}{else}{$lang209}{/if}</span><span class="{if $afunds eq "0" OR $wreqnt GT "0"}disabled{/if} right"><strong><a href="#" {if $afunds ne "0"}onclick="toggle('wdsection');"{/if}>{if $wreqnt GT "0"}{$lang396}{else}{$lang212}{/if}</a></strong></span>
	                        </div>
                            {literal}
                            <script language="javascript" type="text/javascript">
							function toggle(obj) {
								var el = document.getElementById(obj);
								if ( el.style.display != 'none' ) {
									$('#' + obj).slideUp();
								}
								else {
									$('#' + obj).slideDown();
								}
							}
							</script>
                            <style>
							.info2{border:1px solid #DCDCCA;background:#DCDCCA;border-bottom:1px solid #CCCCBA!important;color:#5C5C5A;font-weight:bold;text-shadow:none;margin:0 0 10px;padding:10px 10px 10px 13px;}
							.info2{border:1px solid #E4ECEE;background:#E4ECEE;color:#577;font-weight:bold;text-shadow:#eff 1px 1px;border-bottom:1px solid #D4DCDE!important;margin:0 0 10px;}
							.info2{overflow:hidden;margin:0!important;padding:5px 5px 5px 13px;}
							.info2 span.right{float:right;background:#008dcd;border:1px solid #bcc;padding:4px 6px;}
							.info2 span.right a{text-decoration:none;text-shadow:none;color:#fff;}
							.info2 span.right:hover a{color:#008dcd;}
							.info2 span.right:hover{background:#fff;border:1px solid #008dcd;padding:4px 6px;}
							.info2 span.right2{float:right;background:#390;border:1px solid #bcc;padding:4px 6px;margin-left:5px;}
							.info2 span.right2 a{text-decoration:none;text-shadow:none;color:#fff;}
							.info2 span.right2:hover a{color:#390;}
							.info2 span.right2:hover{background:#fff;border:1px solid #390;padding:4px 6px;}
							.info2 span.right em{font-style:normal;}
							.info2 span.right.disabled{background:#fafafa;border:1px solid #dadada;cursor:text!important;padding:4px 6px;}
							.info2 span.right.disabled a{cursor:text!important;color:#aaa;}
							.info2 span.right.disabled:hover{border:1px solid #dadada!important;}
							</style>
                            {/literal}
                            <div class="info2" id="wdsection" style="display:none;">                            
                                {if $enable_alertpay eq "1"}
                                <span class="{if $afunds eq "0" OR $wreqnt GT "0"}disabled{/if} right2"><strong><a href="#" {if $afunds ne "0"}onclick="document.wdfrm2.submit();"{/if}>{if $wreqnt GT "0"}{$lang396}{else}{$lang451}{/if}</a></strong></span>
                                {/if}
                                {if $enable_paypal eq "1"}
                                <span class="{if $afunds eq "0" OR $wreqnt GT "0"}disabled{/if} right"><strong><a href="#" {if $afunds ne "0"}onclick="document.wdfrm.submit();"{/if}>{if $wreqnt GT "0"}{$lang396}{else}{$lang450}{/if}</a></strong></span>
                                {/if}
	                        </div>                            
                          	<div class="stats">
                            	<div class="pane">
                              		<h3 class="available"><b>{$lang197}{$afunds}</b>{$lang213}<span>{$lang214}</span></h3>
                              		<h3 class="pending"><b>{$lang197}{$app}</b>{$lang194}<span>{$lang394}</span></h3>
                              		<h3 class="upcoming right"><b>{$lang197}{$clr}</b>{$lang215}<span>{$lang216}</span></h3>
                            	</div>
                            	<div class="pane last">
                                	<h3 class="upcoming"><b>{$lang197}{$upcoming}</b>{$lang217}<span>{$lang218}</span></h3> 
                              		<h3 class="withdrawn"><b>{$lang197}{$withdrawn}</b>{$lang219}<span>{$lang220}</span></h3>
                                	<h3 class="paid-by"><b>{$lang197}{$used}</b> {$lang221}<span>{$lang222}</span></h3> 
                            	</div>
                          	</div> 
                            {if $p|@count GT "0"} 
                            <div class="table-container"> 
                            <table width="100%"> 
                              <thead class="topics icons"> 
                                <tr> 
                                  <td class="date first">{$lang110}</td> 
                                  <td class="order">{$lang140}</td> 
                                  <td class="statuses"></td> 
                                  <td class="statuses"></td> 
                                  <td class="amount last">{$lang389}</td> 
                                </tr> 
                              </thead> 
                              <tbody> 
                              	{section name=i loop=$p}
                                {if $p[i].IID GT "0"}
                                {insert name=get_ctp value=a assign=myctp IID=$p[i].IID}
                                {insert name=get_yprice value=a assign=yprice p=$p[i].price c=$myctp}
                                {else}
                                {insert name=get_yprice value=a assign=yprice p=$p[i].price c=$p[i].ctp}
                                {/if}
                                {insert name=get_gtitle value=a assign=gtitle oid=$p[i].OID}
                                <tr class="entry"> 
                                  <td class="first" align="left"><div>{insert name=get_time_to_days_ago value=a time=$p[i].time}</div></td>  
                                  <td class="id">{if $gtitle eq ""}#{$p[i].OID}{else}<a href="{$baseurl}/track?id={$p[i].OID}">#{$p[i].OID}</a>{/if} </td> 
                                  <td class="gig"><div>{if $gtitle eq ""}{$lang384}{else}{$gtitle|stripslashes}{/if}</div></td> 
                                  
                                {if $p[i].status eq "5"}
                                	{insert name=get_days_withdraw value=a assign=wd t=$p[i].cltime}
                                	{if $wd GT "0"}
	                                <td class="status clearing &nbsp;<u>{$wd}</u>" title="{$wd} {$lang391}"><div>{$lang392} &nbsp;<u>{$wd}</u></div></td>
                                    <td class="gross last" width="5px"><div>{$lang197}{$yprice}</div></td>
                                    {else}
                                    	{if $p[i].wd eq "1"}
                                        <td class="status withdrawn" title="{$lang397}"><div>{$lang397}</div></td>
                                        {else}
                                        <td class="status cleared" title="{$lang393}"><div>{$lang393}</div></td>
                                        {/if}
                                        <td class="gross last" width="5px"><div>{$lang197}{$yprice}</div></td>
                                    {/if}
                                {elseif $p[i].status eq "2" OR $p[i].status eq "3" OR $p[i].status eq "7"}
                                <td class="status cancelled" title="{$lang390}"><div>{$lang203}</div></td>
                                <td class="gross last" width="5px"><div>{$lang197}0</div></td>
                                {elseif $p[i].status eq "4"}
                                <td class="status completed" title="{$lang140}"><div>{$lang201}</div></td>
                                <td class="gross last" width="5px"><div>{$lang197}{$yprice}</div></td>
                                {else}
                                <td class="status withdrawal" title="{$lang362}"><div>{$lang362}</div></td>
                                <td class="gross last" width="5px"><div>{$lang197}{$yprice}</div></td>
                                {/if}
                                </tr> 
                                {/section}                     
                              </tbody> 
                            </table> 
                            <div class="sep"></div> 
                          </div>
                          {/if}                   
                        </div>
                    </div>
                  </div>
                  {include file="side2.tpl"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      *}