<form action="{$baseurl}/suggested" id="suggest_form" method="post">                        
<div class="row">
{if !$hide_jobs}
  <div class="large-6 large-offset-2 columns mt10"><span class="heading shadowed-title">დამსაქმებელი ეძებს სამუშაოს შემსრულებელს:</span></div>
{/if}
  <div class="large-6 columns mt10"><span class="heading">{$lang525}</span></div>
</div>
<div class="row">
{if !$hide_jobs}
  <div class="large-6 large-offset-2 columns mt10">
    <div class="recent-suggestions rel">
      {insert name=get_wants value={$catselect} assign=wants}
      <ul class="subheading"{if sizeof($wants)>1} data-orbit data-options="timer_speed:3500;navigation_arrows:false;slide_number:false;timer:false;resume_on_mouseout:true"{/if}>
        {section name=i loop=$wants}
        <li><span class="user-wants"> <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$wants[i].username|stripslashes}">{$wants[i].username|stripslashes}</a> {$lang122}:</span><br/>{$wants[i].want|stripslashes}
            <br/>                                
        </li>
        {/section}
      </ul>
      <a href="{$baseurl}/suggested?jobseeker=1" class="heading all-suggested-btn tiny button radius" id="suggest-btn">ეძებ სამუშაოს?</a>
    </div>
  </div>
{/if}
  <div class="{if !$hide_jobs}large-4{else}large-6 large-centered{/if} columns mt10">
    <label class="subheading{if !$hide_jobs} s08{/if}">{$lang117}</label>
    <div class="geo-box">
    <textarea class="suggestion-box geo{if !$suggest_page} hide-geo-span{/if} ta6-anim mt5" cols="20" id="suggestion_content" maxlength="250" name="sugcontent" rows="3" placeholder="ჩაწერე სამუშაოს აღწერა აქ. ეცადე მოკლედ და გასაგებად აღწერო დავალება.{if $suggest_page} (მაქს. 250 სიმბოლო){/if}"></textarea></div>
    <div class="suggest-category-select{if !$suggest_page} hide{/if}">
      <div class="subheading">{$lang119}</div>
      <div class="category mt5">
        <select id="suggestion_category_id" name="sugcat" class="subheading">
        <option value="">-- აირჩიე კატეგორია --</option>
        {section name=i loop=$cats}
        <option value="{$cats[i].CATID|stripslashes}">{$cats[i].name|stripslashes}</option>
        {/section}
        </select>
      </div>
      <input type="submit" value="{$lang118}" class="button {if !$suggest_page}tiny{else}large{/if} heading right">
    </div>
    <input type="hidden" name="sugsub" value="1">
  </div>
</div>
</form>