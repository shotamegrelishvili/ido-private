<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/contact" class=current>{$lang537}</a>
  </ul>
</div>
{if !$smarty.session.ADMINID}
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=592711610773734&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{/if}

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang537}</h1>
    {if !$smarty.session.ADMINID}<h2 class="subheading">{$lang710}</h2>{/if}
  </div>
  {if $message ne "" OR $error ne ""}<div class="large-12 small-12 columns mt20">{include file="error.tpl"}</div>{/if}
</div>

{if !$smarty.session.ADMINID}
<div class="row mt20">
  <div class="large-7 small-12 columns">
  <p class="subheading">აირჩიე საკითხის თემატიკა და დეტალურად აღწერე ის. საკითხის სირთულიდან გამომდინარე ჩვენ გიპასუხებთ 1-5 სამუშაო დღის განმავლობაში.</p>
    <form action="{$baseurl}/contact" method="post" name="contactform" id="contactform" class=custom>
      {if !$smarty.session.USERID}
      <label for="email" class="subheading">ელფოსტა</label>
      <input type="email" name="email" class="subheading" value="" placeholder="you@email.com">
      {/if}
      <select name="contactcase" id="contactcase">
        <option value="">{$lang711}</option>
        <option value="1"{if $smarty.request.contactcase eq 1} selected{/if}>{$lang712}</option>
        <option value="2"{if $smarty.request.contactcase eq 2} selected{/if}>{$lang713}</option>
        <option value="3"{if $smarty.request.contactcase eq 3} selected{/if}>{$lang714}</option>
        <option value="10"{if $smarty.request.contactcase eq 10} selected{/if}>{$lang715}</option>
      </select>
      <div class="geo-box">
        <textarea name="contacttext" id="contacttext" class="geo ta10" cols="30" rows="10" placeholder="აღწერე საკითხი დეტალურად">{$smarty.post.contacttext}</textarea>
      </div>
      <div class="tcenter mt20">
        <input type="text" name="sdetect" class="hidden-field" value="">
        <input type="hidden" name="subform" value="1">
        <input type="submit" class="button large heading" value="წერილის გაგზავნა">
      </div>
    </form>
  </div>
  <div class="large-5 tcenter hide-for-small columns">
    <a href="https://www.facebook.com/iDoGeorgia" target=_blank><i class="icon-facebook-sign icon-3x"></i><br><span class="subheading">დაგვიმეგობრდი Facebook-ის გვერდზე</span></a><br>
    <div class="fb-like mt20" data-href="https://www.facebook.com/iDoGeorgia" data-layout="box_count" data-action="like" data-show-faces="true" data-share="true"></div>
  </div>

</div>
{else}
{* IS ADMIN *}
<div class="row mt20">
  <div class="large-1 columns table-head">&nbsp;</div>
  <div class="large-3 columns table-head"><strong class="subheading">{$lang36}</strong></div>
  <div class="large-3 columns table-head"><strong class="subheading">{$lang719}</strong></div>
  <div class="large-3 columns table-head"><strong class="subheading">{$lang720}</strong></div>
  <div class="large-2 columns table-head"><i class="icon-reply"></i> <strong class="subheading">{$lang725}</strong></div>
</div>
  {section name=i loop=$crows}
<div class="row contact-row table-row-hover" id="row-{$crows[i].id}">
  <div class="large-1 columns tcenter"><a href="javascript:" class="message-remove" data-mid="{$crows[i].id}"><i class="icon-trash"></i></a></div>
  <div class="large-3 columns"><a href="{$baseurl}/user/{$crows[i].username}" data-tooltip class="has-tip" id="user-{$crows[i].id}" data-email="{$crows[i].email}" title="{$crows[i].datetime|substr:0:16}">{$crows[i].username}</a></div>
  <div class="large-3 columns"><span class="subheading s08">{$caseLangArray[$crows[i].caseid]}</span></div>
  <div class="large-3 columns"><span class="subheading s08 has-tip" data-tooltip id="text-{$crows[i].id}" title="{$crows[i].casetext|escape:'html'|stripslashes}" data-title="{$crows[i].casetext|escape:'html'|stripslashes}">{$crows[i].casetext|mb_truncate:25:"..":"UTF-8"}</span></div>
  <div class="large-2 columns"><a href="javascript:" class="message-reply" data-mid="{$crows[i].id}"><i class="icon-reply"></i> <span class="subheading">{$lang725}</span></a></div>
  <form action="{$baseurl}/contact" method="post" name="replyform" id="replyform-{$crows[i].id}" class="custom replyforms hide">
  <div class="reply-form row collapse mt10 tcenter">
    <a href="javascript:" class="close-replyform" data-mid="{$crows[i].id}"><i class="icon-remove-sign right"></i></a>
    <div class="large-8 large-offset-4 columns geo-box">
      <textarea name="contacttext" id="reply-text-{$crows[i].id}" cols="30" rows="10" class="ta15 geo"></textarea>
    </div>
  </div>
  <div class="row tcenter">
    <div class="large-8 large-offset-4 columns">
      <input type="hidden" name="replyto" value="{$crows[i].id}">
      <input type="submit" value="წერილის გაგზავნა" class="button medium success heading">
    </div>
  </div>
  </form>
</div>
  {/section}
</div>

<script>
  var baseurl = '{$baseurl}';
  var replytemplate = '{$lang726}';

  var def_js = ['{$baseurl}/js/jquery.scrollTo-min.js','{$baseurl}/js/contact.js?v=2'];
</script>
{/if}