{* FOR BUYER *}
<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/track?id={$smarty.get.id}" class=current>{if $o.status eq "0"}{$lang584}{elseif $o.status eq "1" OR $o.status eq "6"}{$lang589}{elseif $o.status eq "2" OR $o.status eq "3"}{$lang203} შეკვეთა{elseif $o.status eq "4"}{$lang201} შეკვეთა{elseif $o.status eq "5"}{$lang202} შეკვეთა{/if}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-{if $smarty.session.ADMINID}8{else}12{/if} small-12 columns">
    <h1 class="heading h-large mb0">{if $o.status eq "0"}{$lang585}{elseif $o.status eq "1" OR $o.status eq "6"}{$lang589}{elseif $o.status eq "2" OR $o.status eq "3"}{$lang203} შეკვეთა{elseif $o.status eq "4"}{$lang201} შეკვეთა{elseif $o.status eq "5"}{$lang202} შეკვეთა{/if}</h1>
    <h2 class="subheading">{if $o.status eq "0"}{$lang584}{elseif $o.status eq "1" OR $o.status eq "6"}{$lang590}{elseif $o.status eq "2"}{$lang605} შეკვეთა{elseif $o.status eq "3"}{$lang607} შეკვეთა{elseif $o.status eq "4"}{$lang617}{elseif $o.status eq "5"}{$lang618}{/if}</h2>
  </div>
  {if $smarty.session.ADMINID}<div class="large-4 columns tright"><a href="{$baseurl}/track?id={$o.OID}{if !$smarty.get.usermode}&amp;usermode=1{/if}" class="button medium {if $smarty.get.usermode==1}secondary{else}alert{/if}">{if $smarty.get.usermode==1}USER{else}GOD{/if} Mode</a></div>{/if}
  {if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
</div>

{if $o.status eq "0"}
<div class="row">
    <div class="large-12 small-12 columns">
      <div class="row s08">
          <div class="large-4 columns bg-green br-white">&nbsp;</div>
          <div class="large-4 columns br-white bg-green">&nbsp;</div>
          <div class="large-4 columns bg-green">&nbsp;</div>
      </div>
    </div>
</div>
{/if}

<div class="work-area-wrapper mt20">
  <div class="row work-area">
    <div class="large-4 columns">
      <!--Left Panel-->
      <div class="row"><a href="{$baseurl}/manage_deals?tab=orders" class="subheading s08">&laquo; {$lang261}</a></div>
      <div class="row subheading">
        <div class="large-5 columns mt20"><strong>შეკვეთა</strong></div>
        <div class="large-7 columns mt20"><strong>#{$o.OID}</strong></div>
      </div>
      <div class="separator mt5"></div>
      <div class="row subheading">
        <div class="large-5 columns mt10">სტატუსი</div>
        <div class="large-7 columns mt10"><span class="ostatus-label label radius {if $o.status eq "0"}alert{elseif $o.status eq "2" OR $o.status eq "3"}secondary{elseif $o.status eq "4" OR $o.status eq "5"}success{/if}">{if $o.status eq "0" OR $o.status eq "6"}{$lang586}{elseif $o.status eq "1"}{$lang279}{elseif $o.status eq "2" OR $o.status eq "3"}{$lang203}{elseif $o.status eq "4"}{$lang201}{elseif $o.status eq "5"}{$lang202}{/if}</span></div>
      </div>
      <div class="separator mt5"></div>
      <div class="row subheading">{insert name=seo_clean_titles assign=title value=a title=$o.gtitle}
        <div class="large-5 columns mt10"><img src="{$purl}/t2/{$o.p1}" alt="{$title}"></div>
        <div class="large-7 columns mt10"><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$o.username|stripslashes}" class="s08"><img src="{$membersprofilepicurl}/thumbs/A_{$o.profilepicture}" width=24 height=24 alt="{$o.username|stripslashes}"> <strong>{$o.username|stripslashes}</strong></a><br>
        <a href="{$baseurl}/conversations/{$o.username|stripslashes}" class="button secondary tiny radius heading expand mt5"><i class="icon-envelope-alt"></i> დაკავშირება</a><br>
        <span class="s08">ბოლო ვიზიტი: {insert name=get_past_period assign=past_period value=var t=$o.lastlogin}<strong>{$past_period}</strong></span>
        </div>
      </div>
      <div class="separator mt5"></div>
      <div class="row subheading">
        <div class="large-12 columns mt10 s08">
          {if $o.days == "0"}<i class="icon-time"></i> სამუშაოს შესრულების ვადა: <i class="icon-rocket icon-large green"></i> {$lang530}</span>{elseif $o.days == "1"}<i class="icon-time blue"></i> {$lang492}{else}<i class="icon-time"></i> სამუშაოს შესრულების სავარაუდო ვადა: <strong>{$o.days} დღე</strong>{/if}
        </div>
      </div>
      <div class="separator mt5"></div>
      <div class="row tcenter subheading mt10">
        <a href="{$baseurl}/agreement?order={$o.OID}" class="button empty radius s08"><i class="icon-file-text"></i> ხელშეკრულება მომსახურებაზე</a>
      </div>
      <!--Left Panel end-->
    </div>
    <div class="large-7 large-offset-1 columns">
      <!--Central Panel-->
      <p class=mb0><a href="{$baseurl}/{$o.seo|stripslashes}/{$o.PID|stripslashes}/{$title}" class="heading h-large">{$o.gtitle|stripslashes}</a>... <span class="subheading{if $o.price eq "0"} red{/if}">{if $o.price ne "0"}{$o.price|stripslashes} {$lang63}{else}{$lang743}{/if}</span></p>
      <div class="separator"></div>
      <div class="row subheading s08">
        <div class="large-8 columns mt5">
          {$lang263} <strong>{if $o.USERID eq $smarty.session.USERID}შენს მიერ{elseif $smarty.session.ADMINID}<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$o.buyer|stripslashes}">{$o.buyer|stripslashes}</a>-ს მიერ{else}<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$o.username|stripslashes}">{$o.username|stripslashes}</a>-ს მიერ{/if}</strong> {insert name=get_past_period assign=order_time value=var t=$o.time_added precise=1}{$order_time} ({$o.time_added|date_format})
        </div>
        <div class="large-4 columns mt5 tright">
          <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$o.username|stripslashes}">მიმოწერა <b>{$o.username|stripslashes}</b>-თან</a>
        </div>
      </div>
      <div class="row">
        <div class="large-2 columns mt20"><i class="icon-check green icon-3x"></i></div>
        <div class="large-10 columns mt20 subheading">
          {$lang588}
        </div>
      </div>
      <div class="row">
        <div class="large-2 columns mt20"><i class="icon-file-text {if $o.status eq "0"}red{else}gray{/if} icon-3x"></i></div>
        <div class="large-10 columns mt20 subheading">
          {if $o.owner ne $smarty.session.USERID}{$lang270}{/if}
          {include file='track_ship.tpl'}
          <textarea name="instr" class="ta{if $o.ginst|mb_strlen > 300 AND $o.status eq "0"}10{else}6{/if} subheading mt10" disabled>{$o.ginst|stripslashes}</textarea>
        </div>
      </div>
      {*if $o.status eq "1" OR $o.status eq "4" OR $o.status eq "6"}
          {include file="track_bit2.tpl"}
          {elseif $o.status eq "0"}
          
          {/if *}
      <div class="row tcenter"><i class="icon-arrow-down icon-large ostatus-icon {if $o.status eq "0"}red{else}gray{/if}"></i></div>
      <div id="msgarray">
        {insert name=last_delivery value=a assign=lasdel oid=$o.OID}
        {insert name=last_predelivery value=a assign=laspredel oid=$o.OID}
        {include file="track_messages.tpl"}
      </div>
      {if $o.status eq "0" OR $o.status eq "1" OR $o.status eq "4" OR $o.status eq "6"}
      <div class="row" id="communication-form">
        <div class="large-2 columns mt10">
        {if $smarty.session.ADMINID}<i class="icon-legal icon-3x red"></i>
        {else}
          {insert name=get_member_profilepicture assign=profilepicture value=var USERID=$smarty.session.USERID}
          {if $profilepicture}<span class="block track-avatar"><img src="{$membersprofilepicurl}/thumbs/{$profilepicture}" alt="{$smarty.session.USERNAME|stripslashes}"></span>{else}<i class="icon-user icon-3x gray"></i>{/if}
        {/if}
        </div>
        <div class="large-10 columns mt10 subheading"><span class="contact-form-title">{if $o.status eq "0"}{$lang274}{/if}</span>
          {assign var=who value="buyer"}
          {include file="track_bit.tpl"}
        </div>
      </div>
      {/if}
      
      <!--Central Panel end-->
    </div>
  </div>
</div>

<script>
  var baseurl = '{$baseurl}';
  var OID = '{$smarty.get.id}';
  var USERID = '{$u.USERID}';
  var lang358 = '{$lang358}';
  var lang606 = '{$lang606}';
  var lang202 = '{$lang202}';
  var lang203 = '{$lang203}';
  var lang371 = '{$lang371}';
  var lang586 = '{$lang586}';
  var lang593 = '{$lang593}';
  var lang627 = '{$lang627}';
  var lang766 = '{$lang766}';
  var errorArr = {literal}{{/literal}message_already_sent: '{$lang731}'{literal}}{/literal};
  var langCloseConfirm = '{if $o.no_moneyback}{$lang780}{else}{$lang623}{/if}';
  var langRejectConfirm = '{$lang702}';
  var mid = '{if $smarty.get.mid}{$smarty.get.mid}{/if}';

  var def_js = ['{$baseurl}/js/track.js?v=5'{if $smarty.session.ADMINID},'{$baseurl}/js/track_admin.js?v=4'{/if}];
</script>

{if $o.status eq "1"}
<div id="need-help" class="reveal-modal subheading">
  <a class="close-reveal-modal"><i class="icon-remove-sign icon-2x"></i></a>
  <h2 class="heading">დაკვეთის შესრულებისას წარმოქმნილი პრობლემების გადაჭრის რჩევები</h2>
  <h3 class="subheading p10 mt10 bg-gray">შემსრულებელი არ გამოდის კავშირზე, ან აგვიანებს სამუშაოს შესრულებას</h3>
  თუკი შემსრულებელი არ გამოდის კავშირზე, მაგრამ სამუშაოს შესრულების ბოლო ვადა ჯერ შორსაა, შეგიძლია მოითხოვო შეკვეთის გაუქმება ურთიერთშეთანხმებით. თუ შემსრულებელი არ მოახდენს რეაგირებას, შეკვეთა ავტომატურად გაუქმდება მოთხოვნიდან {$working_process.mutual_cancelation_period_days} დღეში. თუკი, შემსრულებელი აგვიანებს სამუშაოს შესრულებას, დაელოდე დათქმული ვადის ამოწურვას, შეკვეთა ავტომატურად გაუქმდება ვადის გასვლიდან X დღეში და შემსრულებლის რეიტინგი დაიკლებს. გადახდილი თანხა სრულად დაგიბრუნდება eMoney-ს ანგარიშზე, რის შესახებაც მიიღებ შეტყობინებას.
  <h3 class="subheading p10 mt10 bg-gray">შემსრულებელმა მონიშნა სამუშაო როგორც ჩაბარებული, თუმცა არ მოუწოდებია სამუშაოს შედეგი</h3>
  დაუყონებლივ განაცხადე უარი შესრულებული სამუშაოს მიღებაზე, დაუკავშირდი შემსრულებელს და მოთხოვე სამუშაოს შედეგის მოწოდება.
  <h3 class="subheading p10 mt10 bg-gray">შესრულებული სამუშაო არ შეესაბამება განცხადებაში აღწერილს, ან უხარისხოა.</h3>
  განაცხადე უარი შესრულებული სამუშაოს მიღებაზე, დაუკავშირდი შემსრულებელს, დააზუსტე რა არ მოგწონს და თხოვე გამოასწოროს ხარვეზი. თუ {$working_process.max_reject_times+1} მცდელობის მიუხედავად, სამუშაოს შედეგი მიუღებელია, შეკვეთა გაუქმდება და საიტის ადმინისტრაცია გადაწყვეტს მონაწილე მხარეების რეიტინგის საკითხს. გადახდილი თანხა სრულად დაგიბრუნდება eMoney-ს ანგარიშზე.
  <h3 class="subheading p10 mt10 bg-gray">შემსრულებელი გთხოვს მიიღო სამუშაო, რომელიც ჯერ არ ჩაუბარებია?</h3>
  დაუყონებლივ განაცხადე უარი სამუშაოს მიღებაზე და გვაცნობე ასეთი ფაქტის შესახებ "შეკვეთის გაუქმება და შეტყობინება ადმინისტრაციას" ოპციის საშუალებით. საიტის ადმინისტრაცია გადაწყვეტს დამნაშავე მხარეს რეიტინგის საკითხს ან/და სადამსჯელო ღონისძიებას.
  <h3 class="subheading p10 mt10 bg-gray">აღწერილი რჩევებიდან არცერთი არ მიგაჩნია მისაღებად ანდა სამართლიანად?</h3>
  <span class="mt5">შეგიძლია გააუქმო შეკვეთა და შეგვატყობინო მიზეზი და სხვა დეტალები "შეკვეთის გაუქმება და შეტყობინება ადმინისტრაციას" ოპციის საშუალებით. საიტის ადმინისტრაცია გადაწყვეტს დამნაშავე მხარეს რეიტინგის საკითხს ან/და სადამსჯელო ღონისძიებას.</span>
</div>
{elseif $o.status eq "4"}
<div id="need-help" class="reveal-modal subheading">
  <a class="close-reveal-modal"><i class="icon-remove-sign icon-2x"></i></a>
  <h2 class="heading">{$lang336}</h2>
  <h3 class="subheading p10 mt10 bg-gray">შემსრულებელმა მონიშნა სამუშაო როგორც ჩაბარებული, თუმცა არ მოუწოდებია სამუშაოს შედეგი</h3>
  დაუყონებლივ განაცხადე უარი შესრულებული სამუშაოს მიღებაზე, დაუკავშირდი შემსრულებელს და მოთხოვე სამუშაოს შედეგის მოწოდება.
  <h3 class="subheading p10 mt10 bg-gray">შესრულებული სამუშაო არ შეესაბამება განცხადებაში აღწერილს, ან უხარისხოა.</h3>
  განაცხადე უარი შესრულებული სამუშაოს მიღებაზე, დაუკავშირდი შემსრულებელს, დააზუსტე რა არ მოგწონს და თხოვე გამოასწოროს ხარვეზი. თუ {$working_process.max_reject_times+1} მცდელობის მიუხედავად, სამუშაოს შედეგი მიუღებელია, შეკვეთა გაუქმდება და საიტის ადმინისტრაცია გადაწყვეტს მონაწილე მხარეების რეიტინგის საკითხს. გადახდილი თანხა სრულად დაგიბრუნდება eMoney-ს ანგარიშზე.
  <h3 class="subheading p10 mt10 bg-gray">შემსრულებელი გთხოვს მიიღო სამუშაო, რომელიც ჯერ არ შეუსრულებია?</h3>
  დაუყონებლივ განაცხადე უარი სამუშაოს მიღებაზე და გვაცნობე ასეთი ფაქტის შესახებ "შეკვეთის გაუქმება და შეტყობინება ადმინისტრაციას" ოპციის საშუალებით. საიტის ადმინისტრაცია გადაწყვეტს დამნაშავე მხარეს რეიტინგის საკითხს ან/და სადამსჯელო ღონისძიებას.
  <h3 class="subheading p10 mt10 bg-gray">აღწერილი რჩევებიდან არცერთი არ მიგაჩნია მისაღებად ანდა სამართლიანად?</h3>
  <span class="mt5">შეგიძლია გააუქმო შეკვეთა და შეგვატყობინო მიზეზი და სხვა დეტალები "შეკვეთის გაუქმება და შეტყობინება ადმინისტრაციას" ოპციის საშუალებით. საიტის ადმინისტრაცია გადაწყვეტს დამნაშავე მხარეს რეიტინგის საკითხს ან/და სადამსჯელო ღონისძიებას.</span>
</div>
{/if}