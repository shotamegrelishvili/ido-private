{insert name=get_categoriesbyid assign=cats}
{section name=i loop=$posts}
{if !$gig_view || $smarty.section.i.index!=0}<div class="row separator mt5{$additional_class}"></div>{/if}
{if $smarty.section.i.iteration eq "6"}
<div class="row">
  <div class="large-12 small-12 columns tcenter mt10 mb10"><span class="banner1 heading">{insert name=get_advertisement AID=3}</span></div>
</div>
<div class="row separator mt5{$additional_class}"></div>
{/if}
<div class="deal-line row mt10{$additional_class}">
  {insert name=seo_clean_titles assign=title value=a title=$posts[i].gtitle}
  <div class="large-1 columns">
   <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$posts[i].USERID}{if !$profilepicture}<i class="icon-user icon-2x gray"></i>{else}<span class="avatar avatar-2x ml10"><img alt="{$posts[i].username|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width=50 height=50></span>{/if}</a>
  </div>
  <div class="large-9 columns subheading">
     <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes|truncate:15:"...":true}</a> <a class="button tiny empty subheading radius ml10 mb0" href="{$baseurl}/categories/{$cats[$posts[i].category].seo}">{$cats[$posts[i].category].name}</a>
     <p>
     {$posts[i].want|stripslashes|mb_truncate:400:"...":'UTF-8'}
     </p>
  </div>
  <div class="large-2 columns tright">
      {if $smarty.session.USERID eq $posts[i].USERID}
      <form action="" id="delete_suggested" name="" method="post">
      <input type="hidden" name="sug" value="{$posts[i].WID}">
      <input type="hidden" name="del" value="1">
      <a href="javascript:" class="btn-delete mt10 button tiny alert radius subheading"><i class="icon-trash"></i> {$lang185}</a>
      </form>
      {else}&nbsp;
      {/if}
  </div>


{*

  <div class="large-2 hide-for-small columns">
    {if $posts[i].feat eq "1"}<span class="block label alert small heading">{$lang531}</span>{/if}
    <a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}"><img alt="{$posts[i].gtitle|stripslashes}" src="{$purl}/t2/{$posts[i].p1}"></a>
    {if $posts[i].days == "0"}{include file='bit_instant.tpl'}{elseif $posts[i].days == "1"}{include file='bit_express.tpl'}{/if}
  </div>
  <div class="large-7 small-6 columns">
    <div class="show-for-small">{if $posts[i].feat eq "1"}<span class="block label alert small heading">{$lang531}</span>{/if}{if $posts[i].days == "0"}{include file='bit_instant.tpl'}{elseif $posts[i].days == "1"}{include file='bit_express.tpl'}{/if}</div>
    <h2 class="heading"><a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}">{$posts[i].gtitle|stripslashes|mb_truncate:50:"...":'UTF-8'} {$posts[i].price|stripslashes} {$lang63}</a></h2>
    <p class="subheading">{$posts[i].gdesc|stripslashes|mb_truncate:150:"...":'UTF-8'}
      <span class="author">{$lang414} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes assign=profileurl}">{$posts[i].username|stripslashes|truncate:10:"...":true}</a>&nbsp;<span class="country {$posts[i].country}" title="{insert name=country_code_to_country value=a assign=userc code=$posts[i].country}{$userc}"></span></span></p>
  </div>
  <div class="large-2 small-4 columns tcenter ratings">{for $s=1 to 5}{if $posts[i].rating >= $s*20}<i class="icon-star"></i>{elseif $posts[i].rating > (($s-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}<br><small>(<span data-tooltip class="has-tip" data-width="210" title="{$posts[i].rcount} {$lang529}">{$posts[i].rcount}</span>)</small></div>
  <div class="large-1 small-2 columns"><span data-tooltip class="has-tip" data-width="210" title="{$posts[i].days} {$lang528}"><i class="icon-time"></i> {$posts[i].days}</span></div>
*}
</div>
{/section}