<div class="row all-header mb20{if !$smarty.session.USERID AND !$smarty.cookies['hv']} hide{/if}">
	<div class="large-12 small-12 columns tcenter">
		<h1 class="heading drop-shadow mt20">{$lang556}</h1>
		<h2 class="subheading drop-shadow">{$lang557}</h2>
	</div>
</div>

<div class="contrast-wrapper heading{if !($smarty.session.USERID OR $smarty.cookies['hv'])} hide{/if}">
	{if !$smarty.session.USERID AND !$smarty.session.is_user}<a href="{$baseurl}/signin" class="button medium heading animated shake radius welcome-button c-button mt20" data-shake=shake rel=nofollow>შემოგვიერთდი, იშოვე ფული!</a>{else}{insert name=return_posts_count value=var assign=member_posts USERID=$smarty.session.USERID}{if !$member_posts}<a href="{$baseurl}/about" class="button medium alert heading animated radius hiw-button c-button mt20" data-shake=tada id=firstStop>როგორ მუშაობს?</a> <a href="{$baseurl}/new" class="button medium heading animated radius success newpost-button c-button mt20" data-shake=pulse id="new-deal-shake-btn" rel=nofollow>განათავსე განცხადება</a>{else}<a href="{$baseurl}/new" class="button medium heading animated radius success newpost-button c-button mt20" data-shake=pulse id="new-deal-shake-btn" rel=nofollow>განათავსე განცხადება</a>{/if}{/if}
</div>
{if !$smarty.session.USERID AND !$smarty.cookies['hv']}
{assign var=hideMA value=1 scope=global}
<div class="rel has-large-video-bg i-bg-{1|rand:2}">
{*<div class="video-bg" data-vide-bg="mp4: {$baseurl}/res/cover1f2, webm: {$baseurl}/res/cover1f2, poster: {$baseurl}/res/cover1f" data-vide-options="position: 0% 40%, loop: true, muted: true, posterType: jpg"></div>*}
	<div class="video-bg">
		<div class="mp-large-image-bg"></div>
	</div>
	<div class="row has-large-video-textblock">
		<div class="cat-central-text large-12 columns tcenter mt20">
			<h1 class="heading f25 mb0">დასაქმების ონლაინ პლატფორმა</h1>
			<h2 class="subheading">იპოვე შემსრულებელი, შეუკვეთე და ჩაიბარე ნამუშევარი. ონლაინ.</h2>
			<div class="tcenter show-for-small">
				<a href="{$baseurl}/new?jobseeker=1" class="subheading button medium secondary fw-button-1">ვეძებ სამუშაოს</a><br>
				<a href="{$baseurl}/suggested" class="deal-seaker subheading button medium primary-alt fw-button-1">ვეძებ შემსრულებელს</a>
			</div>
			<div class="tcenter hide-for-small">
				<ul class="button-group radius subheading two-buttons-centered">
				<li><a href="{$baseurl}/new?jobseeker=1" class="button large secondary fw-button-1">ვეძებ სამუშაოს</a></li>
				<li class="rel"><a href="{$baseurl}/suggested" class="deal-seaker button large primary-alt fw-button-1">ვეძებ შემსრულებელს</a></li>
				</ul>  
			</div>
			
			<div class="needs-more-info-poster hide-for-small">
				<a href="{$baseurl}/about" class="button round large empty subheading mp-moreInfo-button">გსურს გაიგო მეტი?</a>
			</div>
			<div class="needs-more-info-poster show-for-small">
				<a href="{$baseurl}/about" class="button round small empty subheading mp-moreInfo-button">გსურს გაიგო მეტი?</a>
			</div>
		</div>
	</div>
	{*
		<div class="row" id="slideshow-row">
			<div class="large-12 large-centered small-12 columns tcenter video-bg-bottom-block needs-more-info">
				
					<span class="subheading large-video-h1">გსურს გაიგო მეტი?</span><br><br>
					<a href="{$baseurl}/about" class="button round empty medium subheading moreInfo-button"><i class="icon-question-sign"></i> როგორ მუშაობს?</a><a href="{$baseurl}/about?advantage" class="button round secondary medium ml20 subheading start-slideshow-button"><i class="icon-rocket"></i> {$site_name}-ს უპირატესობები</a> 
					
				</div>
				<div class="needs-more-info-slideshow hide">
					<ul class="subheading" data-orbit data-options="timer_speed:4500;pause_on_hover:false;navigation_arrows:false;slide_number:false;timer:false;resume_on_mouseout:true">
						<li>გადახდილი თანხა საიმედოდ ინახება სასურველი შედეგის მიღებამდე</li>
						<li>უხარისხო შედეგის შემთხვევაში თანხა სრულად უბრუნდება დამკვეთს</li>
						<li>სახლიდან გაუსვლელად მუშაობის შესაძლებლობა</li>
						<li>საკუთარი პოტენციალის გამოვლენის უნიკალური შესაძლებლობა</li>
						<li>დამატებითი შემოსავლის ამოუწურავი პერსპექტივა</li>
					</ul>
				</div>
			</div>
			*}
</div>
{/if}

<div class="mp-scroll-to"></div>

<div id="mp-post-job" class="{if $smarty.session.USERID}hide{else}hide-for-small{/if}">
	{include file="postjob_intro.tpl"}
</div>

<!--Work Area-->
<div class="work-area-wrapper">

{*if !$smarty.session.USERID}
<form id="search" method="get" action="{$baseurl}/search" class="search-bar">
	<div class="row collapse">
		<div class="large-2 small-{if $viewpage}4{else}1{/if} columns">{if $viewpage}<a href="javascript:" class="button small radius secondary heading cats-btn"><i class="icon-sitemap cat-btn-toggle"></i><i class="icon-collapse-alt cat-btn-toggle hide"></i> {$lang527}</a>{else}&nbsp;{/if}</div>
		<div class="large-6 small-{if $viewpage}5{else}8{/if} columns geo-box">
			<input type="text" id="query" name="query" value="{$smarty.request.query}" placeholder="{$lang524}" class="geo">
		</div>
		<div class="large-2 small-2 columns">
			<a href="javascript:document.forms.search.submit();" class="button prefix subheading top-search-button">{$lang129}</a>
		</div>
		<div class="large-2 small-1 columns">&nbsp;</div>
	</div>
</form>
{/if*}
{*
<div class="row mt20 hide">
	<div class="large-3 large-centered columns tcenter">
		<a href="javascript:" class="button small radius secondary heading cats-btn"><i class="icon-sitemap"></i> კატეგორიები</a>
	</div>
</div>
*}
<div class="filters row">
	<ul class="large-12 small-12 {if $smarty.cookies['hv'] OR $smarty.session.USERID}mt30{else}mt50{/if} subheading columns">
		<li><strong class="heading">{$lang109}</strong></li>
		{if $s eq "d" OR $s eq "dz" OR $s eq ""}
		<li><a href="{$baseurl}?s=d{if $s eq "d" OR $s eq ""}z{/if}" class="secondary radius label">{$lang110}</a></li>
		{else}
		<li><a href="{$baseurl}?s=d">{$lang110}</a></li>
		{/if}
		{if $s eq "p" OR $s eq "pz"}
		<li><a href="{$baseurl}?s=p{if $s eq "p"}z{/if}" class="secondary radius label">{$lang111}</a></li>
		{else}
		<li><a href="{$baseurl}?s=p">{$lang111}</a></li>
		{/if}
		{if $s eq "r" OR $s eq "rz"}
		<li><a href="{$baseurl}?s=r{if $s eq "r"}z{/if}" class="secondary radius label">{$lang112}</a></li>
		{else}
		<li><a href="{$baseurl}?s=r">{$lang112}</a></li>
		{/if}
		{if $s eq "c" OR $s eq "cz"}
		<li><a href="{$baseurl}?s=c{if $s eq "c"}z{/if}" class="secondary radius label">{$lang436}</a></li>
		{else}
		<li><a href="{$baseurl}?s=c">{$lang436}</a></li>
		{/if}
																
		{if $s eq "e" OR $s eq "ez"}
		<li><a href="{$baseurl}?s=e{if $s eq "e"}z{/if}" class="secondary radius label" data-tooltip title="24 საათში შესრულებადი მომსახურება">{$lang494}</a></li>
		{else}
		<li><a href="{$baseurl}?s=e" class="has-tip" data-tooltip title="24 საათში შესრულებადი მომსახურება">{$lang494}</a></li>
		{/if}
	</ul>
</div>
{*include file="categories.tpl"*}
{include file="bit.tpl"}
<div class="row mt20">
	<div class="large-12 tcenter columns">
		<a href="javascript:" class="button large secondary subheading radius send-button" id="load-more" data-order="{$smarty.get.s}" data-start="{$ending}" data-type="recent" data-source="main">{$lang532}</a>
		<div class="progress-indicator-icon-message hide">
				{include file="loader.tpl"}
		</div>
	</div>
</div>
</div>
<!--Work Area end-->
<script>
	var baseurl = '{$baseurl}';
	var isGuest = {if !$smarty.session.USERID}true{else}false{/if};
	var startTour = {if isset($smarty.get.startTour)}true{else}{$autostart_tour}{/if};
	var def_js = ['{$baseurl}/js/mainpage.js?v=030','{$baseurl}/js/submitprocessor.js?v=6'];
	var page = 'main';
 </script>

{*
<ol id="siteTour" class="joyride-list" data-joyride>
{if !isset($smarty.session.USERID) OR $smarty.session.USERID eq '' OR isset($smarty.get.startTour)}
{if !isset($smarty.get.startTour)}
	<li data-id="offerTour" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
		<h4 class=subheading>პირველად ხარ საიტზე <span class="lat almost-black">iDo.ge</span>?</h4>
		<p class="subheading s08 mb0">მიყევი საიტის ტურს და მიიღე ინფორმაცია თუ როგორ გამოიმუშავო, ან დაუკვეთო სასურველი მომსახურება სახლიდან გაუსვლელად.</p>
		<img src="{$baseurl}/images/ar_d1.png" alt="" width=30 height=60><br>
	</li>
{/if}
	<li data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>რას წარმოადგენს საიტი <span class="lat almost-black">iDo.ge</span></h4>
		<p class="subheading s08">ეს არის დისტანციური დასაქმების ონლაინ პლატფორმა, რომლის საშუალებით შეგიძლია მიიღო, ან გასწიო მრავალი სახის მომსახურება. მაგალითად: კომპანიის ლოგოტიპის დამზადება, თარგმნა, კონსულტაცია და ა.შ.</p>
	</li>
	<li data-id="load-more" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>განცხადებები</h4>
		<p class="subheading s08">მომსახურების სპექტრის გასაცნობად დაათვალიერე აქტიური განცხადებები. აქ მოყვანილი მომსახურებიდან ნებისმიერი შეგიძლია მიიღო ონლაინში, სახლიდან გაუსვლელად.</p>
	</li>
	<li data-id="cats-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-scrolltop="1">
		<h4 class=subheading>კატეგორიები</h4>
		<p class="subheading s08">იხილე განცხადებების კატეგორიები და მიიღე მომსახურება იმ სფეროში, რომელშიც გაინტერესებს.</p>
	</li>
 <li data-id="new-deal-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-firebefore="showSearchBar">
		<h4 class=subheading>ახალი განცხადება</h4>
		<p class="subheading s08">თუ გინდა დასაქმდე, ან ეძებ შემსრულებელს, განათავსე ახალი განცხადება სრულიად <strong class="red">უფასოდ</strong>!</p>
	</li>
	<li data-id="query" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>ძიება</h4>
		<p class="subheading s08">სასურველი განცხადების მოსაძებნად, შეიყვანე საძიებო ფრაზა და იპოვე მომსახურების სასურველი შემსრულებელი.</p>
	</li>
	<li data-id="hiw-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>როგორ მუშაობს?</h4>
		<p class="subheading s08">გსურს მიიღო მეტი ინფორმაცია? გაეცანი ფულის გამომუშავების და დასაქმების მარტივ სქემას. საიტის ტურის განმეორებითი ჩვენება ხელმისაწვდომია სწორად ამ ბმულის საშუალებით.</p>
	</li>
	{if isset($smarty.get.startTour)}
	<li data-id="inbox-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>სამართავი პანელი</h4>
		<p class="subheading s08">მართე შენი ანგარიში, აწარმოე მიმოწერა და აკონტროლე შემოსავალი და ხარჯები საიტის თავში განლაგებული მენიუს საშუალებით.</p>
	</li>
	<li data-id="mycabinet-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>შეკვეთის სტატუსის მონიტორინგი</h4>
		<p class="subheading s08">მას შემდეგ, რაც შეუკვეთავ მომსახურებას, მის მონიტორინგს და უკუკავშირის შეძლებ შენი კაბინეტის "ჩემი შეკვეთების" სექციიდან.</p>
	</li>
	<li data-id="suggest-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-scrolltop="1">
		<h4 class=subheading>ეძებ სამუშაოს?</h4>
		<p class="subheading s08">თუკი გაქვს თავისუფალი დრო და სურვილი გამოიმუშავო ფული და არ იცი რა მომსახურება შესთავაზო საზოგადოებას, იხილე დამსაქმებლის განცხადებების სვეტი.</p>
	</li>
	<li data-id="new-deal-shake-btn" data-button="OK">
		<h4 class=subheading>იშოვე ფული ან დაასაქმე!</h4>
		<p class="subheading s08">დაიწყე ონლაინ-საქმიანობა, განათავსე განცხადება ონლაინში, ან დაათვალიერე განცხადებები და დაუკვეთე მომსახურება საიტზე მოღვაწე მონდომებულ და ნიჭიერ შემსრულებლებს.</p>
	</li>
	{/if}
	<li data-id="offerTour" data-button="OK">
		<h4 class=subheading>დაიწყე საიტით სარგებლობა</h4>
		<p class="subheading s08">მოკლე გაცნობითი ტური დასრულებულია.<br>
		iDo.ge გისურვებთ წარმატებას!</p>
	</li>
{else}
	<li data-id="inbox-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>სამართავი პანელი</h4>
		<p class="subheading s08">მართე შენი ანგარიში, აწარმოე მიმოწერა და აკონტროლე შემოსავალი და ხარჯები საიტის თავში განლაგებული მენიუს საშუალებით.</p>
	</li>
	<li data-id="mycabinet-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>ჩემი კაბინეტი</h4>
		<p class="subheading s08">მას შემდეგ, რაც შეუკვეთავ ან დაგიკვეთავენ მომსახურებას, მასზე მუშაობას და უკუკავშირის დამყარებას შეძლებ შენი კაბინეტის საშუალებით.</p>
	</li>
	<li data-id="suggest-btn" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-scrolltop="1">
		<h4 class=subheading>ეძებ სამუშაოს?</h4>
		<p class="subheading s08">თუკი გაქვს თავისუფალი დრო და სურვილი გამოიმუშავო ფული და არ იცი რა მომსახურება შესთავაზო საზოგადოებას, იხილე დამსაქმებლის განცხადებების სვეტი.</p>
	</li>
	<li data-id="new-deal-shake-btn" data-button="OK">
		<h4 class=subheading>იშოვე ფული ან დაასაქმე!</h4>
		<p class="subheading s08">დაიწყე ონლაინ-საქმიანობა, განათავსე განცხადება ონლაინში, ან დაათვალიერე განცხადებები და დაუკვეთე მომსახურება საიტზე მოღვაწე მონდომებულ და ნიჭიერ შემსრულებლებს.</p>
	</li>
{/if}
</ol>
*}