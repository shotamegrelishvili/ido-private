<link href="{$baseurl}/css/slickslider.css" media="screen" rel="stylesheet">
<style>
/* Needed for Foundation 5 */
.clearing-assembled .clearing-container .visible-img {
background: #333333;
overflow: hidden;
height: 85%;
position: relative;}
.clearing-touch-label {
position: absolute;
top: 50%;
left: 50%;
color: #AAAAAA;
font-size: 0.6em;}
</style>

<div class="crumbs">
  <ul class="row collapse heading">
	<li><a href="{$baseurl}/">{$lang0}</a>
	<li><a href="{$baseurl}/user/{$uname}" rel=nofollow class=current><i class="icon-user"></i> {$uname} ({$lang31})</a>
  </ul>
</div>

{if $message ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">
	<ul class=subheading>
	{$message}
	</ul>
  </div>
</div>
{/if}

{if $portfolio}
	{insert name=gfs value=a assign=afs fid=$portfolio[0].FID}
	{if $afs.file_width AND $afs.file_width >= $portfolioSettings.minDimensionsForFeaturing.width AND $afs.file_height >= $portfolioSettings.minDimensionsForFeaturing.height}
	<div class="up-portfolio-feat" style="background-image:url({$baseurl}/files/{$afs.file_path})"></div>
	{$portfolioHasFeat=1}
	{/if}
{/if}

<div class="work-area-wrapper">
<div class="row work-area work-area-pad up-main-wrapper {if $portfolioHasFeat} up-work-area-has-feat{/if}">
	<ul class="hide-for-medium-down wa-side-bar wa-abs-side-bar">
		<li><a href="javascript:" class="wa-sb-active wa-switcher has-tip" data-tooltip title="ძირითადი ინფორმაცია" data-target="basic">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		<path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
		<path d="M0 0h24v24h-24z" fill="none"/>
		</svg>
		</a></li>
		<li><a href="javascript:" class="wa-switcher has-tip{if !$ratings} wa-inactive{/if}" data-tooltip title="დამკვეთის შეფასება" data-target="reviews">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		<path d="M0 0h24v24h-24zm15.35 6.41l-1.77-1.77c-.2-.2-.51-.2-.71 0l-6.87 6.89v2.47h2.47l6.88-6.88c.2-.19.2-.51 0-.71z" fill="none"/>
		<path d="M20 2h-16c-1.1 0-1.99.9-1.99 2l-.01 18 4-4h14c1.1 0 2-.9 2-2v-12c0-1.1-.9-2-2-2zm-14 12v-2.47l6.88-6.88c.2-.2.51-.2.71 0l1.77 1.77c.2.2.2.51 0 .71l-6.89 6.87h-2.47zm12 0h-7.5l2-2h5.5v2z"/>
		</svg>
		</a></li>
		<li><a href="{if $USERID==$smarty.session.USERID AND !$posts}{$baseurl}/manage_deals?tab=deals{else}javascript:{/if}" class="wa-switcher has-tip{if !$posts} wa-inactive{/if}" data-tooltip title="განცხადებები" data-target="deals">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		<path fill="#DB4437" d="M24 0v24h-24v-24h24m1-1h-26v26h26v-26z"/>
		<path d="M20 6h-4v-2c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2h-4c-1.11 0-1.99.89-1.99 2l-.01 11c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2v-11c0-1.11-.89-2-2-2zm-6 0h-4v-2h4v2z"/>
		</svg>
		</a></li>
		<li><a href="{if $USERID==$smarty.session.USERID AND !$portfolio}{$baseurl}/settings?tab=portfolio{else}javascript:{/if}" class="wa-switcher has-tip{if !$portfolio} wa-inactive{/if}" data-tooltip title="პორტფოლიო" data-target="portfolio">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		<path d="M0 0h24v24h-24z" fill="none"/>
		<path d="M22 16v-12c0-1.1-.9-2-2-2h-12c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2zm-11-4l2.03 2.71 2.97-3.71 4 5h-12l3-4zm-9-6v14c0 1.1.9 2 2 2h14v-2h-14v-14h-2z"/>
		</svg>
		</a></li>
		{*<li><a href="javascript:" class="wa-switcher" data-target="experience-settings">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		<path d="M0 0h24v24h-24z" fill="none"/>
		<path d="M5 13.18v4l7 3.82 7-3.82v-4l-7 3.82-7-3.82zm7-10.18l-11 6 11 6 9-4.91v6.91h2v-8l-11-6z"/>
		</svg>
		<span>კომპეტენცია &amp; გამოცდილება</span></a></li>*}
	</ul>
{if $USERID==$smarty.session.USERID}
<div class="up-allow-edit">
	<a href="{$baseurl}/settings?tab=basic" class="button primary-alt medium up-edit"><i class="icon-pencil"></i></a>
{/if}
<div class="row mt20" id="basic">
	<div class="large-2 small-3 columns">
		{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$USERID}
		{if $profilepicture}
		<img alt="{$uname|stripslashes}" src="{$membersprofilepicurl}/{$profilepicture}" class="g-user-avatar">
		{else}
		<i class="icon-user icon-2x"></i>
		{/if}
	</div>
	<div class="large-10 small-9 columns">
		<h1 class="heading h-large mb0">{$uname|stripslashes}</h1>
		<div class="row">
			<div class="large-4 small-12 columns mt3 ratings user-ratings">
				{for $s=1 to 5}{if $rating >= $s*20}<i class="icon-star icon-large"></i>{elseif $rating > (($s-1)*20)+5}<i class="icon-star-half-full icon-large"></i>{else}<i class="icon-star-empty icon-large"></i>{/if}{/for}
			</div>
			<div class="large-4 small-12 columns heading mt3">
				{if $location}<i class="icon-map-marker"></i> <span class="s08">{$location}</span>{/if}
			</div>
			<div class="large-3 end small-12 columns tright">
			{if $USERID!=$smarty.session.USERID}
				<a href="{$baseurl}/conversations/{$uname|stripslashes}" class="button primary-alt medium radius heading expand"><i class="icon-envelope-alt"></i> დაკავშირება</a>
			{/if}
			</div>
		</div>
		<div class="row">
			<div class="large-4 small-12 columns gray"><i class="icon-double-angle-up icon-2x has-tip gray" data-tooltip title="მომხმარებლის რეიტინგი"></i> <span class="f16">{$score}</span></div>
			<div class="large-8 small-12 columns">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style mt5">
				<a class="addthis_button_facebook_share" fb:share:layout="button_count" addthis:url="{$baseurl}/user/{$uname}" addthis:title="{$site_name}: {$lang555}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				<a class="addthis_button_tweet" addthis:url="{$baseurl}/user/{$uname}" addthis:title="{$lang555} #{$site_name}-ზე. (@iDoGeorgia) " addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				<a class="addthis_counter addthis_pill_style" addthis:url="{$baseurl}/user/{$uname}" addthis:title="{$site_name}: {$lang555}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				</div>
				<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-522ee0e443fae154" defer></script>
				<!-- AddThis Button END -->
			</div>
		</div>
		{if $exp}
		<div class="row subheading mt20">
			<div class="large-1 small-2 columns"><span class="up-exp-logo"></span></div>
			<div class="large-11 small-10 columns up-exp-title">
				{section name=e loop=$exp}
				<span class="button secondary small">{$exp[e].title}</span>
				{/section}
			</div>
		</div>
		{/if}
		<div class="row mt20 lh16 subheading">
			<div class="user-desc large-11 large-offset-1 small-12 columns">{$desc|strip_tags|replace:"\n":'<br>'}</div>
		</div>
		
	</div>
	{*<div class="large-2 small-2 columns"><!--Level info goes here-->if $enable_levels eq "1" AND $price_mode eq "3"} <span style="font-size:12px">({$lang499} {$level|stripslashes})</span>{/if</div>*}
</div>
{if $USERID==$smarty.session.USERID}
</div>
{/if}

{if $smarty.session.ADMINID}
<form action="#" class="custom">
<div class="row mt20">
	<div class="large-4 large-centered columns tcenter ap-user-block ap-side">
		<div id="user-notification-panel" class="hide button small round secondary"></div>
		<div class="user-panel">
		  <a href="javascript:" data-mod-space="user" data-mod-tool="rating" class="admin-tool-btn button small expand radius subheading"><i class="icon-signal"></i> რეიტინგი +/-</a><br>
		  <a href="javascript:" data-mod-space="user" data-mod-tool="write" class="admin-tool-btn button small expand radius success subheading"><i class="icon-envelope-alt"></i> მივწეროთ</a><br>
		  <a href="javascript:" data-mod-space="user" data-mod-tool="block" class="admin-tool-btn button small expand radius alert subheading"><i class="icon-lock"></i> დავბლოკოთ</a>
		</div>
		<div class="user-tools hide">
		  <div class="tools-close tright"><a href="javascript:" class="tools-close"><i class="icon-remove-sign"></i></a></div>
		  <div class="rating tool hide">
			<input type="text" name="user-rating" id="user-rating" pattern="[0-9]" placeholder="მაგ.: -300"><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
		  </div>
		  <div class="write tool hide geo-box">
			<textarea name="user-write" id="user-write" cols="30" rows="10" placeholder="შეიყვანე მისაწერი ტექსტი" class="geo ta10">{$lang730|sprintf:$uname}</textarea><input type="button" class="admin-tools-submit prefix button secondary" value="OK">
		  </div>
		  <div class="block tool hide">
			<input type="text" class="mb0" name="user-block" id="user-block" pattern="[0-9]" placeholder="მაგ.: 7"><span class="subheading mt2 s08">ბლოკირების პერიოდი (დღე)</span><input type="button" class="admin-tools-submit prefix button secondary mt5" value="OK">
		  </div>
		</div>
	</div>
</div>
<input type="hidden" id="mod-space" value="">
<input type="hidden" id="mod-tool" value="">
<input type="hidden" id="userUID" value="{$USERID}">
</form>
{/if}

<div class="row separator mt20"></div>

<div class="row mt20">
	<div class="large-4 small-4 columns tcenter">{if !$ratingcount}<span class="subheading s08">ჯერ არ შეუფასებიათ</span>{else}<span class="big-number block {if $rating<80}red{elseif $rating>79 AND $rating<90}gray{else}green{/if}">{$rating}%</span><span class="subheading s08"></span>{/if}</div>
	<div class="large-4 small-4 columns tcenter mt10"><span class="heading h-large block gray">{insert name=get_past_period assign=past_period value=var t=$addtime}<strong>{$past_period}</strong></span><span class="subheading s08"></span></div>
	<div class="large-4 small-4 columns tcenter mt10"><span class="heading h-large block gray">{insert name=get_past_period assign=past_period value=var t=$lastlogin}<strong>{$past_period}</strong></span><span class="subheading s08"></span></div>
</div>
<div class="row">
	<div class="large-4 small-4 columns tcenter subheading s08">{if $ratingcount}{if $rating>0}პოზიტიური{else}ნეგატიური{/if} შეფასება{else}&nbsp;{/if}</div>
	<div class="large-4 small-4 columns tcenter subheading s08">რეგისტრაცია</div>
	<div class="large-4 small-4 columns tcenter subheading s08">ბოლო ვიზიტი</div>
</div>

{if $ratings}
<!-- Reviews -->
<div class="row separator mt20" id="reviews"></div>
<div class="row work-area-wrapper up-rev-wrapper">
	<blockquote class="up-rev-text{if mb_strlen($ratings[0].comment,'UTF-8')<15} tcenter{/if}">{$ratings[0].comment|trim}</blockquote>
	<span class="up-rev-user">
	{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$USERID}
		{if $profilepicture}
		<img alt="{$uname|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}">
		{else}
		<i class="icon-user icon-2x"></i>
		{/if}
	{$ratings[0].firstname}{if $ratings[0].location}, {$ratings[0].location}{/if} ({insert name=get_past_period assign=past_period value=var t=$ratings[0].time_added precise=1}{$past_period})</span>
</div>
<!-- end Reviews -->
{/if}

{if $posts}
<div class="row separator mt20" id="deals"></div>
<!--deals-->
{if $USERID==$smarty.session.USERID}
	<div class="up-allow-edit">
		<a href="{$baseurl}/manage_deals?tab=deals" class="button primary-alt medium up-edit"><i class="icon-pencil"></i></a>
{/if}
<div class="row other-deals">
	<div class="mt20 large-12 columns">
		<h3 class="heading gray">{$uname|stripslashes}{$lang569}</h3>
	</div>
</div>
{assign var="gig_view" value=1}

{include file="bit.tpl"}
<!--deals end-->
{if $USERID==$smarty.session.USERID}
	</div>
{/if}
{/if}

{if $portfolio}
{if $USERID==$smarty.session.USERID}
	<div class="up-allow-edit">
		<a href="{$baseurl}/settings?tab=portfolio" class="button primary-alt medium up-edit"><i class="icon-pencil"></i></a>
{/if}
<div class="row separator mt20" id="portfolio"></div>
<div class="row user-portfolio">
	<div class="mt20 large-12 columns">
		<h3 class="heading gray">{$uname|stripslashes}-ს {$lang830}</h3>
	</div>
	<div class="large-12 small-12 subheading columns tcenter plupload_view_thumbs up-portfolio-wrapper">
		<ul id="portfolio-slider" class="clearing-thumbs slick-slider multiple-items" data-clearing>
		{section name=p loop=$portfolio}
		{insert name=gfs value=a assign=afs fid=$portfolio[p].FID}
		{if strpos($afs.file_type,'image')!==false}
			<li class="plupload_file isimg" data-isimg>
				<a href="{$baseurl}/files/{$afs.file_path}">
					<img src="{$baseurl}/files/{$afs.file_path}" alt="">
				</a>
			</li>
		{else}
			<li class="plupload_file">
				<div class="plupload_file_dummy"><a href="{$baseurl}/files/{$afs.file_path}"><span>{$afs.file_ext}</span></a>
				</div>
			</li>
		{/if}
		{/section}
		</ul>
	</div>
</div>
{if $USERID==$smarty.session.USERID}
	</div>
{/if}
{/if}
</div>
</div>
<script>
  var baseurl = '{$baseurl}';
  var lang = [];
  var def_js = ['{$baseurl}/js/user.js?v=003'{if $smarty.session.ADMINID},'{$baseurl}/js/user_admin.js?v=001'{/if}];
  var page = 'user';
</script>