{if $posts|@sizeof}
{section name=i loop=$posts}
<div class="row collapse mt10 deal-line">
{*if $smarty.section.i.iteration eq "6"}
<div style="padding:5px;">
<center>
{insert name=get_advertisement AID=3}
</center>
</div>
{/if*}
{insert name=seo_clean_titles assign=title value=a title=$posts[i].gtitle}
    <div class="large-1 small-2 columns">
	   <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$posts[i].USERID}{if !$profilepicture}<i class="icon-user icon-2x gray"></i>{else}<span class="avatar avatar-2x ml10"><img alt="{$posts[i].username|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}?{$smarty.now}" width=50 height=50></span>{/if}</a>
    </div>
    <div class="large-9 small-9 columns subheading">
       <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}"><strong>{$posts[i].username|stripslashes|truncate:15:"...":true}</strong></a> <a class="button tiny empty subheading radius ml10 mb0" href="{$baseurl}/categories/{$catsbyid[$posts[i].category].seo}">{$catsbyid[$posts[i].category].name}</a>
       <p>
       {$posts[i].want|stripslashes|mb_truncate:400:"...":'UTF-8'}
       </p>
    </div>
    <div class="large-2 small-1 columns tright">
        {if $smarty.session.USERID eq $posts[i].USERID}
        <form id="delete_suggested-{$posts[i].WID}" method="post">
        <input type="hidden" name="sug" value="{$posts[i].WID}">
        <input type="hidden" name="del" value="1">
        <a href="javascript:" class="btn-delete mt10 button tiny alert radius subheading" data-wid={$posts[i].WID}><i class="icon-trash"></i><span class="hide-for-small"> {$lang185}</span></a>
        </form>
        {else}&nbsp;
        {/if}
    </div>
</div>
<div class="separator"></div>
{/section}
{else}
  <div class="large-12 small-12 columns"><h2 class="subheading">ამ მომენტისთვის არცერთი განცხადება არ არის განთავსებული</h2></div>
{/if}