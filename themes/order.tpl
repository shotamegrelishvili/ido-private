<div class="crumbs">
  <ul class="row collapse heading">
	<li><a href="{$baseurl}/">{$lang0}</a>
	<li><a href="{$baseurl}/order?item={$smarty.get.item}" class=current>შეკვეთის გაფორმება</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-10 small-10 columns">
	<h1 class="heading h-large mb0">{$lang734}</h1>
	<h2 class="subheading">{$lang735}</h2>
  </div>
</div>
<div class="row">
	<div class="large-12 small-12 columns">
	  <div class="row s08">
		  <div class="large-4 columns bg-green br-white">&nbsp;</div>
		  <div class="large-4 columns bg-gray br-white">&nbsp;</div>
		  <div class="large-4 columns bg-gray">&nbsp;</div>
	  </div>
	</div>

	{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="large-12 small-12 columns mt10">{include file="error.tpl"}</div>{/if}
</div>

<div class="row mt20">
	<div class="large-8 small-7 columns h-large">
		<h3 class="subheading">{$p.gtitle}</h3>
	</div>
	<div class="large-4 small-5 tright columns subheading"><span class="label h-large{if $p.totalprice eq "0"} alert{/if}">{if $p.totalprice ne "0"}{$p.totalprice} {$lang197}{else}{$lang743}{/if}</span></div>
</div>

{if $smarty.server['HTTP_HOST'] != 'localhost'}
<script src="https://www.emoney.ge/js/econnect/v1/lib.js"></script>
<script src="https://www.emoney.ge/js/econnect/v1/cl.js"></script>

	{if $insufficient_funds}
	<iframe id="eMoneyAuthorizeWidgetFrame" src="https://www.emoney.ge/index.php/widget/autologin?lang=ka&amp;protocol={$smarty.server['HTTP_X_FORWARDED_PROTO']}&amp;domain={$smarty.server['HTTP_HOST']}&amp;t={$smarty.session.access_token}" style="position:fixed;top:0;left:0;z-index:998;height:100%;width:100%;border:0px;display:none;"></iframe>
	{else}
	<iframe id="eMoneyAuthorizeWidgetFrame" src="https://www.emoney.ge/index.php/widget/login_common?lang=ka&amp;protocol={$smarty.server['HTTP_X_FORWARDED_PROTO']}&amp;domain={$smarty.server['HTTP_HOST']}&amp;external_registration=0" style="position:fixed;top:0;left:0;z-index:998;height:100%;width:100%;border:0px;display:none;"></iframe>
	{/if}
{/if}

<form action="{$baseurl}/order?item={$p.IID}" class="row panel" id="bal_form" name="bal_form" method="post">
	<div class="row">
		<div class="large-12 columns heading">აირჩიე გადახდის მეთოდი</div>
	</div>
	<div class="separator mt5"></div>
	<div class="large-5 large-centered columns mt20">
		<div class="progress-indicator-icon-message tcenter hide">
			{include file="loader.tpl"}
		</div>
		<a href="javascript:"{if $eConnect_canpay AND !$eConnect_disabled} id="eMoney-pay"{/if} class="{if $eConnect_required}econnect-act-btn{if $smarty.server['HTTP_HOST'] == 'localhost'} econnect-local{/if}{/if} button medium {if !$eConnect_disabled}success{else}secondary disabled{/if} radius expand subheading mt10">{if $p.totalprice ne "0"}eMoney-ს ანგარიშით გადახდა
		<span class="subheading s08 block mt5 tcenter fwn">გადახდის უსაფრთხო ელექტრონული სისტემა</span>{elseif $p.totalprice eq "0" AND $eConnect_required}ავტორიზაცია eMoney-ს სისტემაში<span class="subheading s08 block mt5 tcenter fwn">მომსახურება უფასოა, თანხა არ ჩამოგეჭრება</span>{elseif $p.totalprice eq "0"}შეკვეთის დადასტურება<span class="subheading s08 block mt5 tcenter fwn">მომსახურება უფასოა, თანხა არ ჩამოგეჭრება</span>{/if}</a><br><br>
		{*<div class="hide">
		<a href="javascript:" class="button medium {if $funds<$p.totalprice}secondary{else}success{/if} radius expand subheading mb0"{if $afunds<$p.totalprice AND $p.totalprice} disabled{else} onclick="document.getElementById('bal_form').submit()"{/if}>ბალანსიდან გადახდა
		<span class="subheading s08 block mt5 tcenter fwn">ხელმისაწვდომი თანხა: <span class="label secondary fwn">{$funds} {$lang197}</span></span></a><br>
		</div>*}
	</div>
	<input type="hidden" name="paymentgateway" id="paymentgateway" value="2">
	<input type="hidden" name="item" value="{$p.IID}">
	<div class="large-12 columns subheading mt20 subheading">
	<p>
	<a href="{$baseurl}/agreement?item={$p.IID}" target=_blank><i class="icon-paper-clip icon-large"></i> <strong>ელექტრონული ხელშეკრულება</strong></a> შენსა და შემსრულებელს შორის - გაეცანი მას პროცესის გაგრძელებამდე.
	</p>
	
	<p>
	{if $p.totalprice ne "0"}
	<span class="gray">¢</span> <strong>თანხა დროებით დაიბლოკება</strong> და ჩამოიჭრება შენი ანგარიშიდან {if $p.moneyback}შენს მიერ სამუშაოს მიღების შემდეგ.{else}შემსრულებლის მიერ სამუშაოს ჩაბარების შემდეგ.
	</p>

	<p>
	<i class="icon-info-sign icon-large gray"></i> <strong>თანხა არ ექვემდებარება დაბრუნებას</strong> იმ შემთხვევაში, თუ შემსრულებელი ჩაგაბარებს სამუშაოს.{/if}
	</p>

	<p>
	<i class="icon-check icon-large gray"></i> <strong>სამუშაოს დეტალების აღწერა</strong> შესაძლებელი იქნება გადახდის შემდეგ.{else}
	<i class="icon-check icon-large gray"></i> <strong>სამუშაოს დეტალების მიწოდება</strong> შესაძლებელი იქნება შემდეგ საფეხურზე.{/if}
	</p>
	</div>
</form>

<ol id="eConnect-joy" class="joyride-list" data-joyride>
<li data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade;nextButton:false">
<form class="row eConnect-form" method="post">
{if $eConnect_autostart}<span class="subheading red block mt10 mb10 s08">eConnect-ის სესია ვადაგასულია, საჭიროა განმეორებითი იდენტიფიკაცია.</span>{/if}
	<label for="email" class="heading"><i class="icon-envelope gray"></i> ელფოსტა:</label>
	<input type="email" name="email" id="email" class="eConnect-email" placeholder="personal@email.ge" value="{$eemail}" required>
	<label for="password" class="heading"><i class="icon-key gray"></i> პაროლი:</label>
	<input type="password" name="password" id="password" class="eConnect-password" required><br>
	<input type="hidden" name="econnect_signup" value="1">
	<a class="button medium secondary radius expand econnect-btn" href="javascript:"><span class="lat light-gray"><strong class=h-large><i>e</i></strong> </span> <span class="h-large">Connect</span></a><br>
	{if $eConnect_account_required}<span class="subheading block mt10 s08">არ გაქვს ანგარიში eMoney-ს სისტემაში?<br><i class="icon-chevron-sign-right"></i> <a href="https://www.emoney.ge/index.php/main/signup" target="_blank">შექმენი ანგარიში ახლა</a>.</span>{/if}
</form>
</li>
</ol>

<script>
	var eMoneyProcess='order';
	var eMoneyWidgetEnable={if $smarty.server['HTTP_HOST'] != 'localhost'}1{else}0{/if};
	var def_js = ['{$baseurl}/js/order.js?v=36{$smarty.now}','{$baseurl}/js/econnect.js?v=12'];
	var eConnect_required = {if $eConnect_required}1{else}0{/if};
	var eConnect_autostart = '{if $eConnect_autostart}1{else}0{/if}';
	var payconfirm = {if $p.totalprice ne "0"}'{$lang637}- {$p.totalprice} {$lang523}. {$lang638}'{else}''{/if};
	var eMoneyAutoSignin={if $eMoneyAutoSignin}1{else}0{/if};
	var debug='{$d}';
</script>