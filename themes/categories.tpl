<div class="cats-2c row mb20{if !$showcats} hide{/if}">
<ul class="large-block-grid-2 small-block-grid-2">
  {section name=i loop=$cats}
    <li class="subheading{if !($smarty.section.i.index % 2)} tright{/if}"><a href="{$baseurl}/categories/{$cats[i].seo|stripslashes}"{if $catselect eq $cats[i].CATID} class="active"{/if}>{$cats[i].name|stripslashes}</a></li>
  {/section}
</ul>
</div>