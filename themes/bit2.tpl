{if $loadmore}
<div class="fadein"></div>
{else}
<div class="deals-wrapper">
{/if}
{$totalFeaturedDeals = 0}{$isFeatured = 0}{$reg = 0}{assign var=skippedDealCards value=0 scope="global"}
{section name=i loop=$posts}
{if $smarty.section.i.iteration LE $itemsPerPage OR $feat_array_delayed OR $needsFinishRow}
	{if (!$reg AND !$isFeatured) OR $reg==4 OR $isFeatured==2}
		<div class="row">
	{$reg = 0}{$isFeatured = 0}{$needsFinishRow=true}
	{/if}

	{if $posts[i].feat eq "1" OR $posts[i].feat_cat eq "1" OR $posts[i].feat_mainpage eq "1"}
		{if $reg % 4}{* // Keep the featured deal-card for next row *}
			{include file="bit_feat.tpl" assign=feat_item_delayed}
			{$feat_array_delayed[] = $feat_item_delayed}
			{continue}
		{else}{* // end Keep the featured deal-card for next row *}
			{$isFeatured = $isFeatured+1}
			{$totalFeaturedDeals = $totalFeaturedDeals+1}
			<div class="large-6 small-12 columns">
			{include file="bit_feat.tpl"}
			</div>
		{/if}
	{else}
		<div class="large-3{if $smarty.section.i.last} end{/if} small-6 columns">
			{$reg = $reg+1}
			{include file="bit_reg.tpl"}
		</div>
	{/if}

	{if $reg==4 OR $isFeatured==2}{* // Close the Deal-cards Row of 4 *}
		{$needsFinishRow=false}
		</div>
		{if $feat_array_delayed}{* // Insert the delayed Featured Deal-cards to keep the row consistency *}
			<div class="row">
			{$isFeatured = 0}{$reg = 0}
			{section name=f loop=$feat_array_delayed}
				{$isFeatured = $isFeatured + 1}{$totalFeaturedDeals = $totalFeaturedDeals +1}
				<div class="large-6 small-12 columns">
					{$feat_array_delayed[f]}
				</div>
			{/section}
			{$feat_array_delayed = 0}
			{if $isFeatured==2}
				{$needsFinishRow=false}
			</div>
			{else}
			{$needsFinishRow=true}
			{/if}
		{/if}{* // End Insert the delayed Featured Deal-cards to keep the row consistency *}
	{/if}{* // End Close the Deal-cards Row of 4 *}

{else}
{assign var=ending value=$ending-1 scope="global"}
{assign var=skippedDealCards value=$skippedDealCards+1 scope="global"}
{/if}
{*

	{if $smarty.section.i.iteration eq "3"}
		<div class="suggest-work-wrapper mt10">
		{include file="suggest_work.tpl"}
		</div>
	{/if}

	{if $smarty.section.i.iteration eq "6"}
	<div style="padding:5px;">
	<center>
	{insert name=get_advertisement AID=3}
	</center>
	</div>
	{/if}
	*}
{*
<div class="deal-line row mt10{if $posts[i].active eq '0'} success-panel{/if}">
	<div class="large-2 small-12 columns">
		{if $posts[i].feat eq "1"}<span class="block label alert small heading">{$lang531}</span>{/if}
		<div class="row{if $posts[i].feat eq "1"} msg-error field-err{/if}">
			<div class="large-12 small-4 columns"><a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}"><img alt="{$posts[i].gtitle|stripslashes}" src="{$purl}/t2/{$posts[i].p1}"></a>
			<span class="show-for-small">{if $posts[i].days == "0"}{include file='bit_instant.tpl'}{elseif $posts[i].days == "1"}{include file='bit_express.tpl'}{/if}</span>
			</div>
			<div class="small-8 columns hide-for-medium-up">
				<h2 class="heading"><a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}">{$posts[i].gtitle|stripslashes|mb_truncate:70:"...":'UTF-8'}</a></h2>
				<span class="ratings">{for $s=1 to 5}{if $posts[i].rating >= $s*20}<i class="icon-star"></i>{elseif $posts[i].rating > (($s-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}<small> (<i class="icon-user gray"></i> <span data-tooltip class="has-tip" data-width="210" title="{$posts[i].rcount} {$lang529}">{$posts[i].rcount}</span>)</small></span>
				<span data-tooltip class="has-tip inline-block ml10" data-width="210" title="{$posts[i].days} {$lang528}"><i class="icon-time"></i> {$posts[i].days}</span>
				<p class="subheading">
					{$posts[i].gdesc|stripslashes|mb_truncate:80:"...":'UTF-8'}
				</p>
			</div>
		</div>
		<span class="hide-for-small">{if $posts[i].days == "0"}{include file='bit_instant.tpl'}{elseif $posts[i].days == "1"}{include file='bit_express.tpl'}{/if}</span>
	</div>
	<div class="large-7 columns hide-for-small">
		<div class="show-for-small">{if $posts[i].feat eq "1"}<span class="block label alert small heading">{$lang531}</span>{/if}{if $posts[i].days == "0"}{include file='bit_instant.tpl'}{elseif $posts[i].days == "1"}{include file='bit_express.tpl'}{/if}</div>
		<h2 class="heading"><a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}">{$posts[i].gtitle|stripslashes|mb_truncate:110:"...":'UTF-8'} <span class="label {if $posts[i].price ne "0"}success{else}alert{/if} radius">{if $posts[i].price eq "0"}{$lang743}{else}{$posts[i].price|stripslashes} {$lang63}{/if}</span></a></h2>
		<p class="subheading">{$posts[i].gdesc|stripslashes|mb_truncate:150:"...":'UTF-8'}
			<span class="author">{$lang414} <a href="{$baseurl}/{insert name=get_seo_profile value=a username=$posts[i].username|stripslashes}">{$posts[i].username|stripslashes|truncate:20:"...":true}</a>&nbsp;<span class="country {$posts[i].country}" title="{insert name=country_code_to_country value=a assign=userc code=$posts[i].country}{$userc}"></span></span></p>
	</div>
	<div class="large-2 hide-for-small columns tcenter ratings">{for $s=1 to 5}{if $posts[i].rating >= $s*20}<i class="icon-star"></i>{elseif $posts[i].rating > (($s-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}<br><small>(<span data-tooltip class="has-tip" data-width="210" title="{$posts[i].rcount} {$lang529}">{$posts[i].rcount}</span>)</small></div>
	<div class="large-1 hide-for-small columns">
	<span data-tooltip class="has-tip" data-width="210" title="{$posts[i].days} {$lang528}"><i class="icon-time"></i> {$posts[i].days}</span></div>
</div>
*}
{/section}
{if !$loadmore}
</div>
{/if}