{literal}
<script>
$(document).ready( function() {
	$('.spam_message').click(function() {
		$.post($(this).attr('href'), null, null, "script");
		return false;
		});
})
</script>
{/literal}


{section name=i loop=$m}
		{if $m[i].unread eq "1"}
		{if $smarty.session.USERID eq $m[i].MSGTO}
		{insert name=mark_read value=a mid=$m[i].MID}
		{/if}
		{/if}
		{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].MSGFROM}
		<div id="message_{$m[i].MID}" class="{if $smarty.session.USERID eq $m[i].MSGFROM}message by-me{else}message by-partner{/if}">
			<div class="row metadata">
				<div class="large-1 small-2 columns tcenter buddyicon">
					<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}">
					{if !$profilepicture}<i class="icon-user icon-large gray"></i>{else}<img alt="{$m[i].mfrom|stripslashes}" src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width=24 height=24>{/if}</a>
				</div>
				<div class="large-2 small-2 columns inb-user-from"><a href="{$baseurl}/user/{$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><strong>{$m[i].mfrom|stripslashes}</strong></a></div>
				<div class="large-9 small-8 columns content-text subheading">
					{if $m[i].PID GT "0"}
						{insert name=gig_details value=a assign=gml pid=$m[i].PID}
						{section name=z loop=$gml}
						{insert name=seo_clean_titles assign=ztitle value=a title=$gml[z].gtitle}
						<p class="related-gig">
							<span>{$lang239} <a href="{$baseurl}/{$gml[z].seo|stripslashes}/{$gml[z].PID|stripslashes}/{$ztitle}">{$lang62} {$gml[z].gtitle|stripslashes} {$lang63}{$gml[z].price|stripslashes}</a></span>
						</p>
						{/section}
						{/if}
						<a href="{$baseurl}/user/{$m[i].mfrom|stripslashes}" title="{$m[i].mfrom|stripslashes}"><strong>{$m[i].mfrom|stripslashes}</strong></a>
						<p{if $m[i].FID GT "0"} class=mb0{/if}>{$m[i].message|stripslashes|nl2br}</p>
						{if $m[i].FID GT "0"}
							{insert name=file_details value=a assign=fd fid=$m[i].FID}
							{section name=x loop=$fd}
							{insert name=gfs value=a assign=afs fid=$fd[x].FID}
							<div class="attached-files">
								<ul>
									<li><i class="icon-paperclip icon-large"></i> <a href="{$baseurl}/files/{$afs.file_path}" target="_blank">{$fd[x].fname} <b>[{$afs.file_size}]</b></a></li>
								</ul>
							</div>
							{/section}
						{/if}
						<div class="msg-footer">
								{if $m[i].MSGFROM ne $smarty.session.USERID}
								<a href="javascript:void(0)" data-href="{$baseurl}/spam?id={$m[i].MID}" class="spam-message" id="spam_message={$m[i].MID}">{$lang245}</a> &bull;
								{/if}
								<time datetime="{$m[i].time|date_format:"%H"}" class="gray s08">{$m[i].time|date_format:"%H:%M %b %d, %Y"}</time>
						</div>
				</div>
		</div>
	</div>
{/section}

<script>
	var errMsg = {if $error}1{else}0{/if};
	var suspected = {if $suspected}1{else}0{/if};
	var clean_message_body = {if $clean_message_body}1{else}0{/if};
	if (suspected) {
		if (confirm('{$lang786}\n{$lang787}')) {
			$('.send-button').parent().append('<input type="hidden" name="suspect_pass" value="1">');
			$('#new_message').submit();
		}
	}
	$('.do-dont').removeClass('do-dont-visible').slideUp();
	if (clean_message_body) {
		document.getElementById('message_body').value = '';
	}
	if (!errMsg) {
		document.getElementById('message_body').value = '';
		enx.messages.remove_attachment();
		do_dont_show = false;
		var sT = $('.by-me').last().offset().top-57;
		$.scrollTo(sT,100);
	}
	else {
		var sT = $('.msg-error').offset().top-57;
		$.scrollTo(sT,100);
		do_dont_show = true;
	}
</script>

{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}
<div class="large-12 small-12 columns">{include file="error.tpl"}</div>
{/if}