<div class="modal" id="how-it-work">
  <div class="close-wrapper close"><a class="close"><i class="icon-remove-sign icon-2x"></i></a></div>
  <div class="row">
  <div class="large-6 columns c1-w">
    <div class="c1">
    <h1 class="heading">{$lang420}</h1>
    <p class="subheading">{$lang421}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang422}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang423}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang424}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang425}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang426}</p>
    </div>
  </div>
  <div class="large-6 columns c2-w">
    <div class="c2">
    <h1 class="heading">{$lang427}</h1>
    <p class="subheading">{$lang428}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang429}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang430}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang431}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang432}</p>
    <div class="tcenter"><i class="icon-chevron-down icon-2x"></i></div>
    <p class="subheading">{$lang433}</p>
    </div>
  </div>
  </div>
  <div class="row m10 subheading"><i class="icon-check-sign green"></i> <a href="{$baseurl}/terms_of_service">{$lang434}</a></div>
  <div class="row m10 subheading"><i class="icon-info-sign green"></i> <a href="{$baseurl}/?startTour">იხილე საიტის გაცნობითი ტური.</a></div>
</div>