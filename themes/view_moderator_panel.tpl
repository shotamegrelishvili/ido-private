<div class="row">
	<div class="large-12 columns subheading">
		<h3 class="heading">ინსტრუქცია:</h3>
		{$p.ginst|stripslashes}
		<h3 class="heading mt20">ტეგები:</h3>
		{$p.gtags|stripslashes}
	</div>
</div>
<div class="row mt20 tcenter success-panel p10 hide" id="panel-notify"></div>
<div class="row mt20 tcenter">
	<div class="large-3 columns">
		<a href="javascript:" data-control="approve" class="deal-view-modcontrol modcontrol-approve button medium subheading radius{if $p.active eq '1'} secondary disabled{else} success{/if}"{if $p.active eq '1'} disabled="disabled"{/if}><i class="icon-check"></i> დადასტურება</a>
	</div>
	<div class="large-3 columns">
		<a href="javascript:" data-control="deactivate" data-needs-explanations="1" class="deal-view-modcontrol modcontrol-deactivate button medium subheading radius{if $p.active eq '5'} secondary disabled{else} orange{/if}"><i class="icon-check-minus"></i> {if $p.active eq '1'}გათიშვა{else}უარყოფა{/if}</a>
	</div>
	<div class="large-3 columns">
		<a href="javascript:" data-control="remove" data-needs-explanations="1" class="deal-view-modcontrol modcontrol-remove button medium subheading radius alert"><i class="icon-trash"></i> წაშლა</a>
	</div>
	<div class="large-3 columns">
		<a href="javascript:" data-control="orders" data-noconfirm="1" class="deal-view-modcontrol modcontrol-orders button medium subheading radius{if $queuecount eq '0'} secondary disabled{else} primary-alt{/if}"><i class="icon-shopping-cart"></i> შეკვეთების სია</a>
	</div>
</div>
<div class="row mt10 hide" id="send-message-form">
	<div class="large-12 columns">
		<form action="#" class="custom geo-box tcenter">
			<textarea name="message" id="message" class="geo ta15" cols="30" rows="10"></textarea>
			<input type="button" id="deal-view-modcontrol-submit" value="OK" data-control="" class="deal-view-modcontrol button large secondary">
		</form>
	</div>
</div>
<div id="modcontrol-orders" class="mt20 hide">
	<div class="row">
		<div class="large-1 columns table-head subheading">OID #</div>
		<div class="large-4 columns table-head subheading tcenter"><i class="icon-user"></i></div>
		<div class="large-2 columns table-head subheading">სტატუსი:</div>
		<div class="large-2 columns table-head subheading">შეკვეთის დრო:</div>
		<div class="large-3 columns table-head subheading">ელოდება:</div>
	</div>
</div>
<div class="hide">
	<div class="modcontrol-order-container">
		<div class="row table-row-hover modcontrol-order-row">
			<div class="large-1 columns modcontrol-oid"></div>
			<div class="large-4 columns modcontrol-username"></div>
			<div class="large-2 columns modcontrol-status"></div>
			<div class="large-2 columns modcontrol-time-added"></div>
			<div class="large-3 columns modcontrol-pr"></div>
		</div>
	</div>
</div>
<div class="separator2 mt15"></div>

<script>
  var baseurl = '{$baseurl}';
  var PID = '{$p.PID}';

  var lang = [];
  lang['sure'] = '{$lang575}';
  lang['deactivate'] = '{if $p.active eq '1'}{$lang666|@sprintf:$p['username']:$ogurl}{else}{$lang667|@sprintf:$p['username']:$ogurl}{/if}';
  lang['remove'] = '{if $p.active eq '1'}{$lang669|@sprintf:$p['username']:$p.gtitle}{else}{$lang668|@sprintf:$p['username']:$p.gtitle}{/if}';
  var status_matrix={literal}{{/literal}{foreach from=$status_matrix item=s key=k}{$k}:'{$s}',{/foreach}{literal}}{/literal};
  //lang['maxlength'] = '{$lang635}';
  var def_js = ['{$baseurl}/js/view_admin.js?v=006'];
</script>