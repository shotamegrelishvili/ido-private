
<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/settings" class=current>{$lang29}</a>
	</ul>
</div>

<div class="row mt20">
	<div class="large-12 small-12 columns">
		<h1 class="heading h-large mb0">{$lang29}</h1>
		<h2 class="subheading">{$lang550}</h2>
	</div>
</div>

{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="row"><div class="large-12 small-12 columns">{include file="error.tpl"}</div></div>{/if}

{if $smarty.server['HTTP_HOST'] != 'localhost'}
<script src="https://www.emoney.ge/js/econnect/v1/lib.js"></script>
<script src="https://www.emoney.ge/js/econnect/v1/cl.js"></script>

<iframe id="eMoneyAuthorizeWidgetFrame" src="https://www.emoney.ge/index.php/widget/login_common?lang=ka&amp;protocol={$smarty.server['HTTP_X_FORWARDED_PROTO']}&amp;domain={$smarty.server['HTTP_HOST']}&amp;external_registration=0" style="position:fixed;top:0;left:0;z-index:998;height:100%;width:100%;border:0px;display:none;"></iframe>
{/if}

<div class="work-area-wrapper mt10">
	<div class="row">
		<div class="large-4 small-12 columns">
		{if $currentProfileProgress LT 100}
			<div class="tcenter">
				<div id="circle-progress"></div>
				<span class="sp-get-bonus-msg">
					შეავსე პროფილი და გაააქტიურე დამატებითი ფუნქციები!
				</span>
			</div>
		{/if}
			<ul class="wa-side-bar">
				<li><a href="javascript:" class="{if !$smarty.get.tab OR $smarty.get.tab=='basic'}wa-sb-active{/if} wa-switcher" data-target="basic-settings">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
				<path d="M0 0h24v24h-24z" fill="none"/>
				</svg>
				ძირითადი პარამეტრები</a></li>
				<li><a href="javascript:" class="wa-switcher" data-target="contact-settings">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M0 0h24v24h-24z" fill="none"/>
				<path d="M20 15.5c-1.25 0-2.45-.2-3.57-.57-.35-.11-.74-.03-1.02.24l-2.2 2.2c-2.83-1.44-5.15-3.75-6.59-6.58l2.2-2.21c.28-.27.36-.66.25-1.01-.37-1.12-.57-2.32-.57-3.57 0-.55-.45-1-1-1h-3.5c-.55 0-1 .45-1 1 0 9.39 7.61 17 17 17 .55 0 1-.45 1-1v-3.5c0-.55-.45-1-1-1zm-8-12.5v10l3-3h6v-7h-9z"/>
				</svg>
				საკონტაქტო ინფორმაცია</a></li>
				<li><a href="javascript:" class="wa-switcher{if $smarty.get.tab=="experience"} wa-sb-active{/if}" data-target="experience-settings">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M0 0h24v24h-24z" fill="none"/>
				<path d="M5 13.18v4l7 3.82 7-3.82v-4l-7 3.82-7-3.82zm7-10.18l-11 6 11 6 9-4.91v6.91h2v-8l-11-6z"/>
				</svg>
				კომპეტენცია &amp; გამოცდილება</a></li>
				<li><a href="javascript:" class="wa-switcher{if $smarty.get.tab=="portfolio"} wa-sb-active{/if}" data-target="portfolio-settings">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M0 0h24v24h-24z" fill="none"/>
				<path d="M22 16v-12c0-1.1-.9-2-2-2h-12c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2zm-11-4l2.03 2.71 2.97-3.71 4 5h-12l3-4zm-9-6v14c0 1.1.9 2 2 2h14v-2h-14v-14h-2z"/>
				</svg>
				პორტფოლიო</a></li>
				<li class="sp-sb-highlight highlight-inactive"><a href="javascript:" class="wa-switcher{if $smarty.get.tab=="value-added"} wa-sb-active{/if}" data-target="value-added-settings">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M0 0h24v24h-24z" fill="none"/>
				<path d="M11.99 2c-5.52 0-9.99 4.48-9.99 10s4.47 10 9.99 10c5.53 0 10.01-4.48 10.01-10s-4.48-10-10.01-10zm4.24 16l-4.23-2.55-4.23 2.55 1.12-4.81-3.73-3.23 4.92-.42 1.92-4.54 1.92 4.53 4.92.42-3.73 3.23 1.12 4.82z"/>
				</svg>
				დამატებითი ფუნქციები</a></li>
			</ul>
		</div>
	<div class="large-8 small-12 columns">
		<form action="{$baseurl}/settings" class="row work-area-pad work-area" enctype="multipart/form-data" id="edit_user" method="post">
			<!-- Basic settings -->
			<div class="basic-settings wa-target{if $smarty.get.tab AND $smarty.get.tab!='basic'} hide{else} form-tab-active{/if}">
			<div class="wa-message"></div>
			<div class="row">
				<div class="large-2 small-4 columns tcenter g-user-avatar-wrapper ua-100x100-wrapper" id="basic" data-accept="image/png,image/jpeg">
					{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$smarty.session.USERID}
					{if $profilepicture}<img src="{$membersprofilepicurl}/{$profilepicture}" alt="">{else}<i class="icon-user icon-5x"></i>{/if}
				</div>
				<div class="large-4 small-8 columns mt10 subheading upload-wrapper">
					<a class="browsefile regupload mt10" data-target="basic">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path d="M19.35 10.04c-.68-3.45-3.71-6.04-7.35-6.04-2.89 0-5.4 1.64-6.65 4.04-3.01.32-5.35 2.87-5.35 5.96 0 3.31 2.69 6 6 6h13c2.76 0 5-2.24 5-5 0-2.64-2.05-4.78-4.65-4.96zm-5.35 2.96v4h-4v-4h-3l5-5 5 5h-3z"/>
					</svg> ატვირთვა</a>
					<span class="block">{$lang86}</span>
					<input name="gphoto" type="file" class="basic-file-input file-input req-cc" accept="image/png,image/jpeg" data-initial="{$profilepicture}">
				</div>
				<div class="large-6 small-6 columns tcenter">
					{if $p.level==1}
					<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="gray mt10">
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path d="M3 5h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm11 10h2v-10h-4v2h2v8zm7-14h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14z"/>
					</svg>
					{elseif $p.level==2}
					<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="gray">
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path d="M3 5h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm18-4h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14zm-4-4h-4v-2h2c1.1 0 2-.89 2-2v-2c0-1.11-.9-2-2-2h-4v2h4v2h-2c-1.1 0-2 .89-2 2v4h6v-2z"/>
					</svg>
					{elseif $p.level==3}
					<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path d="M21 1h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14zm-18-12h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm14 8v-1.5c0-.83-.67-1.5-1.5-1.5.83 0 1.5-.67 1.5-1.5v-1.5c0-1.11-.9-2-2-2h-4v2h4v2h-2v2h2v2h-4v2h4c1.1 0 2-.89 2-2z"/>
					</svg>
					{/if}<br>
					<span class="subheading">დონე</span>
				</div>
			</div>
			{if $p.e_account_type=='business'}
			<div class="row mt20">
				<div class="large-8 large-centered small-12 columns p10">
					<span class="subheading"><i class="icon-exclamation-sign red icon-large"></i> შენს ანგარიშზე მოქმედებს შეზღუდვა:<br>
					- <u>შეგიძლია</u> გასწიო მომსახურება და მიიღო ანაზღაურება.<br>
					- <u>არ შეგიძლია</u> სხვის მიერ შემოთავაზებული მომსახურების შეძენა.<br>
					<div class="tright mt5 p10">
						<a href="{$baseurl}/faq?p=buyer&amp;t=businessaccount" class="button small secondary mb0">გაეცანი დეტალებს</a>
					</div>
					</span>
				</div>
			</div>
			{/if}
			<div class="row mt20">
				<div class="large-6 small-6 columns">
					<label for="fname" class="subheading"><i class="icon-male"></i> {$lang161} *<a href="javascript:" class="right settings-help" data-goal="name"><i class="icon-question-sign red icon-large"></i></a></label>
					<div class="geo-box"><input class="req-cc text geo{if $csserror.fname} field-err{/if}" id="fname" name="fname" size="30" type="text" value="{if $smarty.post.fname}{$smarty.post.fname|stripslashes}{else}{$p.fullname|stripslashes}{/if}" maxlength="40" pattern="[{$lang836}-]{literal}{2,}{/literal} [{$lang836}-]{literal}{2,}{/literal}" required data-initial="{$p.fullname|stripslashes}"></div>
				</div>
				<div class="large-6 small-6 columns">
					<label for="username" class="subheading"><i class="icon-user"></i> {$lang554} *<a href="javascript:" class="right settings-help" data-goal="username"><i class="icon-question-sign red icon-large"></i></a></label>
					<input class="text" id="username" name="username" size="30" type="text" value="{if $smarty.post.username}{$smarty.post.username|stripslashes}{else}{$p.username|stripslashes}{/if}" disabled required>
				</div>
			</div>
			<div class="row">
				<div class="large-6 small-6 columns">
					<label for="pin" class="subheading"><i class="icon-list-alt"></i> {$lang551} *<a href="javascript:" class="right settings-help" data-goal="pin"><i class="icon-question-sign red icon-large"></i></a></label>
					<input class="req-cc text mb0{if $csserror.pin OR ($p.pin=='' AND (!$smarty.post.pin OR strlen($smarty.post.pin)<11 OR !is_numeric($smarty.post.pin)))} field-err{/if}" id="pin" data-restrict-leave="pin" data-value="{$p.pin|stripslashes}" name="pin" size="11" maxlength="11" type="text" pattern="[0-9]{literal}{11}{/literal}" value="{if $smarty.post.pin}{$smarty.post.pin|stripslashes}{else}{$p.pin|stripslashes}{/if}" required{if $p.pin} disabled{/if} data-initial="{$p.pin}">
					<span class="subheading s08 block mt2 mb10">{$lang552}</span>
				</div>
				<div class="large-6 small-6 columns">
					<label for="emoney" class="subheading"><i class="icon-credit-card"></i> {$lang162} *<a href="javascript:" class="right settings-help" data-goal="emoney"><i class="icon-question-sign red icon-large"></i></a></label>
					<input class="req-cc text{if $csserror.emoney || $p.eemail==''} field-err{/if}" id="emoney" name="emoney" size="30" type="email" data-restrict-leave="emoney" data-value="{$p.eemail|stripslashes}" value="{if $smarty.post.emoney}{$smarty.post.emoney|stripslashes}{else}{$p.eemail|stripslashes}{/if}" required{if $p.eemailprotect eq "1"} disabled{/if}>
				</div>
			</div>
			<div class="row">
			<div class="large-6 small-6 columns">
				<label for="location" class="subheading"><i class="icon-map-marker"></i> {$lang553} *</label>
				<select id="location" name="location" class="req-cc text fat-select subheading{if $csserror.location} field-err{/if}" required data-initial="{$p.location}">
					<option value=""{if !$smarty.post.location && !$p.location} selected{/if}>{$lang67}</option>
					<option value="თბილისი"{if ($smarty.post.location && $smarty.post.location=="თბილისი") OR (!$smarty.post.location && $p.location=="თბილისი")} selected{/if}>თბილისი</option>
					<option value="ქუთაისი"{if ($smarty.post.location && $smarty.post.location=="ქუთაისი") OR (!$smarty.post.location && $p.location=="ქუთაისი")} selected{/if}>ქუთაისი</option>
					<option value="ბათუმი"{if ($smarty.post.location && $smarty.post.location=="ბათუმი") OR (!$smarty.post.location && $p.location=="ბათუმი")} selected{/if}>ბათუმი</option>
					<option value="რუსთავი"{if ($smarty.post.location && $smarty.post.location=="რუსთავი") OR (!$smarty.post.location && $p.location=="რუსთავი")} selected{/if}>რუსთავი</option>
					<optgroup label="----">
						<option value="სხვა"{if ($smarty.post.location && $smarty.post.location=="სხვა") OR (!$smarty.post.location && $p.location=="სხვა")} selected{/if}>სხვა</option>
					</optgroup>
				</select>
			</div>
			</div>
			<div class="row">
			<div class="large-6 small-6 columns">
				<label class="subheading"><i class="icon-link"></i> {$lang160}<a href="javascript:" id="profileurl" class="right settings-help" data-goal="profileurl"><i class="icon-question-sign red icon-large"></i></a></label>
			</div>
			</div>
			<div class="row">
				<div class="large-6 small-6 columns">
					{insert name=get_seo_profile value=a username=$smarty.session.USERNAME|stripslashes assign=profileurl}
					<a href="{$baseurl}/{$profileurl}" class="mt5">{$baseurl}/{$profileurl}</a>
				</div>
				<div class="large-6 small-6 columns">
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style mt3">
					<a class="addthis_button_facebook_share" fb:share:layout="button_count" addthis:url="{$baseurl}/{$profileurl}" addthis:title="{$site_name}: {$lang555}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
					<a class="addthis_button_tweet" addthis:url="{$baseurl}/{$profileurl}" addthis:title="{$lang555} #{$site_name}-ზე. (@iDoGeorgia) " addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
					<a class="addthis_counter addthis_pill_style" addthis:url="{$baseurl}/{$profileurl}" addthis:title="{$site_name}: {$lang555}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
					</div>
					<!-- AddThis Button END -->
				</div>
			</div>
			</div>
			<!-- Basic Settings end -->
			
			<!-- Contact Settings -->
			<div class="contact-settings wa-target hide">
				<div class="wa-message"></div>
				<div class="row">
					<div class="large-12 small-12 columns">
						<label for="email" class="subheading"><i class="icon-envelope"></i> {$lang4} *<a href="javascript:" class="right settings-help" data-goal="email"><i class="icon-question-sign red icon-large"></i></a></label>
						<input class="req-cc text mb0{if $csserror.email} field-err{/if}" id="email" name="email" size="30" type="email" value="{if $smarty.post.email}{$smarty.post.email|stripslashes}{else}{$p.email|stripslashes}{/if}" required data-initial="{$p.email|stripslashes}">
						{if $smarty.session.VERIFIED eq "0"}<span class="subheading s08 block mt2 mb10 tright"><i class="icon-reply-all"></i> <a href="{$baseurl}/resendconfirmation.php">{$lang480}</a></span>
						{else}
						<span class="subheading s08 block mt2 mb10">{$lang749}</span>
						{/if}
					</div>

					<div class="large-12 small-12 columns">
						<label for="mobile" class="subheading"><i class="icon-mobile-phone"></i> {$lang762} *<a href="javascript:" class="right settings-help" data-goal="mobile"><i class="icon-question-sign red icon-large"></i></a></label>
						<input type="text" class="req-cc text mb0{if $csserror.mobile} field-err{/if}" id="mobile" name="mobile" size="9" maxlength="9" value="{if $smarty.post.mobile}{$smarty.post.mobile|stripslashes}{else}{$p.mobile|stripslashes|substr:-9}{/if}" placeholder="599 001122" pattern="[0-9]{literal}{9}{/literal}" required data-initial="{$p.mobile|stripslashes|substr:-9}">
						<span class="subheading s08 block mt2 mb10">{$lang764}</span>
					</div>
				</div>
			</div>
			<!-- Contact Settings end -->

			<!-- Experience Settings -->
			<div class="experience-settings wa-target{if !$smarty.get.tab OR $smarty.get.tab!='experience'} hide{/if}">
				<div class="wa-message"></div>
				<div class="row">
					<div class="large-12 small-12 columns">
						<label for="experience" id="exp-label" class="subheading"><span class="exp-h" data-target="1">ძირითადი</span> კომპეტენცია/გამოცდილება</label>
						<span class="exp-error hide">შეცდომა</span>
						<div class="geo-box ac-wrapper">
							<input type="text" id="experience" name="experience" class="big-input experience auto-complete geo" data-target="experience" data-minlength="2" placeholder="ჩაწერე რა ცოდნას და გამოცდილებას ფლობ..." autocomplete="off">
							<div class="ac-container"></div>
							<div class="ac-tmpl hide">
								<a class="ac-row" href="javascript:" data-mid="%kID%" data-aid="%aID%">
									<img src="{$baseurl}/images/ic_school.svg" alt="">%k% %a%
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row collapse subheading tcenter">
					<div class="large-3 small-12 columns experience-col exp-empty exp-queue{if $exp.1} exp-chosen{/if}" id="exp-1" data-col=1 data-exp="{$exp.1['KID']}-{$exp.1['AID']}" data-title="ძირითადი">
						<span class="exp-sub">
					{if !$exp.1}
						კომპეტენცია<br>
						<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="gray mt10">
						<path d="M0 0h24v24h-24z" fill="none"/>
						<path d="M3 5h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm11 10h2v-10h-4v2h2v8zm7-14h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14z"/>
						</svg>
					{else}
						<img src="{$baseurl}/images/ic_school.svg" alt="">{$exp.1.title}
					{/if}
						</span>
						<input name="exp[]" id="exp-i-1" value="KID={$exp.1['KID']}|AID={$exp.1['AID']}" type="hidden">
					</div>
					<div class="large-3 large-offset-1 small-12 columns experience-col exp-empty{if $exp.1} exp-queue{/if}{if $exp.2} exp-chosen{/if}" id="exp-2" data-col=2 data-exp="{$exp.2['KID']}-{$exp.2['AID']}" data-title="მეორადი">
						<span class="exp-sub">
					{if !$exp.2}
						კომპეტენცია<br>
						<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="gray mt10">
						<path d="M0 0h24v24h-24z" fill="none"/>
						<path d="M3 5h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm18-4h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14zm-4-4h-4v-2h2c1.1 0 2-.89 2-2v-2c0-1.11-.9-2-2-2h-4v2h4v2h-2c-1.1 0-2 .89-2 2v4h6v-2z"/>
						</svg>
					{else}
						<img src="{$baseurl}/images/ic_school.svg" alt="">{$exp.2.title}
					{/if}
						</span>
						<input name="exp[]" id="exp-i-2" value="KID={$exp.2['KID']}|AID={$exp.2['AID']}" type="hidden">
					</div>
					<div class="large-3 large-offset-1 small-12 columns experience-col exp-empty{if $exp.1 AND $exp.2} exp-queue{/if}{if $exp.3} exp-chosen{/if}" id="exp-3" data-col=3 data-exp="{$exp.3['KID']}-{$exp.3['AID']}" data-title="დამატებითი">
						<span class="exp-sub">
					{if !$exp.3}
						კომპეტენცია<br>
						<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="gray mt10">
						<path d="M0 0h24v24h-24z" fill="none"/>
						<path d="M21 1h-14c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-14c0-1.1-.9-2-2-2zm0 16h-14v-14h14v14zm-18-12h-2v16c0 1.1.9 2 2 2h16v-2h-16v-16zm14 8v-1.5c0-.83-.67-1.5-1.5-1.5.83 0 1.5-.67 1.5-1.5v-1.5c0-1.11-.9-2-2-2h-4v2h4v2h-2v2h2v2h-4v2h4c1.1 0 2-.89 2-2z"/>
						</svg>
					{else}
						<img src="{$baseurl}/images/ic_school.svg" alt="">{$exp.3.title}
					{/if}
						</span>
						<input name="exp[]" id="exp-i-3" value="KID={$exp.3['KID']}|AID={$exp.3['AID']}" type="hidden">
						<input type="hidden" name="exp_arr" id="exp-arr" class="req-cc" data-initial="KID={$exp.1['KID']}|AID={$exp.1['AID']},KID={$exp.2['KID']}|AID={$exp.2['AID']},KID={$exp.3['KID']}|AID={$exp.3['AID']}" value="KID={$exp.1['KID']}|AID={$exp.1['AID']},KID={$exp.2['KID']}|AID={$exp.2['AID']},KID={$exp.3['KID']}|AID={$exp.3['AID']}" data-emptyif="KID=|AID=,KID=|AID=,KID=|AID=">
					</div>
				</div>
				<div class="row">
					<div class="large-12 small-12 columns">
						<label for="details" class="subheading">{$lang163}</label>
						<div class="geo-box">
							<textarea cols="10" id="details" name="details" rows="30" class="req-cc slideDown ta6 geo{if $csserror.details} field-err{/if}" data-initial="{$p.description|stripslashes}">{if $smarty.post.details}{$smarty.post.details}{else}{$p.description|stripslashes}{/if}</textarea>
						</div>
					</div>
				</div>
			</div>
			<!-- Experience Settings end -->

			<!-- Portfolio Settings -->
			<div class="portfolio-settings wa-target{if !$smarty.get.tab OR $smarty.get.tab!='portfolio'} hide{/if}">
				<div class="wa-message"></div>
			{if $portfolio}
				<div class="large-12 small-12 subheading columns tcenter plupload_view_thumbs plupload_content">
					<ul class="ui-sortable" id="uploaded-portfolio">
					{section name=p loop=$portfolio}
					{insert name=gfs value=a assign=afs fid=$portfolio[p].FID}
					<li class="plupload_file" id="{$afs.FID}" data-id="{$afs.FID}"><div class="plupload_file_thumb">{if strpos($afs.file_type,'image')!==false}<img src="{$baseurl}/files/{$afs.file_path}" alt=""{$ratio=$afs.file_width/300}{if ($afs.file_height/$ratio) < 150} style="min-height:100%;height:100%;max-width:none;"{/if}>{else}<div class="plupload_file_dummy"><span>{$afs.file_ext}</span></div>{/if}</div><div class="plupload_file_name" title="{$afs.file_name}"><span class="plupload_file_name_wrapper">{$afs.file_name}</span></div><div class="plupload_file_action thumb-file-action"><div class="plupload_action_icon thumb-file-action-icon wa-file-remove" data-id="{$afs.FID}" data-fid="{$afs.FID}">X</div></div><div class="plupload_file_size">{$afs.file_size}</div><div class="plupload_file_fields"> </div></li>
					{sectionelse}
					{/section}
					</ul>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row separator mt20"></div>
				<div class="row">&nbsp;</div>
			{/if}
				<div class="large-12 small-12 subheading columns tcenter upload-wrapper">
					<div id="portfolio" class="plupload-block g-gallery-multi" data-accept="{$portfolio_allowed_types}" data-multiple="1"><span id="no-html5-support">თქვენს ბრაუზერს არ აქვს ფაილების ატვირთვის საჭირო მხარდაჭერა.</span></div>
					<div id="container" class="upload-container dropzone mb10 rel" data-target="portfolio">
					<span id="allowed-types" class="hide">{$lang822|sprintf:($allowed_extensions|strtoupper):$maxUploadSize|replace:',JPEG':''}</span></div>
					<input type="hidden" id="pfile-req" name="pfile-req" class="req-cc"{if $portfolio} value="1"{/if}>
				</div>
			</div>
			<!-- Portfolio Settings end -->

			<!-- Value-added Settings -->
			<div class="value-added-settings wa-target{if !$smarty.get.tab OR $smarty.get.tab!='value-added'} hide{/if}">
				<div class="wa-message"></div>
			{if $currentProfileProgress LT 100}
				<div class="row collapse">
					<div class="large-12 small-12 subheading columns">
						<span class="txt-highlight"><i class="icon-arrow-left"></i> <span class="sp-progress">{$currentProfileProgress}</span>%</span>
						დამატებითი ფუნქციების გასააქტიურებლად, სრულყოფილად შეავსე პროფილი.
					</div>
				</div>
				<div class="row">&nbsp;</div>
				<div class="row separator mt20"></div>
			{else}
			<div class="tcenter subheading f1">
			{if $free_min_invites - $resultFields.members_friends['invites'] > 0}
				<input type="hidden" name="invite-req" class="req-cc">
				<span id="invite-txt">მოიწვიე საიტზე 
				<span id="invite-more" data-txt="კიდევ"{if $resultFields.members_friends['invites'] AND $free_min_invites - $resultFields.members_friends['invites'] > 0 AND $free_min_invites > $resultFields.members_friends['invites']} class="txt-highlight">კიდევ {else}>{/if}
				{$free_min_invites - $resultFields.members_friends['invites']}</span> მეგობარი და დამატებითი ფუნქციები გააქტიურდება:</span>
			{else}
				სურვილისამებრ, გამოხატე პროექტის მხარდაჭერა და მოიწვიე მეტი მეგობარი! 
			{/if}
				<div class="mt20">
					<div id="fb-send-btn" class="fb-send" data-href="{$refLink}" data-ref="2015-Invite5-GetBonus"></div>
				</div>
				<div class="mt10">
					[<span id="invited-friends">{$resultFields.members_friends['invites']+0}</span> / 5]
				</div>
				<div id="invite-sep" class="row separator mt20"></div>
			{if $free_min_invites - $resultFields.members_friends['invites'] > 0}
				<div id="invite-thanks" class="mt20 hide">
					<strong>მადლობა!</strong><br>
					შეგიძლია ჩართო სასურველი ფუნქცია:<br>
					<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" class="green">
					<path d="M16.59 8.59l-4.59 4.58-4.59-4.58-1.41 1.41 6 6 6-6z"/>
					<path d="M0 0h24v24h-24z" fill="none"/>
					</svg>
				</div>
			{/if}
			</div>
			{/if}
				<div class="row collapse mt20">
					<div class="large-4 small-4 columns tcenter togglebutton">
						<label for="setownprice">
							საკუთარი ფასი
							<input id="setownprice" name="setownprice" type="checkbox" value="1"{if $currentProfileProgress LT 100 OR $free_min_invites - $resultFields.members_friends['invites'] > 0} disabled{/if}{if $services.setownprice.status} checked{/if}><span class="toggle"></span>
						</label>
					</div>
					<div class="large-8 small-8 subheading columns gray">
						შეთავაზებაზე საკუთარი ფასის დაწესების შესაძლებლობა.<a href="javascript:" class="sup settings-help" data-goal="value-added-ownprice"><i class="icon-question-sign red icon-large"></i></a>
					</div>
				</div>
				<div class="row collapse mt30">
					<div class="large-4 small-4 columns tcenter togglebutton">
						<label for="zerocommission">
							საკომისიო 0%
							<input id="zerocommission" name="zerocommission" type="checkbox" value="1"{if $currentProfileProgress LT 100 OR $free_min_invites - $resultFields.members_friends['invites'] > 0} disabled{/if}{if $services.zerocommission.status} checked{/if}><span class="toggle"></span>
						</label>
					</div>
					<div class="large-8 small-8 subheading columns gray">
						ჩაგერიცხება გამომუშავებული თანხის 100%.<a href="javascript:" class="sup settings-help" data-goal="value-added-zerocommission"><i class="icon-question-sign red icon-large"></i></a>
					</div>
				</div>
			</div>
			<!-- Value-added Settings end -->

			<div class="row">&nbsp;</div>
			<div class="row separator mt20"></div>
			<div class="row">
			 <div class="large-12 small-12 columns tcenter mt10 submit-wrapper">
				<input type="submit" value="{if $smarty.get.tab == 'portfolio'}{$lang748}{else}{$lang835}{/if}" class="{if $smarty.get.tab == 'portfolio'}send-button{else}next-button{/if} button large radius subheading">
				<div class="progress-indicator-icon-message hide">
						{include file="loader.tpl"}
				</div>
			 </div>
			</div>
			{if $smarty.get.r}<input type="hidden" name="r" value="{$smarty.get.r}">{/if}
			<input type="hidden" name="subform" value="1">
			<input type="hidden" id="econnect_signup" name="econnect_signup" value="0">
			<input type="hidden" id="econnect_email" name="econnect_email" value="">
			<input type="hidden" id="econnect_password" name="econnect_password" value="">
			<input type="hidden" id="has-unsaved-changes" value="0">
			<input type="hidden" id="action-tab" name="tab" value="basic">
			</form>
			</div>
			</div>
</div>

<ol id="eConnect" class="joyride-list" data-joyride>
<li data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade;nextButton:false">
<form class="row eConnect-form" method="post">
		<label class="heading"><i class="icon-envelope gray"></i> ელფოსტა:</label>
		<input type="email" name="email" class="eConnect-email" placeholder="personal@email.ge" required>
		<label class="heading"><i class="icon-key gray"></i> პაროლი:</label>
		<input type="password" name="password" class="eConnect-password" required><br>
		<input type="hidden" name="econnect_signup" value="1">
		<a class="button medium secondary radius expand econnect-btn" href="javascript:"><span class="lat light-gray"><strong class=h-large><i>e</i></strong> </span> <span class="h-large">Connect</span></a>
		<span class="subheading block mt10 s08">არ გაქვს ანგარიში eMoney-ს სისტემაში?<br><i class="icon-chevron-sign-right"></i> <a href="https://www.emoney.ge/index.php/main/signup" target="_blank">შექმენი ანგარიში ახლა</a>.</span>
</form>
</li>
</ol>

<script>
	var eMoneyWidgetEnable={if $smarty.server['HTTP_HOST'] != 'localhost'}1{else}0{/if};
	var eMoneyProcess='update';
	var startImportantInfoTour = true;
	var checkLeave = {if $smarty.get.check}1{else}0{/if};
	var settingsPage=true;
	var confirmLeave = { pin: '{$lang746}', emoney: '{$lang747}', has_unsaved_changes: '{$lang821}' };

	var maxUploadSize = {$maxUploadSize},
	maxUploads = {$maxUploads-sizeof($portfolio)},
	afterUploadFunction = 'requireAjaxUploader',
	action = 'portfolio',
	uploadURI = '/upload.php?target=portfolio',
	allowed_extensions = '{$allowed_extensions}',
	currentProfileProgress = '{$currentProfileProgress}',
	free_min_invites = {$free_min_invites},
	already_invited = {$resultFields.members_friends['invites']+0},
	currentTab='{$userSelectedTab}';
	//fireFunc = '{if $smarty.get.tab == "portfolio"}imageAlign{/if}';

	var lang = { fileTypeNotAllowed : '{$lang823}', fileSizeTooLarge : '{$lang824}', maxUploadsRiched : '{$lang825|sprintf:$maxUploads}', expAlreadyChosen : '{$lang831}', 'noversions' : '{$lang834}', success : '{$lang828}', next : '{$lang835}', submit : '{$lang748}' };
	var def_js = ['{$baseurl}/js/jquery.scrollTo-min.js?v=2','{$baseurl}/js/settings.js?v=15','{$baseurl}/js/submitprocessor.js?v=7','//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-522ee0e443fae154','{$baseurl}/js/econnect.js?v=4','{$baseurl}/js/regupload.js?v=008'];
</script>

<div id="value-added-ownprice-info-popup">
<ol id="value-added-ownprice" class="joyride-list" data-joyride>
	<li data-id="setownprice" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade">
		<h4 class=subheading>საკუთარი ფასი</h4>
		<p class="subheading s08 mb0">ჩართე ეს ფუნქცია თუ გინდა, რომ თავისუფალი იყო საკუთარ განცხადებებზე ანაღზაურების დაწესებისას.<br>
		ფუნქციის ჩართვით მოიხსნება მაქსიმუმ 100 ლარი ანაზღაურების შეზღუდვა.</p>
	</li>
</ol>
</div>

<div id="value-added-zerocommission-info-popup">
<ol id="value-added-zerocommission" class="joyride-list" data-joyride>
	<li data-id="zerocommission" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade">
		<h4 class=subheading>0% საკომისიო</h4>
		<p class="subheading s08 mb0">დარწმუნებული ვართ, გსურს, რომ გამომუშავებული თანხა 100% შენი იყოს :)<br>ჩართე ეს ფუნქცია და <span class="txt-highlight">2015 წლის 31 მაისამდე</span> საიტის საკომისიო იქნება <span class="txt-highlight red">0%</span> - მომსახურების საკომისიოსაგან განთავისუფლებული იქნება ფრილანსერიც და დამკვეთიც!<br><br>
		<span class="gray">
		გაითვალისწინე: "საკომისიო 0%" გააქტიურების გარეშე, საიტის საკომისიო შეადგენს 10%, მინ. 1 ლარს, რომელიც იქვითება ფრილანსერის გამომუშავებული ანაზღაურებიდან.</span></p>
	</li>
</ol>
</div>

<div id="pin-info-popup">
<ol id="pin-info" class="joyride-list" data-joyride>
	<li data-id="pin" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
		<h4 class=subheading>პირადი ნომერი</h4>
		<p class="subheading s08 mb0">სწორი პირადი ნომრის მითითება აუცილებელია როგორც გამომუშავებული თანხის გასანაღდებლად, ასევე გადახდილი თანხის დაბრუნებისათვის.</p>
	</li>
</ol>
</div>

<div id="emoney-info-popup">
<ol id="emoney-info" class="joyride-list" data-joyride>
	<li data-id="emoney" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>ანგარიში eMoney-ს სისტემაში</h4>
		<p class="subheading s08">საიტზე ანგარიშსწორება შესაძლებელია მხოლოდ ელექტრონული საფულის - <a href="https://www.emoney.ge/index.php/content/faq" target="_blank" rel="nofollow">eMoney</a>-ს საშუალებით. eMoney-ს სისტემაში <a href="https://www.emoney.ge/index.php/main/signup" target="_blank" rel="nofollow">რეგისტრაცია</a> მარტივი და უფასოა.</p>
	</li>
</ol>
</div>

<div id="mobile-info-popup">
<ol id="mobile-info" class="joyride-list" data-joyride>
	<li data-id="mobile" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>{$lang762}</h4>
		<p class="subheading s08">{$lang763}</p>
	</li>
</ol>
</div>

<div id="name-info-popup">
<ol id="name-info" class="joyride-list" data-joyride>
	<li data-id="fname" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>სახელი და გვარი</h4>
		<p class="subheading s08">სახელი და გვარი იწერება ელექტრონულ ხელშეკრულებაში, რომელიც ფორმდება ყოველ შეკვეთაზე. გადაამოწმე და სწორად მიუთითე შენი სახელი და გვარი.</p>
	</li>
</ol>
</div>

<div id="username-info-popup">
<ol id="username-info" class="joyride-list" data-joyride>
	<li data-id="username">
		<h4 class=subheading>მეტსახელი</h4>
		<p class="subheading s08">მეტსახელი საჯაროა და ხილულია შეკვეთის გაფორმებამდე. მეტსახელის შეცვლის საჭიროების შემთხვევაში, <a href="{$baseurl}/contact">დაგვიკავშირდი</a>.</p>
	</li>
</ol>
</div>

<div id="profileurl-info-popup">
<ol id="profileurl-info" class="joyride-list" data-joyride>
	<li data-id="profileurl">
		<h4 class=subheading>პროფილის მისამართი</h4>
		<p class="subheading s08">გაუზიარე შენი პროფილის მისამართი მეგობრებს სოციალური ქსელების საშუალებით. რაც უფრო პოპულარულია შენი პროფილი, მით უკეთესი :)</p>
	</li>
</ol>
</div>

<div id="desc-info-popup">
<ol id="desc-info" class="joyride-list" data-joyride>
	<li data-id="details" data-button="შემდეგი" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>ინფორმაცია შენს შესახებ</h4>
		<p class="subheading s08">დისტანციური საქმიანობისათვის მოკლედ აღწერე შენი გამოცდილება და კომპეტენცია. ეს დაეხმარება შენს პოტენციურ პარტნიორს გადაწყვეტილების მიღებაში.</p>
	</li>
</ol>
</div>

<div id="email-info-popup">
<ol id="email-info" class="joyride-list" data-joyride>
	<li data-id="email" data-button="OK" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
		<h4 class=subheading>საკონტაქტო ელფოსტა</h4>
		<p class="subheading s08">მიუთითე ელფოსტა, რომელსაც სისტემატიურად ამოწმებ და რომელზეც შესაძლებელია შენთან უმოკლეს ვადაში დაკავშირება.</p>
	</li>
</ol>
</div>