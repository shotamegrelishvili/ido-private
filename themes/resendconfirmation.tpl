<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/settings" class=current>{$lang31}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang705}</h1>
    <h2 class="subheading">{$lang406}</h2>
  </div>
  {if $message ne "" OR $error ne ""}<div class="large-12 small-12 columns mt20 mb20">{include file="error.tpl"}</div>{/if}
</div>