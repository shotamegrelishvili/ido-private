<li class="pm-d"></li>{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$notifications[n].msgfrom}{insert name=get_username_from_userid assign=username value=var USERID=$notifications[n].msgfrom}{insert name=get_past_period assign=past_period value=var t=$notifications[n].time precise=1}
{if $notifications[n].action == 'new_pm'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_message.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: ახალი წერილი<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'task_pm' || $notifications[n].action == 'mutual_reject'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_forum.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: ახალი შეტყობინება<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'seller_has_new_order'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_shopping_basket.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: ახალი შეკვეთა<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'predelivery'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_spellcheck.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: შეაფასე შესრულებული სამუშაოს წინასწარი/შავი ვერსია<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'predelivery_accept'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_spellcheck.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: სამუშაოს წინასწარი ვერსია კარგია! ჩააბარე სამუშაო სრულად.<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'predelivery_reject'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_block.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: დაიწუნა სამუშაოს წინასწარი/შავი ვერსია. გამოასწორე ხარვეზები!<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'delivery'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_done.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: ჩაიბარე და შეაფასე შესრულებული სამუშაო!<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'complete'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_done_all.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: სამუშაო მიღებულია. იხილე დამკვეთის შეფასება.<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'rejection'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_block.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: დაიწუნა შესრულებული სამუშაო. გამოასწორე ხარვეზები!<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{elseif $notifications[n].action == 'mutual_accept' || $notifications[n].action == 'task_cancel'}
<li class="pm-n-row"><a href="{$baseurl}/{$notifications[n].link}"><img src="{$baseurl}/images/ic_highlight_remove.svg" alt="" class="pm-n-type">
<span class="pm-n-msg">
	<img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" alt="" class="pm-n-avatar"> {$username}: შეკვეთა გაუქმდა<span class="pm-n-time">{$past_period}</span>
</span>
</a>
</li>
{/if}