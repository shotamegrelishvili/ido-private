<form action="{$baseurl}/find_freelancer" id="new_task" method="post">
	<h2 class="subheading f16 tcenter mt20">აღწერე შესასრულებელი სამუშაო</h2>

	<div class="fr-new-task-slide-1 fr-nt-slide-visible">
		<strong class="subheading mt20">მოკლე სათაური</strong><span class="right">max: <span class="fr-nt-title-length">60</span> სიმბოლო</span>
		<div class="geo-box">
			<input type="text" class="geo fr-nt-txt mt5" maxlength="60" name="sugtitle" placeholder="მაგ: {$tarr=array('გასაკეთებელია ვებ-გვერდის დიზაინი','შესაქმნელია კომპანიის ლოგო','დასამუშავებელია ფოტოსურათი','დასაწერია სტატია')}{$tarr[0|rand:3]} (მაქს. 60 სიმბოლო)" value="{$smarty.request.query}">
		</div>
		<h3 class="subheading mt10">დავალების აღწერა</h3>
		<div class="geo-box">
			<textarea class="geo fr-nt-textarea subheading mt5" cols="20" id="suggestion_content" maxlength="250" name="sugcontent" rows="6" placeholder="ჩაწერე სამუშაოს აღწერა აქ. ეცადე მოკლედ და გასაგებად აღწერო დავალება. (მაქს. 250 სიმბოლო)"></textarea>
		</div>
		<div class="tcenter">
			<a href="javascript:" class="button fr-nt-next large primary-alt subheading" data-slide="1">შემდეგი</a>	
		</div>
	</div>

	<div class="fr-new-task-slide-2 fr-nt-slide-hidden">
		<div class="row">
			<div class="large-6 small-6 columns">
				<h3 class="subheading mt20">სავარაუდო ბიუჯეტი</h3>
				<input type="number" name="budget_range" class="subheading fr-nt-txt" placeholder="მაგ: ¢100" pattern="[0-9]+" min="5" max="995" maxlength="3">
			</div>
			<div class="large-6 small-6 columns">
				<h3 class="subheading mt20">პროექტის ხანგრძლივობა</h3>
				<select name="deadline" class="subheading fr-nt-sel">
					<option value="0">აირჩიე</option>
					<option value="1">1 დღემდე</option>
					<option value="2">1 კვირაზე ნაკლები</option>
					<option value="3">1-2 კვირა</option>
					<option value="5">4 კვირამდე</option>
					<option value="7">რამდენიმე თვე</option>
					<option value="9">ჯერ არ ვიცი</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="large-6 small-6 columns">
				<h3 class="subheading mt20">განცხადების ხანგრძლივობა</h3>
				<select name="period" class="subheading fr-nt-sel">
					<option value="1">1 დღე</option>
					<option value="2">2 დღე</option>
					<option value="3">3 დღე</option>
					<option value="7">7 დღე</option>
					<option value="15" selected>15 დღე</option>
					<option value="30">30 დღე</option>
				</select>
			</div>
			<div class="large-6 small-6 columns">
				<h3 class="subheading mt20">სამუშაოს დაწყების თარიღი</h3>
				<input type="text" name="start_date" class="fr-nt-txt date-pick" value="შეკვეთის დღეს">
			</div>
			<div class="tcenter">
				<a href="javascript:" class="button large success subheading">განთავსება</a>	
			</div>
		</div>
	</div>
</form>