<link href="{$baseurl}/css/slickslider.css" media="screen" rel="stylesheet">
<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/find-freelancer" class=current>{$lang741}</a>
	</ul>
</div>
<!--Search freelancers Top-wrapper-->
<div class="fr-wrapper{if $searching} fr-searching{/if}">
	<form name="findfreelancer1" action="{$baseurl}/find-freelancer/" method="get" class="row">
		<h1 class="heading f25">{if $searching}{$lang846}{else}{$lang845}{/if}</h1>
		<h2 class="subheading">ნაპოვნია {if $searching}{sizeof($posts)} მომსახურება ფიქსირებული ფასით{if $totalFreelancers} და {$totalFreelancers} ფრილანსერი, რომელიც მზადაა შეასრულოს სამუშაო{/if}:{else}{insert name=get_total_members assign=totalmembers}{$freelancers=$totalmembers*0.9}{$freelancers|floor} ქართველი ფრილანსერი მზადაა გემსახუროს. ჩაწერე რა სამუშაოა შესასრულებელი:{/if}</h2>
		<div class="geo-box fr-text-field-wrapper">
			<input type="text" name="query" data-target="experience" data-form="findfreelancer1" data-minlength="2"autocomplete="off" placeholder="მაგ.: {$farr=array('ვებ-დიზაინი','ლოგოს დამზადება','ფოტოსურათის დამუშავება')}{$farr[0|rand:2]}" class="fr-text-field subheading auto-complete geo" value="{$smarty.request.query}">
			<input type="hidden" name="mid" class="fr-mid" value="{$smarty.request.mid}">
			<input type="hidden" name="aid" class="fr-aid" value="{$smarty.request.aid}">
			<input type="hidden" name="c" value="{$smarty.request.c}"><input type="hidden" name="s" value="{$smarty.request.s}"><input type="hidden" name="wp" value="{$smarty.request.wp}"><input type="hidden" name="of" value="{$smarty.request.of}">
			<div class="ac-container"></div>
			<div class="ac-tmpl hide">
				<a class="ac-row" href="javascript:" data-mid="%kID%" data-aid="%aID%" data-k="%k%" data-a="%a%">
					%k%%a%
				</a>
			</div>
			<a href="javascript:document.forms['findfreelancer1'].submit()" class="fr-search-icon"><svg width="32px" height="32px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path d="m12.5 11h-.8l-.3-.3c1-1.1 1.6-2.6 1.6-4.2 0-3.6-2.9-6.5-6.5-6.5-3.6 0-6.5 2.9-6.5 6.5 0 3.6 2.9 6.5 6.5 6.5 1.6 0 3.1-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5m-6 0c-2.5 0-4.5-2-4.5-4.5 0-2.5 2-4.5 4.5-4.5 2.5 0 4.5 2 4.5 4.5 0 2.5-2 4.5-4.5 4.5"></path></svg></a>
			{if $searching}{if $smarty.request.c}<a href="{$baseurl}/find-freelancer/{$tag}?mid={$smarty.request.mid}&amp;aid={$smarty.request.aid}&amp;s={$smarty.request.s}&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}" class="fr-remove-filter subheading button tiny secondary"><i class="icon-toggle icon-check"></i> {insert name=get_cat assign=catname value=var CATID=$smarty.request.c}</a>{/if}<a href="{$baseurl}/find-freelancer/{$tag}?mid={$smarty.request.mid}&amp;aid={$smarty.request.aid}&amp;c={$smarty.request.c}&amp;s={$smarty.request.s}&amp;wp={$smarty.request.wp}&amp;of={if $smarty.request.of}0{else}1#fr-list{/if}" class="fr-remove-filter subheading button tiny secondary"><i class="icon-toggle icon-check{if !$smarty.request.of}-empty{/if}"></i> მხოლოდ ფრილანსერები</a><a href="{$baseurl}/find-freelancer/{$tag}?mid={$smarty.request.mid}&amp;aid={$smarty.request.aid}&amp;c={$smarty.request.c}&amp;s={$smarty.request.s}&amp;wp={if $smarty.request.wp}0{else}1{/if}&amp;of={$smarty.request.of}#fr-list" class="fr-remove-filter subheading button tiny secondary"><i class="icon-toggle icon-check{if !$smarty.request.wp}-empty{/if}"></i> აუცილებელია პორტფოლიო</a>{/if}
		</div>
	</form>
</div>
<!--Search freelancers Top-wrapper End-->
<!--Hide on new Task-->
<div class="fr-hide-on-new-task">
{if $searching AND sizeof($posts)}
<div class="row filters mt20">
	<div class="large-2 small-4 columns">
		<a href="javascript:" class="button small radius secondary heading cats-btn"><i class="icon-sitemap cat-btn-toggle"></i><i class="icon-collapse-alt cat-btn-toggle hide"></i>&nbsp;{$lang527}</a>
	</div>
	<ul class="large-10 small-8 subheading columns">
		<li><strong class="heading">{$lang109}</strong></li>
		{if $s eq "d" OR $s eq "dz" OR $s eq ""}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=d&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}{if $s eq "d" OR $s eq ""}z{/if}" class="secondary radius label">{$lang110}</a></li>
		{else}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=d&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}">{$lang110}</a></li>
		{/if}
		{if $s eq "p" OR $s eq "pz"}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=p&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}{if $s eq "p"}z{/if}" class="secondary radius label">{$lang111}</a></li>
		{else}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=p&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}">{$lang111}</a></li>
		{/if}
		{if $s eq "r" OR $s eq "rz"}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=r&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}{if $s eq "r"}z{/if}" class="secondary radius label">{$lang112}</a></li>
		{else}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=r&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}">{$lang112}</a></li>
		{/if}
		{if $s eq "c" OR $s eq "cz"}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=c&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}{if $s eq "c"}z{/if}" class="secondary radius label">{$lang436}</a></li>
		{else}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=c&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}">{$lang436}</a></li>
		{/if}
		{if $s eq "e" OR $s eq "ez"}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=e&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}{if $s eq "e"}z{/if}" class="secondary radius label">{$lang494}</a></li>
		{else}
		<li><a href="{$baseurl}/find-freelancer/{$tag}?c={$c}&amp;s=e&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}">{$lang494}</a></li>
		{/if}
	</ul>
</div>

<div class="cats-2c row hide mb10">
<ul class="large-block-grid-2 small-block-grid-2">
  {insert name=get_categories assign=fcats}
  {section name=i loop=$fcats}
	<li class="subheading{if !($smarty.section.i.index % 2)} tright{/if}"><a href="{$baseurl}/find-freelancer/{$tag}?c={$fcats[i].CATID}&amp;s={$smarty.request.s}&amp;wp={$smarty.request.wp}&amp;of={$smarty.request.of}"{if $c eq $fcats[i].CATID} class="active"{/if}>{$fcats[i].name|stripslashes}</a></li>
  {/section}
</ul>
</div>

<div class="work-area-wrapper">
{include file="bit.tpl"}
</div>
{/if}

{if $totalFreelancers}
<div class="row mt30 collapse">
	<div class="large-4 large-offset-8 small-12 columns tright" id="fr-list">
		<a href="javascript:" class="fr-new-task-btn button large success subheading mb0 expand">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="white">
		<path d="M19 13h-6v6h-2v-6h-6v-2h6v-6h2v6h6v2z"/>
		<path d="M0 0h24v24h-24z" fill="none"/>
		</svg> განათავსე განცხადება</a>
	</div>
</div>
{include file="freelancers_bit.tpl"}
{if $totalFreelancers GT 5}
<div class="row mt30 collapse mb30">
	<div class="large-4 large-offset-8 small-12 columns tright">
		<a href="javascript:" class="fr-new-task-btn button large success subheading mb0 expand">
		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="white">
		<path d="M19 13h-6v6h-2v-6h-6v-2h6v-6h2v6h6v2z"/>
		<path d="M0 0h24v24h-24z" fill="none"/>
		</svg> განათავსე განცხადება</a>
	</div>
</div>
{/if}
{/if}

{if $searching AND !sizeof($posts) AND !$totalFreelancers}
<div class="row mt20">
    <h2 class="subheading h-large">განათავსე განცხადება</h2>
    <span class="subheading">ძებნა უშედეგოა, მარგამ არაუშავს - განათავსე განცხადება <strong class="red">უფასოდ</strong> და დაელოდე ფრილანსერების შემოთავაზებებს!</span>
</div>
{$hide_jobs=1}{assign var=hideMA value=1 scope=global}
<div class="suggest-work-wrapper mt20">
    {include file="suggest_work.tpl"}
</div>
{/if}

{if !$searching}
<!--Why iDo-->
<div class="row mt20 fr-why">
	<h3 class="heading h-large mb0">რატომ iDo.ge?</h3>
	<div class="row mt20">
		<div class="large-1 small-2 columns">
			<span class="fr-why-hl">
			{$freelancersRound=ceil($freelancers/100)*100}{$freelancersRound}
			</span>
		</div>	
		<div class="large-4 small-10 columns fr-why-txt">
			ქართველი ფრილანსერი შენს სამსახურში
		</div>
		<div class="large-1 large-offset-1 small-2 columns">
			<span class="fr-why-hl">
			{insert name=get_total_complete_orders assign=total_complete_orders}
			{$buyers=ceil($total_complete_orders/10)*10}{$buyers}
			</span>
		</div>	
		<div class="large-5 small-10 columns fr-why-txt">
		შესრულებული შეკვეთა და კმაყოფილი კლიენტი
		</div>
	</div>
	<div class="row mt20">
		<div class="large-1 small-2 columns">
			<span class="fr-why-hl">
			{insert name=get_total_posts assign=cc}{$tasks=ceil($cc/50)*50}{$tasks}
			</span>
		</div>	
		<div class="large-4 small-10 columns fr-why-txt fr-has-tip">
			<span class="fr-hide-on-tip">მომსახურება ფიქსირებული ფასით</span>
			<span class="fr-tip">ფრილანსერების მიერ წინასწარ მომზადებული და განცხადების სახით განთავსებული მომსახურება, რომლის შეკვეთა ფიქსირებულ ფასად შეგიძლია.</span>
		</div>
		<div class="large-1 large-offset-1 small-2 columns">
			<span class="fr-why-hl">
				5<span class="subheading">{$lang63}</span>
			</span>
		</div>	
		<div class="large-5 small-10 columns fr-why-txt fr-has-tip">
			<span class="fr-hide-on-tip">შესრულებული სამუშაოს მინიმალური ანაზღაურება</span>
			<span class="fr-tip">შესრულებული სამუშაოს მინიმალური ანაზღაურება 5 ლარია. სამუშაოს სირთულის და მოცულობის მიხედვით, ანაზღაურებაზე შეგიძლია ფრილანსერს თავად შეუთანხმდე.</span>
		</div>
	</div>
	<div class="row mt20">
		<div class="large-1 small-2 columns">
			<span class="fr-why-hl">
				100%
			</span>
		</div>	
		<div class="large-4 small-10 columns fr-why-txt fr-has-tip">
			<span class="fr-hide-on-tip">თანხის დაბრუნების გარანტია</span>
			<span class="fr-tip">გადახდილი თანხა 100% დაგიბრუნდება თუ: შესრულებული სამუშაო დათქმულ ვადაში არ ჩაგბარდა, ან არ მოგწონს შესრულების ხარისხი.</span>
		</div>
		<div class="large-1 large-offset-1 small-2 columns">
			<span class="fr-why-hl">
				0%
			</span>
		</div>	
		<div class="large-5 small-10 columns fr-why-txt fr-has-tip">
			<span class="fr-hide-on-tip">პლატფორმის საკომისიო</span>
			<span class="fr-tip">ფრილანსინგის პოპულარიზაციის მიზნით, პლატფორმით სარგებლობა არის სრულიად უფასო.<br>მეტი ინფორმაციისთვის <a href="{$baseurl}/faq?p=general&amp;t=commission" target="_blank">მიყევი ბმულს</a>.</span></span>
		</div>
	</div>
</div>
<!--Why iDo End-->

<!--How it Works-->
<div class="g-wrapper-gray mt30">
	<div class="row tcenter mt20 mb30 fr-hiw">
		<h3 class="heading h-large mb0">როგორ მუშაობს?</h3>
		<div class="row subheading">
			iDo.ge არის მარტივი საშუალება, დისტანციურად შეუკვეთო და ჩაიბარო სამუშაო სწრაფად, ხარისხიანად და დაცულად 
		</div>
		<div class="row mt20">
			<div class="large-4 small-12 columns">
				<span class="fr-hiw-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 20 20">
					<path d="M15.5 14h-.79l-.28-.27c.98-1.14 1.57-2.62 1.57-4.23 0-3.59-2.91-6.5-6.5-6.5s-6.5 2.91-6.5 6.5 2.91 6.5 6.5 6.5c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99 1.49-1.49-4.99-5zm-6 0c-2.49 0-4.5-2.01-4.5-4.5s2.01-4.5 4.5-4.5 4.5 2.01 4.5 4.5-2.01 4.5-4.5 4.5z"/>
					<path d="M0 0h24v24h-24z" fill="none"/>
					</svg>
				</span>
				<span class="fr-hiw-title">
					1. იპოვე მარტივად
				</span>
				<div class="fr-hiw-txt">
					იპოვე სამუშაოს შემსრულებელი, ან განათავსე საკუთარი განცხადება <strong class="red">უფასოდ</strong> და მიიღე შემოთავაზებები ფრილანსერებისაგან.
				</div>
			</div>
			<div class="large-4 small-12 columns">
				<span class="fr-hiw-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 24 24">
					<path fill-opacity=".9" d="M11.99 2c-5.52 0-9.99 4.48-9.99 10s4.47 10 9.99 10c5.53 0 10.01-4.48 10.01-10s-4.48-10-10.01-10zm.01 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path fill-opacity=".9" d="M12.5 7h-1.5v6l5.25 3.15.75-1.23-4.5-2.67z"/>
					</svg>
				</span>
				<span class="fr-hiw-title">
					2. დაუკვეთე სამუშაო
				</span>
				<div class="fr-hiw-txt">
					გადასახდელი თანხა დროებით დაგებლოკება და ჩამოგეჭრება მხოლოდ შესრულებული სამუშაოს ჩაბარების და შენი დასტურის შემდეგ.
				</div>
			</div>
			<div class="large-4 small-12 columns">
				<span class="fr-hiw-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 24 24">
					<path d="M0 0h24v24h-24z" fill="none"/>
					<path d="M17.21 9l-4.38-6.56c-.19-.28-.51-.42-.83-.42-.32 0-.64.14-.83.43l-4.38 6.55h-4.79c-.55 0-1 .45-1 1 0 .09.01.18.04.27l2.54 9.27c.23.84 1 1.46 1.92 1.46h13c.92 0 1.69-.62 1.93-1.46l2.54-9.27.03-.27c0-.55-.45-1-1-1h-4.79zm-8.21 0l3-4.4 3 4.4h-6zm3 8c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"/>
					</svg>
				</span>
				<span class="fr-hiw-title">
					3. ჩაიბარე და შეაფასე სამუშაო
				</span>
				<div class="fr-hiw-txt">
					წინასწარ შეთანხმებულ ვადაში მიიღებ შესრულებულ სამუშაოს. შეამოწმე, ჩაიბარე და შეაფასე შესრულებული სამუშაო.
				</div>
			</div>
		</div>
	</div>
</div>
<!--How it Works End-->

<!--Reviews-->
<div class="row mt30 up-rev-wrapper fr-rev">
	<h3 class="heading h-large mb0">დამკვეთის აზრი</h3>
	<div class="slick-slider single-item">
		<div class="div">
			<div class="large-2 hide-for-small columns fr-rev-author mt20">
			<img src="{$baseurl}/images/revav2.png" alt=""><br>
			ნატალია, თბილისი
			</div>
			<div class="large-10 small-12 columns">
				<blockquote class="up-rev-text">როცა ჩემმა ოჯახმა გადაწყვიტა სახლში ერთი დიდი ოთახი გაგვერემონტებინა, წავაწყდით ერთ პრობლემას. ვერაფრით ვერ შევთანხმდით ოთახის დიზაინზე და მის ინტერიერზე. არ ვიცოდით მცირე ბიუჯეტით როგორ მოგვეწყო ოთახი, ან ვისთვის მიგვემართა. ჩემმა მეგობარმა მითხრა, რომ www.iDo.ge-ზე არიან ინტერიერის ფრილანს-დიზაინერები. რომ ვნახე მათი ნამუშევრები და ფასები, მოვიხიბლე.<br>
				დავუკავშირდი ერთ-ერთ ფრილანსერს, მივწერე როგორი ოთახი გვინდოდა... შედეგმა გადააჭარბა მოლოდინს!<br>
				ბრწყინვალე და დროული ინიციატივაა ამ პლატფორმის შექმნა საქართველოში. აუცილებლად ვისარგებლებ ფრილანსერების მომსახურებით მომავალშიც.
				</blockquote>		
			</div>
		</div>
		<div class="div">
			<div class="large-2 hide-for-small columns fr-rev-author mt10">
			<img src="{$baseurl}/images/revav1.png" alt=""><br>
			ზოია, თბილისი
			</div>
			<div class="large-10 small-12 columns">
				<blockquote class="up-rev-text">დოკუმენტის თარგმნა სხვადასხვა ენიდან ქართულად ყოველთვის პრობლემას  წარმოადგენდა ჩემთვის. პროფესიონალი თარჯიმნის მონახვა არც ისე იოლი საქმეა. არაერთი სათარჯიმნო ბიურო თუ კერძო თარჯიმანი გამოვცადე. ამისთვის, მათთან შესახვედრად მიწევდა დროის და ენერგიის დახარჯვა.<br>
				iDo.ge საიტზე განთავსებული მომსახურების ცდა გადავწყვიტე. ვიცოდი, რომ თუ არ მომეწონა ნათარგმნი, შემეძლო თანხა სრულად დამებრუნებინა. სახლიდან გაუსვლელად გადავეცი სათარგმნი და დათქმულ ვადაშივე მივიღე ხარისხიანი შედეგი!
				</blockquote>		
			</div>
		</div>
	</div>
</div>
<!--Reviews End-->

<!-- Search freelancers #2-->
<div class="fr-search-bottom-wrapper g-wrapper-gray mt20">
	<form name="findfreelancer_bottom" action="" method="get" class="row mt20 mb30">
		<h3 class="subheading f16">მოსინჯე, ჩაწერე რა სამუშაო გაქვს შესასრულეველი:</h3>
		<div class="geo-box fr-text-field-wrapper">
			<input type="text" name="query" data-target="experience" data-form="findfreelancer1" data-minlength="2"autocomplete="off" placeholder="მაგ.: {$farr=array('ვებ-დიზაინი','ლოგოს დამზადება','ფოტოსურათის დამუშავება')}{$farr[0|rand:2]}" class="fr-text-field subheading auto-complete geo" value="{$smarty.request.query}">
			<input type="hidden" name="mid" class="fr-mid" value="{$smarty.request.mid}">
			<input type="hidden" name="aid" class="fr-aid" value="{$smarty.request.aid}">
			<input type="hidden" name="c" value="{$smarty.request.c}"><input type="hidden" name="s" value="{$smarty.request.s}"><input type="hidden" name="wp" value="{$smarty.request.wp}"><input type="hidden" name="of" value="{$smarty.request.of}">
			<div class="ac-container"></div>
			<div class="ac-tmpl hide">
				<a class="ac-row" href="javascript:" data-mid="%kID%" data-aid="%aID%" data-k="%k%" data-a="%a%">
					%k%%a%
				</a>
			</div>
			<a href="javascript:document.forms['findfreelancer_bottom'].submit()" class="fr-search-icon"><svg width="32px" height="32px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><path d="m12.5 11h-.8l-.3-.3c1-1.1 1.6-2.6 1.6-4.2 0-3.6-2.9-6.5-6.5-6.5-3.6 0-6.5 2.9-6.5 6.5 0 3.6 2.9 6.5 6.5 6.5 1.6 0 3.1-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5m-6 0c-2.5 0-4.5-2-4.5-4.5 0-2.5 2-4.5 4.5-4.5 2.5 0 4.5 2 4.5 4.5 0 2.5-2 4.5-4.5 4.5"></path></svg></a>
		</div>
	</form>
</div>
<!-- Search freelancers #2 End-->
{/if}
</div>
<!--Hide on new Task end-->

<!--New Task-->
<div class="fr-new-task mt30">
	<div class="row fr-list-nf-wrapper hide">
		<div class="large-12 small-12 columns">
			<h2 class="subheading">ფრილანსერები გამოგიგზავნიან თავიანთ შემოთავაზებებს:</h2>
			<ul class="fr-list-notified-freelancers"></ul>
		</div>
	</div>
	<div class="suggest-work-wrapper mt10">
		<div class="row">
			{include file="new_task.tpl"}
		</div>
	</div>
</div>
<!--New Task end-->
{*
<div class="row mt20">
	<div class="large-12 small-12 columns">
		<h1 class="heading h-large mb0">{if !$smarty.get.jobseeker}{$lang741}{else}{$lang742}{/if}</h1>
		{if !$smarty.get.jobseeker}<h2 class="subheading">{$lang742}</h2>{/if}
	</div>
</div>
{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="row"><div class="large-12 small-12 columns">{include file="error.tpl"}</div></div>{/if}

{if !$smarty.get.jobseeker AND !isset($smarty.get.WID)}
<div class="suggest-work-wrapper mt10">
	<div class="row">
		{assign var=hide_jobs value=1}
		{insert name=get_categories assign=cats}
		{include file="suggest_work.tpl"}
	</div>
</div>
{/if}

<div class="row mt10">
	{insert name=get_categoriesbyid assign=catsbyid}
	{include file="bit_suggest.tpl"}
</div>
{if $ending}
<div class="row mt20">
	<div class="large-12 tcenter columns">
		<a href="javascript:" class="button large secondary subheading radius" id="load-more" data-order="{$smarty.get.s}" data-start="{$ending}" data-type="suggested">{$lang532}</a>
	</div>
</div>
{/if}
*}
<script>
	var lang = [];
	lang['sure'] = '{$lang575}';
	lang['maxlength'] = '{$lang635}';
	var def_js = ['{$baseurl}/js/suggested.js?v=4'];
</script>