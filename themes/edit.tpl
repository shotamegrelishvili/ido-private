<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/new" class=current>{$lang55}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang576}</h1>
    <h2 class="subheading">{$lang577}</h2>
  </div>
</div>

{if $error ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">
    <h2 class=subheading>{$lang11}</h2>
    <ul class=subheading>
    {$error}
    </ul>
  </div>
</div>
{/if}

{if $message ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">{include file="error.tpl"}</div>
</div>
{/if}
<form action="{$baseurl}/edit?id={$g.PID}" class="edit-deal" enctype="multipart/form-data" id="edit-deal" method="post">
<div class="work-area row">
  <fieldset>
  <div class="row">
    <div class="large-4 small-4 columns">
      <label for="title" class="heading s08"><i class="icon-pencil"></i> სათაური *<a href="javascript:" class="right newdeal-help" data-goal="title"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box">
      {if $deny_change_fields['title']}
      <input type="hidden" name="title" value="{$g.gtitle|stripslashes}">
      <input class="text geo disabled" id="title-X" maxlength="200" name="title-X" size="200" type="text" value="{$g.gtitle|stripslashes}" placeholder="მაგ.: დავამუშავებ ერთ ფოტოსურათს" required data-minsize="10" disabled>
      {else}
      <input class="text geo" id="title" maxlength="200" name="title" size="200" type="text" value="{if $smarty.post.title}{$smarty.post.title}{else}{$g.gtitle|stripslashes}{/if}" placeholder="მაგ.: დავამუშავებ ერთ ფოტოსურათს" required data-minsize="10">
      {/if}
      </div>
    </div>
    <div class="large-2 small-2 columns">
      <label for="price" class="heading s08"><span class="icon-gel">¢</span> ანაზღაურება *<a href="javascript:" class="right newdeal-help" data-goal="price"><i class="icon-question-sign red icon-large"></i></a></label>
      {insert name=get_member_vas_ownprice value=a assign=allow_ownprice}
      {if $allow_ownprice}
      <input class="text{if $deny_change_fields['price']} disabled{/if}" id="price" maxlength="3" name="price" type="text" value="{$g.price}" placeholder="მაგ.: 100 ლარი" required data-minsize="1" pattern="[0-9]+" autocomplete="off" data-anyprice="1"{if $deny_change_fields['price']} disabled{/if}>
      {else}
      {if $deny_change_fields['price']}
      <input type="hidden" name="price" value="{$g.price}">
      <select class="subheading disabled" id="price-X" required name="price-X" disabled>
      {else}
      <select class="subheading" id="price" required name="price">
      {/if}
        {insert name=get_packs value=a assign=packs}
        {insert name=get_member_level value=a assign=member_level}
        <option value="">-- აირჩიე ფასი --</option>
      {section name=p loop=$packs}
        {if $packs[p].pprice eq "0" AND $smarty.session.USERID}
        <option value="{$packs[p].ID|stripslashes}"{if $g.price eq $packs[p].pprice|stripslashes} selected{/if}>{$lang743}</option>
        {elseif $packs[p].pprice ne "0"}
        <option value="{$packs[p].ID|stripslashes}"{if $smarty.post.price AND $smarty.post.price eq $packs[p].pprice} selected{elseif !$smarty.post.price AND $g.price eq $packs[p].pprice|stripslashes} selected{/if}>{$packs[p].pprice|stripslashes} {$lang523}</option>{/if}
      {/section}
      </select>
      {/if}
    </div>
    <div class="large-6 small-6 columns">
      <h4 class="heading green"><i class="icon-pencil"></i> სათაური და ანაზღაურება</h4>
      <span class="subheading">შეეცადე განცხადების სათაური იყოს მიმზიდველი და მოკლედ აღწერდეს შეთავაზების შინაარს. ოპტიმალურია 40-80 სიმბოლოსაგან შემდგარი სათაური.</span>
    </div>
  </div>

  <div class="row">
    <div class="large-6 small-6 columns">
      <label for="desc" class="heading s08"><i class="icon-edit"></i> განცხადების შინაარსი *<a href="javascript:" class="right newdeal-help" data-goal="desc"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box"><textarea class="text ta6 mb0 geo" cols="74" id="desc" maxlength="450" name="desc" rows="10" placeholder="მაგ.: დავამუშავებ ერთ ფოტოსურათს თქვენთვის სასურველ სტილში." required data-minsize="40">{if $smarty.post.desc}{$smarty.post.desc}{else}{$g.gdesc|stripslashes}{/if}</textarea></div>
      <p class="max-chars subheading s08">{$lang64}: <span class="descmax">450</span> ({$lang65}: <span class="descused">0</span>)</p>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator mt10"></div>
      <h4 class="heading green mt10"><i class="icon-edit"></i> განცხადების შინაარსი</h4>
      <span class="subheading">{$lang70} {$lang71}</span>
    </div>  
  </div>

  <div class="row">
    <div class="large-6 small-6 columns">
      <label for="instr" class="heading s08"><i class="icon-file-text"></i> {$lang73} *<a href="javascript:" class="right newdeal-help" data-goal="instr"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box"><textarea class="text ta6 geo" cols="74" id="instr" maxlength="450" name="instr" rows="10" placeholder="მაგ.: გთხოვთ გამომიგზავნოთ დასამუშავებელი ფოტოსურათი." required data-minsize="10">{if $smarty.post.instr}{$smarty.post.instr}{else}{$g.ginst|stripslashes}{/if}</textarea></div>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator"></div>
      <h4 class="heading green mt10"><i class="icon-file-text"></i> {$lang73}</h4>
      <span class="subheading">{$lang75}</span>
    </div>  
  </div>

  <div class="row">
    <div class="large-6 small-6 columns">
      <label for="cat" class="heading s08"><i class="icon-sitemap"></i> კატეგორია *<a href="javascript:" class="right newdeal-help" data-goal="cat"><i class="icon-question-sign red icon-large"></i></a></label>
      {if $deny_change_fields['cat']}
      <input type="hidden" name="cat" value="{$g.category}">
      <select class="categoryselection disabled" id="cat-X" name="cat-X" required disabled>
      {else}
      <select class="categoryselection" id="cat" name="cat" required>
      {/if}
      <option value="">{$lang67}</option>
        {insert name=get_categories assign=c}
        {section name=i loop=$c}
          <option value="{$c[i].CATID|stripslashes}" {if ($smarty.post.cat AND $smarty.post.cat eq $c[i].CATID) OR (!$smarty.post.cat AND $g.category eq $c[i].CATID)}selected="selected"{/if}>{$c[i].name|stripslashes}</option>
          {if $c[i].CATID ne "0"}
              {insert name=get_subcategories assign=scats parent=$c[i].CATID}
              {section name=j loop=$scats}
              <option value="{$scats[j].CATID}" {if $g.category eq $scats[j].CATID}selected="selected"{/if}>- {$scats[j].name|stripslashes}</option>
              {/section}
          {/if}
        {/section}
        </select>
        <label for="tags" class="heading s08"><i class="icon-tags"></i> {$lang76} *<a href="javascript:" class="right newdeal-help" data-goal="tags"><i class="icon-question-sign red icon-large"></i></a></label>
        <div class="geo-box"><textarea class="text geo" cols="74" id="tags" maxlength="100" name="tags" rows="2" placeholder="მაგ.: ფოტო, ფოტოგრაფია, ფოტოსურათის დამუშავება" required data-minsize="4">{if $smarty.post.tags}{$smarty.post.tags}{else}{$g.gtags|stripslashes}{/if}</textarea></div>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator mt10"></div>
      <h4 class="heading green mt10"><i class="icon-sitemap"></i> კატეგორია და ტეგები</h4>
      <span class="subheading">{$lang68}<br>{$lang77}<br>{$lang78}</span>
    </div>
  </div>

  <div class="row">
    <div class="large-6 small-6 columns">
      <label for="duration" class="heading s08"><i class="icon-time"></i> რა დროში შეასრულებ სამუშაოს *<a href="javascript:" class="right newdeal-help" data-goal="duration"><i class="icon-question-sign red icon-large"></i></a></label>
      <input class="text mb0" id="duration" maxlength="2" name="duration" size="2" type="text" value="{if $smarty.post.duration}{$smarty.post.duration}{else}{$g.days|stripslashes}{/if}" pattern="[0-9]{literal}{1,2}{/literal}" placeholder="მაგ.: 2 (ორ დღეში)" required autocomplete=off data-minsize="1">
      <p class="subheading s08">მიუთითე რამდენი დღე დაგჭირდება სამუშაოს შესრულებისთვის. მაგ.: 2</p>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator mt5"></div>
      <h4 class="heading green mt5"><i class="icon-time"></i> სამუშაოს შესრულების დრო</h4>
      <span class="subheading">მიუთითე შეკვეთიდან მაქსიმუმ რამდენ დღეში შეასრულებ სამუშაოს.
თუ შეკვეთიდან არაუგვიანეს 24 საათის განმავლობაში სრულად შეასრულებ სამუშაოს, მიუთითე "0".</span>
    </div>
  </div>

  <div class="row">
    <div class="large-6 small-6 columns photos-block">
      <span id="photo-upload-container" class="dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-iX" class="thumb-121x55"><i class="icon-picture icon-border icon-3x icon-muted vmiddle show-on-delete"></i></span> <span class="browsefile button medium success subheading radius mt10 inline-block" id="upload-iX" data-id="iX"><i class="icon-upload-alt"></i> ფოტოსურათის ატვირთვა</span><a class="deletefile button alert medium radius subheading mt10 ml10 hide" id="delete-iX" data-id="iX"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-iX" name="photo-iX" data-id="iX" type="file"><input id="photobase64-iX" name="photobase64-iX" data-id="iX" type="hidden"><input id="photo-remove-iX" name="photo-remove-iX" data-id=iX value=0 type=hidden></span>
      <label for="photo-i1" class="heading s08"><i class="icon-picture"></i> ფოტოსურათი *</label>
      <span id="upload-wrapper">
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i1" class="thumb-121x55">{if $g.p1 ne ""}<img src="{$purl}/t2/{$g.p1}" alt="">{/if}<i class="icon-picture icon-border icon-3x icon-muted vmiddle {if $g.p1 ne ""} hide{/if}"></i></span> <span class="browsefile button medium {if $g.p1 eq ""}success{else}secondary{/if} subheading radius mt10 inline-block" id="upload-i1" data-id="i1"><i class="icon-upload-alt"></i> ფოტოსურათის {if $g.p1 eq ""}ატვირთვა{else}შეცვლა{/if}</span><a class="deletefile button alert medium radius subheading mt10 ml10 {if $g.p1 eq ""}hide{/if}" id="delete-i1" data-id="i1"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i1" name="photo-i1" data-id="i1" type="file"><input id="photobase64-i1" name="photobase64-i1" data-id="i1" type="hidden"><input id="photo-remove-i1" name="photo-remove-i1" data-id=i1 value=0 type=hidden>{if $g.p1 ne ""}<input type=hidden id="photo-indb-i1" name="photo-indb-i1" value=1>{/if}</span>
      {if $g.p1 ne ""}
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i2" class="thumb-121x55">{if $g.p2 ne ""}<img src="{$purl}/t2/{$g.p2}" alt="">{/if}<i class="icon-picture icon-border icon-3x icon-muted vmiddle {if $g.p2 ne ""} hide{/if}"></i></span> <span class="browsefile button medium {if $g.p2 eq ""}success{else}secondary{/if} subheading radius mt10 inline-block" id="upload-i2" data-id="i2"><i class="icon-upload-alt"></i> ფოტოსურათის {if $g.p2 eq ""}ატვირთვა{else}შეცვლა{/if}</span><a class="deletefile button alert medium radius subheading mt10 ml10 {if $g.p2 eq ""}hide{/if}" id="delete-i2" data-id="i2"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i2" name="photo-i2" data-id="i2" type="file"><input id="photobase64-i2" name="photobase64-i2" data-id="i2" type="hidden"><input id="photo-remove-i2" name="photo-remove-i2" data-id=i2 value=0 type=hidden></span>
      {/if}
      {if $g.p2 ne "" OR $g.p3 ne ""}
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i3" class="thumb-121x55">{if $g.p3 ne ""}<img src="{$purl}/t2/{$g.p3}" alt="">{/if}<i class="icon-picture icon-border icon-3x icon-muted vmiddle {if $g.p3 ne ""} hide{/if}"></i></span> <span class="browsefile button medium {if $g.p3 eq ""}success{else}secondary{/if} subheading radius mt10 inline-block" id="upload-i3" data-id="i3"><i class="icon-upload-alt"></i> ფოტოსურათის {if $g.p3 eq ""}ატვირთვა{else}შეცვლა{/if}</span><a class="deletefile button alert medium radius subheading mt10 ml10 {if $g.p3 eq ""}hide{/if}" id="delete-i3" data-id="i3"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i3" name="photo-i3" data-id="i3" type="file"><input id="photobase64-i3" name="photobase64-i3" data-id="i3" type="hidden"><input id="photo-remove-i3" name="photo-remove-i3" data-id=i3 value=0 type=hidden></span>
      {/if}
      </span>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator mt5"></div>
      <h4 class="heading green mt5"><i class="icon-picture"></i> ფოტოსურათი</h4>
      <div class="subheading"><ul class="image-tip"><li>{$lang82}</li><li>{$lang83}</li><li>{$lang84}</li><li>{$lang85}</li><li>{$lang86}</li></ul></div>
    </div>
  </div>

  <div class="row hide-nd-more-params{if $g.video OR !$g.moneyback} hide{/if}">
    <div class="separator hide-for-small"></div>
    <div class="large-6 small-12 columns mt5">
      <a class="heading nd-more-params" href="javascript:"><i class="icon-gear"></i> დამატებითი პარამეტრები</a>
    </div>
    <div class="large-6 small-12 columns mt5">
      <div class="subheading">{$lang759}</div>
    </div>
  </div>

  <div class="row unhide-nd-more-params{if !($g.video OR !$g.moneyback)} hide{/if}">
    <div class="large-6 small-12 columns">
      <label for="video" class="heading s08"><i class="icon-facetime-video"></i> ვიდეო <a href="javascript:" class="right newdeal-help" data-goal="video"><i class="icon-question-sign red icon-large"></i></a></label>
      <input type="text" name="video" id="video" placeholder="https://youtube.com/watch?v=..." value="{if $smarty.post.video}{$smarty.post.video}{else}{$g.video|stripslashes}{/if}">
      <input type="hidden" name="videohost" id="videohost" value="{if $smarty.post.videohost}{$smarty.post.videohost}{else}{$g.videohost|stripslashes}{/if}">
      <input type="hidden" name="videofile" id="videofile" value="{if $smarty.post.videofile}{$smarty.post.videofile}{else}{$g.videofile|stripslashes}{/if}">
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-facetime-video"></i> ვიდეო</h4>
      <div class="subheading"><ul class="video-tip">{$lang758}</ul></div>
    </div>
  </div>

  <div class="row unhide-nd-more-params{if !($g.video OR !$g.moneyback)} hide{/if}">
    <div class="large-6 small-12 columns">
      <label for="moneyback" class="heading s08"><i class="icon-rotate-left"></i> სამუშაოს შედეგის გარანტია *<a href="javascript:" class="right newdeal-help" data-goal="moneyback"><i class="icon-question-sign red icon-large"></i></a></label>
      {if !$member_level OR $member_level LT 2}
      <input name="moneyback" type="hidden" value="1">
      <select name="moneyback_x" id="moneyback" class="subheading disabled" disabled>
      {else}
      <select name="moneyback" id="moneyback" class="subheading" required>
      {/if}
        <option value="1"{if !isset($g.moneyback) OR $g.moneyback ne '0'} selected{/if}>თანხის დაბრუნება გარანტირებულია</option>
        <option value="0"{if $g.moneyback eq '0'} selected{/if}>თანხა არ ექვემდებარება დაბრუნებას</option>
      </select>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-rotate-left"></i> სამუშაოს შედეგის გარანტია</h4>
      <div class="subheading">{$lang760}
      {if !$member_level OR $member_level LT 2}
        აღნიშნული პარამეტრის გასააქტიურებლად <a href="{$baseurl}/contact" target="_blank" rel="nofollow">დაგვიკავშირდი</a>.<br>&nbsp;
      {/if}
      </div>
    </div>
  </div>

{*
  <div class="row">
    <div class="large-6 small-6 columns">
      <label class="heading s08"><i class="icon-gear"></i> განცხადების სხვა პარამეტრები *</label>
      <label for="multipleme" class="fs1em subheading">
        <input id="multipleme" name="multipleme" type="checkbox" value="1" class="switch2 switchYesNo"{if $g.add_multiple} checked{/if}><span class="switch2"></span> <span class="s08">{$lang483}</span>
      </label>
    </div>
    <div class="large-6 small-6 columns">
      <div class="separator"></div>
      <h4 class="heading green mt5"><i class="icon-gear"></i> განცხადების სხვა პარამეტრები</h4>
      <span class="subheading">თუ გსურს რომ შეზღუდო ერთი მომხმარებლის მიერ შეკვეთის რაოდენობა, შეცვალე აღნიშნული პარამეტრი.</span>
    </div>
  </div>
  *}
  <div class="row separator mt20"></div>
  <div class="row tcenter submit-wrapper">
    <div class="form-error-notice subheading red mt10 hide">{$lang566}</div>
    <input type="submit" value="{$lang46}" class="send-button secondary button large subheading radius mt10">
    <div class="progress-indicator-icon-message mt10 hide">
      {include file="loader.tpl"}
    </div>
  </div>
<div class="edit-gig-form-b"></div>
<input type="hidden" name="subform" value="1">
  </fieldset>
</div>
</form>

{* Helpers *}
<div id="title-info-popup">
  <ol id="title-popup" class="joyride-list" data-joyride>
  <li data-id="title" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების სათაური</h4>
    <p class="subheading s08 mb0">განცხადების სათაური უნდა იყოს ლაკონური და გასაგებად უნდა აღწერდეს თუ რა მომსახურების გაწევას გეგმავ.<br>
    სათაურში უნდა ჩანდეს რა მოცულობის სამუშაოს შეასრულებ მოთხოვნილი ანაზღაურების სანაცვლოდ. (მაგ.: გადათარგმნი ერთ გვერდს, დაამუშავებ ორ ფოტოს და ა.შ.)<br>
    განსხვავებული მოცულობის ან მომსახურების შეთავაზებისათვის, განათავსე ცალკეული განცხადებები<br>
    მიიღება შემდეგი სახის განცხადებები:<br>
    <br>"2 ფოტოსურათის მხატვრული დამუშავება"<br>
    "ერთი გვერდის თარგმნა რუსულიდან ინგლისურად"<br>
    <u>არ მიიღება</u> ზოგადი სახის განცხადებები:<br>
    "ვთარგმნი ტექსტებს ქართულიდან რუსულად"
    "ვამუშავებ ფოტოსურათებს".
    </p>
  </li> 
  </ol> 
</div>
<div id="price-info-popup">
  <ol id="price-popup" class="joyride-list" data-joyride>
  <li data-id="price" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>ფიქსირებული ანაზღაურება</h4>
    <p class="subheading s08 mb0">განათავსე განცხადება ფიქსირებული ანაზღაურებით. ამისათვის, აღწერე რა მოცულობის სამუშაოს შესრულებას (მაგ.: გადათარგმნი ერთ გვერდს, დაამუშავებ ორ ფოტოს და ა.შ.), რაშიც ითხოვ ფიქსირებულ ანაზღაურებას.<br>
    განსხვავებული მოცულობის ან მომსახურების შეთავაზებისათვის, განათავსე ცალკეული განცხადებები.<br>
    <strong>საიტის მომსახურების საკომისიო შეადგენს მიღებული ანაზღაურების 10% (მინ.: 1 ლარს)</strong><br>
    მიიღება შემდეგი სახის განცხადებები:<br>
    <br>"2 ფოტოსურათის მხატვრული დამუშავება. 15 ლარად"<br>
    "ერთი გვერდის თარგმნა რუსულიდან ინგლისურად. 10 ლარად"<br>
    <u>არ მიიღება</u> ზოგადი სახის განცხადებები:<br>
    "ვთარგმნი ტექსტებს ქართულიდან რუსულად"<br>
    "ვამუშავებ ფოტოსურათებს"
    </p>
  </li> 
  </ol> 
</div>
<div id="desc-info-popup">
  <ol id="desc-popup" class="joyride-list" data-joyride>
  <li data-id="desc" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების შინაარსი</h4>
    <p class="subheading s08 mb0">სრულად აღწერე გასაწევი მომსახურება და შედეგი, რომელსაც მიიღებს კლიენტი.<br>მაგ.: "ფოტოშოპში დავამუშავებ 2 ფოტოსურათს:<br>- ფერების და კონტრასტის კორექცია;<br>- მცირე დეფექტების და ზედმეტი საგნების მოშორება;<br>...<br>ფოტოსურათის დაბალი ხარისხის შემთხვევაში, შეკვეთამდე წინასწარ დამიკავშირდით.<br>მაქვს ფოტოდამუშავების 5 წლიანი გამოცდილება."
    </p>
  </li> 
  </ol> 
</div>
<div id="instr-info-popup">
  <ol id="instr-popup" class="joyride-list" data-joyride>
  <li data-id="instr" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>შეთავაზების მიღების ინსტრუქცია</h4>
    <p class="subheading s08 mb0">აღწერე თუ რა სახით უნდა მოგაწოდოს დამკვეთმა მომსახურების გასაწევად აუცილებელი საწყისი ინფორმაცია ან მასალა.<br>
    ინფორმაციის მოწოდებისათვის დამკვეთმა უნდა გამოიყენოს არსებული ვებ-გვერდი. ელფოსტით ან სხვა საშუალებით მასალის გამოგზავნის მოთხოვნა დაშვებულია მხოლოდ იმ შემთხვევაში, თუ გამოსაგზავნი მონაცემები აღემატება 10 მბ-ს.<br>
    მაგ.: "მომაწოდეთ გადასათარგმნი ტექსტი ვორდის ფაილის სახით. სასურველია ფაილი იყოს MS Word 2013 ფორმატში."
    </p>
  </li> 
  </ol> 
</div>
<div id="cat-info-popup">
  <ol id="cat-popup" class="joyride-list" data-joyride>
  <li data-id="cat" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების კატეგორია</h4>
    <p class="subheading s08 mb0">მნიშვნელოვანია განცხადების სწორი კატეგორიის შერჩევა. აირჩიე გასაწევი მომსახურებისათვის აზრობრივად ყველაზე ახლო მდგომი კატეგორია.
    </p>
  </li> 
  </ol> 
</div>
<div id="tags-info-popup">
  <ol id="tags-popup" class="joyride-list" data-joyride>
  <li data-id="tags" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>მინიშნებები</h4>
    <p class="subheading s08 mb0">იმისათვის, რომ გაუადვილო დამკვეთს შენი შეთავაზების პოვნა, განსაზღვრე და ჩაწერე განცხადების შინაარსთან ყველაზე მჭიდროდ დაკავშირებული რამდენიმე საკვანძო სიტყვა.<br>მაგ.: "სცენარი, სარეკლამო რგოლი".
    </p>
  </li> 
  </ol> 
</div>
<div id="duration-info-popup">
  <ol id="duration-popup" class="joyride-list" data-joyride>
  <li data-id="duration" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>რა დროში შეასრულებ სამუშაოს</h4>
    <p class="subheading s08 mb0">სამუშაოს დროული ჩაბარება ძალიან მნიშვნელოვანია როგორც რეპუტაციისათვის, ასევე შესრულებული სამუშაოს ანაზღაურებისათვის.<br>
    მიუთითე მაქსიმალური, მაგრამ გონივრული ვადა დღეების რაოდენობის სახით. შეკვეთის გაფორმებიდან, არაუგვიანეს მითითებული ვადისა უნდა ჩააბარო სამუშაო დამკვეთს, წინააღმდეგ შემთხვევაში შეკვეთა გაუქმდება, თანხა დაუბრუნდება დამკვეთს, ხოლო შენი რეიტინგი დაიკლებს.<br>
    მაგ.: "5" (დღე)
    </p>
  </li> 
  </ol> 
</div>

<div id="video-info-popup">
  <ol id="video-popup" class="joyride-list" data-joyride>
  <li data-id="video" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>ვიდეო-რგოლი</h4>
    <p class="subheading s08 mb0">ვიდეო-რგოლი უფრო მიმზიდველს და სახალისოს გახდის შენს განცხადებას. მისი საშუალებით შეგიძლია:</p>
    <ul class="subheading ml20">
      <li>გააცნო საკუთარი თავი და მოუყვე დამკვეთს შენი მომსახურების შესახებ;</li>
      <li>ვიდეო-კოლაჟის სახით წარმოაჩინო საკუთარი პორტფოლიო.</li>
    </ul>
    <p class="subheading s08">
    წინასწარ ატვირთე შენი ვიდეო-რგოლი <a href="https://www.youtube.com/upload" target="_blank" rel="nofollow">YouTube.com</a>-ზე, ან <a href="http://www.myvideo.ge/c/uploadnew" rel="nofollow" target="_blank">MyVideo.ge</a>-ზე და მიუთითე ბმული აღნიშნულ ველში.</p>
  </li>
  </ol>
</div>

<div id="moneyback-info-popup">
  <ol id="moneyback-popup" class="joyride-list" data-joyride>
  <li data-id="moneyback" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>სამუშაოს შედეგის გარანტია</h4>
    <span class="subheading s08">თუ დამკვეთი უკმაყოფილოა მიღებული შედეგით, მას უფლება აქვს მოითხოვოს თანხის დაბრუნება. სამუშაოს შედეგის და შესაბამისად, თანხის დაბრუნების გარანტია ძალიან მნიშვნელოვანია დისტანციური საქმიანობის პოპულარიზაციისათვის. შეცვალე ეს პარამეტრი მხოლოდ იმ შემთხვევაში, თუ:</span>
    <ol class="subheading s08 p10 mb0">
    <li>შენს მიერ შესრულებული სამუშაო სპეციფიურია და მისი ჩაბარების შემდეგ თანხას ვერ დაუბრუნებ დამკვეთს <strong>და</strong></li><li>სამუშაოს წინასწარი ან ნაწილობრივი ჩაბარება შენს შემთხვევაში შეუძლებელია <strong>და</strong></li><li>გააზრებული გაქვს, რომ ასეთ მომსახურებას დამკვეთი შეეცდება მოერიდოს.</li></ol>
  </li>
  </ol>
</div>

<script>
  var startNewDealTour=false;
  var dealSubmittedTour = false;
  var editform = true;
  var lang = [];
  lang['change'] = '{$lang563}';
  lang['notimage'] = '{$lang564}';
  lang['minsize'] = '{$lang565}';
  lang['minprice'] = '{$lang838}';
  lang['maxprice'] = '{$lang839}';
  lang['onlyinteger'] = '{$lang840}';
  lang['videohost'] = '{$lang757}';
  var def_js = ['{$baseurl}/js/upload.js','{$baseurl}/js/deal.js?v=13','{$baseurl}/js/submitprocessor.js?v=7'];
</script>