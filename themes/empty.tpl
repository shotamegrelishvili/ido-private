<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/{if $pathURI}{$pathURI}{else}settings{/if}" class=current>{if $pathTitle}{$pathTitle}{else}{$lang31}{/if}</a>
	</ul>
</div>

{if $addWorkAreaWrapper}
<div class="work-area-wrapper">
{/if}
<div class="row mt20">
	<div class="large-12 small-12 columns">
		<h1 class="heading h-large mb0">{if $heading}{$heading}{else}{$lang708}{/if}</h1>
		<h2 class="subheading">{if $subheading}{$subheading}{else}{$lang709}{/if}</h2>
	</div>
	{if $message ne "" OR $error ne ""}<div class="large-12 small-12 columns mt20 mb20">{include file="error.tpl"}</div>{/if}
</div>