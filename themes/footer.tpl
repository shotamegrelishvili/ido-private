{if $addWorkAreaWrapper}
</div>
{/if}
<div class="footer">
	<div class="row">
		<div class="large-3 small-6 columns">
			<h3 class="heading">{$lang535}</h3>
			<ul class="subheading">
				<li><i class="icon-facebook"></i> <a href="//facebook.com/iDoGeorgia" target="_blank">ფეისბუქზე</a>
				<li><i class="icon-google-plus"></i> <a href="//plus.google.com" target="_blank">Google+</a>
				<li><i class="icon-rss"></i> <a href="{$baseurl}/rss" target="_blank">RSS არხი</a>
			</ul>
		</div>
		<div class="large-3 small-6 columns">
			<h3 class="heading">{$lang536}</h3>
			<ul class="subheading">
				<li><i class="icon-question"></i> <a href="{$baseurl}/about">{$lang522}</a>
				<li><i class="icon-bell"></i> <a href="{$baseurl}/terms_of_service">{$lang253}</a>
				<li><i class="icon-check"></i> <a href="{$baseurl}/faq">{$lang755}</a>
			</ul>
		</div>
		<div class="large-3 hide-for-small columns">
			<h3 class="heading">{$lang537}</h3>
			<ul class="subheading">
				<li><i class="icon-info-sign"></i> <a href="{$baseurl}/about">{$lang416}</a>
				<li><i class="icon-edit"></i> <a href="{$baseurl}/contact">{$lang417}</a>
			</ul>
		</div>
		<div class="large-3 hide-for-small columns">
			<h3 class="heading">{$lang538}</h3>
			<ul class="subheading">
				<li><i class="icon-smile"></i> {insert name=get_total_posts assign=cc}{$cc} {$lang533}
				<li><i class="icon-user"></i> {insert name=get_total_members assign=mcc}{$mcc} {$lang534}
			</ul>
		</div>
	</div>
	<div class="row hide-for-medium-up">
		<div class="small-6 columns">
			<h3 class="heading">{$lang537}</h3>
			<ul class="subheading">
				<li><i class="icon-info-sign"></i> <a href="{$baseurl}/about">{$lang416}</a>
				<li><i class="icon-edit"></i> <a href="{$baseurl}/contact">{$lang417}</a>
			</ul>
		</div>
		<div class="small-6 columns">
			<h3 class="heading">{$lang538}</h3>
			<ul class="subheading">
				<li><i class="icon-smile"></i> {insert name=get_total_posts assign=cc}{$cc} {$lang533}
				<li><i class="icon-user"></i> {insert name=get_total_members assign=mcc}{$mcc} {$lang534}
			</ul>
		</div>
	</div>
</div>

{if (!$smarty.session.FB_isFan) AND !$smarty.cookies['FB_isFan'] AND (!$smarty.cookies['FB_isFan_delayed'] OR $smarty.cookies['FB_isFan_delayed'] LT $smarty.now) AND !$FB_Fan_hide}
<div id="fb-like-wrapper" class="modal-window">
	<a href="javascript:" class="modal-close" data-id="fb-like-wrapper" data-qp-active="1" data-cookie="FB_isFan_delayed" data-cookie-value="{($smarty.now+86400)}"><i class="icon-remove icon-large"></i></a>
	<div class="hide-for-small">
		<strong class="f16 subheading mb20 block">შენი მხარდაჭერა მნიშვნელოვანია, ურჩიე მეგობრებს <span class="blue">www.iDo.ge</span></strong>
		<div class="fb-like" data-href="https://www.facebook.com/iDoGeorgia" data-width="441" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="false"></div>
	</div>
	<div class="show-for-small">
		<strong class="f16 subheading block">ურჩიე მეგობრებს <span class="blue">www.iDo.ge</span></strong>
		<div class="fb-like" data-href="https://www.facebook.com/iDoGeorgia" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
	</div>
	{*<div class="fb-like-box" data-href="https://www.facebook.com/iDoGeorgia" data-width="441" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>*}
	{*<div class="fb-like" data-href="https://www.facebook.com/iDoGeorgia" data-width="441" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>*}
</div>
{/if}

<div class="qp-ui-mask-modal"></div>
<div id="sb-menu-swing-layer"></div>
{if !$newDeal AND !$inProject}
<div id="m-action"{if $hideMA} class="hide"{/if}>
	<div class="m-cw m-a">
		<a class="m-c m-c-i show-for-small" href="javascript:">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="white">
			<path d="M19 13h-6v6h-2v-6h-6v-2h6v-6h2v6h6v2z"/>
			<path d="M0 0h24v24h-24z" fill="none"/>
			</svg>
		</a>
		<a class="m-c hide-for-small" href="{$baseurl}/new?jobseeker=1">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="white">
			<path d="M19 13h-6v6h-2v-6h-6v-2h6v-6h2v6h6v2z"/>
			<path d="M0 0h24v24h-24z" fill="none"/>
			</svg>
		</a>
	</div>
	<div class="m-am m-am-bottom">
		<a href="{$baseurl}/suggested"><span class="m-am-txt">ვეძებ შემსრულებელს</span><i class="icon-search m-a m-am-2"></i></a>
		<a href="{$baseurl}/new?jobseeker=1"><span class="m-am-txt">ვეძებ სამუშაოს</span><i class="icon-pencil m-a m-am-1"></i></a>
	</div>
</div>
{/if}
<script>
	var base_url = "{$baseurl}",main_page={if $main_page}1{else}0{/if};
	var fh = {if $smarty.cookies['hv'] OR ($main_page AND $smarty.session.USERID) OR !$main_page}1{else}0{/if};
	var FB_isFan = {if $smarty.session.FB_isFan OR $smarty.cookies['FB_isFan']}1{else}0{/if};
	var FB_isFan_delayed = {if $smarty.cookies['FB_isFan_delayed'] AND $smarty.cookies['FB_isFan_delayed'] > $smarty.now}1{else}0{/if};
	var FB_Fan_hide = {if $FB_Fan_hide}1{else}0{/if};
	var ISUSER = "{if $smarty.session.USERID}1{/if}";
</script>

<script src="{$baseurl}/js/jquery.toolsnew.min.js"></script>
<script src="{$baseurl}/js/f551.min.js"></script>
<script src="{$baseurl}/js/jquery.scrollTo-min.js?v=2"></script>
<script src="{$baseurl}/js/main.js{insert name=file_version value="js/main.js"}"></script>
{if $plupload}
<script src="{$baseurl}/js/jquery.ui.min.js"></script>
<script src="{$baseurl}/js/plupload212/plupload.full.min.js"></script>
<script src="{$baseurl}/js/plupload212/plupload.ui.custom.js"></script>
<script src="{$baseurl}/js/plupload212/i18n/ka.js?v=1"></script>
{/if}
<script src="{$baseurl}/js/geo0.3.3.js" defer></script>

<script>
	$(document).foundation();
</script>

{if $enable_fc eq "1"}
<div id="fb-root"></div>
{/if}
<script>
{if $smarty.session.USERID ne ""}
{literal}
function loadContent(elementSelector, sourceURL) {
$(""+elementSelector+"").load(""+sourceURL+"");
}
{/literal}
{else}
{literal}

// USER LOGGED IN
{/literal}
{/if}

{if $environment=='production'}
{literal}
// FOR GUESTS/USERS

// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-56061791-1', 'auto');
ga('send', 'pageview');

// Facebook Remarketing
(function() {
	window._pa = window._pa || {};
	{/literal}
	{if !$smarty.session.USERID}
	_pa.productId = "Guest";
	{else}
	_pa.productId = "{$smarty.session.USERID}";
	{/if}
	{if $newDeal}
	_pa.orderId = "NewDeal";
	{elseif $retargetingPID}
	_pa.orderId = "PID.{$retargetingPID}";
	{/if}
	var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
	pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/5450a935b364d18ce500003f.js";
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
})();

{literal}
// InspectLet JS Code
window.__insp = window.__insp || [];
__insp.push(['wid', 1172693910]);
(function() {
	function __ldinsp(){var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
	if (window.attachEvent){
		window.attachEvent('onload', __ldinsp);
	}else{
		window.addEventListener('load', __ldinsp, false);
	}
})();
// InspectLet JS Code end

// Zopim
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2aQDpn99g6UjCMNwvYB9l0ASScZT3YEm';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
{/literal}
{if $smarty.session.USERID}{literal}$zopim(function(){{/literal}$zopim.livechat.setName('{$smarty.session.USERNAME|escape}');$zopim.livechat.setEmail('not@required.com');{literal}});{/literal}{/if}
// Zopim End
{/if}{* END SCRIPTS RUNNING ON PRODUCTION ENV ONLY *}
</script>
{if $uploadify eq 1}
<script src="{$baseurl}/js/jquery.uploadify.v2.1.0.min.js"></script>
{/if}
{* if $viewpage eq 1}
<script src="{$baseurl}/js/swfobject.js"></script>
<script src="//assets.pinterest.com/js/pinit.js"></script>
<script src="https://apis.google.com/js/plusone.js"></script>
{/if *}
<script src="{$baseurl}/js/jquery.periodicalupdater.js?v=2" defer></script>
{if $logout}
{include file="log_out.tpl"}
{/if}
</body>
</html>