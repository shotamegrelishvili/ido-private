{if $smarty.server['HTTP_HOST'] != 'localhost' AND $eConnect_disconnect}
<script src="https://www.emoney.ge/js/econnect/v1/lib.js"></script>
<script src="https://www.emoney.ge/js/econnect/v1/cl.js"></script>

<div id="eMoneyAuthorizeWidgetDiv" style="z-index:997;height:100%;width:100%;background-color: #333333;opacity:0.4;display: none;"></div>

<iframe id="eMoneyAuthorizeWidgetFrame" src="https://www.emoney.ge/index.php/widget/login_common?lang=ka&amp;protocol={$smarty.server['HTTP_X_FORWARDED_PROTO']}&amp;domain={$smarty.server['HTTP_HOST']}&amp;external_registration=0" style="position:fixed;top:0;left:0;z-index:998;height:100%;width:100%;border: 0px;display:none;"></iframe>

<script>
$(document).ready( function() {
	client.load("eMoneyAuthorizeWidgetFrame");
	client.on('check',function(accessToken){
		client.callOut('logout');
	});
})
</script>
{/if}

<div id="fb-root"></div>
{if $enable_fc eq "1" AND $smarty.session.FB eq "1"}
{literal}
<script>
	checkLeave=false;
	window.fbAsyncInit = function() {
		FB.init({
		appId      : '{/literal}{$FACEBOOK_APP_ID}{literal}',
		status: true,
		cookie: true,
		xfbml      : true,
		version    : 'v2.1'
		});
	fbLogoutUser();
}

function fbLogoutUser() {
	FB.getLoginStatus(function(response) {
			if (response && response.status === 'connected') {
					FB.logout(function(response) {
							// window.location = "{/literal}{$baseurl}/logout{literal}";
					});
			}
			// window.location = "{/literal}{$baseurl}/logout{literal}";
	});
}

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

if (typeof('settingsPage')!='undefined') {
	// window.location = "{/literal}{$baseurl}/logout{literal}";
}
</script>
{/literal}
{else}
{literal}
<script>
	// window.location = "{/literal}{$baseurl}/logout{literal}"
</script>
{/literal}
{/if}