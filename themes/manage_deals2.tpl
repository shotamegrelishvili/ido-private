<div class="crumbs">
	<ul class="row collapse heading">
		<li><a href="{$baseurl}/">{$lang0}</a>
		<li><a href="{$baseurl}/manage_deals" class=current><i class="icon-briefcase"></i> {$lang591}</a>
	</ul>
</div>

<div class="row collapse mt20">
	<div class="large-12 small-12 columns">
		<h1 class="heading h-large mb0">{$lang591}</h1>
		<h2 class="subheading" id="odp-heading">{if $smarty.request.tab eq "orders" OR ($totalOrders>0 AND !$smarty.request.tab)}{$lang223}{elseif $smarty.request.tab eq "deals" OR (!$smarty.request.tab AND $totalOrders eq '0' AND $dealsTotal > 0)}{$lang571}{elseif $smarty.request.tab eq "pending"}{$lang752}{/if}</h2>
	</div>
	{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div id="se-message" class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
</div>
<div class="row ul-row mt20">
	<ul class="large-8 small-12 columns">
		<li><a href="javascript:" data-action="orders" data-searchin="{$lang782}" class="rel medium button raised orders {if $smarty.request.tab eq "orders" OR ($totalOrders>0 AND !$smarty.request.tab)}primary-alt active{$odpCurrent='orders'}{else}secondary{/if} radius tabControlDealsOrders subheading"><i class="icon-shopping-cart"></i> შეკვეთები<span class="notify-wrapper notify-wrapper-right{if $orderscountc>0 OR $orderscountm>0} nw-active{/if}">{$totalOrders}</span></a></li>
		<li><a href="javascript:" data-action="deals" data-searchin="{$lang783}" class="medium button raised deals {if $smarty.request.tab eq "deals" OR (!$smarty.request.tab AND $totalOrders eq '0' AND $dealsTotal>0 AND $pendingOrders eq '0')}primary-alt active{$odpCurrent='deals'}{else}secondary{/if} radius tabControlDealsOrders subheading"><i class="icon-file-text"></i> განცხადებები<span class="notify-wrapper notify-wrapper-right">{$dealsTotal}</span></a></li>
		<li><a href="javascript:" data-action="pending" data-searchin="{$lang784}" class="rel medium button raised orders {if $smarty.request.tab eq "pending" OR (!$smarty.request.tab AND $totalOrders eq '0' AND $pendingOrders>0)}primary-alt active{$odpCurrent='pending'}{else}secondary{/if} radius tabControlDealsOrders subheading">¢ დაკვეთები<span class="notify-wrapper notify-wrapper-right{if $countn>0} nw-active{/if}">{($counta+$countb+$countc+$countd)}</span></a></li>
	</ul>
	{* Search within *}
	<div class="large-4 small-12 columns">
	  <div class="row collapse odp-search-wrapper{if !$odpCurrent OR ($odpCurrent=='orders' AND $totalOrders<2) OR ($odpCurrent=='deals' AND $dealsTotal<2) OR ($odpCurrent=='pending' AND ($counta+$countb+$countc+$countd)<2)} hide{/if}">
	    <form id="searchOrders" name="searchOrders" action="{$baseurl}/manage_deals?b={$smarty.request.b}&amp;a={$smarty.request.a}" method="post" class="custom odp-search">
	      <div class="large-9 small-10 columns">
	        <div class="geo-box gbs-vmiddle">
	          <input type="text" id="odp-search" data-placeholder-prefix="{$lang129}" name="search" class="geo subheading" placeholder="{$lang129} {if $odpCurrent=='orders'}{$lang782}{elseif $odpCurrent=='deals'}{$lang783}{elseif $odpCurrent=='pending'}{$lang784}{/if}" value="{$smarty.post.search}">
	        </div>
	      </div>
	      <div class="large-3 small-2 columns">
	        <button onclick="javascript:document.searchOrders.submit()" class="button medium primary-alt expand button-rfix"><i class="icon-search"></i></button>
					<input type="hidden" name="tab" id="odp-search-tab" value="{$odpCurrent}">
	      </div>
	    </form>
	    {if $search AND ($ordersResultSize OR $dealsResultSize OR $pendingResultSize)}<strong class="subheading ml10">ნაპოვნია {if $odpCurrent=='orders'}{$ordersResultSize} {$lang107}{elseif $odpCurrent=='deals'}{$dealsResultSize} {$lang533}{elseif $odpCurrent=='pending'}{$pendingResultSize} {$lang785}{/if}</strong>{/if}
	  </div>
	</div>
	{* end Search within *}
</div>

{if $totalOrders+$pendingOrders+$dealsTotal eq '0' AND !$smarty.request.tab}
	<div class="row">
		<h2 class="heading">ამჟამად არცერთი განცხადება და შეკვეთა არ გაქვს</h2>
		<h3 class="subheading"><a href="{$baseurl}/">დაათვალიერე განცხადებები</a>, შეუკვეთე მომსახურება, ან <a href="{$baseurl}/new">განათავსე საკუთარი განცხადება</a> და იმუშავე სახლიდან გაუსვლელად!</h3>
	</div>
{/if}
<div class="pending-section odp-toggle-section wa-h mt20{if $smarty.request.tab eq "pending" OR ($totalOrders eq '0' AND $pendingOrders>0 AND !$smarty.request.tab)}{else} hide{/if}">

{if $pendingOrders > 0}
	<div class="row show-for-small tcenter subheading s08 gray mb3" id="type-selector1">
		<i class="icon-info-sign"></i> ასარჩევად დააჭირე 1 წამის განმავლობაში:
	</div>
{/if}
<div class="row tcenter">
	{*<div class="large-2 columns has-tip nb" data-tooltip data-width=260 title="სამუშაოს დასაწყებად საჭიროა მიაწოდო ინფორმაცია"><a href="{$baseurl}/manage_deals?tab=pending&amp;s=reqs&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $s eq "reqs"}talltab-active{/if} has-tip2" >
			<strong class="heading h-large gray block">{$countm}</strong>
			<span class="subheading s08">საჭიროებს უკუკავშირს</span>
		</a>
	</div>
	<div class="large-2 columns has-tip nb" data-tooltip data-width=220 title="ელოდება შესრულებული სამუშაოს შეფასებას"><a href="{$baseurl}/manage_deals?tab=pending&amp;s=review&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $s eq "review"}talltab-active{/if}">
			<strong class="heading h-large gray block">{$countc}</strong>
			<span class="subheading s08">ელოდება შეფასებას</span>
		</a>
	</div>*}
	<div class="large-2 large-offset-2 columns has-tip nb" data-tooltip data-width=270 title="მიმდინარე შეკვეთა, რომელზეც ამჟამად მუშაობ"><a href="{$baseurl}/manage_deals?tab=pending&amp;sp=active&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $sp eq "active"}talltab-active{/if}">
			<strong class="heading h-large gray block">{($countd+0)}</strong>
			<span class="subheading s08">აქტიური</span>
		</a>
	</div>
	<div class="large-2 columns has-tip nb" data-tooltip data-width=410 title="შენს მიერ სამუშაო შესრულებულია და ელოდება დამკვეთის შეფასებას"><a href="{$baseurl}/manage_deals?tab=pending&amp;sp=delivered&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $sp eq "delivered"}talltab-active{/if}">
			<strong class="heading h-large gray block">{($countc+0)}</strong>
			<span class="subheading s08">მიწოდებული</span>
		</a>
	</div>
	<div class="large-2 columns has-tip nb" data-tooltip data-width=210 title="დასრულებული შეკვეთა"><a href="{$baseurl}/manage_deals?tab=pending&amp;sp=completed&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $sp eq "completed"}talltab-active{/if}">
			<strong class="heading h-large gray block">{($countb+0)}</strong>
			<span class="subheading s08">დასრულებული</span>
		</a>
	</div>
	<div class="large-2 columns has-tip nb" data-tooltip data-width=180 title="გაუქმებული შეკვეთა"><a href="{$baseurl}/manage_deals?tab=pending&amp;sp=cancelled&amp;b={$b}&amp;a={$a}#type-selector1" class="touch-tab {if $sp eq "cancelled"}talltab-active{/if}">
			<strong class="heading h-large gray block">{($counta+0)}</strong>
			<span class="subheading s08">გაუქმებული</span>
		</a>
	</div>
	<div class="large-2 columns">&nbsp;</div>
</div>

{if sizeof($p) > 0}
	<div class="row mt20">
		<div class="large-1 columns subheading table-head"><a href="{$baseurl}/manage_deals?s={$s}&amp;b=id&amp;a={if $a eq "asc"}desc{else}asc{/if}">დაკვეთა</a></div>
		<div class="large-4 columns subheading table-head">&nbsp;</div>
		<div class="large-2 columns subheading table-head tcenter"><i class="icon-user"></i></div>
		<div class="large-1 columns subheading table-head">&nbsp;</div>
		<div class="large-2 columns subheading table-head"><a href="{$baseurl}/manage_deals?s{$s}=&amp;b=status&amp;a={if $a eq "asc"}desc{else}asc{/if}">{$lang191}</a></div>
		<div class="large-2 columns subheading table-head"><a href="{$baseurl}/manage_deals?s={$s}&amp;b=date&amp;a={if $a eq "asc"}desc{else}asc{/if}">ჩაბარების თარიღი</a></div>
	</div>
	{section name=i loop=$p}
	<div class="row table-row-hover">
		<div class="large-1 columns subheading"><a href="{$baseurl}/track?id={$p[i].OID}">#{$p[i].OID}</a></div>
		<div class="large-4 columns subheading"><a href="{$baseurl}/track?id={$p[i].OID}">{$p[i].gtitle|stripslashes}</a></div>
		<div class="large-2 columns subheading"><a href="{$baseurl}/user/{$p[i].username|stripslashes}">{$p[i].username}</a></div>
		<div class="large-1 columns subheading">&nbsp;</div>
		<div class="large-2 columns subheading"{if $p[i].status eq "0"} title="ელოდება საჭირო ინფორმაციის მიწოდებას"{/if}><span class="ostatus-label label radius {if $p[i].status eq "0" OR $p[i].status eq "6"}alert{elseif $p[i].status eq "2" OR $p[i].status eq "3"}secondary{elseif $p[i].status eq "4" OR $p[i].status eq "5"}success{/if}">{if $p[i].status eq "0" OR $p[i].status eq "6"}{$lang586}{elseif $p[i].status eq "1"}{$lang279}{elseif $p[i].status eq "2" OR $p[i].status eq "3"}{$lang203}{elseif $p[i].status eq "4"}{$lang201}{elseif $p[i].status eq "5"}{$lang202}{/if}</span></div>
		<div class="large-2 columns subheading">{if $p[i].status eq "5"}{insert name=get_time_to_days_ago value=a time=$p[i].cltime}{else}{insert name=get_time_to_days_ago value=a time=($p[i].time_added+$p[i].days*86400)}{if ($p[i].status eq "1" OR $p[i].status eq "6") AND ($p[i].time_added+($p[i].days)*86400)-$smarty.now<86400 AND ($p[i].time_added+($p[i].days)*86400)-$smarty.now>0} <i class="icon-exclamation-sign icon-large red has-tip" data-tooltip title="სამუშაოს ჩაბარებამდე დარჩენილია 24 საათზე ნაკლები. არ დააგვიანო სამუშაოს შედეგის მიწოდება."></i>{/if}{/if}</div>
	</div>
	{/section}
	{else}
	<div class="row mt20">
		<div class="large-12 small-12 columns subheading">
			ამჟამად არცერთი დაკვეთა არ გაქვს. იაქტიურე, გაუზიარე <a href="{$baseurl}/manage_deals?tab=deals">შენი განცხადებები</a> მეგობრებს სოციალური ქსელების საშუალებით და დასაქმდი სახლიდან გაუსვლელად!
		</div>
	</div>
	{/if}
</div>


<div class="deals-section odp-toggle-section wa-h{if $smarty.request.tab eq "deals" OR (!$smarty.request.tab AND $totalOrders eq '0' AND $dealsTotal>0 AND $pendingOrders eq '0')}{else} hide{/if}">
{if !$dealsTotal}
<div class="row tcenter mt20 subheading">
	<strong class="block mb20">შეთავაზე შენი მომსახურება საზოგადოებას და დასაქმდი სახლიდან გაუსვლელად:</strong>
	<a href="{$baseurl}/new?jobseeker=1" class="button large success raised">განათავსე განცხადება</a>
</div>
{else}
<div class="row collapse mt20">
	<div class="small-5 columns show-for-small">
		<select name="selection" class="inline-block subheading selection">
			<option value="">მოვნიშნოთ:</option>
			<option value="all">{$lang187}</option>
			<option value="none">{$lang188}</option>
			<option value="inactive">{$lang189}</option>
			<option value="active">{$lang190}</option>  
		</select>
	</div>
	<div class="large-8 columns subheading filters show-for-medium">
		<ul>
			<li><strong class="heading">მოვნიშნოთ: </strong></li>
			<li><a href="javascript:" class="selection-link subheading select-all">{$lang187}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-none">{$lang188}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-inactive">{$lang189}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-active">{$lang190}</a></li>
		</ul>
	</div>
	<div class="large-6 columns subheading filters hide-for-medium-down">
		<ul>
			<li><strong class="heading">მოვნიშნოთ: </strong></li>
			<li><a href="javascript:" class="selection-link subheading select-all">{$lang187}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-none">{$lang188}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-inactive">{$lang189}</a></li>
			<li><a href="javascript:" class="selection-link subheading select-active">{$lang190}</a></li>
		</ul>
	</div>
	<div class="small-6 small-offset-1 columns tright show-for-small">
		<select name="deal-controls" class="deal-controls subheading" disabled>
			<option value="">&nbsp;&nbsp;ქმედება:</option>
			<option value="btn-suspend">{$lang183}</option>
			<option value="btn-activate">{$lang184}</option>
			<option value="btn-delete">{$lang185}</option>
		</select>
	</div>
	<div class="large-3 large-offset-1 columns tright show-for-medium">
		<select name="deal-controls" class="deal-controls subheading" disabled>
			<option value="">&nbsp;&nbsp;ქმედება:</option>
			<option value="btn-suspend">{$lang183}</option>
			<option value="btn-activate">{$lang184}</option>
			<option value="btn-delete">{$lang185}</option>
		</select>
	</div>
	<div class="filters large-6 columns hide-for-medium-down tright">
		<ul class="deal-controls">
			<li><strong class="heading">ქმედება: </strong></li>
			<li class=subheading><a class="btn-suspend disabled" href="javascript:"><i class="icon-collapse-alt"></i> {$lang183}</a></li>
			<li class=subheading><a class="btn-activate disabled" href="javascript:"><i class="icon-check"></i> {$lang184}</a></li>
			<li class=subheading><a class="btn-delete disabled" href="javascript:"><i class="icon-trash"></i> {$lang185}</a></li>
		</ul>
	</div>
</div>
<form action="{$baseurl}/manage_deals" id="deals_form" method="post" class="mt10">
<div class="row subheading">
	<div class="large-1 small-1 columns table-head">&nbsp;</div>
	<div class="large-2 small-1 columns table-head tcenter"><i class="icon-picture"></i></div>
	<div class="large-7 small-7 columns table-head heading">სათაური</div>
	<div class="large-2 small-3 columns table-head tcenter"><i class="icon-bar-chart"></i></div>
</div>
	{section name=i loop=$posts}
{insert name=seo_clean_titles assign=title value=a title=$posts[i].gtitle}
	<div class="row table-row-hover subheading"{if $posts[i].active eq "5"} title="განცხადება საჭიროებს ცვლილებების შეტანას. იხილე ელფოსტა საიტის ადმინისტრაციისგან."{elseif $posts[i].active eq "6"} title="{$lang677}"{/if}>
		<div class="large-1 small-1 columns subheading tcenter">
		{if $posts[i].active eq "1"}
			<input class="checkbox active" name="deal[]" type="checkbox" id="deal-{$posts[i].PID}" value="{$posts[i].PID}"><i class="icon-check-empty icon-large i-checkbox" data-dealid="{$posts[i].PID}"></i>
			{elseif $posts[i].active eq "2"}
			<input class="checkbox inactive" name="deal[]" type="checkbox" value="{$posts[i].PID}"><i class="icon-check-empty icon-large i-checkbox"></i>
			{elseif $posts[i].active eq "5"}{* SOFT REJECT *}
			<input class="checkbox inactive" name="deal[]" type="checkbox" value="{$posts[i].PID}"><i class="icon-check-empty icon-large i-checkbox"></i>
			{elseif $posts[i].active eq "6"}{* PRE-APPROVED, Needs PIN and eMoney *}
			<input class="checkbox active" name="deal[]" type="checkbox" value="{$posts[i].PID}"><i class="icon-check-empty icon-large i-checkbox"></i>
			{elseif $posts[i].active eq "0"}
			<input class="checkbox pending" name="deal[]" type="checkbox" value="{$posts[i].PID}"><i class="icon-check-empty icon-large i-checkbox"></i>
		{/if}
		</div>
		<div class="large-2 small-1 columns tcenter">
			<a href="{$baseurl}/edit?id={$posts[i].PID}"><img src="{$purl}/t2/{$posts[i].p1}" alt=""></a>
		</div>
		<div class="large-7 small-7 columns subheading">
			{if $posts[i].active eq "1"}
			<a href="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" target=_blank><i class="icon-external-link gray"></i></a> {/if}
			<a href="{$baseurl}/edit?id={$posts[i].PID}">{$posts[i].gtitle|stripslashes|mb_truncate:60:"...":'UTF-8'} {if $posts[i].price ne "0"}({$posts[i].price|stripslashes} {$lang63}){else}({$lang743}){/if}</a>
			<p class="subheading">
				{$posts[i].gdesc|stripslashes|truncate:61:"..":true}
			</p>
			<a href="javascript:" class="button small round heading 
			{if $posts[i].active eq "1"}select-active success">{$lang192}
			{elseif $posts[i].active eq "2"}select-inactive">{$lang193}
			{elseif $posts[i].active eq "5"}select-inactive">{$lang320}
			{elseif $posts[i].active eq "6"}select-active alert" title="{$lang677}">{$lang278}
			{elseif $posts[i].active eq "0"}select-pending secondary">{$lang194}
			{elseif $posts[i].active eq "4"} alert">{$lang502}
			{/if}
			</a>
			<span class="ratings">{for $st=1 to 5}{if $posts[i].rating >= $st*20}<i class="icon-star"></i>{elseif $posts[i].rating > (($st-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}<small>(<span data-tooltip class="has-tip" data-width="210" title="{$posts[i].rcount} {$lang529}">{$posts[i].rcount}</span>)</small></span>
		</div>
		<div class="large-2 small-3 columns subheading tcenter">
			<div class="row collapse">
				<div class="large-4 small-4 columns">
					<span class="has-tip" data-tooltip data-width=170 title="{$lang181}"><i class="icon-eye-open"></i> <span class="s08">{$posts[i].viewcount|stripslashes}</span></span>
				</div>
				<div class="large-4 small-4 columns">
					<a href="{$baseurl}/manage_deals?tab=orders&amp;status=active" class="has-tip ml10" data-tooltip data-width=175 title="{$lang195}"><i class="icon-spinner"></i> <span class="s08">{insert name=active_orders value=a assign=ao PID=$posts[i].PID}{$ao}</span></a>
				</div>
				<div class="large-4 small-4 columns">
					<span class="has-tip ml10" data-tooltip data-width=205 title="{$lang196}"><i class="icon-check-sign"></i> <span class="s08">{insert name=done_orders value=a assign=co PID=$posts[i].PID}{$co}</span></span>
				</div>
			</div>
			<div class="row collapse mt5">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style mt3">
				<a class="addthis_button_preferred_2" addthis:url="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" addthis:title="{$lang555} - {$baseurl}/{$profileurl}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				<a class="addthis_button_preferred_1" fb:like:layout="button_count" addthis:url="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" addthis:title="{$lang555}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				<a class="addthis_button_preferred_5" addthis:url="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" addthis:title="{$lang555} - {$baseurl}/{$profileurl}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				<a class="addthis_button_compact"></a>
				<a class="addthis_counter addthis_bubble_style" addthis:url="{$baseurl}/{$posts[i].seo|stripslashes}/{$posts[i].PID|stripslashes}/{$title}" addthis:title="{$lang555} - {$baseurl}" addthis:description="{$site_name} - {$lang556}. {$lang557}"></a>
				</div>
				<!-- AddThis Button END -->
			</div>
		</div>
	</div>
	{/section}
	<input type="hidden" name="subme" value="1">
</form>
<div class="row pagination-centered">
	<ul class="pagination">
		{$pagelinks}
	</ul>
</div>
{/if}
</div>

{include file="orders.tpl"}

<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-522ee0e443fae154" defer></script>
<script>
	var lang = [];
	lang['search_in'] = '{$lang570}';
	lang['sure'] = '{$lang574} {$lang575}';
	lang['deals_selected'] = '{$lang571}';
	lang['orders_selected'] = '{$lang223}';
	lang['pending_selected'] = '{$lang752}';
	var def_js = ['{$baseurl}/js/manage_deals.js{insert name=file_version value="js/manage_deals.js"}'];
</script>