{*<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/manage_deals?tab=orders" class=current>{$lang157}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang157}</h1>
    <h2 class="subheading">{$lang223}</h2>
  </div>
  {if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
</div>

<div class="mt20"></div>
*}

<div class="orders-section odp-toggle-section mt20 wa-h{if $smarty.request.tab eq "orders" OR !$smarty.request.tab AND $totalOrders>0}{else} hide{/if}">
{if $totalOrders > 0}
  <div class="row show-for-small tcenter subheading s08 gray mb3" id="type-selector2">
    <i class="icon-info-sign"></i> ასარჩევად დააჭირე 1 წამის განმავლობაში:
  </div>
{/if}
<div class="row tcenter{if !($orderscounta + $orderscountb + $orderscountc + $orderscountd + $orderscountm)} hide{/if}">
  <div class="large-2 large-offset-1 columns has-tip talltab-hover nb" data-tooltip data-width=260 title="სამუშაოს დასაწყებად საჭიროა მიაწოდო ინფორმაცია"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=reqs&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "reqs"}talltab-active{if $orderscountm} talltab-red{/if}{/if}">
      <strong class="heading h-large {if !$orderscountm || $so eq "reqs"}light-{/if}gray block">{$orderscountm}</strong>
      <span class="subheading s08">საჭიროებს უკუკავშირს</span>
    </a>
  </div>
  <div class="large-2 columns has-tip talltab-hover nb" data-tooltip data-width=220 title="ელოდება შესრულებული სამუშაოს შეფასებას"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=review&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "review"}talltab-active{/if}">
      <strong class="heading h-large {if !$orderscountc}light-{/if}gray block">{$orderscountc}</strong>
      <span class="subheading s08">ელოდება შეფასებას</span>
    </a>
  </div>
  <div class="large-2 columns has-tip talltab-hover nb" data-tooltip data-width=270 title="მიმდინარე შეკვეთა, რომელზეც ამჟამად მიმდინარეობს მუშაობა"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=active&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "active"}talltab-active{/if}">
      <strong class="heading h-large {if !$orderscountd}light-{/if}gray block">{$orderscountd}</strong>
      <span class="subheading s08">აქტიური</span>
    </a>
  </div>
  {*<div class="large-2 columns has-tip talltab-hover nb" data-tooltip data-width=410 title="შემსრულებელმა შეასრულა სამუშაო. გადაამოწმე შესრულებული სამუშაოს ხარისხი"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=delivered&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "delivered"}talltab-active{/if}">
      <strong class="heading h-large {if !$orderscountc}light-{/if}gray block">{$orderscountc}</strong>
      <span class="subheading s08">მიწოდებული</span>
    </a>
  </div>*}
  <div class="large-2 columns has-tip talltab-hover nb" data-tooltip data-width=210 title="დასრულებული შეკვეთა"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=completed&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "completed"}talltab-active{/if}">
      <strong class="heading h-large {if !$orderscountb}light-{/if}gray block">{$orderscountb}</strong>
      <span class="subheading s08">დასრულებული</span>
    </a>
  </div>
  <div class="large-2 columns has-tip talltab-hover nb" data-tooltip data-width=180 title="გაუქმებული შეკვეთა"><a href="{$baseurl}/manage_deals?tab=orders&amp;so=cancelled&amp;b={$b}&amp;a={$a}#type-selector2" class="touch-tab {if $so eq "cancelled"}talltab-active{/if}">
      <strong class="heading h-large {if !$orderscounta}light-{/if}gray block">{$orderscounta}</strong>
      <span class="subheading s08">გაუქმებული</span>
    </a>
  </div>
  <div class="large-1 columns">&nbsp;</div>
</div>
{if $orderscountm AND $so eq "reqs"}
<div class="row rel mb10 hide-for-small">
  <span class="reqs-helper-art"></span>
  <div class="mt20"></div>
</div>
{/if}
<div class="mt30 hide-for-small"></div>

{if $o|@sizeof eq "0"}
<div class="row tcenter panel">
  <h2 class="heading">შეკვეთების სია ცარიელია</h2>
  <span class="subheading">იაქტიურე, დაათვალიერე შემოთავაზებები და შეუკვეთე მომსახურება!</span>
</div>
<p class="tcenter"><i class="icon-arrow-down icon-2x green"></i></p>
  {assign var=showcats value=1}
  {insert name=get_categories assign=cats}
  {include file="categories.tpl"}
{else}
<div class="row hide-for-small">
  <div class="large-1 columns subheading table-head"><a href="{$baseurl}/manage_deals?tab=orders&amp;so={$so}&amp;b=id&amp;a={if $a eq "asc"}desc{else}asc{/if}">შეკვეთა</a></div>
  <div class="large-4 columns subheading table-head">&nbsp;</div>
  <div class="large-2 columns subheading table-head tcenter"><i class="icon-user"></i></div>
  <div class="large-1 columns subheading table-head">&nbsp;</div>
  <div class="large-2 columns subheading table-head"><a href="{$baseurl}/manage_deals?tab=orders&amp;s{$so}=&amp;b=status&amp;a={if $a eq "asc"}desc{else}asc{/if}">{$lang191}</a></div>
  <div class="large-2 columns subheading table-head"><a href="{$baseurl}/manage_deals?tab=orders&amp;so={$so}&amp;b=date&amp;a={if $a eq "asc"}desc{else}asc{/if}">თარიღი</a></div>
</div>

{section name=i loop=$o}
<div class="row table-row-hover">
  <div class="large-1 small-12 columns subheading"><a href="{$baseurl}/track?id={$o[i].OID}">#{$o[i].OID}</a></div>
  <div class="large-4 small-12 columns subheading"><a href="{$baseurl}/track?id={$o[i].OID}">{$o[i].gtitle|stripslashes}</a></div>
  <div class="large-2 small-12 columns subheading"><a href="{$baseurl}/user/{$o[i].username|stripslashes}" class="show-for-small"><i class="icon-user"></i> {$o[i].username}</a><a href="{$baseurl}/user/{$o[i].username|stripslashes}" class="hide-for-small">{$o[i].username}</a></div>
  <div class="large-1 hide-for-small columns subheading">&nbsp;</div>
  <div class="large-2 small-12 columns subheading"{if $o[i].status eq "0"} title="ელოდება საჭირო ინფორმაციის მიწოდებას"{elseif $o[i].status eq "2"} title="ურთიერთშეთანხმებით გაუქმებული შეკვეთა"{/if}><span class="ostatus-label label radius {if $o[i].status eq "0" || $o[i].status eq "6"}alert{elseif $o[i].status eq "2" OR $o[i].status eq "3"OR $o[i].status eq "3" OR $o[i].status eq "7"}secondary{elseif $o[i].status eq "4" OR $o[i].status eq "5"}success{/if}">{if $o[i].status eq "0"}{$lang586}{elseif $o[i].status eq "1" OR $o[i].status eq "6"}{$lang279}{elseif $o[i].status eq "2" OR $o[i].status eq "3" OR $o[i].status eq "7"}{$lang203}{elseif $o[i].status eq "4"}{$lang201}{elseif $o[i].status eq "5"}{$lang202}{/if}</span></div>
  <div class="large-2 small-12 columns subheading">{insert name=get_time_to_days_ago value=a time=$o[i].time_added}</div>
</div>
{/section}
{/if}
</div>
<div class="mt50 hide-for-small"></div>

{*

              	<script src="{$baseurl}/js/jquery.qtip-1.0.0-rc3.js" type="text/javascript"></script> 
				<script src="{$baseurl}/js/jquery.corner.js" type="text/javascript"></script> 
				<script src="{$baseurl}/js/indexes.js" type="text/javascript"></script> 
              <div class="main-wrapper">
                <div id="main">
                  <div class="content">
                  {if $message ne ""}
                  {include file="error.tpl"}
                  {/if}
                    <div class="wrapper">
                      <div class="page-title">
                        <h2>{$lang157}<b class="balance">{$lang223}</b></h2>
                      </div>
                      <div class="metadata">
                        <ul class="filters">
                    
                          <li>{if $so eq "active"}<span>{$lang190} <u>{$o|@count}</u></span>{else}<a href="{$baseurl}/manage_deals?tab=orders&amp;so=active&amp;b={$b}&amp;a={$a}"><span>{$lang190} <u>{$orderscountd}</u></span></a>{/if}</li>
                          <li>{if $so eq "review"}<span>{$lang224} <u>{$o|@count}</u></span>{else}<a href="{$baseurl}/manage_deals?tab=orders&amp;so=review&amp;b={$b}&amp;a={$a}"><span>{$lang224} <u>{$orderscountc}</u></span></a>{/if}</li>
                          <li>{if $so eq "completed"}<span>{$lang202} <u>{$o|@count}</u></span>{else}<a href="{$baseurl}/manage_deals?tab=orders&amp;so=completed&amp;b={$b}&amp;a={$a}"><span>{$lang202} <u>{$orderscountb}</u></span></a>{/if}</li>
                          <li class="last">{if $so eq "cancelled"}<span>{$lang203} <u>{$o|@count}</u></span>{else}<a href="{$baseurl}/manage_deals?tab=orders&amp;so=cancelled&amp;b={$b}&amp;a={$a}"><span>{$lang203} <u>{$orderscounta}</u></span></a>{/if}</li>
                        </ul>
                      </div>
                      {if $o|@count eq "0"}
                      <div class="tabs">
                        <div class="table-container yellow index orders">
                            <div class="empty-data">
                              <h3>{$lang225}</h3>
                              <p></p>
                            </div>
                        </div>
                      </div>
                      {else}
                      <div class="tabs"> 
                        <div class="table-container yellow index orders"> 
                            <table width="100%"> 
                              <thead class="topics icons"> 
                                <tr> 
                                  <td class="first"><a href="{$baseurl}/manage_deals?tab=orders&amp;so={$so}&amp;b=id&amp;a={if $a eq "asc"}desc{else}asc{/if}">{$lang140}</a></td> 
                                  <td class="amount"><a href="{$baseurl}/manage_deals?tab=orders&amp;s{$so}=&amp;b=status&amp;a={if $a eq "asc"}desc{else}asc{/if}">{$lang191}</a></td> 
                                  <td class="last"><a href="{$baseurl}/manage_deals?tab=orders&amp;so={$so}&amp;b=date&amp;a={if $a eq "asc"}desc{else}asc{/if}">{$lang360}</a></td> 
                                </tr> 
                              </thead> 
                              <tbody> 
                                  {section name=i loop=$o}
                                  <tr class="entry"> 
                                    	<td class="gig first"><div><a href="{$baseurl}/track?id={$o[i].OID}"><div class="extended-data"><b class="order-id">#{$o[i].OID}<u>&raquo;</u></b>{$o[i].gtitle|stripslashes}</div></a></div></td> 
                                    	{if $o[i].status eq "0"}
                                      	<td class="status waiting_for_reqs" title="{$lang361}"><div>{$lang362}</div></td> 
                                        {elseif $o[i].status eq "1"}
                                            {insert name=late value=a assign=late time=$o[i].stime days=$o[i].days}
                                            {if $late eq "1"}
                                            <td class="status late_delivery" title="{$lang364}"><div>{$lang363}</div></td>
                                            {else}
                                            <td class="status in_progress" title="{$lang366}"><div>{$lang365}</div></td>
                                            {/if}
                                        {elseif $o[i].status eq "2"}
                                        <td class="status buyer_cancelled" title="{$lang367}"><div>{$lang203}</div></td>
                                        {elseif $o[i].status eq "3"}
                                        <td class="status seller_cancelled" title="{$lang369}"><div>{$lang368}</div></td>
                                        {elseif $o[i].status eq "4"}
                                        <td class="status delivered" title="{$lang370}"><div>{$lang201}</div></td>
                                        {elseif $o[i].status eq "5"}
                                      	<td class="status completed" title="{$lang371}"><div>{$lang202}</div></td>
                                        {elseif $o[i].status eq "6"}
                                        	{insert name=late value=a assign=late time=$o[i].stime days=$o[i].days}
                                            {if $late eq "1"}
                                            <td class="status late_delivery" title="{$lang364}"><div>{$lang363}</div></td>
                                            {else}
                                            <td class="status dispute" title="{$lang372}"><div>{$lang320}</div></td>
                                            {/if}
                                        {elseif $o[i].status eq "7"}
                                        <td class="status buyer_cancelled" title="{$lang374}"><div>{$lang373}</div></td>
                                        {/if}
                                    	<td class="datetime last"><div>{insert name=get_time_to_days_ago value=a time=$o[i].time_added}</div></td>              
                                  </tr>
                                  {/section} 
                            </tbody> 
                          </table> 
                        </div> 
                      </div>
                      {/if}
                    </div>
                  </div>
                  {include file="side.tpl"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      *}