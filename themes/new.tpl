<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/new" class=current>{$lang55}</a>
  </ul>
</div>

{if $smarty.session.USERID ne "" OR $smarty.get.jobseeker eq '1'}
<div class="row mt20" id="wants-type-selector-small">
  <div class="large-6 large-centered small-6 columns hide-for-small">
    <ul class="button-group radius subheading">
      <li><a href="javascript:" class="wants-work button medium white">ვეძებ სამუშაოს</a></li>
      <li class="rel"><span class="wants-job-helper-art is-jobseeker-art"></span><a href="{$baseurl}/suggested" class="button medium primary-alt">ვეძებ შემსრულებელს</a></li>
    </ul>
  </div>
  <div class="small-12 columns show-for-small"><a href="{$baseurl}/suggested" class="subheading">ეძებ შემსრულებელს?</a></div>
</div>
{else}
<div class="rel has-large-video-bg new-deal-video-bg" id="wants-type-selector">
  <div class="video-bg">
    <div class="nd-large-image-bg"></div>
  {*<div class="video-bg" data-vide-bg="mp4: {$baseurl}/res/newdealcover1, webm: {$baseurl}/res/newdealcover1, poster: {$baseurl}/res/newdealcover1" data-vide-options="position: 0% 52%, loop: true, muted: true, posterType: jpg">*}
  </div>
  <div class="row has-large-video-textblock">
    <div class="cat-central-text large-12 tcenter mt20">
      <h1 class="heading f25 mb0">განათავსე განცხადება</h1>
      <h2 class="subheading f16">აირჩიე განცხადების ტიპი და დაიწყე on-line საქმიანობა:</h2>
      <div class="show-for-small tcenter">
        <a href="javascript:" class="wants-work button large white fw-button-1">ვეძებ სამუშაოს</a><br>
        <a href="{$baseurl}/suggested" class="button large primary-alt fw-button-1">ვეძებ შემსრულებელს</a>
      </div>
      <div class="hide-for-small">
        <ul class="button-group radius subheading two-buttons-centered hide-for-small">
        <li><a href="javascript:" class="wants-work button large white fw-button-1">ვეძებ სამუშაოს</a></li>
        <li class="rel"><a href="{$baseurl}/suggested" class="button large primary-alt fw-button-1">ვეძებ შემსრულებელს</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
{/if}

{if $numofposts GT 0 OR $smarty.post.subform}
{assign var=introHidden value=1}
{else}
{assign var=introHidden value=0}
{/if}

<div id="place-deal-header" class="row{if $smarty.session.USERID eq "" AND !$smarty.get.jobseeker} hide{/if} mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang55}</h1>
    <h2 id="newdeal-h2" class="subheading{if !$introHidden} hide{/if}">{$lang61}</h2>
    {*<span class="subheading block mb10"><i class="icon-question-sign gray"></i> გჭირდება იდეა, თუ რაში შეიძლება იშოვო ფული? იხილე <a href="{$baseurl}/suggested?jobseeker=1">დამსაქმებლის განცხადებების სვეტი</a>.</span>*}
    <span id="call-newdealtour-wrapper" class="subheading{if !$introHidden} hide{/if} mb10"><i class="icon-bell gray"></i> <a href="javascript:" id="call-newdealtour"><u>იხილე განცხადების განთავსებისათვის სასარგებლო ინფორმაცია</u></a></span>
  </div>
</div>
{if $message ne "" OR $error ne "" OR $smarty.session.temp['message_title'] ne ""}<div class="row mt20"><div class="large-12 small-12 columns">{include file="error.tpl"}</div></div>{/if}

<div class="tour-wrapper{if ($smarty.session.USERID eq "" AND $smarty.get.jobseeker ne '1') OR $introHidden} hide{/if}">
<div class="row mt30 tour-progress-bar">
  <div class="large-3 columns bg-green br-white">&nbsp;</div>
  <div class="large-3 columns bg-gray br-white">&nbsp;</div>
  <div class="large-3 columns bg-gray br-white">&nbsp;</div>
  <div class="large-3 columns bg-gray">&nbsp;</div>
</div>
<div class="row mt10">
  <div class="large-12 columns">
    <ul id="newdeal-info-tour" class="info-tour">
      <li><h4 class="subheading f16">ინფორმაცია, რომელიც გამოგადგება განცხადების ფორმის შევსებამდე</h4>
      <h4 class="subheading">განცხადების განთავსება <u class="red">უფასოა</u>!</h4>
      <p class="subheading">ყველა განცხადება გადის შემოწმებას და საიტზე აისახება {$moderation_queue_period} დღის განმავლობაში.<br>დეტალური ინფორმაციის მიღება თუ როგორი განცხადებების განთავსებაა დაშვებული შესაძლებელია <a href="{$baseurl}/faq?p=seller&amp;t=dealtypes">კითხვა-პასუხის გვერდზე</a>.</p>
      </li>
      <li><h4 class="subheading f16">განცხადების განთავსების სახელმძღვანელო</h4>
      <h4 class="subheading">განცხადების შევსებისას წარმოშობილი კითხვების შემთხვევაში, ისარგებლე დახმარებით, რომელიც ყოველ ველს თან ერთვის <i class="icon-question-sign icon-large red"></i></h4>
      <p class="subheading">განცხადების სათაური და შინაარსი გასაგებად უნდა აღწერდეს თუ რა მომსახურების გაწევას გეგმავ.<br>
    აუცილებელია, განსაზღვრო ამ კონკრეტული განცხადებით რა მოცულობის სამუშაოს შეასრულებ (მაგ.: 1 გვერდი, 2 ფოტო, და ა.შ.) და დააწესო მოთხოვნილი ანაზღაურება.<br>
    განსხვავებული მოცულობის ან მომსახურების შეთავაზებისათვის, განათავსე ცალკეული განცხადებები.<br>
    მიიღება შემდეგი სახის განცხადებები:
    <br>"2 ფოტოსურათის მხატვრული დამუშავება. 15 ლარად";<br>
    "ერთი გვერდის თარგმნა რუსულიდან ინგლისურად. 10 ლარად"<br>
    <u>არ მიიღება</u> ზოგადი სახის განცხადებები:
    "ვთარგმნი ტექსტებს ქართულიდან რუსულად";<br>
    "ვამუშავებ ფოტოსურათებს".</p>
      </li>
      <li><h4 class="subheading f16">შეკვეთა და მასზე მუშაობა</h4>
      <p class="subheading">დამკვეთი საიტის საშუალებით შეგიკვეთავს მომსახურებას და ამის შესახებ შეიტყობ ელფოსტის საშუალებით{if $email} ({$email}){/if}. შენსა და დამკვეთს შორის გაფორმდება ელექტრონული ხელშეკრულება, სადაც გაიწერება სამუშაოს შესრულების ვადები და სხვა პირობები.<br>
      დამკვეთს დაებლოკება გადასახდელი თანხა.<br>
      შეკვეთის ფარგლებში მიმოწერა და სამუშაოს ჩაბარების პროცესი უნდა აწარმოო საიტის საშუალებით შენი კაბინეტიდან.
      </p>
      </li>
      <li><h4 class="subheading f16">ანაზღაურების მიღება/განაღდება</h4>
      <p class="subheading">
      კუთვნილ ანაზღაურებას მიიღებ დამკვეთის მიერ სამუშაოს შედეგის მიღების შემდეგ, სამუშაოს ჩაბარებიდან შემდეგი თვის 20 რიცხვამდე.<br>
      გამომუშავებული თანხა ჩაგერიცხება <a href="https://www.emoney.ge/index.php/content/faq#4_85" rel="nofollow" target="_blank">eMoney</a>-ს ელექტრონულ საფულეზე. თანხის განაღდების მეთოდები მრავალფეროვანია და შეგიძლია იხილო <a href="https://www.emoney.ge/index.php/content/faq#7_109" rel="nofollow" target="_blank">eMoney-ს გვერდზე</a>.<br>
      დროა განათავსო შენი პირველი განცხადება!
      </p>
      </li>
    </ul>
  </div>
</div>
<div class="row mt10 tright"><a href="javascript:" class="button small secondary subheading form-reveal">პირდაპირ საქმეზე!</a><a href="javascript:" class="button small orange subheading ml20 info-tour-next" data-tour="newdeal">შემდეგი</a></div>
</div>

<form action="{$baseurl}/new" class="new-deal{if !$smarty.get.jobseeker AND !$smarty.session.USERID} hide{/if}{if !$introHidden} transparent-form{/if}" enctype="multipart/form-data" id="new-deal" method="post">
<div class="work-area row">
  <fieldset>
  <div class="row">
    <div class="large-4 small-8 columns">
      <label for="title" class="heading s08"><i class="icon-pencil"></i> სათაური *<a href="javascript:" class="right newdeal-help" data-goal="title"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box"><input class="text geo" id="title" maxlength="200" name="title" size="200" type="text" value="{$gtitle|stripslashes}" placeholder="მაგ.: დავამუშავებ ერთ ფოტოსურათს" required data-minsize="10"></div>
    </div>
    <div class="large-2 small-4 columns">
      <label for="price" class="heading s08"><span class="icon-gel">¢</span> {$lang841} *<a href="javascript:" class="right newdeal-help" data-goal="price"><i class="icon-question-sign red icon-large"></i></a></label>
      {insert name=get_member_vas_ownprice value=a assign=allow_ownprice}
      {if $allow_ownprice}
      <input class="text" id="price" maxlength="3" name="price" type="text" value="{$gprice}" placeholder="მაგ.: 100 ლარი" required data-minsize="1" pattern="[0-9]+" autocomplete="off" data-anyprice="1">
      {else}
      {insert name=get_packs value=a assign=packs}
      {insert name=get_member_level value=a assign=member_level}
      <select name="price" id="price" class="subheading" required>
        <option value="">-- აირჩიე</option>
      {section name=p loop=$packs}
        {if $packs[p].pprice eq "0" AND $smarty.session.USERID}
        <option value="{$packs[p].ID|stripslashes}"{if $gprice eq $packs[p].ID|stripslashes} selected{/if}>{$lang743}</option>
        {elseif $packs[p].pprice ne "0"}
        <option value="{$packs[p].ID|stripslashes}"{if $gprice eq $packs[p].ID|stripslashes} selected{/if}>{$packs[p].pprice|stripslashes} {$lang523}</option>{/if}
      {/section}
      </select>
      {/if}
    </div>
    <div class="large-6 small-12 columns">
      <h4 class="heading green"><i class="icon-pencil"></i> სათაური და ანაზღაურება</h4>
      <span class="subheading">შეეცადე განცხადების სათაური იყოს მიმზიდველი და მოკლედ აღწერდეს შეთავაზების შინაარს. ოპტიმალურია 40-80 სიმბოლოსაგან შემდგარი სათაური.</span>
      {if $smarty.session.USERID}<span id="price-desc" class="subheading red hide"><i class="icon-info-sign"></i> უფასო მომსახურებაზე განცხადების გამოქვეყნების საკითხი წყდება ინდივიდუალურად საიტის ადმინისტრაციის გადაწყვეტილების საფუძველზე.</span>{/if}
    </div>
  </div>

  <div class="show-for-small mt10">&nbsp;</div>

  <div class="row">
    <div class="large-6 small-12 columns">
      <label for="desc" class="heading s08"><i class="icon-edit"></i> განცხადების შინაარსი *<a href="javascript:" class="right newdeal-help" data-goal="desc"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box"><textarea class="text ta6 mb0 geo" cols="74" id="desc" maxlength="450" name="desc" rows="10" placeholder="მაგ.: დავამუშავებ ერთ ფოტოსურათს თქვენთვის სასურველ სტილში." required data-minsize="40">{$gdesc|stripslashes}</textarea></div>
      <p class="max-chars subheading s08 mb0">{$lang64}: <span class="descmax">450</span> ({$lang65}: <span class="descused">0</span>)</p>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt10 hide-for-small"></div>
      <h4 class="heading green mt10"><i class="icon-edit"></i> განცხადების შინაარსი</h4>
      <span class="subheading">{$lang70} {$lang71}</span>
    </div>  
  </div>

  <div class="show-for-small mt10">&nbsp;</div>
  <div class="hide-for-small mt5">&nbsp;</div>

  <div class="row">
    <div class="large-6 small-12 columns">
      <label for="instr" class="heading s08"><i class="icon-file-text"></i> {$lang73} *<a href="javascript:" class="right newdeal-help" data-goal="instr"><i class="icon-question-sign red icon-large"></i></a></label>
      <div class="geo-box"><textarea class="text ta6 geo" cols="74" id="instr" maxlength="450" name="instr" rows="10" placeholder="მაგ.: გთხოვთ გამომიგზავნოთ დასამუშავებელი ფოტოსურათი." required data-minsize="10">{$ginst|stripslashes}</textarea></div>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator hide-for-small"></div>
      <h4 class="heading green"><i class="icon-file-text"></i> {$lang73}</h4>
      <span class="subheading">{$lang75}</span>
    </div>  
  </div>

  <div class="show-for-small mt10">&nbsp;</div>

  <div class="row">
    <div class="large-6 small-12 columns">
      <label for="cat" class="heading s08"><i class="icon-sitemap"></i> კატეგორია *<a href="javascript:" class="right newdeal-help" data-goal="cat"><i class="icon-question-sign red icon-large"></i></a></label>
      <select class="categoryselection" id="cat" name="cat" required><option value="">{$lang67}</option>
        {insert name=get_categories assign=c}
        {section name=i loop=$c}
          <option value="{$c[i].CATID|stripslashes}" {if $gcat eq $c[i].CATID}selected="selected"{/if}>{$c[i].name|stripslashes}</option>
          {if $c[i].CATID ne "0"}
              {insert name=get_subcategories assign=scats parent=$c[i].CATID}
              {section name=j loop=$scats}
              <option value="{$scats[j].CATID}" {if $gcat eq $scats[j].CATID}selected="selected"{/if}>- {$scats[j].name|stripslashes}</option>
              {/section}
          {/if}
        {/section}
        </select>
        <label for="tags" class="heading s08"><i class="icon-tags"></i> {$lang76} *<a href="javascript:" class="right newdeal-help" data-goal="tags"><i class="icon-question-sign red icon-large"></i></a></label>
        <div class="geo-box"><textarea class="text geo" cols="74" id="tags" maxlength="100" name="tags" rows="2" placeholder="მაგ.: ფოტო, ფოტოგრაფია, ფოტოსურათის დამუშავება" required data-minsize="4">{$gtags|stripslashes}</textarea></div>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt10 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-sitemap"></i> კატეგორია და ტეგები</h4>
      <span class="subheading">{$lang68}<br>{$lang77}<br>{$lang78}</span>
    </div>
  </div>

  <div class="show-for-small mt10">&nbsp;</div>

  <div class="row">
    <div class="large-6 small-12 columns">
      <label for="duration" class="heading s08"><i class="icon-time"></i> რა დროში შეასრულებ სამუშაოს *<a href="javascript:" class="right newdeal-help" data-goal="duration"><i class="icon-question-sign red icon-large"></i></a></label>
      <input class="text mb0" id="duration" maxlength="2" name="duration" size="2" type="text" value="{$gdays|stripslashes}" pattern="[0-9]{literal}{1,2}{/literal}" placeholder="მაგ.: 2 (ორ დღეში)" required autocomplete=off data-minsize="1">
      <p class="subheading s08">მიუთითე რამდენი დღე დაგჭირდება სამუშაოს შესრულებისთვის. მაგ.: 2</p>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-time"></i> სამუშაოს შესრულების დრო</h4>
      <span class="subheading">მიუთითე შეკვეთიდან მაქსიმუმ რამდენ დღეში შეასრულებ სამუშაოს.<br>თუ შეკვეთიდან არაუგვიანეს 24 საათის განმავლობაში სრულად შეასრულებ სამუშაოს, მიუთითე "0".</span>
    </div>
  </div>

  <div class="show-for-small mt10">&nbsp;</div>

  <div class="row">
    <div class="large-6 small-12 columns photos-block">
      <span id="photo-upload-container" class="dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-iX" class="thumb-121x55"><i class="icon-picture icon-border icon-large icon-muted vmiddle show-on-delete"></i></span> <span class="browsefile button medium success subheading radius mt10 inline-block" id="upload-iX" data-id="iX"><i class="icon-upload-alt"></i> ფოტოსურათის ატვირთვა</span><a class="deletefile button alert medium radius subheading mt10 ml10 hide" id="delete-iX" data-id="iX"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-iX" name="photo-iX" data-id="iX" type="file"><input id="photobase64-iX" name="photobase64-iX" data-id="iX" type="hidden"><input id="photo-remove-iX" name="photo-remove-iX" data-id=iX value=0 type=hidden></span>
      <label for="photo-i1" class="heading s08"><i class="icon-picture"></i> ფოტოსურათი *</label>
      <span id="upload-wrapper">
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i1" class="thumb-121x55">{if $smarty.request['photobase64-i1']}<img src="{$smarty.request['photobase64-i1']}" alt="">{else}<i class="icon-picture icon-border icon-3x icon-muted vmiddle hide-for-small"></i><i class="icon-picture icon-border icon-large icon-muted vmiddle show-for-small"></i>{/if}</span> <span class="browsefile button medium success subheading radius mt10 inline-block" id="upload-i1" data-id="i1"><i class="icon-upload-alt"></i> ფოტოსურათის ატვირთვა</span><a class="deletefile button alert medium radius subheading mt10 ml10{if !$smarty.request['photobase64-i1']} hide{/if}" id="delete-i1" data-id="i1"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i1" name="photo-i1" data-id="i1" type="file"><input id="photobase64-i1" name="photobase64-i1" data-id="i1" type="hidden" value="{$smarty.request['photobase64-i1']}"></span>
      {if $smarty.request['photobase64-i2']}
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i2" class="thumb-121x55">{if $smarty.request['photobase64-i2']}<img src="{$smarty.request['photobase64-i2']}" alt="">{else}<i class="icon-picture icon-border icon-3x icon-muted vmiddle"></i>{/if}</span> <span class="browsefile button medium success subheading radius mt10 inline-block" id="upload-i2" data-id="i2"><i class="icon-upload-alt"></i> ფოტოსურათის ატვირთვა</span><a class="deletefile button alert medium radius subheading mt10 ml10{if !$smarty.request['photobase64-i2']} hide{/if}" id="delete-i2" data-id="i1"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i2" name="photo-i2" data-id="i2" type="file"><input id="photobase64-i2" name="photobase64-i2" data-id="i2" type="hidden" value="{$smarty.request['photobase64-i2']}"></span>
      {/if}
      {if $smarty.request['photobase64-i3']}
      <span class="upload-container dropzone mb10 rel"><span class="dragndrop heading hide">ფოტოსურათის ასატვირთად ჩააგდე ფაილი</span><span id="photo-thumb-i3" class="thumb-121x55">{if $smarty.request['photobase64-i3']}<img src="{$smarty.request['photobase64-i3']}" alt="">{else}<i class="icon-picture icon-border icon-3x icon-muted vmiddle"></i>{/if}</span> <span class="browsefile button medium success subheading radius mt10 inline-block" id="upload-i3" data-id="i3"><i class="icon-upload-alt"></i> ფოტოსურათის ატვირთვა</span><a class="deletefile button alert medium radius subheading mt10 ml10{if !$smarty.request['photobase64-i3']} hide{/if}" id="delete-i3" data-id="i3"><i class="icon-trash"></i></a><input class="photo-file hide" id="photo-i3" name="photo-i3" data-id="i3" type="file"><input id="photobase64-i3" name="photobase64-i3" data-id="i3" type="hidden" value="{$smarty.request['photobase64-i3']}"></span>
      {/if}
      </span>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-picture"></i> ფოტოსურათი</h4>
      <div class="subheading"><ul class="image-tip"><li>{$lang82}</li><li>{$lang83}</li><li>{$lang84}</li><li>{$lang85}</li><li>{$lang86}</li></ul></div>
    </div>
  </div>

  <div class="row hide-nd-more-params{if $gvideo OR $gmoneyback} hide{/if}">
    <div class="separator hide-for-small"></div>
    <div class="large-6 small-12 columns mt5">
      <a class="heading nd-more-params" href="javascript:"><i class="icon-gear"></i> დამატებითი პარამეტრები</a>
    </div>
    <div class="large-6 small-12 columns mt5">
      <div class="subheading">{$lang759}</div>
    </div>
  </div>

  <div class="row unhide-nd-more-params{if !($gvideo OR $gmoneyback)} hide{/if}">
    <div class="large-6 small-12 columns">
      <label for="video" class="heading s08"><i class="icon-facetime-video"></i> ვიდეო <a href="javascript:" class="right newdeal-help" data-goal="video"><i class="icon-question-sign red icon-large"></i></a></label>
      <input type="text" name="video" id="video" placeholder="https://youtube.com/watch?v=...">
      <input type="hidden" name="videohost" id="videohost" value="{$smarty.post.videohost}">
      <input type="hidden" name="videofile" id="videofile" value="{$smarty.post.videofile}">
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-facetime-video"></i> ვიდეო</h4>
      <div class="subheading"><ul class="video-tip">{$lang758}</ul></div>
    </div>
  </div>

  <div class="row unhide-nd-more-params{if !($gvideo OR $gmoneyback)} hide{/if}">
    <div class="large-6 small-12 columns">
      <label for="moneyback" class="heading s08"><i class="icon-rotate-left"></i> სამუშაოს შედეგის გარანტია *<a href="javascript:" class="right newdeal-help" data-goal="moneyback"><i class="icon-question-sign red icon-large"></i></a></label>
      {if !$smarty.session.USERID OR !$member_level OR $member_level LT 2}
      <input name="moneyback" type="hidden" value="1">
      <select name="moneyback_x" id="moneyback" class="subheading disabled" disabled>
      {else}
      <select name="moneyback" id="moneyback" class="subheading" required>
      {/if}
        <option value="1"{if !isset($gmoneyback) OR $gmoneyback ne '0'} selected{/if}>თანხის დაბრუნება გარანტირებულია</option>
        <option value="0"{if $gmoneyback eq '0'} selected{/if}>თანხა არ ექვემდებარება დაბრუნებას</option>
      </select>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator mt5 hide-for-small"></div>
      <h4 class="heading green"><i class="icon-rotate-left"></i> სამუშაოს შედეგის გარანტია</h4>
      <div class="subheading">{$lang760}
      {if !$smarty.session.USERID OR !$member_level OR $member_level LT 2}
        აღნიშნული პარამეტრის გასააქტიურებლად <a href="{$baseurl}/contact" target="_blank" rel="nofollow">დაგვიკავშირდი</a>.<br>&nbsp;
      {/if}
      </div>
    </div>
  </div>

  <div class="show-for-small mt10">&nbsp;</div>
  <input id="multipleme" name="multipleme" type="hidden" value="1">
  {*
  <div class="row">
    <div class="large-6 small-12 columns">
      <label class="heading s08"><i class="icon-gear"></i> განცხადების სხვა პარამეტრები *</label>
      <label for="multipleme" class="fs1em subheading">
        <input id="multipleme" name="multipleme" type="checkbox" value="1" class="switch2 switchYesNo" checked><span class="switch2"></span> <span class="s08">{$lang483}</span>
      </label>
    </div>
    <div class="large-6 small-12 columns">
      <div class="separator"></div>
      <h4 class="heading green mt5"><i class="icon-gear"></i> განცხადების სხვა პარამეტრები</h4>
      <span class="subheading">თუ გსურს რომ შეზღუდო ერთი მომხმარებლის მიერ შეკვეთის რაოდენობა, შეცვალე აღნიშნული პარამეტრი.</span>
    </div>
  </div>
  *}
  <div class="row mt20"><div class="separator unhide-nd-more-params hide"></div></div>
  <div class="row tcenter submit-wrapper">
    <div class="form-error-notice subheading red mt10 hide">{$lang566}</div>
    <input type="submit" value="{$lang46}" class="send-button secondary button large subheading radius mt10">
    <div class="progress-indicator-icon-message hide">
      {include file="loader.tpl"}
    </div>
  </div>
  <input type="hidden" name="subform" value="1">
  </fieldset>
</div>
</form>

{if $numposts LT 3}
<div id="deal-submitted-popup">
<ol id="DealSubmittedTour" class="joyride-list" data-joyride>
  <li data-button="OK" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade">
    <h4 class=subheading>განცხადება მიღებულია!</h4>
    <p class="subheading s08 mb0">{if $noPINEMONEY}განცხადება გაგზავნილია შესამოწმებლად. შემოწმების შედეგის შესახებ შეიტყობ ელფოსტით, რომელიც მითითებული გაქვს <a href="{$baseurl}/settings" target="_blank">პარამეტრებში</a>. შემოწმების პროცესი საჭიროებს რამდენიმე საათიდან - {$moderation_queue_period} დღემდე.
    {else}
    განცხადება გაგზავნილია და განთავსდება საიტზე შემოწმების შემდეგ. შემოწმების შედეგის შესახებ შეიტყობ ელფოსტით, რომელიც მითითებული გაქვს <a href="{$baseurl}/settings" target="_blank">პარამეტრებში</a>. შემოწმების პროცესი საჭიროებს რამდენიმე საათიდან - {$moderation_queue_period} დღემდე.{/if}</p>
  </li>
</ol>
</div>
{/if}

<div id="title-info-popup">
  <ol id="title-popup" class="joyride-list" data-joyride>
  <li data-id="title" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების სათაური</h4>
    <p class="subheading s08 mb0">განცხადების სათაური უნდა იყოს ლაკონური და გასაგებად უნდა აღწერდეს თუ რა მომსახურების გაწევას გეგმავ.<br>
    სათაურში უნდა ჩანდეს რა მოცულობის სამუშაოს შეასრულებ მოთხოვნილი ანაზღაურების სანაცვლოდ. (მაგ.: გადათარგმნი ერთ გვერდს, დაამუშავებ ორ ფოტოს და ა.შ.)<br>
    განსხვავებული მოცულობის ან მომსახურების შეთავაზებისათვის, განათავსე ცალკეული განცხადებები<br>
    მიიღება შემდეგი სახის განცხადებები:<br>
    <br>"2 ფოტოსურათის მხატვრული დამუშავება"<br>
    "ერთი გვერდის თარგმნა რუსულიდან ინგლისურად"<br>
    <u>არ მიიღება</u> ზოგადი სახის განცხადებები:<br>
    "ვთარგმნი ტექსტებს ქართულიდან რუსულად"
    "ვამუშავებ ფოტოსურათებს".
    </p>
  </li> 
  </ol> 
</div>
<div id="price-info-popup">
  <ol id="price-popup" class="joyride-list" data-joyride>
  <li data-id="price" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>ფიქსირებული ანაზღაურება</h4>
    <p class="subheading s08 mb0">განათავსე განცხადება ფიქსირებული ანაზღაურებით. ამისათვის, აღწერე რა მოცულობის სამუშაოს შესრულებას (მაგ.: გადათარგმნი ერთ გვერდს, დაამუშავებ ორ ფოტოს და ა.შ.), რაშიც ითხოვ ფიქსირებულ ანაზღაურებას.<br>
    განსხვავებული მოცულობის ან მომსახურების შეთავაზებისათვის, განათავსე ცალკეული განცხადებები.<br>
    <strong>საიტის მომსახურების საკომისიო შეადგენს მიღებული ანაზღაურების 10% (მინ.: 1 ლარს)</strong><br>
    მიიღება შემდეგი სახის განცხადებები:<br>
    <br>"2 ფოტოსურათის მხატვრული დამუშავება. 15 ლარად"<br>
    "ერთი გვერდის თარგმნა რუსულიდან ინგლისურად. 10 ლარად"<br>
    <u>არ მიიღება</u> ზოგადი სახის განცხადებები:<br>
    "ვთარგმნი ტექსტებს ქართულიდან რუსულად"<br>
    "ვამუშავებ ფოტოსურათებს"
    </p>
  </li> 
  </ol> 
</div>
<div id="desc-info-popup">
  <ol id="desc-popup" class="joyride-list" data-joyride>
  <li data-id="desc" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების შინაარსი</h4>
    <p class="subheading s08 mb0">სრულად აღწერე გასაწევი მომსახურება და შედეგი, რომელსაც მიიღებს კლიენტი.<br>მაგ.: "ფოტოშოპში დავამუშავებ 2 ფოტოსურათს:<br>- ფერების და კონტრასტის კორექცია;<br>- მცირე დეფექტების და ზედმეტი საგნების მოშორება;<br>...<br>ფოტოსურათის დაბალი ხარისხის შემთხვევაში, შეკვეთამდე წინასწარ დამიკავშირდით.<br>მაქვს ფოტოდამუშავების 5 წლიანი გამოცდილება."
    </p>
  </li> 
  </ol> 
</div>
<div id="instr-info-popup">
  <ol id="instr-popup" class="joyride-list" data-joyride>
  <li data-id="instr" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>შეთავაზების მიღების ინსტრუქცია</h4>
    <p class="subheading s08 mb0">აღწერე თუ რა სახით უნდა მოგაწოდოს დამკვეთმა მომსახურების გასაწევად აუცილებელი საწყისი ინფორმაცია ან მასალა.<br>
    ინფორმაციის მოწოდებისათვის დამკვეთმა უნდა გამოიყენოს არსებული ვებ-გვერდი. ელფოსტით ან სხვა საშუალებით მასალის გამოგზავნის მოთხოვნა დაშვებულია მხოლოდ იმ შემთხვევაში, თუ გამოსაგზავნი მონაცემები აღემატება 10 მბ-ს.<br>
    მაგ.: "მომაწოდეთ გადასათარგმნი ტექსტი ვორდის ფაილის სახით. სასურველია ფაილი იყოს MS Word 2013 ფორმატში."
    </p>
  </li> 
  </ol> 
</div>
<div id="cat-info-popup">
  <ol id="cat-popup" class="joyride-list" data-joyride>
  <li data-id="cat" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>განცხადების კატეგორია</h4>
    <p class="subheading s08 mb0">მნიშვნელოვანია განცხადების სწორი კატეგორიის შერჩევა. აირჩიე გასაწევი მომსახურებისათვის აზრობრივად ყველაზე ახლო მდგომი კატეგორია.
    </p>
  </li> 
  </ol> 
</div>
<div id="tags-info-popup">
  <ol id="tags-popup" class="joyride-list" data-joyride>
  <li data-id="tags" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>მინიშნებები</h4>
    <p class="subheading s08 mb0">იმისათვის, რომ გაუადვილო დამკვეთს შენი შეთავაზების პოვნა, განსაზღვრე და ჩაწერე განცხადების შინაარსთან ყველაზე მჭიდროდ დაკავშირებული რამდენიმე საკვანძო სიტყვა.<br>მაგ.: "სცენარი, სარეკლამო რგოლი".
    </p>
  </li> 
  </ol> 
</div>
<div id="duration-info-popup">
  <ol id="duration-popup" class="joyride-list" data-joyride>
  <li data-id="duration" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>რა დროში შეასრულებ სამუშაოს</h4>
    <p class="subheading s08 mb0">სამუშაოს დროული ჩაბარება ძალიან მნიშვნელოვანია როგორც რეპუტაციისათვის, ასევე შესრულებული სამუშაოს ანაზღაურებისათვის.<br>
    მიუთითე მაქსიმალური, მაგრამ გონივრული ვადა დღეების რაოდენობის სახით. შეკვეთის გაფორმებიდან, არაუგვიანეს მითითებული ვადისა უნდა ჩააბარო სამუშაო დამკვეთს, წინააღმდეგ შემთხვევაში შეკვეთა გაუქმდება, თანხა დაუბრუნდება დამკვეთს, ხოლო შენი რეიტინგი დაიკლებს.<br>
    მაგ.: "5" (დღე)
    </p>
  </li> 
  </ol> 
</div>

<div id="newdeal-info-popup">
<ol id="newDealTour" class="joyride-list" data-joyride>
  <li data-id="place-deal-header" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>განცხადების განთავსება <u class="red">უფასოა</u>!</h4>
    <p class="subheading s08 mb0">ყველა განცხადება გადის შემოწმებას და საიტზე აისახება {$moderation_queue_period} დღის განმავლობაში.<br>დეტალური ინფორმაციის მისაღებად თუ როგორი განცხადებების განთავსებაა დაშვებული, <a href="{$baseurl}/faq?p=seller&amp;t=dealtypes">დააჭირე აქ</a>.</p>
  </li>
  <li data-id="title" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>განცხადების სათაური და ანაზღაურება</h4>
    <p class="subheading s08 mb0">განცხადებაში ზუსტად უნდა ჩანდეს თუ რას სთავაზობ მომხმარებელს კონკრეტულ ფასად.<br>მაგ.: "2 ფოტოსურათის მხატვრული დამუშავება 15 ლარად". გაითვალისწინე, რომ საიტის საკომისიო შეადგენს გამომუშავებული თანხის 10% (მინ.: 1 ლარს)</p>
  </li>
  <li data-id="desc" data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>განცხადების შინაარსი</h4>
    <p class="subheading s08 mb0">სრულად აღწერე გასაწევი მომსახურება და შედეგი, რომელსაც მიიღებს კლიენტი.<br>მაგ.: "ფოტოშოპში დავამუშავებ 2 ფოტოსურათს:<br>- ფერების და კონტრასტის კორექცია;<br>- მცირე დეფექტების და ზედმეტი საგნების მოშორება;<br>...<br>ფოტოსურათის დაბალი ხარისხის შემთხვევაში, შეკვეთამდე წინასწარ დამიკავშირდით.<br>მაქვს ფოტოდამუშავების 5 წლიანი გამოცდილება."</p>
  </li>
  <li data-id="title" data-button="OK" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade">
    <h4 class=subheading>დაიწყე განცხადების შევსება</h4>
    <p class="subheading s08">განცხადების ფორმის შევსებისას ყურადღება მიაქციე ფორმაში მოყვანილ მითითებებს.<br>მეტი ინფორმაციის მისაღებად <a href="{$baseurl}/faq?p=seller&amp;t=dealtypes">დააჭირე აქ</a>.<br>გისურვებთ წარმატებას!</p>
  </li>
</ol>
</div>

<div id="video-info-popup">
  <ol id="video-popup" class="joyride-list" data-joyride>
  <li data-id="video" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>ვიდეო-რგოლი</h4>
    <p class="subheading s08 mb0">ვიდეო-რგოლი უფრო მიმზიდველს და სახალისოს გახდის შენს განცხადებას. მისი საშუალებით შეგიძლია:</p>
    <ul class="subheading ml20">
      <li>გააცნო საკუთარი თავი და მოუყვე დამკვეთს შენი მომსახურების შესახებ;</li>
      <li>ვიდეო-კოლაჟის სახით წარმოაჩინო საკუთარი პორტფოლიო.</li>
    </ul>
    <p class="subheading s08">
    წინასწარ ატვირთე შენი ვიდეო-რგოლი <a href="https://www.youtube.com/upload" target="_blank" rel="nofollow">YouTube.com</a>-ზე, ან <a href="http://www.myvideo.ge/c/uploadnew" rel="nofollow" target="_blank">MyVideo.ge</a>-ზე და მიუთითე ბმული აღნიშნულ ველში.</p>
  </li>
  </ol>
</div>

<div id="moneyback-info-popup">
  <ol id="moneyback-popup" class="joyride-list" data-joyride>
  <li data-id="moneyback" data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>სამუშაოს შედეგის გარანტია</h4>
    <span class="subheading s08">თუ დამკვეთი უკმაყოფილოა მიღებული შედეგით, მას უფლება აქვს მოითხოვოს თანხის დაბრუნება. სამუშაოს შედეგის და შესაბამისად, თანხის დაბრუნების გარანტია ძალიან მნიშვნელოვანია დისტანციური საქმიანობის პოპულარიზაციისათვის. შეცვალე ეს პარამეტრი მხოლოდ იმ შემთხვევაში, თუ:</span>
    <ol class="subheading s08 p10 mb0">
    <li>შენს მიერ შესრულებული სამუშაო სპეციფიურია და მისი ჩაბარების შემდეგ თანხას ვერ დაუბრუნებ დამკვეთს <strong>და</strong></li><li>სამუშაოს წინასწარი ან ნაწილობრივი ჩაბარება შენს შემთხვევაში შეუძლებელია <strong>და</strong></li><li>გააზრებული გაქვს, რომ ასეთ მომსახურებას დამკვეთი შეეცდება მოერიდოს.</li></ol>
  </li>
  </ol>
</div>

<script>
  //var startNewDealTour = {if $numofposts<3 AND !$smarty.post.subform}true{else}false{/if};
  var startNewDealTour = false;
  var dealSubmittedTour = {if $numofposts<3 AND $smarty.request.subform AND !$error}true{else}false{/if};
  var lang = [];
  lang['change'] = '{$lang563}';
  lang['notimage'] = '{$lang564}';
  lang['minsize'] = '{$lang565}';
  lang['minprice'] = '{$lang838}';
  lang['maxprice'] = '{$lang839}';
  lang['onlyinteger'] = '{$lang840}';
  lang['videohost'] = '{$lang757}';
  var def_js = ['{$baseurl}/js/upload.js','{$baseurl}/js/deal.js?v=17','{$baseurl}/js/submitprocessor.js?v=7'{*,'{$baseurl}/js/jquery.vide.min.js?v=014'*}];
</script>