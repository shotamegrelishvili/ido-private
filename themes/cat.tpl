<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/categories/{$cid}" class=current><i class="icon-sitemap"></i> {$cname}</a>
  </ul>
</div>

{if $message ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">
    <ul class=subheading>
    {$message}
    </ul>
  </div>
</div>
{/if}

<div class="rel">
    <div class="cat-central-search hide-for-small b30">
        <form action="{$baseurl}/search" id="catsearch" method="get" class="mb0">
        <div class="row collapse">
            <div class="large-4 columns">&nbsp;</div>
            <div class="large-8 columns"><label for="catquery" class="subheading s08">ძიება კატეგორიაში {$cname}:</label></div>
        </div>
        <div class="row collapse">
            <div class="large-4 columns">&nbsp;</div>
            <div class="large-3 columns">
            <input type="text" name="query" id="catquery" placeholder="{$lang524}" class="geo mb0">
            </div>
            <div class="large-1 columns">
            <a href="javascript:document.forms.catsearch.submit();" class="button success postfix subheading mb0"><i class="icon-search"></i></a>
            </div>
            <div class="large-4 columns"></div>
        </div>
        <input name="search_in" type="hidden" value="category">
        <input name="c" type="hidden" value="{$CATID}">
        </form>
    </div>
    <div class="cat-central-text">
        <div class="row collapse">
            <div class="large-12 small-12 columns">
            <h1 class="heading mb0 f25">{$cname}</h1>
            <h2 class="subheading f16">{$mdetails}</h2>
            </div>
        </div>
    </div>
    <div class="cat-central">
        <img src="{$baseurl}/images/cat{$catrand}.jpg" alt="">
    </div>
</div>

<div class="row filters mt20">
    <ul class="large-12 small-12 subheading columns">
        <li><strong class="heading">{$lang109}</strong></li>
        {if $s eq "d" OR $s eq "dz" OR $s eq ""}
        <li><a href="{$baseurl}/categories/{$cid}?s=d{if $s eq "d" OR $s eq ""}z{/if}" class="secondary radius label">{$lang110}</a></li>
        {else}
        <li><a href="{$baseurl}/categories/{$cid}?s=d">{$lang110}</a></li>
        {/if}
        {if $s eq "p" OR $s eq "pz"}
        <li><a href="{$baseurl}/categories/{$cid}?s=p{if $s eq "p"}z{/if}" class="secondary radius label">{$lang111}</a></li>
        {else}
        <li><a href="{$baseurl}/categories/{$cid}?s=p">{$lang111}</a></li>
        {/if}
        {if $s eq "r" OR $s eq "rz"}
        <li><a href="{$baseurl}/categories/{$cid}?s=r{if $s eq "r"}z{/if}" class="secondary radius label">{$lang112}</a></li>
        {else}
        <li><a href="{$baseurl}/categories/{$cid}?s=r">{$lang112}</a></li>
        {/if}
        {if $s eq "c" OR $s eq "cz"}
        <li><a href="{$baseurl}/categories/{$cid}?s=c{if $s eq "c"}z{/if}" class="secondary radius label">{$lang436}</a></li>
        {else}
        <li><a href="{$baseurl}/categories/{$cid}?s=c">{$lang436}</a></li>
        {/if}
        {if $s eq "e" OR $s eq "ez"}
        <li><a href="{$baseurl}/categories/{$cid}?s=e{if $s eq "e"}z{/if}" class="secondary radius label">{$lang494}</a></li>
        {else}
        <li><a href="{$baseurl}/categories/{$cid}?s=e">{$lang494}</a></li>
        {/if}
        <li><a href="{$baseurl}/rss?c={$catselect}" class="ml5 button tiny alert radius hide-for-medium-down"><i class="icon-rss icon-large"></i> RSS</a></li>
    </ul>
</div>
<div class="row show-for-medium-down">
    <div class="small-6 columns tcenter"><a href="javascript:" class="button small radius secondary heading cats-btn"><i class="icon-sitemap cat-btn-toggle"></i><i class="icon-collapse-alt cat-btn-toggle hide"></i>&nbsp;{$lang527}</a></div>
    <div class="small-6 columns tcenter"><a href="{$baseurl}/rss?c={$catselect}" class="button small radius alert heading"><i class="icon-rss"></i> RSS არხი</a></div>
</div>

{if $smarty.session.ADMINID AND (!isset($smarty.get.god_mode) OR $smarty.get.god_mode eq "1")}
<div class="row">
    <div class="large-12 columns tright"><a href="{$baseurl}/categories/{$cid}?god_mode=0" class="button empty large heading">ახალი განცხადებები <input name="godmode" type="checkbox" value="1" class="switch2 switchYesNo" checked><span class="switch2"></span></a>
    </div>
</div>
{elseif $smarty.session.ADMINID AND $smarty.get.god_mode eq "0"}
<div class="row">
    <div class="large-12 columns tright"><a href="{$baseurl}/categories/{$cid}?god_mode=1" class="button empty large heading">ახალი განცხადებები <input name="godmode" type="checkbox" value="0" class="switch2 switchYesNo"><span class="switch2"></span></a></div>
</div>
{/if}
<div class="work-area-wrapper">
{include file="bit.tpl"}
</div>

{if $posts}
<div class="row mt20">
  <div class="large-12 tcenter columns">
    <a href="javascript:" class="button large secondary subheading radius" id="load-more" data-order="{$smarty.get.s}" data-start="{$ending}" data-type="category" data-catid="{$catselect}">{$lang532}</a>
  </div>
</div>
{else}
<div class="row mt20">
    <div class="large-12 tcenter columns">
        <span class="subheading">ამ მომენტისთვის არცერთი აქტიური განცხადება კატეგორიაში არ არის. (საიტი ახალი გაშვებულია)</span>
        <a href="{$baseurl}/new" class="button success large radius heading mt10">იყავი პირველი, დაამატე ახალი განცხადება!</a>
    </div>
</div>
{/if}
<div class="separator2 mt15"></div>

<div class="row mt20">
    <h3 class="heading">{$cname}</h3>
    <span class="subheading block lh16">{$mdesc}</span>
</div>

{if sizeof($tags)>1}
<div class="crumbs mt20">
    <div class="row mb20">
    <h3 class="heading mt20">ტეგების ღრუბელი</h3>
{section name=i loop=$tags max=10}
{if $tags[i] != ""}<a href="{$baseurl}/search?query={$tags[i]|stripslashes}&amp;search_in=category&amp;c={$CATID}&amp;filter=tags" class="secondary radius label ml10 subheading mb10" title="{$tags[i]|stripslashes}">&nbsp;{$tags[i]|stripslashes}&nbsp;</a>{/if}
{/section}
    </div>
</div>
{/if}

<script>
    var page = 'cat{$cid}';
</script>

{*
              <div class="main-wrapper">
                <div id="main">
                  <div class="content">
                  {include file="error.tpl"}
                    {include file="cat_bit.tpl"}
					<div class="darkenBackground"></div>
                    
                    

                    <div class="featured">   
                    	<div class="gig_filters bordertop">
                          <div class="ul bg-f-a">
                            <div class="li"><span class="helptext">{$lang109}</span></div>
                            	{if $s eq "d" OR $s eq ""}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=dz" class="current">{$lang110}</a></div>
                                {else}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=d" {if $s eq "d" OR $s eq "dz" OR $s eq ""}class="current"{/if}>{$lang110}</a></div>
                                {/if}
                                {if $s eq "p"}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=pz" class="current">{$lang111}</a></div>
                                {else}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=p" {if $s eq "p" OR $s eq "pz"}class="current"{/if}>{$lang111}</a></div>
                                {/if}
                                {if $s eq "r"}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=rz" class="current">{$lang112}</a></div>
                                {else}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=r" {if $s eq "r" OR $s eq "rz"}class="current"{/if}>{$lang112}</a></div>
                                {/if}
                                {if $s eq "c"}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=cz" class="current">{$lang436}</a></div>
                                {else}
                                <div class="li sep-right"><a href="{$baseurl}/categories/{$cid}?s=c" {if $s eq "c" OR $s eq "cz"}class="current"{/if}>{$lang436}</a></div>
                                {/if}
                                
                                <div class="li sep-right">
                                {if $price_mode eq "3"}
                                <script language="JavaScript" type="text/JavaScript"> 
								function jumpMenu(targ,selObj,restore){
								  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'"); 
								  if (restore) selObj.selectedIndex=0; 
								} 
								</script> 
                                {insert name=get_packs value=a assign=packs}
                                <select onChange="jumpMenu('parent',this,0)">
                                <option value="{$baseurl}">{$lang495}</option>
                                {section name=p loop=$packs}
                                <option value="{$baseurl}/categories/{$cid}?s=o&p={$packs[p].pprice|stripslashes}" {if $p eq $packs[p].pprice|stripslashes}selected="selected"{/if}>{$lang197}{$packs[p].pprice|stripslashes}</option>
                                {/section}
                                </select>
                                {/if}
                                </div>
                                
                                {if $s eq "e"}
                                <div class="li last"><a href="{$baseurl}/categories/{$cid}?s=ez" class="current">{$lang494}</a></div>
                                {else}
                                <div class="li last"><a href="{$baseurl}/categories/{$cid}?s=e" {if $s eq "e" OR $s eq "ez"}class="current"{/if}>{$lang494}</a></div>
                                {/if}
                          </div>
                        </div>                
                        {include file="bit.tpl"}
                    </div>
                    
  					<div class="paging">
                    	<div class="p1">
                        	<ul>
                            	{$pagelinks}
                            </ul>
                        </div>
                    </div>
					<div class="rss-link"><a href="{$baseurl}/rss?c={$catselect}">{$lang108}</a></div>
                  </div>
                  {include file="side.tpl"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      *}