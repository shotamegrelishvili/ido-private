<link href="{$baseurl}/css/slickslider.css" media="screen" rel="stylesheet">

{insert name=seo_clean_titles assign=title value=a title=$p.gtitle}
<div class=crumbs>
	<ul class="row collapse heading">
	  <li><a href="{$baseurl}/">{$lang0}</a>
	  <li><a href="{$baseurl}/categories/{$p.seo|stripslashes}" class=current>{$p.name|stripslashes}</a>
	</ul>
</div>

<div class="row mt20">
	<!--Left Part-->
	<div class="large-8 small-12 columns">
		<div class="row">
			<div class="large-11 small-12 columns">
				<h1 class="heading h-large">{$p.gtitle|stripslashes}{* {if $p.price ne "0"}{$p.price|stripslashes} <span class="subheading">{$lang63}</span>{else}{$lang743}{/if}*}</h1>
				<div class="row">
					<div class="mr15">
				{if $smarty.session.USERID GT "0"}
				{insert name=like_cnt value=var assign=liked pid=$p.PID}
					<span class="{if $liked ne "1"} hide{/if}"><a id="removebookmark" href="{$baseurl}/bookmark?id={$p.PID|stripslashes}&amp;do=rem" class="removebookmark button secondary small right subheading like active ml10"><i class="icon-heart-empty"></i> {$lang148}</a></span>
					<span class="{if $liked eq "1"} hide{/if}"><a id="addbookmark" href="{$baseurl}/bookmark?id={$p.PID|stripslashes}&amp;do=add" class="addbookmark button secondary small right subheading like ml10"><i class="icon-heart red"></i> {$lang147}</a></span>
					{/if}
					<a id="fbsharer" href="javascript:fbShare('{$baseurl}/{$p.seo|stripslashes}/{$p.PID}/{$title|htmlentities}','{$p.gtitle|stripslashes|htmlentities}','{$p.gdesc|strip_tags|htmlentities|stripslashes|replace:"\n":' '|replace:"\r":''}', '{$purl}/t2/{$p.p1}', 600, 400)" class="button small subheading right ml10"><i class="icon-facebook prefix-own"></i> {$lang539}</a>
					</div>
				</div>
				{*<span class="left subheading">{assign var=days value=floor(($smarty.now-$p.time_added)/86400)}{$days}</span>*}
				<!--Images-->
				<div class="slideshow-wrapper">
				{*include file='view_image_box.tpl*}
					<div{if $p.p2 || $p.p3 || $p.video} class="slick-slider single-item"{/if}>
						<div><img alt="{$posts[i].gtitle|stripslashes|escape:'html'}" src="{$purl}/t/{$p.p1}"></div>
					{if $p.p2 ne ""}<div><img alt="" src="{$purl}/t/{$p.p2}"></div>{/if}
					{if $p.p3 ne ""}<div><img alt="" src="{$purl}/t/{$p.p3}"></div>{/if}
					{if $p.video ne ""}<div><iframe width="580" height="326" src="{$p.video}" frameborder="0" allowfullscreen></iframe></div>{/if}
					</div>
				</div>
				{if $p.youtube ne ""}{include file='view_yt.tpl'}{/if}
				<!--Images end-->
				<div class="view-page-deal-desc">{$p.gdesc|stripslashes|replace:"\n":"<br>"}</div>
			</div>
		</div>
	</div>
	<!--Left Part end-->
	<!--Right Part-->
	<div class="large-4 small-12 columns view-page-right">
		{include file='view_order_bit.tpl'}
		{include file='view_order_multi.tpl'}
		{if $p.feat eq 1}
			<span class="block heading view-deal-badge"><i class="icon-star icon-large red"></i> {$lang543}</span>
		{/if}
		{if $p.days == "0"}<span class="block heading view-deal-badge"><i class="icon-rocket icon-large green"></i> {$lang530}</span>{elseif $p.days == "1"}<span class="is-express block heading view-deal-badge"><i class="icon-time icon-large blue"></i> {$lang492}</span>{/if}
		{if $p.rcount}
		<div class="tcenter ratings mt5">
			{for $s=1 to 5}{if $p.rating >= $s*20}<i class="icon-star icon-large"></i>{elseif $p.rating > (($s-1)*20)+5}<i class="icon-star-half-full icon-large"></i>{else}<i class="icon-star-empty icon-large"></i>{/if}{/for}
		</div>
		<div class="separator mt5"></div>
		<div class="row collapse">
			<div class="large-7 small-6 columns mt10 tcenter">
				<span class="big-number block gray">{$p.rating}%</span>
				<span class="subheading s08">პოზიტიური შეფასება</span>
			</div>
			<div class="large-5 small-6 columns mt10 tcenter">
				<span class="big-number block gray">{$p.rcount}</span>
				<span class="subheading s08">შემფასებელი</span>
			</div>
		</div>
		{else}
			<div class="gray subheading tcenter mt5">ჯერ არ შეუფასებიათ</div>
		{/if}
		<div class="separator mt10"></div>
		<div class="row collapse">
			<div class="large-3 small-3 columns tright mt10"><i class="icon-time icon-3x gray"></i></div>
			<div class="large-3 small-3 columns tcenter mt10"><span class="big-number">{$p.days}</span></div>
			<div class="large-6 small-6 columns mt15"><span class="subheading s08">{$lang474}</span></div>
		</div>
		<div class="separator mt10"></div>
		<div class="row collapse">
			<div class="large-3 small-3 columns tright mt10"><i class="icon-tasks icon-3x gray"></i></div>
			<div class="large-3 small-3 columns tcenter mt10"><span class="big-number">{$quecount}</span></div>
			<div class="large-6 small-6 columns mt15"><span class="subheading s08">{$lang473}</span></div>
		</div>
		<div class="separator mt10"></div>
		{insert name=get_post_salesnumber assign=salescount value=var PID=$p.PID}
		{if $salescount}
		<div class="row collapse">
			<div class="large-3 small-3 columns tright mt10"><i class="icon-credit-card icon-3x gray"></i></div>
			<div class="large-3 small-3 columns tcenter mt10"><span class="big-number">{$salescount}</span></div>
			<div class="large-6 small-6 columns mt15"><span class="subheading s08">დამკვეთმა შეიძინა მომსახურება</span></div>
		</div>
		<div class="separator mt10"></div>
		{/if}
		<div class="row collapse">
			<div class="large-3 small-3 columns tright mt10">
			{if !$p.moneyback}
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" class="gray">
			<path d="M12 1l-9 4v6c0 5.55 3.84 10.74 9 12 5.16-1.26 9-6.45 9-12v-6l-9-4zm0 10.99h7c-.53 4.12-3.28 7.79-7 8.94v-8.93h-7v-5.7l7-3.11v8.8z"/>
			<path d="M0 0h24v24h-24z" fill="none"/>
			</svg>
			{else}
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" class="green">
			<path d="M0 0h24v24h-24z" fill="none"/>
			<path d="M12 1l-9 4v6c0 5.55 3.84 10.74 9 12 5.16-1.26 9-6.45 9-12v-6l-9-4zm-2 16l-4-4 1.41-1.41 2.59 2.58 6.59-6.59 1.41 1.42-8 8z"/>
			</svg>
			{/if}
			</div>
			<div class="large-3 small-3 columns tcenter mt10">&nbsp;</div>
			<div class="large-6 small-6 columns mt15"><strong class="subheading s08">{if $p.moneyback}<span class="has-tip" data-tooltip title="სამუშაოს შედეგით უკმაყოფილო ხარ?<br>დაიბრუნე გადახდილი თანხის 100%!">სამუშაოს შედეგი გარანტირებულია</span>{else}<span class="has-tip" data-width="300" data-tooltip title="გადახდილი თანხის 100% დაბრუნება შეგიძლია შემსრულებლის მიერ სამუშაოს ჩაბარებამდე.">იხილე გადახდილი თანხის დაბრუნების წესი</span>{/if}</strong></div>
		</div>
		<div class="separator mt10"></div>
		<!--User Bar-->
		<div class="view-page-user-bar mt50">
			<div class="row collapse">
				<div class="large-3 small-3 columns tright">
					{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$p.USERID}
					{if $profilepicture}<img src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width="50" height="50" class="user-avatar-small" alt="{$p.username|stripslashes}">
					{else}
					<i class="icon-user icon-3x gray"></i>
					{/if}
				</div>
				<div class="large-1 small-1 columns"></div>
				<div class="large-8 small-8 columns">{insert name=get_seo_profile value=a username=$p.username|stripslashes assign=profileurl}
					<a href="{$baseurl}/user/{$p.username|stripslashes}">{$p.username|stripslashes}</a>
					{insert name=get_percent value=a assign=mpercent userid=$p.USERID}
					{if $mpercent}
					<div class="user-ratings mt3">
					{for $s=1 to 5}{if $mpercent >= $s*20}<i class="icon-star"></i>{elseif $mpercent > (($s-1)*20)+5}<i class="icon-star-half-full"></i>{else}<i class="icon-star-empty"></i>{/if}{/for}
					</div>
					{/if}

					<div class="separator mt5"></div>
					<span class="subheading s08 mt3 block">რეგისტრაცია: {insert name=get_past_period assign=past_period value=var t=$p.addtime}<strong>{$past_period}</strong></span>
					{if $p.location}<span class="subheading s08 block">ქალაქი/დასახლება: <strong>{$p.location}</strong></span>{/if}
					<a href="{$baseurl}/conversations/{$p.username|stripslashes}" class="button secondary tiny radius heading expand mt5"><i class="icon-envelope-alt"></i> დაკავშირება</a>
				</div>
			</div>
		</div>
		<!--User Bar end-->
	</div>
	<!--Right Part end-->
</div>

{*<div class="separator2 mt15"></div>*}
<div class="row hide-for-small">
	<div class="ad-720 mt10">
		<a href="{$baseurl}/contact?contactcase=3" class="ad-txt">რეკლამა</a>
		<object width="720" height="90">
			<param name="movie" value="{$baseurl}/banners/jobrate.swf">
			<param name="quality" value="high">
			<param name="wmode" value="transparent">
			<param name="allowScriptAccess" value="always">
			<embed src="{$baseurl}/banners/jobrate.swf" quality="high" type="application/x-shockwave-flash" wmode="transparent" width="720" height="90" pluginspage="http://www.macromedia.com/go/getflashplayer">
		</object>	
	</div>
</div>
{if $smarty.session.ADMINID}
{if $moderator_block_changes}
<div class="row mt20">
	<div class="large-12 columns subheading red"><strong>{$lang754}</strong></div>
</div>
{else}
{include file='view_moderator_panel.tpl'}
{/if}
{/if}
<!--Customer Reviews-->
<div class="row">
{if $f|@sizeof GT 0}
	<div class="large-9 small-8 columns heading gray comments">
		<div class="row">
			<div class="large-5 small-7 columns heading h-large mt10">
				დამკვეთის აზრი
			</div>{insert name=get_rating value=a assign=percent b=$brat g=$grat}
			<div class="large-7 small-5 columns tcenter mt15">
				<span class="inline light-gray medium-number">{$grat}</span> <i class="icon-thumbs-up-alt icon-2x"></i> <span class="inline light-gray medium-number ml10">{$brat}</span> <i class="icon-thumbs-down-alt icon-2x"></i>
			</div>
		</div>
	{section name=i loop=$f}
		<div class="separator3{if !$smarty.section.i.index} mt10{/if}"></div>
		<div class="row">
			<div class="large-1 small-2 columns mt10">
				<i class="icon-thumbs-{if $f[i].good eq "1"}up-alt{else}down-alt{/if} icon-2x ml10"></i>
			</div>
			<div class="large-2 small-2 columns tcenter mt10">
				{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$f[i].USERID}
				{if $profilepicture}<img src="{$membersprofilepicurl}/thumbs/{$profilepicture}" width="50" height="50" class="user-avatar-small" alt="{$p.username|stripslashes}">
				{else}
				<i class="icon-user icon-2x gray"></i>
				{/if}
			</div>
			<div class="large-9 small-8 columns mt10">
				<a href="{$baseurl}/{insert name=get_seo_profile value=a username=$f[i].username|stripslashes}" class=s08>{$f[i].username|stripslashes}</a>
				<p class="subheading">{$f[i].comment|stripslashes}</p>
			</div>
		</div>
	{/section}
	</div>
	<div class="large-3 small-4 columns tright mt15">
		{if !$smarty.session.ADMINID AND $smarty.session.USERID GT "0" AND $smarty.session.USERID ne $p.USERID}
		<a onclick="document.ordermulti.submit();" href="#" class="order-now-g button radius success small subheading">{$lang140} {$p.price} <span class="subheading">{$lang63}</span></a>
		{elseif !$smarty.session.ADMINID AND !$smarty.session.USERID}
		<a href="{$baseurl}/signin?r={insert name=get_redirect value=a assign=rurl PID=$p.PID seo=$p.seo gtitle=$title}{$rurl|stripslashes}" class="login-link order-now-g button radius success small heading">{$lang140} {$p.price} <span class="subheading">{$lang63}</span></a>
		{/if}
	</div>
{else}
	<div class="large-12 small-12 columns heading mt15 mb10">დამკვეთს ჯერ არ დაუფიქსირებია საკუთარი აზრი</div>
{/if}
</div>
<!--Customer Reviews End-->
{if $u}
<div class="separator2 mt5"></div>
<!--Other deals-->
<div class="row other-deals">
	<div class="mt20">
		<h3 class="heading"><strong><a href="{$baseurl}/{insert name=get_seo_profile value=a username=$p.username|stripslashes}">{$p.username|stripslashes}</a></strong>{$lang137}</h3>
	</div>
</div>
{$posts = $u}{$gig_view = 1}
<div class="work-area-wrapper">
{include file="bit.tpl"}
</div>
<!--Other deals End-->
{/if}

<script>
	function fbShare(url, title, descr, image, winWidth, winHeight) {
	var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
	window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + encodeURIComponent(title) + '&p[summary]=' + encodeURIComponent(descr) + '&p[url]=' + encodeURIComponent(url) + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	}
	var page = 'view';
	{if $smarty.session.ADMINID}
	def_js.push('{$baseurl}/js/view.js');
	{else}
	var def_js = ['{$baseurl}/js/view.js'];
	{/if}
</script>
{*
<!--Related Deals-->
<div class="related-gigs">
	<h3>{$lang136}</h3>
	<ul>
		{section name=i loop=$r}
		{insert name=seo_clean_titles assign=rtitle value=a title=$r[i].gtitle}
		<li class="other-gig-box">
			<a href="{$baseurl}/{$r[i].seo|stripslashes}/{$r[i].PID|stripslashes}/{$rtitle}"><img alt="{$r[i].gtitle|stripslashes}" src="{$purl}/t2/{$r[i].p1}" /></a>
			<div>
			<p><a href="{$baseurl}/{$r[i].seo|stripslashes}/{$r[i].PID|stripslashes}/{$rtitle}">{$lang62} {$r[i].gtitle|stripslashes} <span class="subheading">{$lang63}</span>{$r[i].price|stripslashes}</a></p>
			<p class="category-label"><a href="{$baseurl}/categories/{$r[i].seo|stripslashes}">{$r[i].name|stripslashes}</a></p>
			</div>
		</li>
		{/section}
	</ul>
</div>
<!--Related Deals End-->
*}