<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/search" class=current><i class="icon-search"></i> {$lang129}</a>
  </ul>
</div>

{if $message ne ""}
<div class="msg-error row red">
  <div class="large-12 small-12 columns">
    <ul class=subheading>
    {$message}
    </ul>
  </div>
</div>
{/if}

<div class="rel">
    <div class="cat-central-search search-central hide-for-small t70">
    <form action="{$baseurl}/search" id="catsearch" method="get" class="mb0">
        <div class="row collapse">
            <div class="large-12 small-12 columns search-result-title tright">
                <h1 class="heading f25 mb0">{if $tag}ძიების შედეგი{else}ძიება{/if}</h1>
                <!--h2 class="subheading f16">საძიებო ფრაზა: "<strong>{$smarty.request.query}</strong>"</h2-->
                <h2 class="subheading block mt20">{if $total}ნაპოვნია: {$total} განცხადება{else}ძიება დასრულდა უშედეგოდ :({/if}</h2>
            </div>
        </div>
        <div class="row collapse">
            <div class="large-7 columns">&nbsp;</div>
            <div class="large-4 columns zoom2">
            <input type="text" name="query" id="catquery" placeholder="საძიებო ფრაზა" value="{$tag|stripslashes}" class="geo subheading mb0">
            </div>
            <div class="large-1 columns zoom2">
            <a href="javascript:document.forms.catsearch.submit();" class="button success postfix subheading mb0"><i class="icon-search"></i></a>
            </div>
        </div>
        <div class="row collapse mt10">
            <div class="large-7 columns">&nbsp;</div>
            <div class="large-5 columns">
                <select id="search_in" name="search_in" class="subheading">
                    <option value="all">ვეძებოთ {$lang505}</option>
                    <optgroup label="&nbsp;&nbsp;ვეძებოთ კატეგორიაში:"></optgroup>
                    {insert name=get_categories assign=cats}
                    {section name=i loop=$cats}
                        <option value="{$cats[i].CATID}"{if $c==$cats[i].CATID} selected{/if}>{$cats[i].name|stripslashes}</option>
                    {/section}
                </select>
            </div>
        </div>
    <input name="c" id="catfield" type="hidden" value="{$c|stripslashes}">
    </form>
    </div>
    <div class="cat-central">
        <img src="{$baseurl}/images/searchbk.jpg" alt="iDo.ge - ფრილანსის პლატფორმა საქართველოში - იპოვე მომსახურება ან საჭირო სპეციალისტი და შეუკვეთე მას სამუშაო">
    </div>
</div>

<div class="row filters mt20">
    <div class="large-2 small-4 columns">
        <a href="javascript:" class="button small radius secondary heading cats-btn"><i class="icon-sitemap cat-btn-toggle"></i><i class="icon-collapse-alt cat-btn-toggle hide"></i>&nbsp;{$lang527}</a>
    </div>
    <ul class="large-10 small-8 subheading columns">
        <li><strong class="heading">{$lang109}</strong></li>
        {if $s eq "d" OR $s eq "dz" OR $s eq ""}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=d{if $s eq "d" OR $s eq ""}z{/if}" class="secondary radius label">{$lang110}</a></li>
        {else}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=d">{$lang110}</a></li>
        {/if}
        {if $s eq "p" OR $s eq "pz"}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=p{if $s eq "p"}z{/if}" class="secondary radius label">{$lang111}</a></li>
        {else}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=p">{$lang111}</a></li>
        {/if}
        {if $s eq "r" OR $s eq "rz"}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=r{if $s eq "r"}z{/if}" class="secondary radius label">{$lang112}</a></li>
        {else}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=r">{$lang112}</a></li>
        {/if}
        {if $s eq "c" OR $s eq "cz"}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=c{if $s eq "c"}z{/if}" class="secondary radius label">{$lang436}</a></li>
        {else}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=c">{$lang436}</a></li>
        {/if}
        {if $s eq "e" OR $s eq "ez"}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=e{if $s eq "e"}z{/if}" class="secondary radius label">{$lang494}</a></li>
        {else}
        <li><a href="{$baseurl}/search?query={$tag}&amp;search_in={$search_in}&amp;c={$c}&amp;s=e">{$lang494}</a></li>
        {/if}
    </ul>
</div>

<div class="cats-2c row hide mb10">
<ul class="large-block-grid-2 small-block-grid-2">
  {insert name=get_categories assign=c}
  {section name=i loop=$c}
    <li class="subheading{if !($smarty.section.i.index % 2)} tright{/if}"><a href="{$baseurl}/categories/{$c[i].seo|stripslashes}"{if $catselect eq $c[i].CATID} class="active"{/if}>{$c[i].name|stripslashes}</a></li>
  {/section}
</ul>
</div>

{assign var=catselect value={$smarty.request.c}}
{if $posts}
<div class="work-area-wrapper">
{include file="bit.tpl"}
</div>
{else}
<div class="row mt20">
    <h2 class="subheading h-large">ძიება დასრულდა უშედეგოდ :(</h2>
    <span class="subheading">სცადე ძიება სხვა ფორმულირებით.</span>
</div>
{assign var=hide_jobs value="1"}
<div class="suggest-work-wrapper mt10">
    {include file="suggest_work.tpl"}
</div>
{/if}

<script>
  var lang = [];
  lang['search_in'] = '{$lang570}';
  var def_js = ['{$baseurl}/js/search.js'];
  var page = 'search{$tag}';
</script>