<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/inbox" class=current>{$lang226}</a>
  </ul>
</div>

<div class="row mt20">
  {if $message ne ""}<div class="large-12 small-12 columns">{include file="error.tpl"}</div>{/if}
  <div class="large-12 small-12 columns">
    <h1 class="heading h-large mb0">{$lang226}</h1>
    <h2 class="subheading">{$lang227}</h2>
  </div>
 </div>
 <div class="row collapse">
    <div class="filters large-12 small-12 columns mt10">
      <ul class="subheading">
        <li><strong class="heading">{$lang109}</strong></li>
      {if $s eq "all" OR $s eq ""}
        <li><a href="{$baseurl}/inbox?o={$o}&amp;a={$a}" class="secondary radius label">{$lang187}</a></li>
        {else}
        <li><a href="{$baseurl}/inbox?o={$o}&amp;a={$a}">{$lang187}</a></li>
        {/if}
        {if $s eq "unread"}
        <li><a href="{$baseurl}/inbox?s=unread&amp;o={$o}&amp;a={$a}" class="secondary radius label">{$lang228}</a></li>
        {else}
        <li><a href="{$baseurl}/inbox?s=unread&amp;o={$o}&amp;a={$a}">{$lang228}</a></li>
        {/if}
        {if $s eq "archived"}
        <li class="last"><a href="{$baseurl}/inbox?s=archived&amp;o={$o}&amp;a={$a}" class="secondary radius label">{$lang229}</a></li>
        {else}
        <li class="last"><a href="{$baseurl}/inbox?s=archived&amp;o={$o}&amp;a={$a}">{$lang229}</a></li>
        {/if}
        <li class="quick-nav ml10">
        <select id="conversations_quick_navigation" class="subheading ml10">
          {if $u GT "0"}
            <option value="o={$o}&amp;a={$a}&amp;s={$s}">{$lang230}</option>
            {section name=i loop=$m}
            <option style="font-weight: bold;" value="o={$o}&amp;a={$a}&amp;s={$s}&amp;u={$m[i].USERID|stripslashes}" selected="selected">{$m[i].username|stripslashes}</option>
            {/section}
            {else}
          <option>{$lang230}</option>
            {section name=i loop=$m}
            <option style="font-weight: bold;" value="o={$o}&amp;a={$a}&amp;s={$s}&amp;u={$m[i].USERID|stripslashes}">{$m[i].username|stripslashes}</option>
            {/section}
            {/if}
        </select>
      </li>
    </ul>
  </div>
 </div>
<div class="row">
{if $m|@count eq 0}
  <div class="large-12 small-12 columns">
    <h3 class="heading">{$lang234}</h3>
  </div>
</div>
{else}
  <div class="large-3 small-3 columns table-head heading s08">
    <a href="{$baseurl}/inbox?o=name&amp;s={$s}{if $a eq "1"}&amp;a=0{else}&amp;a=1{/if}" {if $o eq "name"}class="{if $a eq "1"}descending{else}ascending{/if}"{/if}>{if $o eq "name"}{if $a eq "1"}&#9650;{else}&#9660;{/if}{/if}&nbsp;{$lang231}</a>
  </div>
  <div class="large-7 small-6 columns table-head heading s08">
    <span class="ml10 block">{$lang545}</span>
  </div>
  <div class="large-2 small-3 columns table-head heading s08">
    <a href="{$baseurl}/inbox?o=time&amp;s={$s}{if $a eq "1"}&amp;a=0{else}&amp;a=1{/if}" {if $o eq "time"}class="{if $a eq "1"}descending{else}ascending{/if}"{/if}>{if $o eq "time"}{if $a eq "1"}&#9650;{else}&#9660;{/if}{/if}&nbsp;{$lang232}</a>
  </div>
</div>
{if $s eq "all"}
    {section name=i loop=$m}
    {assign var="show" value=1}
    {section name=j loop=$arc}
    {if $arc[j].AID eq $m[i].USERID}
    {assign var="show" value=0}
    {/if}
    {/section}
    {insert name=last_unread value=a assign=lur uid=$m[i].USERID}
    {if $show eq "1" OR $lur eq "1"}
    <div class="row table-row-hover {if $lur eq "1"}unread{else}read{/if}">
      <div class="large-3 small-3 columns">
        {* {if $lur eq "1"}<i class="icon-fire red"></i> {elseif $smarty.session.USERID == $m[i].msgfrom}<i class="icon-reply-all gray"></i>{/if} *}
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}">{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].USERID}{if $profilepicture}<span class="avatar"><img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width=24 height=24 alt="{$m[i].username|stripslashes}"></span>{else}&nbsp;<i class="icon-user icon-large"></i>&nbsp;{/if} <strong>{$m[i].username|stripslashes}</strong></a>
      </div>
      <div class="large-7 small-6 columns">
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}" class="subheading">{insert name=last_email value=a assign=lem uid=$m[i].USERID}{$lem|stripslashes|truncate:50:"...":true}</a>
      </div>
      <div class="large-2 small-3 columns">
        {insert name=get_time_to_days_ago value=a time=$m[i].time}
      </div>
    </div>
    {/if}
    {/section}
 {elseif $s eq "unread"}
  {section name=i loop=$m}
    {assign var="show" value=1}
    {section name=j loop=$arc}
    {if $arc[j].AID eq $m[i].USERID}
    {assign var="show" value=0}
    {/if}
    {/section}
    {insert name=last_unread value=a assign=lur uid=$m[i].USERID}
    {if $show eq "1" AND $lur eq "1"}
    <div class="row table-row-hover {if $lur eq "1"}unread{else}read{/if}">
      <div class="large-3 small-3 columns">
        {* {if $smarty.session.USERID == $m[i].msgfrom}<i class="icon-reply-all gray"></i>{/if} *}
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}"><a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}">{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].USERID}{if $profilepicture}<span class="avatar"><img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width=24 height=24 alt="{$m[i].username|stripslashes}"></span>{else}&nbsp;<i class="icon-user icon-large"></i>&nbsp;{/if} <strong>{$m[i].username|stripslashes}</strong></a>
      </div>
      <div class="large-7 small-6 columns">
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}" class="subheading">{insert name=last_email value=a assign=lem uid=$m[i].USERID}{$lem|stripslashes|truncate:50:"...":true}</a>
      </div>
      <div class="large-2 small-3 columns">
        {insert name=get_time_to_days_ago value=a time=$m[i].time}
      </div>
    </div>
    {/if}
    {/section}
 {elseif $s eq "archived"}
  {section name=i loop=$m}
    {assign var="show" value=0}
    {section name=j loop=$arc}
    {if $arc[j].AID eq $m[i].USERID}
    {assign var="show" value=1}
    {/if}
    {/section}
    {insert name=last_unread value=a assign=lur uid=$m[i].USERID}
    {if $show eq "1" AND $lur eq "0"}
    <div class="row table-row-hover {if $lur eq "1"}unread{else}read{/if}">
      <div class="large-3 small-3 columns">
      {if $smarty.session.USERID == $m[i].msgfrom}<i class="icon-reply-all gray"></i>{/if}
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}"><a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}">{insert name=get_member_profilepicture assign=profilepicture value=var USERID=$m[i].USERID}{if $profilepicture}<span class="avatar"><img src="{$membersprofilepicurl}/thumbs/A_{$profilepicture}" width=24 height=24 alt="{$m[i].username|stripslashes}"></span>{else}&nbsp;<i class="icon-user icon-large"></i>&nbsp;{/if} <strong>{$m[i].username|stripslashes}</strong></a>
      </div>
      <div class="large-7 small-6 columns">
        <a href="{$baseurl}/{insert name=get_seo_convo value=a assign=cvseo username=$m[i].username|stripslashes}{$cvseo}" class="subheading">{insert name=last_email value=a assign=lem uid=$m[i].USERID}{$lem|stripslashes|truncate:50:"...":true}</a>
      </div>
      <div class="large-2 small-3 columns">
        {insert name=get_time_to_days_ago value=a time=$m[i].time}
      </div>
    </div>
    {/if}
    {/section}
 {/if}
{/if}
<p>&nbsp;</p>