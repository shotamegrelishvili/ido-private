<div class="contrast-wrapper heading">
  <h1 class="heading">იმუშავე სახლიდან გაუსვლელად</h1>
  <h2 class="subheading">მიკროდასაქმების ონლაინ-პლატფორმა</h2>
</div>

{if $enable_fc eq "1"}
<div class="row">
    <div class="large-12 small-12 columns tcenter mt20">
        <a href="https://www.facebook.com/dialog/permissions.request?app_id={$FACEBOOK_APP_ID}&amp;display=page&amp;next={$baseurl}/&amp;response_type=code&amp;fbconnect=1&amp;perms=email" class="button large radius fb heading"><i class="icon-facebook icon-large"></i> {$lang469}</a>       
    </div>
</div>
 
{/if}
{if $environment=='local' || TRUE}
              <div class="row">
                <div id="main">
                  <div class="content mt50">
                      	<link href="{$baseurl}/css/login.css" media="screen" rel="stylesheet" type="text/css" />
                        <div class="login-holder">	
                            <div id="login-toggle-box" class="login-container">
                                <div id="reg-login">
                                    <div class="login-area">
                                        <div class="loginform" >
                                            <div class="loginwrapper" >
                                                <div class="badge-header">
                                                    <h2>{$lang40}</h2>
                                                    <h3>{$lang48} <a href="{$baseurl}/signup{if $r ne ""}?r={$r|stripslashes}{/if}">{$lang49}</a></h3>
                                                </div>
                                                <form action="{$baseurl}/login" method="post">  
                                                	
                                                    {if $error ne ""}
                                                        <div id="errorExplanation">
                                                            <h2>{$lang11}</h2>
                                                            <ul>
                                                                {$error}
                                                            </ul>
                                                        </div>
                                                    {/if}
                                                                                                  
                                                    <div class="form-entry">
                                                        <label for="l_username">{$lang36}</label>
                                                        <input class="text" id="l_username" maxlength="16" name="l_username" size="16" tabindex="1" type="text" value="{$user_username}" />
                                                    </div>
                                                    <div class="form-entry">
                                                        <div class="form-label">
                                                            <label for="l_password">{$lang37}</label>
                                                            <span> <a href="{$baseurl}/forgotpassword" style="text-decoration:none">{$lang39}</a></span>
                                                        </div>
                                                        <input class="text" id="l_password" name="l_password" size="30" tabindex="2" type="password" />
                                                    </div>
                                                    <div class="row">
                                                        <input type="submit" value="{$lang2}" class="button" style="padding-left:10px;padding-right:10px;padding-top:5px;padding-bottom:5px;" />
                                                        <input type="hidden" name="jlog" id="jlog" value="1" />
                                                        <div class="remember">
                                                            <input class="checkbox" id="l_remember_me" name="l_remember_me" type="checkbox" value="1" />
                                                            <label for="l_remember_me">{$lang38}</label>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="r" value="{$r|stripslashes}" />
                                                </form>
                                            </div>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
{/if}