<form action="#" class="custom fr-list-form mt10">
<div class="row subheading">
	<a href="javascript:" class="fr-new-task-btn">განათავსე განცხადება</a> და დაელოდე ქვევით მონიშნული ფრილანსერების შემოთავაზებებს.
</div>
<div class="row fr-list-header mt5">
	<div class="large-1 small-2 columns tcenter"><img src="{$baseurl}/images/ic_checkbox_on_gray.svg" alt="" class="fr-checkbox-toggle fr-checkbox-on-toggle"><img src="{$baseurl}/images/ic_checkbox_off_gray.svg" alt="" class="fr-checkbox-toggle fr-checkbox-off-toggle hide"></div>
	<div class="large-1 small-2 columns">&nbsp;</div>
	<div class="large-10 small-2 columns">ფრილანსერი</div>
</div>
{section name=i loop=$fr}
{$selected_freelancers[]=$fr[i].USERID}
<div class="row fr-list-row">
	<div class="large-1 small-2 columns fr-checkbox">
		<img src="{$baseurl}/images/ic_checkbox_on_gray.svg" alt="" class="fr-checkbox-r fr-checkbox-on" id="fr-c-on-{$fr[i].USERID}" data-userid="{$fr[i].USERID}"><img src="{$baseurl}/images/ic_checkbox_off_gray.svg" alt="" class="fr-checkbox-r fr-checkbox-off hide" id="fr-c-off-{$fr[i].USERID}" data-userid="{$fr[i].USERID}">
	</div>
	<div class="large-1 small-2 columns tcenter">
		<div class="fr-list-av-wrapper">
			<img src="{$membersprofilepicurl}/thumbs/{$fr[i].profilepicture}" alt="" class="fr-av-{$fr[i].USERID}">
	{insert name=get_ismember_online assign=isonline value=var USERID=$fr[i].USERID}{if $isonline}<div class="fr-list-user-online" title="Online"></div>{/if}
		</div>
	</div>
	<div class="large-9 small-2 columns fr-list-details-wrapper">
		<a href="{$baseurl}/user/{$fr[i].username}" class="fr-username-{$fr[i].USERID}">{$fr[i].username}</a><br>
		<span class="fr-list-details">
			{if $fr[i].location AND $fr[i].location NE 'სხვა'}
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
			<path d="M12 2c-3.87 0-7 3.13-7 7 0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/>
			<path d="M0 0h24v24h-24z" fill="none"/>
			</svg> {$fr[i].location} 
			{/if}
			{if $fr[i].ratingcount}
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
			<path d="M0 0h24v24h-24z" fill="none"/>
			<path d="M1 21h4v-12h-4v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06l-1.06-1.05-6.58 6.59c-.37.36-.59.86-.59 1.41v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01.01-.08z"/>
			</svg> {$fr[i].ratingcount} შეფასება <span class="fr-list-hl">{$fr[i].rating}%</span>
			{/if}
			{if $fr[i].portfolioItemsCC}
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
			<path d="M0 0h24v24h-24z" fill="none"></path>
			<path d="M22 16v-12c0-1.1-.9-2-2-2h-12c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2zm-11-4l2.03 2.71 2.97-3.71 4 5h-12l3-4zm-9-6v14c0 1.1.9 2 2 2h14v-2h-14v-14h-2z"></path>
			</svg> <a href="{$baseurl}/user/{$fr[i].username}#portfolio" rel="nofollow">პორტფოლიო</a> <span class="fr-list-hl">{$fr[i].portfolioItemsCC}</span>
			{/if}
			{if $fr[i].projectsCC}
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
			<path fill="#DB4437" d="M24 0v24h-24v-24h24m1-1h-26v26h26v-26z"></path>
			<path d="M20 6h-4v-2c0-1.11-.89-2-2-2h-4c-1.11 0-2 .89-2 2v2h-4c-1.11 0-1.99.89-1.99 2l-.01 11c0 1.11.89 2 2 2h16c1.11 0 2-.89 2-2v-11c0-1.11-.89-2-2-2zm-6 0h-4v-2h4v2z"></path>
			</svg> პროექტი <span class="fr-list-hl">{$fr[i].projectsCC}
			{/if}
		</span>
	</div>
	<div class="large-1 small-4 columns fr-list-action pm-dropdown" data-target="fr-a-{$fr[i].USERID}">
		<a href="javascript:" class="">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			<path d="M0 0h24v24h-24z" fill="none"/>
			<path d="M19.43 12.98c.04-.32.07-.64.07-.98s-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.12-.22-.39-.3-.61-.22l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65c-.03-.24-.24-.42-.49-.42h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.23-.09-.49 0-.61.22l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98s.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.12.22.39.3.61.22l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.23.09.49 0 .61-.22l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zm-7.43 2.52c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z"/>
			</svg>
		</a><span class="pm-ar"></span>
		<ul class="paper-menu fr-a-{$fr[i].USERID}">
			<li><a href="#">
				<span class="icon-gel">¢</span> დაქირავება
			</a></li>
			<li><a href="{$baseurl}/conversations/{$fr[i].username}">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				<path d="M2.01 21l20.99-9-20.99-9-.01 7 15 2-15 2z"/>
				<path d="M0 0h24v24h-24z" fill="none"/>
				</svg> წერილის მიწერა
			</a></li>
			<li><a href="{if !$fr[i].portfolioItemsCC}javascript:{else}{$baseurl}/user/{$fr[i].username}#portfolio{/if}"{if !$fr[i].portfolioItemsCC} class="disabled"{/if}>
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
				<path d="M0 0h24v24h-24z" fill="none"></path>
				<path d="M22 16v-12c0-1.1-.9-2-2-2h-12c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2zm-11-4l2.03 2.71 2.97-3.71 4 5h-12l3-4zm-9-6v14c0 1.1.9 2 2 2h14v-2h-14v-14h-2z"></path>
				</svg> პორტფოლიო
			</a></li>
			<li><a href="{$baseurl}/user/{$fr[i].username}#basic">
			<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="#757575">
			<path d="M0 0h24v24h-24z" fill="none"/>
			<path d="M5 13.18v4l7 3.82 7-3.82v-4l-7 3.82-7-3.82zm7-10.18l-11 6 11 6 9-4.91v6.91h2v-8l-11-6z"/>
			</svg> კომპეტენცია</a></li>
		</ul>
	</div>
</div>
{/section}
<input type="hidden" name="fr_selected_freelancers" id="fr_selected_freelancers" value=",{$selected_freelancers|implode:','},">
</form>