<div class="row tcenter">
	<div class="large-1 columns">&nbsp;</div>
	<div class="large-11 columns">
		<h2 class="heading f16 mt30">დაიქირავე შემსრულებელი. ჩაიბარე სამუშაო.</h2>
	</div>
</div>
<div class="row tcenter">
	<div class="large-10 large-centered small-12 columns">
		<div class="row">
			<div class="large-1 columns">&nbsp;</div>
			<div class="large-3 small-12 columns">
				<a href="{$baseurl}/suggested"><i class="icon-pencil mp-post-job-icon"></i><br>
				<span class="subheading">განათავსე განცხადება უფასოდ</span></a>
			</div>
			<div class="large-1 columns"><i class="icon-chevron-right"></i></div>
			<div class="large-3 small-12 columns">
				<i class="icon-user mp-post-job-icon"></i><br>
				<span class="subheading">შეარჩიე შემსრულებელი ონლაინ</span>
			</div>
			<div class="large-1 columns"><i class="icon-chevron-right"></i></div>
			<div class="large-3 small-12 columns">
				<span class="icon-gel mp-post-job-icon">{$lang63}</span><br>
				<span class="subheading">ჩაიბარე სამუშაო დისტანციურად</span>
			</div>
		</div>
	</div>
</div>
