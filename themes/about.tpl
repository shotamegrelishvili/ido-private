<div class="crumbs">
  <ul class="row collapse heading">
    <li><a href="{$baseurl}/">{$lang0}</a>
    <li><a href="{$baseurl}/about" class=current>{$lang416}</a>
  </ul>
</div>

<div class="row mt20">
  <div class="large-12 small-12 columns">
    <h1 class="heading">{$lang416}</h1>
    <span class=subheading><strong>iDo.ge</strong> წარმოადგენს დასაქმების ონლაინ-პლატფორმას, რომლის მიზანია საქართველოს მასშტაბით ხელი შეუწყოს ნიჭიერ და კრეატიულ მომხმარებლებს საკუთარი შესაძლებლობების რეალიზაციაში, ხოლო დამსაქმებელს ხელმისაწვდომი ადამიანური რესურსების მოძებნაში ინტერნეტის საშუალებით. დისტანციურად მუშაობა შესაძლებელია და ეს კომფორტული და ახლა უკვე ხელმისაწვდომიცაა!<br><a href="javascript:" class="startAdvantagesTour-btn"><i class="icon-rocket"></i> გაეცანი iDo.ge-ს უპირატესობებს</a>.</span>
    <div class="cats-2c mt10 p10 table-head">
      <h2 class="heading"><strong>გარანტიები</strong></h2>
      <span class="subheading">განცხადების ავტორსა და დამკვეთს შორის ავტომატურად ფორმდება  ელექტრონული ხელშეკრულება, რომელიც არეგულირებს საქმიან ურთიერთობებს.<br>
      ყველა ფინანსური ოპერაცია სრულდება დაცული ელექტრონული სისტემა „<a href="https://www.emoney.ge/index.php/content/faq#4_85" target="_blank" rel="nofollow">eMoney</a>“-ს საშუალებით. დამკვეთის მიერ გადახდილი თანხა იბლოკება და საიმედოდ ინახება მანამ, სანამ დამკვეთი არ ჩაიბარებს შესრულებულ სამუშაოს. სამუშაოს არასრულყოფილად ან უხარისხოდ შესრულების შემთხვევაში დამკვეთს შეუძლია <strong>სრულად</strong> დაიბრუნოს გადახდილი თანხა.
      </span>
    </div>
  </div>
</div>

<div class=mt10>
  <div class="row">
  <div class="large-6 columns c1-w">
    <div class="c1b">
    <h1 class="heading">{$lang420}</h1>
    <p class="subheading">{$lang421}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang422}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang423}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang424}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang425}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang426}</p>
    </div>
  </div>
  <div class="large-6 columns c2-w">
    <div class="c2b">
    <h1 class="heading">{$lang427}</h1>
    <p class="subheading">{$lang428}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang429}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang430}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang431}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang432}</p>
    <div class="tcenter"><i class="icon-angle-down icon-2x gray"></i></div>
    <p class="subheading">{$lang433}</p>
    </div>
  </div>
  </div>
</div>
<div class="row"><div class="m10 subheading"><a href="{$baseurl}/terms_of_service"><i class="icon-check-sign"></i> {$lang434}</a></div></div>
<div class="row"><div class="m10 subheading"><i class="icon-info-sign green"></i> <a href="{$baseurl}/?startTour">იხილე საიტის გაცნობითი ტური.</a></div></div>

<div id="advantages-wrapper">
<ol id="advantages" class="joyride-list" data-joyride>
  <li data-options="tipAnimationFadeSpeed:1000;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>რა უპირატესობები აქვს <span class="lat almost-black">iDo.ge</span>-ს?</h4>
    <p class="subheading s08 mb0">1. გადახდილი თანხა საიმედოდ ინახება სასურველი შედეგის მიღებამდე.<br>ყველა ტრანზაქცია სრულდება დაცული ელექტრონული სისტემა eMoney-ს საშუალებით. გადასახდელი თანხა იბლოკება eMoney-ს სისტემაში, ხოლო შემსრულებელი მიიღებს ანაზღაურებას დამკვეთის მიერ სამუშაოს მიღების შემდეგ.</p>
  </li>
  <li data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>თანხის დაბრუნების გარანტია</h4>
    <p class="subheading s08 mb0">2. თუ სამუშაოს შედეგი უხარისხო და მიუღებელია დამკვეთისათვის, გადახდილი თანხა სრულად უბრუნდება მას.</p>
  </li>
  <li data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>დისტანციური მუშაობის საშუალება</h4>
    <p class="subheading s08 mb0">3. იმუშავე შემსრულებელთან ან დამკვეთთან დისტანციურად დღის თავისუფალი განრიგით და შენთვის სასურველი ადგილიდან - თუნდაც ტროპიკული სამოთხიდან, მთავარია გქონდეს ინტერნეტი :)</p>
  </li>
  <li data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-text="შემდეგი">
    <h4 class=subheading>გამოავლინე საკუთარი პოტენციალი</h4>
    <p class="subheading s08 mb0">4. დროა დასაქმდე! გამოავლინე საკუთარი ნიჭი და მონდომება და აქციე დრო ფულად.</p>
  </li>
  <li data-options="tipAnimationFadeSpeed:500;tipAnimation:fade" data-button="OK">
    <h4 class=subheading>დამატებითი შემოსავალი</h4>
    <p class="subheading s08 mb0">5. მოსინჯე საკუთარი თავი ახალ პროფესიებში და მიიღე დამატებითი შემოსავალი.</p>
  </li>
</ol>
</div>

<script>
  var baseurl = '{$baseurl}';
  var startAdvantagesTour = {if isset($smarty.get.advantages)}true{else}false{/if};
  var def_js = ['{$baseurl}/js/jquery.scrollTo-min.js?v=2','{$baseurl}/js/about.js?v=001'];
</script>