<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
header('Location:'.$thebaseurl.'/signin'); exit;


// OBSOLETE
/*
if ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{
	header("Location:$config[baseurl]/");exit;
}
$r = cleanit(stripslashes($_REQUEST['r']));
abr('r',$r);
if ($config['enable_ref'] == "1")
{
	$ref = intval(cleanit(stripslashes($_REQUEST['ref'])));
	abr('ref',$ref);
}
if ($_REQUEST['econnect_signup'] && $_REQUEST['email'] && $_REQUEST['password']) {
	include('libraries/eConnect.class.php');
	$econnectClass = new econnect();
	$r = $econnectClass->eAuth($_REQUEST['email'], $_REQUEST['password']);
	if ($econnectClass->_status == 400) {
		$error .= $lang['628'];
	}
	elseif ($econnectClass->_status == 200) {
		$res = json_decode($r);
		$_SESSION['access_token'] = $res->access_token;
		$_SESSION['signin_gateway'] = 'econnect';
		$_SESSION['session_expiry'] = $_SERVER['REQUEST_TIME'] + 3000;

		$econnected_user = $econnectClass->getUserInfo($_SESSION['access_token']);
		if ($econnected_user['email']) {
			$query="SELECT USERID, pin, mobile, profilepicture FROM members WHERE eemail='".sql_quote($econnected_user['email'])."' LIMIT 1";
			$result=$conn->execute($query);
			if (!$result->_numOfRows) {
				$econnected_user['USERID'] = registerNewUser($econnected_user);
				signinUser($econnected_user);
				$_SESSION['temp']['message_title'] = $lang['629'];
				$_SESSION['temp']['message_type'] = 'success';
				header("Location:$config[baseurl]/settings"); exit;
			}
			else {
				$r=updateUser($result->fields,$econnected_user);
				if ($r)
					$econnected_user['USERID'] = $result->fields['USERID'];
				signinUser($econnected_user);
				header("Location:$config[baseurl]/"); exit;
			}
		}
	}
    
	//d($econnectClass->Authenticate($_GET['email'], $_GET['password']));
}
if($_REQUEST['jsub'] == "1" && FALSE)
{
	$user_email = cleanit($_REQUEST['user_email']);
	if($user_email == "")
	{
		$error .= "<li>".$lang['12']."</li>";
	}
	elseif(!verify_valid_email($user_email))
	{
		$error .= "<li>".$lang['15']."</li>";
	}
	elseif (!verify_email_unique($user_email))
	{
		$error .= "<li>".$lang['16']."</li>";
	}
	
	$user_username = cleanit($_REQUEST['user_username']);
	if($user_username == "")
	{
		$error .= "<li>".$lang['13']."</li>";	
	}
	elseif(strlen($user_username) < 4)
	{
		$error .= "<li>".$lang['25']."</li>";	
	}
	elseif(strlen($user_username) > 15)
	{
		$error .= "<li>".$lang['508']."</li>";	
	}
	elseif(!preg_match("/^[a-zA-Z0-9]*$/i",$user_username))
	{
		$error .= "<li>".$lang['24']."</li>";
	}
	elseif(!verify_email_username($user_username))
	{
		$error .= "<li>".$lang['14']."</li>";
	}
	
	$user_password = cleanit($_REQUEST['user_password']);
	$user_password2 = str_replace(" ", "", $user_password);
	if($user_password == "" || $user_password2 == "")
	{
		$error .= "<li>".$lang['17']."</li>";	
	}
	
	$user_captcha_solution = cleanit($_REQUEST['user_captcha_solution']);
	if($user_captcha_solution == "")
	{
		$error .= "<li>".$lang['18']."</li>";	
	}
	elseif($user_captcha_solution != $_SESSION['imagecode'])
	{
		$error .= "<li>".$lang['19']."</li>";	
	}
	
	$user_terms_of_use = cleanit($_REQUEST['user_terms_of_use']);
	if($user_terms_of_use != "1")
	{
		$error .= "<li>".$lang['20']."</li>";	
	}
	
	if($error == "")
	{
		$md5pass = md5($user_password);
		$def_country = $config['def_country'];
		if($def_country == "")
		{
			$def_country = "US";	
		}
		$query="INSERT INTO members SET email='".sql_quote($user_email)."',username='".sql_quote($user_username)."', password='".sql_quote($md5pass)."', pwd='".sql_quote($user_password)."', addtime='".time()."', lastlogin='".time()."', ip='".$_SERVER['REMOTE_ADDR']."', lip='".$_SERVER['REMOTE_ADDR']."', country='".sql_quote($def_country)."'";
		$result=$conn->execute($query);
		$userid = $conn->_insertid();
		
		if($userid != "" && is_numeric($userid) && $userid > 0)
		{
			$query="SELECT USERID,email,username,verified from members WHERE USERID='".sql_quote($userid)."'";
			$result=$conn->execute($query);
			
			$SUSERID = $result->fields['USERID'];
			$SEMAIL = $result->fields['email'];
			$SUSERNAME = $result->fields['username'];
			$SVERIFIED = $result->fields['verified'];
			$_SESSION['USERID']=$SUSERID;
			$_SESSION['EMAIL']=$SEMAIL;
			$_SESSION['USERNAME']=$SUSERNAME;
			$_SESSION['VERIFIED']=$SVERIFIED;
			
			// Generate Verify Code Begin
			$verifycode = generateCode(5).time();
			$query = "INSERT INTO members_verifycode SET USERID='".sql_quote($SUSERID)."', code='$verifycode'";
            $conn->execute($query);
			if(mysql_affected_rows()>=1)
			{
				$proceedtoemail = true;
			}
			else
			{
				$proceedtoemail = false;
			}
			// Generate Verify Code End
			
			// Send Welcome E-Mail Begin
			if ($proceedtoemail)
			{
                $sendto = $SEMAIL;
                $sendername = $config['site_name'];
                $from = $config['site_email'];
                $subject = $lang['21']." ".$sendername;
                $sendmailbody = stripslashes($_SESSION['USERNAME']).",<br><br>";
				$sendmailbody .= $lang['22']."<br>";
				$sendmailbody .= "<a href=".$config['baseurl']."/confirmemail?c=$verifycode>".$config['baseurl']."/confirmemail?c=$verifycode</a><br><br>";
				$sendmailbody .= $lang['23'].",<br>".stripslashes($sendername);
                mailme($sendto,$sendername,$from,$subject,$sendmailbody,$bcc="");
			}
			// Send Welcome E-Mail End
			
			if ($config['enable_ref'] == "1")
			{
				$ref_price = cleanit($config['ref_price']);
				if($ref > 0)
				{
					$query = "INSERT INTO referrals SET USERID='".sql_quote($ref)."', REFERRED='".sql_quote($SUSERID)."', money='".sql_quote($ref_price)."', time_added='".time()."', ip='".$_SERVER['REMOTE_ADDR']."'";
            		$conn->execute($query);	
				}
			}
			
			$redirect = base64_decode($r);
			if($redirect == "")
			{
				header("Location:$thebaseurl/");exit;
			}
			else
			{
				$rto = $thebaseurl."/".$redirect;
				header("Location:$rto");exit;
			}
		}	
	}
	else
	{
		abr('user_email',$user_email);
		abr('user_username',$user_username);
		abr('user_password',$user_password);
		abr('user_password2',$user_password2);
		abr('user_terms_of_use',$user_terms_of_use);
	}
}

$templateselect = "signup.tpl";
$pagetitle = $lang['1'];
abr('pagetitle',$pagetitle);

//TEMPLATES BEGIN
abr('error',$error);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END

// Functions BEGIN
function registerNewUser($user) {
	global $conn;

	$sqladd = '';
	if (!$user['email']) return false;
	elseif ($user['pin'] && strlen($user['pin']) == 11) $sqladd = ", pinprotect='1'";

	$query="INSERT INTO members SET email='".sql_quote($user['email'])."',eemail='".sql_quote($user['email'])."',username='".sql_quote(ucfirst(strtolower($user['first_name'])).'_'.ucfirst(strtolower($user['last_name'])))."', fullname='".transgeo($user['first_name'].' '.$user['last_name'])."', pin='".sql_quote($user['pin'])."', mobile='".sql_quote($user['phone'])."', password='".sql_quote(md5('0(8&7'.$user['account']))."', addtime='".$_SERVER['REQUEST_TIME']."', lastlogin='".$_SERVER['REQUEST_TIME']."', ip='".$_SERVER['REMOTE_ADDR']."', lip='".$_SERVER['REMOTE_ADDR']."', country='".sql_quote($user['country'])."', verified='1', status='1',eemail_protect='1'".$sqladd;
	$result=$conn->execute($query);
	$USERID = $conn->_insertid();
	if ($user['avatar']) {
		upload_and_update_avatar($USERID, $user['avatar']);
	}
	return $USERID;
}

function updateUser($rowdata, $user) {
	global $conn, $config;

	$sqladd = '';
	if (!$rowdata['profilepicture'] && $user['avatar']) {
		upload_and_update_avatar($rowdata['USERID'], $user['avatar']);
	}
	if (!$rowdata['pinprotect'] && $user['pin']) {
		$sqladd .= ",pin='".sql_quote($user['pin'])."', pinprotect='1'";
	}
	if (!$rowdata['mobile'] && $user['phone']) {
		$sqladd .= ",mobile='".sql_quote($user['phone'])."'";
	}
	$query="UPDATE members SET lastlogin='".$_SERVER['REQUEST_TIME']."', lip='".$_SERVER['REMOTE_ADDR']."', country='".sql_quote($user['country'])."', verified='1', status='1',eemailprotect='1'".$sqladd." WHERE USERID='".intval($rowdata['USERID'])."' LIMIT 1";
	$conn->execute($query);
	return true;
}

function signinUser($user) {
	if (!$user['USERID']) return false;
	$_SESSION['USERID'] = $user['USERID'];
	$_SESSION['EMAIL'] = $user['email'];
	$_SESSION['USERNAME'] = ucfirst(strtolower($user['first_name'])).'_'.ucfirst(strtolower($user['last_name']));
	$_SESSION['VERIFIED'] = 1;
	return true;
}

// Functions END
*/
?>