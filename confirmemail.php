<?php
include("include/config.php");
include("include/functions/import.php");

if ($_REQUEST['c'] != "")
{
	if (strlen($_REQUEST['c']) != "15")
	{
		$error = $lang['403'];
	}
	else
	{
		$code = htmlentities(strip_tags($_REQUEST['c']), ENT_COMPAT, "UTF-8");
		$query="SELECT * FROM `members_verifycode` WHERE `code`='".sql_quote($code)."' LIMIT 1";
		$result=$conn->execute($query);
		
		if($result->recordcount()>=1)
		{
			$userid = $result->fields['USERID'];
			
			$query="SELECT `verified` FROM `members` WHERE `USERID`='".intval($userid)."' LIMIT 1";
			$result=$conn->execute($query);
			$verified = $result->fields['verified'];
			
			if ($verified == "1")
			{
				$error = $lang['404'];
			}
			else
			{
				$query="UPDATE `members` SET `verified`='1' WHERE `USERID`='".intval($userid)."' LIMIT 1";
				$result=$conn->execute($query);
				$message = $lang['405'];
				if ($_SESSION[USERID] == $userid)
				{
					$_SESSION[VERIFIED] = "1";
				}
			}
		}
		else
		{
			$error = $lang['403'];;
		}
	}
}

$pagetitle = $lang['406'];
abr('pagetitle',$pagetitle);
abr('message',$message);
abr('error',$error);

//TEMPLATES BEGIN
$smarty->display('header.tpl');
$smarty->display('confirmemail.tpl');
$smarty->display('footer.tpl');
//TEMPLATES END
?>