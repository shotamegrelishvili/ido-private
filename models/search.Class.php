<?php
//include('../include/config.php');
//include('../include/functions/import.php');

if (!IN_APP) exit('access denied');

class searchClass {
	public $resultSize = 0;

	public $config = array(
		'keywordMinLength' => 2,
		);

	public function keywordsNormalize($keywords = array()) {
		foreach ($keywords as $k=>$keyword) {
			if ($keyword AND ($l=mb_strlen($keyword, 'UTF-8')) > 4 AND ($f=mb_strpos($keyword, 'ების')) > 0 AND $f == $l-4) {
				if ($l-4 >= $this->config['keywordMinLength'])
					$keywords[$k] = mb_substr($keyword,0,$l-4,'UTF-8');
				else
					unset($keywords[$k]);
			}
			elseif ($keyword AND ($l=mb_strlen($keyword, 'UTF-8')) > 3 AND ($f=mb_strpos($keyword, 'ები')) > 0 AND $f == $l-3) {
				if ($l-3 >= $this->config['keywordMinLength'])
					$keywords[$k] = mb_substr($keyword,0,$l-3,'UTF-8');
				else
					unset($keywords[$k]);
			}
			elseif ($l > 3 AND ((($ll=mb_substr($keyword,$l-1,1,'UTF-8'))=='ს' || $ll=='ი'))) {
				$keywords[$k] = mb_substr($keyword, 0, $l-1, 'UTF-8');
			}
			elseif (!$keyword || $l < $this->config['keywordMinLength']) {
				unset($keywords[$k]);
			}
		}
		return $keywords;
	}
	
	public function prepareKeywords($data = '') {
		return (explode(' ', preg_replace('/[ ,.;_:]+/', ' ', $data)));
	}

	public function findFreelancersByKeyID($keys=array('CATID'=>null,'KID'=>null,'ALIASID'=>null),$withPortfolio=false,$limit=20) {
		global $conn;
		$sql_add = '1=1';
		foreach ($keys as $key=>$val) {
			if ($val=intval($val)) {
				$sql_add .= ' AND E.`'.$key.'`="'.$val.'"';
			}
		}

		$query = 'SELECT E.`USERID` FROM `members_exp` E WHERE '.$sql_add;
		//d($query);
		$exp = $conn->execute($query);
		$userids = $exp->getrows();
		if (!$this->resultSize=sizeof($userids)) return false;

		$userstring = '';
		foreach ($userids as $u) {
			$userstring .= $u['USERID'].',';
		}
		$userstring = rtrim($userstring,',');
		$sql_add = '';
		if ($withPortfolio) $sql_add .= ' HAVING `portfolioItemsCC`>0';
		$query = 'SELECT M.`USERID`, M.`level`, M.`score`, M.`username`, M.`rating`, M.`ratingcount`, M.`profilepicture`, M.`location`,
			(SELECT COUNT(P.`FID`) as `portfolioItemsCC` FROM `portfolio` P WHERE P.`USERID`=M.`USERID`) as `portfolioItemsCC`,
			(SELECT COUNT(O.`OID`) FROM `orders` O WHERE O.`PID` IN (SELECT D.`PID` FROM `posts` D WHERE D.`USERID`=M.`USERID`) AND O.`status` IN (1,4,5,6)) as `projectsCC`
			FROM `members` M 
			WHERE 
			/*M.`USERID` IN ('.$userstring.')*/
			M.`USERID` > 0
			AND M.`status`="1"
			AND (M.`blocked_till_date` is null OR M.`blocked_till_date` < UNIX_TIMESTAMP(NOW()))
			'.$sql_add.'
			ORDER BY 
				M.`score` DESC, 
				M.`level` DESC, 
				M.`lastlogin`, 
				M.`rating` DESC, 
				M.`ratingcount` DESC,
				`portfolioItemsCC` DESC,
				`projectsCC` DESC
			LIMIT '.$limit;

			//d($query);

		$exp = $conn->Execute($query);
		$fr = $exp->getrows();
		$this->resultSize=sizeof($fr);
		return $fr;
	}
}

$searchClass = new searchClass();
?>