<?php
//include('../include/config.php');
//include('../include/functions/import.php');

if (!IN_APP) exit('access denied');

class settingsClass {
	public $allowedFields = 
	array(
		'`members`' => array(
			'include' => 1,
			'fields' =>	array(
				'profilepicture'=>1,
				'fullname'=>1,
				'pin'=>1,
				'eemail'=>1,
				'location'=>1,
				'email'=>1,
				'mobile'=>1,
				'description'=>1,
			),
		),

		'`members_exp`' => array(
			'include' => 1,
			'fields' =>	array(
				'CATID'=>1,
			),
		),

		'`portfolio`' => array(
			'include' => 1,
			'fields' =>	array(
				'order'=>1,
			),
		),

		'`members_friends`' => array(
			'include' => 0,
			'fields' =>	array(
			'invites'=>0,
			),
		),
	);

	public $field2Tab = array(
		'profilepicture'=>'basic',
		'fullname'=>'basic',
		'pin'=>'basic',
		'eemail'=>'basic',
		'location'=>'basic',
		'email'=>'contact',
		'mobile'=>'contact',
		'description'=>'experience',
		'CATID'=>'experience',
		'order'=>'portfolio',
		'invites'=>'value-added',
	);

	public $requiredTab = '';
	public $resultFields = array();
	public $userVAS = array();
	public $error = array();

	public $record = array(
		'field'=>'',
		'value'=>''
		);

	public function countTotalFields() {
		$this->ccFields = 0;
		foreach ($this->allowedFields as $tables) {
			if ($tables['include'])
				$this->ccFields += sizeof($tables['fields']);
		}
	}

	public function countFilledFields() {
		global $conn;
		$this->countTotalFields();
		$this->ccFilledFields = 0;

		foreach ($this->allowedFields as $table=>$t) {
			$query = 'SELECT * FROM '.sql_quote($table).' WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 1';
//			d($query);
			$r = $conn->execute($query);
			foreach ($t['fields'] as $field=>$inc) {

				if ($t['include'] && $r->fields[$field]) {
					$this->ccFilledFields++;
				}
				if ($r->fields[$field]) {
					$this->resultFields[trim($table,'`')][$field] = $r->fields[$field];
				}
				elseif (!$this->requiredTab) {
					$this->requiredTab = $this->field2Tab[$field];
				}
			}
		}
		//d($this->requiredTab);
		//d($this->resultFields);
	}

	public function getUserVAS($USERID) {
		global $conn;

		$r = $conn->execute('SELECT *, unix_timestamp(`validfrom_datetime`) as `validfrom`, unix_timestamp(`validtill_datetime`) as `validtill` FROM `members_vas` WHERE `USERID`="'.intval($_SESSION['USERID']).'"');
		$res = $r->getrows();
		foreach ($res as $r) {
			$this->userVAS[$r['service']] = array(
				'VID' => $r['VID'],
				'status' => $r['status'],
				'validfrom' => $r['validfrom'],
				'validtill' => $r['validtill'],
				);
		}
		return $this->userVAS;
	}

	protected function addUserVAS($service='' , $USERID, $fromTimestamp=0, $toTimestamp=0) {
		global $conn;
		if (!$fromTimestamp) $fromTimestamp = $_SERVER['REQUEST_TIME'];
		if (!$toTimestamp) $toTimestamp = $_SERVER['REQUEST_TIME']+86400;
		$conn->execute('INSERT IGNORE INTO `members_vas` SET `USERID`="'.intval($USERID).'", `service`="'.sql_quote($service).'", `status`="1", `validfrom_datetime`="'.strftime('%Y-%m-%d %H:%M:%S',$fromTimestamp).'", `validtill_datetime`="'.strftime('%Y-%m-%d %H:%M:%S',$toTimestamp).'"');
	}

	protected function activateUserVAS($service='' , $USERID) {
		global $conn;
		$conn->execute('UPDATE `members_vas` SET `status`="1" WHERE `USERID`="'.intval($USERID).'" AND `service`="'.sql_quote($service).'" LIMIT 1');

	}

	protected function deactivateUserVAS($service='', $USERID) {
		global $conn;
		$conn->execute('UPDATE `members_vas` SET `status`="0" WHERE `USERID`="'.intval($USERID).'" AND `service`="'.sql_quote($service).'" LIMIT 1');
	}

	public function updateUserVAS($service='', $USERID, $status=1) {
		global $config;
		if ($status==1) {
			if (!$this->userVAS[$service]) {
				$this->userVAS[$service]['status'] = 1;
				$this->addUserVAS($service, $USERID, $_SERVER['REQUEST_TIME'], $_SERVER['REQUEST_TIME'] + $config['value_added']['free_valid_period']);
			}
			elseif ($this->userVAS[$service]['validtill'] > $_SERVER['REQUEST_TIME']) {
				$this->userVAS[$service]['status'] = 1;
				$this->activateUserVAS($service, $USERID);
			}
		}
		else {
			$this->deactivateUserVAS($service, $USERID);
			$this->userVAS[$service]['status'] = 0;
		}
	}

	public function checkValidity($field='', $USERID, $val) {
		global $conn, $lang, $config;
		$val = trim($val);
		if (!$field) return false;
		elseif ($field == 'fullname') {
			// Checking Fullname
			if($val == '' || mb_strlen($val) < 5) {
				$this->error['fullname'] = '<li>'.$lang['559'].'</li>';
				return false;
			}
			else {
				$this->record = array(
					'field'	=>	'fullname',
					'value'	=>	$val
				);
			}
			// Checking Fullname end
		}
		elseif ($field == 'pin') {
			// Checking PIN
			if($val == '' || trim($val) == '' || strpos($val,'0000000')!==FALSE ||  strpos($pin,'1111111')!==FALSE || strpos($val,'123456789')!==FALSE) {
				$this->error['pin'] = '<li>'.$lang['558'].'</li>';
				return false;
			}
			else {
				$q = 'SELECT `pin` FROM `members` WHERE `pinprotect`="1" AND `USERID`="'.intval($USERID).'" LIMIT 1';
				$e = $conn->execute($q);
				if ($e->fields['pin']) {
					$this->error['pin'] = '<li>'.$lang['837'].'</li>';
					return false;
				}

				$q = 'SELECT count(*) as total FROM `members` WHERE `pin`="'.sql_quote($val).'" AND `USERID`!="'.intval($USERID).'" LIMIT 1'; 
				$e = $conn->execute($q);
				$te = $e->fields[total]+0;
				if($te > 0) {
					$this->error['pin'] = '<li>'.$lang['560'].'</li>';
					return false;
				}
				else {
					$this->record = array(
						'field'	=>	'pin',
						'value'	=>	$val
					);
				}
			}
			// Checking PIN end
		}
		elseif ($field == 'location') {
			$this->record = array(
				'field'	=>	'location',
				'value'	=>	$val
			);
		}
		elseif ($field == 'email') {
			// Checking Email
			if($val == '') {
				$this->error['email'] = '<li>'.$lang['12'].'</li>';
				return false;
			}
			elseif(!verify_valid_email($val)) {
				$this->error['email'] = '<li>'.$lang['15'].'</li>';
				return false;
			}
			else {
				$q = 'SELECT count(*) as total FROM `members` WHERE (`email`= "'.sql_quote($val).'" OR `eemail`="'.sql_quote($val).'") AND `USERID`!="'.sql_quote($USERID).'" LIMIT 1';
				$e = $conn->execute($q);
				$te = $e->fields[total]+0;
				if($te > 0) {
					$this->error['email'] = '<li>'.$lang['16'].'</li>';
					return false;
				}
				else {
					$this->record = array(
						'field'	=>	'email',
						'value'	=>	$val
					);
				}
			}
			// Checking Email end
		}
		elseif ($field == 'mobile') {
			// Checking Mobile
			if($val == '' || !is_numeric($val) || strlen($val) != 9 || substr($val, 0, 1) != '5' || substr($val, 0, 3) == '500' || substr($val, 0, 3) == '511' || substr($val, 0, 3) == '512') {
				$this->error['mobile'] = '<li>'.$lang['763'].'</li>';
				return false;
			}
			else {
				$q = 'SELECT count(*) as total FROM `members` WHERE `mobile` like "%'.sql_quote($val).'" AND `USERID`!="'.sql_quote($USERID).'" LIMIT 1';
				$e = $conn->execute($q);
				$mu = $e->fields[total]+0;
				if ($mu > 0) {
					$this->error['mobile'] = '<li>'.$lang['765'].'</li>';
					return false;
				}
				else {
					$this->record = array(
						'field'	=>	'mobile',
						'value'	=>	$val
					);
				}
			}
			// Checking Mobile end
		}
		elseif ($field == 'profilepicture') {
			// Profilepicture routine
			$uploadClass = new uploadClass();
			$uploadClass->allowedTypes = array('image/jpg', 'image/jpeg', 'image/png');
			$uploadClass->filepath = $config['membersprofilepicdir'].'/o/';
			$uploadClass->filename = $_SESSION['USERID'].'-'.substr(md5($_SERVER['REQUEST_TIME']),-4);
			$uploadedPath = $uploadClass->processUpload($val);

			if ($uploadedPath) {
				$query='SELECT `profilepicture` FROM `members` WHERE `USERID`="'.sql_quote($_SESSION['USERID']).'" AND `status`="1" LIMIT 1';
				$r=$conn->execute($query);
				$profilepicture = $r->fields['profilepicture'];

				unlink($config['membersprofilepicdir'].'/o/'.$profilepicture);
				unlink($config['membersprofilepicdir'].'/thumbs/'.$profilepicture);
				unlink($config['membersprofilepicdir'].'/thumbs/A_'.$profilepicture);
				unlink($config['membersprofilepicdir'].'/'.$profilepicture);
				do_resize_image($config['membersprofilepicdir'].'/o/'.$uploadClass->filename.$uploadClass->fileext, '100', '100', false, $config['membersprofilepicdir'].'/'.$uploadClass->filename.$uploadClass->fileext, true);
				do_resize_image($config['membersprofilepicdir'].'/o/'.$uploadClass->filename.$uploadClass->fileext, '54', '54', false, $config['membersprofilepicdir'].'/thumbs/'.$uploadClass->filename.$uploadClass->fileext, true);
				do_resize_image($config['membersprofilepicdir'].'/o/'.$uploadClass->filename.$uploadClass->fileext, '32', '32', false, $config['membersprofilepicdir'].'/thumbs/A_'.$uploadClass->filename.$uploadClass->fileext, true);
				
				$q = 'UPDATE `members` SET `profilepicture`="'.sql_quote($uploadClass->filename.$uploadClass->fileext).'" WHERE `USERID`="'.sql_quote($_SESSION['USERID']).'" LIMIT 1';
				$conn->execute($q);
				$this->record = array(
					'field'	=>	'profilepicture_protect',
					'value'	=>	1
				);
			}
			else {
					$this->error['profilepicture'] = '<li>'.$lang['823'].'</li>';
					return false;
				}
			// Profilepicture routine end
		}
		elseif ($field == 'exp') {
			// Experience routine
			$expArr = explode(',', $val);

			$expClass = new expClass();
			$mainKeywords = $expClass->getMainKeys();
			$aliases = $expClass->getAliasKeys();
			$conn->execute('DELETE FROM `members_exp` WHERE `USERID`="'.intval($_SESSION['USERID']).'" LIMIT 10');
			foreach ($expArr as $e=>$exp) {
				$KID = $AID = null;
				if ($exp) {
					if (preg_match('/KID=(\d+)\|?/', $exp, $kArr)) {
						$KID = $kArr[1];
					}
					if (preg_match('/\|AID=(\d+)/', $exp, $aArr)) {
						$AID = $aArr[1];
					}
					$insertSQL = '';
					if (!$KID)
						continue;
					if ($KID && is_numeric($KID) && isset($mainKeywords[$KID])) {
						$expDisplay[$e+1] = array(
							'KID' => $KID,
							'title' => $mainKeywords[$KID]['keyword']
							);
						// Inserting MainKeyword
						$insertSQL = '`CATID`="'.intval($mainKeywords[$KID]['CATID']).'", `KID`="'.intval($KID).'"';
					}

					if ($AID && is_numeric($AID) && isset($aliases[$AID])) {
						$expDisplay[$e+1]['AID'] = $AID;
						$expDisplay[$e+1]['title'] .= ' / '.$aliases[$AID]['alias'];
						// Inserting Alias
						$insertSQL .= ', `ALIASID`="'.intval($AID).'"';
					}

					if ($insertSQL) {
						$conn->execute('INSERT IGNORE INTO `members_exp` SET `USERID`="'.intval($_SESSION['USERID']).'", '.$insertSQL);
					}
				}
			}
			// Experience routine end
		}
		elseif ($field == 'description') {
			if (isPMvalid($val)!==true || includes_link($val)) {
				$this->error['details'] = '<li>'.$lang['466'].'</li>';
				return false;
			}
			else {
				$this->record = array(
					'field'	=>	'description',
					'value'	=>	$val
				);
			}
		}



		return true;
	}
}
?>