<?php
include("include/config.php");
include("include/functions/import.php");

if ($_SESSION['USERID'] && ($blocked_till = user_blocked_till($_SESSION['USERID']))) {
	$error = sprintf($lang['662'], $blocked_till);
	$templateselect = "empty.tpl";
	abr('pathURI','inbox');
	abr('pathTitle',$lang['226']);
	abr('heading',$lang['732']);
	abr('subheading',$lang['733']);
}

elseif ($_SESSION['USERID'] != "" && $_SESSION['USERID'] >= 0 && is_numeric($_SESSION['USERID']))
{	

	$verify_pm = $config['verify_pm'];
	if($verify_pm == "1" && $_SESSION['VERIFIED'] == "0")
	{
		$error = sprintf($lang['479'], $config['baseurl'].'/resendconfirmation.php');
		$templateselect = "empty.tpl";
	}
	else
	{
		$aboutid = intval(cleanit($_REQUEST['id']));
		if($aboutid > 0)
		{
			abr('aboutid',$aboutid);
		}
		$u = cleanit($_REQUEST['u']);
		if (!$u && isset($_REQUEST['mid'])) {
			$query="SELECT A.`username` FROM `members` A, `inbox` B WHERE B.`MID`='".intval($_REQUEST['mid'])."' AND A.`USERID`=B.`MSGFROM` LIMIT 1";
			$result=$conn->execute($query);
			$u = $result->fields['username'];
		}
		if($u != "")
		{
			$query="SELECT `username`, `USERID` FROM `members` WHERE `username`='".sql_quote($u)."' LIMIT 1";
			$results=$conn->execute($query);
			$u = $results->getrows();
			abr('u',$u[0]);
			$UID = $u[0]['USERID'];
			
			if($UID > 0)
			{
				if ($UID == $_SESSION['USERID']) {
					header("Location:{$config[baseurl]}/inbox");exit;
				}
				$templateselect = 'conversations.tpl';
				$pagetitle = $lang['235'].' '.$u[0]['username'];
				abr('pagetitle',$pagetitle);
				
				$query="SELECT DISTINCT A.`username` AS mto, C.`username` AS mfrom, B.* FROM `members` A, `inbox` B, `members` C WHERE A.`USERID`=B.`MSGTO` AND C.`USERID`=B.`MSGFROM` AND ((B.`MSGTO`='".sql_quote($_SESSION['USERID'])."' AND B.`MSGFROM`='".sql_quote($UID)."') OR (B.`MSGTO`='".sql_quote($UID)."' AND B.`MSGFROM`='".sql_quote($_SESSION['USERID'])."')) ORDER BY B.`MID` ASC";
				$results=$conn->execute($query);
				$m = $results->getrows();
				foreach ($m as $mi=>$msg) {
					$msg['message'] = hyperlinkize(urldecode($msg['message']));
					$m[$mi] = $msg;
				}

				abr('m',$m);
				
				$totalm = sizeof($m);
				if($totalm > 0)
				{
					$lastm = $totalm - 1;
					$lastm = $m[$lastm]['MID'];
				}
				else
				{
					$lastm = "0";
				}
				abr('lastm',$lastm);
			}
		}
		else {
			header("Location:$config[baseurl]/inbox");exit;
		}
	}
}
else
{
	$u = cleanit($_REQUEST['u']);
	$aboutid = intval(cleanit($_REQUEST['id']));
	$eurl = "conversations/".$u."?id=".$aboutid;
	$rurl = base64_encode($eurl);
	$_SESSION['temp']['message_title'] = $lang['724'];
	$_SESSION['temp']['message_type'] = 'error';
	header("Location:$config[baseurl]/signin?r=".$rurl);exit;
}

//TEMPLATES BEGIN
abr('plupload',true);
abr('error',$error);
abr('message',$message);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>