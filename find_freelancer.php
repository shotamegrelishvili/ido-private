<?php
include_once('include/config.php');
if (isset($_GET['query']) && $_GET['query']) {
	header("Location:{$config[baseurl]}/find-freelancer/{$_GET['query']}?mid={$_GET['mid']}&aid={$_GET['aid']}&c={$_GET['c']}&s={$_GET['s']}&wp={$_GET['wp']}&of={$_GET['of']}",true,301);
}
include_once('include/functions/import.php');
$thebaseurl = $config['baseurl'];

if (isset($_GET['q']) && $_GET['q']) {
	$_REQUEST['query'] = $_GET['q'];
	define ('NODISPLAY',true);
	abr('searching',true);
	if (!isset($_REQUEST['s'])) {
		$_REQUEST['s'] = 'r';
	}
	if (!isset($_REQUEST['of']) || !$_REQUEST['of']) {
		require_once('search.php');
	}
	else {
		abr('tag',$_REQUEST['query']);
	}

	if (!isset($_REQUEST['mid']) || !$_REQUEST['mid']) {
		define('IN_SEARCH',1);
		$data = trim($_REQUEST['query']);
		$_POST['action'] == 'getClosestValues';
		require_once($config['basedir'].'/include/functions/ajax/exp.php');
		$findFreelancersArray = array('CATID'=>$_REQUEST['catid'],'KID'=>null,'ALIASID'=>null);
	}
	elseif (isset($_REQUEST['mid']) && is_numeric($_REQUEST['mid'])) {
		$findFreelancersArray = array('CATID'=>null,'KID'=>$_REQUEST['mid'],'ALIASID'=>$_REQUEST['aid']);
	}

	require_once($config['basedir'].'/models/search.Class.php');
	abr('fr',$searchClass->findFreelancersByKeyID($findFreelancersArray, $_REQUEST['wp'], 30));
	abr('totalFreelancers',$searchClass->resultSize);
	
}

$_SESSION['temp']['ajax_allow'] = $_SERVER['REQUEST_TIME'] + 3600;
$templateselect = 'find_freelancer.tpl';

//TEMPLATES BEGIN
abr('hideHeaderSearch',true);
abr('pagetitle',stripslashes($lang['844']));
abr('addCss', 'findfreelancer');
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>