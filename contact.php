<?php
include("include/config.php");
include("include/functions/import.php");

/*
if (!$_SESSION['USERID'] && !$_SESSION['ADMINID']) {
	$_SESSION['temp']['message_title'] = $lang['724'];
	$_SESSION['temp']['message_type'] = 'error';
	header('Location:'.$config['baseurl'].'/signin?r='.base64_encode('contact'));
	exit;
}
*/
if ($_SESSION['USERID'] && ($blocked_till = user_blocked_till($_SESSION['USERID']))) {
	$error = sprintf($lang['662'], $blocked_till);
	$templateselect = "empty.tpl";
	abr('pathURI','contact');
	abr('pathTitle',$lang['537']);
	abr('heading',$lang['732']);
	abr('subheading',$lang['733']);
}
else {
	$thebaseurl = $config['baseurl'];

	$templateselect = "contact.tpl";
	$pagetitle = "$lang[417]";
	abr('pagetitle',$pagetitle);

	if (isset($_POST['subform']) && !$_POST['sdetect']) {
		$error = '';
		if (mb_strlen($_POST['contacttext'], 'UTF8') < 20)
			$error = $lang['716'];
		elseif (!$_POST['contactcase'] && is_numeric($_POST['contactcase']))
			$error = $lang['717'];
		elseif (!isset($_SESSION['USERID']) && (!isset($_POST['email']) || !$_POST['email'] || !verify_valid_email($_POST['email'])) )
			$error = $lang['15'];
		elseif (isset($_SESSION['USERID']) && $_SESSION['EMAIL']) {
			$username = sql_quote($_SESSION['USERNAME']);
			$email = sql_quote($_SESSION['EMAIL']);
		}
		else {
			$username = $_SERVER['REMOTE_ADDR'];
			$email = sql_quote($_POST['email']);
		}

		if (!$error) {
			require_once('libraries/cache.class.php');
			$contactSentIPs = $cache->get('contactSentIPs');
			
			if ((!isset($_SESSION['temp']['contactmessage_md5']) || $_SESSION['temp']['contactmessage_md5'] != md5($_POST['contacttext'])) && (!isset($contactSentIPs[$_SERVER['REMOTE_ADDR']]) || $contactSentIPs[$_SERVER['REMOTE_ADDR']]['contact_times'] <= 2 || $contactSentIPs[$_SERVER['REMOTE_ADDR']]['last_contact_time'] < $_SERVER['REQUEST_TIME'] - 86400)) {
				$query="INSERT INTO `contact` SET `username`='".$username."', `email`='".$email."', `USERID`='".sql_quote($_SESSION['USERID'])."', `caseid`='".intval($_POST['contactcase'])."', `casetext`='".sql_quote($_POST['contacttext'])."', `datetime`=NOW()";
				$result=$conn->execute($query);
				$mid = $conn->_insertid();

				$sendmailbody =  $lang['36'].": <a href=\"".$config['baseurl']."/user/".$_SESSION['USERNAME']."\">".$username."</a> ".($_SESSION['VERIFIED'] == '0' ? '('.$lang['708'].')' : '')."<br />";
				$sendmailbody .= $lang['4'].": ".$email."<br />";
				$sendmailbody .= $lang['719'].$lang[$config['contact_matrix'][$_POST['contactcase']]]."<br /><br />";
				$sendmailbody .= $lang['720'].stripslashes($_POST['contacttext'])."<br /><br />";
				$sendmailbody .= $lang['721']."<a href=\"".$config['baseurl']."/contact#row-".$mid."\">".$config['baseurl']."/contact#mid-".$mid."</a><br /><br />";
				$sendmailbody .= stripslashes($config['site_name']);
				
				mailme($config['adminemail'],$config['site_name'],$config['site_email'],$lang['718'].$lang[$config['contact_matrix'][$_POST['contactcase']]],$sendmailbody);
				$contactSentIPs[$_SERVER['REMOTE_ADDR']]['last_contact_time'] = $_SERVER['REQUEST_TIME'];
				$contactSentIPs[$_SERVER['REMOTE_ADDR']]['contact_times'] = (isset($contactSentIPs[$_SERVER['REMOTE_ADDR']]['contact_times']) ? $contactSentIPs[$_SERVER['REMOTE_ADDR']]['contact_times']+1 : 1);
				$cache->set('contactSentIPs', $contactSentIPs, 86400);
				$message = $lang['722'];
				$_SESSION['temp']['contactmessage_md5'] = md5($_POST['contacttext']);
				
				unset($_POST);
			}
			else
				$error = $lang['723'];
		}
	}

	if ($_SESSION['ADMINID']) {
		if (isset($_POST['replyto']) && is_numeric($_POST['replyto'])) {
			$query = "SELECT * FROM `contact` WHERE `is_del`='0' AND `id` = '".intval($_POST['replyto'])."' LIMIT 1";
			$contact=$conn->execute($query);
			if (!$contact->fields['id']) {
				$error = $lang['727'];
			}
			elseif ($contact->fields['answer']) {
				$error = $lang['728'];
			}

			if (!$error) {
				if ($contact->fields['USERID']) {
					$conn->execute("INSERT INTO `inbox` SET `MSGFROM`='".intval($config['adminUserID'])."', `MSGTO`='".intval($contact->fields['USERID'])."', `message`='".sql_quote($_POST['contacttext'])."', `time`='".$_SERVER['REQUEST_TIME']."'");
					$mid = $conn->_insertid();
					$notifications = new notificationsClass();
					$notifications->msgto = $contact->fields['USERID'];
					$notifications->msgfrom = $config['adminUserID'];
					$notifications->action = 'new_pm';
					$notifications->link = 'conversations/'.$config['adminNickname'].'?mid='.$mid;
					$notifications->send_instant_notification();
				}
				$conn->execute("UPDATE `contact` SET `answer`='".sql_quote($_POST['contacttext'])."', `answer_datetime`=NOW() WHERE `id`='".intval($_POST['replyto'])."' LIMIT 1");
				mailme($contact->fields['email'],$config['site_name'],$config['site_email'],$lang['725'].': '.$lang[$config['contact_matrix'][$_POST['contactcase']]],stripslashes($_POST['contacttext']).$lang['661']);
				$message = $lang['729'];
			}

		}
		$query = "SELECT * FROM `contact` WHERE `is_del`='0' AND `answer` is NULL ORDER BY `id` ASC";
		$executequery=$conn->execute($query);
		$crows = $executequery->getrows();
		abr('crows', $crows);
		foreach ($config['contact_matrix'] as $k=>$v) {
			$langArray[$k] = $lang[$v];
		}
		abr('caseLangArray', $langArray);
	}
}

//TEMPLATES BEGIN
abr('message',$message);
abr('error',$error);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>