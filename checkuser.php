<?php
include("include/config.php");
include("include/functions/import.php");

$username = cleanit($_REQUEST['username']);

if($username != "")
{
	if(!preg_match("/^[a-zA-Z0-9]*$/i",$username))
	{
		echo $lang['24'];
	}
	else
	{
		if(verify_email_username($username))
		{
			echo "OK";
		}
		else
		{
			echo $lang['14'];	
		}
	}
}

?>