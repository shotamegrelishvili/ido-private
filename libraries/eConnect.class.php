<?php

class econnect {
	const apiKey = '449YYYVGOZXPN5QA5W2N2CG8UGP6R7TE';
	const restfulApiURI = 'https://api.emoney.ge/v1/Service.svc/json/';
	public $_debug = true;

	public $_status;
	public $_requestHeaders;
	public $callUri='';
	public $econnected_user;
	public $_response;
	public $INN_access_token;
	public $pinInserted = false;
	public $eemailInserted = false;


	public function eAuth($username, $password) {
		return $this->_sendRequest(array('appkey'=>econnect::apiKey, 'user'=>$username, 'password'=>$password), 'user/login');
	}

	public function getUserInfo($access_token) {
		$this->econnected_user = $this->_Get('user/', $access_token);
		return $this->econnected_user;
	}

	protected function _sendRequest($curlopts = array(), $uri, $accessToken='')
    {
        //$this->_responseHeaders = array();
        $this->_requestHeaders = array();
        $this->_response = array();

        $headers = array(	'Content-Type: application/json',
							'Accept: application/json; charset=utf-8',
							'client_ip: ' . $_SERVER['REMOTE_ADDR'],
							'access_token: ' . $accessToken,
							'app_key: ' . econnect::apiKey
		);
		
		/*d(econnect::restfulApiURI.$uri);
		d($curlopts);
		d('token: '.$accessToken);*/
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, econnect::restfulApiURI.$uri);
		curl_setopt ($ch, CURLOPT_POST, TRUE);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($curlopts));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt ($ch, CURLOPT_HEADER, 0);

		if($this->_debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        }
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt ($ch, CURLOPT_FAILONERROR, FALSE);
		curl_setopt ($ch, CURLOPT_HTTP200ALIASES, array(400));
		$this->_response = curl_exec($ch);
		$this->_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		//d($this->_status);
		//d($this->_response,1);
		if ($this->_status == 500)
			$this->callUri = econnect::restfulApiURI.$uri;
		$this->_requestHeaders = explode("\n", curl_getinfo($ch, CURLINFO_HEADER_OUT));
		curl_close($ch);
        return $this->_response;
    }

    public function _Get($uri, $accessToken = '') {
		$headers = array(
		'Content-Type: application/json',
		'Accept: application/json; charset=utf-8',
		'client_ip: ' . $_SERVER['REMOTE_ADDR'],
		'access_token: ' . $accessToken,
		'app_key: ' . econnect::apiKey
		);

		$ch = curl_init();

		curl_setopt ($ch, CURLOPT_URL, econnect::restfulApiURI.$uri);
		curl_setopt ($ch, CURLOPT_HTTPGET, TRUE);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_VERBOSE, 0);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt ($ch, CURLOPT_FAILONERROR, FALSE);
		curl_setopt ($ch, CURLOPT_HTTP200ALIASES, array(400));
		$this->_response = curl_exec($ch);
		$this->_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		return json_decode($this->_response, true);
	}

	public function _CheckProtectedFields($UID, $econnected_user) {
		global $conn, $config;

		$query="SELECT `pin`, `pinprotect`,`eemail`,`eemailprotect` FROM `members` WHERE `USERID`='".intval($UID)."' AND `status`='1' LIMIT 1";
		$results=$conn->execute($query);
		$pin = $results->fields['pin'];
		//$pinprotect = $results->fields['pinprotect']; // Always protect PIN
		$pinprotect = 1;
		$eemail = $results->fields['eemail'];
		$eemailprotect = $results->fields['eemailprotect'];
		//d($econnected_user['email'],1);
		if (($eemail && $eemailprotect && isset($econnected_user['email']) && $econnected_user['email'] && $eemail!=$econnected_user['email']) || ($pin && $pinprotect && isset($econnected_user['pin']) && $econnected_user['pin'] && $pin!=$econnected_user['pin'])) {
			require_once($config['base_url']."/libraries/cache.class.php");
			$userErr = $cache->get('useErr');
			$userErr[] = array('eemail'=>$eemail, 'pin'=>$pin, 'econnectemail'=>$econnected_user['email'], 'econnectpin'=>$econnected_user['pin']);
			$cache->set('userErr', $userErr, 86400*2);
			return true;
		}
		else return false;
	}

	public function _Authorize($email, $password) {
		$r = $this->eAuth($email, $password);
		if ($this->_status == 400) return false;
		elseif ($this->_status == 200) return json_decode($r);
		else return false;
	}

	public function _UpdateSession($res) {
		$_SESSION['access_token'] = $res->access_token;
		$_SESSION['signin_gateway'] = 'econnect';
		$_SESSION['session_expiry'] = $_SERVER['REQUEST_TIME'] + 3000;
		//d($_SESSION,1);
	}

	public function _CheckDuplicateUser($user) {
		global $conn;

		if (!$user['email']) return false;
		$sqladd = '';

		if ($user['pin']) $sqladd = " OR (`pin`='".$user['pin']."' AND `pinprotect`='1')";
		$result=$conn->execute("SELECT `pin`, `eemail` FROM `members` WHERE `USERID`!='".intval($_SESSION['USERID'])."' AND ((`eemail`='".sql_quote($user['email'])."' AND `eemailprotect`='1')".$sqladd.") LIMIT 1");
		if ($result->_numOfRows)
			return true;
		else return false;
	}

	public function _UpdateUser($userData, $UID=0) {
		global $conn, $config;

		if (!$UID) 
			if (!isset($_SESSION['USERID']) || !$_SESSION['USERID']) return false;
			else $UID = intval($_SESSION['USERID']);
		
		$result=$conn->execute("SELECT `USERID`, `pin`, `pinprotect`, `eemail`, `eemailprotect`, `mobile`, `profilepicture` FROM `members` WHERE `USERID`='".$UID."' LIMIT 1");
		$sqladd = '';
		if (!$result->fields['profilepicture'] && $userData['avatar']) {
			upload_and_update_avatar($UID, $userData['avatar']);
		}
		if (!$result->fields['pinprotect'] && $userData['pin']) {
			$this->pinInserted = true;
			$sqladd .= ",`pin`='".sql_quote($userData['pin'])."', `pinprotect`='1'";
		}
		if (!$result->fields['mobile'] && $userData['phone']) {
			$sqladd .= ",`mobile`='".sql_quote($userData['phone'])."'";
		}
		if (!$result->fields['eemail'] || !$result->fields['eemailprotect']) {
			$this->eemailInserted = true;
			$sqladd .= ",`eemail`='".sql_quote($userData['email'])."'";
		}
		if ($userData['locked']) {
			$sqladd .= ",`e_account_type`='business'";
		}

		if (@$_COOKIE['FB_isFan'] || @$_SESSION['FB_isFan']) {
			$sqladd .= ",`FB_isFan`='1'";
		}
		
		$conn->execute("UPDATE `members` SET `lastlogin`='".$_SERVER['REQUEST_TIME']."', `lip`='".$_SERVER['REMOTE_ADDR']."', `country`='".sql_quote($userData['country'])."', `verified`='1', `status`='1',`eemailprotect`='1'".$sqladd." WHERE `USERID`='".$UID."' LIMIT 1");

//		Deals to go Online once PIN & eMoney were provided
		if ($this->pinInserted || $this->eemailInserted)
			getUserDealsOnline($result->fields['USERID']);

		return true;
	}

	public function paymentWithHold($receiver, $amount, $currency='GEL', $description, $escrow, $withoutCommission=false) {
		if (!$receiver || !$amount || !$description || !$escrow) return false;
		//d(array('receiver'=>$receiver, 'amount'=>$amount, 'currency'=>$currency, 'description'=>$description, 'escrow'=>$escrow));
		//d($_SESSION);
		//d($this->getUserInfo($_SESSION['access_token']));
		if ($withoutCommission) {
			$operationid = 1;
		}
		else {
			$operationid = 0;
		}
//		d(array('receiver'=>$receiver, 'amount'=>$amount, 'currency'=>$currency, 'description'=>$description, 'escrow'=>$escrow, 'denyusercanceltransaction'=>1, 'operation_id'=>$operationid));
//		d($_SESSION,1);
		$startJsonRes = $this->_sendRequest(array('receiver'=>$receiver, 'amount'=>$amount, 'currency'=>$currency, 'description'=>$description, 'escrow'=>$escrow, 'denyusercanceltransaction'=>1, 'denyuserapprovetransaction' => 1, 'operation_id'=>$operationid), 'payment/sendamount_with_mediator', $_SESSION['access_token']);
		$startRes = json_decode($startJsonRes);
		$this->transactionCode = $startRes->transactioncode;

		if ($this->_status == 500) return '500';
		elseif (!$startRes->transactioncode) return '1'; // No TransactionCode. Possibly authorization needed
		else {
			$holdRes = $this->holdPayment($this->transactionCode);
			if ($holdRes->StatusCode==25) {
				if (!$this->INN_access_token) $this->authorizeINN();
				$this->cancelPendingPayment($this->transactionCode, $this->INN_access_token);
				return '5'; // Insufficient Funds
			}
			else {
				return '0'; // Ok, transaction registered
			}
		}
	}

	public function cancelPendingPayment($transactionCode, $access_token) {
		if (!$transactionCode) return false;

		$res = $this->_sendRequest(array(),'data/cancel_transaction/'.$transactionCode, $access_token);
	}

	public function holdPayment($transactionCode) {
		if (!$transactionCode) return false;
		$response = $this->_sendRequestSingleParam($transactionCode,'payment/pay_confirm', $_SESSION['access_token']);
		return(json_decode($response));
	}

	public function commitPayment($transactionCode) {
		if (!$transactionCode) return false;
		if (!$this->INN_access_token) {
			$this->authorizeINN();
		}
		//d('INN: '.$this->INN_access_token);
		//d('Trans Code: '.$transactionCode);
		if (!$this->INN_access_token) return false;
		$response = $this->_sendRequest(array('transaction_code'=>$transactionCode, 'approvementcode'=>1), 'payment/approvereceive', $this->INN_access_token);
		//d('Response: '.$response);
		//d('Status: '.$this->_status);
		//d('_respnse: '.$this->_response,1);

		if ($this->_status == 200)
			return true;
		else
			return false;
	}


	protected function _sendRequestSingleParam($param, $uri, $accessToken='') {

	    $headers = array(	'Content-Type: application/json',
							'Accept: application/json; charset=utf-8',
							'client_ip: ' . $_SERVER['REMOTE_ADDR'],
							'access_token: ' . $accessToken,
							'app_key: ' . econnect::apiKey
					);

		//d(econnect::restfulApiURI.$uri);
		$ch = curl_init( econnect::restfulApiURI.$uri );

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $param ); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER,     $headers);
		$result=curl_exec ($ch);

    	return $result;
    }

    function isUserVerified($transactionCode, $access_token) {
//    	d($transactionCode);
    	//$transactionCode = '765330048';
    	$res = $this->_Get('user/checksenderverificationstatus/'.$transactionCode, '');
    	if ($this->_status==200) {
    		if ($res == 1)
    			return true;
    		else return false;
    	}
    	return;
    }

    function authUserInEmoney() {
    	/*
    	$headers = array(	'Content-Type: application/json',
							'Accept: application/json; charset=utf-8',
							'HTTP_REFERER: http://ido.ge',
							'ip: ' . $_SERVER['REMOTE_ADDR'],
							't: ' . $_SESSION['access_token'],
							'u: ' . 'abcdefghijklm'
		);
		*/
        /*d(econnect::restfulApiURI.$uri);
		d($headers);
		d($curlopts);*/
		$curlopts = array('t'=>$_SESSION['access_token'], 'ip'=>$_SERVER['REMOTE_ADDR'], 'u'=>'abcdefghijklm');
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'https://www.emoney.ge/index.php/widget/autologintoemoneyweb');
		curl_setopt ($ch, CURLOPT_POST, TRUE);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $curlopts);
		//curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);
		//curl_setopt ($ch, CURLOPT_HEADER, 0);
		curl_setopt ($ch, CURLOPT_REFERER, 'https://ido.ge');

		if(true) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        }
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt ($ch, CURLOPT_FAILONERROR, FALSE);
		curl_setopt ($ch, CURLOPT_HTTP200ALIASES, array(400));
		$_response = curl_exec($ch);

		$_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$_requestHeaders = explode("\n", curl_getinfo($ch, CURLINFO_HEADER_OUT));
		curl_close($ch);
        
    	print_r($_status);
    	print_r($_response);
    }

    function authorizeINN() {
    	global $config;
		if (!($res=$this->_Authorize($config['emoney_app_key'], $config['emoney_app_pass']))) {
			d('INN authorize error');
			return false;
		}
		else {
			$this->INN_access_token = $res->access_token;
		}
	}
}

?>