<?php
class cryptClass {
	
	private $key=')mM.*7jQ';

	public function __construct() {
		global $config;

		if (isset($config['cryptKey'])) {
			$this->key = $config['cryptKey'];
		}
	}
	
	public function Encrypt($text){
		if(empty($text))
		 return;
		$string = base64_encode($this->mcrypt_ecb_encrypt($this->key,$text));
		return $string;
	}
	
	public function Decrypt($text){
		if(empty($text))
		 return;
		$string = $this->mcrypt_ecb_decrypt($this->key,base64_decode($text));
		return trim($string);
	}
	
	private function mcrypt_ecb_encrypt($key, $input) {
		$td = mcrypt_module_open('des', '', 'ecb', '');
		$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $key, $iv);
		$encrypted_data = mcrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return $encrypted_data;
	}

	private function mcrypt_ecb_decrypt($key, $input) {
		$td = mcrypt_module_open('des', '', 'ecb', '');
		$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $key, $iv);
		$encrypted_data = mdecrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return $encrypted_data;
	}
}
?>