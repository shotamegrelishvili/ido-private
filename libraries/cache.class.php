<?php
if (class_exists('Memcached')) {
	$memcache = new Memcached();
	$memcache->addServer($config['memcached'], 11211);
	$memcache->setOption('server_max_value_length', 1024*1024*15);
	// $memcache->setOption(Memcached::OPT_COMPRESSION, true);
    global $memcache;
}

if (!isset($cache)) {
	$cache = new cache();
	global $cache;
}

class cache {
	// Where things are cached to (must have trailing slash!)
	var $cacheDir = '/temporary/cache/';
	// How long to cache something for in seconds, default 1hr
	var $defaultCacheLife = '3600';
	
	function __construct() {
		global $config;
		$this->cacheDir = $config['basedir'] . $this->cacheDir;
	}
	/**
        Set($varId, $varValue) --
        Creates a file named "cache.VARID.TIMESTAMP"
        and fills it with the serialized value from $varValue.
        If a cache file with the same varId exists, Delete()
        will remove it.
	 **/
	function Set($varId, $varValue, $varTime=60) {
		global $config, $memcache;
		
		if ($config ['debug'] == 1) {
			add_debug ( 'recache: ' . $varId . ' = ' . $varValue );
		}
		
		if (class_exists('Memcached')) {
			$memcache->set($varId, $varValue, $varTime);
		}
		else {
			// Clean up old caches with same varId
			// $this->Delete($varId);
			// Create new file
			// $fileHandler = fopen($this->cacheDir . "cache." . $varId . "." . time(), "a");
			$file = $this->cacheDir . 'cache.' . $varId;
			//echo $file.'<br />';
			$fileHandler = fopen ( $file, 'w' );
			// Write serialized data
			$s = fwrite ( $fileHandler, serialize ( $varValue ) );
			fclose ( $fileHandler );
		}
		return;
	}

	function Increment($varId) {
		global $config, $memcache;

		if (class_exists('Memcached')) {
			$memcache->increment($varId);
		}
		else {
			$varValue = $this->Get($varId);
			$this->Set($varId, $varValue+1, 86400);
		}
		return;
	}

	function Decrement($varId) {
		global $config, $memcache;

		if (class_exists('Memcached')) {
			$memcache->decrement($varId);
		}
		else {
			$varValue = $this->Get($varId);
			$this->Set($varId, $varValue-1, 86400);
		}
		return;
	}
	
	/**
        Get($varID, $cacheLife) --
        Retrives the value inside a cache file
        specified by $varID if the expiration time
        (specified by $cacheLife) is not over.
        If expired, returns FALSE
	 **/
	function Get($varId, $cacheLife = 0) {
		global $config, $memcache;
		// Set default cache life
		//$cacheLife = (! empty ( $cacheLife )) ? $cacheLife : $this->defaultCacheLife;
		if (class_exists('Memcached')) {
			return $memcache->get($varId);
		}
		else {
			if ($cacheLife !== false && $cacheLife != '0' && ! is_numeric ( $cacheLife )) {
				$cacheLife = $this->defaultCacheLife;
			}
			
			if($cacheLife == 0) {
				$cacheLife = false;
			}
			

			$file = $this->cacheDir . 'cache.' . $varId;
			//d($file);
			//echo '<br />';
			
			if (file_exists ( $file ) && filesize ( $file ) > 0) {
				if ($cacheLife === false || (time () - filemtime ( $file )) <= $cacheLife) {
					$fileHandler = fopen ( $file, 'r' );
					$varValueResult = fread ( $fileHandler, filesize ( $file ) );
					fclose ( $fileHandler );
					// Still good, return unseralized data
					return unserialize ( $varValueResult );
				} else {
					
					if ($config ['debug'] == 1) {					
						//add_debug ( "cache expire: " . $varId . " TimeExpire: " . unix_time ( filectime ( $file ) ) );
					}
					
					return false;
				}
			} else {			
				if ($config ['debug'] == 1) {
					add_debug ( 'cache file not exists! ' . $varId );
				}
				
				return false;
			}
			
			return FALSE;
		}
	}
	
	/**
        Delete($varId) --
        Loops through the cache directory and
        removes any cache files with the varId
        specified in $varID
	 **/
	function Delete($varId) {
		global $memcache;

   		if (class_exists('Memcached')) {
			$memcache->delete($varId);
			return;
		}
		$file = $this->cacheDir . 'cache.' . $varId;
		if (file_exists ( $file )) {
			unlink ( $file );
			return true;
		}
		return false;
	}

}
?>