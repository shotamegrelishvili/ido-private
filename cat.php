<?php
include("include/config.php");
include("include/functions/import.php");
$thebaseurl = $config['baseurl'];
$cid = cleanit($_REQUEST['cid']);
if (!$cid) {
	header("Location:{$config[baseurl]}");exit;
}
else
{
abr('searching', true);
	$query="SELECT `name`,`CATID`,`parent`,`mtitle`,`mdesc`,`mtags`,`details` FROM `categories` WHERE `seo`='".sql_quote($cid)."' LIMIT 1";
	$executequery=$conn->execute($query);
	$CATID = $executequery->fields['CATID'];
	if (!$CATID) {
		header("Location:{$config[baseurl]}");exit;
	}
	$parent = $executequery->fields['parent'];
	$cname = $executequery->fields['name'];
	$mtitle = $executequery->fields['mtitle'];
	$mdesc = $executequery->fields['mdesc'];
	$mdetails = $executequery->fields['details'];
	$mtags = $executequery->fields['mtags'];
	abr('cname',$cname);
	abr('pagetitle',$cname." ".$lang['123']);
	abr('CATID',$CATID);
	abr('mtitle',$mtitle);
	abr('mdesc',$mdesc);
	abr('mdetails',$mdetails);
	abr('mtags',$mtags);
	abr('catrand',rand(1,3));
	if($CATID != "" && is_numeric($CATID))
	{
		abr('cid',$cid);
		abr('catselect',$CATID);
		$s = cleanit($_REQUEST['s']);
		abr('s',$s);
		
		$page = intval($_REQUEST['page']);
		
		if($page=="")
		{
			$page = "1";
		}
		$currentpage = $page;
		
		if ($page >=2)
		{
			$pagingstart = ($page-1)*$config['items_per_page'];
		}
		else
		{
			$pagingstart = "0";
		}
		
		if($s == "r")
		{
			$dby = "A.`rating` desc, A.`rcount` desc";	
		}
		elseif($s == "rz")
		{
			$dby = "A.rating asc";	
		}
		elseif($s == "p")
		{
			$dby = "A.viewcount desc";	
		}
		elseif($s == "pz")
		{
			$dby = "A.viewcount asc";	
		}
		elseif($s == "c")
		{
			$dby = "A.price asc";	
		}
		elseif($s == "cz")
		{
			$dby = "A.price desc";	
		}
		elseif($s == "dz")
		{
			$dby = "A.PID asc";	
		}
		else
		{
			$dby = "A.PID desc";	
		}
		
		if($s == "ez")
		{
			$dby = "A.PID asc";	
			$addsql = "AND days='1'";
			$addsqlb = "AND A.days='1'";
		}
		elseif($s == "e")
		{
			$dby = "A.PID desc";	
			$addsql = "AND days='1'";
			$addsqlb = "AND A.days='1'";
		}
		
		$p = intval(cleanit($_REQUEST['p']));
		if($p > 0)
		{
			$result_addprice = " AND A.price='".sql_quote($p)."'";
			$result_addpriced = " AND price='".sql_quote($p)."'";
			abr('p',$p);
			$addp = "&p=$p";
		}
		
		if($parent == "0" && $CATID != "0")
		{
			
			$query="SELECT `CATID` FROM `categories` WHERE `parent`='".sql_quote($CATID)."'";
			$results=$conn->execute($query);
			$searchsc = $results->getrows();
			if(($ssc = sizeof($searchsc)) > 0)
			{
				for($i=0; $i<$ssc;$i++)
				{
					$ssc .= " OR A.category='".sql_quote($searchsc[$i][0])."'";
					$ssd .= " OR category='".sql_quote($searchsc[$i][0])."'";
				}
				$addtosearch = "AND (A.category='".sql_quote($CATID)."' $ssc)";
				$addtosearch2 = "AND (category='".sql_quote($CATID)."' $ssd)";
			}
			else
			{
				$addtosearch = "AND A.category='".sql_quote($CATID)."'";
				$addtosearch2 = "AND category='".sql_quote($CATID)."'";
			}
		}
		else
		{
			$addtosearch = "AND A.category='".sql_quote($CATID)."'";
			$addtosearch2 = "AND category='".sql_quote($CATID)."'";
		}
		
		if (isset($_SESSION['MODERATOR_MODE']) && isset($_SESSION['ADMINID']) && $_SESSION['ADMINID'] && (!isset($_REQUEST['god_mode']) || $_REQUEST['god_mode']==1)) {
			$activesql = "`active`='0' AND `is_del`='0' AND `last_status` IN ('0')";
		}
		else
			$activesql = "`active`='1' AND `is_del`='0'";
		$query1 = "SELECT count(*) as total FROM `posts` WHERE $activesql $addtosearch2 $addsql $result_addpriced ORDER BY `PID` DESC LIMIT $config[maximum_results]";
		$query2 = "SELECT A.*, B.`seo`, C.`username`, C.`country`, C.`toprated`, C.`profilepicture`, C.`rating` FROM `posts` A, `categories` B, `members` C WHERE $activesql $addtosearch $addsqlb AND A.`category`=B.`CATID` AND A.`USERID`=C.`USERID` $result_addprice ORDER BY A.`feat_mainpage` DESC, A.`feat_cat` DESC, $dby LIMIT $pagingstart, ".($config[items_per_page]+20);
//		d($query2);
		$executequery1 = $conn->Execute($query1);
		$result = $executequery1->fields['total'];
		if ($result > 0)
		{
			if($executequery1->fields['total']<=$config[maximum_results])
			{
				$total = $executequery1->fields['total'];
			}
			else
			{
				$total = $config[maximum_results];
			}
			$toppage = ceil($total/$config[items_per_page]);
			if($toppage==0)
			{
				$xpage=$toppage+1;
			}
			else
			{
				$xpage = $toppage;
			}
			$executequery2 = $conn->Execute($query2);
			$posts = $executequery2->getrows();
			$beginning=$pagingstart+1;
			$ending=$pagingstart+$executequery2->recordcount();
			$pagelinks="";
			$k=1;
			$theprevpage=$currentpage-1;
			$thenextpage=$currentpage+1;
			if($s != "")
			{
				$adds = "&s=$s".$addp;
			}
			if ($currentpage > 0)
			{
				if($currentpage > 1) 
				{
					$pagelinks.="<li class='prev'><a href='$thebaseurl/categories/$cid?page=$theprevpage$adds'>$theprevpage</a></li>&nbsp;";
				}
				else
				{
					$pagelinks.="<li><span class='prev'>previous page</span></li>&nbsp;";
				}
				$counter=0;
				$lowercount = $currentpage-5;
				if ($lowercount <= 0) $lowercount = 1;
				while ($lowercount < $currentpage)
				{
					$pagelinks.="<li><a href='$thebaseurl/categories/$cid?page=$lowercount$adds'>$lowercount</a></li>&nbsp;";
					$lowercount++;
					$counter++;
				}
				$pagelinks.="<li><span class='active'>$currentpage</span></li>&nbsp;";
				$uppercounter = $currentpage+1;
				while (($uppercounter < $currentpage+10-$counter) && ($uppercounter<=$toppage))
				{
					$pagelinks.="<li><a href='$thebaseurl/categories/$cid?page=$uppercounter$adds'>$uppercounter</a></li>&nbsp;";
					$uppercounter++;
				}
				if($currentpage < $toppage) 
				{
					$pagelinks.="<li class='next'><a href='$thebaseurl/categories/$cid?page=$thenextpage$adds'>$thenextpage</a></li>";
				}
				else
				{
					$pagelinks.="<li><span class='next'>next page</span></li>";
				}
			}
		}
		
		$query="SELECT `gtags` FROM `posts` WHERE `category`='".sql_quote($CATID)."' AND `active`='1' ORDER BY rand() LIMIT 20";
		$results=$conn->execute($query);
		$gtags = $results->getrows();
		for($i=0; $i<count($gtags);$i++)
		{
			$tags .= $gtags[$i][0]." ";
		}
		$tags = str_replace(",", " ", $tags);
		$tags = str_replace("  ", " ", $tags);
		$tags = str_replace("/", "", $tags);
		$tags = explode(" ", implode(" ", array_unique(explode(" ", $tags))));
		abr('tags',$tags);
		
		/*
		$queryst = "SELECT name, seo from categories WHERE parent='".sql_quote($CATID)."'";
		$resultst=$conn->execute($queryst);
		$scats = $resultst->getrows();
		abr('scats',$scats);
		*/
		if(count($posts) == "0")
		{
			$error = $lang['506'];
			abr('error',$error);	
		}
		$templateselect = "cat.tpl";
	}
}

//TEMPLATES BEGIN
abr('message',$message);
abr('error',$error);
abr('beginning',$beginning);
abr('ending',$ending);
abr('pagelinks',$pagelinks);
abr('itemsPerPage',$config[items_per_page]);
abr('total',$total);
abr('posts',$posts);
$smarty->display('header.tpl');
$smarty->display($templateselect);
$smarty->display('footer.tpl');
//TEMPLATES END
?>